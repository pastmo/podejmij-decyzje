<?php

use Zend\Session\Storage\SessionArrayStorage;
use Zend\Session\Validator\RemoteAddr;
use Zend\Session\Validator\HttpUserAgent;

return array(
	'db' => array(
		'driver' => 'Pdo',
		'dsn' => 'mysql:dbname=decyzje_test;host=localhost',
		'driver_options' => array(
			PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
		),
	),
	'service_manager' => array(
		'factories' => array(
			'Zend\Db\Adapter\Adapter'
			=> 'Zend\Db\Adapter\AdapterServiceFactory',
		),
	),
	'session_config' => [
		'cookie_lifetime' => 60 * 60 * 10,
		'gc_maxlifetime' => 60 * 60 * 24 * 30,
	],
	'session_manager' => [
		'validators' => [
			RemoteAddr::class,
			HttpUserAgent::class,
		]
	],
	'session_storage' => [
		'type' => SessionArrayStorage::class
	],
);
