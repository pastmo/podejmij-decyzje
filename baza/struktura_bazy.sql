-- MySQL dump 10.16  Distrib 10.1.13-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: decyzje
-- ------------------------------------------------------
-- Server version	10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cennik`
--

DROP TABLE IF EXISTS `cennik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cennik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kwota` decimal(10,2) DEFAULT NULL,
  `opis` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `dni_przedluzenia` int(11) NOT NULL DEFAULT '0',
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktualizacji` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dane_do_faktury`
--

DROP TABLE IF EXISTS `dane_do_faktury`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dane_do_faktury` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `adres_id` int(11) DEFAULT NULL,
  `nip` varchar(15) COLLATE utf8_polish_ci DEFAULT NULL,
  `termin_platnosci` date DEFAULT NULL,
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktualizacji` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_dane_do_faktury_adresy` (`adres_id`),
  KEY `FK_dane_do_faktury_konta` (`konto_id`),
  CONSTRAINT `FK_dane_do_faktury_adresy` FOREIGN KEY (`adres_id`) REFERENCES `adresy` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_dane_do_faktury_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `decyzje`
--

DROP TABLE IF EXISTS `decyzje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `decyzje` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `nazwa` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `wynik` enum('tak','nie','nierozstrzygniety') COLLATE utf8_polish_ci NOT NULL DEFAULT 'nierozstrzygniety',
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktualizacji` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_decyzje_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_decyzje_konta` (`konto_id`),
  CONSTRAINT `FK_decyzje_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_decyzje_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `decyzje_szczegoly`
--

DROP TABLE IF EXISTS `decyzje_szczegoly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `decyzje_szczegoly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `decyzja_id` int(11) DEFAULT NULL,
  `typ` enum('plus','minus') COLLATE utf8_polish_ci NOT NULL DEFAULT 'plus',
  `opis` varchar(2000) COLLATE utf8_polish_ci DEFAULT NULL,
  `selektor_html` varchar(10) COLLATE utf8_polish_ci DEFAULT NULL,
  `wartosc` int(11) DEFAULT '0',
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktualizacji` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_decyzje_szczegoly_decyzje` (`decyzja_id`),
  KEY `FK_decyzje_szczegoly_konta` (`konto_id`),
  CONSTRAINT `FK_decyzje_szczegoly_decyzje` FOREIGN KEY (`decyzja_id`) REFERENCES `decyzje` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_decyzje_szczegoly_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jezyki`
--

DROP TABLE IF EXISTS `jezyki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jezyki` (
  `kod` char(5) COLLATE utf8_polish_ci NOT NULL,
  `skrot` char(5) COLLATE utf8_polish_ci NOT NULL,
  `nazwa` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktualizacji` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`kod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `konta`
--

DROP TABLE IF EXISTS `konta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `konta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rejestrujacy_id` int(11) DEFAULT NULL,
  `data_dodania` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktualizacji` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_waznosci` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_konto_uzytkownicy` (`rejestrujacy_id`),
  CONSTRAINT `FK_konto_uzytkownicy` FOREIGN KEY (`rejestrujacy_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `kontakt`
--

DROP TABLE IF EXISTS `kontakt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kontakt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `imie` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `tresc` varchar(2000) COLLATE utf8_polish_ci DEFAULT NULL,
  `zgoda_przetwarzanie` char(1) COLLATE utf8_polish_ci DEFAULT '0',
  `zgoda_przetwarzanie_marketingowe` char(1) COLLATE utf8_polish_ci DEFAULT '0',
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktualizacji` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `logi`
--

DROP TABLE IF EXISTS `logi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `kod_kategorii` varchar(10) COLLATE utf8_polish_ci DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `tresc` varchar(5000) COLLATE utf8_polish_ci DEFAULT NULL,
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktualizacji` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_logi_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_logi_logi_kategorie` (`kod_kategorii`),
  KEY `FK_logi_konta` (`konto_id`),
  CONSTRAINT `FK_logi_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_logi_logi_kategorie` FOREIGN KEY (`kod_kategorii`) REFERENCES `logi_kategorie` (`kod`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_logi_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `logi_kategorie`
--

DROP TABLE IF EXISTS `logi_kategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logi_kategorie` (
  `kod` varchar(10) COLLATE utf8_polish_ci NOT NULL,
  `nazwa` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktualizacji` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`kod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `platnosci`
--

DROP TABLE IF EXISTS `platnosci`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `platnosci` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `cennik_id` int(11) DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `status` enum('nowa','zarejestrowana','w_trakcie','zakonczona','blad') COLLATE utf8_polish_ci DEFAULT 'nowa',
  `status_systemowy` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `kwota` decimal(10,2) DEFAULT '0.00',
  `email` text COLLATE utf8_polish_ci,
  `token` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktualizacji` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_platnosci_konta` (`konto_id`),
  KEY `FK_platnosci_cennik` (`cennik_id`),
  KEY `FK_platnosci_uzytkownicy` (`uzytkownik_id`),
  CONSTRAINT `FK_platnosci_cennik` FOREIGN KEY (`cennik_id`) REFERENCES `cennik` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_platnosci_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_platnosci_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(15) COLLATE utf16_polish_ci NOT NULL,
  `kod` varchar(10) COLLATE utf16_polish_ci NOT NULL,
  `typ` enum('pracownik','klient') COLLATE utf16_polish_ci DEFAULT 'klient',
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktualizacji` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uprawnienia`
--

DROP TABLE IF EXISTS `uprawnienia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uprawnienia` (
  `kod` varchar(7) COLLATE utf16_polish_ci NOT NULL,
  `opis` varchar(50) COLLATE utf16_polish_ci DEFAULT NULL,
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktualizacji` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`kod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uprawnienia_kategorie`
--

DROP TABLE IF EXISTS `uprawnienia_kategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uprawnienia_kategorie` (
  `kod` varchar(3) COLLATE utf8_polish_ci NOT NULL,
  `opis` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktualizacji` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`kod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uprawnienia_role`
--

DROP TABLE IF EXISTS `uprawnienia_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uprawnienia_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rola_id` int(11) NOT NULL,
  `uprawnienie_kod` varchar(7) COLLATE utf16_polish_ci NOT NULL,
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktualizacji` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_uprawnienia_role_role` (`rola_id`),
  KEY `FK_uprawnienia_role_uprawnienia` (`uprawnienie_kod`),
  CONSTRAINT `FK_uprawnienia_role_role` FOREIGN KEY (`rola_id`) REFERENCES `role` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_uprawnienia_role_uprawnienia` FOREIGN KEY (`uprawnienie_kod`) REFERENCES `uprawnienia` (`kod`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ustawienia_systemu`
--

DROP TABLE IF EXISTS `ustawienia_systemu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ustawienia_systemu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `kod` varchar(7) COLLATE utf8_polish_ci NOT NULL,
  `wartosc` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktualizacji` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_ustawienia_systemu_uzytkownicy` (`uzytkownik_id`),
  CONSTRAINT `FK_ustawienia_systemu_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uzytkownicy`
--

DROP TABLE IF EXISTS `uzytkownicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uzytkownicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `avatar_id` int(11) DEFAULT NULL,
  `rola_id` int(11) DEFAULT NULL,
  `konto_id` int(11) DEFAULT NULL,
  `jezyk_kod` char(5) COLLATE utf8_polish_ci DEFAULT 'en_US',
  `login` text COLLATE utf8_polish_ci,
  `imie` text COLLATE utf8_polish_ci,
  `nazwisko` text COLLATE utf8_polish_ci,
  `haslo` text COLLATE utf8_polish_ci,
  `mail` text COLLATE utf8_polish_ci,
  `telefon` text COLLATE utf8_polish_ci,
  `telefon2` text COLLATE utf8_polish_ci,
  `typ` enum('pracownik','klient') COLLATE utf8_polish_ci DEFAULT 'klient',
  `deklarowana_firma` char(70) COLLATE utf8_polish_ci DEFAULT NULL,
  `czy_aktywny` char(1) COLLATE utf8_polish_ci DEFAULT '0',
  `usuniety` char(1) COLLATE utf8_polish_ci DEFAULT '0',
  `akceptacja_regulaminu` char(1) COLLATE utf8_polish_ci DEFAULT '0',
  `zgoda_przetwarzanie` char(1) COLLATE utf8_polish_ci DEFAULT '0',
  `zgoda_przetwarzanie_marketingowe` char(1) COLLATE utf8_polish_ci DEFAULT '0',
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktualizacji` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_uzytkownicy_zasoby` (`avatar_id`),
  KEY `FK_uzytkownicy_role` (`rola_id`),
  KEY `FK_uzytkownicy_jezyki` (`jezyk_kod`),
  KEY `FK_uzytkownicy_konto` (`konto_id`),
  CONSTRAINT `FK_uzytkownicy_jezyki` FOREIGN KEY (`jezyk_kod`) REFERENCES `jezyki` (`kod`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_uzytkownicy_konto` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`),
  CONSTRAINT `FK_uzytkownicy_role` FOREIGN KEY (`rola_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FK_uzytkownicy_zasoby` FOREIGN KEY (`avatar_id`) REFERENCES `zasoby` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-12 14:27:19
