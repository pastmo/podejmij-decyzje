ALTER TABLE `uprawnienia`
	ADD COLUMN `kategoria_kod` VARCHAR(3) NULL DEFAULT NULL AFTER `kod`;

ALTER TABLE `uprawnienia_role`
	DROP FOREIGN KEY `FK_uprawnienia_role_uprawnienia`;
ALTER TABLE `uprawnienia_role`
	CHANGE COLUMN `uprawnienie_kod` `uprawnienie_kod` VARCHAR(7) NOT NULL COLLATE 'utf8_polish_ci' AFTER `rola_id`;

ALTER TABLE `wiki_artykuly`
	DROP FOREIGN KEY `FK_wiki_artykuly_uprawnienia`;
ALTER TABLE `wiki_artykuly`
	CHANGE COLUMN `uprawnienie_kod` `uprawnienie_kod` VARCHAR(7) NULL DEFAULT NULL COLLATE 'utf8_polish_ci' AFTER `autor_id`;
	
ALTER TABLE `wiki_artykuly_historia`
	DROP FOREIGN KEY `FK_wiki_artykuly_uprawnieniah`;
ALTER TABLE `wiki_artykuly_historia`
	CHANGE COLUMN `uprawnienie_kod` `uprawnienie_kod` VARCHAR(7) NULL DEFAULT NULL COLLATE 'utf8_polish_ci' AFTER `autor_id`;

	
ALTER TABLE `uprawnienia`
	CHANGE COLUMN `kod` `kod` VARCHAR(7) NOT NULL COLLATE 'utf8_polish_ci' FIRST,
	CHANGE COLUMN `kategoria_kod` `kategoria_kod` VARCHAR(3) NULL DEFAULT NULL COLLATE 'utf8_polish_ci' AFTER `kod`,
	CHANGE COLUMN `opis` `opis` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_polish_ci' AFTER `kategoria_kod`;

ALTER TABLE `uprawnienia`
	ADD CONSTRAINT `FK_uprawnienia_uprawnienia_kategorie` FOREIGN KEY (`kategoria_kod`) REFERENCES `uprawnienia_kategorie` (`kod`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `uprawnienia_role`
	ADD CONSTRAINT `FK_uprawnienia_role_uprawnienia` FOREIGN KEY (`uprawnienie_kod`) REFERENCES `uprawnienia` (`kod`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `wiki_artykuly`
	ADD CONSTRAINT `FK_wiki_artykuly_uprawnienia` FOREIGN KEY (`uprawnienie_kod`) REFERENCES `uprawnienia` (`kod`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `wiki_artykuly_historia`
	ADD CONSTRAINT `FK_wiki_artykuly_uprawnienia2` FOREIGN KEY (`uprawnienie_kod`) REFERENCES `uprawnienia` (`kod`) ON UPDATE CASCADE ON DELETE CASCADE;


UPDATE `uprawnienia` SET `kategoria_kod`='bpd' WHERE  `kod` like 'bpd%';
UPDATE `uprawnienia` SET `kategoria_kod`='auk' WHERE  `kod` like 'auk%';
UPDATE `uprawnienia` SET `kategoria_kod`='cha' WHERE  `kod` like 'cha%';
UPDATE `uprawnienia` SET `kategoria_kod`='kos' WHERE  `kod` like 'kos%';
UPDATE `uprawnienia` SET `kategoria_kod`='mag' WHERE  `kod` like 'mag%';
UPDATE `uprawnienia` SET `kategoria_kod`='men' WHERE  `kod` like 'men%';
UPDATE `uprawnienia` SET `kategoria_kod`='prj' WHERE  `kod` like 'prj%';
UPDATE `uprawnienia` SET `kategoria_kod`='pro' WHERE  `kod` like 'pro%';
UPDATE `uprawnienia` SET `kategoria_kod`='pty' WHERE  `kod` like 'pty%';
UPDATE `uprawnienia` SET `kategoria_kod`='rol' WHERE  `kod` like 'rol%';
UPDATE `uprawnienia` SET `kategoria_kod`='skl' WHERE  `kod` like 'skl%';
UPDATE `uprawnienia` SET `kategoria_kod`='sta' WHERE  `kod` like 'sta%';
UPDATE `uprawnienia` SET `kategoria_kod`='ust' WHERE  `kod` like 'ust%';
UPDATE `uprawnienia` SET `kategoria_kod`='uzy' WHERE  `kod` like 'uzy%';
UPDATE `uprawnienia` SET `kategoria_kod`='wys' WHERE  `kod` like 'wys%';
UPDATE `uprawnienia` SET `kategoria_kod`='zam' WHERE  `kod` like 'zam%';

INSERT INTO `pastmo_ggerp`.`uprawnienia` (`kod`, `kategoria_kod`) VALUES ('pro_cen', 'pro');
UPDATE `pastmo_ggerp`.`uprawnienia` SET `opis`='Widoczność cen produktów' WHERE  `kod`='pro_cen';
