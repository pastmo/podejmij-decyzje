CREATE TABLE `cennik` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`kwota` DECIMAL(10,2) NULL DEFAULT NULL,
	`opis` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_polish_ci',
	`dni_przedluzenia` INT(11) NOT NULL DEFAULT '0',
	`data_dodania` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`data_aktualizacji` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_polish_ci'
ENGINE=InnoDB
;


CREATE TABLE `platnosci` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`konto_id` INT(11) NULL DEFAULT NULL,
	`cennik_id` INT(11) NULL DEFAULT NULL,
	`uzytkownik_id` INT(11) NULL DEFAULT NULL,
	`status` ENUM('nowa','zarejestrowana','w_trakcie','zakonczona','blad') NULL DEFAULT 'nowa' COLLATE 'utf8_polish_ci',
	`status_systemowy` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_polish_ci',
	`kwota` DECIMAL(10,2) NULL DEFAULT '0.00',
	`email` TEXT NULL COLLATE 'utf8_polish_ci',
	`token` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_polish_ci',
	`data_dodania` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`data_aktualizacji` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	INDEX `FK_platnosci_konta` (`konto_id`),
	INDEX `FK_platnosci_cennik` (`cennik_id`),
	INDEX `FK_platnosci_uzytkownicy` (`uzytkownik_id`),
	CONSTRAINT `FK_platnosci_cennik` FOREIGN KEY (`cennik_id`) REFERENCES `cennik` (`id`) ON DELETE SET NULL,
	CONSTRAINT `FK_platnosci_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE SET NULL,
	CONSTRAINT `FK_platnosci_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE SET NULL
)
COLLATE='utf8_polish_ci'
ENGINE=InnoDB
;


ALTER TABLE `logi`
	CHANGE COLUMN `tresc` `tresc` VARCHAR(5000) NULL DEFAULT NULL COLLATE 'utf8_polish_ci' AFTER `uzytkownik_id`;
INSERT INTO `cennik` (`kwota`, `opis`, `dni_przedluzenia`) VALUES (5, '5 zł/ rok', 365);

INSERT INTO `logi_kategorie` (`kod`, `nazwa`) VALUES ('platnosci', 'Logi płatności');

ALTER TABLE `platnosci`
	ADD COLUMN `order_id` INT NULL DEFAULT NULL AFTER `token`;

-- wgrane na tekst