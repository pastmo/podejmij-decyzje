CREATE TABLE `decyzje` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`uzytkownik_id` INT(11) NULL DEFAULT NULL,
	`nazwa` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_polish_ci',
	`data_dodania` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`data_aktualizacji` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	INDEX `FK_decyzje_uzytkownicy` (`uzytkownik_id`),
	CONSTRAINT `FK_decyzje_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE CASCADE
)
COLLATE='utf8_polish_ci'
ENGINE=InnoDB
;

CREATE TABLE `decyzje_szczegoly` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`decyzja_id` INT(11) NULL DEFAULT NULL,
	`opis` VARCHAR(2000) NULL DEFAULT NULL COLLATE 'utf8_polish_ci',
	`wartosc` INT(11) NULL DEFAULT '0',
	`data_dodania` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`data_aktualizacji` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	INDEX `FK_decyzje_szczegoly_decyzje` (`decyzja_id`),
	CONSTRAINT `FK_decyzje_szczegoly_decyzje` FOREIGN KEY (`decyzja_id`) REFERENCES `decyzje` (`id`) ON DELETE CASCADE
)
COLLATE='utf8_polish_ci'
ENGINE=InnoDB
;

ALTER TABLE `decyzje_szczegoly`
	ADD COLUMN `typ` ENUM('plus','minus') NOT NULL DEFAULT 'plus' AFTER `decyzja_id`;

ALTER TABLE `decyzje`
	ADD COLUMN `wynik` ENUM('tak','nie','nierozstrzygniety') NOT NULL DEFAULT 'nierozstrzygniety' AFTER `nazwa`;


ALTER TABLE `decyzje`
	ADD COLUMN `konto_id` INT(11) NULL DEFAULT NULL AFTER `id`;
ALTER TABLE `decyzje`
	ADD CONSTRAINT `FK_decyzje_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE;

ALTER TABLE `decyzje_szczegoly`
	ADD COLUMN `konto_id` INT(11) NULL DEFAULT NULL AFTER `id`;
	ALTER TABLE `decyzje_szczegoly`
	ADD CONSTRAINT `FK_decyzje_szczegoly_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE;

ALTER TABLE `decyzje_szczegoly`
	ADD COLUMN `selektor_html` VARCHAR(10) NULL DEFAULT NULL AFTER `opis`;
