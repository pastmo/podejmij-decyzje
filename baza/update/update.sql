-- wgrać bazę płatności!

ALTER TABLE `uzytkownicy`
	ADD COLUMN `akceptacja_regulaminu` CHAR(1) NULL DEFAULT '0' AFTER `usuniety`,
	ADD COLUMN `zgoda_przetwarzanie` CHAR(1) NULL DEFAULT '0' AFTER `akceptacja_regulaminu`,
	ADD COLUMN `zgoda_przetwarzanie_marketingowe` CHAR(1) NULL DEFAULT '0' AFTER `zgoda_przetwarzanie`;

	


ALTER TABLE `kontakt`
	ADD COLUMN `zgoda_przetwarzanie` CHAR(1) NULL DEFAULT '0' AFTER `tresc`,
	ADD COLUMN `zgoda_przetwarzanie_marketingowe` CHAR(1) NULL DEFAULT '0' AFTER `zgoda_przetwarzanie`,
	ADD COLUMN `data_dodania` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP AFTER `zgoda_przetwarzanie_marketingowe`,	
	ADD COLUMN `data_aktualizacji` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `data_dodania`;
	
--- wgrane na test i prod