-- MySQL dump 10.16  Distrib 10.1.13-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: decyzje
-- ------------------------------------------------------
-- Server version	10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `cennik`
--

LOCK TABLES `cennik` WRITE;
/*!40000 ALTER TABLE `cennik` DISABLE KEYS */;
INSERT INTO `cennik` VALUES (1,5.00,'5 zł/ rok',365,'2018-03-03 13:46:22','2018-03-03 13:46:26');
/*!40000 ALTER TABLE `cennik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `dane_do_faktury`
--

LOCK TABLES `dane_do_faktury` WRITE;
/*!40000 ALTER TABLE `dane_do_faktury` DISABLE KEYS */;
INSERT INTO `dane_do_faktury` VALUES (1,NULL,1,'123',NULL,'2017-12-21 11:32:45','2017-12-21 11:32:45');
/*!40000 ALTER TABLE `dane_do_faktury` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `decyzje`
--

LOCK TABLES `decyzje` WRITE;
/*!40000 ALTER TABLE `decyzje` DISABLE KEYS */;
/*!40000 ALTER TABLE `decyzje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `decyzje_szczegoly`
--

LOCK TABLES `decyzje_szczegoly` WRITE;
/*!40000 ALTER TABLE `decyzje_szczegoly` DISABLE KEYS */;
/*!40000 ALTER TABLE `decyzje_szczegoly` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `jezyki`
--

LOCK TABLES `jezyki` WRITE;
/*!40000 ALTER TABLE `jezyki` DISABLE KEYS */;
INSERT INTO `jezyki` VALUES ('en_US','en','English','2017-12-21 11:32:47','2017-12-21 11:32:47'),('pl_PL','pl','Polski','2017-12-21 11:32:47','2017-12-21 11:32:47');
/*!40000 ALTER TABLE `jezyki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `konta`
--

LOCK TABLES `konta` WRITE;
/*!40000 ALTER TABLE `konta` DISABLE KEYS */;
INSERT INTO `konta` VALUES (1,1,'2017-04-17 09:39:23','2018-05-07 22:12:11','2018-12-16 15:35:59');
/*!40000 ALTER TABLE `konta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `kontakt`
--

LOCK TABLES `kontakt` WRITE;
/*!40000 ALTER TABLE `kontakt` DISABLE KEYS */;
/*!40000 ALTER TABLE `kontakt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `logi`
--

LOCK TABLES `logi` WRITE;
/*!40000 ALTER TABLE `logi` DISABLE KEYS */;
INSERT INTO `logi` VALUES (219,1,'error',1,'{\"msg\":\"Prosimy o op\\u0142acenie konta\",\"stack\":\"#0 C:\\\\xampp_7_0_6\\\\htdocs\\\\podejmij_decyzje\\\\module\\\\Decyzje\\\\src\\\\Menager\\\\DecyzjeMenager.php(83): Decyzje\\\\Menager\\\\DecyzjeMenager->sprawdzWaznoscKonta()\\n#1 C:\\\\xampp_7_0_6\\\\htdocs\\\\podejmij_decyzje\\\\module\\\\Decyzje\\\\src\\\\Menager\\\\DecyzjeMenager.php(54): Decyzje\\\\Menager\\\\DecyzjeMenager->zapiszDecyzjeTry(Object(Zend\\\\Stdlib\\\\Parameters))\\n#2 C:\\\\xampp_7_0_6\\\\htdocs\\\\podejmij_decyzje\\\\module\\\\Decyzje\\\\src\\\\Fasada\\\\DecyzjeFasada.php(20): Decyzje\\\\Menager\\\\DecyzjeMenager->zapiszDecyzje(Object(Zend\\\\Stdlib\\\\Parameters))\\n#3 C:\\\\xampp_7_0_6\\\\htdocs\\\\podejmij_decyzje\\\\module\\\\Decyzje\\\\src\\\\Controller\\\\DecyzjeAjaxController.php(16): Decyzje\\\\Fasada\\\\DecyzjeFasada->zapiszDecyzje(Object(Zend\\\\Stdlib\\\\Parameters))\\n#4 C:\\\\xampp_7_0_6\\\\htdocs\\\\podejmij_decyzje\\\\vendor\\\\zendframework\\\\zend-mvc\\\\src\\\\Controller\\\\AbstractActionController.php(78): Decyzje\\\\Controller\\\\DecyzjeAjaxController->zapiszDecyzjeAction()\\n#5 C:\\\\xampp_7_0_6\\\\htdocs\\\\podejmij_decyzje\\\\module\\\\Pastmo\\\\src\\\\Controller\\\\WspolnyController.php(47): Zend\\\\Mvc\\\\Controller\\\\AbstractActionController->onDispatch(Object(Zend\\\\Mvc\\\\MvcEvent))\\n#6 C:\\\\xampp_7_0_6\\\\htdocs\\\\podejmij_decyzje\\\\vendor\\\\zendframework\\\\zend-eventmanager\\\\src\\\\EventManager.php(322): Pastmo\\\\Controller\\\\WspolnyController->onDispatch(Object(Zend\\\\Mvc\\\\MvcEvent))\\n#7 C:\\\\xampp_7_0_6\\\\htdocs\\\\podejmij_decyzje\\\\vendor\\\\zendframework\\\\zend-eventmanager\\\\src\\\\EventManager.php(179): Zend\\\\EventManager\\\\EventManager->triggerListeners(Object(Zend\\\\Mvc\\\\MvcEvent), Object(Closure))\\n#8 C:\\\\xampp_7_0_6\\\\htdocs\\\\podejmij_decyzje\\\\vendor\\\\zendframework\\\\zend-mvc\\\\src\\\\Controller\\\\AbstractController.php(106): Zend\\\\EventManager\\\\EventManager->triggerEventUntil(Object(Closure), Object(Zend\\\\Mvc\\\\MvcEvent))\\n#9 C:\\\\xampp_7_0_6\\\\htdocs\\\\podejmij_decyzje\\\\vendor\\\\zendframework\\\\zend-mvc\\\\src\\\\DispatchListener.php(138): Zend\\\\Mvc\\\\Controller\\\\AbstractController->dispatch(Object(Zend\\\\Http\\\\PhpEnvironment\\\\Request), Object(Zend\\\\Http\\\\PhpEnvironment\\\\Response))\\n#10 C:\\\\xampp_7_0_6\\\\htdocs\\\\podejmij_decyzje\\\\vendor\\\\zendframework\\\\zend-eventmanager\\\\src\\\\EventManager.php(322): Zend\\\\Mvc\\\\DispatchListener->onDispatch(Object(Zend\\\\Mvc\\\\MvcEvent))\\n#11 C:\\\\xampp_7_0_6\\\\htdocs\\\\podejmij_decyzje\\\\vendor\\\\zendframework\\\\zend-eventmanager\\\\src\\\\EventManager.php(179): Zend\\\\EventManager\\\\EventManager->triggerListeners(Object(Zend\\\\Mvc\\\\MvcEvent), Object(Closure))\\n#12 C:\\\\xampp_7_0_6\\\\htdocs\\\\podejmij_decyzje\\\\vendor\\\\zendframework\\\\zend-mvc\\\\src\\\\Application.php(332): Zend\\\\EventManager\\\\EventManager->triggerEventUntil(Object(Closure), Object(Zend\\\\Mvc\\\\MvcEvent))\\n#13 C:\\\\xampp_7_0_6\\\\htdocs\\\\podejmij_decyzje\\\\public_html\\\\index.php(31): Zend\\\\Mvc\\\\Application->run()\\n#14 {main}\"}','2018-05-07 22:11:29','2018-05-07 22:11:29');
/*!40000 ALTER TABLE `logi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `logi_kategorie`
--

LOCK TABLES `logi_kategorie` WRITE;
/*!40000 ALTER TABLE `logi_kategorie` DISABLE KEYS */;
INSERT INTO `logi_kategorie` VALUES ('baza_produ','Edycja stanów magazynowych','2017-12-21 11:32:49','2017-12-21 11:32:49'),('email_err','Błąd emaili','2017-12-21 11:32:49','2017-12-21 11:32:49'),('error','Błąd','2018-02-16 19:05:52','2018-02-16 19:05:52'),('pastmo_pan','Błędy poczty email','2017-12-21 11:32:49','2017-12-21 11:32:49'),('platnosci','Logi płatności','2018-03-04 11:08:56','2018-03-04 11:08:56'),('sta_mag','Wartość rynkowa magazynu','2017-12-21 11:32:49','2017-12-21 11:32:49');
/*!40000 ALTER TABLE `logi_kategorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `platnosci`
--

LOCK TABLES `platnosci` WRITE;
/*!40000 ALTER TABLE `platnosci` DISABLE KEYS */;
/*!40000 ALTER TABLE `platnosci` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Admin','admin','pracownik','2017-12-21 11:32:50','2017-12-21 11:32:50'),(2,'Klient','klient','klient','2017-12-21 11:32:50','2017-12-21 11:32:50'),(3,'Grafik','grafik','pracownik','2017-12-21 11:32:50','2017-12-21 11:32:50'),(4,'Grafik główny','grafik_gl','pracownik','2017-12-21 11:32:50','2017-12-21 11:32:50'),(5,'Sprzedawca','sprzedawca','pracownik','2017-12-21 11:32:50','2017-12-21 11:32:50');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `uprawnienia`
--

LOCK TABLES `uprawnienia` WRITE;
/*!40000 ALTER TABLE `uprawnienia` DISABLE KEYS */;
INSERT INTO `uprawnienia` VALUES ('auk_pup','Przypisanie innych userów','2017-12-21 11:32:51','2017-12-21 11:32:51'),('auk_wdo','Dodawania ofert','2017-12-21 11:32:51','2017-12-21 11:32:51'),('auk_wep','Edycja pytań','2017-12-21 11:32:51','2017-12-21 11:32:51'),('auk_wop','Odpowiedzi na pytania','2017-12-21 11:32:51','2017-12-21 11:32:51'),('auk_wpl','Dodawanie plików do wyceny','2017-12-21 11:32:51','2017-12-21 11:32:51'),('auk_wpy','Możliwość zadawania pytań','2017-12-21 11:32:51','2017-12-21 11:32:51'),('auk_wwi','Widoczność użytkowników w wycenach','2017-12-21 11:32:51','2017-12-21 11:32:51'),('auk_wwo','Wybór oferty','2017-12-21 11:32:51','2017-12-21 11:32:51'),('auk_wza','Dodawanie zapytań o wycenę- projekt kafelka','2017-12-21 11:32:51','2017-12-21 11:32:51'),('auk_zpa','Pliki projektu akceptacja','2017-12-21 11:32:51','2017-12-21 11:32:51'),('auk_zpd','Pliki projektu dodawanie','2017-12-21 11:32:51','2017-12-21 11:32:51'),('auk_zpk','Pliki projektu komentarz','2017-12-21 11:32:51','2017-12-21 11:32:51'),('auk_zsa','Szczegóły zamówienia akceptacja','2017-12-21 11:32:51','2017-12-21 11:32:51'),('auk_zsd','Szczegóły zamówienia dodawanie','2017-12-21 11:32:51','2017-12-21 11:32:51'),('auk_zse','Szczegóły zamówienia edycja- aukcje','2017-12-21 11:32:51','2017-12-21 11:32:51'),('auk_zwz','Wiadomości i załączniki','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kce','Cennik','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kip','Informacje podstawowe','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kka','Kategorie','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kko','Konkurencja','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kkp','Komponenty dodatkowe','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kma','Magazyn','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kod','Odpady','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kpl','Pliki','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kpo','Podstawki','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kpr','Karta produktu','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kpu','Pudełka','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kre','Rezerwacje','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kta','Tagi','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kwa','Wariacje','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kwp','Wyceny producentów','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kwy','Komponenty wymienne','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kzd','Zdjęcia','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_kzn','Zniżki','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd_m3d','Moduł 3D','2017-12-21 11:32:51','2017-12-21 11:32:51'),('cha_all','Komunikacja z dowolnymi pracownikami Glasso','2017-12-21 11:32:51','2017-12-21 11:32:51'),('cha_wys','Wyświetlanie chatu','2017-12-21 11:32:51','2017-12-21 11:32:51'),('kos_ekp','Edycja koszyka produktów','2017-12-21 11:32:51','2017-12-21 11:32:51'),('kos_tpk','Tworzenie projektu z koszyka','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_auk','Menu Moduł aukcyjny','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_awy','Menu Moduł aukcyjny wyceny','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_aza','Menu Moduł aukcyjny zamówienia','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_azk','Menu Moduł aukcyjny zakończone','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_bka','Menu Baza produktów kategorie','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_bpd','Menu Baza produktów','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_bpr','Menu Baza produktów produkty','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_bus','Menu Baza produktów ustawienia','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_bzm','Menu Baza produktów zmiany magazynowe','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_bzn','Menu Baza produktów zniżki','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_das','Menu dashboard','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_edi','Menu Magento EDI','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_ema','Menu Email','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_fir','Menu Firmy','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_kal','Menu Kalendarz','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_kli','Menu Koszyk klienta','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_kos','Menu Koszyk','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_ksp','Menu Koszyk dostaw','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_mag','Menu Magazyn','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_mdo','Menu Magazyn dostawa','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_mpl','Menu Manager plików','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_pdy','Menu Produkty','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_prd','Menu Produkcja','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_pro','Menu Projekty','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_rol','Menu Role','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_skl','Menu Statystyki klientów','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_sog','Menu Statystyki ogólne','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_spr','Menu Statystyki sprzedawców','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_sta','Menu Statystyki','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_swm','Menu Statystyki wartość magazynu','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_urz','Menu Urządzenia','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_uzp','Menu Użytkownicy podmenu','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_uzy','Menu Użytkownicy','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_wia','Menu Wiadomości','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_wik','Menu Wiki','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_wiz','Menu Wizualizacje','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_wka','Menu Wysyłka książka adresowa','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_wmp','Menu Wysyłka moje przesyłki','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_wnp','Menu Wysyłka nowa przesyłka','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_wpn','Menu Wysyłka potwierdzenie nadania','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_wpr','Menu Wysyłka projekty','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men_wys','Menu Wysyłka','2017-12-21 11:32:51','2017-12-21 11:32:51'),('prj_fak','Szczegóły firmy akceptacja','2017-12-21 11:32:51','2017-12-21 11:32:51'),('prj_fde','Szczegoly firmy dodawanie edycja','2017-12-21 11:32:51','2017-12-21 11:32:51'),('prj_ind','Dodawanie indywizualizacji','2017-12-21 11:32:51','2017-12-21 11:32:51'),('prj_kow','Kończenie wizualizacji','2017-12-21 11:32:51','2017-12-21 11:32:51'),('prj_kwi','Komentarz do wizualizacji','2017-12-21 11:32:51','2017-12-21 11:32:51'),('prj_sak','Akcjeptacja szczegółów zamówienia','2017-12-21 11:32:51','2017-12-21 11:32:51'),('prj_sdo','Dodawanie szczegółów zamówienia','2017-12-21 11:32:51','2017-12-21 11:32:51'),('prj_sed','Szczegóły zamówienia edycja- projekty','2017-12-21 11:32:51','2017-12-21 11:32:51'),('prj_sta','Edycja statusów','2017-12-21 11:32:51','2017-12-21 11:32:51'),('prj_tap','Tagi projektu','2017-12-21 11:32:51','2017-12-21 11:32:51'),('prj_taw','Tagi wiadomości','2017-12-21 11:32:51','2017-12-21 11:32:51'),('prj_udo','Użytkownicy w projekcie przypisywanie','2017-12-21 11:32:51','2017-12-21 11:32:51'),('prj_wak','Akcjaptacja wizualizacji i indywidualizacji','2017-12-21 11:32:51','2017-12-21 11:32:51'),('prj_wia','Wiadomości w projektach','2017-12-21 11:32:51','2017-12-21 11:32:51'),('prj_wid','Dodawanie wizualizacji','2017-12-21 11:32:51','2017-12-21 11:32:51'),('pro_ace','Akceptacja cen w projekcie','2017-12-21 11:32:51','2017-12-21 11:32:51'),('pro_ain','Akceptowanie indywidualizacji','2017-12-21 11:32:51','2017-12-21 11:32:51'),('pro_app','Aktualizacja produktów w projekcie','2017-12-21 11:32:51','2017-12-21 11:32:51'),('pro_awi','Akceptowanie wizualizacji','2017-12-21 11:32:51','2017-12-21 11:32:51'),('pro_etp','Edycja transportu produktów','2017-12-21 11:32:51','2017-12-21 11:32:51'),('pro_eup','Edycja użytkowników w projekcie','2017-12-21 11:32:51','2017-12-21 11:32:51'),('pro_ppr','Przeglądanie projektów','2017-12-21 11:32:51','2017-12-21 11:32:51'),('pro_wce','Widoczność cen przed akceptacją','2017-12-21 11:32:51','2017-12-21 11:32:51'),('pty_dig','Dostęp do projektów innych grafików','2017-12-21 11:32:51','2017-12-21 11:32:51'),('pty_pgb','Przekazanie projektu innemy grafikowi bez zgody','2017-12-21 11:32:51','2017-12-21 11:32:51'),('pty_pgz','Przekazanie projektu innemy grafikowi','2017-12-21 11:32:51','2017-12-21 11:32:51'),('pty_psb','Przekazywanie projektu innemu sprzedawcy bez zgody','2017-12-21 11:32:51','2017-12-21 11:32:51'),('pty_psz','Przekazywanie projektu innemu sprzedawcy za zgodą','2017-12-21 11:32:51','2017-12-21 11:32:51'),('rol_adm','Uprawnienia administratora','2017-12-21 11:32:51','2017-12-21 11:32:51'),('rol_def','Domyślne uprawnienie','2017-12-21 11:32:51','2017-12-21 11:32:51'),('rol_eup','Edycja uprawnień','2017-12-21 11:32:51','2017-12-21 11:32:51'),('rol_ggr','Uprawnienia głównego grafika','2017-12-21 11:32:51','2017-12-21 11:32:51'),('rol_gra','Uprawnienia grafika','2017-12-21 11:32:51','2017-12-21 11:32:51'),('rol_kli','Uprawnienia klienta','2017-12-21 11:32:51','2017-12-21 11:32:51'),('rol_pra','Uprawnienia pracownika','2017-12-21 11:32:51','2017-12-21 11:32:51'),('rol_spr','Uprawnienia sprzedawcy','2017-12-21 11:32:51','2017-12-21 11:32:51'),('rol_tup','Tworzenie nowych uprawnień','2017-12-21 11:32:51','2017-12-21 11:32:51'),('skl_psl','Przeglądanie sklepu','2017-12-21 11:32:51','2017-12-21 11:32:51'),('sta_kli','Przeglądanie statystyk klientów','2017-12-21 11:32:51','2017-12-21 11:32:51'),('sta_ogo','Przeglądanie statystyk ogólnych','2017-12-21 11:32:51','2017-12-21 11:32:51'),('sta_psg','Przeglądanie statystyk grafików','2017-12-21 11:32:51','2017-12-21 11:32:51'),('sta_psp','Przeglądanie statystyk produków','2017-12-21 11:32:51','2017-12-21 11:32:51'),('sta_pss','Przeglądanie statystyk sprzedawców','2017-12-21 11:32:51','2017-12-21 11:32:51'),('ust_pus','Przeglądanie ustawień ERP','2017-12-21 11:32:51','2017-12-21 11:32:51'),('uzy_euz','Edycja użytkowników','2017-12-21 11:32:51','2017-12-21 11:32:51'),('uzy_ppu','Przeglądanie profilii użytkowników','2017-12-21 11:32:51','2017-12-21 11:32:51'),('uzy_tuz','Tworzenie nowych użytkowników','2017-12-21 11:32:51','2017-12-21 11:32:51'),('zam_pwy','Przeglądanie wycen','2017-12-21 11:32:51','2017-12-21 11:32:51');
/*!40000 ALTER TABLE `uprawnienia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `uprawnienia_kategorie`
--

LOCK TABLES `uprawnienia_kategorie` WRITE;
/*!40000 ALTER TABLE `uprawnienia_kategorie` DISABLE KEYS */;
INSERT INTO `uprawnienia_kategorie` VALUES ('auk','Moduł aukcyjny','2017-12-21 11:32:51','2017-12-21 11:32:51'),('bpd','Baza produktów','2017-12-21 11:32:51','2017-12-21 11:32:51'),('cha','Chat','2017-12-21 11:32:51','2017-12-21 11:32:51'),('kos','Koszyk','2017-12-21 11:32:51','2017-12-21 11:32:51'),('men','Menu','2017-12-21 11:32:51','2017-12-21 11:32:51'),('prj','Projekt','2017-12-21 11:32:51','2017-12-21 11:32:51'),('pro','Produkty','2017-12-21 11:32:51','2017-12-21 11:32:51'),('pty','Projekty','2017-12-21 11:32:51','2017-12-21 11:32:51'),('rol','Uprawnienia','2017-12-21 11:32:51','2017-12-21 11:32:51'),('skl','Sklep','2017-12-21 11:32:51','2017-12-21 11:32:51'),('sta','Statystyki','2017-12-21 11:32:51','2017-12-21 11:32:51'),('ust','Ustawienia ERP','2017-12-21 11:32:51','2017-12-21 11:32:51'),('uzy','Użytkownicy','2017-12-21 11:32:51','2017-12-21 11:32:51'),('wys','Wysyłka','2017-12-21 11:32:51','2017-12-21 11:32:51'),('zam','Moduł zamówień','2017-12-21 11:32:51','2017-12-21 11:32:51');
/*!40000 ALTER TABLE `uprawnienia_kategorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `uprawnienia_role`
--

LOCK TABLES `uprawnienia_role` WRITE;
/*!40000 ALTER TABLE `uprawnienia_role` DISABLE KEYS */;
INSERT INTO `uprawnienia_role` VALUES (16,2,'rol_def','2017-12-21 11:32:52','2017-12-21 11:32:52'),(20,2,'rol_kli','2017-12-21 11:32:52','2017-12-21 11:32:52'),(144,3,'rol_def','2017-12-21 11:32:52','2017-12-21 11:32:52'),(145,3,'rol_gra','2017-12-21 11:32:52','2017-12-21 11:32:52'),(146,4,'rol_def','2017-12-21 11:32:52','2017-12-21 11:32:52'),(147,4,'rol_ggr','2017-12-21 11:32:52','2017-12-21 11:32:52'),(148,4,'rol_pra','2017-12-21 11:32:52','2017-12-21 11:32:52'),(149,5,'rol_def','2017-12-21 11:32:52','2017-12-21 11:32:52'),(150,5,'rol_pra','2017-12-21 11:32:52','2017-12-21 11:32:52'),(151,5,'rol_spr','2017-12-21 11:32:52','2017-12-21 11:32:52'),(240,1,'auk_wdo','2017-12-21 11:32:52','2017-12-21 11:32:52'),(241,1,'auk_wpl','2017-12-21 11:32:52','2017-12-21 11:32:52'),(242,1,'auk_wza','2017-12-21 11:32:52','2017-12-21 11:32:52'),(243,1,'auk_wep','2017-12-21 11:32:52','2017-12-21 11:32:52'),(244,1,'auk_wpy','2017-12-21 11:32:52','2017-12-21 11:32:52'),(245,1,'auk_wop','2017-12-21 11:32:52','2017-12-21 11:32:52'),(246,1,'auk_zpa','2017-12-21 11:32:52','2017-12-21 11:32:52'),(247,1,'auk_zpd','2017-12-21 11:32:52','2017-12-21 11:32:52'),(248,1,'auk_zpk','2017-12-21 11:32:52','2017-12-21 11:32:52'),(249,1,'auk_pup','2017-12-21 11:32:52','2017-12-21 11:32:52'),(250,1,'auk_zsa','2017-12-21 11:32:52','2017-12-21 11:32:52'),(251,1,'auk_zsd','2017-12-21 11:32:52','2017-12-21 11:32:52'),(252,1,'auk_zse','2017-12-21 11:32:52','2017-12-21 11:32:52'),(253,1,'auk_zwz','2017-12-21 11:32:52','2017-12-21 11:32:52'),(254,1,'auk_wwi','2017-12-21 11:32:52','2017-12-21 11:32:52'),(255,1,'auk_wwo','2017-12-21 11:32:52','2017-12-21 11:32:52'),(256,1,'bpd_kce','2017-12-21 11:32:52','2017-12-21 11:32:52'),(257,1,'bpd_kip','2017-12-21 11:32:52','2017-12-21 11:32:52'),(258,1,'bpd_kpr','2017-12-21 11:32:52','2017-12-21 11:32:52'),(259,1,'bpd_kka','2017-12-21 11:32:52','2017-12-21 11:32:52'),(260,1,'bpd_kkp','2017-12-21 11:32:52','2017-12-21 11:32:52'),(261,1,'bpd_kwy','2017-12-21 11:32:52','2017-12-21 11:32:52'),(262,1,'bpd_kko','2017-12-21 11:32:52','2017-12-21 11:32:52'),(263,1,'bpd_kma','2017-12-21 11:32:52','2017-12-21 11:32:52'),(264,1,'bpd_m3d','2017-12-21 11:32:52','2017-12-21 11:32:52'),(265,1,'bpd_kod','2017-12-21 11:32:52','2017-12-21 11:32:52'),(266,1,'bpd_kpl','2017-12-21 11:32:52','2017-12-21 11:32:52'),(267,1,'bpd_kpo','2017-12-21 11:32:52','2017-12-21 11:32:52'),(268,1,'bpd_kpu','2017-12-21 11:32:52','2017-12-21 11:32:52'),(269,1,'bpd_kre','2017-12-21 11:32:52','2017-12-21 11:32:52'),(270,1,'bpd_kta','2017-12-21 11:32:52','2017-12-21 11:32:52'),(271,1,'bpd_kwa','2017-12-21 11:32:52','2017-12-21 11:32:52'),(272,1,'bpd_kwp','2017-12-21 11:32:52','2017-12-21 11:32:52'),(273,1,'bpd_kzd','2017-12-21 11:32:52','2017-12-21 11:32:52'),(274,1,'bpd_kzn','2017-12-21 11:32:52','2017-12-21 11:32:52'),(275,1,'cha_all','2017-12-21 11:32:52','2017-12-21 11:32:52'),(276,1,'cha_wys','2017-12-21 11:32:52','2017-12-21 11:32:52'),(277,1,'kos_ekp','2017-12-21 11:32:52','2017-12-21 11:32:52'),(278,1,'kos_tpk','2017-12-21 11:32:52','2017-12-21 11:32:52'),(279,1,'men_bpd','2017-12-21 11:32:52','2017-12-21 11:32:52'),(280,1,'men_bka','2017-12-21 11:32:52','2017-12-21 11:32:52'),(281,1,'men_bpr','2017-12-21 11:32:52','2017-12-21 11:32:52'),(282,1,'men_bus','2017-12-21 11:32:52','2017-12-21 11:32:52'),(283,1,'men_bzm','2017-12-21 11:32:52','2017-12-21 11:32:52'),(284,1,'men_bzn','2017-12-21 11:32:52','2017-12-21 11:32:52'),(285,1,'men_das','2017-12-21 11:32:52','2017-12-21 11:32:52'),(286,1,'men_ema','2017-12-21 11:32:52','2017-12-21 11:32:52'),(287,1,'men_fir','2017-12-21 11:32:52','2017-12-21 11:32:52'),(288,1,'men_kal','2017-12-21 11:32:52','2017-12-21 11:32:52'),(289,1,'men_kos','2017-12-21 11:32:52','2017-12-21 11:32:52'),(290,1,'men_ksp','2017-12-21 11:32:52','2017-12-21 11:32:52'),(291,1,'men_kli','2017-12-21 11:32:52','2017-12-21 11:32:52'),(292,1,'men_mag','2017-12-21 11:32:52','2017-12-21 11:32:52'),(293,1,'men_mdo','2017-12-21 11:32:52','2017-12-21 11:32:52'),(294,1,'men_edi','2017-12-21 11:32:52','2017-12-21 11:32:52'),(295,1,'men_mpl','2017-12-21 11:32:52','2017-12-21 11:32:52'),(296,1,'men_auk','2017-12-21 11:32:52','2017-12-21 11:32:52'),(297,1,'men_awy','2017-12-21 11:32:52','2017-12-21 11:32:52'),(298,1,'men_azk','2017-12-21 11:32:52','2017-12-21 11:32:52'),(299,1,'men_aza','2017-12-21 11:32:52','2017-12-21 11:32:52'),(300,1,'men_prd','2017-12-21 11:32:52','2017-12-21 11:32:52'),(301,1,'men_pdy','2017-12-21 11:32:52','2017-12-21 11:32:52'),(302,1,'men_pro','2017-12-21 11:32:52','2017-12-21 11:32:52'),(303,1,'men_rol','2017-12-21 11:32:52','2017-12-21 11:32:52'),(304,1,'men_sta','2017-12-21 11:32:52','2017-12-21 11:32:52'),(305,1,'men_skl','2017-12-21 11:32:52','2017-12-21 11:32:52'),(306,1,'men_sog','2017-12-21 11:32:52','2017-12-21 11:32:52'),(307,1,'men_spr','2017-12-21 11:32:52','2017-12-21 11:32:52'),(308,1,'men_swm','2017-12-21 11:32:52','2017-12-21 11:32:52'),(309,1,'men_urz','2017-12-21 11:32:52','2017-12-21 11:32:52'),(310,1,'men_uzy','2017-12-21 11:32:52','2017-12-21 11:32:52'),(311,1,'men_uzp','2017-12-21 11:32:52','2017-12-21 11:32:52'),(312,1,'men_wia','2017-12-21 11:32:52','2017-12-21 11:32:52'),(313,1,'men_wik','2017-12-21 11:32:52','2017-12-21 11:32:52'),(314,1,'men_wiz','2017-12-21 11:32:52','2017-12-21 11:32:52'),(315,1,'men_wys','2017-12-21 11:32:52','2017-12-21 11:32:52'),(316,1,'men_wka','2017-12-21 11:32:52','2017-12-21 11:32:52'),(317,1,'men_wmp','2017-12-21 11:32:52','2017-12-21 11:32:52'),(318,1,'men_wnp','2017-12-21 11:32:52','2017-12-21 11:32:52'),(319,1,'men_wpn','2017-12-21 11:32:52','2017-12-21 11:32:52'),(320,1,'men_wpr','2017-12-21 11:32:52','2017-12-21 11:32:52'),(321,1,'prj_wak','2017-12-21 11:32:52','2017-12-21 11:32:52'),(322,1,'prj_sak','2017-12-21 11:32:52','2017-12-21 11:32:52'),(323,1,'prj_ind','2017-12-21 11:32:52','2017-12-21 11:32:52'),(324,1,'prj_sdo','2017-12-21 11:32:52','2017-12-21 11:32:52'),(325,1,'prj_wid','2017-12-21 11:32:52','2017-12-21 11:32:52'),(326,1,'prj_sta','2017-12-21 11:32:52','2017-12-21 11:32:52'),(327,1,'prj_kwi','2017-12-21 11:32:52','2017-12-21 11:32:52'),(328,1,'prj_kow','2017-12-21 11:32:52','2017-12-21 11:32:52'),(329,1,'prj_fde','2017-12-21 11:32:52','2017-12-21 11:32:52'),(330,1,'prj_fak','2017-12-21 11:32:52','2017-12-21 11:32:52'),(331,1,'prj_sed','2017-12-21 11:32:52','2017-12-21 11:32:52'),(332,1,'prj_tap','2017-12-21 11:32:52','2017-12-21 11:32:52'),(333,1,'prj_taw','2017-12-21 11:32:52','2017-12-21 11:32:52'),(334,1,'prj_udo','2017-12-21 11:32:52','2017-12-21 11:32:52'),(335,1,'prj_wia','2017-12-21 11:32:52','2017-12-21 11:32:52'),(336,1,'pro_ace','2017-12-21 11:32:52','2017-12-21 11:32:52'),(337,1,'pro_ain','2017-12-21 11:32:52','2017-12-21 11:32:52'),(338,1,'pro_awi','2017-12-21 11:32:52','2017-12-21 11:32:52'),(339,1,'pro_app','2017-12-21 11:32:52','2017-12-21 11:32:52'),(340,1,'pro_etp','2017-12-21 11:32:52','2017-12-21 11:32:52'),(341,1,'pro_eup','2017-12-21 11:32:52','2017-12-21 11:32:52'),(342,1,'pro_ppr','2017-12-21 11:32:52','2017-12-21 11:32:52'),(343,1,'pro_wce','2017-12-21 11:32:52','2017-12-21 11:32:52'),(344,1,'pty_dig','2017-12-21 11:32:52','2017-12-21 11:32:52'),(345,1,'pty_pgz','2017-12-21 11:32:52','2017-12-21 11:32:52'),(346,1,'pty_pgb','2017-12-21 11:32:52','2017-12-21 11:32:52'),(347,1,'pty_psb','2017-12-21 11:32:52','2017-12-21 11:32:52'),(348,1,'pty_psz','2017-12-21 11:32:52','2017-12-21 11:32:52'),(349,1,'rol_def','2017-12-21 11:32:52','2017-12-21 11:32:52'),(350,1,'rol_eup','2017-12-21 11:32:52','2017-12-21 11:32:52'),(351,1,'rol_tup','2017-12-21 11:32:52','2017-12-21 11:32:52'),(352,1,'rol_adm','2017-12-21 11:32:52','2017-12-21 11:32:52'),(353,1,'rol_ggr','2017-12-21 11:32:52','2017-12-21 11:32:52'),(354,1,'rol_gra','2017-12-21 11:32:52','2017-12-21 11:32:52'),(355,1,'rol_kli','2017-12-21 11:32:52','2017-12-21 11:32:52'),(356,1,'rol_pra','2017-12-21 11:32:52','2017-12-21 11:32:52'),(357,1,'rol_spr','2017-12-21 11:32:52','2017-12-21 11:32:52'),(358,1,'skl_psl','2017-12-21 11:32:52','2017-12-21 11:32:52'),(359,1,'sta_psg','2017-12-21 11:32:52','2017-12-21 11:32:52'),(360,1,'sta_kli','2017-12-21 11:32:52','2017-12-21 11:32:52'),(361,1,'sta_ogo','2017-12-21 11:32:52','2017-12-21 11:32:52'),(362,1,'sta_psp','2017-12-21 11:32:52','2017-12-21 11:32:52'),(363,1,'sta_pss','2017-12-21 11:32:52','2017-12-21 11:32:52'),(364,1,'ust_pus','2017-12-21 11:32:52','2017-12-21 11:32:52'),(365,1,'uzy_euz','2017-12-21 11:32:52','2017-12-21 11:32:52'),(366,1,'uzy_ppu','2017-12-21 11:32:52','2017-12-21 11:32:52'),(367,1,'uzy_tuz','2017-12-21 11:32:52','2017-12-21 11:32:52'),(368,1,'zam_pwy','2017-12-21 11:32:52','2017-12-21 11:32:52');
/*!40000 ALTER TABLE `uprawnienia_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ustawienia_systemu`
--

LOCK TABLES `ustawienia_systemu` WRITE;
/*!40000 ALTER TABLE `ustawienia_systemu` DISABLE KEYS */;
INSERT INTO `ustawienia_systemu` VALUES (1,NULL,'js_refr','30000','2017-12-21 11:32:52','2017-12-21 11:32:52');
/*!40000 ALTER TABLE `ustawienia_systemu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `uzytkownicy`
--

LOCK TABLES `uzytkownicy` WRITE;
/*!40000 ALTER TABLE `uzytkownicy` DISABLE KEYS */;
INSERT INTO `uzytkownicy` VALUES (1,NULL,1,1,'pl_PL','pawel.rymsza@pastmo.pl','Paweł','Rymsza','21232f297a57a5a743894a0e4a801fc3','pawel.rymsza@pastmo.pl','',NULL,'pracownik',NULL,'1','0','0','0','0','2016-09-24 10:00:43','2018-05-07 22:17:03');
/*!40000 ALTER TABLE `uzytkownicy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-12 14:27:30
