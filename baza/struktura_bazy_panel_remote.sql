-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Wersja serwera:               5.6.21 - MySQL Community Server (GPL)
-- Serwer OS:                    Win32
-- HeidiSQL Wersja:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Zrzut struktury tabela pastmo_panel_remote.adresy
DROP TABLE IF EXISTS `adresy`;
CREATE TABLE IF NOT EXISTS `adresy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ulica` text COLLATE utf16_polish_ci,
  `kod` text COLLATE utf16_polish_ci,
  `miasto` text COLLATE utf16_polish_ci,
  `kraj` text COLLATE utf16_polish_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;

-- Data exporting was unselected.


-- Zrzut struktury tabela pastmo_panel_remote.klienci
DROP TABLE IF EXISTS `klienci`;
CREATE TABLE IF NOT EXISTS `klienci` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `adres_id` int(11) DEFAULT NULL,
  `adres_faktury_id` int(11) DEFAULT NULL,
  `nazwa` text COLLATE utf16_polish_ci,
  `nazwa_na_fakturze` text COLLATE utf16_polish_ci,
  `nip` bigint(20) DEFAULT NULL,
  `status` enum('Nowy','Stały','Zrealizowany') COLLATE utf16_polish_ci DEFAULT 'Nowy',
  `www` text COLLATE utf16_polish_ci,
  `notatka` text COLLATE utf16_polish_ci,
  PRIMARY KEY (`id`),
  KEY `FK_klienci_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_klienci_adresy` (`adres_id`),
  KEY `FK_klienci_adresy_2` (`adres_faktury_id`),
  CONSTRAINT `FK_klienci_adresy` FOREIGN KEY (`adres_id`) REFERENCES `adresy` (`id`),
  CONSTRAINT `FK_klienci_adresy_2` FOREIGN KEY (`adres_faktury_id`) REFERENCES `adresy` (`id`),
  CONSTRAINT `FK_klienci_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;

-- Data exporting was unselected.


-- Zrzut struktury tabela pastmo_panel_remote.klienci_dodatkowe_pola
DROP TABLE IF EXISTS `klienci_dodatkowe_pola`;
CREATE TABLE IF NOT EXISTS `klienci_dodatkowe_pola` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `klient_id` int(11) DEFAULT NULL,
  `wartosc` text COLLATE utf16_polish_ci,
  PRIMARY KEY (`id`),
  KEY `FK_klienci_dodatkowe_pola_klienci` (`klient_id`),
  CONSTRAINT `FK_klienci_dodatkowe_pola_klienci` FOREIGN KEY (`klient_id`) REFERENCES `klienci` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;

-- Data exporting was unselected.


-- Zrzut struktury tabela pastmo_panel_remote.klienci_notatki
DROP TABLE IF EXISTS `klienci_notatki`;
CREATE TABLE IF NOT EXISTS `klienci_notatki` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `klient_id` int(11) DEFAULT NULL,
  `tekst` text COLLATE utf16_polish_ci NOT NULL,
  `data_dodania` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_notatki_klienci` (`klient_id`),
  CONSTRAINT `FK_notatki_klienci` FOREIGN KEY (`klient_id`) REFERENCES `klienci` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;

-- Data exporting was unselected.


-- Zrzut struktury tabela pastmo_panel_remote.kontakty
DROP TABLE IF EXISTS `kontakty`;
CREATE TABLE IF NOT EXISTS `kontakty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pracownik_id` int(11) DEFAULT NULL,
  `godziny_pracy` text COLLATE utf16_polish_ci,
  `typ` enum('Dział techniczny','Obsługa klienta','Sklep','Portal informacyjny','Sprzedaż') COLLATE utf16_polish_ci DEFAULT 'Dział techniczny',
  PRIMARY KEY (`id`),
  KEY `FK_kontakty_pracownicy` (`pracownik_id`),
  CONSTRAINT `FK_kontakty_pracownicy` FOREIGN KEY (`pracownik_id`) REFERENCES `pracownicy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;

-- Data exporting was unselected.


-- Zrzut struktury tabela pastmo_panel_remote.notatki
DROP TABLE IF EXISTS `notatki`;
CREATE TABLE IF NOT EXISTS `notatki` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pracownik_id` int(11) DEFAULT NULL,
  `tresc` text COLLATE utf16_polish_ci,
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_notatki_uzytkownicy` (`pracownik_id`),
  CONSTRAINT `FK_notatki_uzytkownicy` FOREIGN KEY (`pracownik_id`) REFERENCES `uzytkownicy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;

-- Data exporting was unselected.


-- Zrzut struktury tabela pastmo_panel_remote.pracownicy
DROP TABLE IF EXISTS `pracownicy`;
CREATE TABLE IF NOT EXISTS `pracownicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `gg` varchar(20) COLLATE utf16_polish_ci DEFAULT NULL,
  `stanowisko` varchar(50) COLLATE utf16_polish_ci DEFAULT NULL,
  `data_zatrudnienia` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pracownicy_uzytkownicy` (`uzytkownik_id`),
  CONSTRAINT `FK_pracownicy_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;

-- Data exporting was unselected.


-- Zrzut struktury tabela pastmo_panel_remote.uzytkownicy
DROP TABLE IF EXISTS `uzytkownicy`;
CREATE TABLE IF NOT EXISTS `uzytkownicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `avatar_id` int(11) DEFAULT NULL,
  `login` text COLLATE utf8_polish_ci,
  `imie` text COLLATE utf8_polish_ci,
  `nazwisko` text COLLATE utf8_polish_ci,
  `haslo` text COLLATE utf8_polish_ci,
  `mail` text COLLATE utf8_polish_ci,
  `telefon` char(12) COLLATE utf8_polish_ci DEFAULT NULL,
  `typ` enum('admin','pracownik','klient') COLLATE utf8_polish_ci DEFAULT 'klient',
  PRIMARY KEY (`id`),
  KEY `FK_uzytkownicy_zasoby` (`avatar_id`),
  CONSTRAINT `FK_uzytkownicy_zasoby` FOREIGN KEY (`avatar_id`) REFERENCES `zasoby` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Data exporting was unselected.


-- Zrzut struktury tabela pastmo_panel_remote.zasoby
DROP TABLE IF EXISTS `zasoby`;
CREATE TABLE IF NOT EXISTS `zasoby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text COLLATE utf8_polish_ci NOT NULL,
  `typ` enum('plik','avatar','obrazek') COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
