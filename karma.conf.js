//jshint strict: false
module.exports = function (config) {
    config.set({
	basePath: './public_html',
	files: [

	    'js/pages/kody_uprawnien.js',
	    'js/test/pomocnicze/**/*.js',
	    'lib/jquery/jquery.min.js',
	    'bower_components/jquery-validation/dist/jquery.validate.min.js',
	    'js/jquery-ui-1.10.3.custom.min.js',
	    'js/moment-with-locales.min.js',
	    'js/bootstrap.js',
	    '../node_modules/jasmine-jquery/lib/jasmine-jquery.js',

	    'js/aplikacja_adapter.js',
	    'js/podejmij-decyzje.js',
	    'js/pastmo/*.js',
	    'js/decyzje/*.js',
	    'js/helpery/*.js',
	    'js/strony/*.js',
//	    'js/test/testy/**/*.*',
	    'js/test/testy/decyzje/**/*.*',
	    'js/test/testy/helpery/**/*.*',

//	    'js/*.spec.js'
	],
	autoWatch: true,
	frameworks: ['jasmine'],
	browsers: [
	    'Chrome'
//	, 'Firefox'
	],
	plugins: [
	    'karma-chrome-launcher',
	    'karma-firefox-launcher',
	    'karma-jasmine',
//	    'jasmine-jquery'
	],
	client: {
	  //  args: ['--grep', config.grep]//Odblokwać jeśli jest potrzeba uruchamiać selektywnie testy. np karma start --grep=modali
	}

    });
};
