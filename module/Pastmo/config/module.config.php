<?php

namespace Pastmo;

return \Application\AktywneModulyZPastmo::dodajKontrolery(array(
		'controllers' => array(
			'factories' => array(
				'Pastmo\Wspolne\Controller\PastmoWspolneAjax' => Wspolne\Controller\Factory\AjaxFactory::class
			),
		),
		'router' => array(
			'routes' => array(
				'pastmo_wspolne_ajax' => array(
					'type' => 'segment',
					'options' => array(
						'route' => '/pastmo_wspolne_ajax[/:action][/:id]',
						'constraints' => array(
							'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
							'id' => '[0-9]+',
						),
						'defaults' => array(
							'controller' => 'Pastmo\Wspolne\Controller\PastmoWspolneAjax',
							'action' => 'index',
						),
					),
				),
			),
		),
		'view_manager' => array(
			'template_path_stack' => array(
				'Pastmo' => __DIR__ . '/../view',
			),
		),
	));
