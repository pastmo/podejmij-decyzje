<?php

namespace PastmoTest\TlumaczeniaTest\Integracyjne;

use Zend\Stdlib\ArrayUtils;
use Pastmo\Zasoby\PrzeszukiwaniePlikow\PrzeszukiwaczPlikowTlumaczen;
use Pastmo\Zasoby\PrzeszukiwaniePlikow\RekordWyszukanegoTlumaczenia;
use Pastmo\Tlumaczenia\Menager\GeneratorTlumaczen;

class GeneratorTlumaczenTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_obsluzPojedynczeTlumaczenie() {
	$rekord = RekordWyszukanegoTlumaczenia::create()
		->setTekst("Tekst tlumaczenia")
		->setPlik("jakis plik");
	$generator = $this->zrobGenerator(['generuj', 'zapiszInsertUpdate', 'modyfikujPlikZrodlowy']);

	$generator->obsluzPojedynczeTlumaczenie($rekord);

	$dodaneTlumaczenie = $this->getDodaneTlumaczenie();

	$this->assertNotNull($dodaneTlumaczenie->id);
	$this->assertNotNull($dodaneTlumaczenie->nr);
	$this->assertEquals("Tekst tlumaczenia", $dodaneTlumaczenie->oryginalny_tekst);
    }

    public function test_modyfikujPlikZrodlowy() {
	$this->sprawdzTestModyfikowaniaPliuZrodlowego(
		'$this->_translate("Tekst tlumaczenia");', '$this->_translate("Tekst tlumaczenia", false, 42);');
    }

    public function test_modyfikujPlikZrodlowy_z_ustawionym_domknieciem() {
	$this->sprawdzTestModyfikowaniaPliuZrodlowego(
		'$this->_translate("Tekst tlumaczenia", "\'");', '$this->_translate("Tekst tlumaczenia", "\'", 42);');
    }

    public function test_modyfikujPlikZrodlowy_drugie_wywolanie() {
	$this->sprawdzTestModyfikowaniaPliuZrodlowego(
		'$this->_translate("Tekst tlumaczenia", "\'", 33); $this->_translate("Tekst tlumaczenia", "\'");',
		'$this->_translate("Tekst tlumaczenia", "\'", 33); $this->_translate("Tekst tlumaczenia", "\'", 42);');
    }

    public function test_modyfikujPlikZrodlowy_polska_czcionka() {
	$this->sprawdzTestModyfikowaniaPliuZrodlowego(
		'$this->_translate("Tekst źęść");', '$this->_translate("Tekst źęść", false, 42);', 'Tekst źęść');
    }

    public function test_kompilujDoMo_logowanie_komunikatow() {
	$generator = $this->zrobGenerator(['wykonajMsgfmt']);
	$generator->method('wykonajMsgfmt')->willReturn('Komunikat msgfmt');

	$generator->kompilujDoMo('kod');

	$log = $this->sm->get(\Wspolne\Menager\LogiMenager::class)->getPoprzednioDodany();

	$this->sprawdzStringZawiera($log->tresc,'Komunikat msgfmt');
    }

    private function sprawdzTestModyfikowaniaPliuZrodlowego($zrodlowaTresc, $wynik,
	    $tekstTlumaczenia = "Tekst tlumaczenia") {

	$rekord = RekordWyszukanegoTlumaczenia::create()
		->setTekst($tekstTlumaczenia)
		->setPlik("jakis plik")
		->setCudzyslow('"');

	$generator = $this->zrobGenerator(['generuj', 'pobierzTrescPliku', 'zapiszPlik']);
	$generator->method('pobierzTrescPliku')->willReturn($zrodlowaTresc);
	$generator->expects($this->once())->method('zapiszPlik')->with($this->equalTo("jakis plik"),
		$this->equalTo($wynik));

	$generator->modyfikujPlikZrodlowy($rekord, 42);
    }

    private function zrobGenerator($metodyDoPrzesloniecia = []) {
	return $this->zrobMockObiektu(GeneratorTlumaczen::class, $metodyDoPrzesloniecia);
    }

    private function getDodaneTlumaczenie() {
	return $this->sm->get(\Pastmo\Tlumaczenia\Menager\TlumaczeniaMenager::class)->getPoprzednioDodany();
    }

}
