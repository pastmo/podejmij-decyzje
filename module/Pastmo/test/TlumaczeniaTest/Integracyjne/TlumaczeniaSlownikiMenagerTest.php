<?php

namespace PastmoTest\TlumaczeniaTest\Integracyjne;

use Pastmo\Testy\Util\TB;
use Pastmo\Tlumaczenia\Entity\TlumaczenieSlownik;
use Pastmo\Tlumaczenia\Entity\Slownik;
use Pastmo\Tlumaczenia\Menager\TlumaczeniaSlownikiMenager;
use Pastmo\Tlumaczenia\Entity\Tlumaczenie;

class TlumaczeniaSlownikiMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public $tlumaczenie;
    public $slownik;
    public $tlumaczeniaSlownikiMenager;

    public function setUp() {
	parent::setUp();
	$this->tlumaczeniaSlownikiMenager = $this->sm->get(TlumaczeniaSlownikiMenager::class);
    }

    public function test_dodajRekord() {
	$this->utworzIZalogujUzytkownika();
	$encja = TB::create(TlumaczenieSlownik::class, $this)->make();

	$this->assertNotNull($encja->id);
    }

    public function test_getTekst() {
	$this->utworzIZalogujUzytkownika();
	$tlumaczenieSlownik = $this->zrobTlumaczenieZeSlownikiem();

	$tekst = $tlumaczenieSlownik->getTekst();

	$this->assertEquals('oryginalny tekst', $tekst);
    }

    public function test_getTlumaczenieZBiezacegoSlownika() {
	$uzytkownik = TB::create(\Logowanie\Model\Uzytkownik::class, $this)
			->addOneToMany('slownik', TB::create(Slownik::class, $this)
				->setResultField('slownik'))->make();
	$this->ustawZalogowanegoUsera($uzytkownik->id);

	$tlumaczenieSlownik = $this->zrobTlumaczenieZeSlownikiem();

	$wynik = $this->tlumaczeniaSlownikiMenager->getTlumaczenieZBiezacegoSlownika($this->tlumaczenie->nr);

	$this->assertEquals($tlumaczenieSlownik->id, $wynik->id);
    }

    private function zrobTlumaczenieZeSlownikiem() {
	$tlumaczenie = TB::create(Tlumaczenie::class, $this)
			->setParameters(['oryginalny_tekst' => 'oryginalny tekst'])
			->setResultField('tlumaczenie')->make();
	$tlumaczenieSlownik = TB::create(TlumaczenieSlownik::class, $this)
			->setParameters(['tekst' => null, 'tlumaczenie_nr' => $tlumaczenie->nr, 'slownik_id' => $this->slownik])->make();
	return $tlumaczenieSlownik;
    }

}
