<?php

namespace PastmoTest\TlumaczeniaTest\Integracyjne;

use Zend\Stdlib\ArrayUtils;
use Pastmo\Tlumaczenia\Menager\GeneratorTlumaczen;

class GeneratorTlumaczenRunTest extends \Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase {

    use \Pastmo\Testy\Util\TestBuilderWrapper,
	\Pastmo\Testy\TraityPomocnicze\SprawdzMetody,
	\Pastmo\Testy\TraityPomocnicze\StaleWTestach,
	\Pastmo\Testy\TraityPomocnicze\UstawianieParametrowWTestach;

    public function setUp() {
	$this->ustawIgnorowanyTestZModuluPastmo();
	$configOverrides = [];

	$this->setApplicationConfig(ArrayUtils::merge(
			include \Application\Stale::configPath, $configOverrides
	));

	parent::setUp();

	$this->sm = $this->getApplicationServiceLocator();
    }

    public function test_generator() {
	$generator = new GeneratorTlumaczen($this->sm);
	$generator->generuj();
    }

    public function test_updatujPlikiTlumaczen() {
	$this->sm->get(\Pastmo\Tlumaczenia\Menager\SlownikiMenager::class)->updatujPilkiTlumaczen();
    }

    public function test_dodajTlumaczeniaSlownikiAngielskie() {
	$this->markTestSkipped("Test uruchamiany tylko jeśli jest taka potrzeba");
	$generator = new GeneratorTlumaczen($this->sm);
	$generator->dodajTlumaczeniaSlownikiAngielskie();
    }

}
