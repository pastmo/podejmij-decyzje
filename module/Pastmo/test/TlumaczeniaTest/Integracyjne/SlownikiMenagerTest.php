<?php

namespace PastmoTest\TlumaczeniaTest\Integracyjne;

use Pastmo\Testy\Util\TB;
use Pastmo\Tlumaczenia\Entity\Slownik;
use Pastmo\Tlumaczenia\Menager\SlownikiMenager;

class SlownikiMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public $slownik;

    public function setUp() {
	parent::setUp();
    }

    public function test_dodajRekord() {
	$encja = TB::create(Slownik::class, $this)->make();

	$this->assertNotNull($encja->id);
    }

    public function test_getAktualnySlownikZalogowanego() {
	$uzytkownik = TB::create(\Logowanie\Model\Uzytkownik::class, $this)
			->addOneToMany('slownik', TB::create(Slownik::class, $this)
				->setResultField('slownik'))->make();

	$this->ustawZalogowanegoUsera($uzytkownik->id);

	$wynik = $this->sm->get(SlownikiMenager::class)->getAktualnySlownikZalogowanego();

	$this->assertEquals($this->slownik->id, $wynik->id);
    }

    public function test_getDomyslnySlownik() {
	$wynik = $this->sm->get(SlownikiMenager::class)->getDomyslnySlownik();

	$this->assertNotNull($wynik->id);
    }

}
