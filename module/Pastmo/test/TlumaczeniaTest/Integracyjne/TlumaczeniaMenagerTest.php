<?php

namespace PastmoTest\TlumaczeniaTest\Integracyjne;

use Pastmo\Testy\Util\TB;

class TlumaczeniaMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_dodajRekord() {
	$encja = TB::create(\Pastmo\Tlumaczenia\Entity\Tlumaczenie::class, $this)->make();

	$this->assertNotNull($encja->id);
    }

}
