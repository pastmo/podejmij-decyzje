<?php

namespace PastmoTest\Tlumaczenia\Menager;

class PastmoTranslatorTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    public function setUp() {
	parent::setUp();
    }

    public function test__translate() {
	$wynik = $this->sm->get(\Pastmo\Tlumaczenia\Menager\PastmoTranslator::class)->_translate("Jakis tekst",
		false, -42);
	$this->assertEquals('Jakis tekst <i class="fa fa-pencil edycja_tekstu" data-nr="-42"></i>', $wynik);
    }

    public function test__translate_znaleziony_kod() {
	$wynik = $this->sm->get(\Pastmo\Tlumaczenia\Menager\PastmoTranslator::class)->_translate("Jakis tekst",
		false, 'Polski tekst');
	$this->assertEquals('English text <i class="fa fa-pencil edycja_tekstu" data-nr="Polski tekst"></i>', $wynik);
    }

    public function test_translate_parametr_wewnetrzny() {
	$wynik = $this->sm->get(\Pastmo\Tlumaczenia\Menager\PastmoTranslator::class)->_translate("Jakis tekst", '"',
		-55);
	$this->assertEquals('Jakis tekst" data-nr_tlumaczenia="-55', $wynik);
    }

}
