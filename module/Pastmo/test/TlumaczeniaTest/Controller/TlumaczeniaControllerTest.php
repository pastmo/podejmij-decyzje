<?php

namespace PastmoTest\TlumaczeniaTest\Controller;

use Pastmo\Testy\Util\TB;
use Pastmo\Tlumaczenia\Entity\TlumaczenieSlownik;

class TlumaczeniaControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    public function testIndex() {

    }

    public function test_edycjaTlumaczeniaAction() {
	$tlumaczenieSlownik = $tlumaczenieSlownik = TB::create(TlumaczenieSlownik::class, $this)->make();

	$this->tlumaczeniaMenager->expects($this->once())
		->method('getTlumaczenieZBiezacegoSlownika')
		->with($this->equalTo(42))
		->willReturn($tlumaczenieSlownik);

	$this->dispatch('/tlumaczenia/edycja_tlumaczenia?nr=42');
//	$this->sprawdzCzyStatus200();
    }

}
