<?php

namespace PastmoTest\TlumaczeniaTest\Controller;

class TlumaczeniaAjaxControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    public function testIndex() {

    }

    public function test_aktywacjaTrybuEdycjiAction() {
	$this->sesjaMenager->expects($this->once())->method('setTrybTlumaczen')->with($this->equalTo(1));
	$this->ustawParametryPosta(['tryb' => "1"]);
	$this->dispatch('/tlumaczenia_ajax/aktywacja_trybu_edycji');
	$this->sprawdzCzySukces();
    }

}
