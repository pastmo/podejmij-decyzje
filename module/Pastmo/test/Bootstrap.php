<?php

namespace PastmoTest;

require_once dirname(__FILE__) . '/../../Wspolne/test/WspolneBootstrap.php';

chdir(__DIR__);

class Bootstrap extends \WspolneTest\WspolneBootstrap {

}

Bootstrap::init(array(
	'Application','Wspolne','Email','Logowanie','Konwersacje','Zasoby','Pastmo'
));
Bootstrap::chroot();
