<?php

namespace LogiTest\Integracyjne;

use Pastmo\Testy\Util\TB;
use \Pastmo\Logi\Enumy\KodyKategoriiLogow;

class LogiMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $logiMenager;

    public function setUp() {
	parent::setUp();
	$this->logiMenager = $this->sm->get(\Pastmo\Logi\Menager\LogiMenager::class);
    }

    public function testZapisuKonta() {

	$log = TB::create(\Pastmo\Logi\Entity\Log::class, $this)
		->setPf(\Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuNiepowiazane::create())
		->setParameters(array('tresc' => __CLASS__))
		->make();

	$this->logiMenager->zapisz($log);

	$poprzednioDodany = $this->logiMenager->getPoprzednioDodany();

	$this->assertEquals(__CLASS__, $poprzednioDodany->tresc);
    }

    public function testDodawaniaLogow() {
	$this->ustawZalogowanegoUseraId1();
	$klucz1 = "klasa";
	$klucz2 = "metoda";
	$kluczNieistniejacy = "kluczNieistniejacy";

	$builder = \Pastmo\Logi\Menager\LogiMenager::getBuilder()
		->setKodKategorii(KodyKategoriiLogow::EDYCJA_STANOW_MAGAZYNOWYCH)
		->addTresc($klucz1, __CLASS__)
		->addTresc($klucz2, __METHOD__);
	$this->logiMenager->dodajLog($builder);

	$poprzednioDodany = $this->logiMenager->getPoprzednioDodany();

	$this->assertEquals(KodyKategoriiLogow::EDYCJA_STANOW_MAGAZYNOWYCH, $poprzednioDodany->kategoria->kod);
	$this->assertEquals(__CLASS__, $poprzednioDodany->getTresc($klucz1));
	$this->assertEquals(__METHOD__, $poprzednioDodany->getTresc($klucz2));
	$this->assertEquals('', $poprzednioDodany->getTresc($kluczNieistniejacy));
    }

    public function testDodajLogBazyProduktowPrzezDodaniePelnegoProduktyMagazyn() {
	$produktMagazyn = TB::create(\BazaProduktow\Entity\ProduktMagazyn::class, $this)
		->setPf(\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->sm))
		->make();
	$log = $this->logiMenager->getPoprzednioDodany();
	$trescLoga = $this->zwrocTabliceTresciLoga($log);

	$this->assertEquals($trescLoga['wariacja'], (string) $produktMagazyn->produkt_wariacja);
	$this->assertEquals($trescLoga['magazyn'], $produktMagazyn->magazyn->nazwa);
	$this->assertEquals($trescLoga['ilosc'], $produktMagazyn->ilosc);
    }

    public function testDodajLogBazyProduktowIZrobUpdate() {
	$produktyMagazynMenager = $this->sm->get(\BazaProduktow\Menager\ProduktyMagazynMenager::class);
	$produktWariacja = TB::create(\BazaProduktow\Entity\ProduktWariacja::class, $this)
		->setPf(\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->sm))
		->make();
	$magazyn = TB::create(\Magazyn\Entity\Magazyn::class, $this)
		->setPf(\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->sm))
		->make();
	$produktMagazyn = TB::create(\BazaProduktow\Entity\ProduktMagazyn::class, $this)
		->setPf(\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->sm))
		->setParameters([
			'produkt_wariacja' => $produktWariacja->id,
			'magazyn' => $magazyn->id
		])
		->make();
	$log = $this->logiMenager->getPoprzednioDodany();
	$trescLogaPoDodaniu = $this->zwrocTabliceTresciLoga($log);

	$produktyMagazynMenager->update($produktMagazyn->id, ['ilosc' => ($trescLogaPoDodaniu['ilosc'] + 1)]);

	$logPoUpdate = $this->logiMenager->getPoprzednioDodany();
	$trescLogaPoUpdate = $this->zwrocTabliceTresciLoga($logPoUpdate);

	$this->assertNotEquals($trescLogaPoDodaniu['ilosc'], $trescLogaPoUpdate['ilosc']);
    }

    private function zwrocTabliceTresciLoga(\Pastmo\Logi\Entity\Log $log) {
	$tresc = $log->tresc;
	$tablica = json_decode($tresc);
	return (array) $tablica;
    }

}
