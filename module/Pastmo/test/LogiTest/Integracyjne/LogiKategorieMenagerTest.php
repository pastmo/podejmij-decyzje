<?php

namespace LogiTest\Integracyjne;

use Pastmo\Testy\Util\TB;

class LogiKategorieMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public function testZapisuKonta() {

	$log = TB::create(\Pastmo\Logi\Entity\LogKategoria::class, $this)
		->setPf(\Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuNiepowiazane::create())
		->make();

	$logiMenager = $this->sm->get(\Pastmo\Logi\Menager\LogiKategorieMenager::class);

	$logiMenager->zapisz($log);

	$poprzednioDodany = $logiMenager->pobierzZWherem("kod='{$log->kod}'",'kod asc');

	$this->assertEquals(1, count($poprzednioDodany));
    }

}
