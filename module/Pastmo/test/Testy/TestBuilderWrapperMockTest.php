<?php

namespace PastmoTest\Testy;

class TestBuilderWrapperMockTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_makeEntityBuilder() {
	$builder = $this->makeEntityBuilder(\Projekty\Entity\Projekt::class);

	$this->assertNotNull($builder);
    }

    public function test_setDefaultFactoryParameters() {

	$this->assertEquals(\Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::class,get_class ($this->defaultFactoryParameters));
    }

}
