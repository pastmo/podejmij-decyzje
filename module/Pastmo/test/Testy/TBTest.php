<?php

namespace PastmoTest\Testy;

use \Pastmo\Uzytkownicy\Entity\Uzytkownik;
use \Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuNiepowiazane;

class TBTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    public $wynik;
    public $rola;

    public function setUp() {
	parent::setUp();
    }

    public function test_make() {

	$uzytkownik = $this->makeEntityBuilder(Uzytkownik::class)->make();

	$this->checkResultClass($uzytkownik);
	$this->assertNotNull($uzytkownik->id);
	$this->assertEquals($this->lastEntity, $uzytkownik);
    }

    public function test_make_parametry() {

	$uzytkownik = $this->makeEntityBuilder(Uzytkownik::class)
		->setParameters(array('imie' => 'Nowe imie'))
		->make();

	$this->assertEquals('Nowe imie', $uzytkownik->imie);
    }

    public function test_make_factoryClass() {

	$uzytkownik = $this->makeEntityBuilder(Uzytkownik::class)
		->setFactoryClass(\Pastmo\Testy\Util\TB::FABRYKA_REKORDOW)
		->setPf(PFIntegracyjneBrakzapisuNiepowiazane::create())
		->make();

	$this->checkResultClass($uzytkownik);
	$this->assertNull($uzytkownik->id);
    }

    public function test_make_resultField() {

	$uzytkownik = $this->makeEntityBuilder(Uzytkownik::class)
		->setResultField('wynik')
		->make();

	$this->assertEquals($this->wynik, $uzytkownik);
    }

    public function test_make_oneToMany() {

	$this->makeEntityBuilder(Uzytkownik::class)
		->setResultField('wynik')
		->addOneToMany('rola',
			$this->makeEntityBuilder(\Logowanie\Entity\Rola::class)
			->setResultField('rola')
		)
		->make();

	$this->assertEquals($this->rola, $this->wynik->rola);
	$this->assertNotNull($this->rola->id);
    }

    private function checkResultClass($uzytkownik) {
	$this->assertEquals(\Logowanie\Model\Uzytkownik::class, get_class($uzytkownik));
    }

}
