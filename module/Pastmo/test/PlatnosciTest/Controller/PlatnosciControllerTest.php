<?php

namespace PastmoTest\Platnosci\Controller;

use Pastmo\Testy\Util\TB;
use Pastmo\Tlumaczenia\Entity\TlumaczenieSlownik;

class PlatnosciControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    public function testIndex() {

    }

    public function test_powrotAction() {
	$this->dispatch('/platnosci/powrot/42');
	$this->sprawdzCzyStatus200();
    }

    public function test_statusAction() {
	$this->platnosciMenager->expects($this->once())->method('obsluzPotwierdzeniePlatnosci');
	$this->ustawParametryPosta([]);

	$this->dispatch('/platnosci/status/42');
    }

}
