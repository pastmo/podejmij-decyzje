<?php
namespace Pastmo\PlatnosciTest\Entity;

use Pastmo\Platnosci\Entity\Platnosc;

class PlatnoscTest extends \Testy\BazoweKlasyTestow\AbstractMockTest{

    public function test_getKwote(){
	$platnosc= new Platnosc();
	$platnosc->kwota=42.44;

	$wynik= $platnosc->getKwote();

	$this->assertEquals('4244',$wynik);
    }
}
