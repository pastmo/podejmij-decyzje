<?php

namespace Pastmo\PlatnosciTest\Integracyjne;

use Pastmo\Testy\Util\TB;
use Pastmo\Platnosci\Entity\Cennik;
use Pastmo\Platnosci\Entity\Platnosc;
use Pastmo\Platnosci\Menager\PlatnosciMenager;
use Pastmo\Platnosci\Enumy\PlatnosciStatusy;

class PlatnosciMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $platnosciMenager;

    public function setUp() {
	parent::setUp();
	$this->platnosciMenager = $this->sm->get(PlatnosciMenager::class);
    }

    public function testDodajEncje() {

	$encja = TB::create(Platnosc::class, $this)
		->make();

	$this->assertNotNull($encja->id);
    }

    public function test_utworzPlatnoscDlaUzytkownika() {
	$this->utworzIZalogujUzytkownika();
	$cennik = TB::create(Cennik::class, $this)->make();

	$this->platnosciMenager = $this->zrobMockObiektu(PlatnosciMenager::class,
		['utworzUrlPowrotu', 'utworzUrlPrzekazaniaStatusu']);
	$this->platnosciMenager->method('utworzUrlPowrotu')->willReturn('https://pastmo.pl');
	$this->platnosciMenager->method('utworzUrlPrzekazaniaStatusu')->willReturn('https://pastmo.pl');

	$urlPrzekierowania = $this->platnosciMenager->utworzPlatnoscDlaUzytkownika($this->zalogowany, $cennik);

	$platnosc = $this->platnosciMenager->getPoprzednioDodany();

	$this->assertEquals($this->zalogowany->id, $platnosc->uzytkownik_id);
	$this->assertEquals($this->zalogowany->mail, $platnosc->email);
	$this->assertEquals($cennik->kwota, $platnosc->kwota);
	$this->assertEquals(PlatnosciStatusy::ZAREJESTROWANA, $platnosc->status);
	$this->assertNotNull($platnosc->token);
	$this->assertNotNull($urlPrzekierowania);
    }

    public function test_utworzPlatnoscDlaUzytkownika_obsluga_bledow() {
	$this->utworzIZalogujUzytkownika();
	$cennik = TB::create(Cennik::class, $this)->make();

	$this->platnosciMenager = $this->zrobMockObiektu(PlatnosciMenager::class,
		['utworzObiektPrzelewow24', 'utworzUrlPowrotu', 'utworzUrlPrzekazaniaStatusu']);
	$przelewy24 = $this->getMockBuilder(\Pastmo\Platnosci\Utils\Przelewy24::class)
		->disableOriginalConstructor()
		->getMock();

	$przelewy24->method('trnRegister')->willReturn(['error' => 1, 'errorMessage' => 'tresc_bledu']);

	$this->platnosciMenager->method('utworzObiektPrzelewow24')->willReturn($przelewy24);

	try {
	    $this->platnosciMenager->utworzPlatnoscDlaUzytkownika($this->zalogowany, $cennik);
	    $this->fail('Powinien być zwrócony wyjątek');
	} catch (\Pastmo\Platnosci\Exception\PlatnosciException $e) {
	    $this->assertEquals('tresc_bledu', $e->getMessage());
	}

	$platnosc = $this->platnosciMenager->getPoprzednioDodany();

	$this->assertEquals(PlatnosciStatusy::BLAD, $platnosc->status);

	$log = $this->getPoprzedniLog();
	$this->sprawdzStringZawiera($log->tresc, 'tresc_bledu');
    }

    public function test_obsluzPotwierdzeniePlatnosci() {
	$platnosc = TB::create(Platnosc::class, $this)->setParameters(['kwota' => 5])
			->addOneToMany('cennik', TB::create(Cennik::class, $this)
				->setParameters(['dni_przedluzenia' => 365]))->make();

	$this->platnosciMenager = $this->zrobMockObiektu(PlatnosciMenager::class, ['utworzObiektPrzelewow24']);
	$przelewy24 = $this->getMockBuilder(\Pastmo\Platnosci\Utils\Przelewy24::class)
		->disableOriginalConstructor()
		->getMock();
	$przelewy24->method('trnVerify')->willReturn(['error' => 0]);
	$this->platnosciMenager->method('utworzObiektPrzelewow24')->willReturn($przelewy24);

	$arr = ['p24_merchant_id' => "999",
		'p24_pos_id' => "999",
		'p24_session_id' => "{$platnosc->id}",
		'p24_amount' => "",
		'p24_currency' => "500",
		'p24_order_id' => "109746551",
		'p24_method' => "5",
		'p24_statement' => "p24-A09-746-551",
		'p24_sign' => "b293d66b6e62dd6c47b9bea006169615"];

	$this->platnosciMenager->obsluzPotwierdzeniePlatnosci($this->arrayToParameters($arr));


	$log = $this->getPoprzedniLog();
	$this->sprawdzStringZawiera($log->tresc, 'p24_merchant_id');

	$zapisany = $this->platnosciMenager->getRekord($platnosc->id);
	$this->assertEquals(109746551, $zapisany->order_id);
	$this->assertEquals(PlatnosciStatusy::ZAKONCZONA, $zapisany->status);
    }

    public function test_obsluzPotwierdzeniePlatnosci_blad() {
	$platnosc = new Platnosc();
	$platnosc->id = 2;
	$platnosc->kwota = 542;

	$this->platnosciMenager = $this->zrobMockObiektu(PlatnosciMenager::class, ['getRekord']);
	$this->platnosciMenager->method('getRekord', 'zapisz')->willReturn($platnosc);

	$arr = ['p24_session_id' => "2",
		'p24_order_id' => "109746551",
	];

	$this->platnosciMenager->obsluzPotwierdzeniePlatnosci($this->arrayToParameters($arr));

	$log = $this->getPoprzedniLog();
	$this->sprawdzStringZawiera($log->tresc, 'Niezgodno\u015b\u0107 kwoty transakcji');
    }

    private function getPoprzedniLog() {
	$log = $this->sm->get(\Wspolne\Menager\LogiMenager::class)->getPoprzednioDodany();
	return $log;
    }

}
