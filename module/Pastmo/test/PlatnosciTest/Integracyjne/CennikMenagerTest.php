<?php

namespace Pastmo\PlatnosciTest\Integracyjne;

use Pastmo\Testy\Util\TB;

class CennikMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public function setUp() {
	parent::setUp();
    }

    public function testDodajEncje() {

	$encja = TB::create(\Pastmo\Platnosci\Entity\Cennik::class, $this)
		->make();

	$this->assertNotNull($encja->id);
    }

}
