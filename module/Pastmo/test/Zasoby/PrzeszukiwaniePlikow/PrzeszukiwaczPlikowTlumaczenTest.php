<?php

namespace PastmoTest\ZasobyTest\PrzeszukiwaniePlikow;

use Pastmo\Zasoby\PrzeszukiwaniePlikow\PrzeszukiwaczPlikowTlumaczen;

class PrzeszukiwaczPlikowTlumaczenTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_pobierzTablicePlikowZKilkuLokalizacji() {
	$p3 = dirname(__FILE__) . '\przyklady_php\TestowaKlasa.php';
	$p4 = dirname(__FILE__) . '\przyklady_php\widok.phtml';


	$exected = array($p3 => $p3, $p4 => $p4);

	$wynik = PrzeszukiwaczPlikowTlumaczen::create()->pobierzTablicePlikow(dirname(__FILE__) . '\przyklady_php');

	$this->assertEquals($exected, $wynik);
    }

    public function test_przeszukaj_js() {
	$wynik = PrzeszukiwaczPlikowTlumaczen::create()
		->setBasePath(dirname(__FILE__) . '\przyklady')
		->setSearched(\Pastmo\Zasoby\PrzeszukiwaniePlikow\PrzeszukiwaczPlikowTlumaczen::TRANSLATE_JS)
		->przeszukaj();

	$this->assertEquals(1, count($wynik));

	$rekord = $wynik[0];
	$this->assertEquals('Tłumaczenie w js', $rekord->tekst);
	$this->assertEquals(dirname(__FILE__) . '\przyklady\testowy_plik.js', $rekord->plik);
    }

    public function test_przeszukaj_php() {
	$wynik = PrzeszukiwaczPlikowTlumaczen::create()
		->setBasePath(dirname(__FILE__) . '\przyklady_php')
		->setSearched(\Pastmo\Zasoby\PrzeszukiwaniePlikow\PrzeszukiwaczPlikowTlumaczen::TRANSLATE_PHP)
		->przeszukaj();

	$this->assertEquals(3, count($wynik));

	$rekord = $wynik[0];
	$this->assertEquals('Tekst do tłumaczenia', $rekord->tekst);
	$this->assertEquals(dirname(__FILE__) . '\przyklady_php\TestowaKlasa.php', $rekord->plik);
    }

    public function test_obsluzPojedynczyPlik_pozostale_parametry() {
	$przeszukiwacz = $this->zrobMockObiektu(PrzeszukiwaczPlikowTlumaczen::class, ['pobierzTrescPliku']);
	$przeszukiwacz->method('pobierzTrescPliku')->willReturn('$this->_translate("treść", "@", 999);');

	$przeszukiwacz
		->setSearched(\Pastmo\Zasoby\PrzeszukiwaniePlikow\PrzeszukiwaczPlikowTlumaczen::TRANSLATE_PHP)
		->obsluzPojedynczyPlik("jakis plik");

	$rekord = $przeszukiwacz->wynik[0];
	$this->assertEquals('treść', $rekord->tekst);
	$this->assertEquals("jakis plik", $rekord->plik);
	$this->assertEquals(' "@"', $rekord->domkniecie);
	$this->assertEquals(999, $rekord->nr);
    }

    public function test_obsluzPojedynczyPlik_brak_pozostalych_parametrow() {
	$przeszukiwacz = $this->zrobMockObiektu(PrzeszukiwaczPlikowTlumaczen::class, ['pobierzTrescPliku']);
	$przeszukiwacz->method('pobierzTrescPliku')->willReturn('$this->_translate("treść");');

	$przeszukiwacz
		->setSearched(\Pastmo\Zasoby\PrzeszukiwaniePlikow\PrzeszukiwaczPlikowTlumaczen::TRANSLATE_PHP)
		->obsluzPojedynczyPlik("jakis plik");

	$rekord = $przeszukiwacz->wynik[0];
	$this->assertEquals('treść', $rekord->tekst);
	$this->assertEquals("jakis plik", $rekord->plik);
	$this->assertEquals(null, $rekord->domkniecie);
	$this->assertEquals(null, $rekord->nr);
    }

    public function test_obsluzPojedynczyPlik_wewnetrzne_nawiasy() {
	$przeszukiwacz = $this->zrobMockObiektu(PrzeszukiwaczPlikowTlumaczen::class, ['pobierzTrescPliku']);
	$przeszukiwacz->method('pobierzTrescPliku')->willReturn('$this->_translate("treść(zł)");');

	$przeszukiwacz
		->setSearched(\Pastmo\Zasoby\PrzeszukiwaniePlikow\PrzeszukiwaczPlikowTlumaczen::TRANSLATE_PHP)
		->obsluzPojedynczyPlik("jakis plik");

	$rekord = $przeszukiwacz->wynik[0];
	$this->assertEquals('treść(zł)', $rekord->tekst);
    }

    public function test_obsluzPojedynczyPlik_wewnetrzny_przecinek() {
	$przeszukiwacz = $this->zrobMockObiektu(PrzeszukiwaczPlikowTlumaczen::class, ['pobierzTrescPliku']);
	$przeszukiwacz->method('pobierzTrescPliku')->willReturn("\$this->_translate('Min. %s, krok %s','\"')");

	$przeszukiwacz
		->setSearched(\Pastmo\Zasoby\PrzeszukiwaniePlikow\PrzeszukiwaczPlikowTlumaczen::TRANSLATE_PHP)
		->obsluzPojedynczyPlik("jakis plik");

	$rekord = $przeszukiwacz->wynik[0];
	$this->assertEquals('Min. %s, krok %s', $rekord->tekst);
	$this->assertEquals('\'"\'', $rekord->domkniecie);
	$this->assertEquals(null, $rekord->nr);
    }

}
