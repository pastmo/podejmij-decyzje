var Test={
    initDeleteVariation: function () {
	$('.usun-wariacje').click(function (e) {
	    var tr = $(this).closest('tr');
	    bootbox.confirm("Czy na pewno chcesz usunąć wariację z wyceny?", function (result) {
		if (result) {
		    var wariacjaId = tr.data('id');
		    Pastmo.wyslijPost('/aukcje/wycena_akcje', {id: wariacjaId, akcja: 'usun_wariacje'}, function (data) {
			if (data.success === true) {
			    location.reload();
			}
		    });
		}
	    });

	    Pastmo.wyslijGet('/ajax/message_find_tags', {
			query: request.term
		    }, function (data) {
			response(data);
		    });


	Pastmo.wyslijGet("/konwersacje/wiadomosci",
		"watek_id=" + appWiad.domOperation.pobierzAktualnyWatekId() + "&ostatnia_wiadomosc=" + appWiad.domOperation.pobierzOstatniaWiadomoscWatkuId(),
		me.odbierzWszystkieWiadomosci);
	});

	return Tlumaczenia._translate('Tłumaczenie w js');
    },
};