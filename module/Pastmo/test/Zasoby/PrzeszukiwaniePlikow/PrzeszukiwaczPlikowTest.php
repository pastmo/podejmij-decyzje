<?php

namespace PastmoTest\ZasobyTest\PrzeszukiwaniePlikow;

use Pastmo\Zasoby\PrzeszukiwaniePlikow\PrzeszukiwaczPlikow;

class PrzeszukiwaczPlikowTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_pobierzTablicePlikow() {
	$exected = array(
		dirname(__FILE__) . '\przyklady\testowy_plik.js' => dirname(__FILE__) . '\przyklady\testowy_plik.js',
		dirname(__FILE__) . '\przyklady\wewnetrzny_folder\inny_plik.js' => dirname(__FILE__) . '\przyklady\wewnetrzny_folder\inny_plik.js');

	$wynik = PrzeszukiwaczPlikow::create()->pobierzTablicePlikow(dirname(__FILE__) . '\przyklady');

	$this->assertEquals($exected, $wynik);
    }

    public function test_przeszukaj() {
	$exected = array('/aukcje/wycena_akcje', '/ajax/go_home');

	$wynik = PrzeszukiwaczPlikow::create()
		->setBasePath(dirname(__FILE__) . '\przyklady')
		->setSearched(PrzeszukiwaczPlikow::WYSLIJ_POST)
		->przeszukaj();

	$this->assertEquals($exected, $wynik);
    }

    public function test_przeszukaj_get() {
	$exected = array('/ajax/message_find_tags', '/konwersacje/wiadomosci');

	$wynik = PrzeszukiwaczPlikow::create()
		->setBasePath(dirname(__FILE__) . '\przyklady')
		->setSearched(PrzeszukiwaczPlikow::WYSLIJ_GET)
		->przeszukaj();

	$this->assertEquals($exected, $wynik);
    }

}
