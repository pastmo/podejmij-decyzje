<?php

namespace ZasobyTest\Menager;

use Pastmo\Testy\Util\TB;

class ZasobyKonwerterNapisowMenagerTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    protected $traceError = true;
    private $zasobyKonwerterNapisowMenager;
    private $zasob1;
    private $zasob2;

    const IMAGE_TYPE = 'image/png';
    const ORYGINAL_NAME = "name.jpg";
    const SRC_TYP = 'data:image';
    const BASE_64 = 'iVBORw0KGgoAAAANSUhEUgAAAAoAAAAJCAIAAACExCpEAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAwSURBVChTY3gro4IHES39/xMWNhG6kfWhIagEUAWmIqAIQgiiAg3hNBaC8ErLqAAAHHRzxxyBezYAAAAASUVORK5CYII=';
    const ZAWARTOSC_SRC = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAJCAIAAACExCpEAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAwSURBVChTY3gro4IHES39/xMWNhG6kfWhIagEUAWmIqAIQgiiAg3hNBaC8ErLqAAAHHRzxxyBezYAAAAASUVORK5CYII=';
    const CALE_IMG = '<img src="' . self::ZAWARTOSC_SRC . '">';

    public function setUp() {
	parent::setUp();

	$this->zasobyKonwerterNapisowMenager = new \Pastmo\Zasoby\Menager\ZasobyKonwerterNapisowMenager($this->sm);

	$this->zasob1 = TB::create(\Pastmo\Zasoby\Entity\Zasob::class, $this)->make();
	$this->zasob2 = TB::create(\Pastmo\Zasoby\Entity\Zasob::class, $this)->make();

	$this->zasobyUploadMenager->expects($this->any())
		->method('getPoprzednioDodany')
		->will($this->onConsecutiveCalls($this->zasob1, $this->zasob2));
    }

    public function test_konwertujZasobyZNapisu() {
	$wynik = $this->zasobyKonwerterNapisowMenager->konwertujZasobyZNapisu(self::CALE_IMG);

	$expect = '!(' . $this->zasob1->nazwa . ')[' . $this->zasob1->id . ']';
	$this->assertEquals($expect, $wynik);
    }

    public function test_konwertujZasobyZNapisu_dodatkowy_url() {
	$wynik = $this->zasobyKonwerterNapisowMenager->konwertujZasobyZNapisu(self::CALE_IMG . '<img src="ggerp.pl/url">');

	$expect = '!(' . $this->zasob1->nazwa . ')[' . $this->zasob1->id . ']<img src="ggerp.pl/url">';
	$this->assertEquals($expect, $wynik);
    }

    public function test_konwertujZasobyZNapisu_dwa_linki() {
	$wynik = $this->zasobyKonwerterNapisowMenager->konwertujZasobyZNapisu(self::CALE_IMG . '<img src="data:image/png;base64,asdfghj">');

	$expect = '!(' . $this->zasob1->nazwa . ')[' . $this->zasob1->id . ']!(' . $this->zasob2->nazwa . ')[' . $this->zasob2->id . ']';
	$this->assertEquals($expect, $wynik);
    }

    public function test_wydobadzPlikiZNapisu() {

	$napis = self::CALE_IMG;

	$wynik = $this->zasobyKonwerterNapisowMenager->wydobadzBase64PlikiZNapisu($napis);

	$this->assertEquals(1, count($wynik));
	$this->assertEquals(self::BASE_64, $wynik[0]->base64);
    }

    public function test_PlikZNapisu_fromStr() {

	$wynik = \Pastmo\Zasoby\Menager\PlikZNapisu::fromStr(self::CALE_IMG);

	$this->assertEquals(self::BASE_64, $wynik->base64);
	$this->assertEquals('png', $wynik->typ);
	$this->assertEquals(self::CALE_IMG, $wynik->zrodlo);
    }

    public function test_aktualizujNapis() {

	$plikZNapisu = \Pastmo\Zasoby\Menager\PlikZNapisu::fromStr(self::CALE_IMG);
	$plikZNapisu->zasob = $this->zasob1;

	$wynik = $this->zasobyKonwerterNapisowMenager->aktualizujNapis(self::CALE_IMG, $plikZNapisu);

	$expected = "!({$this->zasob1->nazwa})[{$this->zasob1->id}]";

	$this->assertEquals($expected, $wynik);
    }

    public function test_zrobKodLinka() {
	$zasob = TB::create(\Pastmo\Zasoby\Entity\Zasob::class, $this)->make();

	$wynik = $this->zasobyKonwerterNapisowMenager->zrobKodLinka($zasob);

	$expected = "!({$zasob->nazwa})[{$zasob->id}]";
	$this->assertEquals($expected, $wynik);
    }

}
