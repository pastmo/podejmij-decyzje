<?php

namespace PastmoTest\Zasoby\Integracyjne;

use Zasoby\Menager\ZasobyUploadMenager;
use Pastmo\Testy\Util\TB;

class ZasobyMenagerOperacjeNaPlikachTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $zasobyTable;


    public function setUp() {
	parent::setUp();
	$this->zasobyTable = new ZasobyUploadMenager($this->sm);
	$this->zalogowany = TB::create(\Logowanie\Model\Uzytkownik::class, $this)->setPF_IZP($this->sm)->make();
	$this->ustawZalogowanegoUsera($this->zalogowany->id);
    }

    public function testOperacjeNaPlikach() {
	$zasob = $this->zasobyTable->zapiszPlik('plik_testowy.txt',
		'Treść pliku testowego');

	$this->assertNotNull($zasob);
	$fullPath = $this->zasobyTable->zasobRealPathZeStringa($zasob->url);
	$this->assertTrue(file_exists($fullPath));
    }

    public function testOperacjeNaPlikach_bledna_nazwa() {
	$zasob = $this->zasobyTable->zapiszPlik("dziwna nazwa.#\t##",
		'Treść pliku testowego');

	$this->assertNotNull($zasob);
	$fullPath = $this->zasobyTable->zasobRealPathZeStringa($zasob->url);
	$this->assertTrue(file_exists($fullPath));
    }

    public function testzrobZasobZLinka() {
	$zasob = $this->zasobyTable->zrobZasobZLinka('http://pastmo.pl/logo.jpg');

	$this->assertNotNull($zasob);
	$fullPath = $this->zasobyTable->zasobRealPathZeStringa($zasob->url);
	$this->assertTrue(file_exists($fullPath));
    }

    public function test_zrobFolder() {
	$this->zasobyTable->zrobFolder('nazwa');

	$fullPath = $this->zasobyTable->zasobRealPathZeStringa('nazwa');
	$this->assertTrue(file_exists($fullPath));
	rmdir($fullPath);
    }

    public function test_pobierzUrlPodgladu() { //TODORG: testy przechodzą, ale trzeba poprawić
        $zasobPdf = $this->zasobyTable->getRekord(113);
        $this->zasobyTable->pobierzUrlPodgladu($zasobPdf);
        $nowyZasob = $this->zasobyTable->getRekord($zasobPdf->podglad_id);
        $fullPath = $this->zasobyTable->zasobRealPathZeStringa($nowyZasob->url);
	$this->assertTrue(file_exists($fullPath));
    }

    public function testWieleStron_pobierzUrlPodgladu() {

        $zasobPdf = $this->zasobyTable->getRekord(114);
        $this->zasobyTable->pobierzUrlPodgladu($zasobPdf);
        $nowyZasob = $this->zasobyTable->getRekord($zasobPdf->podglad_id);
        $fullPath = $this->zasobyTable->zasobRealPathZeStringa($nowyZasob->url);
	$this->assertTrue(file_exists($fullPath));
    }

    public function test_wyswietlMiniature() {
        $duzyObrazek = $this->zasobyTable->getRekord(93);
        $miniaturka = $duzyObrazek->pobierzMiniaturke();
        $path = \substr($miniaturka, 7);
        $fullPath = $this->zasobyTable->zasobRealPathZeStringa($path);
	$this->assertTrue(file_exists($fullPath));
    }

    public function test_wyswietlDuzaMiniature() {
        $duzyObrazek = $this->zasobyTable->getRekord(93);
        $miniaturka = $duzyObrazek->pobierzMiniaturke(100);
        $path = \substr($miniaturka, 7);
        $fullPath = $this->zasobyTable->zasobRealPathZeStringa($path);
	$this->assertTrue(file_exists($fullPath));

    }

}
