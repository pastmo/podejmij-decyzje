<?php

namespace ZasobyTest\Integracyjne;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zasoby\Model\Zasob;
use Zasoby\Menager\ZasobyUploadMenager;

class ZasobyKonwerterNapisowMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    protected $traceError = true;
    private $zasobyKonwerterNapisowMenager;
    private $fileOperator;

    const IMAGE_TYPE = 'image/png';
    const ORYGINAL_NAME = "name.jpg";

    public function setUp() {
	parent::setUp();

	$this->zasobyKonwerterNapisowMenager = $this->sm->get(\Pastmo\Zasoby\Menager\ZasobyKonwerterNapisowMenager::class);
    }

    public function test_konwertujZasobyZNapisu() {

	$napis = '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAJCAIAAACExCpEAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAwSURBVChTY3gro4IHES39/xMWNhG6kfWhIagEUAWmIqAIQgiiAg3hNBaC8ErLqAAAHHRzxxyBezYAAAAASUVORK5CYII=">';

	$wynik = $this->zasobyKonwerterNapisowMenager->konwertujZasobyZNapisu($napis);

	$zasob = $this->zasobyKonwerterNapisowMenager->getPoprzednioDodany();
	$expect = '!(file.png)[' . $zasob->id . ']';
	$this->assertEquals($expect, $wynik);
    }

    public function test_zapiszBase64PlikiZNapisu() {
	$plikZNapisu = \Pastmo\Zasoby\Menager\PlikZNapisu::create()
		->setBase64('iVBORw0KGgoAAAANSUhEUgAAAAoAAAAJCAIAAACExCpEAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAwSURBVChTY3gro4IHES39/xMWNhG6kfWhIagEUAWmIqAIQgiiAg3hNBaC8ErLqAAAHHRzxxyBezYAAAAASUVORK5CYII=')
		->setTyp('png');

	$this->zasobyKonwerterNapisowMenager->zapiszBase64PlikiZNapisu($plikZNapisu);

	$zasob = $this->zasobyKonwerterNapisowMenager->getPoprzednioDodany();

	$this->assertNotNull($zasob->id);
	$fullPath = $this->zasobyKonwerterNapisowMenager->zasobRealPathZeStringa($zasob->url);
	$this->assertTrue(file_exists($fullPath));
    }

}
