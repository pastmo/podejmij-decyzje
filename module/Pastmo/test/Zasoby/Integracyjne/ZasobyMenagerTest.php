<?php

namespace PastmoTest\Zasoby\Integracyjne;

use Zasoby\Menager\ZasobyUploadMenager;
use Pastmo\Testy\Util\TB;

class ZasobyMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $zasobyTable;

    public function setUp() {
	parent::setUp();
	$this->zasobyTable = new ZasobyUploadMenager($this->sm);
	$this->zalogowany = TB::create(\Logowanie\Model\Uzytkownik::class, $this)->setPF_IZP($this->sm)->make();
	$this->ustawZalogowanegoUsera($this->zalogowany->id);
    }

    public function test_ustawianie_dodany_przez() {
	$zasob = TB::create(\Zasoby\Model\Zasob::class, $this)->make();

	$this->assertEquals($this->zalogowany->id, $zasob->getDodanyPrzez()->id);
    }

}
