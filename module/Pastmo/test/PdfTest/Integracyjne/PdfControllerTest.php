<?php

class PdfControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

	public function testTest() {
		$this->obslugaKlasObcych->ustawZalogowanegoUsera(\Logowanie\Model\Uzytkownik::create(),
				1);

		Pastmo\Pdf\Menager\PdfMenager::$test = true;

		$this->dispatch('/pdf/test');

		$this->sprawdzCzyStatus200();
	}

	public function testIndex() {

	}

	public function getBigName() {

	}

	public function getSmallName() {

	}

}
