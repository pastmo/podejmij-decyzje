<?php

namespace PastmoTest\Uzytkownicy\Controller;

use Pastmo\Testy\Util\TB;
use Logowanie\Polecenie\WynikPrzetwarzaniaFormularza;
use Pastmo\Uzytkownicy\Entity\OdpowiedzLogowania;

class LogowanieControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    protected $traceError = true;
    private $msg = "wiadomość komunikat";

    const wiadomosc = "Wiadomośc przetwarzania formularza";
    const sukces = 'sukces';
    const nie_sukces = 'nie_sukces';
    const post = 'post';
    const nie_post = 'nie_post';
    const menager = 'wywolywany menager';
    const nie_menager = 'nie_menager menager';
    const msg = 'zawiera_msg';
    const nie_msg = 'nie_msg';
    const zawiera_form = 'zawiera_form';
    const nie_form = 'nie_form';

    private $zalogowany;

    public function setUp() {
	parent::setUp();
    }

    public function testPrzekierowaniePrzyNiezalogwanym() {
	//Ten test w tym przypadku nie ma sensu:)
    }

    public function testlogowanieAction() {
	$post = ['uzytkownik' => ['login' => 'login', 'haslo' => 'abc'], 'redirect' => 'logowanie'];
	$this->ustawParametryPosta($post);
	$this->obslugaKlasObcych->ustawZalogowanegoUsera();
	$this->uzytkownikTable->method('zaloguj')->willReturn(
		OdpowiedzLogowania::create()->setUzytkownik($this->obslugaKlasObcych->zalogowany));

	$this->dispatch('/logowanie/logowanie');
	$this->sprawdzPrzekierowanie('logowanie');
    }

    public function testlogowanieAction_wyswietlanieKomunikatow() {
	$post = ['uzytkownik' => ['login' => 'login', 'haslo' => 'abc'], 'redirect' => 'logowanie'];
	$this->ustawParametryPosta($post);
	$this->obslugaKlasObcych->ustawZalogowanegoUsera();
	$this->uzytkownikTable->method('zaloguj')->willReturn(
		OdpowiedzLogowania::create()->setCzySukces(false)->setMsg("komunikat o błędzie logowania"));

	$this->dispatch('/logowanie/logowanie');
//	$this->sprawdzBodyZawiera("komunikat o błędzie logowania");//TODO_TEST: Podpiąć jakieś sprawdzanie
    }

    public function testrejestracjaAction() {
	$this->dispatch('/logowanie/rejestracja');
	$this->sprawdzCzyStatus200();
    }

    public function testrejestracjaAction_rejestraja() {

	$this->ustawParametryPosta(array('imie' => 'imie'));

	$this->sprawdzWywolanieRejestracji();

	$this->dispatch('/logowanie/rejestracja');

	$this->sprawdzPrzekierowanie('/logowanie/po_rejestracji');
    }

    public function testrejestracjaAction_rejestraja_false() {

	$this->ustawParametryPosta(array('imie' => 'imie'));

	$this->sprawdzWywolanieRejestracji(false);

	$this->dispatch('/logowanie/rejestracja');

	$this->sprawdzCzyStatus200();
    }

    public function test_aktywujAction() {
	$this->sprawdzWywolanieAktywacji();

	$this->dispatch('/logowanie/aktywuj?id=1&kod=c13fde8c21332db79e4f057dcd518e48');

	$this->sprawdzCzyStatus200();
    }

    public function test_resetHaslaAction_bezPosta() {
	$this->wykonajTestFormularza('/logowanie/reset_hasla', 'wyslijPotwierdzeniePrzypominaniaHasla',
		self::nie_sukces, self::nie_post, self::nie_menager, self::nie_msg, self::zawiera_form);
    }

    public function test_resetHaslaAction_postSukces() {
	$this->wykonajTestFormularza('/logowanie/reset_hasla', 'wyslijPotwierdzeniePrzypominaniaHasla', self::sukces,
		self::post, self::menager, self::msg, self::nie_form);
    }

    public function test_resetHaslaAction_postFail() {
	$this->wykonajTestFormularza('/logowanie/reset_hasla', 'wyslijPotwierdzeniePrzypominaniaHasla',
		self::nie_sukces, self::post, self::menager, self::msg, self::zawiera_form);
    }

    public function test_resetuj() {
	$this->wykonajTestFormularza('/logowanie/resetuj?id=1&kod=3da41bc0b56dc354d2765805335a8df3',
		'sprzawdzZadaniePrzypomnieniaHasla', self::sukces, self::nie_post, self::menager, self::msg,
		self::zawiera_form);
    }

    public function test_resetuj_fail() {
	$this->wykonajTestFormularza('/logowanie/resetuj?id=1&kod=3da41bc0b56dc354d2765805335a8df3',
		'sprzawdzZadaniePrzypomnieniaHasla', self::nie_sukces, self::nie_post, self::menager, self::msg,
		self::nie_form);
    }

    public function test_resetuj_post_sukces() {
	$this->wykonajTestFormularza('/logowanie/resetuj?id=1&kod=3da41bc0b56dc354d2765805335a8df3',
		'zmienHasloZPrzypomnienia', self::sukces, self::post, self::menager, self::msg, self::nie_form);
    }

    public function test_resetuj_post_fail() {
	$this->wykonajTestFormularza('/logowanie/resetuj?id=1&kod=3da41bc0b56dc354d2765805335a8df3',
		'zmienHasloZPrzypomnienia', self::nie_sukces, self::post, self::menager, self::msg, self::nie_form);
    }

    public function test_resetuj_brak_geta() {
	$this->wykonajTestFormularza('/logowanie/resetuj', 'sprzawdzZadaniePrzypomnieniaHasla', self::nie_sukces,
		self::nie_post, self::nie_menager, self::nie_msg, self::nie_form);
    }

    public function test_nowaRolaAction() {
	$this->ustawParametryPosta(array('nazwa' => 'nowa rola'));
	$this->roleMenager->expects($this->once())->method('dodajRole');

	$this->dispatch("/logowanie/nowa_rola");
	$this->sprawdzPrzekierowanie('/logowanie/role');
    }

    public function test_ustawieniaAction() {
	$this->obslugaKlasObcych->ustawZalogowanegoUsera();
	$this->uzytkownicyKontaMailoweMenager->expects($this->once())->method('pobierzDlaZalogowanegoUzytkownika')->willReturn([]);
	$this->uzytkownicyKontaMailoweMenager->expects($this->once())->method('sprawdzPoprawnoscKontMailowych')->willReturn((object) ['wynikDlaKont' => []]);

	$this->dispatch("/logowanie/ustawienia");
	$this->sprawdzCzyStatus200();
    }

    private function wykonajTestFormularza($link, $metoda, $czySukcesStr, $czyPostStr, $czyWywolywanyMenager,
	    $msgStr, $formStr) {
	$czyPost = $czyPostStr === self::post;
	$ileRazySprawdzac = $czyWywolywanyMenager === self::menager ? 1 : 0;
	$czySukces = $czySukcesStr === self::sukces ? true : false;
	$czyMsg = $msgStr === self::msg ? true : false;
	$czyForm = $formStr === self::zawiera_form ? true : false;

	if ($czyPost) {
	    $this->ustawParametryPosta(array());
	}

	$this->ustawWynikPrzypominania($czySukces, $this->msg, $ileRazySprawdzac, $metoda);

	$this->dispatch($link);

	$this->sprawdzCzyStatus200();

	if ($czyMsg) {
	    $this->sprawdzOdpowiedzZawiera($this->msg);
	} else {
	    $this->sprawdzOdpowiedzNieZawiera($this->msg);
	}

	if ($czyForm) {
	    $this->sprawdzOdpowiedzZawiera('<form');
	} else {
	    $this->sprawdzOdpowiedzNieZawiera('<form');
	}
    }

    public function getBigName() {
	return 'Logowanie';
    }

    public function getSmallName() {
	return 'logowanie';
    }

    protected function sprawdzWywolanieRejestracji($czySukces = true) {
	$this->rejestracjaMenager->expects($this->exactly(1))->method('zarejestruj')->willReturn(new WynikPrzetwarzaniaFormularza($czySukces,
		self::wiadomosc));
    }

    protected function sprawdzWywolanieAktywacji() {
	$this->aktywacjaMenager->expects($this->exactly(1))->method('aktywujKonto')
		->will($this->returnValue(new \Logowanie\Polecenie\WynikPrzetwarzaniaFormularza(true, "sukces")));
    }

    protected function ustawWynikPrzypominania($sukces, $komunikat, $ileRazy, $metoda) {
	$wynik = new \Logowanie\Polecenie\WynikPrzetwarzaniaFormularza($sukces, $komunikat);
	$this->przypominanieHaslaMenager
		->expects($this->exactly($ileRazy))
		->method($metoda)
		->willReturn($wynik);
    }

}
