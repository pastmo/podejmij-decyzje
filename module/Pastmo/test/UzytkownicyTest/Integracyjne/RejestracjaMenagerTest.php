<?php

namespace Pastmo\Uzytkownicy\Integracyjne;

use Logowanie\Model\Uzytkownik;
use Pastmo\Testy\Util\TB;

class RejestracjaMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    protected $traceError = true;
    private $rejestracjaMenager;
    private $aktywacjaTable;
    private $infoDoAdmina;
    private $uzytkownikTable;

    public function setUp() {
	parent::setUp();

	$this->sm->setAllowOverride(true);
	$this->aktywacjaTable = $this->getMockBuilder(\Logowanie\Model\AktywacjaMail::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->infoDoAdmina = $this->getMockBuilder(\Logowanie\Model\InfoDoAdminaMail::class
		)
		->disableOriginalConstructor()
		->getMock();

	$this->sm->setService(\Logowanie\Model\AktywacjaMail::class, $this->aktywacjaTable);
	$this->sm->setService(\Logowanie\Model\InfoDoAdminaMail::class, $this->infoDoAdmina);

	$this->rejestracjaMenager = $this->sm->get(\Logowanie\Menager\RejestracjaMenager::class);
	$this->uzytkownikTable = $this->sm->get(\Logowanie\Menager\UzytkownicyMenager::class);
    }

    public function test_zarejestrujUzytkownika() {
	$uzytkownik = TB::create(\Pastmo\Uzytkownicy\Entity\Uzytkownik::class, $this)
			->setParameters(array('haslo' => null))
			->setPF_IBN($this->sm)->make();
	$wynik = $this->rejestracjaMenager->zarejestrujUzytkownika($uzytkownik);

	$this->assertTrue($wynik->isSuccess, $wynik->message);

	$ostatniRekord = $this->uzytkownikTable->getPoprzednioDodany();

	$this->assertEquals($ostatniRekord->jezyk_kod, \Pastmo\Uzytkownicy\Enumy\KodyJezykow::DOMYSLNY);
	$this->assertEquals($ostatniRekord->rola->kod, \Logowanie\Entity\KodyRol::DOMYSLNIE);
	$this->assertNotNull($ostatniRekord->data_dodania);
    }

}
