<?php

namespace PastmoTest\Uzytkownicy\Integracyjne;

use Logowanie\Model\Uzytkownik;
use Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane;
use Pastmo\Testy\Util\TB;

class UzytkownicyMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    protected $traceError = true;
    private $uzytkownicyMenager;
    public $slownik;

    public function setUp() {
	parent::setUp();

	$this->uzytkownicyMenager = $this->sm->get(\Logowanie\Menager\UzytkownicyMenager::class);
    }

    public function test_dodajRekord() {
	$nazwa = 'test_dodajRekord';

	$uzytkownik = new Uzytkownik();
	$uzytkownik->login = $nazwa;
	$this->uzytkownicyMenager->zapisz($uzytkownik);

	$ostatniRekord = $this->uzytkownicyMenager->getPoprzednioDodany();

	$this->assertEquals($ostatniRekord->login, $nazwa);
	$this->assertEquals($ostatniRekord->jezyk_kod, \Pastmo\Uzytkownicy\Enumy\KodyJezykow::DOMYSLNY);
    }

    public function test_dodajRekord_laczenie_ze_slownikiem() {
	$wynik = TB::create(Uzytkownik::class,$this)->addOneToMany('slownik',
			TB::create(\Pastmo\Tlumaczenia\Entity\Slownik::class,$this)->setResultField('slownik'))->make();

	$this->assertEquals($this->slownik->id, $wynik->slownik->id);

    }

    public function test_zapisz_ustawianie_typu_z_roli() {
	$nazwa = 'test_dodajRekord';

	$rola = \FabrykaRekordow::makeEncje(\Logowanie\Entity\Rola::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm)
				->setparametry(array('typ' => \Logowanie\Model\TypyUzytkownikow::PRACOWNIK)));

	$uzytkownik = new Uzytkownik();
	$uzytkownik->login = $nazwa;
	$uzytkownik->rola = $rola->id;
	$this->uzytkownicyMenager->zapisz($uzytkownik);

	$ostatniRekord = $this->uzytkownicyMenager->getPoprzednioDodany();

	$this->assertEquals($ostatniRekord->login, $nazwa);
	$this->assertEquals($ostatniRekord->typ, \Logowanie\Model\TypyUzytkownikow::PRACOWNIK);
    }

    public function test_pobierzUzytkownikowZUprawnieniamiBezZalogowanego() {
	$this->ustawZalogowanegoUseraId1();
	$uzytkownicy = $this->uzytkownicyMenager->pobierzUzytkownikowZUprawnieniamiBezZalogowanego(\Logowanie\Enumy\KodyUprawnien::UPRAWNIENIA_PRACOWNIKA);

	$this->assertTrue(count($uzytkownicy) > 0);

	foreach ($uzytkownicy as $uzytkownik) {
	    $this->assertNotSame('1', $uzytkownik->id);
	}
    }

    public function test_zapiszZPosta_sprawdzanie_branu_napisywania_nieedytowanych_kolumn() {

	$uzytkownik = \FabrykaRekordow::makeEncje(Uzytkownik::class,
			PFIntegracyjneZapisPowiazane::create($this->sm)
				->setparametry(array('czy_aktywny' => true)));

	$post = array('id' => $uzytkownik->id, 'imie' => 'nowe imie');

	$this->uzytkownicyMenager->zapiszZPosta($post);

	$zmodyfikowany = $this->uzytkownicyMenager->getRekord($uzytkownik->id);

	$this->assertEquals('nowe imie', $zmodyfikowany->imie);
	$this->assertEquals(true, $zmodyfikowany->czy_aktywny);
    }

}
