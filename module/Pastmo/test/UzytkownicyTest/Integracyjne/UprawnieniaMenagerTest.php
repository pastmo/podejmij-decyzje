<?php

namespace PastmoTest\Uzytkownicy\Integracyjne;

use Logowanie\Enumy\KodyUprawnien;

class UprawnieniaMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $uprawnieniaMenager;

    public function setUp() {
	parent::setUp();

	$this->uprawnieniaMenager = new \Pastmo\Uzytkownicy\Menager\UprawnieniaMenager($this->sm);
    }

    public function test_pobierzUprawnieniePoKodzie() {
	$wynik = $this->uprawnieniaMenager->pobierzUprawnieniePoKodzie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE);
	$this->assertEquals(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, $wynik->kod);
    }

    public function test_pobierzUprawnieniePoKodzie_brak() {
	$wynik = $this->uprawnieniaMenager->pobierzUprawnieniePoKodzie("brak uprawnienia");
	$this->assertEquals("brak uprawnienia", $wynik->opis);
    }

    public function test_pobierzUprawnieniaDlaRoli() {
	$wynik = $this->uprawnieniaMenager->pobierzUprawnieniaDlaRoli(null);
	$this->assertEquals([], $wynik);
    }

}
