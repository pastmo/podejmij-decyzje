<?php

namespace Pastmo\WikiTest\Integracyjne;

use Pastmo\Testy\Util\TB;

class WikiArtykulyHistoriaMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public function setUp() {
	parent::setUp();
    }

    public function testDodajEncje() {

	$tytul = "testowy tytul historyczny";
	$tresc = "testowa tresc testowa tresc historyczna";

	$encja = TB::create(\Pastmo\Wiki\Entity\WikiArtykulHistoria::class, $this)
		->setPF_IZP($this->sm)
		->setParameters(array('tytul' => $tytul,
				      'tresc' => $tresc))
		->make();

	$this->assertNotNull($encja->id);
	$this->assertEquals($tytul, $encja->tytul);
	$this->assertEquals($tresc, $encja->tresc);
    }

}
