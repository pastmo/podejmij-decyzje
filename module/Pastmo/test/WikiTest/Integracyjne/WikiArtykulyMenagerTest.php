<?php

namespace Pastmo\WikiTest\Integracyjne;

use Pastmo\Testy\Util\TB;

class WikiArtykulyMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $wikiArtykulyMenager;
    private $wikiArtykulyHistoriaMenager;

    public function setUp() {
	parent::setUp();
	$this->wikiArtykulyMenager = $this->sm->get(\Pastmo\Wiki\Menager\WikiArtykulyMenager::class);
	$this->wikiArtykulyHistoriaMenager = $this->sm->get(\Pastmo\Wiki\Menager\WikiArtykulyHistoriaMenager::class);

	$this->zalogowany = TB::create(\Logowanie\Model\Uzytkownik::class, $this)->make();
	$this->ustawZalogowanegoUsera($this->zalogowany->id);
    }

    public function testDodajEncje() {
	$tytul = "testowy tytul";
	$tresc = "testowa tresc testowa tresc";

	$encja = TB::create(\Pastmo\Wiki\Entity\WikiArtykul::class, $this)
		->setParameters(array('tytul' => $tytul,
			'tresc' => $tresc))
		->make();

	$this->assertNotNull($encja->id);
	$this->assertEquals($tytul, $encja->tytul);
	$this->assertEquals($tresc, $encja->tresc);
    }

    public function testTworzeniaNowegoArtkulu_aktualizujLubUtworzArtykul() {

	$post = ['autor_id' => '', 'tytul' => __CLASS__, 'tresc' => __METHOD__, 'avatar_id' => '', 'id' => ''];
	$parameters = new \Zend\Stdlib\Parameters($post);
	$this->wikiArtykulyMenager->aktualizujLubUtworzArtykul($parameters);

	$wynik = $this->wikiArtykulyMenager->getPoprzednioDodany();

	$this->assertEquals(__CLASS__, $wynik->tytul);
    }

    //TODO_TEST: Czasami test wysypuje się z informacją: -'Artykul' +'Artykuł1'
    public function t_estEdycjiArtkulu_aktualizujLubUtworzArtykul() {

	$post = ['tytul' => "Artykul", 'id' => 1];
	$parameters = new \Zend\Stdlib\Parameters($post);
	$this->wikiArtykulyMenager->aktualizujLubUtworzArtykul($parameters);

	$wynik = $this->wikiArtykulyMenager->getRekord(1);
	$wynikHistoria = $this->wikiArtykulyHistoriaMenager->getPoprzednioDodany();

	$this->assertEquals($wynik->tytul, $wynikHistoria->tytul);
    }
}
