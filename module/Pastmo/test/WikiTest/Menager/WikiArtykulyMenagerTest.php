<?php

namespace WikiTest\Menager;

use \Pastmo\Wiki\Entity\WikiArtykul;
use Pastmo\Testy\Util\TB;
use \Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane;
use \Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuNiepowiazane;

class WikiArtykulyMenagerTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    public function setUp() {
	parent::setUp();
	$this->ustawMockaGateway(\Pastmo\Wiki\KonfiguracjaModulu::WIKI_GATEWAY);
	$this->wikiArtykulyMenager = new \Pastmo\Wiki\Menager\WikiArtykulyMenager($this->sm);
    }

    public function testCzystegoArtkulu_pobierzArtykul() {

	$this->stworzArtykul(array());
	$this->obslugaTestowanejKlasy->ustawMockiDoPobierzZWherem([$this->artykul]);
	$wynik = $this->wikiArtykulyMenager->pobierzArtykul(42, 'pl');
	$this->assertEquals($wynik->id, $this->artykul->id);
    }

    public function testPusta_pobierzListeArtykulowPowiazanych () {

	$this->stworzArtykul(array());
	$this->obslugaTestowanejKlasy->ustawMockiDoPobierzZWherem([$this->artykul]);
	$wynik = $this->wikiArtykulyMenager->pobierzListeArtykulowPowiazanych($this->artykul, 'pl');
	$spodziewany = [0 => $this->artykul];
	$this->assertEquals($spodziewany, $wynik);
    }

    public function testPelna_pobierzListeArtykulowPowiazanych () {

	$artykul = $this->stworzArtykul(['id_tematu' => 1, 'jezyk_kod' => 'pl_PL']);
	$this->obslugaTestowanejKlasy->ustawMockiDoPobierzZWherem([$this->artykul]);
	$wynik = $this->wikiArtykulyMenager->pobierzListeArtykulowPowiazanych($this->artykul, 'pl');
	$this->assertEquals([$this->artykul], $wynik);
    }

    private function stworzArtykul($parametry) {

	$this->artykul = TB::create(\Pastmo\Wiki\Entity\WikiArtykul::class, $this)
	    ->setPF_MP($this->sm)
	    ->setParameters($parametry)
	    ->make();
    }
}
