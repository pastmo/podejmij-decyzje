<?php

namespace PastmoTest\LokalizacjeTest\Integracyjne;

class KrajeMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    protected $traceError = true;
    private $krajeMenager;

    public function setUp() {
	parent::setUp();

	$this->krajeMenager = $this->sm->get(\Pastmo\Lokalizacje\Menager\KrajeMenager::class);
    }

    public function test_pobierzWszystkieKraje() {
	$kraje = $this->krajeMenager->zwrocTablicePanstw();

	$this->assertGreaterThan(0, count($kraje));
    }

    public function test_pobierzKraj() {
	$kraj = $this->krajeMenager->pobierzKraj('pl');

	$this->assertEquals('pl', $kraj->kod);
    }

}
