<?php

namespace PastmoTest\Wspolne\Menager;

class ContainerBlednejSesjiTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    private $container;

    public function setUp() {
	$this->container = new \Pastmo\Sesja\Entity\ContainerBlednejSesji("nothingImportant");
    }

    public function test_offsetSet() {
	try {

	    $this->container->offsetSet('klucz', 'asdhj');
	} catch (\Throwable $e) {
	    $this->fail($e->getMessage());
	}
    }

    public function test_offsetExists() {

	$wynik = $this->container->offsetExists('klucz');
	$this->assertFalse($wynik);
    }

    public function test_offsetGet() {

	$wynik = $this->container->offsetGet('klucz');
	$this->assertNull($wynik);
    }

    public function test_offsetUnset() {

	$wynik = $this->container->offsetUnset('klucz');
    }

}
