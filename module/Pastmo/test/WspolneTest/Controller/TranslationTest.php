<?php

namespace PastmoTest\Wspolne\Controller;

use Zend\I18n\Translator\Translator;
use Pastmo\Testy\Util\TB;

class TranslationTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    private $user;
    private $translator;

    public function setUp() {
	parent::setUp();
	$translator = $this->sm->get('MvcTranslator');
	$translator->setLocale(\Pastmo\Uzytkownicy\Enumy\KodyJezykow::ANGIELSKI);
    }

    public function test_czystyTranslator() {
	$this->ustawTranslator();
	$tekst = $this->translator->translate("Polski tekst", "default", 'en_US');
	$this->assertEquals("English text", $tekst);
    }

    public function test_translateAction_niezalogowany() {
	$this->dispatch("/tests/translate");
	$this->sprawdzBodyZawiera("English text");
    }

    public function test_menagera() {
	$menager = new \Logowanie\Menager\UzytkownicyMenager($this->sm);

	$tekst = $menager->_translate("Polski tekst", false, "Polski tekst");
	$this->sprawdzStringZawiera($tekst, "English text");
    }

    public function test_menagera_bez_sm() {
	$menager = new \Zasoby\Menager\ZasobyUploadMenager();

	$tekst = $menager->_translate("Polski tekst", '', "Polski tekst");
	$this->sprawdzStringZawiera($tekst, "Polski tekst");
    }

    public function test_helpera() {
	$helper = new \Pastmo\Wspolne\Helper\TranslateHelper($this->sm);

	$tekst = $helper->_translate("Polski tekst", '', "Polski tekst");
	$this->sprawdzStringZawiera($tekst, "English text");
    }

    public function test_controlera() {
	$pastmoTranslator = $this->sm->get(\Pastmo\Tlumaczenia\Menager\PastmoTranslator::class);
	$init = new \Wspolne\Controller\Factory\InitObiekt();
	$init->translator = $pastmoTranslator;

	$controler = new \Wspolne\Controller\TestsController($this->sm);
	$controler->init($init);

	$tekst = $controler->_translate("Polski tekst", 'default', "Polski tekst");
	$this->sprawdzStringZawiera($tekst, "English text");
    }

    public function test_translateConst_menagera() {
	$menager = new \Zasoby\Menager\ZasobyUploadMenager();

	$tekst = $menager->translateConst(\Projekty\Enumy\KrokiMiloweTypy::NEGOCJACJE_CENOWE);
	$this->assertEquals("Negocjacje cenowe", $tekst);
    }

    public function test_translateConst_controlera() {
	$this->ustawTranslator();
	$init = new \Wspolne\Controller\Factory\InitObiekt();
	$init->constantTranslator = $this->sm->get(\Lokalizacje\Menager\ConstantTranslatorMenager::class);

	$controler = new \Wspolne\Controller\TestsController($this->sm);
	$controler->init($init);

	$tekst = $controler->translateConst(\Projekty\Enumy\KrokiMiloweTypy::NEGOCJACJE_CENOWE);
	$this->sprawdzStringZawiera($tekst, "Price negotiations");
    }

    public function test_translateConst_helpera() {
	$helper = new \Pastmo\Wspolne\Helper\TranslateHelper($this->sm);

	$tekst = $helper->translateConst(\Projekty\Enumy\KrokiMiloweTypy::NEGOCJACJE_CENOWE);
	$this->sprawdzStringZawiera($tekst, "Price negotiations");
    }

    /** TODO_TEST: Sprawdzić dlaczego test normalnie przechodzi a wysypuje się jak są uruchamiane wszystkie testy
     * @requires PHP 42
     */
    public function test_helperaDoLokalizacjiJs() {
	$this->dispatch("/tests/sam_layout");
	$this->sprawdzBodyZawiera("locale = 'en';");
    }

    //TODO::onBootstrap w Application Module wywołuje się przed testami, więc poniższe dwa testy nie działają.
    public function t_est_translateAction_zalogowany_en() {
	$this->obslugaKlasObcych->ustawZalogowanegoUsera($this->user);

	$this->dispatch("/tests/translate");
	$this->sprawdzBodyZawiera("English text");
    }

    public function t_est_translateAction_zalogowany_nbl() {
	$this->user->jezyk_kod = "Nibylandia";
	$this->obslugaKlasObcych->ustawZalogowanegoUsera($this->user);

	$this->dispatch("/tests/translate");
	$this->sprawdzBodyZawiera("Polski tekst");
    }

    private function ustawTranslator() {
	$this->translator = new Translator();
	$this->translator->setLocale("en_US");
	$this->translator->addTranslationFile('gettext', __DIR__ . "../../../../../../data/languages/en_US.mo",
		"default", 'en_US');
    }

    public function testIndex() {

    }

    public function testPrzekierowaniePrzyNiezalogwanym() {

    }

}
