<?php

namespace PastmoTest\Testy\Controller;

use Application\Controller\LadowaczSkryptow;

class LadowaczSkryptowTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    public function setUp() {
	parent::setUp();
	LadowaczSkryptow::$skrypty = [];
	LadowaczSkryptow::$cssy = [];
	LadowaczSkryptow::$parametryCss = [];
	LadowaczSkryptow::$parametrySkryptow = [];
    }

    public function testIndex() {

    }

    public function test_add_zwykle_wyswietlanie() {
	LadowaczSkryptow::add('jakis skrypt.js', 'FunkcjaJS');
	LadowaczSkryptow::wyswietl();

	$this->expectOutputString('<script src="jakis skrypt.js?wersja=' . LadowaczSkryptow::WERSJA . '"></script>'.PHP_EOL);
    }

    public function test_add__podwojne_dodanie_tego_samego() {
	LadowaczSkryptow::add('jakis skrypt.js', 'FunkcjaJS');
	LadowaczSkryptow::add('jakis skrypt.js', 'FunkcjaJS');
	LadowaczSkryptow::wyswietl();

	$this->expectOutputString('<script src="jakis skrypt.js?wersja=' . LadowaczSkryptow::WERSJA . '"></script>'.PHP_EOL);
    }

    public function test_pobierzJakoTabliceJson() {
	LadowaczSkryptow::add('jakis skrypt.js', 'FunkcjaJS');
	$wynik = LadowaczSkryptow::pobierzJakoTabliceJson();

	$this->assertEquals('[{"url":"jakis skrypt.js?wersja=' . LadowaczSkryptow::WERSJA . '","funkcja_js":"FunkcjaJS"}]',$wynik);
    }

    public function test_wyswietlCssy(){
	LadowaczSkryptow::addCss('style.css');
	LadowaczSkryptow::wyswietlCssy();

	$this->expectOutputString('<link href="style.css?wersja=' . LadowaczSkryptow::WERSJA . '" rel="stylesheet" >'.PHP_EOL);

    }

    public function test_wyswietlCssy_parametry(){
	LadowaczSkryptow::addCss('style.css','media="print"');
	LadowaczSkryptow::wyswietlCssy();

	$this->expectOutputString('<link href="style.css?wersja=' . LadowaczSkryptow::WERSJA . '" rel="stylesheet" media="print">'.PHP_EOL);

    }

}
