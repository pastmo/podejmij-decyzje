<?php

namespace PastmoTest\Testy\Controller;

class PastmoWspolneAjaxControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    public function setUp() {
	parent::setUp();
    }

    public function testIndex() {
    }

    public function test_zapiszWielkoscEkranuAction() {
	$this->ustawParametryPosta(['szerokosc'=>42]);
	$this->sesjaMenager->expects($this->once())->method('zapiszSzerokoscEkranu');

	$this->dispatch('/pastmo_wspolne_ajax/zapisz_wielkosc_ekranu');
	
	$this->sprawdzCzySukces();
    }

}
