<?php

namespace PastmoTest\Wspolne\Helper;

class WyswietlWynikowaTrescWysiwygaTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $wyswietlWynikowaTrescWysiwyga;
    private $viewMock;

    public function setUp() {
	parent::setUp();
	$this->wyswietlWynikowaTrescWysiwyga = new \Pastmo\Wspolne\Helper\WyswietlWynikowaTrescWysiwyga($this->sm);
	$this->viewMock = new \Pastmo\Testy\Mocki\ViewMock();
    }

    public function test_wyswietlWynikowaTrescWysiwyga_brak_dowiazanych_zasobow() {
	$expected = "Zwykła treść, która nie powinna być zmieniona";
	$wynik = $this->wyswietlWynikowaTrescWysiwyga->__invoke($expected, $this->viewMock);

	$this->assertEquals($expected, $wynik);
    }

    public function test_wyswietlWynikowaTrescWysiwyga_z_zasobami() {
	$input = 'Oto link!(nazwa.jpg)[42]';
	$expect = 'Oto link<img src="obrazek">';

	$this->zasobyUploadMenager
		->expects($this->once())
		->method('wyswietlUrlObrazkaZBasePathPoId')
		->willReturn('obrazek')
		->with($this->equalTo(42));
	$wynik = $this->wyswietlWynikowaTrescWysiwyga->__invoke($input, $this->viewMock);

	$this->assertEquals($expect, $wynik);
    }

}
