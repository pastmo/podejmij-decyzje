<?php

namespace PastmoTest\Wspolne\Util;

use Pastmo\Wspolne\Utils\StrUtil;

class StrUtilTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_skrocStr() {
	$input = "abcdef";
	$expected = "abcde";

	$result = \Wspolne\Utils\StrUtil::skrocStr($input, 1);
	$this->assertEquals($expected, $result);
    }

    public function test_lamLinie() {
	$input = "serwis drzwi wejściowych
Proszę również";
	$expected = "serwis drzwi wejści
owych
Proszę również
";

	$result = \Wspolne\Utils\StrUtil::lamLinie($input, 20);
	$this->assertEquals($expected, $result);
    }

    public function test_drukujCene() {
	$input = "22";
	$wynik = StrUtil::drukujCene($input);

	$this->assertEquals('22,00', $wynik);
    }

    public function test_drukujCene_wejscie_z_kropka() {
	$input = "142.8864";
	$wynik = StrUtil::drukujCene($input);

	$this->assertEquals('142,89', $wynik);
    }

    public function test_drukujCene_duze_kwoty() {
	$input = "142142142";
	$wynik = StrUtil::drukujCene($input);

	$this->assertEquals('142 142 142,00', $wynik);
    }

    public function testpolaczDateZCzasem() {
	$data = "2016-08-16";
	$czas = "16:33";

	$wynik = StrUtil::polaczDateZCzasem($data, $czas);

	$this->assertEquals("2016-08-16 16:33:00", $wynik);
    }

    public function testdrukujSamaDate() {
	$data = "2016-08-16 16:33:00";

	$wynik = StrUtil::drukujSamaDate($data);

	$this->assertEquals("2016-08-16", $wynik);
    }

    public function testdrukujSamaDateNull() {
	$data = null;

	$wynik = StrUtil::drukujSamaDateNull($data);

	$this->assertEquals(null, $wynik);
    }

    public function testdrukujGodzinyMinuty() {
	$data = "2016-08-16 16:33:00";

	$wynik = StrUtil::drukujGodzinyMinuty($data);

	$this->assertEquals("16:33", $wynik);
    }

    public function testdrukujGodzinyMinutyNull() {
	$data = null;

	$wynik = StrUtil::drukujGodzinyMinutyNull($data);

	$this->assertEquals(null, $wynik);
    }

    public function testdateTimeBezSekund() {
	$data = "2016-08-16 16:33:00";

	$wynik = StrUtil::dateTimeBezSekund($data);

	$this->assertEquals("2016-08-16 16:33", $wynik);
    }

    public function testusunNiedrukowalneZnaki_spacje() {
	$wynik = StrUtil::usunNiedrukowalneZnaki(" napis");
	$this->assertEquals('napis', $wynik);
    }

    public function testusunNiedrukowalneZnaki_entery() {
	$wynik = StrUtil::usunNiedrukowalneZnaki("\nnapis" . PHP_EOL);
	$this->assertEquals('napis', $wynik);
    }

    public function testusunNiedrukowalneZnaki_tabulatory() {
	$wynik = StrUtil::usunNiedrukowalneZnaki("\tnapis");
	$this->assertEquals('napis', $wynik);
    }

    public function test_ulamekNaProcenty() {
	$wynik = StrUtil::ulamekNaProcenty(0.5);
	$this->assertEquals('50,00%', $wynik);
    }

    public function test_from_array() {
	$input = array(1, '2', 3);

	$wynik = StrUtil::fromArray($input);

	$this->assertEquals('1,2,3', $wynik);
    }

    public function test_fromArrayToSql() {
	$input = array(1, '2', 'napis');

	$wynik = StrUtil::fromArrayToSql($input);

	$this->assertEquals("1,2,'napis'", $wynik);
    }

    public function test_fromArrayToSql_empty() {
	$input = array();

	$wynik = StrUtil::fromArrayToSql($input);

	if ($wynik) {
	    $this->fail("Wynik powinien być interpreteowany jako false");
	}
    }

    public function test_fromArrayToSql_kolumny() {
	$input = array('napis1', 'napis2');

	$wynik = StrUtil::fromArrayToSql($input, false);

	$this->assertEquals("napis1,napis2", $wynik);
    }

    public function test_dodajZnacznikiOdpowiedziMaila() {
	$tresc = "abc\n\ncdef";
	$expected = "> abc\n> \n> cdef";

	$wynik = StrUtil::dodajZnacznikiOdpowiedziMaila($tresc);
	$this->assertEquals($expected, $wynik);
    }

    public function test_nazwaAkcjiNaRoute() {
	$tresc = "PrzykladowaAkcjaAction";
	$expected = "przykladowa_akcja";

	$wynik = StrUtil::nazwaAkcjiNaRoute($tresc);
	$this->assertEquals($expected, $wynik);
    }

    public function test_klasaNaRoute() {
	$tresc = \Wspolne\Controller\AjaxController::class;
	$expected = "ajax";

	$wynik = StrUtil::klasaNaRoute($tresc);
	$this->assertEquals($expected, $wynik);
    }

    public function test_usunZnakiNienadajaceSieNaRozszerzeniePliku() {
	$input = ".as5\t?>^&*";
	$expected = ".as5";

	$result = StrUtil::usunZnakiNienadajaceSieNaRozszerzeniePliku($input);

	$this->assertEquals($expected, $result);
    }

    public function test_is_base64_encoded() {
	$base64 = base64_encode("zakodowany napis");
	$this->assertTrue(StrUtil::is_base64_encoded($base64));
	$this->assertFalse(StrUtil::is_base64_encoded("$base64 coś"));
    }

    public function test_base64_decode_niekodowane() {
	$input = "Treść niekodowana";
	$result = StrUtil::base64_decode($input);
	$this->assertEquals($input, $result);
    }

    public function test_base64_decode_kodowane() {
	$niekodowany = "Treść niekodowana";
	$kodowany = base64_encode($niekodowany);

	$result = StrUtil::base64_decode($kodowany);
	$this->assertEquals($niekodowany, $result);
    }

    public function test_base64_email() {
	$kodowany = 'IGN6YXJueW0gdGxlICAKCgpQb3pkcmF3aWFtLAoKCgpQcnplbXlzxYJhdyBLxYJvc293c2tpCgpH
cmFmaWsKCgoKRTogW3Aua2xvc293c2tpQGtyaXNwb2wucGxdKGh0dHBzOi8vcG9jenRhLmtyaXNw';

	$result = StrUtil::base64_decode($kodowany);
	$this->assertEquals(base64_decode($kodowany), $result);
    }

    public function test_stringZawiera_true() {
	$string = "dfg";
	$kontener = "asdfghi";

	$result = StrUtil::stringZawiera($kontener, $string);
	$this->assertTrue($result);
    }

    public function test_stringZawiera_false() {
	$string = "dfg";
	$kontener = "inny napis";

	$result = StrUtil::stringZawiera($kontener, $string);
	$this->assertFalse($result);
    }

    public function test_stringZawiera_ofset() {
	$string = "dfg";
	$kontener = "dfginny napis";

	$result = StrUtil::stringZawiera($kontener, $string, 1);
	$this->assertFalse($result);
    }

    public function test_substr() {
	$kontener = "[aaaa[bbb'o";

	$result = StrUtil::substr('[', $kontener, "'", 1);
	$this->assertEquals('bbb', $result);
    }

    public function test_substr_wlacznie() {
	$kontener = "rrrrr[aaaa]oooooo";

	$result = StrUtil::substr_wlacznie('[', $kontener, "]", 0);
	$this->assertEquals('[aaaa]', $result);
    }

    public function test_substr_wlacznie_dluzszy_przyklad() {
	$kontener = '(string) <img src="data:image/png;base64,iVI=">';

	$result = StrUtil::substr_wlacznie('<img src="', $kontener, '>', 0);
	$this->assertEquals('<img src="data:image/png;base64,iVI=">', $result);
    }

    public function test_substr_nieznaleziony_poczatek() {
	$kontener = "aaaabbb'ooo";

	$result = StrUtil::substr('[', $kontener, "'", 2);
	$this->assertEquals('', $result);
    }

    public function test_substr_nieznaleziony_koniec() {
	$kontener = "aaaa[bbbooo";

	$result = StrUtil::substr('[', $kontener, "'", 2);
	$this->assertEquals('', $result);
    }

    public function test_wyodrebnijOstatniaCzescUri() {
	$uri = "http://www.grupaglasso.pl/media/catalog/product/cache/1/image/800x800/9df78eab33525d08d6e5fb8d27136e95/i/n/inn001.png";

	$result = StrUtil::wyodrebnijOstatniaCzescUri($uri);
	$this->assertEquals("inn001.png", $result);
    }

}
