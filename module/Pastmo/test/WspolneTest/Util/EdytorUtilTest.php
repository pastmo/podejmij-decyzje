<?php

namespace PastmoTest\Wspolne\Util;

use Pastmo\Wspolne\Utils\StrUtil;

class EdytorUtilTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_pobierzKodyZasobow_brak_kodow() {
	$input = "abcde";

	$result = \Pastmo\Wspolne\Utils\EdytorUtil::pobierzKodyZasobow($input);

	$this->assertEquals(0, count($result));
    }

    public function test_pobierzKodyZasobow() {
	$input = "abcde!(nazwa.jpg)[42] abcde !(inna_nazwa.jpg)[43] (errjpg)[78]";
	$expected = ["!(nazwa.jpg)[42]","!(inna_nazwa.jpg)[43]"];

	$result = \Pastmo\Wspolne\Utils\EdytorUtil::pobierzKodyZasobow($input);

	$this->assertEquals(2, count($result));
	$this->assertEquals($expected, $result);
    }

}
