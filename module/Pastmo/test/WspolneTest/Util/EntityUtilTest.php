<?php

namespace PastmoTest\Wspolne\Util;

use Pastmo\Wspolne\Utils\EntityUtil;

class EntityUtilTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $adres;

    public function setUp() {
	parent::setUp();

	$this->adres = \FabrykaEncjiMockowych::makeEncje(\Pastmo\Wspolne\Entity\Adres::class,
			\Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::create()
				->setparametry(array('ulica' => 'Ulica')));
    }

    public function test_wydobadzIdZObiektu() {
	$wynik = EntityUtil::wydobadzId($this->adres);

	$this->assertEquals($this->adres->id, $wynik);
    }

    public function test_wydobadzIdZObiektu_null() {
	$adres = \FabrykaRekordow::makeEncje(\Pastmo\Wspolne\Entity\Adres::class,
			\Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::create());

	$wynik = EntityUtil::wydobadzId($adres);

	$this->assertEquals(null, $wynik);
    }

    public function test_wydobadzId_zInta() {
	$adres = 5;

	$wynik = EntityUtil::wydobadzId($adres);

	$this->assertEquals($adres, $wynik);
    }

    public function test_wydobadzId_nulla() {
	$adres = null;

	$wynik = EntityUtil::wydobadzId($adres);

	$this->assertEquals($adres, $wynik);
    }

    public function test_wydobadzPole() {

	$wynik = EntityUtil::wydobadzPole($this->adres, 'ulica');

	$this->assertEquals($this->adres->ulica, $wynik);
    }

    public function test_czyEncjaZId() {

	$wynik = EntityUtil::czyEncjaZId($this->adres);

	$this->assertEquals(true, $wynik);
    }

    public function test_czyEncjaZId_false() {

	$wynik = EntityUtil::czyEncjaZId(null);

	$this->assertEquals(false, $wynik);
    }

    public function test_sprawdzCzyWspolnyModel() {
	$model = new \Pastmo\Wspolne\Entity\UstawienieSystemu();
	$wynik = EntityUtil::sprawdzCzyWspolnyModel($model);
	$this->assertTrue($wynik);
    }

    public function testNaFalse_sprawdzCzyWspolnyModel() {
	$model = 1;
	$wynik = EntityUtil::sprawdzCzyWspolnyModel($model);
	$this->assertFalse($wynik);
    }

    public function test_pobierzKluczEncji() {
	$wynik = EntityUtil::pobierzKluczEncji('pole_id');
	$this->assertEquals('pole', $wynik);
    }

    public function test_pobierzKluczEncji_inny_klucz() {
	$wynik = EntityUtil::pobierzKluczEncji('pole_id', 'InnePole');
	$this->assertEquals('InnePole', $wynik);
    }

}
