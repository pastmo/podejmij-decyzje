<?php

namespace PastmoTest\Wspolne\Util;

use Pastmo\Wspolne\Utils\DateUtil;

class DateUtilTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_obliczInterwal() {
	$expected = 30 * 60;

	$result = DateUtil::obliczInterwal(30);
	$this->assertEquals($expected, $result->s);
    }

    public function test_obliczInterwal_duzy_ulamek() {
	$expected = 12 * 60;

	$result = DateUtil::obliczInterwal(12.00001);
	$this->assertEquals($expected, $result->s);
    }

    public function test_dodajOdejmijInterwal_plus() {
	$start = "2016-10-08 12:00";
	$expected = "2016-10-08 12:30:00";


	$result = DateUtil::dodajOdejmijInterwal($start, 30);
	$this->assertEquals($expected, $result);
    }

    public function test_dodajOdejmijInterwal_minus() {
	$start = "2016-10-08 12:00";
	$expected = "2016-10-08 11:30:00";


	$result = DateUtil::dodajOdejmijInterwal($start, -30);
	$this->assertEquals($expected, $result);
    }

    public function test_dodajOdejmijInterwal_zero() {
	$start = "2016-10-08 12:00";
	$expected = "2016-10-08 12:00:00";


	$result = DateUtil::dodajOdejmijInterwal($start, 0);
	$this->assertEquals($expected, $result);
    }

    public function test_dodajInterwal() {
	$start = "2016-10-08 12:00";
	$expected = "2016-10-08 12:30:00";


	$result = DateUtil::dodajInterwal($start, 30);
	$this->assertEquals($expected, $result);
    }

    public function test_odejmijInterwal() {
	$start = "2016-10-08 12:00";
	$expected = "2016-10-08 11:30:00";


	$result = DateUtil::odejmijInterwal($start, 30);
	$this->assertEquals($expected, $result);
    }

    public function test_dodajSekundy() {
	$start = "2016-10-08 12:00";
	$expected = "2016-10-08 12:01:30";


	$result = DateUtil::dodajSekundy($start, 90);
	$this->assertEquals($expected, $result);
    }

    public function test_odejmijSekundy() {
	$start = "2016-10-08 12:00";
	$expected = "2016-10-08 11:58:30";


	$result = DateUtil::odejmijSekundy($start, 90);
	$this->assertEquals($expected, $result);
    }

    public function test_obliczMinutyInterwalu() {
	$data1 = "2016-10-12 13:00";
	$data2 = "2016-10-12 14:00";
	$expected = 60;

	$wynik = DateUtil::obliczMinutyInterwalu($data1, $data2);

	$this->assertEquals($expected, $wynik);
    }

    public function test_pobierzNajblizyPoczatekMiesiaca_biezacyRok() {
	$data = "2016-10-12 13:00";
	$miesiac = "11";
	$expected = '2016-11-01 00:00:00';

	$wynik = DateUtil::pobierzNajblizyPoczatekMiesiaca($miesiac, $data);

	$this->assertEquals($expected, $wynik);
    }

    public function test_pobierzNajblizyPoczatekMiesiaca_nastepnyRok() {
	$data = "2016-10-12 13:00";
	$miesiac = "05";
	$expected = '2017-05-01 00:00:00';

	$wynik = DateUtil::pobierzNajblizyPoczatekMiesiaca($miesiac, $data);

	$this->assertEquals($expected, $wynik);
    }

    public function test_pobierzNajblizyPoczatekMiesiaca_wzgledem_biezacej_daty() {
	$miesiac = "05";

	$wynik = DateUtil::pobierzNajblizyPoczatekMiesiaca($miesiac);

	$this->assertNotNull($wynik);
    }

    public function test_formatujDateNaTimestamp() {
	$wynik = DateUtil::formatujDateNaTimestamp("2016-10-9 9:3");
	$this->assertEquals('2016-10-09 09:03:00', $wynik);
    }

    public function test_pobierzDzienTygodnia_niedziela() {
	$wynik = DateUtil::pobierzDzienTygodnia(new \DateTime("2016-10-9 9:3"));
	$this->assertEquals(DateUtil::NIEDZIELA, $wynik);
    }

    public function test_pobierzDzienTygodnia_poniedzialek() {
	$wynik = DateUtil::pobierzDzienTygodnia(new \DateTime("2016-10-10 9:3"));
	$this->assertEquals(DateUtil::PONIEDZIALEK, $wynik);
    }

    public function test_zwiekszDateODni() {
	$start = '2016-10-14 17:12:00';

	$wynik = DateUtil::zwiekszDateODni(3, new \DateTime($start));

	$this->assertEquals('2016-10-17 17:12:00', $wynik->format(DateUtil::FORMAT_TIMESTAMP));
    }

    public function test_zmniejszDateODni() {
	$start = '2016-10-14 17:12:00';

	$wynik = DateUtil::zmniejszDateODni(3, new \DateTime($start));

	$this->assertEquals('2016-10-11 17:12:00', $wynik->format(DateUtil::FORMAT_TIMESTAMP));
    }

    public function test_zwiekszDateODni_null() {
	$wynik = DateUtil::zwiekszDateODni(3);

	$this->assertNotNull($wynik);
    }

    public function test_poczatekDnia() {
	$start = '2016-10-14 17:12:00';

	$wynik = DateUtil::poczatekDnia(new \DateTime($start));

	$this->assertEquals('00:00:00', $wynik->format(DateUtil::FORMAT_CZAS));
	$this->assertEquals('2016-10-14 00:00:00', $wynik->format(DateUtil::FORMAT_TIMESTAMP));
    }

    public function test_koniecDnia() {
	$start = '2016-10-14 17:12:00';

	$wynik = DateUtil::koniecDnia(new \DateTime($start));

	$this->assertEquals('23:59:59', $wynik->format(DateUtil::FORMAT_CZAS));
	$this->assertEquals('2016-10-14 23:59:59', $wynik->format(DateUtil::FORMAT_TIMESTAMP));
    }

    public function test_ustawCzas() {
	$start = '2016-10-14 17:12:00';

	$wynik = DateUtil::ustawCzas(12, 15, new \DateTime($start));

	$this->assertEquals('2016-10-14 12:15:00', $wynik->format(DateUtil::FORMAT_TIMESTAMP));
    }

    public function test_pobierzNastepnaDateBedacaWielokrotnosciaMinut() {
	$wynik = DateUtil::pobierzNastepnaDateBedacaWielokrotnosciaMinut('2017-10-07 12:00:00',
			'2017-10-07 12:21:00', 10);
	$this->assertEquals('2017-10-07 12:30:00', $wynik);
    }

    public function test_pobierzNastepnaDateBedacaWielokrotnosciaMinut_ta_sama_data() {
	$wynik = DateUtil::pobierzNastepnaDateBedacaWielokrotnosciaMinut('2017-10-07 12:00:00',
			'2017-10-07 12:00:00', 10);
	$this->assertEquals('2017-10-07 12:10:00', $wynik);
    }

    public function test_czyTenSamDzien_true() {

	$this->assertTrue(DateUtil::czyTenSamDzien('2017-10-07 12:50:00', '2017-10-07 18:22:00'));
    }

    public function test_czyTenSamDzien_false() {

	$this->assertFalse(DateUtil::czyTenSamDzien('2017-10-07 12:50:00', '2017-10-08 12:50:00'));
    }

    public function test_data1WiekszOd2_true() {

	$this->assertTrue(DateUtil::data1WiekszOd2('2002-02-02 02:02:00', '2001-01-01 01:01:00'));
    }

}
