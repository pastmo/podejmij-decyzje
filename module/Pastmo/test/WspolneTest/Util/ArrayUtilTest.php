<?php

namespace PastmoTest\Wspolne\Util;

use Statystyki\Entity\ProjektySprzedawcy;
use Pastmo\Wspolne\Utils\ArrayUtil;
use \Pastmo\Testy\Util\TB;
use Projekty\Entity\UzytkownikProjekt;
use \Pastmo\Testy\GeneratoryTestow\TestUprawnienie;

class ArrayUtilTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $adres;
    private $inputArray;
    private $uzytkownicyProjekty;

    public function setUp() {
	parent::setUp();

	$this->adres = \FabrykaRekordow::makeEncje(\Pastmo\Wspolne\Entity\Adres::class,
			\Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::create()->setparametry(array('id' => '42')));

	$this->inputArray = array($this->adres, '38', null);
    }

    public function test_ustawPustaTablicePodKluczemJesliNieIstnieje() {
	$tablica = array();
	$klucz = "klucz";

	ArrayUtil::ustawPustaTablicePodKluczemJesliNieIstnieje($tablica, $klucz);
	$this->assertTrue(isset($tablica[$klucz]));
	$this->assertTrue(is_array($tablica[$klucz]));
    }

    public function test_ustawDomyslnyObiektPodKluczemJesliNieIstnieje() {
	$tablica = array();
	$klucz = "klucz";

	ArrayUtil::ustawDomyslnyObiektPodKluczemJesliNieIstnieje($tablica, $klucz, \Projekty\Entity\Projekt::class);
	$this->assertTrue(isset($tablica[$klucz]));
	$this->assertEquals(\Projekty\Entity\Projekt::class, get_class($tablica[$klucz]));
    }

    public function test_ustawWartoscPodKluczemJesliNieIstnieje() {
	$tablica = array();
	$klucz = "klucz";

	ArrayUtil::ustawWartoscPodKluczemJesliNieIstnieje($tablica, $klucz, 42);
	$this->assertTrue(isset($tablica[$klucz]));
	$this->assertEquals(42,$tablica[$klucz]);
    }

    public function test_czyIstniejeNiepustyKlucz_false() {
	$tablica = array();
	$klucz = "klucz";

	$wynik = ArrayUtil::czyIstniejeNiepustyKlucz($tablica, $klucz);
	$this->assertFalse($wynik);
    }

    public function test_czyIstniejeNiepustyKlucz_false_pusty() {
	$tablica = array('klucz' => '');
	$klucz = "klucz";

	$wynik = ArrayUtil::czyIstniejeNiepustyKlucz($tablica, $klucz);
	$this->assertFalse($wynik);
    }

    public function test_czyIstniejeNiepustyKlucz_true() {
	$tablica = array('klucz' => 'klucz');
	$klucz = "klucz";

	$wynik = ArrayUtil::czyIstniejeNiepustyKlucz($tablica, $klucz);
	$this->assertTrue($wynik);
    }

    public function test_pobierzElementKontenera_array() {
	$tablica = ['klucz' => 'wartość'];
	$klucz = "klucz";

	$wynik = ArrayUtil::pobierzElementKontenera($tablica, $klucz);
	$this->assertEquals('wartość', $wynik);
    }

    public function test_pobierzElementKontenera_object() {
	$tablica = ['klucz' => 'wartość'];
	$klucz = "klucz";

	$wynik = ArrayUtil::pobierzElementKontenera((object) $tablica, $klucz);
	$this->assertEquals('wartość', $wynik);
    }

    public function test_pobierzElementKontenera_default() {

	$wynik = ArrayUtil::pobierzElementKontenera([], "klucz", "Domyślna wartość");
	$this->assertEquals("Domyślna wartość", $wynik);
    }

    public function test_zrobTablice_tablicaNaWejsciu() {
	$tablica = array('klucz' => 'klucz');

	$wynik = ArrayUtil::zrobTablice($tablica);
	$this->assertEquals($tablica, $wynik);
    }

    public function test_zrobTablice_nieTablicaWejsciu() {
	$input = 5;

	$wynik = ArrayUtil::zrobTablice($input);
	$this->assertEquals(array(5), $wynik);
    }

    public function test_zrobTablice_nullWejsciu() {

	$wynik = ArrayUtil::zrobTablice(null);
	$this->assertEquals(array(), $wynik);
    }

    public function test_sortujPoKluczu() {
	$dane = [
		['order' => 4, 'value' => 4],
		['order' => 0, 'value' => 42],
		['order' => '3', 'value' => 124],
	];

	ArrayUtil::sortujPoKluczu($dane, 'order', SORT_ASC);

	$this->assertEquals($dane[0]['order'], 0);
	$this->assertEquals($dane[1]['order'], 3);
	$this->assertEquals($dane[2]['order'], 4);
    }

    public function test_sortujMalejaco() {
	$wartosciPoczatkowe = array(2, 4, 1);
	$spodziewaneWyniki = array(4, 2, 1);

	$this->wykonajSprawdzanieTestowaniaMalejacego($wartosciPoczatkowe, $spodziewaneWyniki);
    }

    public function test_sortujMalejaco_powtarzajace_sie_wartosci() {
	$wartosciPoczatkowe = array(2, 1, 4, 1, 2, 4);
	$spodziewaneWyniki = array(4, 4, 2, 2, 1, 1);

	$this->wykonajSprawdzanieTestowaniaMalejacego($wartosciPoczatkowe, $spodziewaneWyniki);
    }

    public function test_przerobNaTabliceId() {

	$wynik = ArrayUtil::przerobNaTabliceId($this->inputArray);

	$this->assertEquals(array('42', '38'), $wynik);
    }

    public function test_przerobNaTabliceWartosci() {

	$wynik = ArrayUtil::przerobNaTabliceWartosci($this->inputArray, 'miasto');

	$this->assertEquals(array("Warszawa", "38"), $wynik);
    }

    public function test_wydobadzIdDoZapytaniaIn_zEncji() {

	$wynik = ArrayUtil::wydobadzIdDoZapytaniaIn($this->adres);

	$this->assertEquals('42', $wynik);
    }

    public function test_wydobadzIdDoZapytaniaIn() {

	$wynik = ArrayUtil::wydobadzIdDoZapytaniaIn($this->inputArray);

	$this->assertEquals('42,38', $wynik);
    }

    public function test_wydobadzWartosciDoZapytaniaIn() {

	$wynik = ArrayUtil::wydobadzWartosciDoZapytaniaIn($this->inputArray, 'miasto');

	$this->assertEquals("'Warszawa',38", $wynik);
    }

    public function test_pobierzPierwszyElement() {
	$wejscie = array(\FabrykaRekordow::makeEncje(\Pastmo\Wspolne\Entity\Adres::class,
			\Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::create()->setparametry(array('id' => '42'))));

	$wynik = ArrayUtil::pobierzPierwszyElement($wejscie);

	$this->assertEquals('42', $wynik->id);
    }

    public function test_pobierzPierwszyElementNieTablica() {
	$expected = "Wejście nie będące tablicą";

	$wynik = ArrayUtil::pobierzPierwszyElement($expected);

	$this->assertEquals($expected, $wynik);
    }

    public function test_pobierzPierwszyElement_domyslnie() {
	$wynik = ArrayUtil::pobierzPierwszyElement(array(), 'domyslnie');

	$this->assertEquals('domyslnie', $wynik);
    }

    public function test_konwertujNaArray() {
	$tablica = array(1, 2, 3, 4, 5, 6, 7, 8);
	$input = (object) $tablica;

	$wynik = ArrayUtil::konwertujNaArray($input);

	$this->assertEquals($tablica, $wynik);
    }

    public function test_konwertujNaArray_limit() {
	$input = array(1, 2, 3, 4, 5, 6, 7, 8);
	$expected = array(1, 2, 3, 4, 5);

	$wynik = ArrayUtil::konwertujNaArray($input, 5);

	$this->assertEquals($expected, $wynik);
    }

    public function testgetUzytkownicyBezZalogowanego() {
	$this->ustawUzytkownicyProjekty();

	$wynik = ArrayUtil::getUzytkownicyBezZalogowanego($this->uzytkownicyProjekty,
			$this->uzytkownicyProjekty[1]->uzytkownik);

	$this->assertEquals(2, count($wynik));
	$this->assertEquals($this->uzytkownicyProjekty[0]->uzytkownik, $wynik[0]);
	$this->assertEquals($this->uzytkownicyProjekty[2]->uzytkownik, $wynik[1]);
    }

    public function testgetEncjeZalogowanego() {
	$this->ustawUzytkownicyProjekty();

	$wynik = ArrayUtil::getEncjeZalogowanego($this->uzytkownicyProjekty,
			$this->uzytkownicyProjekty[1]->uzytkownik);

	$this->assertEquals($wynik->id, $this->uzytkownicyProjekty[1]->id);
    }

    public function testgetEncjeZalogowanego_wartosc_domyslna() {

	$wynik = ArrayUtil::getEncjeZalogowanego(array(), "cokolwiek", "wartość domyślna");

	$this->assertEquals("wartość domyślna", $wynik);
    }

    public function test_inEntityArray_object_objects() {
	$this->ustawUzytkownicyProjekty();
	$this->assertTrue(ArrayUtil::inEntityArray($this->uzytkownicyProjekty[2], $this->uzytkownicyProjekty));
    }

    public function test_inEntityArray_id_objects() {
	$this->ustawUzytkownicyProjekty();
	$this->assertTrue(ArrayUtil::inEntityArray($this->uzytkownicyProjekty[2]->id, $this->uzytkownicyProjekty));
    }

    public function test_inEntityArray_id_ids() {
	$this->ustawUzytkownicyProjekty();
	$this->assertTrue(ArrayUtil::inEntityArray($this->uzytkownicyProjekty[2]->id,
			array($this->uzytkownicyProjekty[2]->id)));
    }

    public function test_inEntityArray_false() {
	$this->assertFalse(ArrayUtil::inEntityArray(43, array(4, 15)));
    }

    public function test_multiMerge() {
	$result = ArrayUtil::multiMerge(array(1), array(4, 15), array(33), array(42));
	$this->assertEquals(array(1, 4, 15, 33, 42), $result);
    }

    public function test_sortujNapisy_rosnaco() {
	$input = array(
		TestUprawnienie::domyslneUprawnienie('abc', false),
		TestUprawnienie::domyslneUprawnienie('dddd', true),
		TestUprawnienie::domyslneUprawnienie('aw', false),
	);
	$expected = array(
		TestUprawnienie::domyslneUprawnienie('abc', false),
		TestUprawnienie::domyslneUprawnienie('aw', false),
		TestUprawnienie::domyslneUprawnienie('dddd', true)
	);

	$result = ArrayUtil::sortujNapisy($input, 'getAkcje');
	$this->assertEquals($expected, $result);
    }

    public function test_getArrayZOrderString() {
	$input = "id ASC";
	$result = ArrayUtil::getArrayZOrderString($input);
	$this->assertEquals(array('id' => "ASC"), $result);
    }

    public function test_getArrayZOrderString_puste() {
	$input = false;
	$result = ArrayUtil::getArrayZOrderString($input);
	$this->assertEquals(array(), $result);
    }

    public function test_getArrayZOrderString_spacja() {
	$input = " ";
	$result = ArrayUtil::getArrayZOrderString($input);
	$this->assertEquals(array(), $result);
    }

    public function test_tablicaStringowZawiera() {
	$szukany = "abc";
	$tablica = ["ak", "weabce"];

	$result = ArrayUtil::tablicaStringowZawiera($tablica, $szukany);
	$this->assertTrue($result);
    }

    public function test_stringZawiera() {
	$szukane = ["abc", 'ghj'];
	$kontener = "asdfghjkl";

	$result = ArrayUtil::stringZawiera($kontener, $szukane);
	$this->assertTrue($result);
    }

    public function test_sprawdzWyciaganieElementowObiektowZTablicy() {
	$urzadzenie = TB::create(\Produkcja\Entity\Urzadzenie::class, $this)->setParameters([
			'id' => 1,
			'nazwa' => 'Laser'
		])->setPF_IBN()->make();
	$wynik = ArrayUtil::wyciagnijZTablicyObiektowWybranePola([$urzadzenie], ['id', 'nazwa']);

	$this->assertEquals($wynik[0]['id'], $urzadzenie->id);
	$this->assertEquals($wynik[0]['nazwa'], $urzadzenie->nazwa);
    }

    private function wykonajSprawdzanieTestowaniaMalejacego($wartosciPoczatkowe, $spodziewaneWyniki) {

	$tablica = array();

	foreach ($wartosciPoczatkowe as $wartosc) {
	    $tablica[] = (new ProjektySprzedawcy())->setLiczbeProjektow($wartosc);
	}

	$wynik = ArrayUtil::sortujMalejaco($tablica, 'liczbaWszystkichProjektow');

	$i = 0;

	$this->assertEquals(count($wartosciPoczatkowe), count($wynik));

	foreach ($wynik as $wartosc) {
	    $this->assertEquals($spodziewaneWyniki[$i], $wartosc->liczbaWszystkichProjektow());
	    $i++;
	}
    }

    private function ustawUzytkownicyProjekty() {
	$this->setDefaultFactoryParameters(\Pastmo\Testy\ParametryFabryki\PFMockowePowiazane::create($this->sm));

	$this->uzytkownicyProjekty = array(
		TB::create(\Logowanie\Entity\UzytkownikKontoMailowe::class, $this)->make(),
		TB::create(\Logowanie\Entity\UzytkownikKontoMailowe::class, $this)->make(),
		TB::create(\Logowanie\Entity\UzytkownikKontoMailowe::class, $this)->make(),
	);
    }

}
