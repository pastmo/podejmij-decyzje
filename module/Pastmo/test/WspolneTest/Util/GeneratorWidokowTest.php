<?php

namespace PastmoTest\Wspolne\Util;

class GeneratorWidokowTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    use \Pastmo\Wspolne\Utils\GeneratorWidokow;

    public function setUp() {
	parent::setUp();
	$this->initGeneratorWidokow();
    }

    public function testgenerujHtml() {
	$wynik = $this->generujHtml('wspolne/tests/generator_widokow', ['dane' => 'Przekazane parametry']);
	$this->assertEquals('<div>Wynik generatora widokow</div><div>Przekazane parametry</div>',$wynik);
    }

}
