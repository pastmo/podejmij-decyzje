<?php

namespace PastmoTest\Wspolne\Menager;

class SesjaMenagerTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_bladSession_validation_failed() {
	$this->sesjaMenager = $this->getMockBuilder(\Wspolne\Menager\SesjaMenager::class)
			->disableOriginalConstructor()
			->setMethods(['createContainer','logujBlad'])->getMock();

	$this->sesjaMenager->expects($this->once())->method('createContainer')->will($this->throwException(new \Zend\Session\Exception\RuntimeException('Session validation failed')));
	$this->sesjaMenager->expects($this->once())->method('logujBlad');

	$this->sesjaMenager->init();

    }

}
