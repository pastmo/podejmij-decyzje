<?php

namespace PastmoTest\Wspolne\Menager;

use Pastmo\Testy\Util\TB;

class WspolnyModelTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_pobierzListeZTabeliObcej() {
	$email = TB::create(\Pastmo\Email\Entity\KontoMailowe::class, $this)->make();
	$this->emaileFolderyMenager->expects($this->once())->method('pobierzDlaKontaMailowego')->willReturn('wynik');

	$wynik = $email->getFoldery();
	$wynik2 = $email->getFoldery();

	$this->assertNotNull($wynik);
	$this->assertEquals($wynik, $wynik2);
    }

    public function test_ustawIdTabeliObcej() {
	$zasob = new \Pastmo\Zasoby\Entity\Zasob();

	$zasob->ustawPole('url', '5');

	$this->assertEquals(5, $zasob->url);
    }

    public function test_ustawIdTabeliObcej_null() {
	$zasob = new \Pastmo\Zasoby\Entity\Zasob();

	$zasob->ustawPole('url', null);

	$this->assertNull($zasob->url);
    }

}
