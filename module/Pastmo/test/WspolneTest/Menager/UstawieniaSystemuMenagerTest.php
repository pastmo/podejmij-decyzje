<?php

namespace Pastmo\WspolneTest\Menager;

use Pastmo\Testy\Util\TB;
use \Pastmo\Wspolne\Enumy\KodyUstawienSystemu;

class UstawieniaSystemuMenagerTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    private $menager;
    private $wartosc = "42";
    private $ustawienieZBazy;

    public function setUp() {
	parent::setUp();

	$this->ustawMockaGateway(\Pastmo\Wspolne\KonfiguracjaModulu::USTAWIENIA_SYSTEMU_GATEWAY);
	$this->menager = new \Pastmo\Wspolne\Menager\UstawieniaSystemuMenager($this->sm);
    }

    public function test_pobierzUstawieniePoKodzie() {
	$this->ustawMockaUstawien();
	$this->ustawWynikPobierzZWherem(array($this->ustawienieZBazy));

	$ustawienie = $this->menager->pobierzUstawieniePoKodzie(KodyUstawienSystemu::ODSWIEZANIE_W_JS);
	$this->assertEquals($this->wartosc, $ustawienie->wartosc);
    }

    public function test_pobierzUstawieniePoKodzie_wartosc_domyslna() {
	$this->ustawWynikPobierzZWherem(array());

	$ustawienie = $this->menager->pobierzUstawieniePoKodzie(KodyUstawienSystemu::ODSWIEZANIE_W_JS, 1500);
	$this->assertEquals(1500, $ustawienie->wartosc);
    }

    private function ustawMockaUstawien() {
	$this->ustawienieZBazy = TB::create(\Pastmo\Wspolne\Entity\UstawienieSystemu::class, $this)
		->setParameters(array('wartosc' => $this->wartosc))
		->make();
    }

    private function ustawWynikPobierzZWherem($wynik) {
	$this->obslugaTestowanejKlasy->ustawMockiDoPobierzZWherem($wynik);
    }

}
