<?php

namespace Pastmo\WspolneTest\Integracyjne;

use Pastmo\Testy\Util\TB;
use \Pastmo\Wspolne\Entity\UstawienieSystemu;

class UstawieniaSystemuMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    const KOD = "test";
    const WARTOSC = "42";

    private $ustawieniaSystemuMenager;
    private $ustawienieSystemu;

    public function setUp() {
	parent::setUp();
	$this->ustawieniaSystemuMenager = $this->sm->get(\Pastmo\Wspolne\Menager\UstawieniaSystemuMenager::class);

	$this->utworzIZalogujUzytkownika();

	$this->ustawienieSystemu = TB::create(\Pastmo\Wspolne\Entity\UstawienieSystemu::class, $this)
		->setParameters(array('wartosc' => self::WARTOSC, 'uzytkownik' => $this->zalogowany, 'kod' => self::KOD))
		->make();
    }

    public function testDodajEncje() {

	$this->assertNotNull($this->ustawienieSystemu->id);
	$this->assertEquals(self::WARTOSC, $this->ustawienieSystemu->wartosc);
	$this->assertEquals($this->zalogowany->id, $this->ustawienieSystemu->uzytkownik->id);
    }

    public function testpobierzUstawienieZalogowanegoPoKodzie() {

	$wynik = $this->ustawieniaSystemuMenager->pobierzUstawienieZalogowanegoPoKodzie(self::KOD);

	$this->assertEquals(self::WARTOSC, $wynik->wartosc);
	$this->assertEquals($this->ustawienieSystemu->id, $wynik->id);
    }

    public function testaktualizujUstawieniaZalogowanego() {

	$this->ustawieniaSystemuMenager->aktualizujUstawieniaZalogowanego(self::KOD, 150);
	$wynik = $this->ustawieniaSystemuMenager->pobierzUstawienieZalogowanegoPoKodzie(self::KOD);

	$this->assertEquals(150, $wynik->wartosc);
	$this->assertEquals($this->ustawienieSystemu->id, $wynik->id);
    }

}
