<?php

namespace Pastmo\WspolneTest\Integracyjne;

class SqlInjectionTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $uzytkownicyMenager;

    public function setUp() {
        parent::setUp();

        $this->uzytkownicyMenager = $this->sm->get(\Logowanie\Menager\UzytkownicyMenager::class);
    }

    public function test_pobierzZWherem() {
        try {
            $this->uzytkownicyMenager->pobierzZWherem("1; INSERT INTO `uzytkownicy` (`login`) VALUES ('haker'); ");
        } catch (\Exception $e) {
            
        }

        $wynik = $this->uzytkownicyMenager->pobierzZWherem("login='haker'");
        $this->assertEquals(0, count($wynik));
    }

    public function test_pobierzResultSetZWherem() {
        try {
            $this->uzytkownicyMenager->pobierzResultSetZWherem("1; INSERT INTO `uzytkownicy` (`login`) VALUES ('haker'); ");
        } catch (\Exception $e) {
            
        }

        $wynik = $this->uzytkownicyMenager->pobierzResultSetZWherem("login='haker'");
        $this->assertEquals(0, count($wynik));
        $wynik2 = $this->uzytkownicyMenager->pobierzZWherem("login='haker'");
        $this->assertEquals(0, count($wynik2));
    }

    public function test_pobierzZWheremCount() {
        try {
            $this->uzytkownicyMenager->pobierzZWheremCount("1; INSERT INTO `uzytkownicy` (`login`) VALUES ('haker'); ");
        } catch (\Exception $e) {
            
        }

        $wynik = $this->uzytkownicyMenager->pobierzZWherem("login='haker'");
        $this->assertEquals(0, count($wynik));
    }

    public function test_updateWhere() {
        try {
            $set = ['czy_aktywny' => '1'];
            $whereArray = ['id' => "1; INSERT INTO `uzytkownicy` (`login`) VALUES 'haker')"];
            $this->uzytkownicyMenager->updateWhere($set, $whereArray);
        } catch (\Exception $e) {
            
        }

        $wynik = $this->uzytkownicyMenager->pobierzZWherem("login='haker'");
        $this->assertEquals(0, count($wynik));
    }

    public function test_usunPoId() {
        try {
            $this->uzytkownicyMenager->usun("1; INSERT INTO `uzytkownicy` (`login`) VALUES ('haker'); ");
        } catch (\Exception $e) {
            
        }

        $wynik = $this->uzytkownicyMenager->pobierzZWherem("login='haker'");
        $this->assertEquals(0, count($wynik));
    }

    public function test_usun() {
        try {
            $this->uzytkownicyMenager->usun("1; INSERT INTO `uzytkownicy` (`login`) VALUES ('haker'); ");
        } catch (\Exception $e) {
            
        }

        $wynik = $this->uzytkownicyMenager->pobierzZWherem("login='haker'");
        $this->assertEquals(0, count($wynik));
    }

    public function test_updateNaNull() {
        try {
            $klucz = 'haslo';
            $id = '1';
            $this->uzytkownicyMenager->updateNaNull($klucz, $id);
        } catch (\Exception $e) {
            
        }

        $wynik = $this->uzytkownicyMenager->pobierzZWherem("login='haker'");
        $this->assertEquals(0, count($wynik));
    }  

    public function test_zapisz() {
        try {
            $user = new \Logowanie\Model\Uzytkownik();
            $user->id = "1; INSERT INTO `uzytkownicy` (`login`) VALUES 'haker')";
            $this->uzytkownicyMenager->zapisz($user);
        } catch (\Exception $e) {
            
        }

        $wynik = $this->uzytkownicyMenager->pobierzZWherem("login='haker'");
        $this->assertEquals(0, count($wynik));
    }

}
