<?php

namespace Pastmo\WspolneTest\Integracyjne;

use Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest;
use Pastmo\Wspolne\Utils\ArrayUtil;
use Wspolne\Menager\SesjaMenager;

class SesjaMenagerTest extends WspolnyIntegracyjnyTest {

    private $sesjaMenager;

    public function setUp() {
	parent::setUp();
	$this->ustawZalogowanegoUseraId1();
	$this->sesjaMenager = new SesjaMenager($this->sm);
    }

    public function test_getSzerokoscEkranu_domyslnie() {
	$szerokosc = $this->sesjaMenager->getSzerokoscEkranu();

	$this->assertTrue(0 === $szerokosc);
    }

    public function test_getSzerokoscEkranu() {
	$this->sesjaMenager->zapiszSzerokoscEkranu(42);
	$szerokosc = $this->sesjaMenager->getSzerokoscEkranu();

	$this->assertEquals(42, $szerokosc);
    }

    public function test_logujBlad() {

	$this->sesjaMenager->logujBlad('tresc bledu');
	$wynik = $this->sm->get(\Pastmo\Logi\Menager\LogiMenager::class)->getPoprzednioDodany();

	$this->assertEquals('["tresc bledu"]', $wynik->tresc);
    }

    public function test_czyTrybTlumaczen() {
	$wynik = $this->sesjaMenager->czyTrybTlumaczen();
	$this->assertEquals(0, $wynik);

	$this->sesjaMenager->setTrybTlumaczen(true);
	$wynik = $this->sesjaMenager->czyTrybTlumaczen();
	$this->assertEquals(1, $wynik);

	$this->sesjaMenager->setTrybTlumaczen(false);
	$wynik = $this->sesjaMenager->czyTrybTlumaczen();
	$this->assertEquals(0, $wynik);
    }

}
