<?php

namespace Pastmo\WspolneTest\Integracyjne;

use Pastmo\Testy\Util\TB;

class WspolnyModelTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $kontoMailowe;
    private $emailFolder;

    public function setUp() {
	parent::setUp();

	$this->kontoMailowe = $this->sm->get(\Firmy\Menager\FirmyMenager::class);
	$this->daneDoFakturyMenager = $this->sm->get(\Projekty\Menager\DaneDoFakturyMenager::class);
    }

    public function test_pobierzListeZTabeliObcej() {
	$this->utworzSkrzynkeMailowa();
	$this->utworzEmailFolder();
	$this->utworzEmailFolder();

	$wynik = $this->kontoMailowe->getFoldery();
	$this->assertEquals(2, count($wynik));
    }

    public function test_pobierzTabeleObca() {
	$pw = TB::create(\Projekty\Entity\ParametryWizualizacji::class, $this)
		->setPF_MP($this->sm)
		->make();

	$this->assertNotNull($pw->grafik->id);

	$pw->aktualizujArray(array());

	$this->assertNotNull($pw->grafik->id);
    }

//    public function test_pobierzTabeleObca() {
//	$pw = TB::create(\Projekty\Entity\ParametryWizualizacji::class, $this)
//		->setPF_MP($this->sm)
//		->make();
//
//	$this->assertNotNull($pw->grafik->id);
//
//	$pw->aktualizujArray(array());
//
//	$this->assertNotNull($pw->grafik->id);
//    }

    private function utworzSkrzynkeMailowa() {
	$this->kontoMailowe = TB::create(\Pastmo\Email\Entity\KontoMailowe::class,
			$this)->make();
    }

    private function utworzEmailFolder() {
	$this->emailFolder = TB::create(\Email\Entity\EmailFolder::class, $this)
		->setParameters(array('konto_mailowe' => $this->kontoMailowe))
		->make();
    }

}
