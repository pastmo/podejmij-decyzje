<?php

namespace Pastmo\WspolneTest\Integracyjne;

use Pastmo\Testy\Util\TB;

class AdresyMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public function setUp() {
	parent::setUp();
    }

    public function testDodajEncje() {

	$encja = TB::create(\Pastmo\Wspolne\Entity\Adres::class, $this)
		->make();

	$this->assertNotNull($encja->id);
    }

}
