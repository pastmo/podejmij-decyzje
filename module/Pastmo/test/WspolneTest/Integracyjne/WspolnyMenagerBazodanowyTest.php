<?php

namespace Pastmo\WspolneTest\Integracyjne;

use Pastmo\Testy\Util\TB;
use Logowanie\Model\TypyUzytkownikow;

class WspolnyMenagerBazodanowyTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $uzytkownicyMenager;
    private $roleMenager;
    private $rola;
    private $roboczaRola;

    const NAZWA_ROLI = 'rola test';
    const KOD_ROLI = 'kod_roli';

    public function setUp() {
	parent::setUp();

	$this->uzytkownicyMenager = $this->sm->get(\Logowanie\Menager\UzytkownicyMenager::class);
	$this->roleMenager = $this->sm->get(\Logowanie\Menager\RoleMenager::class);

	$this->rola = TB::create(\Logowanie\Entity\Rola::class, $this)->setParameters(['nazwa' => self::NAZWA_ROLI, 'kod' => self::KOD_ROLI,
			'typ' => \Logowanie\Model\TypyUzytkownikow::PRACOWNIK])->make();
	$this->roboczaRola = new \Logowanie\Entity\Rola();
    }

    public function test_Dolaczanie_tabel_obcych_w_relacji_wiele_do_jednego___zapis_i_odczyt_z_bazy() {
	$data = $this->arrayToParameters(['rola_id' => $this->rola->id]);

	$uzytkownik = \Logowanie\Model\Uzytkownik::create($this->sm);
	$uzytkownik->exchangeArray($data);
	$this->uzytkownicyMenager->zapisz($uzytkownik);

	$wynik = $this->uzytkownicyMenager->getPoprzednioDodany();
	$this->assertEquals($this->rola->id, $wynik->rola->id);
    }

    public function test_aktualizujPowiazanaTabeleZObiektu() {
	$uzytkownik = TB::create(\Logowanie\Model\Uzytkownik::class, $this)->setPF_IZP($this->sm)->make();

	$this->uzytkownicyMenager->aktualizujPowiazanaTabeleZObiektu($uzytkownik->id, 'rola',
		\Logowanie\Menager\RoleMenager::class
		, $this->rola);

	$zapisanyUzytkownik = $this->uzytkownicyMenager->getRekord($uzytkownik->id);
	$this->assertEquals(self::NAZWA_ROLI, $zapisanyUzytkownik->rola->nazwa);
	$this->assertNotEquals($this->rola->id, $zapisanyUzytkownik->rola->id);
    }

    public function test_aktualizujPowiazanaTabele() {
	$uzytkownik = TB::create(\Logowanie\Model\Uzytkownik::class, $this)
			->setParameters(['rola' => $this->rola])->make();

	$post = array('nazwa' => 'nowsza nazwa', 'kod' => 'new kod');

	$daneDoFabryki = $this->uzytkownicyMenager->aktualizujPowiazanaTabele($uzytkownik->id, 'rola',
		\Logowanie\Menager\RoleMenager::class, $post);

	$rola = $this->roleMenager->getRekord($daneDoFabryki->rola->id);

	$this->assertEquals('nowsza nazwa', $rola->nazwa);
	$this->assertEquals('new kod', $rola->kod);
    }

    public function test_pobierzPaginator() {
	$liczbaRekordowNaStronie = 4;

	$builder = \Pastmo\Wspolne\Builder\PaginatorBuilder::create()
		->setWynikowNaStronie($liczbaRekordowNaStronie)
		->setWhere("id<10")
		->setOrder("id ASC");
	$paginator = $this->sm->get(\Produkty\Menager\ProduktyMenager::class)->pobierzPaginator($builder);

	$licznik = 0;

	$count = count($paginator);
	$ostatniProdukt;

	foreach ($paginator as $item) {
	    $licznik++;
	    $ostatniProdukt = $item;
	}

	$this->assertEquals($liczbaRekordowNaStronie, $licznik);
	$this->assertEquals((int) ceil(9 / 4), $count);
	$this->assertEquals($liczbaRekordowNaStronie, $ostatniProdukt->id);
    }

    public function testPobieranieZWherem() {
	$result = $this->uzytkownicyMenager->pobierzZWherem("1", array("imie" => "ASC", 'data_dodania' => "DESC"));
	$this->assertTrue(count($result) > 0);
    }

    public function test_zapisywanie_z_formularza() {
	$data = $this->arrayToParameters(['id' => $this->rola->id, 'nazwa' => 'nowa nazwa', 'kod' => '']);

	$this->roboczaRola->aktualizujArray($data);
	$this->roleMenager->zapisz($this->roboczaRola);

	$this->sprawdzWynikZapisuRoli('', TypyUzytkownikow::PRACOWNIK);
    }

    public function test_zapisywanie_z_formularza_brak_klucza() {
	$data = $this->arrayToParameters(['id' => $this->rola->id, 'nazwa' => 'nowa nazwa']);

	$this->roboczaRola->aktualizujArray($data);
	$this->roleMenager->zapisz($this->roboczaRola);

	$this->sprawdzWynikZapisuRoli(self::KOD_ROLI, TypyUzytkownikow::PRACOWNIK);
    }

    public function test_zapisywanie_exchenge_nie_zmienia_wartosci_w_przypadku_pustych_wartosci_i_ustawia_wartosci_domyslne() {
	$data = $this->arrayToParameters(['id' => $this->rola->id, 'nazwa' => 'nowa nazwa', 'kod' => '']);

	$this->roboczaRola->exchangeArray($data);
	$this->roleMenager->zapisz($this->roboczaRola);

	$this->sprawdzWynikZapisuRoli(self::KOD_ROLI, TypyUzytkownikow::KLIENT);
    }

    public function test_getRekord() {
	$ustawienieSystemu = TB::create(\Pastmo\Wspolne\Entity\UstawienieSystemu::class, $this)->make();
	$wynik = $this->sm->get(\Pastmo\Wspolne\Menager\UstawieniaSystemuMenager::class)->getRekord($ustawienieSystemu->id);

	$this->assertEquals($ustawienieSystemu->id, $wynik->id);
    }

    private function sprawdzWynikZapisuRoli($spodziewanyKod, $spodziewanyTyp) {
	$wynik = $this->roleMenager->getRekord($this->rola->id);

	$this->assertEquals($this->rola->id, $wynik->id);
	$this->assertEquals('nowa nazwa', $wynik->nazwa);
	$this->assertEquals($spodziewanyKod, $wynik->kod);
	$this->assertEquals($spodziewanyTyp, $wynik->typ);
    }

}
