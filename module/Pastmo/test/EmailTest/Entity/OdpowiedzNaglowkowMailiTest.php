<?php

namespace EmailTest\Entity;

use Pastmo\Email\Entity\OdpowiedzNaglowkowMaili;
use Pastmo\Testy\Util\TB;

class OdpowiedzNaglowkowMailiTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_jsonSerialize() {
	$odpowiedzOdswiezania = OdpowiedzNaglowkowMaili::create($this->sm)
		->addToListaPobranychMaili("Pobrane maile");

	$wynik = json_encode($odpowiedzOdswiezania);

	$this->sprawdzStringNieZawiera($wynik, "translator");
	$this->sprawdzStringZawiera($wynik, "Pobrane maile");
	$this->sprawdzStringZawiera($wynik, 'foldery');
    }

    public function test_setFoldery() {

	$zrodloweFoldery = new \Zend\Mail\Storage\Folder('LOCAL INBOX', 'INBOX');

	$odpowiedzOdswiezania = OdpowiedzNaglowkowMaili::create($this->sm)
		->setFoldery($zrodloweFoldery);

	$this->assertEquals(1, count($odpowiedzOdswiezania->foldery));

	$folder1 = $odpowiedzOdswiezania->foldery[0];
	$this->assertEquals('LOCAL INBOX', $folder1->nazwa);
	$this->assertEquals('INBOX', $folder1->nazwa_fizyczna);
	$this->assertEquals(0, count($folder1->foldery));
    }

    public function test_setFoldery_kodowanie() {

	$zrodlo = 'folder_&AQU-_&AQc-_&ARk-_&AUI-_&APM-_&AXw-_&AXo-';
	$expected = 'folder_ą_ć_ę_ł_ó_ż_ź';

	$zrodloweFoldery = new \Zend\Mail\Storage\Folder($zrodlo, $zrodlo);

	$odpowiedzOdswiezania = OdpowiedzNaglowkowMaili::create($this->sm)
		->setFoldery($zrodloweFoldery);

	$wynik = $odpowiedzOdswiezania->foldery[0]->nazwa;
	$this->assertEquals($expected, $wynik);
    }

}
