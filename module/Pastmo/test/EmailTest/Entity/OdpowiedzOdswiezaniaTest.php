<?php

namespace EmailTest\Entity;

use Pastmo\Email\Entity\OdpowiedzOdswiezania;
use Pastmo\Testy\Util\TB;

class OdpowiedzOdswiezaniaTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    public function test_jsonSerialize() {
	$email = TB::create(\Email\Entity\Email::class, $this)->setParameters(['tytul' => 'tytul', 'data_naglowka' => '2017-00-00'])->make();

	$odpowiedzOdswiezania = OdpowiedzOdswiezania::create()
		->addPrzetworzoneMaile($email)
		->addToListaPobranychMaili("To nie powinno sie wyswietlac");

	$wynik = json_encode($odpowiedzOdswiezania);

	$this->sprawdzStringNieZawiera($wynik, "To nie powinno sie wyswietlac");
	$this->sprawdzStringZawiera($wynik, 'tytul');
	$this->sprawdzStringZawiera($wynik, ''.$email->id);
	$this->sprawdzStringZawiera($wynik, ''.$email->data_naglowka);
    }

}
