<?php

namespace EmailTest\Entity;

use Pastmo\Email\Entity\RekordSzczegoluMaila;


require_once dirname(__FILE__) . "/TestoweMaile.php";

class RekordSzczegoluMailaTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

	private $message;

	public function setUp() {
		parent::setUp();
	}

	public function testPobieranieTresci() {
		$this->ustawMessage(TestoweMaile::Z_TESTOWEJ_PLAIN);

		$wynik = RekordSzczegoluMaila::generujMessageId($this->message);
		$this->assertEquals('<<564F0042.3020001@tidio.net>>',$wynik);
	}

	private function ustawMessage($klucz) {
		$this->message = new \Zend\Mail\Storage\Message(array('raw' => TestoweMaile::$arr[$klucz]));
	}


}
