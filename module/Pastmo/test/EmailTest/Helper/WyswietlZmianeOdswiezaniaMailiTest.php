<?php

namespace PastmoTest\Email\Helper;

use Application\Controller\LadowaczSkryptow;
use Pastmo\Testy\Util\TB;

class WyswietlZmianeOdswiezaniaMailiTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $wyswietlZmianeOdswiezaniaMaili;

    public function setUp() {
	parent::setUp();

	$this->wyswietlZmianeOdswiezaniaMaili = new \Email\Helper\WyswietlZmianeOdswiezaniaMaili($this->sm);

	$this->view = new \Pastmo\Testy\Mocki\ViewMock();
	LadowaczSkryptow::$skrypty = array();
    }

    public function testWywolania() {
	$ustawienieSystemu = TB::create(\Pastmo\Wspolne\Entity\UstawienieSystemu::class, $this)->setParameters(['wartosc' => []])->make();
	$this->ustawieniaSystemuMenager->expects($this->once())->method('pobierzUstawienieZalogowanegoPoKodzie')->willReturn($ustawienieSystemu);
	$this->ustawieniaSystemuMenager->expects($this->once())->method('pobierzUstawieniePoKodzie')->willReturn($ustawienieSystemu);

	$this->wyswietlZmianeOdswiezaniaMaili->__invoke($this->view);
	$this->sprawdzCzyStatus200();
	$this->sprawdzOutputZawiera('<form id="zmiana_odswiezania_email">');
    }

    public function test_czyWartoscWDomyslnychCzasach_true() {
	$wynik = \Email\Helper\WyswietlZmianeOdswiezaniaMaili::czyWartoscWDomyslnychCzasach(30000);
	$this->assertTrue($wynik);
    }

    public function test_czyWartoscWDomyslnychCzasach_false() {
	$wynik = \Email\Helper\WyswietlZmianeOdswiezaniaMaili::czyWartoscWDomyslnychCzasach(125);
	$this->assertFalse($wynik);
    }

}
