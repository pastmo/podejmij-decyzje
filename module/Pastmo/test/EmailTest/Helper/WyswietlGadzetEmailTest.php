<?php

namespace PastmoTest\Email\Helper;

use Application\Controller\LadowaczSkryptow;
use Pastmo\Testy\Util\TB;

class WyswietlGadzetEmailTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $wyswietlGadzetEmail;

    public function setUp() {
	parent::setUp();

	$this->view = new \Pastmo\Testy\Mocki\ViewMock();

	$this->wyswietlGadzetEmail = new \Email\Helper\WyswietlGadzetEmail($this->sm);
	$this->wyswietlGadzetEmail->setView($this->view);

	LadowaczSkryptow::$skrypty = array();
    }

    public function test_dodania_skryptow() {
	$this->markTestSkipped("TODO_TEST: Test wysypał się po zmianie struktury przechowywania danych w ładowaczu skryptów");

	$this->wyswietlGadzetEmail->__invoke($this->view);

	$skrypty = LadowaczSkryptow::$skrypty;
	$this->assertContains('js/helpery/odswiezacz_email.js', $skrypty);
	$this->assertContains('js/pastmo/email/bazowy_odswiezacz_email.js', $skrypty);
    }

    public function testWywolania() {
	$ustawienieSystemu=TB::create(\Pastmo\Wspolne\Entity\UstawienieSystemu::class,$this)->setParameters(['wartosc'=>'4000'])->make();
	$this->ustawieniaSystemuMenager->expects($this->once())->method('pobierzUstawienieZalogowanegoPoKodzie')->willReturn($ustawienieSystemu);

	$this->wyswietlGadzetEmail->__invoke($this->view);
	$this->sprawdzCzyStatus200();
	$this->sprawdzOutputZawiera('var czestotliwoscOdswiezaniaMaili = 4000');
    }

}
