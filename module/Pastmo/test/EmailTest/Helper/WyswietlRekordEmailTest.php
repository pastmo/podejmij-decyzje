<?php

namespace PastmoTest\Email\Helper;

use Application\Controller\LadowaczSkryptow;
use Pastmo\Testy\Util\TB;
use Pastmo\Wiadomosci\Enumy\SkrzynkiWiadomosci;

class WyswietlRekordEmailTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $wyswietlRekordEmail;
    private $email;
    private $skrzynka;
    private $wybraneKonto;

    public function setUp() {
	parent::setUp();

	$this->wyswietlRekordEmail = new \Email\Helper\WyswietlRekordEmail($this->sm);

	$this->email = TB::create(\Email\Entity\Email::class, $this)->make();
	$this->skrzynka = SkrzynkiWiadomosci::ODBIORCZA;
	$this->wybraneKonto = $this->zrobMockObiektu(\Pastmo\Email\Entity\KontoMailowe::class,['getFoldery']);
	$this->wybraneKonto->expects($this->any())->method('getFoldery')->willReturn([]);

	$this->view = new \Pastmo\Testy\Mocki\ViewMock();
	LadowaczSkryptow::$skrypty = array();
    }

    public function testWywolania() {

	$this->wyswietlRekordEmail->__invoke($this->view,$this->email,$this->wybraneKonto, SkrzynkiWiadomosci::ODBIORCZA);

	$this->sprawdzCzyStatus200();
    }

    public function testWywolaniaSzablonu() {

	$this->wyswietlRekordEmail->__invoke($this->view);

	$this->sprawdzCzyStatus200();
    }


}
