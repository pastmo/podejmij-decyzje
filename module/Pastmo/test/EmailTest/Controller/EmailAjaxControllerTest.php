<?php
namespace EmailTest\Integracyjne\Controller;

class EmailAjaxControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    public function setUp() {
	parent::setUp();
    }

    public function testIndex() {

    }

    public function test_odswiezMaileZeWszystkichSkrzynek() {
	$this->kontaMailoweManager
		->expects($this->once())
		->method('odswiezMaileZWszystkichSkrzynekCzesciowo')
		->with($this->equalTo([1, 42]))
		->willReturn([]);

	$this->dispatch('/email_ajax/odswiez_maile_ze_wszystkich_skrzynek?ignorowane_konta[]=1&ignorowane_konta[]=42');
	$this->sprawdzCzySukces();
    }

    public function test_zmienCzestotliwoscOdswiezaniaAction() {

	$this->ustawParametryPosta(['wartosc' => 30000]);
	$this->ustawieniaSystemuMenager
		->expects($this->once())
		->method('aktualizujUstawieniaZalogowanego')
		->with($this->equalTo(\Pastmo\Email\Helper\WyswietlZmianeOdswiezaniaMaili::KOD_USTAWIEN_MAILI),
			$this->equalTo(30000));

	$this->dispatch('/email_ajax/zmien_czestotliwosc_odswiezania');
	$this->sprawdzCzySukces();
    }

    public function test_zmienCzestotliwoscOdswiezaniaAction_bledny_parametr() {

	$this->ustawParametryPosta(['wartosc' => 1]);
	$this->ustawieniaSystemuMenager
		->expects($this->never())
		->method('aktualizujUstawieniaZalogowanego');

	$this->dispatch('/email_ajax/zmien_czestotliwosc_odswiezania');
	$this->sprawdzCzyFail();
    }

    public function getSmallName() {

    }

}
