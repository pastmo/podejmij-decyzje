<?php

namespace PastmoTest\Email\Menager;

use Pastmo\Testy\Util\TB;

class EmailScanerTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $kontoMailowe;
    private $emailScaner;
    private $email;

    public function setUp() {
	parent::setUp();

	$this->kontoMailowe = TB::create(\Pastmo\Email\Entity\KontoMailowe::class, $this)->make();
	$this->emailScaner = $this->kontoMailowe->pobierzEmailScanera();
	$this->email = TB::create(\Pastmo\Email\Entity\Email::class, $this)->setParameters(['licznik' => 42, 'message_id' => __CLASS__])->make();

	$this->emailScaner->polaczenie = $this->getMockBuilder(\Zend\Mail\Storage\Imap::class)
			->disableOriginalConstructor()
			->setMethods(['getMessage'])->getMock();
    }

}
