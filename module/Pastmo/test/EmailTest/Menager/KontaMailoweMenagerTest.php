<?php

namespace PastmoTest\Email\Menager;

use Pastmo\Testy\Util\TB;
use Pastmo\Email\Entity\KontoMailowe;
use Pastmo\Email\Menager\KontaMailoweMenager;
use Pastmo\Email\Entity\PrzetworzonyEmail;
use Pastmo\Email\Entity\OdpowiedzOdswiezania;

class KontaMailoweMenagerTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    const TYTUL = "Tytuł wiadomości";

    protected $kontaMailoweMenager;
    private $uzytkownikKonto;

    public function setUp() {
	parent::setUp();
	$this->ustawMockaGateway(\Pastmo\Module::KONTA_MAILOWE_GATEWAY);
	$this->kontaMailoweMenager = new KontaMailoweMenager($this->sm);
	$this->uzytkownikKonto = TB::create(\Logowanie\Entity\UzytkownikKontoMailowe::class, $this)->setPF_MP($this->sm)->make();
    }

    public function test_odswiezMaileZWszystkichSkrzynekCzesciowo() {

	$this->mockujKontaMailoweMenager(['odswiezMaileWspolne']);
	$this->kontaMailoweMenager->expects($this->once())->method('odswiezMaileWspolne')->willReturn(OdpowiedzOdswiezania::create());

	$this->ustawUzytkownikKontaMialowe([$this->uzytkownikKonto]);

	$wynik = $this->kontaMailoweMenager->odswiezMaileZWszystkichSkrzynekCzesciowo();

	$this->assertEquals(1, count($wynik));
    }

    public function test_odswiezMaileWspolne() {
	$this->ustawDodanegoEmaila();
	$this->mockujKontaMailoweMenager(['zapiszEmailZeSkanera', 'getRekord', 'getPoprzednioDodany']);
	$this->mockujPobieranieKontaMailowego();
	$this->mockujEmailZeScanera();

	$wynik = $this->kontaMailoweMenager->odswiezMaileWspolne(42, true);

	$this->assertEquals(42, $wynik->idKonta);
	$this->assertEquals(1, count($wynik->przetworzoneMaile));

	$expected = PrzetworzonyEmail::create()
		->setId($this->zapisanaWiadomoscEmail->id)
		->setTytul(self::TYTUL)
		->setNadawca('nadawca <email@nadawcy.pl>');
	$this->assertEquals([$expected], $wynik->przetworzoneMaile);
    }

    public function test_odswiezMaileZWszystkichSkrzynekCzesciowo_ignorowaneId() {
	$this->ustawUzytkownikKontaMialowe([(object) ['konto_mailowe' => 5]]);

	$this->sprawdzCzyBrakOdswiezania();

	$wynik = $this->kontaMailoweMenager->odswiezMaileZWszystkichSkrzynekCzesciowo([5]);

	$this->assertEquals([], $wynik);
    }

    public function test_odswiezMaileZWszystkichSkrzynekCzesciowo_obsluga_bledow() {

	$odpowiedziOdswiezania = [OdpowiedzOdswiezania::create()->addErrors("Błąd odświeżania", 42)];

	$this->logiMenager->expects($this->once())->method('dodajLog')->with($this->callback(function($builder) {
		    $this->assertEquals(["Błąd odświeżania"], $builder->tresc);
		    return true;
		}));

	$this->kontaMailoweMenager->logujBledy($odpowiedziOdswiezania);
    }

    public function testpobierzOstatnioZapisanegoMaila() {
	$kontoMailowe = TB::create(KontoMailowe::class, $this)->make();
	$this->emaileMenager->expects($this->once())->method('pobierzOstatnioZapisanegoMaila')->willReturn(__CLASS__);

	$wynik = $kontoMailowe->pobierzOstatnioZapisanegoMaila();

	$this->assertEquals(__CLASS__, $wynik);
    }

    private function ustawDodanegoEmaila() {
	$this->zapisanaWiadomoscEmail = TB::create(\Email\Entity\Email::class, $this)->setParameters(['tytul' => self::TYTUL])->make();
	$this->emaileMenager->expects($this->once())->method('getPoprzednioDodany')->willReturn($this->zapisanaWiadomoscEmail);
    }

    private function mockujKontaMailoweMenager($metodyDoPrzesloniecia) {
	$this->kontaMailoweMenager = $this->zrobMockObiektu(KontaMailoweMenager::class, $metodyDoPrzesloniecia);
    }

    private function mockujEmailZeScanera() {
	$emailScaner = $this->getMockBuilder(\Pastmo\Email\Menager\EmailScaner::class)->disableOriginalConstructor()->getMock();
	$rekordSzczegoluMaila = new \Pastmo\Email\Entity\RekordSzczegoluMaila();

	$emailScaner->expects($this->once())->method('getUnhandledMessages')
		->willReturn(\Pastmo\Email\Entity\OdpowiedzOdswiezania::create()
			->addToListaPobranychMaili($rekordSzczegoluMaila)
			->setIlePobranychMaili(1));
	$this->kontoMailowe->expects($this->once())->method('pobierzEmailScanera')->willReturn($emailScaner);
    }

    private function mockujPobieranieKontaMailowego() {
	$this->kontoMailowe = $this->getMockBuilder(KontoMailowe::class)->getMock();
	$this->kontaMailoweMenager->expects($this->once())->method('getRekord')->willReturn($this->kontoMailowe);
    }

    private function ustawUzytkownikKontaMialowe($konta) {
	$this->uzytkownicyKontaMailoweMenager->expects($this->once())
		->method('pobierzDlaZalogowanegoUzytkownika')->willReturn($konta);
    }

    private function sprawdzCzyBrakOdswiezania() {
	$this->obslugaTestowanejKlasy->ustawWynikGetRekord(null, $this->gateway, 0);
    }

}
