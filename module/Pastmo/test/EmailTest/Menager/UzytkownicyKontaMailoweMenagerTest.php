<?php

namespace PastmoTest\Email\Menager;

use Pastmo\Testy\Util\TB;

class UzytkownicyKontaMailoweMenagerTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $uzytkownikKonto;
    private $emailScaner;

    public function setUp() {
	parent::setUp();
	$this->uzytkownicyKontaMailoweMenager = $this->zrobMockObiektu(\Pastmo\Email\Menager\UzytkownicyKontaMailoweMenager::class,
		['pobierzDlaZalogowanegoUzytkownika']);

	$this->emailScaner = $this->zrobMockObiektu(\Pastmo\Email\Menager\EmailScaner::class, ['sprawdzPolaczenie']);
	$this->uzytkownikKonto = TB::create(\Logowanie\Entity\UzytkownikKontoMailowe::class, $this)->setPF_MP($this->sm)->make();
	$this->uzytkownikKonto->konto_mailowe->emailScaner=$this->emailScaner;

    }

    public function testsprawdzPoprawnoscKontMailowych() {

	$this->uzytkownicyKontaMailoweMenager->expects($this->once())->method('pobierzDlaZalogowanegoUzytkownika')->willReturn([]);

	$wynik = $this->uzytkownicyKontaMailoweMenager->sprawdzPoprawnoscKontMailowych();

	$this->assertTrue($wynik->sukces);
    }

    public function testsprawdzPoprawnoscKontMailowych_poprawnaWeryfikacja() {

	$this->uzytkownicyKontaMailoweMenager->expects($this->once())->method('pobierzDlaZalogowanegoUzytkownika')->willReturn([$this->uzytkownikKonto]);
	$this->emailScaner->expects($this->once())->method('sprawdzPolaczenie')->willReturn(true);

	$wynik = $this->uzytkownicyKontaMailoweMenager->sprawdzPoprawnoscKontMailowych();

	$this->assertTrue($wynik->sukces);
    }

    public function testsprawdzPoprawnoscKontMailowych_blad() {

	$this->uzytkownicyKontaMailoweMenager->expects($this->once())->method('pobierzDlaZalogowanegoUzytkownika')->willReturn([$this->uzytkownikKonto]);
	$this->emailScaner->expects($this->once())->method('sprawdzPolaczenie')->willReturn("coś się wysypało podczas konfiguracji");

	$wynik = $this->uzytkownicyKontaMailoweMenager->sprawdzPoprawnoscKontMailowych();

	$this->assertFalse($wynik->sukces);
	$this->assertEquals(1,count($wynik->wynikDlaKont));
	$this->assertEquals("coś się wysypało podczas konfiguracji",$wynik->wynikDlaKont[$this->uzytkownikKonto->id]->sukces);
    }

    public function test_zrobDomyslneKontoNadawcze_konfiguracja(){
	$konto= \Logowanie\Entity\UzytkownikKontoMailowe::zrobDomyslneKontoNadawcze($this->sm);

	$this->assertNotNull($konto->konto_mailowe->host);
	$this->assertNotNull($konto->konto_mailowe->user);
	$this->assertNotNull($konto->konto_mailowe->passwordKodowane);

    }

}
