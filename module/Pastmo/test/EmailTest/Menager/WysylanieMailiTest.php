<?php

namespace PastmoTest\Email\Menager;



use \ProjektyTest\Menager\ProjektyMenagerTest;
use \Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane;
use \Pastmo\Testy\ParametryFabryki\PFMockowePowiazane;

class WysylanieMailiTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

	private $post = array(
		"email" => 'email@email.pl',
	);
	private $get = array(
		'id' => '1',
		'kod' => 'c13fde8c21332db79e4f057dcd518e48'
	);
	public $projekt;
	public $uzytkownik;
	protected $wysylanieMailiMenager;
	private $emailDoWysylki;

	public function setUp() {
		parent::setUp();

		$this->wysylanieMailiMenager = new \Pastmo\Email\Menager\WysylanieMailiMenager($this->sm);
		$this->wysylanieMailiMenager->test = true;
	}

	public function test_wyslijMaila() {

		$this->ustawEmailDoWysylki();
		$this->wspolneWyslijMaila();
	}

	public function test_wyslijMailaZZalacznikami() {

		$this->ustawEmailDoWysylki();
		$this->dodajZalaczniki();
		$this->wspolneWyslijMaila();
	}

	private function ustawEmailDoWysylki() {
		$this->emailDoWysylki = \Pastmo\Email\Entity\EmailDoWysylki::create();
		$this->emailDoWysylki->temat = "Temat maila";
		$this->emailDoWysylki->tresc = "Treść maila";
		$this->emailDoWysylki->odbiorcaEmail = "odbiorca@asdf.dd";
		$this->emailDoWysylki->odbiorcaImieNazwisko = "Odbiorca Zacny";

		$this->emailDoWysylki->uzytkownikKonto = \FabrykaEncjiMockowych::makeEncje(\Logowanie\Entity\UzytkownikKontoMailowe::class,
						PFMockowePowiazane::create());
	}

	private function dodajZalaczniki() {

		\Zasoby\Menager\ZasobyUploadMenager::$uploadDir="./public_html/test/";
		$zasob = \FabrykaEncjiMockowych::makeEncje(\Zasoby\Model\Zasob::class,
						PFMockoweNiepowiazane::create($this->sm));

		$zalacznikiId = array($zasob->id);
		$this->emailDoWysylki->setIdZasobow($zalacznikiId);

		$this->obslugaKlasObcych->ustawGetRecord(\Zasoby\Menager\ZasobyUploadMenager::class,
				$zasob);
		$this->obslugaKlasObcych->ustawMetode(\Zasoby\Menager\ZasobyUploadMenager::class,
				'zasobRealPath', \Zasoby\Menager\ZasobyUploadMenager::$uploadDir . '1.jpg', 1);
	}

	private function wspolneWyslijMaila() {
		$this->ustawUzytkownikaPoMailu(\FabrykaEncjiMockowych::utworzEncje(\Logowanie\Model\Uzytkownik::class));



		$wynik = $this->wysylanieMailiMenager->wyslijEmail($this->emailDoWysylki);
		$this->assertTrue($wynik->isSuccess, $wynik->message);
	}

	protected function ustawPrawidlowyKod() {
		$this->get['kod'] = \Logowanie\Menager\RejestracjaMenager::generujKod($this->uzytkownik);
	}

}
