<?php

namespace EmailTest\Integracyjne;

use Pastmo\Testy\Util\TB;

class KontoMailoweTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public function testZapisuKonta() {

	$haslo = 'haslo1';

	$konktoMailowe = new \Pastmo\Email\Entity\KontoMailowe($this->sm);
	$konktoMailowe->host = "host";
	$konktoMailowe->user = "user";
	$konktoMailowe->ssl = "abc";
	$konktoMailowe->uzytkownik = \FabrykaRekordow::makeEncje(\Logowanie\Model\Uzytkownik::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));

	$konktoMailowe->ustawHaslo($haslo);

	$kontoMailoweMenager = $this->sm->get(\Pastmo\Email\Menager\KontaMailoweMenager::class);

	$kontoMailoweMenager->zapisz($konktoMailowe);

	$poprzednioDodany = $kontoMailoweMenager->getPoprzednioDodany();

	$this->assertEquals('host', $poprzednioDodany->host);
	$this->assertEquals($haslo, $poprzednioDodany->getHasloJawne());
	$this->assertEquals('abc', $poprzednioDodany->ssl);

	$wyszukane = $kontoMailoweMenager->pobierzKontoMailoweDlaUzytkownika($konktoMailowe->host,
		$konktoMailowe->user, $konktoMailowe->uzytkownik->id);

	$this->assertEquals($poprzednioDodany->id, $wyszukane->id);
    }


}
