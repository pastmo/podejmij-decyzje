<?php

namespace EmailTest\Integracyjne;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Pastmo\Email\Menager\EmailScaner;

class EmailScanerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    protected $traceError = true;
    protected $emailScaner;
    private $konto;

    public function setUp() {
	parent::setUp();
	$this->konto = \FabrykaRekordow::makeEncje(\Pastmo\Email\Entity\KontoMailowe::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));

	$this->emailScaner = new EmailScaner($this->sm);
	$this->emailScaner->init($this->konto);
    }

    public function test_pobierMaileDoWyswietlenia_inbox() {
	$messages = $this->emailScaner->pobierMaileDoWyswietlenia('INBOX', 0, 10);

	$this->assertEquals(10, count($messages->listaPobranychMaili));
	$this->assertNotNull($messages->foldery);
    }

    public function test_pobierMaileDoWyswietlenia_inbox_2_strona() {
	$messages = $this->emailScaner->pobierMaileDoWyswietlenia('INBOX', 1, 10);

	$this->assertEquals(1, count($messages->listaPobranychMaili));
    }

    public function test_pobierMaileDoWyswietlenia_SENT_2_strona() {
	$messages = $this->emailScaner->pobierMaileDoWyswietlenia('SENT', 0, 10);

	$this->assertEquals(2, count($messages->listaPobranychMaili));
    }

    public function test_pobierMaileDoWyswietlenia_foldery() {
	$messages = $this->emailScaner->pobierMaileDoWyswietlenia('INBOX', 0, 10);

	$this->assertEquals(1, count($messages->foldery));
	$this->assertNotNull($messages->foldery);
	$this->assertEquals(8, count($messages->foldery[0]->foldery));

	$odbiorcza = $this->sm->get(\Lokalizacje\Menager\ConstantTranslatorMenager::class)->translateConst('INBOX');
	$this->assertEquals($odbiorcza, $messages->biezacyFolder);
    }

    public function test_pobierSzczegolyMaila() {
	$messages = $this->emailScaner->pobierzSzczegolyMaila(9, 'INBOX');
	$this->assertNotNull($messages->email);
	$this->assertEquals(0,count($messages->errors));
    }

    public function test_pobierSzczegolyMaila_error() {
	$messages = $this->emailScaner->pobierzSzczegolyMaila(942, 'INBOX');
	$this->assertNull($messages->email);
	$this->assertEquals(1,count($messages->errors));
    }

    public function testgetUnhandledMessages() {
	$allMessages = $this->emailScaner->getUnhandledMessages();

	$this->assertTrue(count($allMessages) > 0);
    }

    public function testgetUnhandledMessages_limit_odswiezania() {
	$wynik = $this->emailScaner->getUnhandledMessages(true);

	$this->assertEquals(10, $wynik->ilePobranychMaili);
	$this->assertGreaterThan(0, $wynik->ileMailiZostaloDoPobrania);
    }

    public function testgetAllMessages_basigFields() {

	$allMessages = $this->emailScaner->getAllMessages();

	$this->assertNotNull($allMessages[0]->tytul);
	$this->assertNotNull($allMessages[0]->nadawca);
	$this->assertNotNull($allMessages[0]->data);
    }

    public function testsprawdzPolaczenie() {

	$wynik = $this->emailScaner->sprawdzPolaczenie();
	$this->assertTrue($wynik);
    }

    public function testsprawdzPolaczenie_false() {

	$this->konto->host = "błędny host";
	$this->emailScaner->init($this->konto);

	$wynik = $this->emailScaner->sprawdzPolaczenie();

	$this->sprawdzStringZawiera( $wynik,"Nie można połączyć się z serwerem 'błędny host'");

	$log = $this->sm->get(\Wspolne\Menager\LogiMenager::class)->getPoprzednioDodany();

	$this->sprawdzStringNieZawiera($log->tresc, "password");
	$this->assertFalse(false);
    }

}
