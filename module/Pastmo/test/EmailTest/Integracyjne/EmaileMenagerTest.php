<?php

namespace Pastmo\EmailTest\Integracyjne;

require_once dirname(__FILE__) . "/../Entity/TestoweMaile.php";

use \Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane;
use \Pastmo\Email\Entity\PotencjalnyZasob;
use \Pastmo\Wiadomosci\Enumy\SkrzynkiWiadomosci;
use \Pastmo\Wiadomosci\Enumy\StatusyWiadomosci;
use Pastmo\Testy\Util\TB;
use Pastmo\Email\Entity\RekordSzczegoluMaila;
use EmailTest\Entity\TestoweMaile;

class EmaileMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    protected $emaileMenager;
    protected $kontaMailoweMenager;
    protected $kontoMailowe;
    protected $zapisany;

    public function setUp() {
	parent::setUp();
	$this->emaileMenager = $this->sm->get($this->getEmaileMenagerClass());
	$this->kontaMailoweMenager = $this->sm->get(\Pastmo\Email\Menager\KontaMailoweMenager::class);
	$this->kontoMailowe = TB::create(\Pastmo\Email\Entity\KontoMailowe::class, $this)->make();
    }

    public function test_zapisEncji() {
	$this->ustawZalogowanegoUseraId1();
	$tresc = 'tresc';
	$tytul = 'tytul';
	$zrodlo = 'zrodlo';
	$wiadomosc = \FabrykaRekordow::makeEncje(\Konwersacje\Entity\Wiadomosc::class,
			PFIntegracyjneZapisNiepowiazane::create($this->sm));

	$mail = $this->nowyEmail();
	$mail->data_naglowka = "Fri, 20 Nov 2015 12:13:06 +0100";
	$mail->konto_mailowe = \FabrykaRekordow::utworzEncje($this->sm, \Pastmo\Email\Entity\KontoMailowe::class);
	$mail->tresc = $tresc;
	$mail->tytul = $tytul;
	$mail->wiadomosc = $wiadomosc;
	$mail->zrodlo = TB::create(\Pastmo\Zasoby\Entity\Zasob::class, $this)
		->setParameters(array('nazwa' => $zrodlo))
		->make();

	$this->emaileMenager->zapisz($mail);

	$dodany = $this->emaileMenager->getPoprzednioDodany();

	$this->assertEquals($tytul, $dodany->tytul);
	$this->assertEquals($zrodlo, $dodany->zrodlo->nazwa);
	$this->assertEquals(SkrzynkiWiadomosci::ODBIORCZA, $dodany->skrzynka);
	$this->assertEquals(StatusyWiadomosci::NIEPRZECZYTANA, $dodany->status);
	$this->assertNotNull($dodany->konto_mailowe->id);
	$this->assertNotNull($dodany->wiadomosc->id);
	$this->assertInstanceOf($this->emailClass(), $dodany);
    }

    public function test_zapisEncji_zalaczniki() {
//
	$nazwa = 'nazwa.txt';
	$tresc = 'treść nowego załącznika';
	$zrodlo = 'zrodlo';

	$rekordSzczegoluMaila = \FabrykaRekordow::makeEncje(\Pastmo\Email\Entity\RekordSzczegoluMaila::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuNiepowiazane::create()
				->setparametry(array('raw' => $zrodlo, 'zasoby' => array(new PotencjalnyZasob($nazwa, $tresc)))
	));

	$mail = $this->konwertujZeSkanera($rekordSzczegoluMaila, null, $this->sm);
	$this->emaileMenager->zapisz($mail);

	$dodany = $this->emaileMenager->getPoprzednioDodany();

	$emailZasoby = $dodany->getEmailZasoby();
	$this->assertEquals(1, count($emailZasoby));
	$this->assertNotNull(1, $dodany->zrodlo->id);
    }

    public function testZmianaSkrzynki() {
	$email = \FabrykaRekordow::makeEncje(\Pastmo\Email\Entity\Email::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));

	$this->assertEquals(StatusyWiadomosci::NIEPRZECZYTANA, $email->status);
	$this->assertEquals(SkrzynkiWiadomosci::ODBIORCZA, $email->skrzynka);

	$this->emaileMenager->zmienStatus($email->id, StatusyWiadomosci::PRZECZYTANA);
	$this->emaileMenager->zmienSkrzynke($email->id, SkrzynkiWiadomosci::SPAM);

	$zmieniony = $this->emaileMenager->getRekord($email->id);

	$this->assertEquals(StatusyWiadomosci::PRZECZYTANA, $zmieniony->status);
	$this->assertEquals(SkrzynkiWiadomosci::SPAM, $zmieniony->skrzynka);
    }

    public function t_est_zapiszEmail() {//TODO_TEST: Poprawić kodowanie emaili
	$this->zapiszEmail(\EmailTest\Entity\TestoweMaile::TESTOWY_OD_GRZEGORZA_1);
	$this->assertNotNull($this->zapisany);

	$this->sprawdzStringZawiera($this->zapisany->tresc, "A możesz to do zwykłego pliku tekstowego wrzucić?");
    }

      public function testpobierzOstatnioZapisanegoMaila() {
	$email = TB::create(\Pastmo\Email\Entity\Email::class, $this)->setPF_IZP($this->sm)->make();

	$wynik = $this->emaileMenager->pobierzOstatnioZapisanegoMaila($email->konto_mailowe->id);

	$this->assertEquals($email->id, $wynik->id);
    }

    private function zapiszEmail($klucz) {
	$this->message = new \Zend\Mail\Storage\Message(array('raw' => TestoweMaile::$arr[$klucz]));
	$this->rekordSzczegoluMaila = new RekordSzczegoluMaila();
	$this->rekordSzczegoluMaila->init($this->message, TestoweMaile::$arr[$klucz]);

	$this->kontaMailoweMenager->zapiszEmailZeSkanera($this->rekordSzczegoluMaila, $this->kontoMailowe);

	$this->zapisany = $this->emaileMenager->getPoprzednioDodany();
    }

    protected function getEmaileMenagerClass() {
	return \Pastmo\Email\Menager\EmaileMenager::class;
    }

    protected function nowyEmail() {
	return new \Pastmo\Email\Entity\Email($this->sm);
    }

    protected function konwertujZeSkanera($mailSkanera, $kontoMailowe) {
	return \Pastmo\Email\Entity\Email::konwertujZMailSkanera($mailSkanera, $kontoMailowe,$this->sm);
    }

    protected function emailClass() {
	return \Pastmo\Email\Entity\Email::class;
    }

}
