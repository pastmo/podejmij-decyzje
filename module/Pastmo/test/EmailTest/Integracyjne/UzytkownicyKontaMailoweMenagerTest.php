<?php

namespace PastmoTest\Email\Integracyjne;

class UzytkownicyKontaMailoweMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $user;
    private $user2;
    private $uzytkownicyKontaMailowe;
    private $kontaMailoweMenager;
    private $arr;

    const HOST = "unikalnyHost";
    const EMAIL = "unikalnyUser";

    public function setUp() {
	parent::setUp();

	$this->user = \FabrykaRekordow::makeEncje(\Logowanie\Model\Uzytkownik::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));
	$this->user2 = \FabrykaRekordow::makeEncje(\Logowanie\Model\Uzytkownik::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));
	$this->uzytkownicyKontaMailowe = $this->sm->get(\Logowanie\Menager\UzytkownicyKontaMailoweMenager::class);
	$this->kontaMailoweMenager = $this->sm->get(\Pastmo\Email\Menager\KontaMailoweMenager::class);

	$this->arr = array(
		'id' => [''],
		'host' => array(self::HOST),
		'user' => array(self::EMAIL),
		'password' => array('haslo1')
	);
    }

    public function test_zapiszKontaMailowe() {
	$this->startTransaction();

	$arr = array(
		'id' => ['', ''],
		'host' => array('host1', 'host2'),
		'user' => array('user1', 'user2'),
		'password' => array('haslo1', 'haslo2'),
		'host_wy' => ['host3_wy', 'host4_wy'],
		'imap' => ['111', '222'],
		'stmp' => ['333', '444'],
		'ssl' => ['SSL', 'SSL'],
	);
	$post = $this->arrayToParameters($arr);

	$this->uzytkownicyKontaMailowe->zapiszKontaMailoweDlaUzytkownika($post, $this->user->id);

	$ostatnioDodany = $this->uzytkownicyKontaMailowe->getPoprzednioDodany();
	$this->assertNotNull($ostatnioDodany);
	$this->assertEquals('host2', $ostatnioDodany->konto_mailowe->host);
	$this->assertEquals('host4_wy', $ostatnioDodany->konto_mailowe->host_wychodzacy);
	$this->assertEquals('user2', $ostatnioDodany->konto_mailowe->user);
	$this->assertEquals('222', $ostatnioDodany->konto_mailowe->port_imap);
	$this->assertEquals('444', $ostatnioDodany->konto_mailowe->port_stmp);
	$this->assertEquals('SSL', $ostatnioDodany->konto_mailowe->ssl);

	$this->rollbackTransaction();
    }

    public function test_zapiszKontaMailowe_te_same_maile_dla_dwoch_userow() {
	$this->startTransaction();

	$post = $this->arrayToParameters($this->arr);

	$this->uzytkownicyKontaMailowe->zapiszKontaMailoweDlaUzytkownika($post, $this->user->id);
	$this->uzytkownicyKontaMailowe->zapiszKontaMailoweDlaUzytkownika($post, $this->user2->id);

	$where = "host='" . self::HOST . "'";
	$wynik = $this->kontaMailoweMenager->pobierzZWherem($where);
	$this->assertEquals(1, count($wynik));

	$this->rollbackTransaction();
    }

    public function test_zapiszKontaMailowe_zmiana_maila_dla_tej_samej_skrzynki() {
	$this->startTransaction();

	$post = $this->arrayToParameters($this->arr);
	$this->uzytkownicyKontaMailowe->zapiszKontaMailoweDlaUzytkownika($post, $this->user->id);

	$uzytkownikKonto = $this->uzytkownicyKontaMailowe->getPoprzednioDodany();

	$this->arr['id'] = [$uzytkownikKonto->id];
	$this->arr['user'] = ['inny_user'];

	$post2 = $this->arrayToParameters($this->arr);
	$this->uzytkownicyKontaMailowe->zapiszKontaMailoweDlaUzytkownika($post2, $this->user->id);

	$where = "host='" . self::HOST . "'";
	$wynik = $this->kontaMailoweMenager->pobierzZWherem($where);
	$this->assertEquals(1, count($wynik));

	$this->rollbackTransaction();
    }

}
