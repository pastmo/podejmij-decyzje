<?php

namespace EmailTest\Integracyjne;
use Zend\Crypt\BlockCipher;



class BlockCipherTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

public function testBlockCipher() {

	$cipher = BlockCipher::factory('mcrypt', array('algorithm' => 'aes'));
	$cipher->setKey('this is the encryption key');
	$cipher->setSalt(15487454598746548574174);
	$text = 'This is the message to encrypt';
	$encrypted = $cipher->encrypt($text);

	$cipher = BlockCipher::factory('mcrypt', array('algorithm' => 'aes'));
	$cipher->setKey('this is the encryption key');
	$cipher->setSalt(15487454598746548574174);
	$result = $cipher->decrypt($encrypted);


	$this->assertEquals($text, $result);
}

}
