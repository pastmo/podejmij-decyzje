<?php

namespace Pastmo\Zasoby\Fasada;

class ZasobyFasada extends \Wspolne\Fasada\WspolneFasada {

    protected $zasobyMenager;
    protected $zasobyUploadMenager;
    protected $zasobyDownloadMenager;
    protected $zasobyKonwerterNapisowMenager;

    public function __construct(\Zend\ServiceManager\ServiceManager $serviceMenager) {
	parent::__construct($serviceMenager);

	$this->zasobyMenager = $serviceMenager->get(\Pastmo\Zasoby\Menager\ZasobyMenager::class);
	$this->zasobyUploadMenager = $serviceMenager->get(\Zasoby\Menager\ZasobyUploadMenager::class);
	$this->zasobyDownloadMenager = $serviceMenager->get(\Zasoby\Menager\ZasobyDownloadMenager::class);
	$this->zasobyKonwerterNapisowMenager = $serviceMenager->get(\Pastmo\Zasoby\Menager\ZasobyKonwerterNapisowMenager::class);
    }

    public function downloadZasobResponse($zasobId) {
	return $this->zasobyDownloadMenager->downloadZasobResponse($zasobId);
    }

    public function downloadZip(array $zasobyId, $nazwaZipa) {
	return $this->zasobyDownloadMenager->downloadZip($zasobyId, $nazwaZipa);
    }

    public function pobierzUrlPodgladu($zasobId) {
	return $this->zasobyDownloadMenager->pobierzUrlPodgladu($zasobId);
    }

    public function usunZasob($zasobId) {

    }

    public function zaladujPlik() {
	return $this->zasobyUploadMenager->zaladujPlik();
    }

    public function zrobZasobZLinka($link) {
	return $this->zasobyUploadMenager->zrobZasobZLinka($link);
    }

    public function getRekord($id) {
	return $this->zasobyDownloadMenager->getRekord($id);
    }

    public function wyswietlUrlObrazkaZBasePath($zasob) {
	return $this->zasobyDownloadMenager->wyswietlUrlObrazkaZBasePath($zasob);
    }

    public function zapiszPlik($oryginalnaNazwa, $tresc) {
	return $this->zasobyUploadMenager->zapiszPlik($oryginalnaNazwa, $tresc);
    }

    public function pobierzIdZasobow($post, $klucz) {
	return $this->zasobyUploadMenager->pobierzIdZasobow($post, $klucz);
    }

    public function konwertujZasobyZNapisu($string) {
	return $this->zasobyKonwerterNapisowMenager->konwertujZasobyZNapisu($string);
    }

    public function wyswietlUrlPlikuZBasePath($zasob) {
	return $this->zasobyDownloadMenager->wyswietlUrlPlikuZBasePath($zasob);
    }

    public function pobierzPodglad($podglad_id) {
        return $this->zasobyMenager->pobierzPodglad($podglad_id);
    }

    public function sprawdzCzyIstniejeZasob($nazwa) {
	return $this->zasobyMenager->sprawdzCzyIstniejeZasob($nazwa);
    }

    public function getPoprzednioDodany(){
	return $this->zasobyUploadMenager->getPoprzednioDodany();
    }

}
