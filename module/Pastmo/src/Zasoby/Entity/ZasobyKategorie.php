<?php

namespace Pastmo\Zasoby\Entity;

class ZasobyKategorie extends \Wspolne\Model\WspolneModel {

    public $id;
    public $opis;

    public function __construct($sm = null) {
	parent::__construct($sm);
    }

    public function exchangeArray($data) {
        $this->id = $this->pobierzLubNull($data, self::ID);
        $this->opis = $this->pobierzLubNull($data, 'opis');
    }
    
    public function konwertujNaKolumneDB($nazwaWKodzie) {
        return $nazwaWKodzie;
    }
    
    public function pobierzKlase() {
        
    }    
    
}
