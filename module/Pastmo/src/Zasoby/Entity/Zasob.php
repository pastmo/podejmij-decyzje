<?php

namespace Pastmo\Zasoby\Entity;

use Zasoby\Model\TypZasobu;

class Zasob extends \Wspolne\Model\WspolneModel {

    public $id;
    public $dodany_przez;
    public $dodany_przez_id;
    public $url;
    public $nazwa;
    public $typ;
    public $typ_fizyczny;
    public $rozmiar;
    public $data_dodania;
    public $kategoria_id;
    public $podglad_id;
    public $bazowy_zasob_id;
    public $kod_wielkosci_miniaturki;

    public function __construct($sm = null) {
	parent::__construct($sm);
    }

    public function exchangeArray($data) {
	$this->data = $data;

	$this->id = (!empty($data['id'])) ? $data['id'] : null;
	$this->url = (!empty($data['url'])) ? $data['url'] : null;
	$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
	$this->typ = (!empty($data['typ'])) ? $data['typ'] : TypZasobu::OBRAZEK;
	$this->typ_fizyczny = $this->pobierzLubNull($data, 'typ_fizyczny');
	$this->rozmiar = $this->pobierzLubNull($data, 'rozmiar');
	$this->data_dodania = $this->pobierzLubNull($data, 'data_dodania');

	$this->kategoria_id = $this->pobierzTabeleObca('zasoby_kategorie',
		\Pastmo\Zasoby\Menager\ZasobyKategorieMenager::class, new ZasobyKategorie());
	$this->podglad_id = $this->pobierzLubNull($data, 'podglad_id');
	$this->kod_wielkosci_miniaturki = $this->pobierzLubNull($data, 'kod_wielkosci_miniaturki');
	$this->bazowy_zasob_id = $this->pobierzLubNull($data, 'bazowy_zasob_id');
	$this->dodany_przez_id = $this->pobierzLubNull($data, 'dodany_przez_id');



	if ($this->czyTabeleDostepne()) {
	    $this->wyswietlanie_js = $this->sm->get(\Zasoby\Fasada\ZasobyFasada::class)
		    ->wyswietlUrlObrazkaZBasePath($this);
	}
    }

    public function getDodanyPrzez() {
	$this->dodany_przez = $this->pobierzTabeleObca('dodany_przez_id',
		\Logowanie\Menager\UzytkownicyMenager::class, new \Logowanie\Model\Uzytkownik());
	return $this->dodany_przez;
    }

    public function getRozmiar() {
	$zwrotnyString = "0";
	$kb = $this->rozmiar / 1024;
	$zwrotnyString = round($kb, 2) . ' KB';
	if ($kb >= 1024) {
	    $mb = $kb / 1024;
	    $zwrotnyString = round($mb, 2) . ' MB';
	    if ($mb >= 1024) {
		$gb = $mb / 1024;
		$zwrotnyString = round($gb, 2) . ' GB';
	    }
	}

	return $zwrotnyString;
    }

    public function wyswietl() {
	if ($this->czyTabeleDostepne()) {
	    $zasobyTable = $this->sm->get(\Zasoby\Menager\ZasobyUploadMenager::class);
	} else {
	    $zasobyTable = new \Zasoby\Menager\ZasobyUploadMenager();
	}
	return $zasobyTable->wyswietlUrlObrazka($this);
    }

    public function pobierzMiniaturke($max_wymiar = false) {
	$zasobyTable = $this->sm->get(\Zasoby\Menager\ZasobyUploadMenager::class);
	return $zasobyTable->pobierzUrlMiniaturki($this, $max_wymiar);
    }

    public function pobierzUrlPodgladu() {
	$zasobyTable = $this->sm->get(\Zasoby\Menager\ZasobyUploadMenager::class);
	return $zasobyTable->pobierzUrlPodgladu($this);
    }

    public function wyswietlUrlBasePath() {
	$zasobyFasada = $this->sm->get(\Zasoby\Fasada\ZasobyFasada::class);
	return $zasobyFasada->wyswietlUrlPlikuZBasePath($this);
    }

    public function dowiazListyTabelObcych() {

    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'dodany_przez':
		return 'dodany_przez_id';
	    default :return $nazwaWKodzie;
	}
    }

    public static function zrobDomyslnyZasobAvatara($sm = null) {
	$avatar = new Zasob($sm);
	$avatar->exchangeArray(array('typ' => \Zasoby\Model\TypZasobu::AVATAR));
	return $avatar;
    }

    public static function zrobAvataraSystemowego($sm = null) {
	$avatar = new Zasob($sm);
	$avatar->exchangeArray(array('typ' => \Zasoby\Model\TypZasobu::AVATAR));
	return $avatar;
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
