<?php

namespace Pastmo\Zasoby\Entity;

class WielkosciMiniaturek extends \Wspolne\Model\WspolneModel {

    public $kod;
    public $domyslna;

    public function __construct($sm = null) {
	parent::__construct($sm);
    }

    public function exchangeArray($data) {
        $this->kod = $this->pobierzLubNull($data, 'kod');
        $this->domyslna = $this->pobierzLubNull($data, 'domyslna');
    }
    
    public function konwertujNaKolumneDB($nazwaWKodzie) {
        return $nazwaWKodzie;
    }
    
    public function pobierzKlase() {
        
    }    
    
}
