<?php

namespace Pastmo\Zasoby\Menager;

use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;
use Zasoby\Model\FileOperator;
use Pastmo\Zasoby\Entity\Zasob;
use Zasoby\Model\TypZasobu;
use Zasoby\Model\KategoriaZasobu;

class ZasobyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    const domyslnyObrazek = "default.png";
    const domyslnyObrazekTypu = "default_%s.png";

    static $uploadDir = "./public_html/upload/";
    static $tmpDir = "./public_html/tmp/";

    const uploadToShow = "upload/";

    public $fileOperator;
    protected $id = 0;
    private $finalName;
    private $extension;
    private $pelnaSciezkaZapisu;
    private $wielkosciMiniaturekMenager;

    public function __construct(ServiceManager $sm = null) {
	parent::__construct($sm, 'ZasobyTableGateway');
	$this->fileOperator = new FileOperator();
    }

    public function zapisz(\Pastmo\Wspolne\Entity\WspolnyModel $model) {
	if (!$model->id) {
	    $model->dodany_przez = $this->get(\Wspolne\Menager\SesjaMenager::class)->pobierzIdUzytkownika();
	}
	parent::zapisz($model);
    }

    public function zaladujPlik() {

	$this->wynik = array();

	$this->target_dir = $this->getUploadDir();
	$this->id = $this->pobierz_ilosc_zasobow() + 1;

	while (list($klucz, $wartosc) = each($_FILES)):
	    $this->przeniesPliki($wartosc);
	endwhile;

	return $this->wynik;
    }

    public function zapiszPlik($oryginalnaNazwa, $tresc) {
	$this->target_dir = $this->getUploadDir();

	$this->zrobSciezkeDoZapisu($oryginalnaNazwa);

	$fh = fopen($this->pelnaSciezkaZapisu, 'w');
	fwrite($fh, $tresc);
	fclose($fh);

	return $this->wydobadzInformacjeIZrobZasob($oryginalnaNazwa);
    }

    private function zrobSciezkeDoZapisu($oryginalnaNazwa) {
	$this->extention = pathinfo($oryginalnaNazwa, PATHINFO_EXTENSION);

	$this->finalName = $this->kodujNazwe($oryginalnaNazwa, "." . $this->extention);

	$this->pelnaSciezkaZapisu = $this->target_dir . $this->finalName;
    }

    private function wydobadzInformacjeIZrobZasob($oryginalnaNazwa) {
	$finfo = finfo_open(FILEINFO_MIME_TYPE);

	$type = finfo_file($finfo, $this->pelnaSciezkaZapisu);

	$param = array();
	$param['size'] = filesize($this->pelnaSciezkaZapisu);
	$param['type'] = $type;

	$zasob = $this->zrobZasob($this->finalName, $oryginalnaNazwa, $param);
	return $zasob;
    }

    public function downloadZasobResponse($id) {

	$zasob = $this->getRekord($id);

	$fileName = $this->zasobRealPath($zasob);

	return $this->zrobDownloadLink($fileName, $zasob->nazwa);
    }

    public function downloadZip(array $zasobyId, $nazwaZipa) {
	$zip = new \ZipArchive;

	$nazwa = $nazwaZipa . ".zip";
	$fullPath = $this->realPathTmp($nazwa);
	$res = $zip->open($fullPath, \ZipArchive::CREATE);
	if ($res === TRUE) {

	    foreach ($zasobyId as $zasobId) {
		$zasob = $this->getRekord($zasobId);

		$zip->addFile($this->zasobRealPath($zasob), $zasob->nazwa);
	    }
	    $zip->close();

	    return $this->zrobDownloadLink($fullPath, $nazwa);
	} else {
	    throw new \Wspolne\Exception\GgerpException("Nie można utworzyć pliku $fullPath");
	}
    }

    public function zrobZasobZLinka($link) {
	$this->target_dir = $this->getUploadDir();

	$oryginalnaNazwa = basename($link);

	$this->zrobSciezkeDoZapisu($oryginalnaNazwa);

	copy($link, $this->pelnaSciezkaZapisu);

	return $this->wydobadzInformacjeIZrobZasob($oryginalnaNazwa);
    }

    public function pobierzUrlPodgladu($zasob) {

	$result = self::uploadToShow;
	if ($zasob->url !== null && $zasob->typ !== TypZasobu::PLIK) {
	    $result.=$zasob->url;
	} else {
	    $nowyZasob = $this->sprawdzCzyPodgladIstnieje($zasob);
	    if (!isset($nowyZasob->url)) {
		return $this->wyswietlUrlObrazka($zasob);
	    }
	    $result.= $nowyZasob->url;
	}
	return $result;
    }

    private function sprawdzCzyPodgladIstnieje($zasob) {
	$podglad_id = $zasob->podglad_id;
	if (isset($podglad_id)) {
	    $podgladWygenerowanyWczesniej = $this->getRekord($podglad_id);
	    return $podgladWygenerowanyWczesniej;
	}
	$nowyZasob = $this->zrobPodgladPdfIZapiszWBazie($zasob);
	return $nowyZasob;
    }

    private function zrobPodgladPdfIZapiszWBazie($zasob) {
	if ($this->sprawdzCzyPdf($zasob->typ_fizyczny)) {
	    $this->makePngFromPdfFolderIfDontExist();
	    $converted = $this->konwertujPdfDoObrazka($zasob);
	    $link = self::$tmpDir . 'pngFromPdf/' . $converted . '/page-1.png';
	    $nowyZasob = $this->zrobZasobZLinka($link);
	    $zasob->podglad_id = $nowyZasob->id;
	    $this->zapisz($zasob);
	    return $nowyZasob;
	}
	$obrazek = self::ustawDomyslnyObrazek($zasob->typ, $zasob->url);
	return $obrazek;
    }

    private function konwertujPdfDoObrazka($zasob) {
	$pdflib = new \ImalH\PDFLib\PDFLib();
	$realPdfPath = $this->zasobRealPathZeStringa($zasob->url);
	$pdflib->setPdfPath($realPdfPath);
	$tmpRandom = $this->kodujNazwe($zasob->nazwa, $zasob->typ_fizyczny);
	$nazwaFolderu = 'pngFromPdf/' . $tmpRandom;
	if (!$this->czyPlikIstnieje(self::$tmpDir . $nazwaFolderu)) {
	    $this->zrobFolder($nazwaFolderu, true);
	}
	$outputPath = self::$tmpDir . 'pngFromPdf/' . $tmpRandom;
	$pdflib->setOutputPath($outputPath);
	$pdflib->setImageFormat(\ImalH\PDFLib\PDFLib::$IMAGE_FORMAT_PNG);
	$pdflib->setDPI(300);
	$pdflib->setPageRange(1, 1); //tylko pierwsza strona, bo to podgląd

	$pdflib->convert();
	return $tmpRandom;
    }

    private function makePngFromPdfFolderIfDontExist() {
	if (!$this->czyPlikIstnieje(self::$tmpDir . 'pngFromPdf')) {
	    $this->zrobFolder('pngFromPdf', true);
	}
    }

    public function pobierzUrlMiniaturki($zasob, $max_wymiar) {
	$miniaturka = $this->pobierzMiniaturke($zasob, $max_wymiar);
	return $miniaturka->wyswietl();
    }

    public function pobierzMiniaturke($zasob, $max_wymiar) {
	if (!$max_wymiar) {
	    $max_wymiar = $this->getDomyslnyRozmiar();
	}
	$miniaturka = $this->getMiniaturke($zasob, $max_wymiar);
	if (empty($miniaturka)) {
	    $miniaturka = $this->stworzMiniaturke($zasob, $max_wymiar);
	}
	return $miniaturka;
    }

    private function getDomyslnyRozmiar() {
	$wielkosciMiniaturekMenager = $this->sm->get(WielkosciMiniaturekMenager::class);
	$domyslny_max_wymiar = $wielkosciMiniaturekMenager->getRekordKlucz('domyslna', 1);
	return $domyslny_max_wymiar->kod ? $domyslny_max_wymiar->kod : 100;
    }

    private function getMiniaturke($zasob, $max_wymiar) {
	if (!isset($zasob->id)) {
	    return new Zasob($this->sm);
	}
	$where = 'bazowy_zasob_id = ' . $zasob->id . ' AND '
		. 'kod_wielkosci_miniaturki = ' . $max_wymiar;
	$tablica = $this->pobierzZWherem($where);
	return \Pastmo\Wspolne\Utils\ArrayUtil::pobierzPierwszyElement($tablica);
    }

    protected function stworzMiniaturkeJesliTrzeba($zasob, $max_wymiar = false) {
	$wynik = null;
	if ($zasob->typ_fizyczny === TypZasobu::OBRAZEK) {
	    $wynik = $this->stworzMiniaturke($zasob, $max_wymiar);
	}
	return $wynik;
    }

    protected function stworzMiniaturke($zasob, $max_wymiar) {
	if (!$zasob->url) {
	    return new Zasob($this->sm);
	}
	$maxWymiar = $zasob->kod_wielkosci_miniaturki ? $zasob->kod_wielkosci_miniaturki : $max_wymiar;
	$nowyFolder = $this->zrobFolderJesliJeszczeNieIstnieje($maxWymiar);

	$filename = self::$uploadDir . $zasob->url;
	$image_p = $this->zmiejszObrazekZachowujacProporcje($filename, $max_wymiar);
	imagepng($image_p, $nowyFolder . '/' . $zasob->url);

	$nowy_zasob = $this->zrobZasobZLinka($nowyFolder . '/' . $zasob->url);
	$nowy_zasob->bazowy_zasob_id = $zasob->id;
	$nowy_zasob->kod_wielkosci_miniaturki = $maxWymiar;
	$this->zapisz($nowy_zasob);

	return $nowy_zasob;
    }

    private function zrobFolderJesliJeszczeNieIstnieje($nazwa) {
	if (!$this->czyPlikIstnieje(self::$uploadDir . $nazwa)) {
	    $this->zrobFolder($nazwa);
	}
	return self::$uploadDir . $nazwa;
    }

    public function zrobFolder($sciezka, $tmp = false) {
	$path = $this->zasobRealPathZeStringa($sciezka);
	if ($tmp) {
	    $path = $this->realPathTmp($sciezka);
	}
	mkdir($path);
    }

    public function zasobRealPath($zasob) {
	return $this->zasobRealPathZeStringa($zasob->url);
    }

    public function getUploadDir() {
	return self::$uploadDir;
    }

    public function zasobRealPathZeStringa($url) {//TODO: Czasmi używane do zapisu a czasami do odczytu
	return $this->getUploadDir() . $url;
    }

    public function realPathTmp($url) {
	return self::$tmpDir . $url;
    }

    private function przeniesPliki($wartosc) {
	if (is_array($wartosc["name"])) {
	    for ($i = 0; $i < count($wartosc['name']); $i++) {
		$komunikat = array(
			'name' => $wartosc['name'][$i],
			'tmp_name' => $wartosc['tmp_name'][$i],
			'type' => $wartosc['type'][$i],
			'size' => $wartosc['size'][$i]);
		$this->przeniesPliki($komunikat);
	    }
	} else {

	    $nazwa = basename($wartosc["name"]);

	    $finalName = $this->przeniesPlik($wartosc["tmp_name"], $nazwa);


	    if ($finalName) {

		$nowyZasob = $this->zrobZasob($finalName, $nazwa, $wartosc);
		$this->stworzMiniaturkeJesliTrzeba($nowyZasob);
		$this->wynik[] = $nowyZasob;
		$this->id++;
	    } else {
		//TODO: Wymyślić co zrobić w takim przypadku. Wyrzuca błąd jeśli jest multifile.
		//throw new Exception("Sorry, there was an error uploading your file.");
	    }
	}
    }

    private function przeniesPlik($zrodlo, $nazwa) {
	$rozszerzenie = $this->pobierzRozszerzenie($nazwa);

	$finalName = $this->kodujNazwe($nazwa, $rozszerzenie);
	$target_file = $this->target_dir . $finalName;

	if ($this->fileOperator->move_uploaded_file($zrodlo, $target_file)) {
	    return $finalName;
	} else {
	    return false;
	}
    }

    private function zrobDownloadLink($fullPath, $nazwa) {
	$response = new \Zend\Http\Response\Stream();
	$response->setStream(fopen($fullPath, 'r'));
	$response->setStatusCode(200);

	$headers = new \Zend\Http\Headers();
	$headers->addHeaderLine('Content-Type', 'whatever your content type is')
		->addHeaderLine('Content-Disposition', 'attachment; filename="' . $nazwa . '"')
		->addHeaderLine('Content-Length', filesize($fullPath));

	$response->setHeaders($headers);
	return $response;
    }

    private function pobierzRozszerzenie($nazwa) {
	$kropka = strpos($nazwa, ".");
	$rozszerzenie = substr($nazwa, $kropka);
	return $rozszerzenie;
    }

    private function zrobZasob($finalName, $nazwa, $rowArray) {
	$zasob = new Zasob();
	$zasob->url = $finalName;
	$zasob->typ = $this->wyliczTyp($rowArray['type']);
	$zasob->rozmiar = $rowArray['size'];
	$zasob->kategoria = $this->wyliczKategorie();
	$zasob->nazwa = $nazwa;
	$zasob->typ_fizyczny = $rowArray['type'];
	$this->zapisz($zasob);
	$zapisany = $this->getPoprzednioDodany();
	return $zapisany;
    }

    private function wyliczTyp($typ) {
	if (substr($typ, 0, 5) === 'image') {
	    return TypZasobu::OBRAZEK;
	}
	return TypZasobu::PLIK;
    }

    private function sprawdzCzyPdf($typ_fizyczny) {
	if (strpos($typ_fizyczny, 'pdf') !== false) {
	    return true;
	}
	return false;
    }

    private function wyliczKategorie() {
	if (isset($_POST['kategoria'])) {
	    return $_POST['kategoria'];
	}
	return KategoriaZasobu::ZWYKLY_PLIK;
    }

    protected function kodujNazwe($nazwa, $rozszerzenie) {
	$this->id = $this->pobierz_ilosc_zasobow() + 1;
	return $this->id . md5($nazwa) . \Pastmo\Wspolne\Utils\StrUtil::usunZnakiNienadajaceSieNaRozszerzeniePliku($rozszerzenie);
    }

    public function wyswietlUrlObrazkaZBasePathPoId($id) {
	$zasob = $this->getRekord($id);
	$wynik = $this->wyswietlUrlObrazkaZBasePath($zasob);
	return $wynik;
    }

    public function wyswietlUrlObrazkaZBasePath($zasob) {
	$wynik = $this->wyswietlUrlObrazka($zasob);
	return $this->opakujBasePath($wynik);
    }

    public function wyswietlUrlPlikuZBasePath($zasob) {
	$result = self::uploadToShow;
	if (!empty($zasob->url)) {
	    $result .= $zasob->url;
	}
	return $this->opakujBasePath($result);
    }

    public function wyswietlUrlObrazka($zasob) {
	$result = self::uploadToShow;
	if ($zasob->url !== null && $zasob->typ !== TypZasobu::PLIK) {
	    $result.=$zasob->url;
	} else {
	    $result.=self::ustawDomyslnyObrazek($zasob->typ, $zasob->url);
	}
	return $result;
    }

    public function pobierz_ilosc_zasobow() {
	$sql = "select count(*) as suma from zasoby;";

	$statement = $this->dbAdapter->query($sql);

	$results = $statement->execute();


	$result = $results->current();
	$suma = $result['suma'];
	if ($suma != null) {
	    return $suma;
	} else {
	    return "0";
	}
    }

    public function usunPlik($sciezka) {
	if ($this->czyPlikIstnieje($sciezka)) {
	    unlink($sciezka);
	}
    }

    private static function ustawDomyslnyObrazek($typ, $url) {
	if ($typ === TypZasobu::PLIK) {
	    $poUrl = self::sprawdzRozszerzenie($url);
	    if ($poUrl) {
		return $poUrl;
	    }
	}
	if ($typ !== NULL) {
	    $result = sprintf(self::domyslnyObrazekTypu, $typ);
	    return $result;
	}

	return self::domyslnyObrazek;
    }

    private static function sprawdzRozszerzenie($url) {
	$last = substr($url, -3);
	switch ($last) {
	    case 'pdf':
	    case 'zip':
	    case 'doc':
		return sprintf(self::domyslnyObrazekTypu, $last);
	}
	return false;
    }

    /**
     * Resize an image and keep the proportions
     * @author Allison Beckwith <allison@planetargon.com>
     * @param string $filename
     * @param integer $max_width
     * @param integer $max_height
     * @return image
     */
    private function zmiejszObrazekZachowujacProporcje($filename, $max_dimension = 100) {

	list($uploadWidth, $uploadHeight, $uploadType) = getimagesize($filename);

	$srcImage = $this->getOdpowiedniTypObrazka($filename);

	$new_height = $new_width = $max_dimension;
	$this->getNewDimensions($uploadWidth, $uploadHeight, $max_dimension, $new_width, $new_height);

	$targetImage = imagecreatetruecolor($new_width, $new_height);
	imagealphablending($targetImage, false);
	imagesavealpha($targetImage, true);

	imagecopyresampled($targetImage, $srcImage, 0, 0, 0, 0, $new_width, $new_height, $uploadWidth, $uploadHeight);
	return $targetImage;
    }

    private function getOdpowiedniTypObrazka($file) {
	$extension = strtolower(strrchr($file, '.'));

	switch ($extension) {
	    case '.jpg':
	    case '.jpeg':
		$img = @imagecreatefromjpeg($file);
		break;
	    case '.gif':
		$img = @imagecreatefromgif($file);
		break;
	    case '.png':
		$img = @imagecreatefrompng($file);
		break;
	    default:
		$img = false;
		break;
	}

	return $img;
    }

    private function getNewDimensions($orig_width, $orig_height, $max_dimension, &$width, &$height) {

	$width = $orig_width;
	$height = $orig_height;

	$max_height = $max_width = $max_dimension;

	# taller
	if ($height > $max_height) {
	    $width = ($max_height / $height) * $width;
	    $height = $max_height;
	}

	# wider
	if ($width > $max_width) {
	    $height = ($max_width / $width) * $height;
	    $width = $max_width;
	}
    }

    public function pobierzPodglad($podglad_id) {
	$zasob = $this->getRekord($podglad_id);
	return $zasob;
    }

    public function sprawdzCzyIstniejeZasob($nazwa) {
	return !!$this->pobierzZWheremCount("nazwa = '$nazwa'");
    }

    private function czyPlikIstnieje($sciezka) {
	return file_exists($sciezka);
    }

}
