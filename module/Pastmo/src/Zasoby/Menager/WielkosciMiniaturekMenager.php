<?php

namespace Pastmo\Zasoby\Menager;

class WielkosciMiniaturekMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Zasoby\KonfiguracjaModulu::WIELKOSCI_MINIATUREK_GATEWAY);
    }
    
}
