<?php

namespace Pastmo\Zasoby\Menager;

use Pastmo\Wspolne\Utils\StrUtil;
use Pastmo\Zasoby\Entity\Zasob;

class ZasobyKonwerterNapisowMenager extends ZasobyMenager {

    const START = '<img src="';
    const STOP_SRC = '"';
    const STOP_CALOSCI = '>';
    const FORMAT_IMG = '!(%s)[%s]';

    private $wynikPlikiZNapisu;
    private $offset;
    private $zrodlo;
    private $zasobyTable;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm);
	$this->zasobyTable = $this->get(\Zasoby\Menager\ZasobyUploadMenager::class);
    }

    public function konwertujZasobyZNapisu($string) {
	$plikiZNapisu = $this->wydobadzBase64PlikiZNapisu($string);

	foreach ($plikiZNapisu as $plikZNapisu) {
	    $this->zapiszBase64PlikiZNapisu($plikZNapisu);
	    $plikZNapisu->zasob = $this->zasobyTable->getPoprzednioDodany();
	    $string = $this->aktualizujNapis($string, $plikZNapisu);
	}

	return $string;
    }

    public function wydobadzBase64PlikiZNapisu($zrodlo) {
	$this->wynikPlikiZNapisu = [];
	$this->offset = 0;
	$this->zrodlo = $zrodlo;

	$this->wydobadzPojedynczyBase64PlikiZNapisu();


	return $this->wynikPlikiZNapisu;
    }

    private function wydobadzPojedynczyBase64PlikiZNapisu() {
	$pozycja = strpos($this->zrodlo, self::START, $this->offset);
	if ($pozycja !== false) {
	    $this->offset = $pozycja;
	    $calyLink = StrUtil::substr_wlacznie(self::START, $this->zrodlo, self::STOP_CALOSCI, $this->offset);
	    if ($calyLink) {
		$plikZNapisu = PlikZNapisu::fromStr($calyLink);
		if ($plikZNapisu) {
		    $this->wynikPlikiZNapisu[] = $plikZNapisu;
		}
	    }
	    $this->offset++;
	    $this->wydobadzPojedynczyBase64PlikiZNapisu();
	}
    }

    public function zapiszBase64PlikiZNapisu(PlikZNapisu $plikZNapisu) {
	$decoded = base64_decode($plikZNapisu->base64);
	$this->zasobyTable->zapiszPlik("file." . $plikZNapisu->typ, $decoded);
    }

    public function aktualizujNapis($input, PlikZNapisu $plikZNapisu) {
	$link = $this->zrobKodLinka($plikZNapisu->zasob);
	$wynik = str_replace($plikZNapisu->zrodlo, $link, $input);
	return $wynik;
    }

    public function zrobKodLinka(Zasob $zasob) {
	$wynik = sprintf(self::FORMAT_IMG, $zasob->nazwa, $zasob->id);
	return $wynik;
    }

}

class PlikZNapisu {

    public $typ;
    public $base64;
    public $zasob;
    public $zrodlo;

    public function setTyp($typ) {
	$this->typ = $typ;
	return $this;
    }

    public function setBase64($base64) {
	$this->base64 = $base64;
	return $this;
    }

    public function setZasob($zasob) {
	$this->zasob = $zasob;
	return $this;
    }

    public function setZrodlo($zrodlo) {
	$this->zrodlo = $zrodlo;
	return $this;
    }

    public static function create() {
	return new PlikZNapisu();
    }

    public static function fromStr($calyLink) {
	$zrodlo=  StrUtil::substr(ZasobyKonwerterNapisowMenager::START, $calyLink, ZasobyKonwerterNapisowMenager::STOP_SRC);
	$wynik = self::create();

	$part = explode('base64,', $zrodlo);
	$rozrzerzenie = StrUtil::substr('image/', $zrodlo, ';');
	if (count($part) < 2 || !$rozrzerzenie) {
	    return false;
	}
	$wynik->setBase64($part[1]);
	$wynik->setTyp($rozrzerzenie);
	$wynik->setZrodlo($calyLink);

	return $wynik;
    }

}
