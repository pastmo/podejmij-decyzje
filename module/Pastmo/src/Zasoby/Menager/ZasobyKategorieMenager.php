<?php 

namespace Pastmo\Zasoby\Menager;

class ZasobyKategorieMenager extends ZasobyMenager{

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm);
	$this->zasobyTable = $this->get(\Zasoby\Menager\ZasobyUploadMenager::class);
    }
    
}
