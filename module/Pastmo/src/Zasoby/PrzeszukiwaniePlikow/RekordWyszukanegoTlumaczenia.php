<?php

namespace Pastmo\Zasoby\PrzeszukiwaniePlikow;

class RekordWyszukanegoTlumaczenia {

    public $tekst;
    public $plik;
    public $domkniecie;
    public $nr;
    public $cudzyslow;

    public static function create() {
	return new RekordWyszukanegoTlumaczenia();
    }

    function setTekst($tekst) {
	$this->tekst = $tekst;
	return $this;
    }

    function setPlik($plik) {
	$this->plik = $plik;
	return $this;
    }

    function setDomkniecie($domkniecie) {
	$this->domkniecie = $domkniecie;
	return $this;
    }

    function setNr($nr) {
	$this->nr = $nr;
	return $this;
    }

    function setCudzyslow($cudzyslow) {
	$this->cudzyslow = $cudzyslow;
	return $this;
    }

}
