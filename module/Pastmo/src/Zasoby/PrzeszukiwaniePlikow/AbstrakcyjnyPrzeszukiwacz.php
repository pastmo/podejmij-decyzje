<?php

namespace Pastmo\Zasoby\PrzeszukiwaniePlikow;

abstract class AbstrakcyjnyPrzeszukiwacz {

    protected $basePath;
    protected $searched;
    protected $znalezionePliki = [];
    public $wynik = [];

    abstract public function wydobadzZawartosc();

    protected function getIgnorowanePliki() {
	return array(
		'.',
		'..',
		'test');
    }

    public function przeszukaj() {
	$this->wynik = [];
	$this->pobierzTablicePlikow($this->basePath);
	$this->wydobadzZawartosc();

	return $this->wynik;
    }

    public function pobierzTablicePlikow($dir) {
	if (is_dir($dir)) {

	    $pliki = scandir($dir);
	    foreach ($pliki as $plik) {
		if (!in_array($plik, $this->getIgnorowanePliki())) {

		    $this->pobierzTablicePlikow($this->nazwaPliku($dir, $plik));
		}
	    }
	} else {
	    $this->znalezionePliki[$dir] = $dir;
	}

	return $this->znalezionePliki;
    }

    protected function nazwaPliku($dir, $filename) {
	return "$dir\\$filename";
    }

    public function setBasePath($basePath) {
	$this->basePath = $basePath;
	return $this;
    }

    public function setSearched($searched) {
	$this->searched = $searched;
	return $this;
}

}
