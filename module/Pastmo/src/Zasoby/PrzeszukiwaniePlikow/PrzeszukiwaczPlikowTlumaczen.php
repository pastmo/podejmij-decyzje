<?php

namespace Pastmo\Zasoby\PrzeszukiwaniePlikow;

use Pastmo\Wspolne\Utils\ArrayUtil;

class PrzeszukiwaczPlikowTlumaczen extends AbstrakcyjnyPrzeszukiwacz {

    const TRANSLATE_JS = "._translate(";
    const TRANSLATE_PHP = ">_translate(";

    private $ignorowane;
    private $poczatek_przeszukiwania;

    public function __construct() {
	;
    }

    protected function getIgnorowanePliki() {
	if (!$this->ignorowane) {
	    $wspolne = parent::getIgnorowanePliki();
	    $this->ignorowane = array_merge(
		    ['plugins', 'lib', 'google-code-prettify', 'PrzeszukiwaczPlikowTlumaczen.php', 'TlumaczeniaHelper.php'],
		    $wspolne);
	}
	return $this->ignorowane;
    }

    public function wydobadzZawartosc() {
	foreach ($this->znalezionePliki as $plik) {
	    $this->obsluzPojedynczyPlik($plik);
	}
    }

    public function obsluzPojedynczyPlik($plik) {
	$tresc = $this->pobierzTrescPliku($plik);
	$tablicaTresci = explode($this->searched, $tresc);

	for ($i = 1; $i < count($tablicaTresci); $i++) {
	    $tr = $tablicaTresci[$i];
	    $znakCudzyslowa = $tr[0];

	    $tekst = $this->pobierzTresc($tr, $znakCudzyslowa);

	    $wnetrzeFunckji = $this->pobierzWnetrzeFunkcji($tr, $znakCudzyslowa);
	    $parametry = explode(",", $wnetrzeFunckji);


	    $domkniecie = ArrayUtil::pobierzElementKontenera($parametry, 1);
	    $nr = ArrayUtil::pobierzElementKontenera($parametry, 2);

	    $rekord = RekordWyszukanegoTlumaczenia::create()
		    ->setTekst($tekst)
		    ->setPlik($plik)
		    ->setDomkniecie($domkniecie)
		    ->setNr($nr)
		    ->setCudzyslow($znakCudzyslowa);
	    $this->wynik[] = $rekord;
	}
    }

    private function pobierzTresc($tr,$znakCudzyslowa) {
	$this->poczatek_przeszukiwania = mb_strpos($tr, $znakCudzyslowa, 1);
	$wnetrzeFunckji = mb_substr($tr, 1, $this->poczatek_przeszukiwania - 1);
	return $wnetrzeFunckji;
    }

    private function pobierzWnetrzeFunkcji($tr, $znakCudzyslowa) {
	$koniecFunkcji = mb_strpos($tr, ")", $this->poczatek_przeszukiwania);
	$wnetrzeFunckji = mb_substr($tr, $this->poczatek_przeszukiwania, $koniecFunkcji - $this->poczatek_przeszukiwania);

//	if ($this->czyZawieraWewnetrzneOtwarcieNawiasu($wnetrzeFunckji)) {
//	    $koniecFunkcji = mb_strpos($tr, ")", $koniecFunkcji);
//	    $wnetrzeFunckji = mb_substr($tr, 1, $koniecFunkcji);
//	}

	return $wnetrzeFunckji;
    }

    private function czyZawieraWewnetrzneOtwarcieNawiasu($wnetrzeFunckji) {
	return mb_strpos($wnetrzeFunckji, "(", 0);
    }

    private function usunKoncowyCudzyslow($tekst, $znakCudzyslowa) {
	return str_replace($znakCudzyslowa, "", $tekst);
    }

    public function pobierzTrescPliku($plik) {
	return file_get_contents($plik);
    }

    public static function create() {
	return new PrzeszukiwaczPlikowTlumaczen();
    }

}
