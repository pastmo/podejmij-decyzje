<?php

namespace Pastmo\Zasoby\PrzeszukiwaniePlikow;

class PrzeszukiwaczPlikow extends AbstrakcyjnyPrzeszukiwacz {

    const WYSLIJ_POST = "Pastmo.wyslijPost(";
    const WYSLIJ_GET = "Pastmo.wyslijGet(";

    public function wydobadzZawartosc() {
	foreach ($this->znalezionePliki as $plik) {
	    $tresc = file_get_contents($plik);
	    $tablicaTresci = explode($this->searched, $tresc);

	    for ($i = 1; $i < count($tablicaTresci); $i++) {
		$tr = $tablicaTresci[$i];
		$apostrof = $tr[0];

		$pos = strpos($tr, "$apostrof", 1);

		$this->wynik[] = str_replace('-', '_', substr($tr, 1, $pos - 1));
	    }
	}
    }

    public static function create() {
	return new PrzeszukiwaczPlikow();
    }

}
