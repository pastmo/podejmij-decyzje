<?php

namespace Pastmo\Tlumaczenia\Entity;

class TlumaczenieSlownik extends \Wspolne\Model\WspolneModel {

    public $id;
    public $slownik_id;
    public $uzytkownik_id;
    public $tlumaczenie_nr;
    public $tekst;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->data = $data;

	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->slownik_id = $this->pobierzLubNull($data, 'slownik_id');
	$this->uzytkownik_id = $this->pobierzLubNull($data, 'uzytkownik_id');
	$this->tlumaczenie_nr = $this->pobierzLubNull($data, 'tlumaczenie_nr');
	$this->tekst = $this->pobierzLubNull($data, 'tekst');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'tlumaczenie':
		return 'tlumaczenie_nr';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function getEncjeTabeliObcej($name) {
	switch ($name) {
	    case 'tlumaczenie':
		return $this->getEncjeZTabeliObcej('tlumaczenie_nr', \Pastmo\Tlumaczenia\Menager\TlumaczeniaMenager::class,
				new Tlumaczenie(), 'tlumaczenie', 'pobierzPoNumerze');
	}
    }

    public function getTekst() {
	if ($this->tekst !== null) {
	    return $this->tekst;
	} else {
	    return $this->tlumaczenie->oryginalny_tekst;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
