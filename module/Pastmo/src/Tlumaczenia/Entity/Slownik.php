<?php

namespace Pastmo\Tlumaczenia\Entity;

class Slownik extends \Wspolne\Model\WspolneModel {

    public $id;
    public $jezyk_kod;
    public $nazwa;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->data = $data;

	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->jezyk_kod = $this->pobierzLubNull($data, 'jezyk_kod');
	$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
