<?php

namespace Pastmo\Tlumaczenia\Entity;

class Tlumaczenie extends \Wspolne\Model\WspolneModel {

    public $id;
    public $nr;
    public $oryginalny_tekst;
    public $plik;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->data = $data;

	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->nr = $this->pobierzLubNull($data, 'nr');
	$this->plik = $this->pobierzLubNull($data, 'plik');
	$this->oryginalny_tekst = $this->pobierzLubNull($data, 'oryginalny_tekst');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
