<?php

namespace Pastmo\Tlumaczenia\Fasada;

class TlumaczeniaFasada extends \Wspolne\Fasada\WspolneFasada {

    public function getTlumaczenieZBiezacegoSlownika($nr) {
	return $this->get(\Pastmo\Tlumaczenia\Menager\TlumaczeniaSlownikiMenager::class)
			->getTlumaczenieZBiezacegoSlownika($nr);
    }

    public function zapiszTlumaczenie($form) {
	$this->get(\Pastmo\Tlumaczenia\Menager\TlumaczeniaSlownikiMenager::class)
		->zapiszTlumaczenie($form);
    }

    public function getDostepneSlowniki() {
	return $this->get(\Pastmo\Tlumaczenia\Menager\SlownikiMenager::class)->fetchAll('domyslny desc');
    }

    public function ustawBiezacySlownik($slownikId) {
	return $this->get(\Logowanie\Menager\UzytkownicyMenager::class)->zmienBiezacySlownik($slownikId);
    }

    public function aktualizujCyklicznie() {
	$this->get(\Pastmo\Tlumaczenia\Menager\SlownikiMenager::class)->aktualizujCyklicznie();
    }

}
