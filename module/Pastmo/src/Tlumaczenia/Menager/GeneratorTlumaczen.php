<?php

namespace Pastmo\Tlumaczenia\Menager;

use Pastmo\Zasoby\PrzeszukiwaniePlikow\PrzeszukiwaczPlikowTlumaczen;
use Pastmo\Zasoby\PrzeszukiwaniePlikow\RekordWyszukanegoTlumaczenia;

class GeneratorTlumaczen extends \Pastmo\Wspolne\Menager\WspolnyMenager {

    protected $class;
    protected $metody = array();
    private $tlumaczeniaMenager = false;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm);
    }

    public function generuj() {
	$plikiPhp = PrzeszukiwaczPlikowTlumaczen::create()
		->setBasePath(dirname(__FILE__) . '/../../../../../module')
		->setSearched(PrzeszukiwaczPlikowTlumaczen::TRANSLATE_PHP)
		->przeszukaj();
	$this->obsluzTlumaczenia($plikiPhp);

	$plikiJs = PrzeszukiwaczPlikowTlumaczen::create()
		->setBasePath(dirname(__FILE__) . '/../../../../../public_html/js')
		->setSearched(PrzeszukiwaczPlikowTlumaczen::TRANSLATE_JS)
		->przeszukaj();
	$this->obsluzTlumaczenia($plikiJs);
    }

    public function obsluzTlumaczenia($rekordy) {
	foreach ($rekordy as $rekord) {
	    $this->obsluzPojedynczeTlumaczenie($rekord);
	}
    }

    public function obsluzPojedynczeTlumaczenie(RekordWyszukanegoTlumaczenia $rekord) {

	if (!$rekord->nr) {
	    $tlumaczenie = new \Pastmo\Tlumaczenia\Entity\Tlumaczenie();
	    $tlumaczenie->oryginalny_tekst = $rekord->tekst;
	    $tlumaczenie->plik = $rekord->plik;
	    $this->getTlumaczeniaMenager()->zapisz($tlumaczenie);
	    $this->dodaneTlumaczenie = $this->getTlumaczeniaMenager()->getPoprzednioDodany();
	    $this->dodajInsertUpdate();
	    $this->modyfikujPlikZrodlowy($rekord, $this->dodaneTlumaczenie->nr);
	} else {
	    $tlumaczenie = $this->getTlumaczeniaMenager()->pobierzPoNumerze($rekord->nr);

	    if ($tlumaczenie->oryginalny_tekst !== $rekord->tekst) {
		$tlumaczenie->oryginalny_tekst = $rekord->tekst;
		$this->getTlumaczeniaMenager()->zapisz($tlumaczenie);
		$this->dodajUpdateZapisanych($rekord->tekst, $rekord->nr);
	    }
	}
    }

    public function dodajUpdateZapisanych($napis, $nr) {
	$tekst = $this->escaptujApostrofy($napis);
	$sql = "UPDATE `tlumaczenia` SET `oryginalny_tekst`='$tekst' WHERE  `nr`=$nr;" . PHP_EOL;
	$this->zapiszInsertUpdate($sql);
    }

    public function dodajInsertUpdate() {
	$tekst = $this->escaptujApostrofy($this->dodaneTlumaczenie->oryginalny_tekst);
	$sql = "INSERT INTO `tlumaczenia` (`nr`, `oryginalny_tekst`,`plik`) VALUES ({$this->dodaneTlumaczenie->nr}, '{$tekst}', '{$this->dodaneTlumaczenie->plik}');" . PHP_EOL;
	$this->zapiszInsertUpdate($sql);
    }

    public function zapiszInsertUpdate($tekst) {
	$file = dirname(__FILE__) . '/../../../../../baza/update/update_tlumaczen.sql';
	$current = $this->pobierzTrescPliku($file);
	$current .= $tekst;
	$this->zapiszPlik($file, $current);
    }

    public function dodajTlumaczeniaSlownikiAngielskie() {

	$translator = $this->sm->get('MvcTranslator');
	$translator->setLocale(\Pastmo\Uzytkownicy\Enumy\KodyJezykow::ANGIELSKI);

	$tlumaczenia = $this->getTlumaczeniaMenager()->fetchAll("id ASC");
	$file = dirname(__FILE__) . '/../../../../../baza/update/update_tlumaczen_slownikow.sql';
	$current = $this->pobierzTrescPliku($file);

	foreach ($tlumaczenia as $tlumaczenie) {

	    $szukany = $tlumaczenie->oryginalny_tekst;
	    $czyNawias = strpos($szukany, '(');

	    if ($czyNawias !== false) {
		$szukany.=")";
	    }

	    $tekst = $translator->translate($szukany);
	    if ($tekst !== $szukany) {
		$tekst = $this->escaptujApostrofy($tekst);
		$insert = "INSERT INTO `tlumaczenia_slowniki` (`slownik_id`, `tlumaczenie_nr`, `tekst`) VALUES (2, {$tlumaczenie->nr}, '$tekst');" . PHP_EOL;
		$current.=$insert;
	    }
	}

	$this->zapiszPlik($file, $current);
    }

    private function escaptujApostrofy($tekst) {
	return str_replace("'", "\'", $tekst);
    }

    public function pobierzTrescPliku($sciezka) {
	return file_get_contents($sciezka);
    }

    public function zapiszPlik($sciezka, $tresc) {
	file_put_contents($sciezka, $tresc);
    }

    public function modyfikujPlikZrodlowy(RekordWyszukanegoTlumaczenia $rekord, $nr) {

	$tresc = $this->pobierzTrescPliku($rekord->plik);

	$szukany = "_translate({$rekord->cudzyslow}{$rekord->tekst}{$rekord->cudzyslow}";

	$start = 0;

	$start = $this->wyliczStart($tresc, $szukany, $start);

	for (; $start !== false; $start = $this->wyliczStart($tresc, $szukany, $start)) {

	    $koniecFunkcji = strpos($tresc, ")", $start);
	    $zawartosc = substr($tresc, $start, $koniecFunkcji - $start);
	    $parametry = explode(',', $zawartosc);

	    $dodawanaTresc = "";

	    if (count($parametry) === 1) {
		$dodawanaTresc.=", false";
	    } else if (count($parametry) === 3) {
		continue;
	    } else {
		$start = $koniecFunkcji;
	    }

	    $dodawanaTresc.= ", {$nr}";

	    $nowaTresc = substr_replace($tresc, $dodawanaTresc, $start, 0);

	    $this->zapiszPlik($rekord->plik, $nowaTresc);
	    break;
	}
    }

    public function updatujPilkiTlumaczen() {
	$slowniki = $this->get(\Pastmo\Tlumaczenia\Menager\SlownikiMenager::class)->fetchAll();

	foreach ($slowniki as $slownik) {
	    $kod = $slownik->jezyk_kod;
	    $sciezka = $this->getSciezkeDoPlikuPo($kod);
	    $trescPliku = $this->pobierzTrescPliku($this->getSciezkeDoPlikuPo("szablon"));

	    $this->get(\Zasoby\Menager\ZasobyUploadMenager::class)->usunPlik($sciezka);

	    $tlumaczeniaSlowniki = $this->get(TlumaczeniaSlownikiMenager::class)->getTlumaczeniaZeSlownika($slownik->id);

	    foreach ($tlumaczeniaSlowniki as $tlumaczenieSlownik) {
		$trescPliku.=$this->getTekstTlumaczenia($tlumaczenieSlownik->tlumaczenie_nr, $tlumaczenieSlownik->tekst,
			$tlumaczenieSlownik->id);
	    }

	    $this->zapiszPlik($sciezka, $trescPliku);

	    $this->kompilujDoMo($kod);
	}
    }

    private function getSciezkeDoPlikuPo($kod) {
	return $this->getSciezkeTlumaczen($kod, 'po');
    }

    private function getSciezkeDoPlikuMo($kod) {
	return $this->getSciezkeTlumaczen($kod, 'mo');
    }

    private function getSciezkeTlumaczen($kod, $rozszerzenie) {
	return dirname(__FILE__) . "/../../../../../data/languages/$kod.$rozszerzenie";
    }

    private function getTekstTlumaczenia($nr, $tresc, $scieza) {
	$komentarz = "tlumaczenie_slownik.id=$scieza";
	return '#: ' . $komentarz . '
msgid "' . $nr . '"
msgstr "' . $tresc . '"' . PHP_EOL . PHP_EOL;
    }

    public function kompilujDoMo($kod) {
	$po = $this->getSciezkeDoPlikuPo($kod);
	$mo = $this->getSciezkeDoPlikuMo($kod);

	$komenda="msgfmt $po -o $mo";
	$wynik = $this->wykonajMsgfmt($komenda);

	$builder = \Pastmo\Logi\Builder\LogiBuilder::getBuilder()
		->addTresc('msg', __METHOD__)
		->addTresc('komenda', $komenda)
		->addTresc('komunikat', $wynik)
		->setKodKategorii("update_tlum")
	;
	$this->get(\Wspolne\Menager\LogiMenager::class)->dodajLog($builder);
    }

    public function wykonajMsgfmt($komenda) {
	return exec($komenda);
    }

    private function wyliczStart($tresc, $szukany, $start) {
	$pos = strpos($tresc, $szukany, $start);

	if ($pos) {
	    $pos+= strlen($szukany);
	    return $pos;
	}
	return false;
    }

    private function getTlumaczeniaMenager() {
	if (!$this->tlumaczeniaMenager) {
	    $this->tlumaczeniaMenager = $this->get(TlumaczeniaMenager::class);
	}
	return $this->tlumaczeniaMenager;
    }

}
