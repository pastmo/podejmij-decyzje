<?php

namespace Pastmo\Tlumaczenia\Menager;

class PastmoTranslator extends \Pastmo\Wspolne\Menager\WspolnyMenager {

    const SZABLON_EDYCJI = ' <i class="fa fa-pencil edycja_tekstu" data-nr="%s"></i>';

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	$this->translator = $sm->get('MvcTranslator');
    }

    public function translate($tekst, $domkniecie = false, $nr = null) {
	$tlumaczenie = $this->translator->translate($nr);

	if($tlumaczenie==$nr){
	    $tlumaczenie=$tekst;
	}

	if ($domkniecie) {
	    return $this->domknijIDodajParametr($tlumaczenie, $domkniecie, $nr);
	} else {
	    return $this->opakujTlumaczenie($tlumaczenie, $nr);
	}
    }

    public function getLocale() {
	return $this->translator->getLocale();
    }

    private function domknijIDodajParametr($msg, $znak, $nr) {
	$wynik = $msg . $znak . " data-nr_tlumaczenia=$znak$nr";
	return $wynik;
    }

    private function opakujTlumaczenie($msg, $nr) {
	return $msg . sprintf(self::SZABLON_EDYCJI, $nr);
    }

}
