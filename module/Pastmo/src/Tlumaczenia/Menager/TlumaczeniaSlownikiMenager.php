<?php

namespace Pastmo\Tlumaczenia\Menager;

use Pastmo\Wspolne\Utils\EntityUtil;

class TlumaczeniaSlownikiMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Tlumaczenia\KonfiguracjaModulu::TLUMACZNIE_SLOWNIK_GATEWAY);
    }

    public function zapisz(\Pastmo\Wspolne\Entity\WspolnyModel $model) {
	$model->uzytkownik_id = $this->getLogowanieFasada()->getZalogowanegoUsera();
	parent::zapisz($model);
    }

    public function getTlumaczenieZBiezacegoSlownika($nr) {
	$slownik = $this->get(SlownikiMenager::class)->getAktualnySlownikZalogowanego();

	$wynik = $this->pobierzZWherem("slownik_id={$slownik->id} AND tlumaczenie_nr=$nr");
	if (count($wynik) > 0) {
	    return $wynik[0];
	} else {
	    $nowe = new \Pastmo\Tlumaczenia\Entity\TlumaczenieSlownik($this->sm);
	    $nowe->ustawPole('tlumaczenie_nr',$nr);
	    $nowe->slownik_id = $slownik->id;
	    return $nowe;
	}
    }

    public function getTlumaczeniaZeSlownika($slownikId) {
	$wynik = $this->pobierzZWherem("slownik_id={$slownikId}", self::CHRONOLOGICZNY_ORDER);
	return $wynik;
    }

    public function zapiszTlumaczenie($form) {
	$nr = $form->tlumaczenie_nr;
	$tlumaczenie = $this->getTlumaczenieZBiezacegoSlownika($nr);
	$tlumaczenie->tekst = $form->tekst;
	$this->zapisz($tlumaczenie);

	$this->get(SlownikiMenager::class)->updatujPilkiTlumaczen();

    }

}
