<?php

namespace Pastmo\Tlumaczenia\Menager;

class TlumaczeniaMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Tlumaczenia\KonfiguracjaModulu::TLUMACZNIE_GATEWAY);
    }

    public function zapisz(\Pastmo\Wspolne\Entity\WspolnyModel $model) {
	if (!\Pastmo\Wspolne\Utils\EntityUtil::czyEncjaZId($model)) {
	    $model->nr = $this->pobierzNrOstatniegoRekordu() + 1;
	}
	parent::zapisz($model);
    }

    public function pobierzNrOstatniegoRekordu() {
	$tlumaczenia = $this->pobierzZWherem("1", self::DOMYSLNY_ORDER, 1);
	if (count($tlumaczenia) > 0) {
	    return $tlumaczenia[0]->nr;
	}
	return 0;
    }

    public function pobierzPoNumerze($nr){
	$rekord= $this->pobierzZWherem("nr=$nr");

	if(count($rekord)>0){
	    return $rekord[0];
}
	return new \Pastmo\Tlumaczenia\Entity\Tlumaczenie($this->sm);
    }

}
