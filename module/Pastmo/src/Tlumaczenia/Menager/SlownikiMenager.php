<?php

namespace Pastmo\Tlumaczenia\Menager;

use Pastmo\Tlumaczenia\Entity\Slownik;

class SlownikiMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    use \Pastmo\Wspolne\Mechanizmy\CykliczneAktualizacjeTrait;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Tlumaczenia\KonfiguracjaModulu::SLOWNIK_GATEWAY);
    }

    public function getAktualnySlownikZalogowanego() {
	$zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();

	if ($zalogowany->slownik->id !== null) {
	    return $this->getRekord($zalogowany->slownik->id);
	} else {
	    return $this->getDomyslnySlownik();
	}
    }

    public function getDomyslnySlownik() {
	$wynik = $this->pobierzZWherem("domyslny=1");

	if (count($wynik) > 0) {
	    return $wynik[0];
	} else {
	    return new Slownik();
	}
    }

    public function updatujPilkiTlumaczen() {
	$generator = new GeneratorTlumaczen($this->sm);
	$generator->updatujPilkiTlumaczen();
    }

    public function getDomyslnaGodzine() {
	return "02:30";
    }

    public function getKodLogu() {
	return "update_tlum";
    }

    public function getKodUstawienGodziny() {
	return "up_tlum";
    }

    public function getNazweMetodyAktualizacji() {
	return 'updatujPilkiTlumaczen';
    }

}
