<?php

namespace Pastmo\Tlumaczenia\Controller;

use Pastmo\Wspolne\Adnotacje\DomyslnyJsonModel;
use Wspolne\Controller\WspolneController;
use Logowanie\Enumy\KodyUprawnien;

/**
 * @DomyslnyJsonModel
 */
class TlumaczeniaAjaxController extends WspolneController {

    public function aktywacjaTrybuEdycjiAction() {
	$this->sprawdzUprawnienie(KodyUprawnien::EDYCJA__TEKSTOW_TLUMACZEN, true);

	$tryb = $this->params()->fromPost('tryb', 0);
	$this->sesjaMenager->setTrybTlumaczen($tryb);

	return $this->returnSuccess(['tryb' => $tryb]);
    }

    public function zapiszTlumaczenieAction() {
	$this->sprawdzUprawnienie(KodyUprawnien::EDYCJA__TEKSTOW_TLUMACZEN, true);

	$form = $this->getPost();
	$this->tlumaczeniaFasada->zapiszTlumaczenie($form);

	return $this->returnSuccess();
    }

    public function zmienSlownikAction() {
	$this->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$id = $this->params()->fromPost('slownik_id');
	$this->tlumaczeniaFasada->ustawBiezacySlownik($id);

	return $this->returnSuccess();
    }

}
