<?php

namespace Pastmo\Tlumaczenia\Controller;

use Wspolne\Controller\WspolneController;
use Logowanie\Enumy\KodyUprawnien;

class TlumaczeniaController extends WspolneController {

    public function edycjaTlumaczeniaAction() {
	$this->sprawdzUprawnienie(KodyUprawnien::EDYCJA__TEKSTOW_TLUMACZEN, true);

	$this->ustawModalLayout();
	$nr = $this->params()->fromQuery('nr', false);
	if ($nr) {
	    $tlumaczenieSlownik = $this->tlumaczeniaFasada->getTlumaczenieZBiezacegoSlownika($nr);
	    return new \Zend\View\Model\ViewModel([
		    'tlumaczenie_slownik' => $tlumaczenieSlownik,
		    'nr' => $nr]);
	}
	throw new \Pastmo\Wspolne\Exception\PastmoException($this->_translate("Nieprzekazany nr tłumaczenia", false, 2226));
    }

}
