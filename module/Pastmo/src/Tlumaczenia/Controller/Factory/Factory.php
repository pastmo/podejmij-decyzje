<?php

namespace Pastmo\Tlumaczenia\Controller\Factory;

class Factory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new \Pastmo\Tlumaczenia\Controller\TlumaczeniaController();
    }

}
