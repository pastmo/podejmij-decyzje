<?php

namespace Pastmo\Tlumaczenia\Controller\Factory;

class AjaxFactory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new \Pastmo\Tlumaczenia\Controller\TlumaczeniaAjaxController();
    }

}
