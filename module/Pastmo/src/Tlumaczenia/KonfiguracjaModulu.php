<?php

namespace Pastmo\Tlumaczenia;

class KonfiguracjaModulu extends \Pastmo\KonfiguracjaModulu {

    use \Pastmo\FunkcjeKonfiguracyjne;

    const SLOWNIK_GATEWAY = "slownik_gateway";
    const TLUMACZNIE_GATEWAY = "tlumacznie_gateway";
    const TLUMACZNIE_SLOWNIK_GATEWAY = "tlumacznie_slownik_gateway";

    public function getFactories() {
	return array(
		Fasada\TlumaczeniaFasada::class => function($sm) {
		    $menager = new Fasada\TlumaczeniaFasada($sm);
		    return $menager;
		},
		Menager\SlownikiMenager::class => function($sm) {
		    $menager = new Menager\SlownikiMenager($sm);
		    return $menager;
		},
		Menager\TlumaczeniaMenager::class => function($sm) {
		    $menager = new Menager\TlumaczeniaMenager($sm);
		    return $menager;
		},
		Menager\TlumaczeniaSlownikiMenager::class => function($sm) {
		    $menager = new Menager\TlumaczeniaSlownikiMenager($sm);
		    return $menager;
		},
		self::SLOWNIK_GATEWAY => function($sm) {
		    return $this->ustawGateway($sm, Entity\Slownik::class, 'slowniki');
		},
		self::TLUMACZNIE_GATEWAY => function($sm) {
		    return $this->ustawGateway($sm, Entity\Tlumaczenie::class, 'tlumaczenia');
		},
		self::TLUMACZNIE_SLOWNIK_GATEWAY => function($sm) {
		    return $this->ustawGateway($sm, Entity\TlumaczenieSlownik::class, 'tlumaczenia_slowniki');
		}
	);
    }

    public function onBootstrap(\Zend\Mvc\MvcEvent $e) {
	parent::onBootstrap($e);
    }

    public static function dodajKontrolery($array) {
	$array['controllers']['factories']['Pastmo\Tlumaczenia\Controller\TlumaczeniaAjax'] = \Pastmo\Tlumaczenia\Controller\Factory\AjaxFactory::class;
	$array['controllers']['factories']['Pastmo\Tlumaczenia\Controller\Tlumaczenia'] = \Pastmo\Tlumaczenia\Controller\Factory\Factory::class;

	$array['router']['routes']['tlumaczenia_ajax'] = array(
		'type' => 'segment',
		'options' => array(
			'route' => '/tlumaczenia_ajax[/:action][/:id]',
			'constraints' => array(
				'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
				'id' => '[0-9]+',
			),
			'defaults' => array(
				'controller' => 'Pastmo\Tlumaczenia\Controller\TlumaczeniaAjax',
				'action' => 'index',
			),
		),
	);
	$array['router']['routes']['tlumaczenia'] = array(
		'type' => 'segment',
		'options' => array(
			'route' => '/tlumaczenia[/:action][/:id]',
			'constraints' => array(
				'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
				'id' => '[0-9]+',
			),
			'defaults' => array(
				'controller' => 'Pastmo\Tlumaczenia\Controller\Tlumaczenia',
				'action' => 'index',
			),
		),
	);
	return $array;
    }

    public static function dowiazMenagery(\Wspolne\Controller\Factory\InitObiekt $initObiekt,
	    \Interop\Container\ContainerInterface $container) {
	$initObiekt->tlumaczeniaFasada = $container->get(Fasada\TlumaczeniaFasada::class);
    }

}
