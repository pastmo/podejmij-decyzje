<?php

namespace Pastmo\Firmy\Entity;

class Adres extends \Pastmo\Wspolne\Entity\Adres {

    public $nazwa;
    public $osoba_kontaktowa;
    public $nr_domu;
    public $nr_lokalu;
    public $telefon;
    public $email;
    public $akceptacja;
    public $typ;

    public function exchangeArray($data) {
	parent::exchangeArray($data);

	$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
	$this->osoba_kontaktowa = $this->pobierzLubNull($data, 'osoba_kontaktowa');
	$this->nr_domu = $this->pobierzLubNull($data, 'nr_domu');
	$this->nr_lokalu = $this->pobierzLubNull($data, 'nr_lokalu');
	$this->telefon = $this->pobierzLubNull($data, 'telefon');
	$this->email = $this->pobierzLubNull($data, 'email');
	$this->akceptacja = $this->pobierzLubNull($data, 'akceptacja', false);
	$this->typ = $this->pobierzLubNull($data, 'typ');
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
