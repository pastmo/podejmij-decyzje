<?php

namespace Pastmo\Firmy\Entity;

use Zasoby\Model\Zasob;

class Firma extends \Wspolne\Model\WspolneModel {

    public $id;
    public $adres_wysylki_id;
    public $adres_faktury_id;
    public $nazwa;
    public $data_dodania;
    public $img_id;
    public $nip;
    protected $uzytkownicy;

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
	$this->data_dodania = $this->pobierzLubNull($data, 'data_dodania');
	$this->nip = $this->pobierzLubNull($data, 'nip');
	$this->adres_faktury_id = $this->pobierzLubNull($data, 'adres_faktury_id');
	$this->adres_wysylki_id = $this->pobierzLubNull($data, 'adres_wysylki_id');
	$this->img_id = $this->pobierzLubNull($data, 'img_id');

	$this->data = $data;
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'adres_faktury':
		return 'adres_faktury_id';
	    case 'adres_wysylki':
		return 'adres_wysylki_id';
	    case 'img':
		return 'img_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function getEncjeTabeliObcej($name) {
	switch ($name) {
	    case 'adres_faktury':
		return $this->getEncjeZTabeliObcej('adres_faktury_id', \Firmy\Menager\AdresyMenager::class, new Adres());
	    case 'adres_wysylki':
		return $this->getEncjeZTabeliObcej('adres_wysylki_id', \Firmy\Menager\AdresyMenager::class, new Adres());
	    case 'img':
		return $this->getEncjeZTabeliObcej('img_id', \Zasoby\Menager\ZasobyUploadMenager::class,
				Zasob::zrobDomyslnyZasobAvatara());
	}
    }

    public function getUzytkownicy() {
	$this->pobierzListeZTabeliObcej(\Firmy\Menager\FirmyMenager::class, 'pobierzUzytkownikowFirmy', $this->id,
		false, 'uzytkownicy');

	return $this->uzytkownicy;
    }

    public function czySaPrzypisaniUzytkownicy() {
	return count($this->getUzytkownicy()) > 0;
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public function __toString() {
	if ($this->nazwa) {
	    return $this->nazwa;
	} else {
	    return "";
	}
    }

    public static function create($sm = null) {
	$encja = new Firma($sm);
	$encja->img = Zasob::zrobDomyslnyZasobAvatara();
	$encja->adres_wysylki = new Adres();
	$encja->adres_faktury = new Adres();
	$encja->domyslny_sprzedawca = \Logowanie\Model\Uzytkownik::create($sm);
	return $encja;
    }

}
