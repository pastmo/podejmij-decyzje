<?php

namespace Pastmo\Platnosci\Enumy;

class PlatnosciStatusy {

    const NOWA = 'nowa';
    const ZAREJESTROWANA = 'zarejestrowana';
    const W_TRAKCIE = 'w_trakcie';
    const ZAKONCZONA = 'zakonczona';
    const BLAD = 'blad';

}
