<?php

namespace Pastmo\Platnosci\Menager;

class CennikMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    protected $wyszukiwanieZKontem = false;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Platnosci\KonfiguracjaModulu::CENNIK_GATEWAY);
    }

    public function pobierzDomyslnyCennik() {
	$wynik = $this->pobierzZWherem("1", self::CHRONOLOGICZNY_ORDER);
	return $wynik[0];
    }

}
