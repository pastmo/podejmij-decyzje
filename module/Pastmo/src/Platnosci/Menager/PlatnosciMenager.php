<?php

namespace Pastmo\Platnosci\Menager;

use Pastmo\Platnosci\Entity\Platnosc;
use Pastmo\Platnosci\Utils\Przelewy24;
use Pastmo\Platnosci\Utils\ObslugaP24;
use Pastmo\Platnosci\Enumy\PlatnosciStatusy;

class PlatnosciMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    private $platnosc;
    private $post;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Platnosci\KonfiguracjaModulu::PLATNOSCI_GATEWAY);
    }

    public function utworzPlatnoscDlaUzytkownika($uzytkownik, $cennik) {

	try {

	    return $this->utworzPlatnoscDlaUzytkownikaTry($uzytkownik, $cennik);
	} catch (\Pastmo\Platnosci\Exception\PlatnosciException $e) {
	    $this->obsluzWyjatek($e);
	    throw $e;
	} catch (\Exception $e) {
	    $this->obsluzWyjatek($e);
	    throw new \Pastmo\Platnosci\Exception\PlatnosciException($this->_translate('Wystąpił nieznany wyjątek'));
	}
    }

    public function obsluzPotwierdzeniePlatnosci($post) {
	try {

	    $this->obsluzPotwierdzeniePlatnosciTry($post);
	} catch (\Pastmo\Platnosci\Exception\PlatnosciException $e) {
	    $this->obsluzWyjatek($e);
	} catch (\Exception $e) {
	    $this->obsluzWyjatek($e);
	}
    }

    private function obsluzPotwierdzeniePlatnosciTry($post) {
	$this->logujOtrzymanegoPosta($post);
	$this->zapiszParametryPotwierdzenia($post);
	$this->wyslijPotwierdzenieTransakci();
	$this->przedluzKonto();
    }

    private function logujOtrzymanegoPosta($post) {
	$this->logujKrok('post', $post);
    }

    private function logujKrok($klucz, $tresc) {
	$builder = \Pastmo\Logi\Builder\LogiBuilder::getBuilder()
		->addTresc($klucz, $tresc)
		->setKodKategorii('platnosci');
	$this->loguj($builder);
    }

    private function zapiszParametryPotwierdzenia($post) {
	$platnoscId = $post->p24_session_id;
	$orderId = $post->p24_order_id;

	$this->platnosc = $this->getRekord($platnoscId);
	$this->platnosc->order_id = $orderId;
	$this->zapisz($this->platnosc);

    }

    private function wyslijPotwierdzenieTransakci() {
	$P24 = $this->utworzObiektPrzelewow24();

	$P24->addValue("p24_session_id", $this->platnosc->id);
	$P24->addValue("p24_amount", $this->platnosc->getKwote());
	$P24->addValue("p24_currency", 'PLN');
	$P24->addValue("p24_order_id", $this->platnosc->order_id);

	$RET = $P24->trnVerify();

	if (ObslugaP24::czyTransakcjaUdana($RET)) {

	    $this->platnosc->status = PlatnosciStatusy::ZAKONCZONA;
	    $this->zapisz($this->platnosc);
	} else {

	    $this->platnosc->status = PlatnosciStatusy::BLAD;
	    $this->zapisz($this->platnosc);
	    $msg = ObslugaP24::getTrescBledu($RET);
	    throw new \Pastmo\Platnosci\Exception\PlatnosciException($msg);
	}
    }

    private function przedluzKonto() {

	$this->get(\Uzytkownicy\Menager\KontaMenager::class)->przedluzKonto($this->platnosc);
    }

    public function utworzPlatnoscDlaUzytkownikaTry($uzytkownik, $cennik) {
	$this->platnosc = $this->utworzEncjePlatnosci($uzytkownik, $cennik);
	return $this->wyslijPlatnosc($this->platnosc);
    }

    public function utworzObiektPrzelewow24() {

	$konfiguracja = $this->sm->get('Config');
	$przelewyKonfig = $konfiguracja['przelewy_24'];
	$P24 = new Przelewy24($przelewyKonfig['MERCHANT_ID'], $przelewyKonfig['POS_ID'], $przelewyKonfig['SALT'],
		$przelewyKonfig['SANBOX']);
	return $P24;
    }

    private function utworzEncjePlatnosci($uzytkownik, $cennik) {
	$zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();

	$platnosc = new Platnosc();
	$platnosc->konto = $uzytkownik->konto;
	$platnosc->cennik_id = $cennik;
	$platnosc->kwota = $cennik->kwota;
	$platnosc->uzytkownik_id = $zalogowany;
	$platnosc->email = $zalogowany->mail;

	$this->zapisz($platnosc);

	return $this->getPoprzednioDodany();
    }

    private function wyslijPlatnosc($platnosc) {
	$P24 = $this->utworzObiektPrzelewow24();

	$P24->addValue("p24_session_id", $platnosc->id);
	$P24->addValue("p24_amount", $platnosc->getKwote());
	$P24->addValue("p24_currency", 'PLN');
	$P24->addValue("p24_email", $platnosc->email);
	$P24->addValue("p24_url_return", $this->utworzUrlPowrotu());
	$P24->addValue("p24_url_status", $this->utworzUrlPrzekazaniaStatusu());
	$P24->addValue("p24_description", 'Abonament za korzystanie z serwisu podejmijdecyzje.pl');
	$P24->addValue("p24_country", 'PL');

	$RET = $P24->trnRegister(false);

	if (ObslugaP24::czyTransakcjaUdana($RET)) {
	    $token = ObslugaP24::getTokenTransakcji($RET);
	    $platnosc->token = $token;
	    $platnosc->status = PlatnosciStatusy::ZAREJESTROWANA;
	    $this->zapisz($platnosc);
	    $urlPrzekierowania = ObslugaP24::getUrlPrzekierowania($P24, $token);
	    return $urlPrzekierowania;
	} else {
	    $msg = ObslugaP24::getTrescBledu($RET);
	    throw new \Pastmo\Platnosci\Exception\PlatnosciException($msg);
	}
    }

    public function utworzUrlPowrotu() {
	$url = $this->generujUrl('platnosci', ['action' => 'powrot', 'id' => $this->platnosc->id], [], true);
	return $url;
    }

    public function utworzUrlPrzekazaniaStatusu() {
	$url = $this->generujUrl('platnosci', ['action' => 'status', 'id' => $this->platnosc->id], [], true);
	return $url;
    }

    private function obsluzWyjatek($e) {
	$builder = $this->getLogiBuilderZWyjatku($e);
	$builder->addTresc('id platnosci', $this->platnosc->id);
	$this->loguj($builder);
	$this->platnosc->status = PlatnosciStatusy::BLAD;
	$this->zapisz($this->platnosc);
    }

}
