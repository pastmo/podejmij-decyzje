<?php

namespace Pastmo\Platnosci\Controller\Factory;

class Factory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new \Pastmo\Platnosci\Controller\PlatnosciController();
    }

}
