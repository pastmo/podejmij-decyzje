<?php

namespace Pastmo\Platnosci\Controller;

use Pastmo\Wspolne\Adnotacje\DomyslnyJsonModel;
use Wspolne\Controller\WspolneController;
use Logowanie\Enumy\KodyUprawnien;

class PlatnosciController extends WspolneController {

    public function powrotAction() {
	$request = $this->getEvent()->getRequest();
	$post = new \Zend\Stdlib\Parameters(['msg' => 'Dziękujemy za rejestrację! Twoje konto zostanie aktywowane po zaksięgowaniu wpłaty. Zwykle nie trwa to dłużej niż 15 minut.']);
	$request->setPost($post);

	return $this->forward()->dispatch('Decyzje\Controller\Decyzje', [
			'action' => 'index',
	]);
    }

    public function statusAction() {
	$post = $this->getPost();
	$this->platnosciFasada->obsluzPotwierdzeniePlatnosci($post);
	return $this->returnSuccess();
    }

}
