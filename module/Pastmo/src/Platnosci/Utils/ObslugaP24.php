<?php

namespace Pastmo\Platnosci\Utils;

class ObslugaP24 {

    public static function czyTransakcjaUdana($RET) {
	return isset($RET['error']) && $RET["error"] == '0';
    }

    public static function getTokenTransakcji($RET) {
	if (isset($RET['token'])) {
	    return $RET['token'];
	}
    }

    public static function getTrescBledu($RET) {
	if (isset($RET['errorMessage'])) {
	    return $RET['errorMessage'];
	}
    }

    public static function getUrlPrzekierowania(Przelewy24 $P24, $token) {
	return $P24->trnRequest($token, false);
    }

}
