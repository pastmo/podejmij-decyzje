<?php

namespace Pastmo\Platnosci;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Pastmo\Email\Entity\KontoMailowe;
use Pastmo\Email\Entity\Email;
use Pastmo\Email\Entity\EmailZasob;

class KonfiguracjaModulu extends \Pastmo\KonfiguracjaModulu {

    use \Pastmo\FunkcjeKonfiguracyjne;

    const CENNIK_GATEWAY = "cennik_gateway";
    const PLATNOSCI_GATEWAY = "platnosci_gateway";

    public function getFactories() {
	return array(
		Fasada\PlatnosciFasada::class => function($sm) {
		    $menager = new Fasada\PlatnosciFasada($sm);
		    return $menager;
		},
		Menager\CennikMenager::class => function($sm) {
		    $menager = new Menager\CennikMenager($sm);
		    return $menager;
		},
		Menager\PlatnosciMenager::class => function($sm) {
		    $menager = new Menager\PlatnosciMenager($sm);
		    return $menager;
		},
		self::CENNIK_GATEWAY => function($sm) {
		    return $this->ustawGateway($sm, Entity\Cennik::class, 'cennik');
		},
		self::PLATNOSCI_GATEWAY => function($sm) {
		    return $this->ustawGateway($sm, Entity\Platnosc::class, 'platnosci');
		}
	);
    }

    public static function dodajKontrolery($array) {
	$array['controllers']['factories']['Pastmo\Platnosci\Controller\Platnosci'] = \Pastmo\Platnosci\Controller\Factory\Factory::class;

	$array['router']['routes']['platnosci'] = array(
		'type' => 'segment',
		'options' => array(
			'route' => '/platnosci[/:action][/:id]',
			'constraints' => array(
				'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
				'id' => '[0-9]+',
			),
			'defaults' => array(
				'controller' => 'Pastmo\Platnosci\Controller\Platnosci',
				'action' => 'index',
			),
		),
	);
	return $array;
    }

    public static function dowiazMenagery(\Wspolne\Controller\Factory\InitObiekt $initObiekt,
	    \Interop\Container\ContainerInterface $container) {
	$initObiekt->platnosciFasada = $container->get(Fasada\PlatnosciFasada::class);
    }

}
