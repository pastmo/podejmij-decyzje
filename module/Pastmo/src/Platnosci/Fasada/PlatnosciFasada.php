<?php

namespace Pastmo\Platnosci\Fasada;

class PlatnosciFasada extends \Wspolne\Fasada\WspolneFasada {

    public function obsluzPotwierdzeniePlatnosci($post) {
	$this->get(\Pastmo\Platnosci\Menager\PlatnosciMenager::class)
		->obsluzPotwierdzeniePlatnosci($post);
    }

}
