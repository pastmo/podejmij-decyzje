<?php

namespace Pastmo\Platnosci\Entity;

class Platnosc extends \Wspolne\Model\WspolneModel {

    public $id;
    public $cennik_id;
    public $uzytkownik_id;
    public $status;
    public $status_systemowy;
    public $kwota;
    public $email;
    public $token;
    public $order_id;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->data = $data;
	parent::exchangeArray($data);

	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->cennik_id = $this->pobierzLubNull($data, 'cennik_id');
	$this->uzytkownik_id = $this->pobierzLubNull($data, 'uzytkownik_id');
	$this->status = $this->pobierzLubNull($data, 'status');
	$this->status_systemowy = $this->pobierzLubNull($data, 'status_systemowy');
	$this->kwota = $this->pobierzLubNull($data, 'kwota');
	$this->email = $this->pobierzLubNull($data, 'email');
	$this->token = $this->pobierzLubNull($data, 'token');
	$this->order_id = $this->pobierzLubNull($data, 'order_id');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'cennik':
		return 'cennik_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function getEncjeTabeliObcej($name) {
	switch ($name) {
	    case 'cennik':
		return $this->getEncjeZTabeliObcej('cennik_id', \Pastmo\Platnosci\Menager\CennikMenager::class, new Cennik());
	}
    }

    public function getKwote() {
	return $this->kwota * 100;
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
