<?php

namespace Pastmo\Platnosci\Entity;

class Cennik extends \Wspolne\Model\WspolneModel {

    public $id;
    public $kwota;
    public $opis;
    public $dni_przedluzenia;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->data = $data;
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->kwota = $this->pobierzLubNull($data, 'kwota');
	$this->opis = $this->pobierzLubNull($data, 'opis');
	$this->dni_przedluzenia = $this->pobierzLubNull($data, 'dni_przedluzenia');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
