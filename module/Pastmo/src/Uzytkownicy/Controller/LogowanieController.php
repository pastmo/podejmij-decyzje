<?php

namespace Pastmo\Uzytkownicy\Controller;

use Logowanie\Enumy\KodyUprawnien;
use Zend\View\Model\ViewModel;
use Zend\Stdlib\Parameters;

abstract class LogowanieController extends \Wspolne\Controller\WspolneController {

    protected $post;

    public function logowanieAction() {

	$this->post = $this->getPost();
	if (isset($this->post['uzytkownik'])) {
	    $wynik = $this->uzytkownikTable->zaloguj($this->post['uzytkownik']['login'],
		    $this->post['uzytkownik']['haslo'], $this->post);

	    if ($wynik->czySukces) {
		return $this->przekieruj($this->post);
	    } else {
		return $this->wywolanieStronyLogowania($wynik->msg);
	    }
	} else {
	    return $this->domyslnePrzekierowanieNiezalogowanego();
	}
    }

    public function rejestracjaAction() {
	$wynikPrzetwarzanie = null;
	$post = $this->getPost();

	if ($post) {

	    $wynikPrzetwarzanie = $this->logowanieFasada->zarejestruj($post);
	    if ($wynikPrzetwarzanie->isSuccess) {
		$this->redirect()->toRoute('logowanie', ['action' => 'po_rejestracji']);
	    }
	}

	return $this->wyswietlFormularzRejestracji($wynikPrzetwarzanie, $post);
    }

    public function ustawieniaAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$zalogowany = $this->uzytkownikTable->getZalogowanegoUsera();
	$dostepne_slowniki = $this->tlumaczeniaFasada->getDostepneSlowniki();
	$uzytkownicy_konta = $this->logowanieFasada->pobierzKontaMailoweDlaZalogowanegoUzytkownika();

	if (count($uzytkownicy_konta) == 0) {
	    $uzytkownicy_konta[] = \Logowanie\Entity\UzytkownikKontoMailowe::create();
	}

	$sprawdzanieKontMailowych = $this->logowanieFasada->sprawdzPoprawnoscKontMailowych();

	return new ViewModel(array(
		'uzytkownik' => $zalogowany,
		'uzytkownicy_konta' => $uzytkownicy_konta,
		'dostepne_slowniki' => $dostepne_slowniki,
		'wynikSprawdzania' => $sprawdzanieKontMailowych
	));
    }

    public function zapiszKontaMailoweAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$post = $this->getPost();
	if ($post) {
	    $this->logowanieFasada->zapiszKontaMailowe($post);
	    $wynikSprawdzania = $this->logowanieFasada->sprawdzPoprawnoscKontMailowych();

	    if (!$wynikSprawdzania->sukces) {
		return $this->redirect()->toRoute('logowanie', array('action' => 'ustawienia'));
	    }
	}

	return $this->redirect()->toRoute('email', array('action' => 'index'));
    }

    public function zapiszJezykUzytkownikaAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);


	$request = $this->getRequest();
	if ($request->isPost()) {
	    $uzytkownikId = $this->params()->fromPost('id');
	    $kod = $this->params()->fromPost('jezyk_kod');
	    $this->uzytkownikTable->zmienJezykUzytkownika($uzytkownikId, $kod);
	}
	return $this->redirect()->toRoute('logowanie', array('action' => 'ustawienia'));
    }

    abstract protected function wyswietlFormularzRejestracji($wynikPrzetwarzanie = null, $post = []);

    public function poRejestracjiAction() {
	$this->ustawLogowanieLayout();
    }

    protected function przekieruj($post) {
	$przekierowanie = isset($post['redirect']) ? $post['redirect'] : "";
	if ($przekierowanie !== '') {
	    return $this->redirect()->toUrl($przekierowanie);
	} else {
	    return $this->domyslnePrzekierowanieZalogowanego();
	}
    }

    protected function wywolanieStronyLogowania($msg = false) {
	$request = $this->getEvent()->getRequest();

	$this->post->set('msg', $msg);

	$request->setPost($this->post);

	return $this->forward()->dispatch('Logowanie\Controller\Logowanie', [
			'action' => 'index',
	]);
    }

    protected function domyslnePrzekierowanieNiezalogowanego($msg = false) {
	return $this->redirect()->toRoute('home');
    }

    protected function domyslnePrzekierowanieZalogowanego() {
	return $this->redirect()->toRoute('home');
    }

}
