<?php

namespace Pastmo\Uzytkownicy\Exception;

class LogowanieException extends UzytkownicyException {

    private $urlPrzekierowania;

    public function setUrlPrzekierowania($url) {
	$this->urlPrzekierowania = $url;
}

    public function getUrlPrzekierowania() {
	return $this->urlPrzekierowania;
    }

}
