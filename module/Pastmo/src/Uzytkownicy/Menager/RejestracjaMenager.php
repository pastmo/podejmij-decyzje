<?php

namespace Pastmo\Uzytkownicy\Menager;

use Zend\ServiceManager\ServiceManager;
use Logowanie\Polecenie\WynikPrzetwarzaniaFormularza;
use \Logowanie\Model\Uzytkownik;
use Pastmo\Wspolne\Exception\PastmoException;

class RejestracjaMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    private $uzytkownicyTable;
    protected $dodanyUzytkownik;
    protected $wynik;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Pastmo\Uzytkownicy\KonfiguracjaModulu::UZYTKOWNIK_TABLE_GATEWAY);
	$this->uzytkownicyTable = $this->get(\Logowanie\Menager\UzytkownicyMenager::class);
    }

    public function pobierzZarejestrujUzytkownikaPoMailu(Uzytkownik $uzytkownik) {
	$uzytkownicy = $this->pobierzZWherem("mail='{$uzytkownik->mail}'");
	if (count($uzytkownicy) > 0) {
	    return $uzytkownicy[0];
	} else {
	    $this->zapiszUzytkownika($uzytkownik->zrobArray());
	    return $this->dodanyUzytkownik;
	}
    }

    public function zarejestrujUzytkownika(Uzytkownik $uzytkownik) {

	$post = $uzytkownik->zrobArray();

	return $this->zarejestruj($post);
    }

    public function zarejestruj($post) {
	try {
	    return $this->zarejestrujTry($post);
	} catch (\Exception $e) {
	    $this->wynik->setSuccess(false);
	    $this->wynik->addMsg($e->getMessage());
	    $this->get(\Wspolne\Menager\SesjaMenager::class)->ustawIdUzytkownika();
	    return $this->wynik;
	}
    }

    protected function zarejestrujTry($post) {

	$this->wynik = \Pastmo\Uzytkownicy\Entity\OdpowiedzRejestracji::create(true, "Zapisano pomyślnie");
	$this->sprawdzCaptcha($post);
	$post = $this->konwertujPost($post);
	$this->sprawdzHasla($post);
	$this->sprawdzMaila($post);

	$this->zapiszUzytkownika($post);
//	$this->wyslijMaila();
	$this->akcjePoRejestracji();

	return $this->wynik;
    }

    private function sprawdzCaptcha($post) {
	if (isset($post['g-recaptcha-response'])) {
	    $reCaptchaService = $this->sm->get('ReCaptchaService');
	    if (!$reCaptchaService->isValid($post['g-recaptcha-response'], $post)) {
		throw new PastmoException("Nie powiodła się walidacja ReCaptcha.");
	    }
	}
    }

    public function konwertujPost($post) {
	if (isset($post['uzytkownik']) && !isset($post['haslo'])) {
	    $post = $post['uzytkownik'];
	}
	if (isset($post['email'])) {
	    $post['mail'] = $post['email'];
	}
	if (isset($post['username'])) {
	    $username = explode(" ", $post['username']);
	    if (count($username) > 0) {
		$post['imie'] = $username[0];
	    }
	    if (count($username) > 1) {
		$post['nazwisko'] = $username[1];
	    }
	}
	if (isset($post['user_id'])) {
	    $post['id'] = $post['user_id'];
	}
	if (isset($post['phone'])) {
	    $post['telefon'] = $post['phone'];
	}
	if (isset($post['phone2'])) {
	    $post['telefon2'] = $post['phone2'];
	}


	return $post;
    }

    protected function akcjePoRejestracji() {

    }

    public static function generujKod($uzytkownik) {
	if (!$uzytkownik->id) {
	    throw new PastmoException('Użytkownik nieznaleziony');
	}
	$wynik = md5($uzytkownik->nazwisko . $uzytkownik->id);
	return $wynik;
    }

    private function sprawdzHasla($dane) {
	if (isset($dane['haslo']) && $dane['haslo'] !== $dane['haslo2']) {
	    throw new PastmoException('Hasła różnią się.');
	}
    }

    private function sprawdzMaila($dane) {
	$mail = $dane['mail'];

	$duplikaty = $this->uzytkownicyTable->pobierzUzytkownikaPoMailu($mail);
	if ($duplikaty->id) {
	    throw new PastmoException("Użytkownik o takim adresie e-mail już istnieje.");
	}
    }

    private function zapiszUzytkownika($post) {
	$uzytkownik = $this->uzytkownicyTable->createUzytkownik();

	$uzytkownik->aktualizujArray($post);

	if (isset($post['haslo'])) {
	    $uzytkownik->haslo = $uzytkownik->kodujHaslo($post['haslo']);
	}

	$this->uzytkownicyTable->zapisz($uzytkownik);

	$this->dodanyUzytkownik = $this->uzytkownicyTable->getPoprzednioDodany();

	$this->polaczFirmeZUzytkownikiem($post);
    }

    private function polaczFirmeZUzytkownikiem($post) {
	if (isset($post['firma'])) {
	    $nazwaFirmy = $post['firma'];
	    $firma = $this->getFirmyFasada()->pobierzLubUtworzFirme($nazwaFirmy);

	    $this->getFirmyFasada()->polaczUzytkownikaZFirma($this->dodanyUzytkownik->id, $firma->id);
	}
    }

    protected function wyslijMaila() {

	$aktywacjaTable = $this->get(\Logowanie\Model\AktywacjaMail::class);
	$aktywacjaTable->ustawParametry($this->dodanyUzytkownik->mail, $this->dodanyUzytkownik->id,
		self::generujKod($this->dodanyUzytkownik));
	$aktywacjaTable->send();
    }

}
