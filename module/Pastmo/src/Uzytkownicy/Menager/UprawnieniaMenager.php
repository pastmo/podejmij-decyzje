<?php

namespace Pastmo\Uzytkownicy\Menager;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;
use Pastmo\Uzytkownicy\Exception\UprawnieniaException;
use Pastmo\Uzytkownicy\Exception\LogowanieException;
use Pastmo\Uzytkownicy\Exception\UzytkownicyException;
use Pastmo\Wspolne\Exception\PastmoException;

class UprawnieniaMenager extends BazowyMenagerBazodanowy {

    use \Pastmo\Wspolne\Utils\ObslugaBledowTrait;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Logowanie\Module::UPRAWNIENIA_GATEWAY);
    }

    private $uzytkownikDoSprawdzenia;
    private $uzytkownik;
    private $rolaId;
    private $kod;

    public function sprawdzUprawnienieUzytkownika($kod, $uzytkownikLubId) {

	if (is_numeric($uzytkownikLubId)) {
	    $this->uzytkownik = $this->get(\Logowanie\Menager\UzytkownicyMenager::class)->getRekord($uzytkownikLubId);
	} else {
	    $this->uzytkownik = $uzytkownikLubId;
	}

	$this->czyWyjatek = FALSE;
	$this->kod = $kod;

	return $this->sprawdzUprawnienieWspolne();
    }

    public function sprawdzUprawnienie($kod, $czyWyjatek = false) {
	$this->czyWyjatek = $czyWyjatek;
	$this->kod = $kod;

	return $this->sprawdzUprawnienieWspolne();
    }

    private function sprawdzUprawnienieWspolne() {
	try {
	    $this->sprawdzanieUprawnienTry();
	    return true;
	} catch (UzytkownicyException $e) {
	    if ($e instanceof LogowanieException) {
		$e->setUrlPrzekierowania($this->zwrocBiezacyUrl());
	    }
	    return $this->zwrocBlad($e);
	}
    }

    private function sprawdzanieUprawnienTry() {

	$this->pobierzZalogowanegoUzytkownika();
	$this->pobierzIdRoli();
	$this->sprawdzCzyRolaMaDaneUprawnienie();
    }

    private function pobierzZalogowanegoUzytkownika() {
	if ($this->uzytkownik) {
	    $this->uzytkownikDoSprawdzenia = $this->uzytkownik;
	} else {
	    $this->uzytkownikDoSprawdzenia = $this->getLogowanieFasada()->getZalogowanegoUsera();

	    if (!$this->uzytkownikDoSprawdzenia) {
		throw new LogowanieException("Nie jesteś zalogowany");
	    }
	}
    }

    private function pobierzIdRoli() {
	$this->rolaId = $this->uzytkownikDoSprawdzenia->rola->id;
	if (!$this->rolaId) {
	    throw new UprawnieniaException("Użytkownik nie ma przypisanej roli");
	}
    }

    private function sprawdzCzyRolaMaDaneUprawnienie() {
	$where = " kod in(select ur.uprawnienie_kod from uprawnienia_role ur where ur.rola_id=$this->rolaId AND ur.uprawnienie_kod='$this->kod')";
	$uprawnienia = $this->pobierzZWheremCount($where, "kod");

	if ($uprawnienia === 0) {
	    $uprawnenieArr = $this->pobierzZWherem("kod='{$this->kod}'", 'kod asc');

	    $uprawnienie = \Pastmo\Wspolne\Utils\ArrayUtil::pobierzPierwszyElement($uprawnenieArr, false);
	    if ($uprawnienie) {
		$uprawnienieTekst = $this->translateConst($uprawnienie->opis);
		throw new UprawnieniaException("Brak uprawnienia: '{$uprawnienieTekst}'");
	    } else {
		$tekstWyjatku = sprintf($this->_translate("Brak uprawnienia o kodzie %s w bazie danych", false, 6), $this->kod);
		throw new PastmoException($tekstWyjatku);
	    }
	}
    }

    private function zwrocBiezacyUrl() {
	$uri = $this->sm->get('Application')->getRequest()->getUri();
	$uri->setScheme(null)
		->setHost(null)
		->setPort(null)
		->setUserInfo(null);
	return $this->opakujBasePath($uri->toString());
    }

    public function pobierzUprawnieniaDlaRoli($rolaId) {
	$wynik = [];

	if ($rolaId) {
	    $where = " kod in(select ur.uprawnienie_kod from uprawnienia_role ur where ur.rola_id=$rolaId)";
	    $uprawnienia = $this->pobierzZWherem($where, "kod");


	    foreach ($uprawnienia as $uprawnienie) {
		$wynik[$uprawnienie->kod] = $uprawnienie;
	    }
	}
	return $wynik;
    }

    public function pobierzUprawnieniePoKodzie($kod) {
	$wynik = $this->pobierzZWherem("kod='$kod'", "kod desc");

	if (count($wynik) > 0) {
	    return $wynik[0];
	} else {
	    return \Logowanie\Entity\Uprawnienie::create($kod);
	}
    }

}
