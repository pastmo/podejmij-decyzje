<?php

namespace Pastmo\Uzytkownicy\Menager;

use Zend\ServiceManager\ServiceManager;
use Pastmo\Wspolne\Utils\SearchPole;
use Logowanie\Model\Uzytkownik;
use Wspolne\Menager\SesjaMenager;
use Pastmo\Wspolne\Exception\PastmoException;

class UzytkownicyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowyEventowy {

    private $uzytkownik;
    private $sesjaMenager;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, 'UzytkownikTableGateway');
    }

    public function pobierzUzytkownikowZUprawnieniamiBezZalogowanego($kodUprawnienia) {
	$zalogowany = $this->getZalogowanegoUsera();
	return $this->pobierzZWherem($this->whereDoUzytkownikowZUprawnieniami($kodUprawnienia) . " AND id!={$zalogowany->id}" .
			" AND usuniety = 0");
    }

    public function pobierzUzytkownikowZUprawnieniami($kodUprawnienia, $order = self::DOMYSLNY_ORDER,
	    $limit = false) {
	return $this->pobierzZWherem($this->whereDoUzytkownikowZUprawnieniami($kodUprawnienia) . " AND usuniety = 0",
			$order, $limit);
    }

    public function pobierzLiczbeUzytkownikowZUprawnieniami($kodUprawnienia) {
	return $this->pobierzZWheremCount($this->whereDoUzytkownikowZUprawnieniami($kodUprawnienia) . " AND usuniety = 0");
    }

    protected function whereDoUzytkownikowZUprawnieniami($kodUprawnienia) {
	return "rola_id IN (
											SELECT rola_id
											FROM uprawnienia_role
											WHERE uprawnienie_kod='$kodUprawnienia')";
    }

    public function zapisz(\Pastmo\Wspolne\Entity\WspolnyModel $model) {
	$model = $this->zaktualizujTyp($model);

	if (!\Pastmo\Wspolne\Utils\EntityUtil::wydobadzId($model->rola)) {
	    $model->rola = $this->pobierzJednaRolePoKodzie(\Logowanie\Entity\KodyRol::Pusta);
	}
	parent::zapisz($model);
    }

    private function zaktualizujTyp($model) {
	$rola = $model->rola;
	if ($rola) {
	    if (!$rola instanceof \Logowanie\Entity\Rola) {
		$rola = $this->get(\Logowanie\Menager\RoleMenager::class)->getRekord($rola);
	    }
	    $model->typ = $rola->typ;
	}

	return $model;
    }

    protected function pobierzPolaWyszukiwania() {
	return array(
		SearchPole::create()->setDataType(SearchPole::STRING)->setName("login"),
		SearchPole::create()->setDataType(SearchPole::STRING)->setName("imie"),
		SearchPole::create()->setDataType(SearchPole::STRING)->setName("nazwisko"),
	);
    }

    protected function konwertujNaRekordWyszuiwania($encja) {
	$wynik = new \Pastmo\Wspolne\Entity\RekordWyszukiwania();

	$wynik->value = $encja . "";
	$wynik->tokens = array($encja->imie, $encja->nazwisko, $encja->login);
	$wynik->url = $this->generujUrl('logowanie', array('action' => 'profil', 'id' => $encja->id));
	$wynik->type = "Użytkownicy";
	$wynik->id = $encja->id;
	$wynik->menager = \Logowanie\Menager\UzytkownicyMenager::class;

	return $wynik;
    }

    public function zaloguj($mail, $haslo, $post = null) {
	try {
	    return $this->zalogujTry($mail, $haslo, $post);
	} catch (\Exception $e) {
	    $this->wynik->setCzySukces(false);
	    $this->wynik->setMsg($e->getMessage());
	    return $this->wynik;
	}
    }

    public function zalogujUzytkownika($post) {
	try {
	    return $this->zalogujTry($post['uzytkownik']['login'], $post['uzytkownik']['haslo'], $post);
	} catch (\Exception $e) {
	    $this->wynik->setCzySukces(false);
	    $this->wynik->setMsg($e->getMessage());
	    return $this->wynik;
	}
    }

    private function zalogujTry($mail, $haslo, $post = null) {
	$this->wynik = \Pastmo\Uzytkownicy\Entity\OdpowiedzLogowania::create();
	$this->sprawdzCaptcha($post);
	$this->pobierzUzytkownika($mail, $haslo);
	$this->sprawdzCzyIstnieje();
	$this->sprawdzCzyUsuniety();
	$this->sprawdzCzyAktywny();
	$this->getSesjaMenagera()->ustawIdUzytkownika($this->uzytkownik->id);
	$this->wynik->setUzytkownik($this->uzytkownik);
	$this->wynik->setOplaconeKonto($this->czyWazneKonto());
	return $this->wynik;
    }

    public function sprawdzCaptcha($post) {
	$reCaptchaService = $this->sm->get('ReCaptchaService');
	if (!$reCaptchaService->isValid($post['g-recaptcha-response'], $post)) {
	    throw new PastmoException("Nie powiodła się walidacja ReCaptcha.");
	}
    }

    private function pobierzUzytkownika($mail, $haslo) {
	$md5Haslo = (new Uzytkownik())->kodujHaslo($haslo);
	$rowset = $this->tableGateway->select(array('mail' => $mail, 'haslo' => $md5Haslo));
	$this->uzytkownik = $rowset->current();
    }

    private function sprawdzCzyIstnieje() {
	if (!$this->uzytkownik) {
	    throw new PastmoException("Błędny login lub hasło");
	}
    }

    private function sprawdzCzyUsuniety() {
	if ($this->uzytkownik->usuniety) {
	    throw new PastmoException("Użytkownik usunięty");
	}
    }

    private function sprawdzCzyAktywny() {
	if (!$this->uzytkownik->czy_aktywny) {
	    throw new PastmoException("Użytkownik nieaktywny");
	}
    }

    public function wyloguj() {
	$this->getSesjaMenagera()->zakonczSesje();
    }

    public function getZalogowanegoUsera() {
	$userId = $this->getSesjaMenagera()->pobierzIdUzytkownika();
	if (!empty($userId)) {
	    return $this->getRekord($userId);
	}
	return false;
    }

    public function zapiszZPosta($post) {

	if (is_object($post)) {
	    $post = (array) $post;
	}

	$rejestracjaMenager = $this->get(\Logowanie\Menager\RejestracjaMenager::class);
	$post = $rejestracjaMenager->konwertujPost($post);

	$uzytkownik = null;
	if (isset($post['id'])) {
	    $uzytkownik = $this->getRekord($post['id']);
	} else {
	    $uzytkownik = new Uzytkownik($this->sm);
	}

	$uzytkownik->aktualizujArray($post);
	$uzytkownik->zmienHaslo($post);

	$zasobyTable = $this->sm->get(\Zasoby\Menager\ZasobyUploadMenager::class);
	$zasoby = $zasobyTable->zaladujPlik();

	foreach ($zasoby as $zasob) {
	    $uzytkownik->avatar = $zasob;

	    break;
	}

	$this->zapisz($uzytkownik);
    }

    public function pobierzPracownikow() {
	$klientRola = $this->pobierzRoleKLient();

	$wynik = $this->pobierzZWherem("rola_id!='" . $klientRola->id . "' AND usuniety = 0");

	return $wynik;
    }

    private function pobierzRoleKLient() {
	$roleMenager = $this->sm->get(\Logowanie\Menager\RoleMenager::class);
	return $roleMenager->pobierzRolePoNazwie(\Logowanie\Entity\Rola::KLIENT);
    }

    protected function pobierzRolePoKodzie($kod) {
	$roleMenager = $this->sm->get(\Logowanie\Menager\RoleMenager::class);
	return $roleMenager->pobierzRolePoKodzie($kod);
    }

    protected function pobierzJednaRolePoKodzie($kod) {
	$roleMenager = $this->sm->get(\Logowanie\Menager\RoleMenager::class);
	return $roleMenager->pobierzJednaRolePoKodzie($kod);
    }

    public function pobierzUzytkownikowZProjektowBezZalogowanego() {
	$zalogowany = $this->getZalogowanegoUsera();
	return $this->pobierzZWherem("id IN (select uzytkownik_id from uzytkownicy_projekty where projekt_id " .
			"IN(select projekt_id from uzytkownicy_projekty where uzytkownik_id={$zalogowany->id}) ) AND " .
			" id!={$zalogowany->id} AND usuniety = 0");
    }

    public function pobierzUzytkownikaPoMailu($mail) {
	$uzytkownicy = $this->pobierzZWherem("mail = '$mail'");
	if (count($uzytkownicy) > 0) {
	    return $uzytkownicy[0];
	} else {
	    return $this->createUzytkownik();
	}
    }

    public function createUzytkownik() {
	$uzytkownik = new Uzytkownik();
	$uzytkownik->typ = \Logowanie\Model\TypyUzytkownikow::KLIENT;

	return $uzytkownik;
    }

    public function zarejestrujLubUaktualnijZBuildera(\Wspolne\Interfejs\UzytkownikBuilderInterfejs $builder) {
	$uzytkownik = $this->pobierzUzytkownikaPoMailu($builder->getEmail());

	$uzytkownik->mail = $builder->getEmail();
	$uzytkownik->setImieINazwisko($builder->getImieINazwisko());
	$uzytkownik->telefon = $builder->getTelefon();

	$this->zarejestrujLubUaktualnij($uzytkownik);

	if (!$uzytkownik->id) {
	    $uzytkownik = $this->getPoprzednioDodany();
	}

	return $uzytkownik;
    }

    public function zarejestrujLubUaktualnij(Uzytkownik $uzytkownik) {
	if ($uzytkownik->id) {
	    $this->zapisz($uzytkownik);
	} else {
	    $this->getLogowanieFasada()->zarejestrujUzytkownika($uzytkownik);
	}
    }

    public function pobierzUzytkownikowZRola($rolaId, $order = 'imie ASC') {
	return $this->pobierzZWherem("rola_id = $rolaId" . " AND usuniety = 0", $order);
    }

    public function pobierzUzytkownikowZRol(array $role, $order = 'imie ASC') {
	if (count($role) > 0) {
	    $rolaId = \Pastmo\Wspolne\Utils\ArrayUtil::wydobadzIdDoZapytaniaIn($role);
	    return $this->pobierzZWherem("rola_id IN($rolaId)" . " AND usuniety = 0", $order);
	}
	return [];
    }

//TODO: Funkcja do usunięcia
    public function zmienJezykUzytkownika($uzytkownikId, $jezykKod) {
	$this->update($uzytkownikId, array('jezyk_kod' => $jezykKod));
    }

    public function zmienBiezacySlownik($slownikId) {
	$zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();
	$this->update($zalogowany->id, array('slownik_id' => $slownikId));
    }

    public function pobierzUzytkownikowPoKodzieRoli($kodRoli) {
	$rola = $this->pobierzRolePoKodzie($kodRoli);
	return $this->pobierzUzytkownikowZRol($rola);
    }

    public function ustawUzytkownikaJakoUsuniety($uzytkownikId) {
	$this->update($uzytkownikId, ['usuniety' => 1]);
    }

    public function przywrocUsunietegoUzytkownika($uzytkownikId) {
	$this->update($uzytkownikId, ['usuniety' => 0]);
    }

    public function pobierzGrafikowWedlugUprawnien() {
	$wynik = [];

	if ($this->uprawnienie(\Logowanie\Enumy\KodyUprawnien::UPRAWNIENIA_GRAFIKA)) {
	    $zalogowany = $this->getZalogowanegoUsera();
	    $wynik[] = $zalogowany;
	}

	if ($this->uprawnienie(\Logowanie\Enumy\KodyUprawnien::UPRAWNIENIA_GLOWNEGO_GRAFIKA)) {
	    $uzytkownicy = $this->pobierzUzytkownikowZUprawnieniamiBezZalogowanego(\Logowanie\Enumy\KodyUprawnien::UPRAWNIENIA_GRAFIKA);
	    $wynik = array_merge($wynik, $uzytkownicy);
	}

	return $wynik;
    }

    private function getSesjaMenagera() {
	if ($this->sesjaMenager === null) {
	    $this->sesjaMenager = $this->sm->get(SesjaMenager::class);
	}
	return $this->sesjaMenager;
    }

}
