<?php

namespace Pastmo\Uzytkownicy\Entity;

use Zasoby\Model\Zasob;
use Logowanie\Entity\Rola;

class Uzytkownik extends \Pastmo\Wspolne\Entity\WspolnyModel implements \JsonSerializable {

    public $id;
    public $login;
    public $imie;
    public $nazwisko;
    public $haslo;
    public $mail;
    public $telefon;
    public $telefon2;
    public $typ;
    public $deklarowana_firma;
    public $avatar;
    public $rola;
    public $firma;
    public $czy_aktywny;
    public $usuniety;
    public $data_dodania;
    public $jezyk_kod;
    public $akceptacja_regulaminu;
    public $zgoda_przetwarzanie;
    public $zgoda_przetwarzanie_marketingowe;
    protected $slownik_id;
    private $kontaMailowe = array();
    private $firmy = array();

    public function __construct($sm = null) {
	parent::__construct($sm);
    }

    public function exchangeArray($data) {
	$this->data = $data;

	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->slownik_id = $this->pobierzLubNull($data, 'slownik_id');
	$this->login = $this->pobierzLubNull($data, 'login');
	$this->imie = $this->pobierzLubNull($data, 'imie');
	$this->nazwisko = $this->pobierzLubNull($data, 'nazwisko');
	$this->haslo = $this->pobierzLubNull($data, 'haslo');
	$this->mail = $this->pobierzLubNull($data, 'mail');
	$this->telefon = $this->pobierzLubNull($data, 'telefon');
	$this->telefon2 = $this->pobierzLubNull($data, 'telefon2');
	$this->nr_konta = $this->pobierzLubNull($data, 'nr_konta');
	$this->typ = $this->pobierzLubNull($data, 'typ', \Logowanie\Model\TypyUzytkownikow::KLIENT);
	$this->deklarowana_firma = $this->pobierzLubNull($data, 'deklarowana_firma');
	$this->ostatnie_logowanie = $this->pobierzLubNull($data, 'ostatnie_logowanie');
	$this->czy_aktywny = $this->pobierzLubNull($data, 'czy_aktywny', false);
	$this->usuniety = $this->pobierzLubNull($data, 'usuniety');
	$this->data_dodania = $this->pobierzLubNull($data, 'data_dodania');
	$this->jezyk_kod = $this->pobierzLubNull($data, 'jezyk_kod', \Pastmo\Uzytkownicy\Enumy\KodyJezykow::DOMYSLNY);
	$this->akceptacja_regulaminu = $this->pobierzLubNull($data, 'akceptacja_regulaminu');
	$this->zgoda_przetwarzanie = $this->pobierzLubNull($data, 'zgoda_przetwarzanie');
	$this->zgoda_przetwarzanie_marketingowe = $this->pobierzLubNull($data, 'zgoda_przetwarzanie_marketingowe');
	$this->wyswietlanie_js = $this . '';

	if ($this->czyTabeleDostepne()) {

	    $this->rola = $this->pobierzTabeleObca('rola_id', \Logowanie\Menager\RoleMenager::class,
		    $this->domyslnaRola());
	}
    }

    private function domyslnaRola() {
	$rola = new Rola();
	return $rola;
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'avatar':
		return 'avatar_id';
	    case 'rola':
		return 'rola_id';
	    case 'firma':
		return 'firma_id';
	    case 'slownik':
		return 'slownik_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function getEncjeTabeliObcej($name) {
	switch ($name) {
	    case 'slownik':
		return $this->getEncjeZTabeliObcej('slownik_id', \Pastmo\Tlumaczenia\Menager\SlownikiMenager::class,
				new \Pastmo\Tlumaczenia\Entity\Slownik());
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public static function getClass() {
	return __CLASS__;
    }

    public function dowiazListyTabelObcych() {
	if ($this->czyTabeleDostepne()) {
	    $kontaMailoweMenager = $this->sm->get(\Pastmo\Email\Menager\KontaMailoweMenager::class);
	    $firmyMenager = $this->sm->get(\Firmy\Menager\FirmyMenager::class);

	    $this->kontaMailowe = $kontaMailoweMenager->pobierzKontaMailoweUzytkownika($this->id);
	    $this->firmy = $firmyMenager->pobierzFirmyUzytkownika($this->id);
	}
    }

    /**
     * TODO: Metoda do usunięcia, należy korzystać z logowanieFasada::pobierzKontaMailoweDlaZalogowanegoUzytkownika
     */
    public function getKontaMailowe() {
	$this->dowiazListyTabelObcych();
	return $this->kontaMailowe;
    }

    public function getFirmy() {
	$this->dowiazListyTabelObcych();
	return $this->firmy;
    }

    public function zmienHaslo($post) {//stare_haslo nowe_haslo nowe_haslo2
	if (isset($post['stare_haslo']) && $post['stare_haslo'] != "") {
	    $stareHaslo = $this->kodujHaslo($post['stare_haslo']);
	    $noweHaslo = $post['nowe_haslo'];
	    $noweHaslo2 = $post['nowe_haslo2'];


	    $obiektTable = $this->sm->get(\Logowanie\Module::UZYTKOWNIK_TABLE);
	    $uzytkownikZDB = $obiektTable->getRekord($this->id);

	    if ($uzytkownikZDB->haslo !== $stareHaslo) {
		throw new \Exception("Stare hasło nieprawidłowe");
	    }

	    $this->zmienHasloBezSprawdzaniaStarego($noweHaslo, $noweHaslo2);
	}
    }

    public function zmienHasloBezSprawdzaniaStarego($noweHaslo, $noweHaslo2) {
	if ($noweHaslo !== $noweHaslo2) {
	    throw new \Exception("Nowe hasła nie są zgodne");
	}

	$this->haslo = $this->kodujHaslo($noweHaslo);
    }

    public function kodujHaslo($hasloJawne) {
	return md5($hasloJawne);
    }

    public function __toString() {
	if ($this->imie || $this->nazwisko) {
	    return $this->imie . ' ' . $this->nazwisko;
	}
	if ($this->mail) {
	    return $this->mail;
	}

	return '';
    }

    public function jsonSerialize() {

	$this->haslo = '';
	return $this;
    }

    public function czyAktualniePrzegladajacy() {

	$zalogowany = $this->getZalogowanego();
	if ($zalogowany) {
	    $czyAktualnieZalgowany = $zalogowany->id == $this->id;
	    return $czyAktualnieZalgowany;
	}
	return false;
    }

    public function setImieINazwisko($imieINazwisko) {
	$this->imie = \Wspolne\Utils\StrUtil::getImie($imieINazwisko);
	$this->nazwisko = \Wspolne\Utils\StrUtil::getNazwisko($imieINazwisko);
    }

    public static function create($sm = null) {
	$encja = new Uzytkownik();
	$encja->avatar = Zasob::zrobDomyslnyZasobAvatara($sm);
	$encja->rola = new Rola();

	return $encja;
    }

}
