<?php

namespace Pastmo\Uzytkownicy\Entity;

class Jezyk extends \Pastmo\Wspolne\Entity\WspolnyModel {

    public $kod;
    public $skrot;
    public $nazwa;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->kod = $this->pobierzLubNull($data, 'kod');
	$this->skrot = $this->pobierzLubNull($data, 'skrot');
	$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	return $nazwaWKodzie;
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
