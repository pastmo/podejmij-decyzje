<?php

namespace Pastmo\Uzytkownicy\Entity;

class OdpowiedzRejestracji extends \Logowanie\Polecenie\WynikPrzetwarzaniaFormularza {

    public $urlPrzekierowania;

    function setUrlPrzekierowania($urlPrzekierowania) {
	$this->urlPrzekierowania = $urlPrzekierowania;
    }

    public static function create() {
	$odp = new OdpowiedzRejestracji(true, "");
	return $odp;
    }

}
