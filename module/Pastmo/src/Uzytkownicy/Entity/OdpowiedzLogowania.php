<?php

namespace Pastmo\Uzytkownicy\Entity;

class OdpowiedzLogowania extends \Pastmo\Wspolne\Entity\DomyslnaOdpowiedzPrzetwarzania {

    public static function create() {
	return new OdpowiedzLogowania();
    }

    public $czySukces = true;
    public $msg;
    public $uzytkownik;
    public $oplaconeKonto;

    public function toArray() {
	$par = parent::toArray();
	return array_merge($par, ['oplaconeKonto' => $this->oplaconeKonto]);
    }

    public function setCzySukces($czySukces) {
	$this->czySukces = $czySukces;
	$this->success = $czySukces;
	return $this;
    }

    public function setMsg($msg) {
	$this->msg = $msg;
	$this->addMsg($msg);
	return $this;
    }

    public function setUzytkownik($uzytkownik) {
	$this->uzytkownik = $uzytkownik;
	return $this;
    }

    function setOplaconeKonto($oplaconeKonto) {
	$this->oplaconeKonto = $oplaconeKonto;
	return $this;
    }

}
