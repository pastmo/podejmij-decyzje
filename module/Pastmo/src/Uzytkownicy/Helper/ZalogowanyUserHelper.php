<?php

namespace Pastmo\Uzytkownicy\Helper;

use Zend\View\Model\ViewModel;
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceManager;

class ZalogowanyUserHelper extends AbstractHelper {

    protected $uzytkownicyTable;

    public function __construct($uzytkownicyTable) {
	$this->uzytkownicyTable = $uzytkownicyTable;
    }

    public function __invoke() {
	$result = $this->uzytkownicyTable->getZalogowanegoUsera();
	return $result;
    }

}
