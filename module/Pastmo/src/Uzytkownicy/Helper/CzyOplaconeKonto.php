<?php

namespace Pastmo\Uzytkownicy\Helper;

class CzyOplaconeKonto extends \Zend\View\Helper\AbstractHelper {

    public function __construct($sm) {
	$this->kontaMenager = $sm->get(\Uzytkownicy\Menager\KontaMenager::class);
    }

    public function __invoke() {
	return $this->kontaMenager->czyWazneKonto();
    }

}
