<?php

namespace Pastmo\Uzytkownicy\Helper;

use Zend\View\Helper\AbstractHelper;

class DisabledHelper extends AbstractHelper {

    protected $count = 0;
    protected $uprawnieniaMenager;

    public function __construct($uprawnieniaMenager) {
	$this->uprawnieniaMenager = $uprawnieniaMenager;
    }

    public function __invoke($kod) {
	$result = $this->uprawnieniaMenager->sprawdzUprawnienie($kod, false);
	if (!$result) {
	    echo 'disabled';
	}
    }

}
