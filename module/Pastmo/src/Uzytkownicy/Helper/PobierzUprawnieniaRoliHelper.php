<?php

namespace Pastmo\Uzytkownicy\Helper;

use Zend\View\Helper\AbstractHelper;
use Pastmo\Wspolne\Utils\EntityUtil;

class PobierzUprawnieniaRoliHelper extends AbstractHelper {

    protected $uprawnieniaMenager;
    protected $logowanieFasada;

    public function __construct($sm) {
	$this->uprawnieniaMenager = $sm->get(\Logowanie\Menager\UprawnieniaMenager::class);
	$this->logowanieFasada = $sm->get(\Logowanie\Fasada\LogowanieFasada::class);
    }

    public function __invoke() {
	$zalogowany = $this->logowanieFasada->getZalogowanegoUsera();
	$result = $this->uprawnieniaMenager->pobierzUprawnieniaDlaRoli(
		EntityUtil::wydobadzId(
			EntityUtil::wydobadzPole($zalogowany, 'rola')
		)
	);

	return $result;
    }

}
