<?php

namespace Pastmo\Uzytkownicy;

use Zend\Mvc\MvcEvent;
use Pastmo\Uzytkownicy\Helper\UprawnieniaHelper;
use Pastmo\Uzytkownicy\Helper\DisabledHelper;
use Pastmo\Uzytkownicy\Helper\ZalogowanyUserHelper;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class KonfiguracjaModulu extends \Pastmo\KonfiguracjaModulu {

    const UZYTKOWNIK_TABLE_GATEWAY = 'UzytkownikTableGateway';

    public function onBootstrap(MvcEvent $e) {
	$viewHelperMenager = $e->getApplication()->getServiceManager()->get('ViewHelperManager');

	$viewHelperMenager->setFactory('disabled',
		function($sm) use ($e) {
	    $uprawnieniaMenager = $sm->get(\Logowanie\Menager\UprawnieniaMenager::getClass());
	    $viewHelper = new DisabledHelper($uprawnieniaMenager);
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('uprawnienia',
		function($sm) use ($e) {
	    $uprawnieniaMenager = $sm->get(\Logowanie\Menager\UprawnieniaMenager::getClass());
	    $viewHelper = new UprawnieniaHelper($uprawnieniaMenager);
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('zalogowanyUser',
		function($sm) use ($e) {
	    $uzytkownikMenager = $sm->get(\Logowanie\Menager\UzytkownicyMenager::getClass());
	    $viewHelper = new ZalogowanyUserHelper($uzytkownikMenager);
	    return $viewHelper;
	})
	;
	$viewHelperMenager->setFactory('pobierzUprawnieniaRoli',
		function($sm) use ($e) {
	    $viewHelper = new Helper\PobierzUprawnieniaRoliHelper($sm);
	    return $viewHelper;
	})
	;
	$viewHelperMenager->setFactory('czyOplaconeKonto',
		function($sm) use ($e) {
	    $viewHelper = new Helper\CzyOplaconeKonto($sm);
	    return $viewHelper;
	})
	;
    }

    public function getFactories() {
	return [
		Menager\JezykiMenager::class => function($sm) {
		    $menager = new Menager\JezykiMenager($sm);
		    return $menager;
		},
		Menager\UzytkownicyMenager::class => function($sm) {
		    $menager = $sm->get(\Logowanie\Menager\UzytkownicyMenager::class);
		    return $menager;
		},
		self::UZYTKOWNIK_TABLE_GATEWAY => function ($sm) {
		    return $this->ustawGateway($sm, new \Logowanie\Model\Uzytkownik($sm), 'uzytkownicy');
		},
		self::JEZYKI_GATEWAY => function($sm) {
		    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
		    $resultSetPrototype = new ResultSet();
		    $resultSetPrototype->setArrayObjectPrototype(new Entity\Jezyk($sm));
		    return new TableGateway('jezyki', $dbAdapter, null, $resultSetPrototype);
		},
	];
    }

}
