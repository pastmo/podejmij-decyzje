<?php

namespace Pastmo\Email\Fasada;

use Pastmo\Email\Menager\WysylanieMailiMenager;

class EmailFasada extends \Wspolne\Fasada\WspolneFasada {

    protected $emaileMenager;
    protected $wysylanieMailiMenager;

    public function __construct(\Zend\ServiceManager\ServiceManager $serviceMenager) {
	parent::__construct($serviceMenager);
	$this->ustawEmaileMenager();
	$this->wysylanieMailiMenager = new WysylanieMailiMenager($serviceMenager);
    }

    protected function ustawEmaileMenager() {
	$this->emaileMenager = $this->serviceMenager->get(\Pastmo\Email\Menager\EmaileMenager::class);
    }

    public function pobierzEmailaPoId($emailId) {
	return $this->emaileMenager->getRekord($emailId);
    }

    public function zapiszEmail($email) {
	$this->emaileMenager->zapisz($email);
    }

    public function wyslijEmail(\Pastmo\Email\Entity\EmailDoWysylki $emailDoWysylki) {

	return $this->wysylanieMailiMenager->wyslijEmail($emailDoWysylki);
    }

    public function utworzTrescZSzablonem($template,$parametry) {

	return $this->wysylanieMailiMenager->utworzTrescZSzablonem($template,$parametry);
    }



    public function zmienSkrzynke($emailId, $skrzynka) {

	return $this->emaileMenager->zmienSkrzynke($emailId, $skrzynka);
    }

    public function zmienStatus($emailId, $nowyStatus) {

	return $this->emaileMenager->zmienStatus($emailId, $nowyStatus);
    }

}
