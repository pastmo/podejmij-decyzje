<?php

namespace Pastmo\Email\Helper;

use Logowanie\Enumy\KodyUprawnien;

abstract class WyswietlGadzetEmail extends \Pastmo\Wspolne\Helper\TranslateHelper {

    protected $ustawieniaSystemuMenager;
    protected $czestotliwoscOdswiezaniaMaili;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	parent::__construct($sm);
	$this->ustawieniaSystemuMenager = $sm->get(\Pastmo\Wspolne\Menager\UstawieniaSystemuMenager::class);
    }

    public function __invoke() {
	if (!$this->view->uprawnienia(KodyUprawnien::MENU_EMAIL)) {
	    return;
	}
	$this->ustawZmienne();
	$this->dolaczSkrypty();
	$this->wyswietlHtml();
    }

    public function ustawZmienne() {
	$this->czestotliwoscOdswiezaniaMaili = $this->ustawieniaSystemuMenager->pobierzUstawienieZalogowanegoPoKodzie(\Email\Helper\WyswietlZmianeOdswiezaniaMaili::KOD_USTAWIEN_MAILI,
		\Email\Helper\WyswietlZmianeOdswiezaniaMaili::DOMYSLNA_CZESTOTLIWOSC_ODSWIEZANIA);
    }

    public function dolaczSkrypty() {
	\Application\Controller\LadowaczSkryptow::add($this->view->basePath('js/pastmo/timer.js'));
	\Application\Controller\LadowaczSkryptow::add($this->view->basePath('js/pastmo/email/bazowy_odswiezacz_email.js'));
	\Application\Controller\LadowaczSkryptow::add($this->view->basePath('js/helpery/odswiezacz_email.js'));
    }

    public function wyswietlHtml() {
	$this->wyswietlPoczatek();
	$this->wyswietlIkone();
	$this->wyswietlCzestotliwoscOdswiezania();
	$this->wyswietlKoniec();
    }

    public function wyswietlPoczatek() {
	?>
	<li>
	    <a id="gadzet_email" class="<?php echo $this->pobierzKlaseGadzetuEmail(); ?>"
	       href="<?php
	       echo $this->view->url('email', array('action' => 'index'));
	       ?>"
	       <?php $this->wyswietlTytulPozycjiMenu(); ?>
	       >
		   <?php
	       }

	       public function wyswietlTytulPozycjiMenu() {
		   ?>
		title="Nowe wiadomości email"
		<?php
	    }

	    public function wyswietlIkone() {
		?>


		<i class="fa fa-at">
		    <span class="item-quantity" style="display: none;">0</span>
		</i>

		<?php
	    }

	    public function wyswietlKoniec() {
		?>
	    </a>
	</li>
	<?php
    }

    public function wyswietlCzestotliwoscOdswiezania() {
	?>
	<script>
	    var czestotliwoscOdswiezaniaMaili = <?php echo $this->czestotliwoscOdswiezaniaMaili; ?>
	</script>
	<?php
    }

    abstract public function pobierzKlaseGadzetuEmail();
}
