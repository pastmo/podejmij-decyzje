<?php

namespace Pastmo\Email\Helper;

use Logowanie\Enumy\KodyUprawnien;

abstract class WyswietlZmianeOdswiezaniaMaili extends \Pastmo\Wspolne\Helper\TranslateHelper {

    const KOD_USTAWIEN_MAILI = "mail_r";
    const KOD_WSZYSTKICH_DOSTEPNYCH_MAILI = "mail_ts";
    const DOMYSLNA_CZESTOTLIWOSC_ODSWIEZANIA = "600000";

    private $ustawieniaSystemuMenager;
    private $aktualnyCzasOdswiezania;
    private $wszystkieDostepneCzasy;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	parent::__construct($sm);
	$this->ustawieniaSystemuMenager = $sm->get(\Pastmo\Wspolne\Menager\UstawieniaSystemuMenager::class);
    }

    public static function getDomyslneCzasy(){
	return ['10000' => '10s', "30000" => '30s', "60000" => '1min', "600000" => '10min'];
    }

    public static function czyWartoscWDomyslnychCzasach($szukanyKlucz){
	$domyslne=self::getDomyslneCzasy();
	return isset($domyslne[$szukanyKlucz]);
    }

    public function __invoke($view) {
	$this->view = $view;

	if (!$this->view->uprawnienia(KodyUprawnien::MENU_EMAIL)) {
	    return;
	}
	$this->aktualnyCzasOdswiezania = $this->ustawieniaSystemuMenager->pobierzUstawienieZalogowanegoPoKodzie(self::KOD_USTAWIEN_MAILI,
		'600000');
	$this->wszystkieDostepneCzasy = $this->ustawieniaSystemuMenager->pobierzUstawieniePoKodzie(self::KOD_WSZYSTKICH_DOSTEPNYCH_MAILI,
		self::getDomyslneCzasy());

	$this->wyswietlHtml();
    }

    public function wyswietlHtml() {
	?>
	<form id="zmiana_odswiezania_email">
	    <label for="wartosc"><?php echo $this->_translate("Częstotliwość odświeżania maili", false, 3); ?></label>
	    <select name="wartosc">
			<?php foreach ($this->wszystkieDostepneCzasy->wartosc as $wartosc => $opis): ?>
	    	<option value="<?php echo $wartosc; ?>" <?php $this->view->selected($wartosc.'',
			$this->aktualnyCzasOdswiezania->wartosc.'');
			    ?>><?php echo $opis; ?></option>
	<?php endforeach; ?>

	    </select>
	</form>
	<?php
    }

}
