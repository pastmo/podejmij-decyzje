<?php

namespace Pastmo\Email\Menager;

class EmaileZasobyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Module::EMAILE_ZASOBY_GATEWAY);
    }

    public function pobierzPoEmailu($emailId) {
	return $this->pobierzZWherem("email_id=$emailId");
    }

    public static function getClass() {
	return __CLASS__;
    }

}
