<?php

namespace Pastmo\Email\Menager;

use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

class WysylanieMailiMenager extends \Pastmo\Email\Menager\BazowyWysylaczMaili {

    use \Pastmo\Wspolne\Utils\GeneratorWidokow;

    private $emailDowysyki;

    public function wyslijEmail(\Pastmo\Email\Entity\EmailDoWysylki $emailDoWysylki) {
	$this->tresc = $emailDoWysylki->tresc;
	$this->trescHtml = $emailDoWysylki->trescHtml;
	$this->mail = $emailDoWysylki->odbiorcaEmail;
	$this->tytul = $emailDoWysylki->temat;
	$this->emailDowysyki = $emailDoWysylki;
	$this->zalacznikiId = $emailDoWysylki->idZasobow;
	$this->fromImie = $emailDoWysylki->uzytkownikKonto->konto_mailowe->user;
	$this->fromMail = $emailDoWysylki->uzytkownikKonto->konto_mailowe->user;
	$this->replyTo = $emailDoWysylki->replyTo;
	$this->send();
	return new \Logowanie\Polecenie\WynikPrzetwarzaniaFormularza(TRUE, 'wiadomość');
    }

    public function utworzTrescZSzablonem($template, $parametry) {
	$this->initGeneratorWidokow();

	$partial = $this->viewRender->partial($template, $parametry);

	$layout = new \Zend\View\Model\ViewModel(['content' => $partial]);
	$layout->setTemplate('email/layout/email');

	$wynik = $this->viewRender->render($layout);

	return $wynik;
    }

    protected function ustawTransport() {

	$transport = new SmtpTransport();

	$host = $this->emailDowysyki->uzytkownikKonto->konto_mailowe->host;
	$login = $this->emailDowysyki->uzytkownikKonto->konto_mailowe->user;
	$port = $this->emailDowysyki->uzytkownikKonto->konto_mailowe->port_stmp;
	$haslo = $this->emailDowysyki->uzytkownikKonto->konto_mailowe->getHasloJawne();

	$options = new SmtpOptions([
		'name' => $host,
		'host' => $host,
		'connection_class' => 'plain',
		'connection_config' => [
			'username' => $login,
			'password' => $haslo,
		],
	]);

	if ($port) {
	    $options->port = $port;
	}

	$transport->setOptions($options);
	return $transport;
    }

    public static function getClass() {
	return __CLASS__;
    }

}
