<?php

namespace Pastmo\Email\Menager;

use Zend\Mail\Storage\Imap;
use Zend\Mail\Storage;
use Pastmo\Email\Entity\RekordSzczegoluMaila;
use Pastmo\Email\Entity\RekordNaglowkaEmail;

class EmailScaner {

    use \Pastmo\Wspolne\Utils\TranslateTrait;

    public $errors;
    public $polaczenie;
    private $host;
    private $user;
    private $password;
    private $port_imap;
    private $ssl;
    private $sm;
    private $kontoMailowe;
    private $zasobyTable;
    private $emaileMenager;
    private $poprzednioOstatniEmail;

    /* $host = 'serwer1578643.home.pl',
      $user = 'testowe1@grupaglasso.pl', $password = 'Testowe1', */

    public function __construct($sm = null) {
	$this->sm = $sm;
	$this->zasobyTable = $this->sm->get(\Zasoby\Menager\ZasobyUploadMenager::getClass());
	$this->translator = $this->sm->get(\Pastmo\Tlumaczenia\Menager\PastmoTranslator::class);
	$this->logiMenager = $this->sm->get(\Pastmo\Logi\Menager\LogiMenager::class);
    }

    public function init(\Pastmo\Email\Entity\KontoMailowe $kontoMailowe) {
	$this->host = $kontoMailowe->host;
	$this->user = $kontoMailowe->user;
	$this->password = $kontoMailowe->getHasloJawne();
	$this->port_imap = $kontoMailowe->port_imap;
	$this->ssl = $kontoMailowe->ssl;
	$this->kontoMailowe = $kontoMailowe;
    }

    public function connect() {
	$params = array('host' => $this->host,
		'user' => $this->user,
		'password' => $this->password,
	);
	try {

	    if ($this->port_imap) {
		$params['port'] = $this->port_imap;
	    }
	    if ($this->ssl) {
		$params['ssl'] = $this->ssl;
	    }

	    $this->polaczenie = new \Zend\Mail\Storage\Imap($params);
	} catch (\Exception $e) {

	    unset($params['password']);
	    $this->logujMaila($params);

	    $message = $e->getMessage();

	    if ($message === 'cannot login, user or password wrong') {
		throw new \Pastmo\Wspolne\Exception\PastmoException($this->_translate('Błędny login lub hasło', false, 4));
	    }
	    if (false !== strpos($message, 'cannot connect to host')) {
		$newMessage = sprintf($this->_translate("Nie można połączyć się z serwerem '%s'", false, 5), $this->host);
		throw new \Pastmo\Wspolne\Exception\PastmoException($newMessage);
	    }

	    throw $e;
	}
    }

    public function pobierMaileDoWyswietlenia($folder, $strona, $rekordowNaStrone) {
	$this->connect();
	$odpowiedz = \Pastmo\Email\Entity\OdpowiedzNaglowkowMaili::create($this->sm);
	$odpowiedz->setPolaczenie($this->polaczenie);
	$odpowiedz->setBiezacyFolderFizycznaNazwa($folder);
	$this->emaileMenager = $this->sm->get(\Pastmo\Email\Menager\EmaileMenager::class);

	$this->polaczenie->selectFolder($folder);
	$ilosc = $this->polaczenie->countMessages();
	$odpowiedz->setIleWszystkichMaili($ilosc);
	$odpowiedz->setIleNieprzeczytanych($this->polaczenie->countMessages(Storage::FLAG_UNSEEN));


	$start = $ilosc - $strona * $rekordowNaStrone;
	$koniec = $start - $rekordowNaStrone;
	$koniec = $koniec >= 0 ? $koniec : 0;

	for ($i = $start; $i > $koniec; $i--) {
	    try {
		$message = $this->polaczenie->getMessage($i);

		$mail = new RekordNaglowkaEmail($this->zasobyTable);
		$mail->init($message);
		$mail->setId($i);

		$odpowiedz->addToListaPobranychMaili($mail);
	    } catch (\Exception $e) {
		$raw = $this->polaczenie->getRawHeader($i) . $this->polaczenie->getRawContent($i);
		$msg = $e->getMessage();
		$this->errors[] = $msg;

		$zasob = $this->zasobyTable->zapiszPlik('error' . $i . '.txt', $raw);

		$odpowiedz->addErrors($msg, $zasob->id);
	    }
	}

	$odpowiedz->setFoldery($this->polaczenie->getFolders());

	$this->disconnect();

	return $odpowiedz;
    }

    public function pobierzSzczegolyMaila($id, $folder) {
	$odpowiedz = new \Pastmo\Email\Entity\OdpowiedzSzczegoluMaila();

	try {
	    $this->connect();

	    $this->polaczenie->selectFolder($folder);
	    $message = $this->polaczenie->getMessage($id);
	    $odpowiedz->init($message);

	    $this->disconnect();
	} catch (\Exception $e) {
	    $odpowiedz->addError($this->translateConst($e->getMessage()));
	}

	return $odpowiedz;
    }

    public function disconnect() {
	try {
	    if ($this->polaczenie) {
		$this->polaczenie->close();
	    }
	} catch (\Exception $e) {
	    $this->logujMaila($e->getMessage());
	}
    }

    private function logujMaila($msg) {
	$builder = \Pastmo\Logi\Builder\LogiBuilder::getBuilder()->setKodKategorii('email_err');
	$builder->dodajTresc($msg);
	$this->logiMenager->dodajLog($builder);
    }

    public function getAllMessages() {
	$this->connect();
	$result = array();
	$ilosc = $this->polaczenie->countMessages();

	for ($i = $ilosc; $i > 0; $i--) {
	    $message = $this->polaczenie->getMessage($i);

	    $mail = new RekordSzczegoluMaila();
	    $mail->init($message, $this->getRawMessageFromImap($this->polaczenie, $i));
	    $result[] = $mail;
	}
	$this->disconnect();

	return $result;
    }

    public function getUnhandledMessages($limit = false) {
	$this->connect();
	$odpowiedz = \Pastmo\Email\Entity\OdpowiedzOdswiezania::create();
	$this->emaileMenager = $this->sm->get(\Pastmo\Email\Menager\EmaileMenager::class);

	$ilosc = $this->polaczenie->countMessages();

	for ($i = $ilosc; $i > 0; $i--) {
	    try {
		if ($limit && $odpowiedz->ilePobranychMaili >= KontaMailoweMenager::LIMIT_WIADOMOSCI_W_CZESCIOWYM_ODSWIEZANIU) {
		    break;
		}
		$message = $this->polaczenie->getMessage($i);

		$messageId = RekordSzczegoluMaila::generujMessageId($message);
		$czyIstnieje = $this->emaileMenager->sprawdzCzyMailJuzIstniejeZMailId($messageId, $this->kontoMailowe->id);
		if ($czyIstnieje) {
		    continue;
		}

		$mail = new RekordSzczegoluMaila();
		$mail->init($message, $this->getRawMessageFromImap($this->polaczenie, $i));

		$odpowiedz->addToListaPobranychMaili($mail);
		$odpowiedz->ilePobranychMaili++;
	    } catch (\Exception $e) {
		$raw = $this->polaczenie->getRawHeader($i) . $this->polaczenie->getRawContent($i);
		$msg = $e->getMessage();
		$this->errors[] = $msg;

		$zasob = $this->zasobyTable->zapiszPlik('error' . $i . '.txt', $raw);

		$odpowiedz->addErrors($msg, $zasob->id);
	    }
	}

	$odpowiedz->setIleMailiZostaloDoPobrania($i);

	$this->disconnect();

	return $odpowiedz;
    }

    public function sprawdzPolaczenie() {
	try {
	    $this->connect();
	    $this->disconnect();
	    return true;
	} catch (\Exception $e) {
	    return $e->getMessage();
	}
    }

    private function getRawMessageFromImap($imap, $index) {
	return $imap->getRawHeader($index) . $imap->getRawContent($index);
    }

    public function initConstantTranslation() {
	$this->constantTranslator = $this->sm->get(\Lokalizacje\Menager\ConstantTranslatorMenager::class);
    }

    public static function getClass() {
	return __CLASS__;
    }

}
