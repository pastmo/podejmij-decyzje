<?php

namespace Pastmo\Email\Menager;

use Zend\ServiceManager\ServiceManager;
use Pastmo\Wspolne\Utils\EntityUtil;

class KontaMailoweMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public $emailScaner;
    public $emaileMenager;

    const LIMIT_WIADOMOSCI_W_CZESCIOWYM_ODSWIEZANIU = 10;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Pastmo\Module::KONTA_MAILOWE_GATEWAY);
	$this->emaileMenager = $this->sm->get(\Email\Menager\EmaileMenager::class);
    }

    public function pobierzKontoMailoweDlaUzytkownika($host, $userSerwera, $userAplikacjiId) {
	$kontaMailowe = $this->pobierzZWherem("host='$host' AND user='$userSerwera' AND uzytkownik_id=$userAplikacjiId");

	if (count($kontaMailowe) > 0) {
	    return $kontaMailowe[0];
	} else {
	    return null;
	}
    }

    public function pobierzKontoMailowe($host, $userSerwera) {
	$kontaMailowe = $this->pobierzZWherem("host='$host' AND user='$userSerwera'");

	if (count($kontaMailowe) > 0) {
	    return $kontaMailowe[0];
	} else {
	    return null;
	}
    }

    public function pobierzKontaMailoweUzytkownika($userAplikacjiId) {
	$kontaMailowe = $this->pobierzZWherem("uzytkownik_id=$userAplikacjiId", "id ASC");

	return $kontaMailowe;
    }

    public function odswiezMaileZWszystkichSkrzynek() {
	try {
	    $zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();
	    $konta = $zalogowany->getKontaMailowe();
	    foreach ($konta as $konto) {
		if ($konto->id !== 0) {
		    $this->odsiwezMaile($konto->id);
		}
	    }
	} catch (\Exception $e) {
	    $this->emailScaner->errors[] = $e->getMessage();
	}
    }

    public function odswiezMaileZWszystkichSkrzynekCzesciowo(array $ignorowaneSkrzynkiId = []) {
	$wynik = [];

	$konta = $this->getLogowanieFasada()->pobierzKontaMailoweDlaZalogowanegoUzytkownika();

	foreach ($konta as $uzytkownikKontoMailowe) {

	    $kontoId = EntityUtil::wydobadzId($uzytkownikKontoMailowe->konto_mailowe);
	    if ($kontoId !== 0 && !in_array($kontoId, $ignorowaneSkrzynkiId)) {
		$wynik[] = $this->odsiwezMaileCzesciowo($kontoId);
	    }
	}

	$this->logujBledy($wynik);

	return $wynik;
    }

    public function odsiwezMaileCzesciowo($kontoId) {
	return $this->odswiezMaileWspolne($kontoId, true);
    }

    public function odsiwezMaile($kontoId) {
	return $this->odswiezMaileWspolne($kontoId, false);
    }

    public function odswiezMaileWspolne($kontoId, $czesioweOdswiezanie) {

	try {
	    $kontoMailowe = $this->getRekord($kontoId);
	    $this->emailScaner = $kontoMailowe->pobierzEmailScanera();
	    $odpowiedzOdswiezania = $this->emailScaner->getUnhandledMessages($czesioweOdswiezanie);
	    $odpowiedzOdswiezania->setIdKonta($kontoId);

	    for ($i = $odpowiedzOdswiezania->ilePobranychMaili - 1; $i >= 0; $i--) {
		$mailSkanera = $odpowiedzOdswiezania->listaPobranychMaili[$i];
		$this->zapiszEmailZeSkanera($mailSkanera, $kontoMailowe);

		$email = $this->emaileMenager->getPoprzednioDodany();
		$odpowiedzOdswiezania->addPrzetworzoneMaile($email);
	    }
	} catch (\Exception $e) {
	    $odpowiedzOdswiezania = \Pastmo\Email\Entity\OdpowiedzOdswiezania::create()->addErrors($e->getMessage(),
		    NULL);
	}


	return $odpowiedzOdswiezania;
    }

    public function logujBledy(array $odpowiedziOdswiezania) {

	$logiMenager=  $this->get(\Pastmo\Logi\Menager\LogiMenager::class);

	foreach($odpowiedziOdswiezania as $odpowiedz){
	    if(count($odpowiedz->errors)===0){
		continue;
	    }

	    $builder=\Pastmo\Logi\Builder\LogiBuilder::getBuilder()->setKodKategorii('email_err');

	    foreach($odpowiedz->errors as $error){
			$tresc=$error->msg;
			$builder->dodajTresc($tresc);

	    }

	  $logiMenager->dodajLog($builder);


	}
    }

    public function zapiszEmailZeSkanera($mailSkanera, $kontoMailowe) {
	$mail = \Pastmo\Email\Entity\Email::konwertujZMailSkanera($mailSkanera, $kontoMailowe, $this->sm);

	$this->emaileMenager->zapisz($mail);
    }

    public function pobierMaileDoWyswietlenia($kontoId, $skrzynka, $strona, $rekordowNaStrone){
	try {
	    $kontoMailowe = $this->getRekord($kontoId);
	    $this->emailScaner = $kontoMailowe->pobierzEmailScanera();
	    $odpowiedzOdswiezania = $this->emailScaner->pobierMzaileDoWyswietlenia($kontoId, $skrzynka, $strona, $rekordowNaStrone);
	    $odpowiedzOdswiezania->setIdKonta($kontoId);

	} catch (\Exception $e) {
	    $odpowiedzOdswiezania = \Pastmo\Email\Entity\OdpowiedzOdswiezania::create()->addErrors($e->getMessage(),
		    NULL);
	}


	return $odpowiedzOdswiezania;
    }

    public static function getClass() {
	return __CLASS__;
    }

}
