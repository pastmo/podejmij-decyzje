<?php

namespace Pastmo\Email\Menager;

use Zend\ServiceManager\ServiceManager;
use Pastmo\Email\Entity\Email;
use Pastmo\Email\Entity\EmailZasob;
use Pastmo\Wiadomosci\Enumy\StatusyWiadomosci;

class EmaileMenager extends \Pastmo\Wiadomosci\Menager\WiadomosciZeStatusemMenager {

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, $this->getGatewayConst());
    }

    protected function getGatewayConst() {
	return \Pastmo\Module::MAILE_GATEWAY;
    }

    public function sprawdzCzyMailJuzIstnieje(Email $mail) {
	$istnejace = $this->pobierzZWherem(
		"konto_mailowe_id=" . $mail->konto_mailowe->id .
		" AND data_naglowka='" . $mail->data_naglowka . "'"
		. " AND tytul='" . $mail->tytul . "'");

	return count($istnejace) > 0;
    }

    public function sprawdzCzyMailJuzIstniejeZMailId($mailTekstowyId, $kontoId) {
	$istnejace = $this->pobierzZWherem(
		"konto_mailowe_id=" . $kontoId .
		" AND message_id='" . $mailTekstowyId . "'");

	return count($istnejace) > 0;
    }

    public function pobierzWolneMaileDlaKonta($kontoMailoweId) {
	$return = $this->pobierzResultSetZWherem('konto_mailowe_id=' . $kontoMailoweId . " AND wiadomosc_id IS NULL");
	return $return;
    }

    public function pobierzEmaileDlaKontaISkrzynki($kontoMailoweId, $skrzynka) {
	$where = 'konto_mailowe_id=' . $kontoMailoweId . ""
		. " AND status !='" . StatusyWiadomosci::UKRYTA . "'"
		. $this->whereDlaSkrzynki($skrzynka);
	$return = $this->pobierzResultSetZWherem($where
	);
	return $return;
    }

    protected function whereDlaSkrzynki($skrzynka) {
	return " AND skrzynka='$skrzynka'";
    }

    public function zapisz(\Pastmo\Wspolne\Entity\WspolnyModel $email) {

	parent::zapisz($email);
	$potencjalneZasoby = $email->pobierzPotencjalneZasoby();

	if ($email->id === null) {
	    $email = $this->getPoprzednioDodany();
	}

	$emaileZasobyMenager = $this->get(\Pastmo\Email\Menager\EmaileZasobyMenager::getClass());

	foreach ($potencjalneZasoby as $potencjalnyZasob) {
	    $zasobyTable = $this->get(\Zasoby\Menager\ZasobyUploadMenager::getClass());
	    $zasob = $zasobyTable->zapiszPlik($potencjalnyZasob->nazwaPliku, $potencjalnyZasob->trescPliku);

	    $emailZasob = new EmailZasob();
	    $emailZasob->zasob = $zasob;
	    $emailZasob->email = $email;
	    $emaileZasobyMenager->zapisz($emailZasob);
	}
    }

    public function ustawJakoPrzeczytane($id) {
	$this->zmienStatus($id, StatusyWiadomosci::PRZECZYTANA);
    }

    public function pobierzSkrzynke($skrzynka) {
	return $skrzynka;
    }

    public function pobierzOstatnioZapisanegoMaila($idKontaMailowego){
	$wynik= $this->pobierzZWherem("konto_mailowe_id=$idKontaMailowego",self::DOMYSLNY_ORDER,1);

	if(count($wynik)>0){
	    return $wynik[0];
	}

	return Email::create();
    }

    public static function getClass() {
	return __CLASS__;
    }

}
