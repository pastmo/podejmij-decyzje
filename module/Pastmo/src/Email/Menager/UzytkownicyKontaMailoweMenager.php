<?php

namespace Pastmo\Email\Menager;

use Zend\ServiceManager\ServiceManager;
use Logowanie\Menager\UzytkownicyMenager;

class UzytkownicyKontaMailoweMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    const DEFAULT_STMP_PORT = '587';
    const DEFAULT_IMAP_PORT = '993';

    private $post;
    private $kontoMailoweMenager;
    private $uzytkownikId;
    //
    private $id;
    private $host;
    private $host_wychodzacy;
    private $userSerwera;
    private $imap;
    private $stmp;
    private $ssl;
    private $hasloJawne;
    //
    private $kontoMailowe;
    private $uzytkownikKontoMailowe;
    private $odpowiedzZapisuKontMailowych;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Logowanie\Module::UZYTKOWNICY_KONTA_MAILOWE_GATEWAY);
    }

    public function zapiszKontaMailowe($post) {
	$uzytkownikTable = $this->sm->get(UzytkownicyMenager::class);
	$zalogowany = $uzytkownikTable->getZalogowanegoUsera();
	$this->zapiszKontaMailoweDlaUzytkownika($post, $zalogowany->id);
    }

    public function pobierzDlaZalogowanegoUzytkownika() {
	$zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();
	$wynik = $this->pobierzZWherem("uzytkownik_id={$zalogowany->id}");
	return $wynik;
    }

    public function zapiszKontaMailoweDlaUzytkownika($post, $uzytkownikId) {

	$this->kontoMailoweMenager = $this->sm->get(\Pastmo\Email\Menager\KontaMailoweMenager::class);
	$this->post = $post;
	$this->uzytkownikId = $uzytkownikId;

	$count = count($this->post['host']);

	for ($i = 0; $i < $count; $i++) {

	    $this->zapiszPojedynczeUzytkownikKontoMailowe($i);
	}
    }

    public function sprawdzPoprawnoscKontMailowych() {
	$this->odpowiedzZapisuKontMailowych = \Pastmo\Email\Entity\OdpowiedzZapisuKontMailowych::create();
	$uzytkownicyKonta = $this->pobierzDlaZalogowanegoUzytkownika();

	foreach ($uzytkownicyKonta as $uzytkownikKonto) {
	    $scaner = $uzytkownikKonto->konto_mailowe->pobierzEmailScanera();
	    $wynik = $scaner->sprawdzPolaczenie();
	    $this->odpowiedzZapisuKontMailowych->addWynikDlaKont($uzytkownikKonto, $wynik);
	    if ($wynik !== true) {
		$this->odpowiedzZapisuKontMailowych->setSukces(false);
	    }
	}

	return $this->odpowiedzZapisuKontMailowych;
    }

    private function zapiszPojedynczeUzytkownikKontoMailowe($i) {

	$this->ustawParametry($i);
	if (!$this->sprawdzParametry()) {
	    return;
	}
	$this->pobierzGlowneEncje();
	$this->ustawParametryKontaMailowego();

	$this->kontoMailoweMenager->zapisz($this->kontoMailowe);

	$this->odswiezEncjeKontaMailowego();
	$this->polaczUzytkownikaZKontemMailowym();
    }

    private function ustawParametry($i) {
	$this->id = $this->post['id'][$i];
	$this->host = $this->post['host'][$i];
	$this->host_wychodzacy = $this->post['host_wy'][$i];
	$this->userSerwera = $this->post['user'][$i];
	$this->imap = $this->pobierzNiepewnyIndex('imap', $i);
	$this->stmp = $this->pobierzNiepewnyIndex('stmp', $i);
	$this->ssl = $this->pobierzNiepewnyIndex('ssl', $i);
	$this->hasloJawne = $this->post['password'][$i];
    }

    private function sprawdzParametry() {
	return $this->userSerwera || $this->userSerwera === "";
    }

    private function pobierzGlowneEncje() {
	if ($this->id) {
	    $this->uzytkownikKontoMailowe = $this->getRekord($this->id);
	    $this->kontoMailowe = $this->uzytkownikKontoMailowe->konto_mailowe;
	} else {
	    $this->pobierzKontoMailowe();
	}
    }

    private function pobierzKontoMailowe() {
	$this->kontoMailowe = $this->kontoMailoweMenager->pobierzKontoMailowe($this->host, $this->userSerwera);

	if (!$this->kontoMailowe) {
	    $this->kontoMailowe = new \Pastmo\Email\Entity\KontoMailowe($this->sm);
	}
    }

    private function ustawParametryKontaMailowego() {
	$this->kontoMailowe->port_imap = $this->imap;
	$this->kontoMailowe->port_stmp = $this->stmp;
	$this->kontoMailowe->host_wychodzacy = $this->host_wychodzacy;
	$this->kontoMailowe->host = $this->host;
	$this->kontoMailowe->user = $this->userSerwera;
	$this->kontoMailowe->uzytkownik = $this->uzytkownikId;
	$this->kontoMailowe->ssl = $this->ssl;

	if ($this->hasloJawne !== "") {
	    $this->kontoMailowe->ustawHaslo($this->hasloJawne);
	}
    }

    private function odswiezEncjeKontaMailowego() {
	if (!$this->kontoMailowe->id) {
	    $this->kontoMailowe = $this->kontoMailoweMenager->getPoprzednioDodany();
	}
    }

    private function polaczUzytkownikaZKontemMailowym() {
	$wczesniejsze = $this->pobierzZWheremCount("uzytkownik_id=$this->uzytkownikId AND konto_mailowe_id={$this->kontoMailowe->id}");

	if ($wczesniejsze === 0) {
	    $this->uzytkownikKontoMailowe = new \Logowanie\Entity\UzytkownikKontoMailowe();
	    $this->uzytkownikKontoMailowe->uzytkownik = $this->uzytkownikId;
	    $this->uzytkownikKontoMailowe->konto_mailowe = $this->kontoMailowe;

	    $this->zapisz($this->uzytkownikKontoMailowe);
	}
    }

    private function pobierzNiepewnyIndex($index, $i) {
	if (isset($this->post[$index])) {
	    $wynik = $this->post[$index][$i];
	    return $wynik;
	}
	return null;
    }

}
