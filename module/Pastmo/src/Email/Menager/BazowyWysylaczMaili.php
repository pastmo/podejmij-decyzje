<?php

namespace Pastmo\Email\Menager;

use Zend\ServiceManager\ServiceManager;
use Zend\Mail;
use Zend\Mime;
use Logowanie\Polecenie\WynikPrzetwarzaniaFormularza;

class BazowyWysylaczMaili extends \Pastmo\Wspolne\Menager\WspolnyMenager {

    protected $title;
    protected $body;
    protected $copyIfNotWork;
    protected $LINK = '<a href="%s">%s</a>';
    protected $zalacznikiId = array();
    public $id;
    public $kod;
    public $mail;
    public $test = false;
    protected $tytul;
    protected $trescHtml;
    protected $tresc;
    protected $akcja = 'aktywuj';
    protected $fromImie;
    protected $fromMail;
    protected $replyTo = false;

    public function send() {

	$mail = new Mail\Message();
	$mail->setBody($this->getTresc());
	if ($this->fromMail !== null) {
	    $mail->setFrom($this->fromMail, $this->fromImie);
	}
	if ($this->replyTo) {
	    $mail->setReplyTo($this->replyTo);
	}
	$mail->addTo($this->mail, 'Uzytkownik');
	$mail->setSubject($this->getTytul());

	$transport = $this->ustawTransport();

	if (!$this->test) {
	    $transport->send($mail);
	}
	return new WynikPrzetwarzaniaFormularza(TRUE, "Wysłano maila");
    }

    public function ustawParametry($mail, $idUzytkownika, $kod) {
	$this->mail = $mail;
	$this->id = $idUzytkownika;
	$this->kod = $kod;
	$this->trescHtml = $this->generujTresc();
	$this->tytul = $this->utworzTytul();
    }

    public function utworzTytul() {
	return $this->title;
    }

    public function generujTresc() {
	$result = "";

	$link = $this->generujLinka($this->id, $this->kod, $this->test);
	$result.=$this->body;
	$result.=sprintf($this->LINK, $link, $link);
	$result.=$this->copyIfNotWork;

	return $result;
    }

    public function generujLinka($id, $kod) {
	$link = $this->generujUrl('logowanie', array('action' => $this->akcja), array('id' => $id, 'kod' => $kod),
		!$this->test);
	return $link;
    }

    public function getTytul() {
	return $this->tytul;
    }

    public function getTresc() {

	$wysylka = array();

	if ($this->tresc) {
	    $text = new Mime\Part($this->tresc);
	    $text->type = 'text/plain';
	    $text->charset = 'utf-8';

	    $wysylka[] = $text;
	}

	if ($this->trescHtml) {
	    $text = new Mime\Part($this->trescHtml);
	    $text->type = 'text/html';
	    $text->charset = 'utf-8';

	    $wysylka[] = $text;
	}

	$zasobyTable = $this->sm->get(\Zasoby\Menager\ZasobyUploadMenager::class);

	foreach ($this->zalacznikiId as $zalId) {

	    $zalacznik = $zasobyTable->getRekord($zalId);
	    $zasobPath = $zasobyTable->zasobRealPath($zalacznik);
	    $fileContent = fopen($zasobPath, 'r');
	    $attachment = new Mime\Part($fileContent);
	    $attachment->type = $zalacznik->typ_fizyczny;
	    $attachment->filename = $zalacznik->nazwa;
	    $attachment->disposition = Mime\Mime::DISPOSITION_ATTACHMENT;
	    $attachment->encoding = Mime\Mime::ENCODING_BASE64;

	    $wysylka[] = $attachment;
	}
	$mimeMessage = new Mime\Message();
	$mimeMessage->setParts($wysylka);

	return $mimeMessage;
    }

    protected function ustawTransport() {
	return new Mail\Transport\Sendmail();
    }

    public static function getClass() {
	return __CLASS__;
    }

}
