<?php

namespace Pastmo\Email\Entity;

use \Pastmo\Email\Menager\EmaileMenager;

class EmailZasob extends \Wspolne\Model\WspolneModel {

    public $id;
    public $email;
    public $zasob;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	if ($this->czyTabeleDostepne()) {
	    $this->data = $data;
	    $this->zasob = $this->pobierzTabeleObca('zasob_id', \Zasoby\Menager\ZasobyUploadMenager::class,
		    new \Zasoby\Model\Zasob());
	    $this->email = $this->pobierzTabeleObca('email_id', EmaileMenager::class, new Email());
	}
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'email':
		return 'email_id';
	    case 'zasob':
		return 'zasob_id';
	    default:
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
