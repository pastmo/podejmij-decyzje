<?php

namespace Pastmo\Email\Entity;

class RekordNaglowkaEmail {

    public $id;
    public $tytul;
    public $nadawca;
    public $data;
    public $zasoby = array();
    public $messageId;
    private $zasobyTable;

    public function __construct(\Zasoby\Menager\ZasobyUploadMenager $zasobyTable) {
	$this->zasobyTable = $zasobyTable;
    }

    public static function generujMessageId($message) {
	if (isset($message->messageId) && $message->messageId !== null) {
	    $messageId = $message->messageId;
	} else {
	    $messageId = $message->date . $message->from . md5($message->subject);
	}

	return $messageId;
    }

    public function init($message) {
	$this->tytul = isset($message->subject) ? $message->subject : "Brak tematu";
	$this->nadawca = $message->from;
	$this->data = $message->date;
	$this->messageId = self::generujMessageId($message);
    }

    public function setId($id) {
	$this->id = $id;
	return $this;
    }

}
