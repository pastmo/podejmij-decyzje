<?php

namespace Pastmo\Email\Entity;

class OdpowiedzOdswiezania implements \JsonSerializable {

    public $ileMailiZostaloDoPobrania;
    public $ilePobranychMaili;
    public $listaPobranychMaili = [];
    public $przetworzoneMaile = [];
    public $idKonta;
    public $errors = [];

    public static function create() {
	return new OdpowiedzOdswiezania();
    }

    public function setIleMailiZostaloDoPobrania($ileMailiZostaloDoPobrania) {
	$this->ileMailiZostaloDoPobrania = $ileMailiZostaloDoPobrania;
	return $this;
    }

    public function setIlePobranychMaili($ilePobranychMaili) {
	$this->ilePobranychMaili = $ilePobranychMaili;
	return $this;
    }

    public function addToListaPobranychMaili($pobranyEmail) {
	$this->listaPobranychMaili[] = $pobranyEmail;
	return $this;
    }

    public function addPrzetworzoneMaile(Email $email) {
	$przetworzony=  PrzetworzonyEmail::create()
		->setId($email->id)
		->setData_naglowka($email->data_naglowka)
		->setTytul($email->tytul)
		->setNadawca($email->nadawca);

	$this->przetworzoneMaile[] = $przetworzony;
	return $this;
    }

    public function setIdKonta($idKonta) {
	$this->idKonta = $idKonta;
	return $this;
    }

    public function addErrors($msg, $zasobId) {
	$this->errors[] = OdswiezanieError::create($msg, $zasobId);
	return $this;
    }

    public function jsonSerialize() {
	$this->listaPobranychMaili = [];
	return $this;
    }

}


