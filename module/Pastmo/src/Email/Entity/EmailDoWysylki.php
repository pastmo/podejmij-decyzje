<?php

namespace Pastmo\Email\Entity;

class EmailDoWysylki {

    public $uzytkownikKonto;
    public $odbiorcaEmail;
    public $replyTo = false;
    public $odbiorcaImieNazwisko;
    public $temat;
    public $tresc;
    public $trescHtml;
    public $idZasobow;

    private function __construct() {
	$this->idZasobow = array();
    }

    public function setUzytkownikKonto(\Logowanie\Entity\UzytkownikKontoMailowe $uzytkownikKonto) {
	$this->uzytkownikKonto = $uzytkownikKonto;
	return $this;
    }

    public function setOdbiorcaEmail($odbiorcaEmail) {
	$this->odbiorcaEmail = $odbiorcaEmail;
	return $this;
    }

    public function setReplyTo($replyTo) {
	$this->replyTo = $replyTo;
    }

    public function setOdbiorcaImieNazwisko($odbiorcaImieNazwisko) {
	$this->odbiorcaImieNazwisko = $odbiorcaImieNazwisko;
	return $this;
    }

    public function setTemat($temat) {
	$this->temat = $temat;
	return $this;
    }

    public function settresc($tresc) {
	$this->tresc = $tresc;
	return $this;
    }

    public function setTrescHtml($trescHtml) {
	$this->trescHtml = $trescHtml;
	return $this;
    }

    public function setIdZasobow($idZasobow) {
	$this->idZasobow = $idZasobow;
	return $this;
    }

    public static function create() {
	$wynik = new EmailDoWysylki();

	return $wynik;
    }

}
