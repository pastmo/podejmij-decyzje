<?php

namespace Pastmo\Email\Entity;

use Zend\Mime\Decode;

class RekordSzczegoluMaila {

    public $tytul;
    public $nadawca;
    public $data;
    public $tresc;
    public $zrodlo;
    public $zasoby = array();
    public $messageId;
    private $trescPlain = "";
    private $trescHtml = "";

    public static function generujMessageId($message) {
	if (isset($message->messageId) && $message->messageId !== null) {
	    $messageId = $message->messageId;
	} else {
	    $messageId = $message->date . $message->from . md5($message->subject);
	}

	return $messageId;
    }

    public function init($message, $zrodlo = "brak źródła") {
	$this->tytul = isset($message->subject) ? $message->subject : "Brak tematu";
	$this->nadawca = $message->from;
	$this->data = $message->date;
	$this->messageId = self::generujMessageId($message);
	$this->zrodlo = $zrodlo;
	if ($message->isMultipart()) {

	    $this->tresc = $this->getTrescZMultipart($message);
	} else {
	    $this->tresc = $message->getContent();
	}

	$this->odkodujWiadomosc();
	$this->usunTabulatoryZTresci();
    }

    private function getTrescZMultipart($message) {
	foreach (new \RecursiveIteratorIterator($message) as $part) {
	    try {
		if ($this->czyTypTekstowy($part)) {
		    $this->getTrescZPart($part);
		} else {
		    $potencjalny = $this->wyciagnijZasob($part);
		    if ($potencjalny) {
			$this->zasoby[] = $potencjalny;
		    }
		}
	    } catch (Zend\Mail\Exception $e) {
		// ignore
	    }
	}
	return $this->zwrocTresc();
    }

    private function getTrescZPart($part) {
	$partTresc = \Pastmo\Wspolne\Utils\StrUtil::base64_decode($part . '');
	if ($this->czyPlain($part)) {
	    $this->trescPlain .= $partTresc;
	} else if ($this->czyHtml($part)) {
	    $this->trescHtml .= $partTresc;
	}
    }

    private function czyTypTekstowy($part) {
	return $this->czyPlain($part) || $this->czyHtml($part);
    }

    private function czyPlain($part) {
	$contentType = $part->contentType;
	return strtok($contentType, ';') == 'text/plain';
    }

    private function czyHtml($part) {
	$contentType = $part->contentType;
	return strtok($contentType, ';') == 'text/html';
    }

    private function zwrocTresc() {
	if ($this->trescHtml && $this->trescHtml !== '') {
	    return $this->trescHtml;
	} else {
	    return $this->trescPlain;
	}
    }

    private function wyciagnijZasob($part) {

	$filename = $this->poberzNazwePliku($part);
	if (!$filename) {
	    return false;
	}
	$content = base64_decode($part->getContent());

	return new PotencjalnyZasob($filename, $content);
    }

    private function poberzNazwePliku($part) {

	$type;

	foreach ($part->getHeaders() as $name => $value) {
	    if ($value instanceof \Zend\Mail\Header\GenericHeader && $value->getFieldName() === 'Content-Disposition') {
		$headerFilename = $value;

		$headerFile = $headerFilename->getFieldValue(false);
		$filename = preg_replace(array('~^.*; filename=(.*)$~', '~;[\s\S]*~'), array('$1', ''), $headerFile);
		return $filename;
	    } else if ($value instanceof \Zend\Mail\Header\ContentType) {
		$type = $value->getType();
	    }
	}
	return false;
    }

    private function odkodujWiadomosc() {
	$this->tresc = quoted_printable_decode($this->tresc);
//		$this->contenet=iconv('ASCII', 'UTF-8', $this->contenet);//TODO konwertować to na utf8
//		$this->contenet = utf8_encode ($this->contenet);
//	$this->contenet = \Pastmo\Wspolne\Utils\DomUtil::decode_qprint($this->contenet);
    }

    private function usunTabulatoryZTresci() {
	$this->tresc = str_replace("\t\t", "", $this->tresc);
	$this->tresc = str_replace("\t", " ", $this->tresc);
    }

}
