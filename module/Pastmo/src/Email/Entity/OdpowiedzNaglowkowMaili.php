<?php

namespace Pastmo\Email\Entity;

use Zend\ServiceManager\ServiceManager;
use Zend\Mail\Storage;

class OdpowiedzNaglowkowMaili implements \JsonSerializable {

    use \Pastmo\Wspolne\Utils\TranslateTrait;

    public $listaPobranychMaili = [];
    public $idKonta;
    public $errors = [];
    public $ileWszystkichMaili;
    public $ileNieprzeczytanych;
    public $biezacyFolder;
    public $biezacyFolderFizycznaNazwa;
    public $foldery = [];
    protected $sm;
    protected $polaczenie = false;

    public static function create(ServiceManager $sm) {
	return new OdpowiedzNaglowkowMaili($sm);
    }

    public function __construct(ServiceManager $sm = null) {
	if ($sm) {
	    $this->sm = $sm;
	    $this->translator = $this->sm->get(\Pastmo\Tlumaczenia\Menager\PastmoTranslator::class);
	}
    }

    public function initConstantTranslation() {
	if ($this->sm) {
	    $this->constantTranslator = $this->sm->get(\Lokalizacje\Menager\ConstantTranslatorMenager::class);
	}
    }

    public function addToListaPobranychMaili($pobranyEmail) {
	$this->listaPobranychMaili[] = $pobranyEmail;
	return $this;
    }

    public function setIdKonta($idKonta) {
	$this->idKonta = $idKonta;
	return $this;
    }

    public function addErrors($msg, $zasobId) {
	$this->errors[] = OdswiezanieError::create($msg, $zasobId);
	return $this;
    }

    public function setIleWszystkichMaili($ileWszystkichMaili) {
	$this->ileWszystkichMaili = $ileWszystkichMaili;
	return $this;
    }

    public function setIleNieprzeczytanych($ileNieprzeczytanych) {
	$this->ileNieprzeczytanych = $ileNieprzeczytanych;
	return $this;
    }

    public function setBiezacyFolder($biezacyFolder) {
	$this->biezacyFolder = $biezacyFolder;
	return $this;
    }

    public function setBiezacyFolderFizycznaNazwa($biezacyFolderFizycznaNazwa) {
	$this->biezacyFolderFizycznaNazwa = $biezacyFolderFizycznaNazwa;
	return $this;
    }

    public function setFoldery($foldery) {
	$this->foldery = [$this->pobierzPoziomFolderow($foldery)];
	return $this;
    }

    private function pobierzPoziomFolderow($folder) {
	$localName = $folder->getLocalName();
	$convertedLocalName = imap_mutf7_to_utf8($localName);

	$wynik = Folder::create()
		->setNazwa($this->translateConst($convertedLocalName))
		->setNazwa_fizyczna($folder->getGlobalName())
		->setSelectable($folder->isSelectable())
	;

	if ($wynik->nazwa_fizyczna === $this->biezacyFolderFizycznaNazwa) {
	    $this->setBiezacyFolder($wynik->nazwa);
	}

	if ($folder->isSelectable() && $this->polaczenie) {
	    $this->polaczenie->selectFolder($folder->getGlobalName());
	    $wynik->setIleNieprzeczytanych($this->polaczenie->countMessages(Storage::FLAG_UNSEEN));
	}

	$subfoldery = [];
	foreach ($folder as $dziecko) {
	    $subfoldery[] = $this->pobierzPoziomFolderow($dziecko);
	}

	$wynik->setFoldery($subfoldery);
	return $wynik;
    }

    public function setPolaczenie($polaczenie) {
	$this->polaczenie = $polaczenie;
    }

    public function jsonSerialize() {
	unset($this->translator);
	unset($this->constantTranslator);
	unset($this->polaczenie);
	return $this;
    }

}

class Folder {

    public $nazwa;
    public $nazwa_fizyczna;
    public $selectable;
    public $ileNieprzeczytanych = 0;
    public $foldery = [];

    public static function create() {
	return new Folder();
    }

    public function setNazwa($nazwa) {
	$this->nazwa = $nazwa;
	return $this;
    }

    public function setNazwa_fizyczna($nazwa_fizyczna) {
	$this->nazwa_fizyczna = $nazwa_fizyczna;
	return $this;
    }

    public function setSelectable($selectable) {
	$this->selectable = $selectable;
	return $this;
    }

    public function setIleNieprzeczytanych($ileNieprzeczytanych) {
	$this->ileNieprzeczytanych = $ileNieprzeczytanych;
	return $this;
    }

    public function setFoldery($foldery) {
	$this->foldery = $foldery;
	return $this;
    }

}
