<?php

namespace Pastmo\Email\Entity;

use Pastmo\Email\Entity\RekordSzczegoluMaila;
use Pastmo\Wspolne\Utils\EntityUtil;

class Email extends \Pastmo\Wiadomosci\Entity\WiadomoscZeStatusem {

    public $id;
    public $data_naglowka;
    public $nadawca;
    public $tytul;
    public $tresc;
    public $skrzynka;
    public $konto_mailowe;
    public $wiadomosc;
    public $message_id;
    public $zrodlo;
    private $emailZasoby = array();
    private $potencjalneZasoby = array();

    public function dowiazListyTabelObcych() {
	if ($this->czyTabeleDostepne() && $this->id) {
	    $emailZasobyMenager = $this->sm->get(\Pastmo\Email\Menager\EmaileZasobyMenager::getClass());
	    $this->emailZasoby = $emailZasobyMenager->pobierzPoEmailu($this->id);
	}
    }

    public function getEmailZasoby() {
	$this->dowiazListyTabelObcych();
	return $this->emailZasoby;
    }

    public function pobierzPotencjalneZasoby() {
	return $this->potencjalneZasoby;
    }

    public function exchangeArray($data) {
	parent::exchangeArray($data);
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->data_naglowka = $this->pobierzLubNull($data, 'data_naglowka');
	$this->nadawca = $this->pobierzLubNull($data, 'nadawca');
	$this->tytul = $this->pobierzLubNull($data, 'tytul');
	$this->tresc = $this->pobierzLubNull($data, 'tresc');
	$this->message_id = $this->pobierzLubNull($data, 'message_id');
	$this->skrzynka = $this->pobierzLubNull($data, 'skrzynka');
	$this->zrodlo = $this->pobierzLubNull($data, 'zrodlo');

	$this->data = $data;
	$this->konto_mailowe = $this->pobierzTabeleObca('konto_mailowe_id',
		\Pastmo\Email\Menager\KontaMailoweMenager::class, new KontoMailowe());
	$this->wiadomosc = $this->pobierzTabeleObca('wiadomosc_id', \Konwersacje\Model\WiadomosciMenager::class,
		new \Konwersacje\Entity\Wiadomosc());
	$this->zrodlo = $this->pobierzTabeleObca('zrodlo_id', \Zasoby\Menager\ZasobyUploadMenager::class,
		new \Pastmo\Zasoby\Entity\Zasob());
    }

    public function getNadawcaEmail() {
	$nadawcaArray = $this->podzielNadawce();

	return \Wspolne\Utils\StrUtil::skrocStr($nadawcaArray[1], 1);
    }

    public function getNadawcaImie() {
	$nadawcaArray = $this->podzielNadawce();
	return explode(" ", $nadawcaArray[0])[0];
    }

    public function getNadawcaNazwisko() {
	$nadawcaArray = $this->podzielNadawce();
	return explode(" ", $nadawcaArray[0])[1];
    }

    private function podzielNadawce() {
	return explode("<", $this->nadawca);
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'konto_mailowe':
		return 'konto_mailowe_id';
	    case 'zrodlo':
		return 'zrodlo_id';
	    case 'wiadomosc':
		return 'wiadomosc_id';
	    default:
		return $nazwaWKodzie;
	}
    }

    public function dodajPotencjalnyZasob($zasob) {
	$this->potencjalneZasoby[] = $zasob;
    }

    public function getUzytkownika() {
	$uzytkownik = EntityUtil::wydobadzPole($this->wiadomosc, 'autor');
	if ($uzytkownik) {
	    return $uzytkownik;
	}
    }

    public function getProjekt() {
	$watek = EntityUtil::wydobadzPole($this->wiadomosc, 'watek');
	if ($watek) {
	    $projekt = $watek->pobierzWlasciciela();

	    if ($projekt) {
		return $projekt;
	    }
	}

	return \Projekty\Entity\Projekt::create();
    }

    public function pobierzDate() {
	$przetworzonaData = preg_replace('( \([A-Z]+\))', '', $this->data_naglowka);

	$date = \DateTime::createFromFormat('D, d M Y H:i:s O', $przetworzonaData);

	if ($date) {
	    $wynik = $date->format(\Pastmo\Wspolne\Utils\DateUtil::FORMAT_TIMESTAMP);

	    return $wynik;
	} else {
	    return $this->data_naglowka;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public static function konwertujZMailSkanera($mailSkanera, $kontoMailowe, $sm) {
	$mail = new Email();

	$mail->data_naglowka = $mailSkanera->data;
	$mail->tresc = $mailSkanera->tresc;
	$mail->tytul = $mailSkanera->tytul;
	$mail->konto_mailowe = $kontoMailowe;
	$mail->nadawca = $mailSkanera->nadawca;
	$mail->message_id = $mailSkanera->messageId;

	$zasobyFasada = $sm->get(\Zasoby\Fasada\ZasobyFasada::class);
	$mail->zrodlo = $zasobyFasada->zapiszPlik($mail->message_id . '.txt', $mailSkanera->zrodlo);

	$mail->emailZasoby = array();
	foreach ($mailSkanera->zasoby as $zasob) {

	    $mail->dodajPotencjalnyZasob($zasob);
	}

	return $mail;
    }

    public function zrobArray() {
	$wynik = parent::zrobArray();

	return $wynik;
    }

    public static function create() {
	return new self();
    }

}
