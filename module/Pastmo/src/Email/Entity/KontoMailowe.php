<?php

namespace Pastmo\Email\Entity;

use Zend\Crypt\BlockCipher;

class KontoMailowe extends \Wspolne\Model\WspolneModel {

    const KLUCZ = "klucz szyfrowaniaasdfhkj asdlfjhas;dlkfj ;k;lkjkkkjasd; k;k;LKADFJ;LK;LHASDMVNZCM  asd;lfkjas;dlkfj;alskdjf;lk kasd;lkfja;sldk ff;lkasjdf;lkjkj;lkjs;lfkjashalkjdflkjadgfsaljfhcbvzcvkarhdvsjvd415254151a5dsf14asdf1asd";
    const SOL = "15487454598746548574174";

    public $id;
    public $uzytkownik;
    public $host;
    public $host_wychodzacy;
    public $user;
    public $port_imap;
    public $port_stmp;
    public $ssl;
    public $passwordKodowane;
    private $passwordJawne;
    public $emailScaner;
    protected $foldery;


    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->host = $this->pobierzLubNull($data, 'host');
	$this->host_wychodzacy = $this->pobierzLubNull($data, 'host_wychodzacy');
	$this->user = $this->pobierzLubNull($data, 'user');
	$this->port_imap = $this->pobierzLubNull($data, 'port_imap');
	$this->port_stmp = $this->pobierzLubNull($data, 'port_stmp');
	$this->ssl = $this->pobierzLubNull($data, 'ssl');
	$this->passwordKodowane = $this->pobierzLubNull($data, 'password');
    }

    public function getFoldery() {
	$this->pobierzListeZTabeliObcej(\Email\Menager\EmaileFolderyMenager::class, 'pobierzDlaKontaMailowego',
		$this->id, null, 'foldery');
	return $this->foldery;
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'passwordKodowane':
		return 'password';
	    case 'uzytkownik':
		return 'uzytkownik_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public function ustawHaslo($hasloJawne) {
	$cipher = BlockCipher::factory('mcrypt', array('algorithm' => 'aes'));
	$cipher->setKey(self::KLUCZ);
	$cipher->setSalt(self::SOL);

	$this->passwordJawne = $hasloJawne;
	$this->passwordKodowane = $cipher->encrypt($hasloJawne);
    }

    public function getHasloJawne() {
	$cipher = BlockCipher::factory('mcrypt', array('algorithm' => 'aes'));
	$cipher->setKey(self::KLUCZ);
	$cipher->setSalt(self::SOL);
	$encrypted = $cipher->decrypt($this->passwordKodowane);
	return $encrypted;
    }

    public function pobierzEmailScanera() {
	if (!$this->emailScaner) {
	    $this->emailScaner = new \Pastmo\Email\Menager\EmailScaner($this->sm);
	    $this->emailScaner->init($this);
	}
	return $this->emailScaner;
    }

    public function pobierzOstatnioZapisanegoMaila() {
	$emaileMenager = $this->sm->get(\Email\Menager\EmaileMenager::class);

	return $emaileMenager->pobierzOstatnioZapisanegoMaila($this->id);
    }

    public static function create() {
	return new KontoMailowe();
    }

    public static function getClass() {
	return __CLASS__;
    }

}
