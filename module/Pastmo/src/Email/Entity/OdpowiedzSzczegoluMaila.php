<?php

namespace Pastmo\Email\Entity;

class OdpowiedzSzczegoluMaila implements \JsonSerializable {

    public $idKonta;
    public $errors = [];
    public $email;

    public static function create() {
	return new OdpowiedzSzczegoluMaila();
    }

    public function init($message) {
	$this->email = new RekordSzczegoluMaila();
	$this->email->init($message);
    }

    public function addError($msg) {
	$this->errors[] = $msg;
	return $this;
    }

    public function jsonSerialize() {
	return $this;
    }

}
