<?php

namespace Pastmo\Email\Entity;

class OdswiezanieError {

    public $msg = "";
    public $zasobId = 0;

    public static function create($msg, $zasobId) {
	$error = new OdswiezanieError();
	$error->msg = $msg;
	$error->zasobId = $zasobId;
	return $error;
    }

}
