<?php

namespace Pastmo\Email\Entity;

class PrzetworzonyEmail {

    public $id;
    public $tytul;
    public $nadawca;
    public $data_naglowka;

    public static function create() {
	return new PrzetworzonyEmail();
    }

    public function setId($id) {
	$this->id = $id;
	return $this;
    }

    public function setTytul($tytul) {
	$this->tytul = $tytul;
	return $this;
    }

    public function setNadawca($nadawca) {
	$this->nadawca = $nadawca;
	return $this;
    }

    public function setData_naglowka($data_naglowka) {
	$this->data_naglowka = $data_naglowka;
	return $this;
    }

}