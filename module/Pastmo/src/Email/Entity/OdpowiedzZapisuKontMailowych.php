<?php

namespace Pastmo\Email\Entity;

class OdpowiedzZapisuKontMailowych {

    public $sukces = true;
    public $wynikDlaKont = [];

    public function setSukces($sukces) {
	$this->sukces = $sukces;
	return $this;
    }

    public function addWynikDlaKont($uzytkownikKonto, $czySukces) {
	$this->wynikDlaKont[$uzytkownikKonto->id] = (object)
		['sukces' => $czySukces, 'uzytkownikKonto' => $uzytkownikKonto];
	return $this;
    }

    public static function create() {
	return new OdpowiedzZapisuKontMailowych();
    }

}
