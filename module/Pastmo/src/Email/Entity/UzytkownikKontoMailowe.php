<?php

namespace Pastmo\Email\Entity;

use \Logowanie\Menager\UzytkownicyMenager;
use \Logowanie\Model\Uzytkownik;
use \Pastmo\Email\Menager\KontaMailoweMenager;
use \Pastmo\Email\Entity\KontoMailowe;

class UzytkownikKontoMailowe extends \Wspolne\Model\WspolneModel {

    public $id;
    public $konto_mailowe;
    public $uzytkownik;

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);

	$this->haslo = $this->pobierzLubNull($data, 'password');

	if ($this->czyTabeleDostepne()) {
	    $this->data = $data;
	    $this->uzytkownik = $this->pobierzTabeleObca('uzytkownik_id', UzytkownicyMenager::class,
		    Uzytkownik::create());
	    $this->konto_mailowe = $this->pobierzTabeleObca('konto_mailowe_id', KontaMailoweMenager::class,
		    new KontoMailowe());
	}
    }

    public function dowiazListyTabelObcych() {

    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'konto_mailowe':
		return 'konto_mailowe_id';
	    case 'uzytkownik':
		return 'uzytkownik_id';
	    case 'passwordKodowane':
		return 'password';

	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public static function create() {
	$wynik = new self();

	$wynik->konto_mailowe = new KontoMailowe();
	$wynik->uzytkownik = Uzytkownik::create();

	return $wynik;
    }

    public static function zrobDomyslneKontoNadawcze($sm) {
	$konfig = $sm->get("config");

	$host = $konfig['konto_email']['host'];
	$user = $konfig['konto_email']['user'];
	$passwordKodowane = $konfig['konto_email']['passwordKodowane'];

	$konto = new UzytkownikKontoMailowe();
	$konto->konto_mailowe = new KontoMailowe();
	$konto->konto_mailowe->host = $host;
	$konto->konto_mailowe->user = $user;
	$konto->konto_mailowe->passwordKodowane = $passwordKodowane;
	return $konto;
    }

}
