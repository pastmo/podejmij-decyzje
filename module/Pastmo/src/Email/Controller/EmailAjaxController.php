<?php

namespace Pastmo\Email\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Pastmo\Wspolne\Adnotacje\DomyslnyJsonModel;

/**
 * @DomyslnyJsonModel
 */
class EmailAjaxController extends \Wspolne\Controller\WspolneController {

    protected $zasobyTable;

    public function indexAction() {
	$watekWIadomosci = $this->getRequest()->getQuery('id');

	$indexView = new ViewModel(array(
		'aktualnyWatek' => $watekWIadomosci ? $watekWIadomosci : 0
	));
	$indexView->setTemplate('konwersacje/konwersacje/wiadomosci_szablon');
	$this->uzytkownikWatkekWiadomosciTable->ustawPrzeczytwane();
	$view = new ViewModel();
	$view->addChild($indexView, 'wiadomosci');

	return $view;
    }

    public function odswiezMaileZeWszystkichSkrzynekAction() {
	$this->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$parametry = $this->params()->fromQuery('ignorowane_konta', []);

	$wynik = $this->kontaMailoweMenager->odswiezMaileZWszystkichSkrzynekCzesciowo($parametry);

	return $this->returnSuccess(['odpowiedz' => $wynik]);
    }

    public function dodajAction() {
	if (!$this->uzytkownikTable->getZalogowanegoUsera()) {
	    return $this->redirect()->toRoute('logowanie', array('action' => 'index'));
	}

	$userId = $this->params()->fromRoute('id', null);

	$request = $this->getRequest();
	if ($request->isPost()) {
	    $post = $request->getPost();

	    $wiadomosc = $this->wiadomosciMenager->zrobWiadomosc($post);

	    $this->wiadomosciMenager->zapisz($wiadomosc);

	    $this->redirect()->toRoute('konwersacje', array('action' => 'index'));
	}

	switch ($this->params('format')) {
	    case 'json':
		return new JsonModel(array(
			'success' => true,
			'wiadomosci' => $doWyslania,
		));
	    default:
		return new ViewModel(
			array('uzytkownicy' => $this->uzytkownikTable->fetchAll(),
			'wybrany_uzytkownik' => $userId
		));
	}
    }

    public function watkiAction() {
	try {
	    $zalogowany = $this->uzytkownikTable->getZalogowanegoUsera();
	    if (!$zalogowany) {
		return new JsonModel(array('success' => false, 'message' => "Niezalogowany użytkownik"));
	    }

	    $konta = $zalogowany->getKontaMailowe();

	    if (count($konta) > 0) {
		$aktywneKonto = $konta[0];
		$this->kontaMailoweMenager->odsiwezMaile($aktywneKonto->id);


		$emails = $this->maileMenager->pobierzWolneMaileDlaKonta($aktywneKonto->id);


		$watkiDoWyslania = array();

		foreach ($emails as $email) {

		    $watkiDoWyslania[] = array(
			    'watek' => $email,
			    'dodano' => $email->data_naglowka);
		}

		return $this->returnSuccess(array('watki_wiadomosci' => $watkiDoWyslania));
	    }
	    return $this->returnSuccess(array('watki_wiadomosci' => []));
	} catch (\Pastmo\Wspolne\Exception\PastmoException $e) {
	    return $this->returnFail(['msg' => $e->getMessage()]);
	}
    }

    public function wiadomosciAction() {
	$zalogowany = $this->uzytkownikTable->getZalogowanegoUsera();
	if (!$zalogowany) {
	    return new JsonModel(array('success' => false, 'message' => "Niezalogowany użytkownik"));
	}

	$watekId = null;
	$post = $this->getPost();
	if ($post) {
	    $watekId = $post['watek_id'];
	}

	$wiadomosc = $this->maileMenager->getRekord($watekId);
	$emaileZasoby = $wiadomosc->getEmailZasoby();

	$zasoby = array();

	foreach ($emaileZasoby as $emailZasob) {
	    $zasob = array(
		    'zasob' => $emailZasob->zasob,
		    'obrazek' => $emailZasob->zasob->wyswietl()
	    );
	    $zasoby[] = $zasob;
	}

	return $this->returnSuccess(array('wiadomosc' => $wiadomosc, 'nadawca' => $wiadomosc->getNadawcaEmail(), 'zasoby' => $zasoby));
    }

    public function pobierzWspolneWiadomosci($srodek) {
	$indexView = new ViewModel(array(
	));
	//	$indexView->setTemplate('konwersacje/konwersacje/wiadomosci_szablon');

	$indexView->addChild($srodek, 'srodek');


	return $indexView;
    }

    public function zmienCzestotliwoscOdswiezaniaAction() {
	$this->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);
	$wartosc = $this->params()->fromPost('wartosc', 0);
	if (\Email\Helper\WyswietlZmianeOdswiezaniaMaili::czyWartoscWDomyslnychCzasach($wartosc)) {

	    $this->ustawieniaSystemuMenager->aktualizujUstawieniaZalogowanego(\Email\Helper\WyswietlZmianeOdswiezaniaMaili::KOD_USTAWIEN_MAILI,
		    $wartosc);
	    return $this->returnSuccess();
	}
	return $this->returnFail(['msg' => $this->_translate("Błędny czas odświeżania", false, 2)]);
    }

}
