<?php

namespace Pastmo\Email;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Pastmo\Email\Entity\KontoMailowe;
use Pastmo\Email\Entity\Email;
use Pastmo\Email\Entity\EmailZasob;

class KonfiguracjaModulu extends \Pastmo\KonfiguracjaModulu {

    public function onBootstrap(\Zend\Mvc\MvcEvent $e) {
	$viewHelperMenager = $e->getApplication()->getServiceManager()->get('ViewHelperManager');

	$viewHelperMenager->setFactory('wyswietlGadzetEmail',
		function($sm) use ($e) {
	    $viewHelper = new \Email\Helper\WyswietlGadzetEmail($sm);
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('wyswietlZmianeOdswiezaniaMaili',
		function($sm) use ($e) {
	    $viewHelper = new \Email\Helper\WyswietlZmianeOdswiezaniaMaili($sm);
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('wyswietlRekordEmail',
		function($sm) use ($e) {
	    $viewHelper = new \Email\Helper\WyswietlRekordEmail($sm);
	    return $viewHelper;
	});
    }

    public function getFactories() {
	return array(
		\Pastmo\Email\Menager\EmailScaner::class => function($sm) {
		    $table = new \Pastmo\Email\Menager\EmailScaner($sm);
		    return $table;
		},
		\Pastmo\Email\Fasada\EmailFasada::class => function($sm) {

		    $table = new \Pastmo\Fasada\EmailFasada($sm);
		    return $table;
		},
		\Pastmo\Email\Menager\WysylanieMailiMenager::class => function($sm) {

		    $table = new \Pastmo\Menager\WysylanieMailiMenager($sm);
		    return $table;
		},
		\Pastmo\Email\Menager\EmaileMenager::class => function($sm) {

		    $table = new \Pastmo\Email\Menager\EmaileMenager($sm);
		    return $table;
		},
		\Pastmo\Email\Menager\KontaMailoweMenager::getClass() => function($sm) {

		    $table = new \Pastmo\Email\Menager\KontaMailoweMenager($sm);
		    return $table;
		},
		\Pastmo\Email\Menager\EmaileZasobyMenager::class => function($sm) {

		    $table = new \Pastmo\Email\Menager\EmaileZasobyMenager($sm);
		    return $table;
		},
		\Pastmo\Module::KONTA_MAILOWE_GATEWAY => function ($sm) {
		    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
		    $resultSetPrototype = new ResultSet();
		    $resultSetPrototype->setArrayObjectPrototype(new KontoMailowe($sm));
		    return new TableGateway('konta_mailowe', $dbAdapter, null, $resultSetPrototype);
		},
		\Pastmo\Module::MAILE_GATEWAY => function ($sm) {
		    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
		    $resultSetPrototype = new ResultSet();
		    $resultSetPrototype->setArrayObjectPrototype(new Email($sm));
		    return new TableGateway('emaile', $dbAdapter, null, $resultSetPrototype);
		},
		\Pastmo\Module::EMAILE_ZASOBY_GATEWAY => function ($sm) {
		    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
		    $resultSetPrototype = new ResultSet();
		    $resultSetPrototype->setArrayObjectPrototype(new EmailZasob($sm));
		    return new TableGateway('emaile_zasoby', $dbAdapter, null, $resultSetPrototype);
		},);
    }

}
