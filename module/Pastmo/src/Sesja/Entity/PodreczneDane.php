<?php

namespace Pastmo\Sesja\Entity;

class PodreczneDane extends \Wspolne\Model\WspolneModel {
    
    public $id;
    public $uzytkownik_id;
    public $namespace;
    public $klucz;
    public $wartosc;
    
    public function exchangeArray($data) {
        $this->id = $this->pobierzLubNull($data, self::ID);
        $this->uzytkownik_id = $this->pobierzLubNull($data, 'uzytkownik_id');
        $this->namespace = $this->pobierzLubNull($data, 'namespace');
        $this->klucz = $this->pobierzLubNull($data, 'klucz');
        $this->wartosc = $this->pobierzLubNull($data, 'wartosc');
    }
    
    public function konwertujNaKolumneDB($nazwaWKodzie) {
        return $nazwaWKodzie;
    }
    
    public function pobierzKlase() {
        return __CLASS__;
    }
    
    public static function create($sm = null) {
        $encja = new PodreczneDane($sm);
        return $encja;
    }
}