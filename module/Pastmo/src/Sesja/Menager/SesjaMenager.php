<?php

namespace Pastmo\Sesja\Menager;

use Zend\Session\Container;
use Zend\Session\SessionManager;

class SesjaMenager extends \Pastmo\Wspolne\Menager\WspolnyMenager {

    protected $containerEkranu;
    protected $containerTrybuTlumaczen;
    protected $kontenery = [];
    protected $containerUzytkownicy;

    const UZYTKOWNICY_CONTAINER = 'UzytkownicyContainer';
    const EKRAN_CONTAINER = "EKRAN_CONTAINER";
    const SZEROKOSC_EKRANU = "SZEROKOSC_EKRANU";
    const TRYB_TLUMACZEN = "TRYB_TLUMACZEN";
    const UZYTKOWNIK_KEY = 'user_id';

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm);

	$this->init();
    }

    public function init() {
	try {
	    $this->initTry();
	} catch (\Exception $e) {

	    if (session_status() !== PHP_SESSION_NONE) {
		session_destroy();
	    }

	    $this->initKontaineryBledu();
	    $this->logujBlad($e->getMessage());
	}
    }

    public function initTry() {
	foreach ($this->kontenery as $zmienna => $kluczKontenera) {
	    $this->$zmienna = $this->createContainer($kluczKontenera);
	}
    }

    public function initKontaineryBledu() {
	foreach ($this->kontenery as $zmienna => $kluczKontenera) {
	    $this->$zmienna = new \Pastmo\Sesja\Entity\ContainerBlednejSesji($kluczKontenera);
	}
    }

    public function createContainer(string $namespace) {
	return new Container($namespace);
    }

    public function createContainerBazodanowy(string $namespace, \Zend\ServiceManager\ServiceManager $sm) {
	return new \Pastmo\Sesja\ContainerBazodanowy($namespace, $sm);
    }

    public function ustawZawartoscKlucza(Container $container, string $key, $value) {
	$container->offsetSet($key, $value);
    }

    public function sprawdzCzyIstniejeKlucz(Container $container, string $key) {
	return $container->offsetExists($key);
    }

    public function usunZawartoscKlucza(Container $container, string $key) {
	if ($container->offsetExists($key)) {
	    $container->offsetUnset($key);
	}
    }

    public function zwrocZawartoscKluczaLubFalsz(Container $container, string $key) {
	if ($container->offsetExists($key)) {
	    return $container->offsetGet($key);
	} else {
	    return false;
	}
    }

    public function ustawKluczBooleanLubUsun(Container $container, string $key, bool $value) {
	if ($value) {
	    $this->ustawZawartoscKlucza($container, $key, $value);
	} else {
	    $this->usunZawartoscKlucza($container, $key);
	}
    }

    public function ustawKluczLubUsun(Container $container, string $key, $value) {
	if (!empty($value)) {
	    $this->ustawZawartoscKlucza($container, $key, $value);
	} else {
	    $this->usunZawartoscKlucza($container, $key);
	}
    }

    public function zakonczSesje() {
	$container = $this->createContainer('destroy');
	$sessionManager = $container->getManager();
	$sessionManager->destroy();
    }

    public function zapiszSzerokoscEkranu($nowaSzerokosc) {
	$this->ustawKluczLubUsun($this->containerEkranu, self::SZEROKOSC_EKRANU, $nowaSzerokosc);
    }

    public function getSzerokoscEkranu() {
	$wynik = $this->zwrocZawartoscKluczaLubFalsz($this->containerEkranu, self::SZEROKOSC_EKRANU);

	return $wynik ? $wynik : 0;
    }

    public function setTrybTlumaczen($czyTrybTlumaczen) {
	$this->ustawKluczLubUsun($this->containerTrybuTlumaczen, self::TRYB_TLUMACZEN, $czyTrybTlumaczen);
    }

    public function czyTrybTlumaczen() {
	$wynik = $this->zwrocZawartoscKluczaLubFalsz($this->containerTrybuTlumaczen, self::TRYB_TLUMACZEN);

	return $wynik ? $wynik : 0;
    }

    public function logujBlad($msg) {
	$logiMenager = $this->get(\Pastmo\Logi\Menager\LogiMenager::class);
	$builder = \Pastmo\Logi\Builder\LogiBuilder::getBuilder()->setKodKategorii('error');
	$builder->dodajTresc($msg);
	$builder->setIgnorujZalogowanego(true);
	$logiMenager->dodajLog($builder);
    }

    public function ustawIdUzytkownika($id = null) {
	$this->ustawKluczLubUsun($this->containerUzytkownicy, $this::UZYTKOWNIK_KEY, $id);
    }

    public function pobierzIdUzytkownika() {
	$uzytkownikId = $this->zwrocZawartoscKluczaLubFalsz($this->containerUzytkownicy, $this::UZYTKOWNIK_KEY);
	if ($uzytkownikId) {
	    return $uzytkownikId;
	}
    }

}
