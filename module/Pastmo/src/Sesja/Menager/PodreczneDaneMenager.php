<?php

namespace Pastmo\Sesja\Menager;

use Pastmo\Wspolne\Utils\ArrayUtil;
use Logowanie\Fasada\LogowanieFasada;

class PodreczneDaneMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {
    
    private $podreczneDane;
    private $namespace;
    private $uzytkownik_id;
    protected $sm;
    
    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
        parent::__construct($sm, \Pastmo\Sesja\KonfiguracjaModulu::PODRECZNE_DANE_GATEWAY);
        $this->sm = $sm;
    }
    
    public function zwrocDaneZBazy($key) {
        
        //        $ret = null;
        
//       $podreczneDaneMenager = new \Pastmo\Sesja\Menager\PodreczneDaneMenager($this->sm);
//        $podreczneDaneMenager->dodajNoweDane(['namespace'=>'1']);
//        if (!$this->offsetExists($key)) {
//            return $ret;
//        }
//        $storage = $this->getStorage();
//        $name    = $this->getName();
//        $ret =& $storage[$name][$key];   
        $this->setIdZalogowanegoUzytkownika();
        
        $result = $this->pobierzZWherem($this->warunek($key));

        if (empty($result)) {
            return $result;
        }
        $wynik = ArrayUtil::pobierzPierwszyElement($result);
        return $wynik;
    }
    
    public function dodajNoweDane($key, $value) {
        
//        $this->expireKeys($key);
//        $storage = $this->verifyNamespace();
//        $name    = $this->getName();
//        $storage[$name][$key] = $value; 
        
        $this->podreczneDane = new \Pastmo\Sesja\Entity\PodreczneDane();
        $data = $this->przygotujDane($key, $value);
        $this->podreczneDane->exchangeArray($data);
        $podreczneDane = $this->podreczneDane;
        $this->zapisz($podreczneDane);
    }
    
    public function sprawdzCzyDaneIstnieja($key) {
        
        // If no container exists, we can't inspect it
        
//        if (null === ($storage = $this->verifyNamespace(false))) {
//            return false;
//        }
//        $name = $this->getName();
//
        // Return early if the key isn't set
//        if (!isset($storage[$name][$key])) {
//            return false;
//        }
//
//        $expired = $this->expireKeys($key);
//
//        return !$expired;
        return true;
    }
    
    public function setNamespace($namespace) {
        $this->namespace = $namespace;
    }
    
    public function setIdZalogowanegoUzytkownika() {
        $id = $this->sm->get(LogowanieFasada::class)->getZalogowanegoUsera()->id; 
        $this->uzytkownik_id = $id;
    }
    
    public function przygotujDane($key, $value = null) {
        $data = [];
        $daneIstnieja = $this->pobierzZWherem($this->warunek($key, $value));
        if ($daneIstnieja) {
            $data['id'] = ArrayUtil::pobierzPierwszyElement($daneIstnieja)->id;
        }
        $data['namespace'] = $this->namespace; 
        $this->setIdZalogowanegoUzytkownika();
        $data['uzytkownik_id'] = $this->uzytkownik_id;
        $data['klucz'] = $key;
        $data['wartosc'] = json_encode($value);
        return $data;
    }
    
    private function warunek($key, $value = null) {
        $where  = "uzytkownik_id = '". $this->uzytkownik_id ."' AND ";
        $where .= "namespace = '" . $this->namespace . "' AND ";
        $where .= "klucz = '". $key ."'";
        return $where;
    }
}
