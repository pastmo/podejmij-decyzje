<?php

namespace Pastmo\Faktury\Menager;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;
use Klienci\Entity\Klient;
use Pastmo\Wspolne\Utils\EntityUtil;

class KlienciMenager extends BazowyMenagerBazodanowy {

    protected $sm;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, 'KlienciGateway');
    }

    public function zapiszKlienta($post) {

	$klient = new Klient($this->sm);
	$klient->exchangeArray($post);
	unset($post['id']);

	if (!EntityUtil::wydobadzId($klient->uzytkownik)) {
	    $zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();
	    $klient->uzytkownik = $zalogowany->id;
	}

	$this->zapisz($klient);

	if (!$klient->id) {
	    $klient = $this->getPoprzednioDodany();
	}

	$this->aktualizujPowiazanaTabele($klient->id, 'adres', AdresyMenager::class, $post);
    }

    public function pobierzNazwyKlientow() {
	$klienci = $this->fetchAll();
	$wynik = array();

	foreach ($klienci as $klient) {
	    $wynik[] = $klient->nazwa;
	}

	return $wynik;
    }

    public function zapiszNotatkeKlienta($id, $notatka) {
	$this->update($id, ['notatka' => $notatka]);
    }

    public function pobierzLiczbeKlientow() {
	$wynik = $this->pobierzZWheremCount("1");
	return $wynik;
}

    public function pobierzLiczbeKlientowBiezacyMiesiac() {
	$wynik = $this->pobierzZWheremCount("MONTH(data_dodania) = MONTH(CURDATE())");
	return $wynik;
}

    public function pobierzOstatnioDodanego() {
	$wynik = $this->pobierzZWherem("1", "data_dodania DESC", 1);

	if (count($wynik) > 0) {
	    return $wynik[0];
	}
	else{
	    return new Klient();
	}
    }

}
