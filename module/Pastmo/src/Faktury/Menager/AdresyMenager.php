<?php

namespace Pastmo\Faktury\Menager;

class AdresyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	parent::__construct($sm, \Pastmo\Faktury\KonfiguracjaModulu::ADRESY_GATEWAY);
    }

}
