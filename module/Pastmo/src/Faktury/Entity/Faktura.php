<?php

namespace Pastmo\Faktury\Entity;

class Faktura extends \Wspolne\Model\WspolneModel {

    public $id;
    public $klient;
    public $wystawca;
    public $nr_faktury;
    public $rok;
    public $kwota_netto;
    public $zaplacono;
    public $vat;
    public $slownie;
    public $data_wystawienia;
    public $data_wykonania;
    public $termin_platnosci;
    public $miejsce_wystawienia;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, 'id');
	$this->nr_faktury = $this->pobierzLubNull($data, 'nr_faktury');
	$this->rok = $this->pobierzLubNull($data, 'rok');
	$this->kwota_netto = $this->pobierzLubNull($data, 'kwota_netto');
	$this->zaplacono = $this->pobierzLubNull($data, 'zaplacono');
	$this->vat = $this->pobierzLubNull($data, 'vat');
	$this->slownie = $this->pobierzLubNull($data, 'slownie');
	$this->data_wystawienia = $this->pobierzLubNull($data, 'data_wystawienia');
	$this->data_wykonania = $this->pobierzLubNull($data, 'data_wykonania');
	$this->termin_platnosci = $this->pobierzLubNull($data, 'termin_platnosci');
	$this->miejsce_wystawienia = $this->pobierzLubNull($data, 'miejsce_wystawienia');

	if ($this->czyTabeleDostepne()) {
	    $this->data = $data;

	    $this->wystawca = $this->pobierzTabeleObca('wystawca_id',
		    \Pastmo\Faktury\Menager\WystawcyFakturMenager::class, new WystawcaFaktur());
	    $this->klient = $this->pobierzTabeleObca('klient_id', \Pastmo\Faktury\Menager\KlienciMenager::class,
		    new Klient());
	}
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case "klient":
		return "klient_id";
	    case "wystawca":
		return "wystawca_id";
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public function getKwoteNetto() {
	return \Pastmo\Wspolne\Utils\StrUtil::drukujCene($this->kwota_netto);
    }

    public function getKwotaVat() {
	$wynik = $this->wyliczKwoteVat();
	return \Pastmo\Wspolne\Utils\StrUtil::drukujCene($wynik);
    }

    public function getKwotaBrutto() {
	$kwotaVat = $this->wyliczKwoteVat();
	$wynik = $this->kwota_netto + $kwotaVat;
	return \Pastmo\Wspolne\Utils\StrUtil::drukujCene($wynik);
    }

    private function wyliczKwoteVat() {
	$wynik = $this->kwota_netto * $this->vat / 100;
	return $wynik;
    }

    public function getNazwePliku() {
	return str_replace("/", "_", $this->nr_faktury) . " " . $this->klient->nazwa;
    }

}
