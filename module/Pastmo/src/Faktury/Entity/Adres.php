<?php

namespace Pastmo\Faktury\Entity;

class Adres extends \Wspolne\Model\WspolneModel {

    const domyslnyKraj = "Polska";

    public $id;
    public $kod;
    public $ulica;
    public $nr_domu;
    public $nr_lokalu;
    public $miasto;
    public $kraj;
    public $email;
    public $telefon;
    private $przedrostekZFormularza = "";

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, 'id');
	$this->ulica = $this->pobierzLubNull($data, 'ulica');
	$this->nr_domu = $this->pobierzLubNull($data, 'nr_domu');
	$this->nr_lokalu = $this->pobierzLubNull($data, 'nr_lokalu');
	$this->kod = $this->pobierzLubNull($data, 'kod');
	$this->miasto = $this->pobierzLubNull($data, 'miasto');
	$this->email = $this->pobierzLubNull($data, 'email');
	$this->telefon = $this->pobierzLubNull($data, 'telefon');
	$this->kraj = $this->pobierzLubNull($data, 'kraj', self::domyslnyKraj);
    }

    public function dowiazListyTabelObcych() {

    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	return $nazwaWKodzie;
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public function pobierzPrzedrostek() {
	return $this->przedrostekZFormularza;
    }

    public function ustawPrzedrostek($przedrostek) {
	$this->przedrostekZFormularza = $przedrostek;
    }

    public function __toString() {
	$wynik = "";
	$wynik.=$this->ulica . ' ';
	$wynik.=$this->kod . ' ';
	$wynik.=$this->miasto . ' ';
	return $wynik;
    }

}
