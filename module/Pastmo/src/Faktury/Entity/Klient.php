<?php

namespace Pastmo\Faktury\Entity;

use Logowanie\Model\Uzytkownik;

class Klient extends \Wspolne\Model\WspolneModel {

    public $id;
    public $nazwa;
    public $nazwa_skrocona;
    public $nip;
    public $uzytkownik;
    public $status;
    public $adres;
    public $adres_faktury;
    public $nazwa_na_fakturze;
    public $notatka;
    public $data_dodania;
    private $dodatkowe_pola = array();
    private $zlecenia = array();
    private $zasoby = array();

    public function __construct($sm = null) {
	parent::__construct($sm);
    }

    public function getZlecenia() {
	$this->dowiazListyTabelObcych();
	return $this->zlecenia;
    }

    public function getDodatkowePola() {
	$this->dowiazListyTabelObcych();
	return $this->dodatkowe_pola;
    }

    public function getNotatki() {
	$this->dowiazListyTabelObcych();
	return $this->notatki;
    }

    public function getZasoby() {
	$this->dowiazListyTabelObcych();
	return $this->zasoby;
    }

    public function exchangeArray($data) {
	parent::exchangeArray($data);

	$this->id = $this->pobierzLubNull($data, 'id');
	$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
	$this->nazwa_skrocona = $this->pobierzLubNull($data, 'nazwa_skrocona');
	$this->nip = $this->pobierzLubNull($data, 'nip');
	$this->status = $this->pobierzLubNull($data, 'status');
	$this->nazwa_na_fakturze = $this->pobierzLubNull($data, 'nazwa_na_fakturze');
	$this->notatka = $this->pobierzLubNull($data, 'notatka');
	$this->data_dodania = $this->pobierzLubNull($data, 'data_dodania');

	if ($this->czyTabeleDostepne()) {
	    $this->data = $data;
	    $this->pobierz_uzytkownika();
	    $this->pobierz_adres();
	    $this->pobierz_adres_faktury();
	}
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'uzytkownik':
		return 'uzytkownik_id';
	    case 'adres':
		return 'adres_id';
	    case 'adres_faktury':
		return 'adres_faktury_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public function pobierz_uzytkownika() {
	$this->uzytkownik = $this->pobierzTabeleObca('uzytkownik_id', \Logowanie\Module::UZYTKOWNIK_TABLE,
		new Uzytkownik());
    }

    public function pobierz_adres() {
	$this->adres = $this->pobierzTabeleObca('adres_id', \Pastmo\Faktury\Menager\AdresyMenager::class, new Adres());
    }

    public function pobierz_adres_faktury() {
	$this->adres_faktury = $this->pobierzTabeleObca('adres_faktury_id',
		\Pastmo\Faktury\Menager\AdresyMenager::class, new Adres());
    }

    public function dowiazListyTabelObcych() {
	if ($this->czyTabeleDostepne()) {
	    $zleceniaTable = $this->sm->get(\Zlecenia\Module::ZLECENIA_TABLE);
	    $dodatkowePolaTable = $this->sm->get(\Klienci\Module::DODATKOWE_POLA_TABLE);
	    $notatkiTable = $this->sm->get(\Klienci\Module::NOTATKI_TABLE);
	    $zasobyTable = $this->sm->get(\Zasoby\Menager\ZasobyUploadMenager::class);


	    $this->zlecenia = $zleceniaTable->pobierzZWherem("klient_id=$this->id");
	    $this->dodatkowe_pola = $dodatkowePolaTable->pobierzZWherem("klient_id=$this->id");
	    $this->notatki = $notatkiTable->pobierzZWherem("klient_id=$this->id");

	    $zasoby1 = $zasobyTable->pobierzZWherem($this->zrobZasobZleceniaWhere(\Zasoby\Model\KategoriaZasobu::ZWYKLY_PLIK));
	    $zasoby2 = $zasobyTable->pobierzZWherem($this->zrobZasobUzytkownikWhere(\Zasoby\Model\KategoriaZasobu::ZWYKLY_PLIK));
	    $wszytkieZasoby = array_merge($zasoby2, $zasoby1);

	    $this->zasoby = $wszytkieZasoby;
	}
    }

    private function zrobZasobZleceniaWhere() {
	return "id in (select zasob_id from zlecenia_zasoby zz join zlecenia zle on zle.id=zz.zlecenie_id where zle.klient_id=" . $this->id . ")";
    }

    private function zrobZasobUzytkownikWhere() {
	return "id in (select zasob_id from uzytkownicy_zasoby where uzytkownik_id=" . $this->uzytkownik->id . ")";
    }

    public static function create() {
	$klient = new Klient();
	$klient->adres = new Adres();
	$klient->uzytkownik = new Uzytkownik();
	return $klient;
    }

    public function __toString() {
	if($this->nazwa){
	    return $this->nazwa;
	}
	return "";
    }

}
