<?php

namespace Pastmo\Faktury\Entity;

class WystawcaFaktur extends \Wspolne\Model\WspolneModel {

    public $id;
    public $adres;
    public $nazwa;
    public $nip;
    public $imie_nazwisko;
    public $miejsce_wystawienia;
    public $typ;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
	$this->nip = $this->pobierzLubNull($data, 'nip');
	$this->imie_nazwisko = $this->pobierzLubNull($data, 'imie_nazwisko');
	$this->miejsce_wystawienia = $this->pobierzLubNull($data, 'miejsce_wystawienia');
	$this->typ = $this->pobierzLubNull($data, 'typ');

	if ($this->czyTabeleDostepne()) {
	    $this->data = $data;
	    $this->adres = $this->pobierzTabeleObca('adres_id', \Pastmo\Faktury\Menager\AdresyMenager::class,
		    new Adres());
	} else {
	    $this->adres = new Adres();
	}
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'adres':
		return 'adres_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
