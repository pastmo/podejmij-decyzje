--------Wymagane tabele:-----
CREATE TABLE `klienci` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`id`)
)
COLLATE='utf16_polish_ci'
ENGINE=InnoDB
;


-------------

DROP TABLE `faktury`;
DROP TABLE `klienci`;
DROP TABLE `wystawca_faktur`;


CREATE TABLE `wystawca_faktur` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`adres_id` INT(11) NULL DEFAULT NULL,
	`nazwa` VARCHAR(200) NOT NULL DEFAULT '0' COLLATE 'utf8_polish_ci',
	`nip` VARCHAR(10) NOT NULL DEFAULT '0' COLLATE 'utf8_polish_ci',
	`imie_nazwisko` VARCHAR(50) NOT NULL DEFAULT '0' COLLATE 'utf8_polish_ci',
	`miejsce_wystawienia` VARCHAR(30) NOT NULL DEFAULT '0' COLLATE 'utf8_polish_ci',
	PRIMARY KEY (`id`),
	INDEX `FK_wystawca_faktur_adresy` (`adres_id`),
	CONSTRAINT `FK_wystawca_faktur_adresy` FOREIGN KEY (`adres_id`) REFERENCES `adresy` (`id`)
)
COLLATE='utf8_polish_ci'
ENGINE=InnoDB
;

CREATE TABLE `klienci` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`uzytkownik_id` INT(11) NULL DEFAULT NULL,
	`adres_id` INT(11) NULL DEFAULT NULL,
	`adres_faktury_id` INT(11) NULL DEFAULT NULL,
	`nazwa` TEXT NULL COLLATE 'utf16_polish_ci',
	`nazwa_skrocona` TEXT NULL COLLATE 'utf16_polish_ci',
	`nazwa_na_fakturze` TEXT NULL COLLATE 'utf16_polish_ci',
	`nip` TINYTEXT NULL COLLATE 'utf16_polish_ci',
	`status` ENUM('Nowy','Stały','Zrealizowany') NULL DEFAULT 'Nowy' COLLATE 'utf16_polish_ci',
	`www` TEXT NULL COLLATE 'utf16_polish_ci',
	`notatka` TEXT NULL COLLATE 'utf16_polish_ci',
	`email` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf16_polish_ci',
	`osoba_kontaktowa` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf16_polish_ci',
	`telefon` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf16_polish_ci',
	PRIMARY KEY (`id`),
	INDEX `FK_klienci_uzytkownicy` (`uzytkownik_id`),
	INDEX `FK_klienci_adresy` (`adres_id`),
	INDEX `FK_klienci_adresy_2` (`adres_faktury_id`),
	CONSTRAINT `FK_klienci_adresy` FOREIGN KEY (`adres_id`) REFERENCES `adresy` (`id`),
	CONSTRAINT `FK_klienci_adresy_2` FOREIGN KEY (`adres_faktury_id`) REFERENCES `adresy` (`id`),
	CONSTRAINT `FK_klienci_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`)
)
COLLATE='utf8_polish_ci'
ENGINE=InnoDB
;



CREATE TABLE `faktury` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`klient_id` INT(11) NULL DEFAULT NULL,
	`wystawca_id` INT(11) NULL DEFAULT NULL,
	`nr_faktury` VARCHAR(30) NULL DEFAULT NULL COLLATE 'utf8_polish_ci',
	`rok` YEAR NULL DEFAULT NULL,
	`kwota_netto` DECIMAL(10,2) NULL DEFAULT NULL,
	`zaplacono` DECIMAL(10,2) NULL DEFAULT NULL,
	`vat` INT(11) NULL DEFAULT '0',
	`slownie` VARCHAR(300) NULL DEFAULT NULL COLLATE 'utf8_polish_ci',
	`data_wystawienia` DATE NULL DEFAULT NULL,
	`data_wykonania` DATE NULL DEFAULT NULL,
	`termin_platnosci` DATE NULL DEFAULT NULL,
	`miejsce_wystawienia` VARCHAR(30) NULL DEFAULT NULL COLLATE 'utf8_polish_ci',
	PRIMARY KEY (`id`),
	INDEX `FK_faktury_klienci` (`klient_id`),
	INDEX `FK_faktury_wystawca_faktur` (`wystawca_id`),
	CONSTRAINT `FK_faktury_klienci` FOREIGN KEY (`klient_id`) REFERENCES `klienci` (`id`),
	CONSTRAINT `FK_faktury_wystawca_faktur` FOREIGN KEY (`wystawca_id`) REFERENCES `wystawca_faktur` (`id`)
)
COLLATE='utf8_polish_ci'
ENGINE=InnoDB
;


