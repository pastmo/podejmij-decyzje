<?php

namespace Pastmo;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Pastmo\Email\Entity\KontoMailowe;
use Pastmo\Email\Entity\Email;
use Pastmo\Email\Entity\EmailZasob;

class Module implements ConfigProviderInterface {

    const KONTA_MAILOWE_GATEWAY = 'kontaMailoweGateway';
    const MAILE_GATEWAY = 'PastmoMaileGateway';
    const MAILE_FOLDERY_GATEWAY = 'MAILE_FOLDERY_GATEWAY';
    const EMAILE_ZASOBY_GATEWAY = 'EmaileZasobyGateway';

    private $aktywneModuly;

    public function __construct() {
	$this->aktywneModuly = \Application\AktywneModulyZPastmo::pobierzAktywneModuly();
    }

    public function onBootstrap(MvcEvent $e) {

	foreach ($this->aktywneModuly as $modul) {
	    $modul->onBootstrap($e);
	}

	$eventManager = $e->getApplication()->getEventManager();
	$eventManager->attach(\Zend\Mvc\MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'przekierowanieDoLogowania'));
	$eventManager->attach(\Zend\Mvc\MvcEvent::EVENT_RENDER_ERROR, array($this, 'przekierowanieDoLogowania'));
    }

    public function przekierowanieDoLogowania(MvcEvent $e) {

	$exception = $e->getParam('exception');
	if ($exception instanceof \Pastmo\Uzytkownicy\Exception\LogowanieException) {

	    $sm = $e->getApplication()->getServiceManager();
	    $url = $sm->get('ViewHelperManager')->get('url')
		    ->__invoke('logowanie', array(), array('force_canonical' => true, 'query' =>
			    ['redirect' => json_encode($exception->getUrlPrzekierowania())]));

	    $e->getResponse()->setStatusCode(302);
	    $e->getResponse()->getHeaders()
		    ->addHeaderLine('Location', $url);
	}
    }

    public function getConfig() {
	return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig() {
	$factories = array();
	foreach ($this->aktywneModuly as $modul) {
	    $part = $modul->getFactories();
	    $factories = array_merge($factories, $part);
	}

	return array(
		'factories' => $factories
	);
    }

    public static function dowiazMenagery(\Wspolne\Controller\Factory\InitObiekt $initObiekt,
	    \Interop\Container\ContainerInterface $container) {

	$initObiekt->ustawieniaSystemuMenager = $container->get(\Pastmo\Wspolne\Menager\UstawieniaSystemuMenager::class);
	$initObiekt->sesjaMenager = $container->get(\Wspolne\Menager\SesjaMenager::class);
	$initObiekt->pdfMenager = $container->get(\Pastmo\Pdf\Menager\PdfMenager::class);
	$initObiekt->logiMenager = $container->get(\Pastmo\Logi\Menager\LogiMenager::class);
	$initObiekt->krajeMenager = $container->get(Lokalizacje\Menager\KrajeMenager::class);
	$initObiekt->reCaptchaService = $container->get('ReCaptchaService');

	\Application\AktywneModulyZPastmo::dowiazMenagery($initObiekt, $container);

	    }

}
