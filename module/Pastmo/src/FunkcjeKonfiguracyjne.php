<?php

namespace Pastmo;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Mvc\MvcEvent;

trait FunkcjeKonfiguracyjne {

    protected $czy_https = true;

    public function ustawGateway($sm, $kasaEncji, $table) {
	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
	$resultSetPrototype = new ResultSet();
	$resultSetPrototype->setArrayObjectPrototype(new $kasaEncji($sm));
	return new TableGateway($table, $dbAdapter, null, $resultSetPrototype);
    }

    protected function podepnijHttps($sm, $eventManager) {
	$config = $sm->get('Config');
	if (isset($config['czy_https'])) {
	    $this->czy_https = $config['czy_https'];
	}

	if ($this->czy_https) {
	    $eventManager->attach('route', array($this, 'doHttpsRedirect'));
	}
    }

    public function doHttpsRedirect(MvcEvent $e) {
	$sm = $e->getApplication()->getServiceManager();
	$request = $e->getRequest();
	if (get_class($request) === \Zend\Console\Request::class) {
	    return;
	}
	$uri = $request->getUri();
	$scheme = $uri->getScheme();
	if ($scheme != 'https') {
	    $uri->setScheme('https');
	    $response = $e->getResponse();
	    $response->getHeaders()->addHeaderLine('Location', $uri);
	    $response->setStatusCode(302);
	    $response->sendHeaders();
	    return $response;
	}
    }

}
