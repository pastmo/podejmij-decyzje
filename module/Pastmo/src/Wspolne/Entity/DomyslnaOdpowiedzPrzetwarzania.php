<?php

namespace Pastmo\Wspolne\Entity;

class DomyslnaOdpowiedzPrzetwarzania {

    public $success = true;
    private $msgs = [];

    public static function create() {
	return new self();
    }

    public function toArray() {
	return ['success' => $this->success, 'msg' => implode(',', $this->msgs)];
    }

    public function setSuccess($success) {
	$this->success = $success;
	return $this;
    }

    public function addMsg($msg) {
	$this->msgs[] = $msg;
	return $this;
    }

    public function getMsgs() {
	return $this->msgs;
    }

    public function __toString() {
	return implode(",", $this->msgs);
    }

}
