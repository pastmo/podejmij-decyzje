<?php

namespace Pastmo\Wspolne\Entity;

class UstawienieSystemu extends WspolnyModel {

    public $id;
    public $uzytkownik;
    public $kod;
    public $wartosc;
 
    public function exchangeArray($data) {
	$this->data = $data;
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->kod = $this->pobierzLubNull($data, 'kod');
	$this->wartosc = $this->pobierzLubNull($data, 'wartosc');
	$this->uzytkownik = $this->pobierzTabeleObca('uzytkownik_id', \Logowanie\Menager\UzytkownicyMenager::class,
		\Logowanie\Model\Uzytkownik::create());
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'uzytkownik':
		return 'uzytkownik_id';
	    default:
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public function __toString() {
	return $this->wartosc ? $this->wartosc : "";
    }

    public static function create($kod, $wartosc, $uzytkownik) {
	$ustawienie = new UstawienieSystemu();
	$ustawienie->kod = $kod;
	$ustawienie->wartosc = $wartosc;
	$ustawienie->uzytkownik = $uzytkownik;
	return $ustawienie;
    }

}
