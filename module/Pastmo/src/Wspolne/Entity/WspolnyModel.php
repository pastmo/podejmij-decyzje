<?php

namespace Pastmo\Wspolne\Entity;

use Pastmo\Wspolne\Utils\EntityUtil;

abstract class WspolnyModel {

    use \Pastmo\Wspolne\Utils\TranslateTrait;

    const _ID = "_id";
    const ID = "id";
    const EXTRA_ID = 'exta_id';
    const EXTRA = 'exta_';
    const NO_DATABASE = 'NO_DATABASE';

    protected $sm;
    protected $data;
    public $id;
    public $wyswietlanie_js = '';
    protected $aktualizuj = false;
    protected $tabeleObce = [];
    public $updateSet;

    public function __construct($sm = null) {
	$this->sm = $sm;

	if ($this->sm) {
	    $this->translator = $this->sm->get(\Pastmo\Tlumaczenia\Menager\PastmoTranslator::class);
	}
    }

    public function zrobArray() {
	$class_vars = $this->pobierzSkladnikiEncji();
	$result = array();
	foreach ($class_vars as $name => $value) {

	    $bazowaKolumna = $this->konwertujNaKolumneDBWKlasachBazowych($name);
	    $kolumnaWTabeli = $this->konwertujNaKolumneDB($bazowaKolumna);


	    if ($this->$name === null || $this->czyIgnorowane($name)) {
		continue;
	    } else if (is_object($this->$name) && property_exists(get_class($this->$name), 'id') && $this->$name->id) {
		$result[$kolumnaWTabeli] = $this->pobierzKluczGlownyEncji($name);
	    } else if (!is_array($this->$name) && !is_object($this->$name)) {
		$result[$kolumnaWTabeli] = $this->$name;
	    }
	}
	return $result;
    }

    protected function pobierzKluczGlownyEncji($name) {
	return $this->$name->id;
    }

    public function pobierzSkladnikiEncji() {
	return get_class_vars($this->pobierzKlase());
    }

    public function czyIgnorowane($name) {
	$ignorowane = array_merge(array("aktualizuj", "wyswietlanie_js", "sm", "data", 'data_aktualizacji', 'tabeleObce',
		'updateSet'), $this->dodatkowoIgnorowane());
	return in_array($name, $ignorowane);
    }

    protected function dodatkowoIgnorowane() {
	return array();
    }

    public function pobierzLubNull($data, $key, $wartoscDomyslna = null) {

	if ($this->aktualizuj && isset($this->$key)) {
	    $wartoscDomyslna = $this->$key;
	} else if ($this->aktualizuj) {
	    $wartoscDomyslna = null;
	}

	return ($this->kluczIstnieje($data, $key)) ? $data[$this->pobierzPrzedrostek() . $key] : $wartoscDomyslna;
    }

    private function kluczIstnieje($data, $key) {
	if ($this->aktualizuj) {
	    return isset($data[$this->pobierzPrzedrostek() . $key]);
	}
	return !empty($data[$this->pobierzPrzedrostek() . $key]);
    }

    public function aktualizujArray($data) {
	$this->aktualizuj = true;

	$this->exchangeArray($data);
    }

    abstract function exchangeArray($data);

    abstract function pobierzKlase();

    protected function konwertujNaKolumneDBWKlasachBazowych($nazwaWKodzie) {
	return $nazwaWKodzie;
    }

    abstract function konwertujNaKolumneDB($nazwaWKodzie);

    protected function ustawExtraId($data) {
	if (!empty($data[self::EXTRA_ID])) {
	    $this->id = $data[self::EXTRA_ID];
	}
    }

    protected function ustawExtra($data, $klucz) {
	$extraKlucz = self::getExtraKey($klucz);
	if (!empty($data[$extraKlucz])) {
	    $this->$klucz = $data[$extraKlucz];
	}
    }

    public static function getExtraKey($klucz) {
	return self::EXTRA . $klucz;
    }

    public function pobierzPrzedrostek() {
	return "";
    }

    public function ustawPrzedrostek($przedrostek) {
	$this->przedrostekZFormularza = $przedrostek;
    }

    protected function czyTabeleDostepne() {
	return $this->sm !== null;
    }

    protected function getEncjeZTabeliObcej($klucz, $table, $domyslnie, $kluczWEncji = false,$metoda="getRekord") {
	$docelowePole = EntityUtil::pobierzKluczEncji($klucz, $kluczWEncji);

	if (empty($this->tabeleObce[$docelowePole])) {
	    $this->tabeleObce[$docelowePole] = $this->pobierzTabeleObcaWspolne($klucz, $table, $domyslnie,
		    $docelowePole,$metoda);
	}
	return $this->tabeleObce[$docelowePole];
    }

    /**
     *
     * Zalecana metoda to getEncjeZTabeliObcej
     */
    protected function pobierzTabeleObca($klucz, $table, $domyslnie, $kluczWEncji = false) {

	if ($this->aktualizuj && !$this->czyDataZawieraKlucz($klucz)) {
	    $kluczWEncji = EntityUtil::pobierzKluczEncji($klucz, $kluczWEncji);
	    return $this->$kluczWEncji;
	}

	return $this->pobierzTabeleObcaWspolne($klucz, $table, $domyslnie, $kluczWEncji);
    }

    protected function pobierzTabeleObcaWspolne($klucz, $table, $domyslnie, $kluczWEncji, $metoda = "getRekord") {
	if (!$this->czyTabeleDostepne()) {
	    return $domyslnie;
	}

	$id = $this->czyDataZawieraKlucz($klucz) ? $this->data[$klucz] : null;
	if ($id) {
	    $obiektTable = $this->sm->get($table);
	    return $obiektTable->$metoda($id);
	} else {
	    return $domyslnie;
	}
    }

    /**
     *
     * TODO: Metoda do usunięcia- w nowym kodzie należy używać getEncjeZTabeliObcej
     */
    protected function pobierzLeniwieTabeleObca($poleZId, $table, $domyslnie, $kluczWEncji = false) {

	if (!$this->czyTabeleDostepne()) {
	    return $domyslnie;
	}

	$kluczWEncji = EntityUtil::pobierzKluczEncji($poleZId, $kluczWEncji);

	if ($this->$kluczWEncji !== null) {
	    return $this->$kluczWEncji;
	}

	$id = $this->$poleZId;
	if ($id) {
	    $obiektTable = $this->sm->get($table);
	    return $obiektTable->getRekord($id);
	} else {
	    return $domyslnie;
	}
    }

    protected function czyDataZawieraKlucz($klucz) {
	$wynik = isset($this->data[$klucz]);
	return $wynik;
    }

    protected function pobierzListeZTabeliObcej($menagerClass, $metodaMenagera, $parametr1, $parametr2,
	    $poleWyniku, $wymusZaladowanie = false) {
	if (!$wymusZaladowanie && $this->$poleWyniku !== null) {
	    return;
	} else if (!$this->czyTabeleDostepne()) {
	    $this->$poleWyniku = array();
	} else {
	    $menager = $this->sm->get($menagerClass);
	    $wynik = $menager->$metodaMenagera($parametr1, $parametr2);
	    $this->$poleWyniku = $wynik;
	}
    }

    protected function pobierzTabeleObcaZKluczemObcym($klucz, $table, $domyslnie, $kluczObcy) {

	$wartosc = isset($this->data[$klucz]) ? $this->data[$klucz] : null;

	if ($wartosc && $this->czyTabeleDostepne()) {
	    $obiektTable = $this->sm->get($table);
	    $wynikArr = $obiektTable->pobierzZWherem("$kluczObcy='$wartosc'", "$kluczObcy ASC");
	    if (count($wynikArr) > 0) {
		return $wynikArr[0];
	    }
	}
	return $domyslnie;
    }

    protected function getZalogowanego() {
	if ($this->czyTabeleDostepne()) {
	    $uzytkownikTable = $this->sm->get(\Logowanie\Menager\UzytkownicyMenager::getClass());
	    return $uzytkownikTable->getZalogowanegoUsera();
	}
	return false;
    }

    protected function pobierzSamaDate($timestamp) {
	return date('Y-m-d', strtotime($timestamp));
    }

    public function setSm($sm) {
	$this->sm = $sm;
    }

    public function __set($nazwa, $value) {
	$this->tabeleObce[$nazwa] = $value;
	$nazwaDb = $this->konwertujNaKolumneDB($nazwa);

	if ($nazwaDb !== $nazwa) {
	    $this->$nazwaDb = $value;
	}
    }

    public function __get($name) {
	$wynik = $this->getEncjeTabeliObcej($name);

	if (empty($wynik)) {
	    throw new \Pastmo\Wspolne\Exception\PastmoException(get_class($this) . " Nieznaleziony parametr:" . $name);
	}

	return $wynik;
    }

    public function ustawPole($klucz, $wartosc) {
	$this->data[$klucz] = $wartosc;
	$this->$klucz = $wartosc;
    }

    public function getEncjeTabeliObcej($name) {

    }

    public function jsonSerialize() {
	$zwyklePola = $this->zrobArray();
	$wynik = array_merge($zwyklePola, $this->tabeleObce);
	return $wynik;
    }

}
