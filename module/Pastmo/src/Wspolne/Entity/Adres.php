<?php

namespace Pastmo\Wspolne\Entity;

class Adres extends \Wspolne\Model\WspolneModel {

    public $id;
    public $ulica;
    public $kod;
    public $miasto;
    public $kraj;

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->ulica = $this->pobierzLubNull($data, 'ulica');
	$this->kod = $this->pobierzLubNull($data, 'kod');
	$this->miasto = $this->pobierzLubNull($data, 'miasto');
	$this->kraj = $this->pobierzLubNull($data, 'kraj');
    }

    public function dowiazListyTabelObcych() {

    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	return $nazwaWKodzie;
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public function wyswietlUliceNr() {
	$ulica = $this->ulica . ' ' . $this->nr_domu;
	if (!empty($this->nr_domu) && !empty($this->nr_lokalu)) {
	    $ulica .= "/" . $this->nr_lokalu;
	}
	return $ulica;
    }

}
