<?php

namespace Pastmo\Wspolne\Helper;

class WyswietlGoogleAnalizy extends TranslateHelper {

    use \Pastmo\Wspolne\Utils\TranslateTrait;

    protected $sm;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	parent::__construct($sm);
    }

    public function __invoke() {
	$config = $this->get('config');
	if (isset($config['analizy'])) {
	    echo $this->view->partial('wspolne/google_analizy.phtml');
	}
    }

}
