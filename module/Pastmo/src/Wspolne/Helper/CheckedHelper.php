<?php

namespace Pastmo\Wspolne\Helper;

use Zend\View\Helper\AbstractHelper;

class CheckedHelper extends AbstractHelper {

    public function __construct() {

    }

    public function __invoke($expected, $value) {
	if (is_array($expected)) {
	    $checked = in_array($value, $expected, TRUE);
	} else {
	    $checked = $expected == $value;
	}

	if ($checked) {
	    echo 'checked="true"';
	}
    }

}
