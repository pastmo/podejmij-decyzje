<?php

namespace Pastmo\Wspolne\Helper;

class TlumaczeniaHelper extends TranslateHelper {

    use \Pastmo\Wspolne\Utils\TranslateTrait;

    protected $sm;

    public function __invoke($message, $domkniecie = false, $nr = null) {
	return $this->_translate($message, $domkniecie, $nr);
    }

}
