<?php

namespace Pastmo\Wspolne\Helper;

use Zend\View\Helper\AbstractHelper;

class WyswietlWynikowaTrescWysiwyga extends AbstractHelper {

    private $zasobyMenager;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	$this->zasobyMenager = $sm->get(\Zasoby\Menager\ZasobyUploadMenager::class);
    }

    public function __invoke($tresc, $view) {
	$wynik = $this->dolaczZasoby($tresc);
	return $wynik;
    }

    private function dolaczZasoby($tresc) {
	$obrazki = \Pastmo\Wspolne\Utils\EdytorUtil::pobierzKodyZasobow($tresc);

	foreach ($obrazki as $obrazek) {
	    $id = \Wspolne\Utils\StrUtil::substr('[', $obrazek, ']');
	    $path = $this->zasobyMenager->wyswietlUrlObrazkaZBasePathPoId($id);
	    $src= \Pastmo\Wspolne\Utils\EdytorUtil::opakujImg($path);
	    $tresc = str_replace($obrazek, $src, $tresc);
	}

	return $tresc;
    }

}
