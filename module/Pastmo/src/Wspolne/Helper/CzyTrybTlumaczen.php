<?php

namespace Pastmo\Wspolne\Helper;

class CzyTrybTlumaczen extends TranslateHelper {

    use \Pastmo\Wspolne\Utils\TranslateTrait;

    protected $sm;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	$this->sesjaMenager = $sm->get(\Wspolne\Menager\SesjaMenager::class);
    }

    public function __invoke() {
	$wartosc = $this->sesjaMenager->czyTrybTlumaczen();
	return $wartosc == "1";
    }

}
