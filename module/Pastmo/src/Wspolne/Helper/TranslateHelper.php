<?php

namespace Pastmo\Wspolne\Helper;

use Zend\View\Helper\AbstractHelper;

class TranslateHelper extends AbstractHelper {

    use \Pastmo\Wspolne\Utils\TranslateTrait;

    protected $sm;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	$this->sm = $sm;
	$this->translator = $this->sm->get(\Pastmo\Tlumaczenia\Menager\PastmoTranslator::class);
	$this->constantTranslator = $sm->get(\Lokalizacje\Menager\ConstantTranslatorMenager::class);
    }

    public function get($klasa) {
	return $this->sm->get($klasa);
    }

}
