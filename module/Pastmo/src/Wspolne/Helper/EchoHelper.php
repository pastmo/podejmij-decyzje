<?php

namespace Pastmo\Wspolne\Helper;

class EchoHelper extends \Zend\View\Helper\EscapeHtml {

    public function __invoke($value, $recurse = self::RECURSE_NONE) {
	echo parent::__invoke($value, $recurse);
    }

}
