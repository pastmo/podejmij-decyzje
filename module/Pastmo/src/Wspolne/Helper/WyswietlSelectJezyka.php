<?php

namespace Pastmo\Wspolne\Helper;

use Zend\View\Helper\AbstractHelper;
use Pastmo\Uzytkownicy\Menager\JezykiMenager;

class WyswietlSelectJezyka extends AbstractHelper {

    private $jezykiMenager;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	$this->jezykiMenager = $sm->get(\Pastmo\Uzytkownicy\Menager\JezykiMenager::class);
    }

    public function __invoke($jezyk_kod, $view) {
	?>
	    <form id="wyswietlSelectJezykaForm" method="GET">
		<!--<label><?php // echo  $this->_translate("Wybierz jezyk", false, 8); ?></label>-->
		<select onchange="this.form.submit()" class="form-control" name="jezyk">
		    <?php
		    $tablicaJezykow = $this->jezykiMenager->pobierzDostepneJezyki();
		    foreach ($tablicaJezykow as $jezyk):
			?>
			<option value="<?php echo $jezyk->kod; ?>" <?php $view->selected($jezyk->kod, $jezyk_kod)?> ><?php echo $jezyk->nazwa; ?></option>
		    <?php endforeach; ?>
		</select>
		<!--<input type="submit"></input>-->
	    </form>
	<?php
    }

}
