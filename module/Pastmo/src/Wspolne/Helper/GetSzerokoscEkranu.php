<?php

namespace Pastmo\Wspolne\Helper;

use Zend\View\Helper\AbstractHelper;

class GetSzerokoscEkranu extends AbstractHelper {

    private $sesjaMenager;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	$this->sesjaMenager = $sm->get(\Wspolne\Menager\SesjaMenager::class);
    }

    public function __invoke() {
	$wartosc = $this->sesjaMenager->getSzerokoscEkranu();
	return $wartosc;
    }

}
