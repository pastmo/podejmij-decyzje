<?php

namespace Pastmo\Wspolne\Builder;

class PobieranieStronicowaneBuilder {

    public $table;
    public $dodatkowyWhere = '1';
    public $pola = null;
    public $domyslneSortowanie = null;
    public $wynikowNaStronie = \Wspolne\Menager\BazowyMenagerBazodanowy::DOMYSLNY_LIMIT;
    public $strona = PaginatorBuilder::DOMYSLNA_STRONA;
    public $zwrocPaginator = true;
    public $kolumnySortowania = [];
    public $groupBy = false;

    public static function create() {
	$wynik = new PobieranieStronicowaneBuilder();
	return $wynik;
    }

    public function setTable($table) {
	$this->table = $table;
	return $this;
    }

    public function setDodatkowyWhere($dodatkowyWhere) {
	$this->dodatkowyWhere = $dodatkowyWhere;
	return $this;
    }

    public function setPola(array $pola) {
	$this->pola = $pola;
	return $this;
    }

    public function setDomyslneSortowanie($order) {
	$this->domyslneSortowanie = $order;
	return $this;
    }

    public function setWynikowNaStronie($wynikowNaStronie) {
	$this->wynikowNaStronie = $wynikowNaStronie;
	return $this;
    }

    public function setStrona($strona) {
	$this->strona = $strona;
	return $this;
    }

    public function setZwrocPaginator(bool $zwrocPaginator) {
	$this->zwrocPaginator = $zwrocPaginator;
	return $this;
    }

    public function setKolumnySortowania(array $kolumnySortowania) {
	$this->kolumnySortowania = $kolumnySortowania;
	return $this;
    }

    public function setGroupBy($groupBy) {
	$this->groupBy = $groupBy;
	return $this;
    }



}
