<?php

namespace Pastmo\Wspolne\Builder;

class PaginatorBuilder {

    const DOMYSLNA_STRONA = 0;

    public $strona = self::DOMYSLNA_STRONA;
    public $wynikowNaStronie = \Wspolne\Menager\BazowyMenagerBazodanowy::DOMYSLNY_LIMIT;
    public $where = "1";
    public $order = \Wspolne\Menager\BazowyMenagerBazodanowy::DOMYSLNY_ORDER;
    public $groupBy = false;

    public static function create() {
	$wynik = new PaginatorBuilder();
	return $wynik;
    }

    public function setStrona($strona) {
	$this->strona = $strona;
	return $this;
    }

    public function setWynikowNaStronie($wynikowNaStronie) {
	$this->wynikowNaStronie = $wynikowNaStronie;
	return $this;
    }

    public function setWhere($where) {
	$this->where = $where;
	return $this;
    }

    public function setOrder($order) {
	$this->order = $order;
	return $this;
    }

    public function setGroupBy($groupBy) {
	$this->groupBy = $groupBy;
	return $this;
    }

}
