<?php

namespace Pastmo\Wspolne\Utils;

class ArrayUtil {

    public $funkcjaPobierajacaWartosci;

    public static function ustawPustaTablicePodKluczemJesliNieIstnieje(&$tablica, $klucz) {
	if (!isset($tablica[$klucz])) {
	    $tablica[$klucz] = array();
	}
    }

    public static function ustawDomyslnyObiektPodKluczemJesliNieIstnieje(&$tablica, $klucz,
	    $klasaDomyslnegoObiektu, $parametrCreate = null) {
	if (!isset($tablica[$klucz])) {
	    $tablica[$klucz] = $klasaDomyslnegoObiektu::create($parametrCreate);
	}
    }

    public static function ustawWartoscPodKluczemJesliNieIstnieje(&$tablica, $klucz,$wartosc) {
	if (!isset($tablica[$klucz])) {
	    $tablica[$klucz] = $wartosc;
	}
    }

    public static function czyIstniejeNiepustyKlucz($tablica, $klucz) {
	return isset($tablica[$klucz]) && $tablica[$klucz] !== '';
    }

    public static function pobierzElementKontenera($tablicaLubObiekt, $klucz, $wartoscDomyslna = null) {
	if (is_array($tablicaLubObiekt) && isset($tablicaLubObiekt[$klucz]) && $tablicaLubObiekt[$klucz] !== '') {
	    return $tablicaLubObiekt[$klucz];
	}
	if (is_object($tablicaLubObiekt) && isset($tablicaLubObiekt->$klucz) && $tablicaLubObiekt->$klucz !== '') {
	    return $tablicaLubObiekt->$klucz;
	}
	return $wartoscDomyslna;
    }

    public static function wydobadzIdDoZapytaniaIn($input) {

	return self::wydobadzWartosciDoZapytaniaIn($input, 'id');
    }

    public static function wydobadzWartosciDoZapytaniaIn($input, $pole) {

	$tablica = self::zrobTablice($input);
	$tablicaId = self::przerobNaTabliceWartosci($tablica, $pole);
	$wynik = StrUtil::fromArrayToSql($tablicaId);
	return $wynik;
    }

    public static function zrobTablice($input) {
	if (is_array($input)) {
	    return $input;
	}

	if ($input !== null) {
	    return array($input);
	}

	return array();
    }

    public static function przerobNaTabliceId($idLubEncjeLubResultSet) {
	return self:: przerobNaTabliceWartosci($idLubEncjeLubResultSet, 'id');
    }

    public static function przerobNaTabliceWartosci($idLubEncjeLubResultSet, $pole) {
	$wynik = array();

	foreach ($idLubEncjeLubResultSet as $idLubEncja) {
	    $wartosc = EntityUtil::wydobadzPole($idLubEncja, $pole);
	    if ($wartosc !== null) {
		$wynik[] = $wartosc;
	    }
	}

	return $wynik;
    }

    public static function pobierzPierwszyElement($tablica, $domyslnie = false) {
	if (!is_array($tablica)) {
	    return $tablica;
	}

	if (count($tablica) > 0) {
	    return $tablica[0];
	}
	return $domyslnie;
    }

    public static function konwertujNaArray($resultSet, $limit = false) {
	$wynik = array();
	$i = 0;

	foreach ($resultSet as $result) {
	    $wynik[] = $result;

	    $i++;
	    if ($limit && $i >= $limit) {
		break;
	    }
	}

	return $wynik;
    }

    public static function getUzytkownicyBezZalogowanego($lista, $zalogowany) {
	$wynik = array();

	foreach ($lista as $encja) {
	    if ($encja->uzytkownik->id === $zalogowany->id) {
		continue;
	    }
	    $wynik[] = $encja->uzytkownik;
	}
	return $wynik;
    }

    public static function getEncjeZalogowanego($lista, $zalogowany, $domyslnie = null) {

	foreach ($lista as $encja) {
	    if ($encja->uzytkownik->id === $zalogowany->id) {
		return $encja;
	    }
	}
	return $domyslnie;
    }

    public static function inEntityArray($szukany, array $tablica) {
	return in_array(EntityUtil::wydobadzId($szukany), self::przerobNaTabliceId($tablica));
    }

    public static function multiMerge() {
	$arguments = func_get_args();

	$result = array();
	foreach ($arguments as $argument) {
	    $result = array_merge($result, $argument);
	}
	return $result;
    }

    public static function getArrayZOrderString($order) {
	$part = explode(" ", $order);
	if (count($part) > 1 && $part[0] && $part[1]) {
	    $staryOrder = array($part[0] => $part[1]);
	    return $staryOrder;
	}
	return [];
    }

    public static function mergeAndReturnUnique($array1, $array2) {
	return array_unique(array_merge($array1, $array2));
    }

    public static function tablicaStringowZawiera(array $kontener, $szukane) {

	foreach ($kontener as $element) {
	    $wynik = StrUtil::stringZawiera($element, $szukane);
	    if ($wynik) {
		return true;
	    }
	}
	return false;
    }

    public static function stringZawiera($kontener, array $szukaneFiltry) {

	foreach ($szukaneFiltry as $szukane) {
	    $wynik = StrUtil::stringZawiera($kontener, $szukane);
	    if ($wynik) {
		return true;
	    }
	}
	return false;
    }

    public static function sortujPoKluczu(&$tablica, $kolumnaKlucza, $order = SORT_ASC) {
	$tablicaKolejnosci = array();
	foreach ($tablica as $key => $row) {
	    $tablicaKolejnosci[$key] = $row[$kolumnaKlucza];
	}
	array_multisort($tablicaKolejnosci, $order, $tablica);
    }

    public static function sortujMalejaco($tablica, $funkcjaPobierajacaWartosci) {

	$comparator = new ArrayUtil();
	$comparator->funkcjaPobierajacaWartosci = $funkcjaPobierajacaWartosci;
	usort($tablica, array($comparator, 'compare'));

	return $tablica;
    }

    public static function sortujNapisy($tablica, $funkcjaPobierajacaWartosci) {

	$comparator = new ArrayUtil();
	$comparator->funkcjaPobierajacaWartosci = $funkcjaPobierajacaWartosci;
	usort($tablica, array($comparator, 'compareStr'));

	return $tablica;
    }

    private function compare($a, $b) {
	$nazwaFunkcji = $this->funkcjaPobierajacaWartosci;

	$wartoscA = $a->$nazwaFunkcji();
	$wartoscB = $b->$nazwaFunkcji();

	if ($wartoscA == $wartoscB) {
	    return 0;
	}
	return ($wartoscA > $wartoscB) ? -1 : 1;
    }

    private function compareStr($a, $b) {
	$nazwaFunkcji = $this->funkcjaPobierajacaWartosci;

	$wartoscA = $a->$nazwaFunkcji();
	$wartoscB = $b->$nazwaFunkcji();

	return strcmp($wartoscA, $wartoscB);
    }

    public static function wyciagnijZTablicyObiektowWybranePola(array $obiekty, array $tablicaNazwPol) {
	$wynik = [];
	if (count($tablicaNazwPol) > 0 && !empty($obiekty)) {
	    foreach ($obiekty as $obiekt) {
		$wynik[] = self::wyciagnijZObiektuWybranePola($obiekt, $tablicaNazwPol);
	    }
	}
	return $wynik;
    }

    public static function wyciagnijZObiektuWybranePola($obiekt, array $tablicaNazwPol) {
	$wynik = [];
	foreach ($tablicaNazwPol as $nazwaPola) {
	    $wynik[$nazwaPola] = EntityUtil::wydobadzPole($obiekt, $nazwaPola);
	}
	return $wynik;
    }

}
