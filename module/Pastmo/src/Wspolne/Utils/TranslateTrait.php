<?php

namespace Pastmo\Wspolne\Utils;

trait TranslateTrait {

    public $translator;
    public $constantTranslator;

    public function translate($tekst, $domkniecie = false, $nr = null) {
	if (!$this->translator) {
	    return $tekst;
	}
	return $this->translator->translate($tekst, $domkniecie, $nr);
    }

    public function _translate($tekst, $domkniecie = false, $nr = null) {
	return $this->translate($tekst, $domkniecie, $nr);
    }

    public function translateConst($tekst, $domkniecie = false) {
	$this->sprwadzIInicjujConstantTraslator();
	if (!$this->constantTranslatorDostepny()) {
	    return $tekst;
	}
	return $this->constantTranslator->translateConst($tekst, $domkniecie);
    }

    public function initConstantTranslation() {
	throw new \Wspolne\Exception\GgerpException("Metoda powinna być przesłonięta w klasach używających traita");
    }

    private function sprwadzIInicjujConstantTraslator() {
	if (!$this->constantTranslatorDostepny()) {
	    $this->initConstantTranslation();
	}
    }

    private function constantTranslatorDostepny() {
	return $this->constantTranslator !== null;
    }

}
