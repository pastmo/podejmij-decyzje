<?php

namespace Pastmo\Wspolne\Utils;

class DateCommon {

    const FORMAT_TIMESTAMP = "Y-m-d H:i:s";
    const FORMAT_TIMESTAMP_BEZ_SEKUND = "Y-m-d H:i:00";
    const FORMAT_DATA = "Y-m-d";
    const FORMAT_DATA_PODKRESLNIKI = "Y_m_d";
    const FORMAT_DZIEN_MIESIAC_KROPKI = "d.m";
    const FORMAT_DZIEN_MIESIAC_ROK_KROPKI = "d.m.Y";
    const FORMAT_CZAS = "H:i:s";
    const GODZINY_PRACY_OD = 9;
    const GODZINY_PRACY_DO = 17;
    const NIEDZIELA = 0;
    const PONIEDZIALEK = 1;
    const WTOREK = 2;
    const SRODA = 3;
    const CZWARTEK = 4;
    const PIATEK = 5;
    const SOBOTA = 6;

    public static function dateAdapter($stringLubDateTime) {
	if ($stringLubDateTime instanceof \DateTime) {
	    $kopia = clone $stringLubDateTime;
	    return $kopia;
	} else {
	    return new \DateTime($stringLubDateTime);
	}
    }

    public static function toString($stringLubDateTime) {
	if ($stringLubDateTime instanceof \DateTime) {
	    return $stringLubDateTime->format(self::FORMAT_TIMESTAMP);
	} else {
	    return $stringLubDateTime;
	}
    }

}
