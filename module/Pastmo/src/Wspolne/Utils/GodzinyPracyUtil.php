<?php

namespace Pastmo\Wspolne\Utils;

class GodzinyPracyUtil extends DateCommon {

    public static function podzielNaDniRobocze($start, $stop) {
	$dateStart = self::dateAdapter($start);
	$dateStop = self::dateAdapter($stop);

	$wynik = array();


	$koniec = false;

	$nowaDataPoczatkowa = self::normalizujDateWprzod($dateStart);

	while (!$koniec) {
	    $dataPoczatkowaWDniu = clone $nowaDataPoczatkowa;
	    $dataKoncaWDniu = self::obliczDateKoncaWDNiu($dataPoczatkowaWDniu);

	    if ($dataKoncaWDniu >= $dateStop) {
		$dataKoncaWDniu = GodzinyPracyUtil::normalizujDate($dateStop);
		$koniec = true;
	    }

	    $nowaDataPoczatkowa = self::zwiekszDateODzienRoboczy($nowaDataPoczatkowa);

	    if ($dataKoncaWDniu == $dataPoczatkowaWDniu) {
		continue;
	    }

	    if ($dataKoncaWDniu < $dataPoczatkowaWDniu) {
		$dataPoczatkowaWDniu = $dataKoncaWDniu;
	    }

	    $wynik[] = array(
		    'od' => $dataPoczatkowaWDniu->format(self::FORMAT_TIMESTAMP),
		    'do' => $dataKoncaWDniu->format(self::FORMAT_TIMESTAMP)
	    );

	    if ($dateStop < $nowaDataPoczatkowa) {
		break;
	    }
	}


	return $wynik;
    }

    public static function obliczCzasTrwaniaWMinutachRoboczych($dataStartu, $dataKonca) {

	$dateStart = self::dateAdapter($dataStartu);
	$dateStop = self::dateAdapter($dataKonca);
	$dodatnieWartosci = true;

	if ($dateStart <= $dateStop) {
	    $daty = self::podzielNaDniRobocze($dataStartu, $dataKonca);
	} else {
	    $daty = self::podzielNaDniRobocze($dataKonca, $dataStartu);
	    $dodatnieWartosci = false;
	}

	$wynik = 0;

	foreach ($daty as $data) {
	    $wynik+= DateUtil::obliczSekundyInterwalu($data['od'], $data['do']);
	}

	if (!$dodatnieWartosci) {
	    $wynik = -$wynik;
	}

	return DateUtil::sekundyNaMinuty($wynik);
    }

    public static function obliczDateKoncaWDniachRoboczych($dataStartu, $czasPracyWMinutach) {

	$przesuwanieWPrzod = $czasPracyWMinutach > 0;
	$aktualnyCzasPracy = 0;

	$startWDniu = self::normalizujDate(self::dateAdapter($dataStartu));
	$wynik = $startWDniu;

	while (abs($czasPracyWMinutach) > abs($aktualnyCzasPracy)) {
	    $skrajnaDataWDniu = $przesuwanieWPrzod ? self::obliczDateKoncaWDniu($startWDniu) : self::obliczDatePoczatkuPracyWDniu($startWDniu);

	    $ileZostaloDoWykorzystania = $czasPracyWMinutach - $aktualnyCzasPracy;

	    $maxInterwalWDniu = DateUtil::obliczMinutyInterwaluD($startWDniu, $skrajnaDataWDniu);


	    $dataZIleZostalo = self::dateAdapter(DateUtil::dodajOdejmijInterwal($startWDniu,
				    $ileZostaloDoWykorzystania));

	    if (abs($ileZostaloDoWykorzystania) > abs($maxInterwalWDniu)) {
		$aktualnyCzasPracy+=$maxInterwalWDniu;
		$wynik = $skrajnaDataWDniu;
	    } else {
		$aktualnyCzasPracy+=$ileZostaloDoWykorzystania;
		$wynik = $dataZIleZostalo;
	    }

	    $startWDniu = $przesuwanieWPrzod ?
		    self::zwiekszDateODzienRoboczy($startWDniu) : self::zmniejszDateODzienRoboczy($startWDniu);
	}

	return $wynik->format(self::FORMAT_TIMESTAMP);
    }

    public static function normalizujDate(\DateTime $data) {
	$dataPoczatku = self::obliczDatePoczatkuPracyWDniu($data);
	$dataKonca = self::obliczDateKoncaWDniu($data);

	if (self::sprawdzCzyWeekend($data)) {
	    return self::zmniejszDateODzienRoboczy($dataKonca);
	}

	if ($data < $dataPoczatku) {
	    return $dataPoczatku;
	}
	if ($data > $dataKonca) {
	    return $dataKonca;
	} else {
	    return $data;
	}
    }

    public static function normalizujDateWprzod(\DateTime $data, $obcinajDateDo = false) {
	$dataPoczatku = self::obliczDatePoczatkuPracyWDniu($data);
	$dataKonca = self::obliczDateKoncaWDniu($data);
	$dataPoczatkuNastepnegoDnia = self::obliczDatePoczatkuPracyWDniu(
			self::zwiekszDateODzienRoboczy($data));

	if (self::sprawdzCzyWeekend($data)) {
	    return $dataPoczatkuNastepnegoDnia;
	}

	if ($data < $dataPoczatku) {
	    return $dataPoczatku;
	}

	$warunekKoncaDnia = $obcinajDateDo ? $data >= $dataKonca : $data > $dataKonca;

	if ($warunekKoncaDnia) {
	    return $dataPoczatkuNastepnegoDnia;
	} else {
	    return $data;
	}
    }

    public static function pobierzNastepnaDate($data) {
	return $data; //TODOPK: zaimplementować zwiększanie daty o 1s
    }

    public static function zwiekszDateODzienRoboczy($data) {

	return self::zmienDateODzienRoboczy($data, 'add', true);
    }

    public static function zmniejszDateODzienRoboczy($data) {
	return self::zmienDateODzienRoboczy($data, 'sub', false);
    }

    private static function zmienDateODzienRoboczy($dataInput, $metoda, $przesuniecieWPrzod) {
	$data = DateUtil::dateAdapter($dataInput);

	$data->$metoda(new \DateInterval('P1D'));

	$wynik = $przesuniecieWPrzod ?
		self::obliczDatePoczatkuPracyWDniu($data) : self::obliczDateKoncaWDniu($data);

	if (self::sprawdzCzyWeekend($wynik)) {
	    return self::zmienDateODzienRoboczy($wynik, $metoda, $przesuniecieWPrzod);
	} else {
	    return $wynik;
	}
    }

    private static function sprawdzCzyWeekend($data) {
	$dzien = DateUtil::pobierzDzienTygodnia($data);
	$wynik = (int) $dzien === self::SOBOTA || (int) $dzien === self::NIEDZIELA;
	return $wynik;
    }

    public static function obliczDatePoczatkuPracyWDniu(\DateTime $data) {
	$wynik = clone $data;
	$wynik->setTime(self::GODZINY_PRACY_OD, 00);
	return $wynik;
    }

    public static function obliczDateKoncaWDniu(\DateTime $data) {
	$wynik = clone $data;
	$wynik->setTime(self::GODZINY_PRACY_DO, 00);
	return $wynik;
    }

}
