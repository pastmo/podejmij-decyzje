<?php

namespace Pastmo\Wspolne\Utils;

class AdresUtil {

    const POLSKA = 'Polska';
    const CZECHY = 'Czechy';
    const ESTONIA = 'Estonia';
    const HISZPANIA = 'Hiszpania';
    const NIEMCY = 'Niemcy';
    const NORWEGIA = 'Norwegia';
    const USA = 'Stany Zjednoczone Ameryki';
    const WIELKABRYTANIA = 'Wielka Brytania';
    const WLOCHY = 'Włochy';
    const CZECHY_KOD = 'cz';
    const NIEMCY_KOD = 'de';
    const ESTONIA_KOD = 'ee';
    const HISZPANIA_KOD = 'es';
    const WLOCHY_KOD = 'it';
    const NORWEGIA_KOD = 'no';
    const POLSKA_KOD = 'pl';
    const USA_KOD = 'us';

    public static function zwrocTablicePanstw() {
	return [
		self::POLSKA, self::CZECHY, self::ESTONIA, self::HISZPANIA,
		self::NIEMCY, self::NORWEGIA, self::USA, self::WLOCHY
	];
    }

    public static function pobierzKodPanstwaDhl($nazwa) {
	switch ($nazwa) {
	    case "pl":
		return "PL";
	    case "cz":
		return "CZ";
	    case "ee":
		return "EE";
	    case "es":
		return "ES";
	    case "de":
		return "DE";
	    case "no":
		return "NO";
	    case "us":
		return "US";
	    case "it":
		return "IT";
	}
    }

}
