<?php

namespace Pastmo\Wspolne\Utils;

class DateUtil extends DateCommon {

    public static function obliczInterwal($czasWMinutach) {
	$minuty = round($czasWMinutach * 60);
	$param = "PT{$minuty}S";
	$interval = new \DateInterval($param);
	return $interval;
    }

    public static function obliczMinutyInterwalu($dataOd, $dataDo) {
	$sekundy = self::obliczSekundyInterwalu($dataOd, $dataDo);

	return self::sekundyNaMinuty($sekundy);
    }

    public static function obliczMinutyInterwaluD(\DateTime $dataOd, \DateTime $dataDo) {

	return self::obliczMinutyInterwalu($dataOd->format(self::FORMAT_TIMESTAMP),
			$dataDo->format(self::FORMAT_TIMESTAMP));
    }

    public static function obliczSekundyInterwalu($dataOd, $dataDo) {
	$date1 = strtotime($dataOd);
	$date2 = strtotime($dataDo);

	$diff = $date2 - $date1;

	return $diff;
    }

    public static function sekundyNaMinuty($sekundy) {
	return floor($sekundy / 60);
    }

    public static function dodajOdejmijInterwal($dataString, $czasPrzesunieciaWMinutach) {
	if ($czasPrzesunieciaWMinutach > 0) {
	    return self::dodajInterwal($dataString, $czasPrzesunieciaWMinutach);
	} else {
	    return self::odejmijInterwal($dataString, abs($czasPrzesunieciaWMinutach));
	}
    }

    public static function dodajInterwal($dataString, $czasPrzesunieciaWMinutach, $wynikStr = true) {
	return self::wykonajOdejmijInterwal($dataString, $czasPrzesunieciaWMinutach, 'add', $wynikStr);
    }

    public static function odejmijInterwal($dataString, $czasPrzesunieciaWMinutach, $wynikStr = true) {
	return self::wykonajOdejmijInterwal($dataString, $czasPrzesunieciaWMinutach, 'sub', $wynikStr);
    }

    public static function dodajSekundy($dataString, $czasPrzesunieciaWSekundach, $wynikStr = true) {
	return self::dodajOdejmijSekundy($dataString, $czasPrzesunieciaWSekundach, 'add', $wynikStr);
    }

    public static function odejmijSekundy($dataString, $czasPrzesunieciaWSekundach, $wynikStr = true) {
	return self::dodajOdejmijSekundy($dataString, $czasPrzesunieciaWSekundach, 'sub', $wynikStr);
    }

    public static function dodajOdejmijSekundy($dataString, $czasPrzesunieciaWSekundach, $metoda,
	    $wynikStr = true) {
	$param = "PT{$czasPrzesunieciaWSekundach}S";
	$interval = new \DateInterval($param);

	$data = self::dateAdapter($dataString);
	$data->$metoda($interval);
	if ($wynikStr) {
	    return $data->format("Y-m-d H:i:s");
	} else {
	    return $data;
	}
    }

    public static function pobierzNajblizyPoczatekMiesiaca($nrMiesiaca, $dataBazowa = null) {

	$data = new \DateTime($dataBazowa);

	$rok = $data->format('Y');
	$miesiac = $data->format('m');

	if ($nrMiesiaca <= $miesiac) {
	    $rok++;
	}

	$data->setDate($rok, $nrMiesiaca, 1);
	$data->setTime(0, 0);


	return $data->format(self::FORMAT_TIMESTAMP);
    }

    public static function ustawCzas($godzina, $minuta, $dataBazowa = null) {
	$data = self::dateAdapter($dataBazowa);
	$data->setTime($godzina, $minuta);

	return $data;
    }

    public static function zwiekszDateODni($ileDni, $date = null) {
	$data = self::dateAdapter($date);

	$data->add(new \DateInterval("P{$ileDni}D"));

	return $data;
    }

    public static function zmniejszDateODni($ileDni, $date = null) {
	$data = self::dateAdapter($date);

	$data->sub(new \DateInterval("P{$ileDni}D"));

	return $data;
    }

    public static function formatujDateNaTimestamp($strData = null) {
	return (new \DateTime($strData))->format(self::FORMAT_TIMESTAMP);
    }

    public static function pobierzDzienTygodnia(\DateTime $data) {
	return $data->format("w");
    }

    public static function poczatekDnia($dataLubString = null) {
	$data = self::dateAdapter($dataLubString);
	$data->setTime(0, 0, 0);

	return $data;
    }

    public static function koniecDnia($dataLubString) {
	$data = self::dateAdapter($dataLubString);
	$data->setTime(23, 59, 59);

	return $data;
    }

    public static function pobierzNastepnaDateBedacaWielokrotnosciaMinut($start, $end, $liczbaMinut) {

	$interwal = self::obliczMinutyInterwalu($start, $end);

	$ileJednostekCzasu = floor($interwal / $liczbaMinut);
	$ileJednostekCzasu+=1;
	$wynikowyInterwal = $ileJednostekCzasu * $liczbaMinut;

	$wynik = self::dodajOdejmijInterwal($start, $wynikowyInterwal);

	return $wynik;
    }

    private static function wykonajOdejmijInterwal($dataString, $czasPrzesunieciaWMinutach, $metoda, $wynikStr) {
	$interval = \Pastmo\Wspolne\Utils\DateUtil::obliczInterwal($czasPrzesunieciaWMinutach);

	$data = self::dateAdapter($dataString);
	$data->$metoda($interval);
	if ($wynikStr) {
	    return $data->format("Y-m-d H:i:s");
	} else {
	    return $data;
	}
    }

    public static function czyTenSamDzien($data1, $data2) {
	$koniec1 = self::koniecDnia($data1);
	$koniec2 = self::koniecDnia($data2);

	return $koniec1 == $koniec2;
    }

    public static function data1WiekszOd2($data1, $data2) {
	return (self::dateAdapter($data1) > self::dateAdapter($data2));
    }

    public static function formatujDoDzienMiesiac($dataStr) {
	$data = self::dateAdapter($dataStr);
	return $data->format(self::FORMAT_DZIEN_MIESIAC_KROPKI);
    }

    public static function formatujDoDzienMiesiacRok($dataStr) {
	$data = self::dateAdapter($dataStr);
	return $data->format(self::FORMAT_DZIEN_MIESIAC_ROK_KROPKI);
    }

}
