<?php

namespace Pastmo\Wspolne\Utils;

class Search {

    const QUERY_KEY = "query_key";

    private $i;

    public function createWhere($pola, $szkanyTekst) {
	$this->i = 0;
	$where = "";
	foreach ($pola as $pole) {
	    $where .= $this->getWherePart($pole, $szkanyTekst);
	}

	return $where;
    }

    public function zrobPojedynczyWhere($nazwaKolumny, $szukanyTekst) {
	return $nazwaKolumny .
		"  REGEXP '[\w]*(" . $this->changePolishWords($szukanyTekst) . ")[\w]*' ";
    }

    private function getWherePart($column, $szukanyTekst) {
	$nazwaKolumny = $column->getName();

	switch ($column->getDataType()) {
	    case ("datetime"):
	    case ("timestamp"):
	    case ("char"):
		return "";
	    case ("int"):
	    case ("float"):

		if ($this->czyLiczba($szukanyTekst) && $this->czyNieKluczObcy($nazwaKolumny)) {
		    return $this->getOr() . $nazwaKolumny . "= $szukanyTekst";
		} else {
		    return "";
		}
	    case ("query"):
		return $this->wyszukiwanieQuery($column, $szukanyTekst);
	    default :
		return $this->wyszukiwanieStringa($nazwaKolumny, $szukanyTekst);
	}
    }

    private function czyLiczba($szukanyTekst) {
	$isNumeric = is_numeric($szukanyTekst);
	return $isNumeric;
    }

    private function czyNieKluczObcy($nazwaKolumny) {
	return stripos($nazwaKolumny, '_id') === false;
    }

    private function wyszukiwanieQuery($kolumna, $szukanyTekst) {
	$query = $kolumna->getQuery();
	$name = $kolumna->getName();

	$oldI = $this->i;
	$this->i = 0;

	$str = $this->wyszukiwanieStringa("", $szukanyTekst);

	$this->i = $oldI;
	$finalQuery = $this->getOr() . $name . ' ' . sprintf($query, $str);
	return $finalQuery;
    }

    private function wyszukiwanieStringa($nazwaKolumny, $szukanyTekst) {
	return $this->getOr() . $this->zrobPojedynczyWhere($nazwaKolumny, $szukanyTekst);
    }

    private function getOr() {
	$result = $this->i > 0 ? ' OR ' : '';
	$this->i++;
	return $result;
    }

    public function changePolishWords($input) {
	$result = "";

	for ($i = 0; $i < strlen($input); $i++) {
	    $sign = mb_substr($input, $i, 1);
	    $result.=$this->getReplace($sign);
	}
	return $result;
    }

    private function getReplace($leter) {
	if (in_array($leter, array('a', 'A', 'ą', 'Ą')))
	    return "[aą]";
	if (in_array($leter, array('c', 'C', 'ć', 'Ć')))
	    return '[cć]';
	if (in_array($leter, array('e', 'E', 'ę', 'Ę')))
	    return '[eę]';
	if (in_array($leter, array('l', 'L', 'ł', 'Ł',)))
	    return '[lł]';
	if (in_array($leter, array('o', 'O', 'ó', 'Ó',)))
	    return '[oó]';
	if (in_array($leter, array('s', 'S', 'ś', 'Ś',)))
	    return '[sś]';
	if (in_array($leter, array('z', 'Z', 'ź', 'Ź', 'ż', 'Ż')))
	    return '[zźż]';
	return $leter;
    }

    public static function getClass() {
	return __CLASS__;
    }

}
