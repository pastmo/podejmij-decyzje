<?php

namespace Pastmo\Wspolne\Utils;

class StrUtil {

    public static function skrocStr($str, $oIleZnakowOdKonca) {
	return substr($str, 0, strlen($str) - $oIleZnakowOdKonca);
    }

    public static function lamLinie($str, $ileZnakowe, $separatorWierszy = PHP_EOL) {
	$strTab = explode($separatorWierszy, $str);
	$result = "";

	foreach ($strTab as $el) {
	    $result .= self::lamPojedynczaLinie($el, $ileZnakowe);
	}

	return $result;
    }

    private static function lamPojedynczaLinie($str, $ileZnakowe, $separatorWierszy = PHP_EOL) {
	$wynik = "";

	$dlugoscPierwotna = strlen($str);

	for ($aktulnyPoczatek = 0; $aktulnyPoczatek < $dlugoscPierwotna; $aktulnyPoczatek += $ileZnakowe) {
	    $wynik .= substr($str, $aktulnyPoczatek, $ileZnakowe) . $separatorWierszy;
	}

	return $wynik;
    }

    public static function getImie($imieNazwisko) {
	return self::wydobadzElement(explode(" ", $imieNazwisko), 0);
    }

    public static function getNazwisko($imieNazwisko) {
	return self::wydobadzElement(explode(" ", $imieNazwisko), 1);
    }

    private static function wydobadzElement($array, $index) {
	return isset($array[$index]) ? $array[$index] : "";
    }

    public static function pobierzKoncowke($str, $ileZnakowOdKonca) {
	return substr($str, strlen($str) - $ileZnakowOdKonca, $ileZnakowOdKonca);
    }

    public static function drukujCene($input) {
	$float = (float) $input;
	$wynik = number_format($float, 2, ',', ' ');
	return $wynik;
    }

    public static function polaczDateZCzasem($data, $czas) {
	$datoCzas = date("Y-m-d H:i:s", strtotime("$data $czas"));
	return $datoCzas;
    }

    public static function drukujSamaDateNull($data) {
	if ($data) {
	    return self::drukujSamaDate($data);
	}
    }

    public static function drukujSamaDate($data) {
	$dateTime = new \DateTime($data);
	return $dateTime->format("Y-m-d");
    }

    public static function drukujGodzinyMinutyNull($data) {
	if ($data) {
	    return self::drukujGodzinyMinuty($data);
	}
    }

    public static function drukujGodzinyMinuty($data) {
	$dateTame = new \DateTime($data);
	return $dateTame->format("H:i");
    }

    public static function dateTimeBezSekund($data) {
	if ($data) {
	    $dateTame = new \DateTime($data);
	    return $dateTame->format("Y-m-d H:i");
	}
    }

    public static function usunNiedrukowalneZnaki($str) {
	$str = str_replace(' ', '', $str);
	$str = str_replace(PHP_EOL, '', $str);
	$str = str_replace("\n", '', $str);
	$str = str_replace("\t", '', $str);

	return $str;
    }

    public static function ulamekNaProcenty($ulamek) {
	return self::drukujCene($ulamek * 100) . "%";
    }

    public static function fromArray(array $wejscie) {

	return implode(",", $wejscie);
    }

    public static function fromArrayWithSpaces(array $wejscie) {

	return implode(" ", $wejscie);
    }

    public static function fromArrayToSql(array $wejscie, $poleTekstowe = true) {
	$wejsciePrzerobione = array();

	foreach ($wejscie as $input) {
	    if (is_numeric($input)) {
		$wejsciePrzerobione[] = $input;
	    } else {
		$wejsciePrzerobione[] = $poleTekstowe ? "'$input'" : $input;
	    }
	}

	return implode(",", $wejsciePrzerobione);
    }

    public static function dodajZnacznikiOdpowiedziMaila($tresc) {
	return "> " . str_replace("\n", "\n> ", $tresc);
    }

    public static function czyNiepusty($string) {
	return $string && $string !== '';
    }

    public static function nazwaAkcjiNaRoute($nazwaMetody) {
	$nazwaMetody = str_replace("Action", "", $nazwaMetody);
	$result = strtolower(preg_replace('/(.)([A-Z])/', '$1_$2', $nazwaMetody));
	return $result;
    }

    public static function klasaNaRoute($klasaKontrolera) {
	$tabl = explode('\\', $klasaKontrolera);
	$klasaPelna = $tabl[count($tabl) - 1];
	$klasa = str_replace("Controller", "", $klasaPelna);
	$result = self::nazwaAkcjiNaRoute($klasa);
	return $result;
    }

    public static function usunZnakiNienadajaceSieNaRozszerzeniePliku($string) {
	$result = preg_replace("/[^a-zA-Z0-9.]+/", "", $string);
	return $result;
    }

    public static function is_base64_encoded($string) {
	$string = str_replace(PHP_EOL, "", $string);
	$string = str_replace("\n", "", $string);
	$decoded = base64_decode($string, true);

	// Check if there is no invalid character in string
	if (!preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $string))
	    return false;

	// Decode the string in strict mode and send the response
	if (!base64_decode($string, true))
	    return false;

	// Encode and compare it to original one
	if (base64_encode($decoded) != $string)
	    return false;

	return true;
    }

    public static function base64_decode($data) {
	$result = self::is_base64_encoded($data);
	if ($result) {
	    return base64_decode($data);
	} else {
	    return $data;
	}
    }

    public static function stringZawiera($kontener, $string, $offset = 0) {
	$wynik = strpos($kontener, $string, $offset) !== false;
	return $wynik;
    }

    public static function substr($klucz, $zrodlo, $koncowyZnak, $ofset = 0) {
	$dlugoscKlucza = strlen($klucz);

	$pozycjaPoczatkowa = strpos($zrodlo, $klucz, $ofset);
	$ofset = $pozycjaPoczatkowa + $dlugoscKlucza;
	$enter = strpos($zrodlo, $koncowyZnak, $ofset);

	if ($pozycjaPoczatkowa !== false && $enter !== false) {

	    $offset = $pozycjaPoczatkowa + $dlugoscKlucza;

	    $wynik = substr($zrodlo, $offset, $enter - $offset);
	    return $wynik;
	}

	return FALSE;
    }

    public static function substr_wlacznie($klucz, $zrodlo, $koncowyZnak, $ofset = 0) {
	$dlugoscKlucza = strlen($klucz);
	$dlugoscKoncowegoZnaku = strlen($koncowyZnak);

	$pozycjaPoczatkowa = strpos($zrodlo, $klucz, $ofset);
	$ofset = $pozycjaPoczatkowa + $dlugoscKlucza;
	$enter = strpos($zrodlo, $koncowyZnak, $ofset);

	if ($pozycjaPoczatkowa !== false && $enter !== false) {

	    $offset = $pozycjaPoczatkowa;

	    $wynik = substr($zrodlo, $offset, $enter - $offset + $dlugoscKoncowegoZnaku);
	    return $wynik;
	}

	return FALSE;
    }

    public static function wyodrebnijOstatniaCzescUri($url) {
	$uri = \Zend\Uri\UriFactory::factory($url);
	$path = $uri->getPath();
	$pathFragments = explode('/', $path);
	$end = end($pathFragments);
	return $end;
    }

}
