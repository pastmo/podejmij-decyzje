<?php

namespace Pastmo\Wspolne\Utils;

use \Pastmo\Wspolne\Entity\WspolnyModel;

class EntityUtil {

    public static function czyEncjaZId($modelLubId) {
	$id = self::wydobadzId($modelLubId);
	return $id !== null;
    }

    public static function wydobadzId($modelLubId) {
	return self::wydobadzPole($modelLubId, 'id');
    }

    public static function wydobadzPole($modelLubWartosc, $nazwaPola) {
	if (is_object($modelLubWartosc)) {
	    return $modelLubWartosc->$nazwaPola;
	}
	return $modelLubWartosc;
    }

    public static function sprawdzCzyWspolnyModel($inputData) {
	if (is_object($inputData) && $inputData instanceof WspolnyModel) {
	    return true;
	}
	return false;
    }

    public static function pobierzKluczEncji($klucz, $kluczWEncji = false) {
	if (!$kluczWEncji) {
	    $kluczWEncji = str_replace('_id', '', $klucz);
	}
	return $kluczWEncji;
    }

}
