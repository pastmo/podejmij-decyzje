<?php

namespace Pastmo\Wspolne\Mechanizmy;

use Pastmo\Wspolne\Utils\DateUtil;

trait CykliczneAktualizacjeTrait {

    public function aktualizujCyklicznie() {
	if ($this->czyGodzinaAktualizacji()) {
	    try {
		$this->beginTransaction();
		$metoda = $this->getNazweMetodyAktualizacji();
		$this->$metoda();

		$builder = \Pastmo\Logi\Menager\LogiMenager::getBuilder()
			->addTresc('msg', "Wykonano:" . __CLASS__)
			->setKodKategorii($this->getKodLogu())
		;
		$logiMenager = $this->sm->get(\Pastmo\Logi\Menager\LogiMenager::class);
		$logiMenager->dodajLog($builder);

		$this->commit();
	    } catch (\Exception $e) {
		echo $e->getTraceAsString();
		$this->rollback();

		$builder = \Pastmo\Logi\Menager\LogiMenager::getBuilder()
			->addTresc('msg', $e->getMessage())
			->addTresc('file', $e->getFile())
			->addTresc('line', $e->getLine())
			->addTresc('stack', $e->getTraceAsString())
			->setKodKategorii($this->getKodLogu())
		;
		$logiMenager = $this->sm->get(\Pastmo\Logi\Menager\LogiMenager::class);
		$logiMenager->dodajLog($builder);
	    }
	}
    }

    public function czyGodzinaAktualizacji() {
	$godzinaAktualizacji = $this->get(\Pastmo\Wspolne\Menager\UstawieniaSystemuMenager::class)->pobierzUstawieniePoKodzie($this->getKodUstawienGodziny(),
		$this->getDomyslnaGodzine());
	$aktualnyTimestamp = $this->get(\Pastmo\Wspolne\Menager\CzasMenager::class)->getAktualnyTimestamp();

	$godzinaAktualizacjiArr = explode(":", $godzinaAktualizacji);

	$dataAktualizacji = DateUtil::ustawCzas($godzinaAktualizacjiArr[0], $godzinaAktualizacjiArr[1],
			$aktualnyTimestamp);

	$interwal = DateUtil::obliczMinutyInterwaluD(DateUtil::dateAdapter($aktualnyTimestamp), $dataAktualizacji);

	return abs($interwal) < 2;
    }

    abstract public function getNazweMetodyAktualizacji();

    abstract public function getKodUstawienGodziny();

    abstract public function getDomyslnaGodzine();

    abstract public function getKodLogu();
}
