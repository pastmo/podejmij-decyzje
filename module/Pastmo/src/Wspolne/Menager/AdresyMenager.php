<?php

namespace Pastmo\Wspolne\Menager;

class AdresyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Wspolne\KonfiguracjaModulu::PASTMO_ADRESY_GATEWAY);
    }

}
