<?php

namespace Pastmo\Wspolne\Menager;

use Pastmo\Wspolne\Entity\UstawienieSystemu;
use Pastmo\Wspolne\Utils\EntityUtil;

class UstawieniaSystemuMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    protected $wyszukiwanieZKontem = false;
    private $kod;
    private $zalogowany;
    private $domyslnaWartosc;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Wspolne\KonfiguracjaModulu::USTAWIENIA_SYSTEMU_GATEWAY);
    }

    public function pobierzUstawieniePoKodzie($kod, $domyslnaWartosc = false) {
	$this->kod = $kod;
	$this->domyslnaWartosc = $domyslnaWartosc;
	$this->zalogowany = null;

	return $this->pobierzWspolne();
    }

    public function pobierzUstawienieZalogowanegoPoKodzie($kod, $domyslnaWartosc = false) {
	$this->kod = $kod;
	$this->domyslnaWartosc = $domyslnaWartosc;
	$this->zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();

	return $this->pobierzWspolne("AND uzytkownik_id={$this->zalogowany->id}");
    }

    public function aktualizujUstawieniaZalogowanego($kod, $wartosc) {
	$ustawienie = $this->pobierzUstawienieZalogowanegoPoKodzie($kod, $wartosc);

	$ustawienie->wartosc = $wartosc;
	$this->zapisz($ustawienie);
    }

    private function pobierzWspolne($dodatkowyWhere = "") {
	$wynik = $this->pobierzZWherem("kod='{$this->kod}' $dodatkowyWhere");

	if (count($wynik) > 0) {
	    return $wynik[0];
	}

	return UstawienieSystemu::create($this->kod, $this->domyslnaWartosc, $this->zalogowany);
    }

}
