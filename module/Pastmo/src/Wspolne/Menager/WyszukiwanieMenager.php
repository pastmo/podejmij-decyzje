<?php

namespace Pastmo\Wspolne\Menager;

abstract class WyszukiwanieMenager extends WspolnyMenager {

    const DOMYSLNY_MAX_RESULT = 10;

    private $maxResult = self::DOMYSLNY_MAX_RESULT;

    public function pobierzPodpowiedzi($string) {
	$menagery = $this->pobierzKlasyModulow();
	$wynik = array();

	foreach ($menagery as $klasyMenagerow) {
	    $menager = $this->sm->get($klasyMenagerow);
	    $part = $menager->wyszukaj($string, $this->maxResult);
	    $wynik = array_merge($wynik, $part);
	}

	$this->setMaxResult(self::DOMYSLNY_MAX_RESULT);
	return $wynik;
    }

    public function setMaxResult($maxResult) {
	$this->maxResult = $maxResult;
    }

    abstract public function pobierzKlasyModulow();
}
