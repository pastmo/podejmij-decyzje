<?php

namespace Pastmo\Wspolne\Menager;

use \Pastmo\Wspolne\Entity\WspolnyModel;
use Zend\ServiceManager\ServiceManager;
use Wspolne\Utils\Search;
use \Pastmo\Wspolne\Utils\ArrayUtil;
use \Zend\Db\Sql\Combine;
use Zend\Db\Adapter\Platform\Mysql;

abstract class WspolnyMenagerBazodanowy extends WspolnyMenager implements \Zend\Hydrator\HydratorInterface {

    protected $tableGateway;
    protected $dbAdapter;
    protected $fasadyContainer;
    protected $dao;

    const DOMYSLNY_ORDER = "id DESC";
    const CHRONOLOGICZNY_ORDER = "id ASC";
    const DOMYSLNY_LIMIT = false;

    public function __construct(ServiceManager $sm = null, $gatewayName = '') {
	parent::__construct($sm);
	if ($this->sm) {
	    $this->tableGateway = $this->sm->get($gatewayName);
	    $this->dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
	}
    }

    public function ustawDao($klasa, $nazwaGateway) {
	$this->dao = new $klasa($this->sm, $nazwaGateway);
    }

    function initFactory() {

    }

    public function fetchAll($order = self::DOMYSLNY_ORDER) {
	$select = $this->tableGateway->getSql()->select();
	$select->order($order);
	$this->modyfikujSelecta($select);
	$resultSet = $this->tableGateway->selectWith($select);
	return $resultSet;
    }

    public function pobierzResultSetZWherem($where, $order = self::DOMYSLNY_ORDER,
	    $limit = self::DOMYSLNY_LIMIT, $groupBy = false) {
	$select = $this->tableGateway->getSql()->select();
	$select->order($order);
	$safeWhere = $this->sqlInjectionProtector($where);
	$select->where($safeWhere);
	$this->modyfikujSelecta($select);

	if ($limit) {
	    $select->limit($limit);
	}
	if ($groupBy) {
	    $select->group($groupBy);
	}

	$string = $this->tableGateway->getSql()->buildSqlString($select);

	$resultSet = $this->tableGateway->selectWith($select);
	return $resultSet;
    }

    public function pobierzWszystkoArray($order = self::DOMYSLNY_ORDER) {

	$resultSet = $this->fetchAll($order);
	return $this->konwertujNaArray($resultSet);
    }

    public function pobierzZWherem($where, $order = self::DOMYSLNY_ORDER, $limit = false) {
	$select = $this->tableGateway->getSql()->select();
	$select->order($order);
	$safeWhere = $this->sqlInjectionProtector($where);
	$select->where($safeWhere);
	$this->modyfikujSelecta($select);
	if ($limit) {
	    $select->limit($limit);
	}

	//Tutaj można podejrzeć wynikowy sql i sprawdzić go wklejając do klienta sql
	$string = $this->tableGateway->getSql()->buildSqlString($select);
	
	$resultSet = $this->wykonajSelect($select);
	return ArrayUtil::konwertujNaArray($resultSet);
    }

    public function pobierzZWheremCount($where) {
	$select = $this->tableGateway->getSql()->select();
	$safeWhere = $this->sqlInjectionProtector($where);
	$select->where($safeWhere);
	$this->modyfikujSelecta($select);
	$resultSet = $this->tableGateway->selectWith($select);

	$string = $this->tableGateway->getSql()->buildSqlString($select);

	return $resultSet->count();
    }

    protected function wykonajSelect($select) {
	$resultSet = $this->tableGateway->selectWith($select);
	return $resultSet;
    }

    //TODO: Metoda zalecana to ArrayUtil::konwertujNaArray
    public function konwertujNaArray($resultSet) {
	return ArrayUtil::konwertujNaArray($resultSet);
    }

    public function pobierzPaginator(\Pastmo\Wspolne\Builder\PaginatorBuilder $builder) {

	$prototypEncji = $this->tableGateway->getResultSetPrototype()->getArrayObjectPrototype();
	$resultSet = new \Zend\Db\ResultSet\HydratingResultSet($this, $prototypEncji);

	$query = $this->tableGateway->getSql()->select();
	$query->where($builder->where);
	$query->order($builder->order);
	if ($builder->groupBy) {
	    $query->group($builder->groupBy);
	}

	$this->modyfikujSelecta($query);

	$adapter = new \Zend\Paginator\Adapter\DbSelect($query, $this->dbAdapter, $resultSet);
	$paginator = new \Zend\Paginator\Paginator($adapter);

	if ($builder->wynikowNaStronie) {
	    $paginator->setItemCountPerPage($builder->wynikowNaStronie);
	}

	$paginator->setCurrentPageNumber($builder->strona);

	return $paginator;
    }

    public function getRekord($id) {
	$id = (int) $id;
	$rowset = $this->tableGateway->select(array('id' => $id));
	$row = $rowset->current();
	if (!$row) {
	    throw new \Exception("Could not find row $id");
	}
	return $row;
    }

    public function getRekordKlucz($klucz, $wartosc) {
	$rowset = $this->tableGateway->select(array($klucz => $wartosc));
	$row = $rowset->current();
	if (!$row) {
	    throw new \Exception("Could not find row $klucz");
	}
	return $row;
    }

    public function getRekordKluczWartoscDomyslna($klucz, $wartosc, $wartoscDomyslna) {
	$rowset = $this->tableGateway->select(array($klucz => $wartosc));
	$row = $rowset->current();
	if (!$row) {
	    return $wartoscDomyslna;
	}
	return $row;
    }

    public function getPoprzednioDodany() {
	$lastId = $this->tableGateway->getLastInsertValue();
	if ($lastId) {
	    $lastRekord = $this->getRekord($lastId);
	    return $lastRekord;
	} else {
	    return NULL;
	}
    }

    public function update($id, $set) {
	$this->tableGateway->update($set, array('id' => $id));
    }

    public function updateWhere($set, $whereArray) {
	$this->tableGateway->update($set, $whereArray);
    }

    public function zapisz(WspolnyModel $model) {
	$id = (int) $model->id;

	$data = $model->zrobArray();

	if ($id == 0) {
	    $this->tableGateway->insert($data);
	} else {
	    if ($this->getRekord($id)) {
		$this->tableGateway->update($data, array('id' => $id));
	    } else {
		throw new \Exception('Brak rekordu');
	    }
	}
    }

    public function updateNaNull($klucz, $id) {
	$this->tableGateway->update(array($klucz => NULL), array('id' => $id));
    }

    public function getNazwyArray() {
	$obiekty = $this->fetchAll();
	$wynik = array();
	foreach ($obiekty as $obiekt) {
	    $wynik[$obiekt->id] = $obiekt->nazwa;
	}

	return $wynik;
    }

    public function usun($where) {
	$this->tableGateway->delete($where);
    }

    public function usunPoId($id) {
	if (!$id || $id === 0) {
	    throw new \Exception("Błedny nr id $id");
	}

	$this->usun("id=$id");
    }

    protected function usunZTabeliObcej($klasaTable, $id) {
	$table = $this->get($klasaTable);
	if ($id) {
	    $table->usunPoId($id);
	}
    }

    protected function usunZTabeliObcejZWherem($klasaTable, $klucz, $id) {
	$table = $this->get($klasaTable);
	if ($id) {
	    $table->usun("$klucz=$id");
	}
    }

    //TODO:: należy używać ArrayUtil::konwertujNaArray
    protected function konwertujNaTablice($resultSet) {
	return ArrayUtil::konwertujNaArray($resultSet);
    }

    public function zapiszWInnymTable(WspolnyModel $encja, $tableName) {
	$table = $this->sm->get($tableName);
	$table->zapisz($encja);
	$zapisanaEncja = $table->getPoprzednioDodany();
	return $zapisanaEncja;
    }

    public function aktualizujPowiazanaTabeleZObiektu($wlasnyId, $polePowiazanej, $klasaMenagera,
	    WspolnyModel $encja) {

	$encja->id = null;
	$post = $encja->zrobArray();

	return $this->aktualizujPowiazanaTabele($wlasnyId, $polePowiazanej, $klasaMenagera, $post);
    }

    public function aktualizujPowiazanaTabele($wlasnyId, $polePowiazanej, $klasaMenagera, $post) {
	$encja = $this->getRekord($wlasnyId);
	$akctualizowana = $encja->$polePowiazanej;

	$akctualizowana->aktualizujArray($post);
	$this->get($klasaMenagera)->zapisz($akctualizowana);

	if (!$akctualizowana->id) {
	    $zapisanaEncja = $this->get($klasaMenagera)->getPoprzednioDodany();
	    $encja->$polePowiazanej = $zapisanaEncja;
	    $this->zapisz($encja);
	}

	return $encja;
    }

    public function kopiujEncje(WspolnyModel $encja, $tableName) {
	$klon = clone $encja;
	$klon->id = null;
	return $this->zapiszWInnymTable($klon, $tableName);
    }

    public function utworzWherePoWszystkichPolach($text) {
	$search = new Search();

	$kolumnyInfo = $this->pobierzKolumnyInfo();

	$wynik = $search->createWhere($kolumnyInfo, $text);
	return $wynik;
    }

    public function utworzWherePoWybranychPolach($text) {
	$search = new Search();

	$kolumnyInfo = $this->pobierzKolumnyInfo();

	$wynik = $search->createWhere($kolumnyInfo, $text);
	return $wynik;
    }

    public function pobierzIdZasobow($post, $klucz) {
	$zasobyArray = array();

	if (isset($post[$klucz]) && $post[$klucz] !== "") {
	    $zasobyArray = json_decode($post[$klucz]);
	}

	return $zasobyArray;
    }

    public function beginTransaction() {
	$this->tableGateway->adapter->getDriver()->getConnection()->beginTransaction();
    }

    public function commit() {
	$this->tableGateway->adapter->getDriver()->getConnection()->commit();
    }

    public function rollback() {
	$this->tableGateway->adapter->getDriver()->getConnection()->rollback();
    }

    protected function pobierzKolumnyInfo() {
	$metadata = new \Zend\Db\Metadata\Metadata($this->dbAdapter);
	$tableName = $this->tableGateway->getTable();
	$table = $metadata->getTable($tableName);
	$meta = $table->getColumns();

	return $meta;
    }

    public function wyszukaj($string, $limit) {
	$wyszukiwanieMenager = \Pastmo\Wspolne\Menager\Wyszukiwacz::create()
		->setTable($this)
		->setLimit($limit)
		->setPoszukiwanyNapis($string)
		->setPola($this->pobierzPolaWyszukiwania());

	$resultSet = $wyszukiwanieMenager->wyszukaj();

	$wynik = array();

	foreach ($resultSet as $part) {
	    $czesciowyWynik = $this->konwertujNaRekordWyszuiwania($part);

	    $wynik[] = $czesciowyWynik;
	}

	return $wynik;
    }

    public function extract($object) {
	$array = $object->zrobArray();
	return $array;
    }

    public function modyfikujSelecta($select) {

    }

    public function hydrate(array $data, $object) {
	$object->exchangeArray($data);
	return $object;
    }

    public function getSelect() {
	return $this->tableGateway->getSql()->select();
    }

    public function pobierzZKilkuSelectow(array $selects, $order = self::DOMYSLNY_ORDER) {
	$combinate = new Combine();

	if (count($selects) === 0) {
	    return [];
	}

	foreach ($selects as $select) {

	    $this->modyfikujSelecta($select);

	    $combinate->union($select);
	}

	$string = $combinate->getSqlString(new Mysql);
	$string.=" ORDER BY $order";

	$resultSet = $this->tableGateway->adapter->query($string, 'execute');
	$this->kopiujPrototypDoResultSeta($resultSet);

	return $resultSet;
    }

    protected function kopiujPrototypDoResultSeta($resultSet) {
	$resultSet->setArrayObjectPrototype($this->tableGateway->getResultSetPrototype()->getArrayObjectPrototype());
    }

    protected function pobierzPolaWyszukiwania() {
	throw new Exception("Należy przesłonić tę metodę");
    }

    protected function konwertujNaRekordWyszuiwania($encja) {
	throw new Exception("Należy przesłonić tę metodę");
    }

    protected function sqlInjectionProtector($where) {

	$pattern = '/(;)/i';
	$replacement = '/;';
	$result = preg_replace($pattern, $replacement, $where);

	return $result;
    }

}
