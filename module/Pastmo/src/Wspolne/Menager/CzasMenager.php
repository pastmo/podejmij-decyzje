<?php

namespace Pastmo\Wspolne\Menager;

use Pastmo\Wspolne\Utils\DateUtil;

class CzasMenager extends WspolnyMenager {

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm);
    }

    public function getAktualnyTimestamp() {
	return date(DateUtil::FORMAT_TIMESTAMP);
    }

    public function getAktualnyTimestampBezSekund() {
	return date(DateUtil::FORMAT_TIMESTAMP_BEZ_SEKUND);
    }

}
