<?php

namespace Pastmo\Wspolne\Menager;

class Wyszukiwacz extends WspolnyMenager {

    public $table;
    public $dodatkowyWhere;
    public $pola = false;
    public $poszukiwanyNapis = false;
    public $sortKlucz = false;
    public $sortTyp = 'DESC';
    public $limit = \Wspolne\Menager\BazowyMenagerBazodanowy::DOMYSLNY_LIMIT;
    private $where;
    private $order = \Wspolne\Menager\BazowyMenagerBazodanowy::DOMYSLNY_ORDER;

    public function wyszukaj() {


	$this->resetujWhere();
	$this->utworzWhereZDodatkowymPolem();
	$this->utworzGlownyWhere();
	$this->utworzOrder();
	return $this->wykonajWyszukiwanie();
    }

    public function setTable($table) {
	$this->table = $table;
	return $this;
    }

    public function setDodatkowyWhere($dodatkowyWhere) {
	$this->dodatkowyWhere = $dodatkowyWhere;
	return $this;
    }

    public function setPola($pola) {
	$this->pola = $pola;
	return $this;
    }

    public function setPoszukiwanyNapis($poszukiwanyNapis) {
	$this->poszukiwanyNapis = $poszukiwanyNapis;
	return $this;
    }

    public function setSortKlucz($sortKlucz) {
	$this->sortKlucz = $sortKlucz;
	return $this;
    }

    public function setSortTyp($sortTyp) {
	$this->sortTyp = $sortTyp;
	return $this;
    }

    public function setLimit($limit) {
	$this->limit = $limit;
	return $this;
    }

    public static function create() {
	return new Wyszukiwacz();
    }

    private function resetujWhere() {

	$this->where = "1 ";
    }

    private function utworzWhereZDodatkowymPolem() {
	if ($this->dodatkowyWhere) {
	    $this->where .="AND {$this->dodatkowyWhere} ";
	}
    }

    private function utworzGlownyWhere() {
	if ($this->poszukiwanyNapis) {

	    $this->where.=' AND (';
	    if ($this->pola === false) {
		$this->where.= $this->table->utworzWherePoWszystkichPolach($this->poszukiwanyNapis);
	    } else {
		$search = new \Wspolne\Utils\Search();
//TODO: sprawdzić czy createWhere nie zwraca pustego wyniku, bo wtedy błędy są
		$this->where .= $search->createWhere($this->pola, $this->poszukiwanyNapis);
	    }
	    $this->where.=")";
	}
    }

    private function utworzOrder() {
	if ($this->sortKlucz) {

	    $this->order = "{$this->sortKlucz} {$this->sortTyp}";
	}
    }

    private function wykonajWyszukiwanie() {
	$wynik = $this->table->pobierzResultSetZWherem($this->where, $this->order, $this->limit);

	return $wynik;
    }

}
