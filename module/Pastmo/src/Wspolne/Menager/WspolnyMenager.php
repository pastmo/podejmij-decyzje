<?php

namespace Pastmo\Wspolne\Menager;

use Zend\ServiceManager\ServiceManager;
use Zend\EventManager\EventManagerInterface;

class WspolnyMenager implements \Zend\EventManager\ListenerAggregateInterface {

    use \Pastmo\Wspolne\Utils\TranslateTrait;

    protected $sm;
    protected $listeners = [];

    public function __construct(ServiceManager $sm = null) {
	if ($sm) {
	    $this->sm = $sm;
	    $this->translator = $this->sm->get(\Pastmo\Tlumaczenia\Menager\PastmoTranslator::class);
	}
    }

    public function initConstantTranslation() {
	if ($this->sm) {
	    $this->constantTranslator = $this->sm->get(\Lokalizacje\Menager\ConstantTranslatorMenager::class);
	}
    }

    public function generujUrl($modul, $parametry = array(), $parametryGet = array(), $czyPelnyUrl = false) {
	$link = $this->sm->get('ViewHelperManager')->get('url')
		->__invoke($modul, $parametry, array('force_canonical' => $czyPelnyUrl, 'query' => $parametryGet));
	return $link;
    }

    public function opakujBasePath($string) {
	try {
	    $link = $this->sm->get('ViewHelperManager')->get('basePath')
		    ->__invoke($string);
	    return $link;
	} catch (\Exception $e) {
	    $this->sm->get('ViewHelperManager')->get('basePath')->setBasePath('');

	    return $this->opakujBasePath($string);
	}
    }

    public function uprawnienie($kod, $czyWyjatek = false) {
	$uprawnieniaMenager = $this->sm->get(\Logowanie\Menager\UprawnieniaMenager::getClass());
	return $uprawnieniaMenager->sprawdzUprawnienie($kod, $czyWyjatek);
    }

    public function uprawnienieUzytkonika($kod, $uzytownikId) {
	$uprawnieniaMenager = $this->sm->get(\Logowanie\Menager\UprawnieniaMenager::getClass());
	return $uprawnieniaMenager->sprawdzUprawnienieUzytkownika($kod, $uzytownikId);
    }

    public function attach(EventManagerInterface $events, $priority = 1) {

    }

    public function detach(\Zend\EventManager\EventManagerInterface $events) {
	foreach ($this->listeners as $index => $listener) {
	    $events->detach($listener);
	    unset($this->listeners[$index]);
	}
    }

    public function get($class) {
	return $this->sm->get($class);
    }

    public function getCurrentTimestamp() {
	$czasMenager = $this->get(CzasMenager::class);
	return $czasMenager->getAktualnyTimestamp();
    }

    public function getCzasMenager() {
	$czasMenager = $this->get(CzasMenager::class);
	return $czasMenager;
    }

    public function getLogiBuilderZWyjatku($e) {
	$builder = \Pastmo\Logi\Builder\LogiBuilder::getBuilder()
		->addTresc('msg', $e->getMessage())
		->addTresc('stack', $e->getTraceAsString())
		->setKodKategorii('error')
	;
	return $builder;
    }

    public function logujWyjatek($e) {
	$builder = $this->getLogiBuilderZWyjatku($e);
	$this->loguj($builder);
    }

    public function loguj(\Pastmo\Logi\Builder\LogiBuilder $builder) {
	$this->get(\Wspolne\Menager\LogiMenager::class)->dodajLog($builder);
    }

}
