<?php

namespace Pastmo\Wspolne\Menager;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;

class StaleGenerator extends BazowyMenagerBazodanowy {

     protected $wyszukiwanieZKontem = false;

    public function fetchAll($order = "id DESC") {
	$select = $this->tableGateway->getSql()->select();
	$select->order("opis ASC");
	$resultSet = $this->tableGateway->selectWith($select);
	return $resultSet;
    }

    public function generuj($listaEncji, $poleWEncji, $czyPhp = true) {

	$result = array();
	foreach ($listaEncji as $encja) {
	    $opis = $this->usunPolskieZnaki($encja->$poleWEncji);
	    $opis = $this->usunApostrofy($opis);
	    $opis = $this->usunSpacje($opis);
	    $opis = $this->usunSlashe($opis);
	    $opis = $this->usunKropki($opis);
	    $opis = $this->usunNawiasy($opis);
	    $opis = $this->usunMyslniki($opis);
	    $opis = $this->naWielkieLitery($opis);

	    if ($czyPhp) {
		$result[] = $this->utworzWpisPhp($encja, $opis);
	    } else {
		$result[] = $this->utworzWpisJavascript($encja, $opis);
	    }
	}
	return $result;
    }

    protected function utworzWpisPhp($encja, $opis) {
	return "const " . $opis . '="' . $encja->kod . '";' . PHP_EOL;
    }

    protected function utworzWpisJavascript($encja, $opis) {
	return $opis . ':"' . $encja->kod . '",' . PHP_EOL;
    }

    private function usunPolskieZnaki($string) {
	return iconv('UTF-8', 'ASCII//TRANSLIT', $string);
    }

    private function usunApostrofy($string) {
	return str_replace("'", "", $string);
    }

    private function usunSpacje($string) {
	return str_replace(" ", "_", $string);
    }

    private function usunSlashe($string) {
	return str_replace("/", "_", $string);
    }

    private function usunKropki($string) {
	return str_replace(".", "_", $string);
    }

    private function usunNawiasy($string) {
	$part = str_replace("(", "_", $string);
	return str_replace(")", "_", $part);
    }

    private function usunMyslniki($string) {
	return str_replace("-", "_", $string);
    }

    private function naWielkieLitery($string) {
	return strtoupper($string);
    }

}
