<?php

namespace Pastmo\Wspolne\Menager;

use Zend\Code\Annotation\AnnotationManager;
use Zend\Code\Reflection\ClassReflection;
use Zend\Code\Reflection\MethodReflection;
use Zend\Code\Annotation\Parser;

class AdnotacjeModeluMenager {

    private $annotationManager;
    private $annotationParser;
    protected $defaultAnnotations = [
	    \Pastmo\Wspolne\Adnotacje\DomyslnyJsonModel::class
    ];

    public function sprawdzCzyMetodaMaUstawionyJsonModel($klasa, $metoda) {
	$reflection = new MethodReflection($klasa, $metoda);
	return $this->sprawdzCzyZawieraAdnotace($reflection, \Pastmo\Wspolne\Adnotacje\DomyslnyJsonModel::class);
    }

    public function sprawdzCzyKlasaMaUstawionyJsonModel($klasa) {
	$reflection = new ClassReflection($klasa);
	return $this->sprawdzCzyZawieraAdnotace($reflection, \Pastmo\Wspolne\Adnotacje\DomyslnyJsonModel::class);
    }

    private function sprawdzCzyZawieraAdnotace($reflection, $szukanaAdnotacja) {
	$annotationManager = $this->getAnnotationManager();
	$annotations = $reflection->getAnnotations($annotationManager);

	if (!$annotations) {
	    return false;
	}

	$wynik = $annotations->hasAnnotation($szukanaAdnotacja);
	return $wynik;
    }

    public function getAnnotationManager() {
	if ($this->annotationManager) {
	    return $this->annotationManager;
	}

	$this->setAnnotationManager(new AnnotationManager());
	return $this->annotationManager;
    }

    public function setAnnotationManager(AnnotationManager $annotationManager) {
	$parser = $this->getAnnotationParser();
	foreach ($this->defaultAnnotations as $annotationName) {
	    $class = $annotationName;
	    $parser->registerAnnotation($class);
	}
	$annotationManager->attach($parser);
	$this->annotationManager = $annotationManager;
	return $this;
    }

    public function getAnnotationParser() {
	if (null === $this->annotationParser) {
	    $this->annotationParser = new Parser\DoctrineAnnotationParser();
	}

	return $this->annotationParser;
    }

}
