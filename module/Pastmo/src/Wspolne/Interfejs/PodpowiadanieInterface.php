<?php

namespace Pastmo\Wspolne\Interfejs;

interface PodpowiadanieInterface {

    public function wyszukaj($string, $limit);
}
