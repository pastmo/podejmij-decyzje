<?php

namespace Pastmo\Wspolne\Controller;

use Pastmo\Wspolne\Adnotacje\DomyslnyJsonModel;
use Wspolne\Controller\WspolneController;
use Logowanie\Enumy\KodyUprawnien;

/**
 * @DomyslnyJsonModel
 */
class PastmoWspolneAjaxController extends WspolneController {

    public function zapiszWielkoscEkranuAction(){
	$this->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE,true);

	$nowaSzerokosc=$this->params()->fromPost('szerokosc',0);
	$this->sesjaMenager->zapiszSzerokoscEkranu($nowaSzerokosc);

	return $this->returnSuccess();
    }
}
