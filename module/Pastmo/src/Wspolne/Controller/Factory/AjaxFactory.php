<?php

namespace Pastmo\Wspolne\Controller\Factory;

use Pastmo\Wspolne\Controller\PastmoWspolneAjaxController;

class AjaxFactory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new PastmoWspolneAjaxController();
    }

}
