<?php

namespace Pastmo\Wspolne;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class KonfiguracjaModulu extends \Pastmo\KonfiguracjaModulu {

    const USTAWIENIA_SYSTEMU_GATEWAY = "USTAWIENIA_SYSTEMU_GATEWAY";
    const PASTMO_ADRESY_GATEWAY = "PASTMO_ADRESY_GATEWAY";



    public function onBootstrap(\Zend\Mvc\MvcEvent $e) {
	parent::onBootstrap($e);

	$viewHelperMenager = $e->getApplication()->getServiceManager()->get('ViewHelperManager');
	$viewHelperMenager->setFactory('checked',
		function($sm) use ($e) {
	    $viewHelper = new Helper\CheckedHelper();
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('active',
		function($sm) use ($e) {
	    $viewHelper = new Helper\ActiveHelper();
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('echo',
		function($sm) use ($e) {
	    $viewHelper = new Helper\EchoHelper();
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('pobierzUstawienie',
		function($sm) use ($e) {
	    $viewHelper = new Helper\PobierzUstawienie($sm);
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('translateConst',
		function($sm) use ($e) {
	    $viewHelper = new Helper\TranslateConst($sm);
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('_translate',
		function($sm) use ($e) {
	    $viewHelper = new Helper\TlumaczeniaHelper($sm);
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('czyTrybTlumaczen',
		function($sm) use ($e) {
	    $viewHelper = new Helper\CzyTrybTlumaczen($sm);
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('wyswietlWynikowaTrescWysiwyga',
		function($sm) use ($e) {
	    $viewHelper = new Helper\WyswietlWynikowaTrescWysiwyga($sm);
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('wyswietlSelectJezyka',
		function($sm) use ($e) {
	    $viewHelper = new Helper\WyswietlSelectJezyka($sm);
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('getSzerokoscEkranu',
		function($sm) use ($e) {
	    $viewHelper = new Helper\GetSzerokoscEkranu($sm);
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('wyswietlGoogleAnalizy',
		function($sm) use ($e) {
	    $viewHelper = new Helper\WyswietlGoogleAnalizy($sm);
	    return $viewHelper;
	});
    }

    public function getFactories() {
	return array(
		Menager\CzasMenager::class => function($sm) {
		    return new Menager\CzasMenager($sm);
		},
		Menager\UstawieniaSystemuMenager::class => function($sm) {
		    return new Menager\UstawieniaSystemuMenager($sm);
		}
		,
		Menager\AdresyMenager::class => function($sm) {
		    return new Menager\AdresyMenager($sm);
		}
		,
		//Trzeba PastmoTranslator tutaj dołączyć, bo nie we wszystkich projektach tłumaczenia są dostępne
		\Pastmo\Tlumaczenia\Menager\PastmoTranslator::class => function($sm) {
		    return new \Pastmo\Tlumaczenia\Menager\PastmoTranslator($sm);
		}
		,
		self::USTAWIENIA_SYSTEMU_GATEWAY => function ($sm) {
		    return $this->ustawGateway($sm, Entity\UstawienieSystemu::class, 'ustawienia_systemu');
		},
		self::PASTMO_ADRESY_GATEWAY => function ($sm) {
		    return $this->ustawGateway($sm, Entity\Adres::class, 'adresy');
		}
	);
    }

}
