<?php

namespace Pastmo\Controller;

use \Zend\ServiceManager\FactoryInterface;

abstract class PastmoControllerFactory implements FactoryInterface {

    abstract public function pobierzMenageryModulow();

    abstract public function pobierzMinimalnaKonfiguracje();

    public function __invoke(\Interop\Container\ContainerInterface $container, $requestedName,
	    array $options = NULL) {

	$initObiekt = new \Wspolne\Controller\Factory\InitObiekt();
	$dostawcyMinimum = $this->pobierzMinimalnaKonfiguracje();
	$dostawcyMenagerow = $this->pobierzMenageryModulow();

	foreach ($dostawcyMinimum as $dostawca) {
	    $dostawca::dowiazMenagery($initObiekt, $container);
	}

	foreach ($dostawcyMenagerow as $dostawca) {
	    $dostawca::dowiazMenagery($initObiekt, $container);
	}


	$controller = $this->newController();
	$controller->init($initObiekt);
	return $controller;
    }

}
