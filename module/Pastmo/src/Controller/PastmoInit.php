<?php

namespace Pastmo\Controller;

trait PastmoInit {

    public $ustawieniaSystemuMenager;
    public $sesjaMenager;
    public $krajeMenager;
    public $reCaptchaService;
    public $tlumaczeniaFasada;
    public $platnosciFasada;

}
