<?php

namespace Pastmo\Controller;

abstract class AbstractControllerFactory {

    abstract public static function dowiazMenagery(\Wspolne\Controller\Factory\InitObiekt $initObiekt,
	    \Interop\Container\ContainerInterface $container);
}
