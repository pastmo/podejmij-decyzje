<?php

namespace Pastmo\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Pastmo\Wspolne\Builder\PobieranieStronicowaneBuilder;
use \Pastmo\Wspolne\Enumy\KluczeGet;
use Pastmo\Wspolne\Utils\ArrayUtil;

class WspolnyController extends AbstractActionController {

    use \Pastmo\Wspolne\Utils\TranslateTrait;

    private $pomocniczyGet;
    private $whereWyszukiwania;
    private $order;
    private $builder;
    public static $PUSTY_LAYOUT = false;
    public static $JSON_MODEL = false;
    public $adnotacjeMenager;
    public $ustawieniaSystemuMenager;
    public $sesjaMenager;
    public $krajeMenager;
    public $reCaptchaService;
    public $platnosciFasada;

    public function init(\Wspolne\Controller\Factory\InitObiekt $initObiekt) {
	$this->translator = $initObiekt->translator;
	$this->constantTranslator = $initObiekt->constantTranslator;
	$this->adnotacjeMenager = new \Pastmo\Wspolne\Menager\AdnotacjeModeluMenager();
	$this->ustawieniaSystemuMenager = $initObiekt->ustawieniaSystemuMenager;
	$this->sesjaMenager = $initObiekt->sesjaMenager;
	$this->krajeMenager = $initObiekt->krajeMenager;
	$this->reCaptchaService = $initObiekt->reCaptchaService;
	$this->platnosciFasada = $initObiekt->platnosciFasada;
	;
    }

    public function onDispatch(\Zend\Mvc\MvcEvent $e) {

	$this->zmienDomyslnyModel($e);

	if (self::$PUSTY_LAYOUT) {
	    $e->getViewModel()->setTemplate('logowanie/layout/pusty_layout');
	}
	return parent::onDispatch($e);
    }

    private function zmienDomyslnyModel(&$e) {

	$routeMatch = $e->getRouteMatch();
	if ($routeMatch) {

	    $klasa = $routeMatch->getParam('controller') . 'Controller'; //TODO:
	    $akcja = $routeMatch->getParam('action');
	    $metoda = static::getMethodFromAction($akcja);

	    if ($klasa === "Controller") {
		return;
	    }

	    if ($this->adnotacjeMenager->sprawdzCzyKlasaMaUstawionyJsonModel($klasa) ||
		    $this->adnotacjeMenager->sprawdzCzyMetodaMaUstawionyJsonModel($klasa, $metoda)) {

		$model = new JsonModel();
		$model->setVariables(array('success' => false));
		$e->setViewModel($model);
	    }
	}
    }

    protected function getPost() {
	$request = $this->getRequest();
	if ($request->isPost()) {
	    return $request->getPost();
	}
	return false;
    }

    protected function getGet() {
	$request = $this->getRequest();
	if ($request->isGet()) {
	    return $request->getQuery();
	}
	return false;
    }

    protected function pobierzZGeta($table, $dodatkowyWhere, $pola = null, $domyslneSortowanie = null) {
	$builder = PobieranieStronicowaneBuilder::create()
		->setTable($table)
		->setDodatkowyWhere($dodatkowyWhere)
		->setPola($pola)
		->setDomyslneSortowanie($domyslneSortowanie)
		->setZwrocPaginator(false)
	;

	return $this->pobierzZGetaStronicowanie($builder);
    }

    protected function pobierzZGetaStronicowanie(PobieranieStronicowaneBuilder $builder, $wszystko = false) {

	$this->pomocniczyGet = $this->getGet();
	$this->builder = $builder;

	$this->ustawWhere();
	$this->ustawOrder();
	$this->ustawStrone();
	$this->ustawWynikowNaStronie($wszystko);
	$wynik = $this->pobierzWynik();

	return $wynik;
    }

    private function ustawWhere() {
	$table = $this->builder->table;
	$pola = $this->builder->pola;
	$dodatkowyWhere = $this->builder->dodatkowyWhere;

	$this->whereWyszukiwania = "1 AND $dodatkowyWhere ";

	$poszukiwany = $this->params()->fromQuery(KluczeGet::WYSZUKAJ, false);

	if ($poszukiwany) {

	    $this->whereWyszukiwania.=' AND (';
	    if (!$pola) {
		$this->whereWyszukiwania = $table->utworzWherePoWszystkichPolach($poszukiwany);
	    } else {
		$search = new \Wspolne\Utils\Search();

		$this->whereWyszukiwania .= $search->createWhere($pola, $poszukiwany);
	    }
	    $this->whereWyszukiwania.=")";
	}
    }

    private function ustawOrder() {
	$this->order = false;

	if (ArrayUtil::czyIstniejeNiepustyKlucz($this->pomocniczyGet, KluczeGet::SORT_KLUCZ)) {
	    $klucz = $this->pomocniczyGet[KluczeGet::SORT_KLUCZ];
	    $typ = $this->pomocniczyGet[KluczeGet::SORT_TYP];

	    $this->order = "$klucz $typ";
	} else if ($this->builder->domyslneSortowanie) {
	    $this->order = $this->builder->domyslneSortowanie;
	}

	$kolumnyOrder = $this->builder->kolumnySortowania;
	if (count($kolumnyOrder) > 0) {
	    $staryOrder = ArrayUtil::getArrayZOrderString($this->order);
	    $this->order = array_merge($staryOrder, $kolumnyOrder);
	}
    }

    private function ustawStrone() {
	if (ArrayUtil::czyIstniejeNiepustyKlucz($this->pomocniczyGet, KluczeGet::STRONA)) {
	    $this->builder->strona = $this->pomocniczyGet[KluczeGet::STRONA];
	}
    }

    private function ustawWynikowNaStronie($wszystko = false) {
	if (ArrayUtil::czyIstniejeNiepustyKlucz($this->pomocniczyGet, KluczeGet::LICZBA_WIERSZY)) {
	    $this->builder->wynikowNaStronie = $this->pomocniczyGet[KluczeGet::LICZBA_WIERSZY];
	}
	if ($wszystko) {
	    $this->builder->wynikowNaStronie = 999999999;
	}
    }

    public function pobierzKluczOrder() {
	$get = $this->getGet();
	$wyszukiwanie = isset($get[KluczeGet::SORT_KLUCZ]) ? $get[KluczeGet::SORT_KLUCZ] : null;
	return $wyszukiwanie;
    }

    public function pobierzTypOrder() {
	$get = $this->getGet();
	$wyszukiwanie = isset($get['sort_typ']) ? $get['sort_typ'] : null;
	return $wyszukiwanie;
    }

    private function pobierzWynik() {
	if ($this->builder->zwrocPaginator) {
	    return $this->pobierzWynikZeStronicowaniem();
	} else {
	    return $this->pobierzWynikBezStronicowania();
	}
    }

    public function pobierzWynikZeStronicowaniem() {
	$paginatorBuilder = \Pastmo\Wspolne\Builder\PaginatorBuilder::create()
		->setOrder($this->order)
		->setStrona($this->builder->strona)
		->setWhere($this->whereWyszukiwania)
		->setWynikowNaStronie($this->builder->wynikowNaStronie)
		->setGroupBy($this->builder->groupBy)
	;

	return $this->builder->table->pobierzPaginator($paginatorBuilder);
    }

    public function pobierzWynikBezStronicowania() {
	if ($this->order) {
	    $wynik = $this->builder->table->pobierzResultSetZWherem($this->whereWyszukiwania, $this->order);
	} else {
	    $wynik = $this->builder->table->pobierzResultSetZWherem($this->whereWyszukiwania);
	}
	return $wynik;
    }

    protected function returnSuccess($param = array()) {
	return $this->returnFailSucces($param, true);
    }

    protected function returnFail($param = array()) {
	return $this->returnFailSucces($param, false);
    }

    private function returnFailSucces($param, $succes) {
	$successArray = array('success' => $succes);

	$result = array_merge($successArray, $param);
	return $this->returnJsonModel($result);
    }

    protected function returnJsonModel($param) {
	return new JsonModel($param);
    }

    public function pobierzNiepustyZGeta($klucz, $domyslnie) {
	$wynik = $this->params()->fromQuery($klucz, $domyslnie);

	if ($wynik === '') {
	    return $domyslnie;
	}
	return $wynik;
    }

    public function sprawdzUprawnienie($kod, $czyWyjatek = false) {
	$wynik = $this->uprawnieniaMenager->sprawdzUprawnienie($kod, $czyWyjatek);
	return $wynik;
    }

    public function ustawModalLayout() {
	$this->layout()->setTemplate('/pastmo/layout/modal_layout');
    }

    public function ustawBialyLayout() {
	$this->layout()->setTemplate('/layout/bialy_layout');
    }

    public function ustawLogowanieLayout() {
	$this->layout()->setTemplate('logowanie/layout/logowanie_layout');
    }

    public function ustawLayoutSzablonuEmail() {
	$this->layout()->setTemplate('/email/layout/email');
    }

}
