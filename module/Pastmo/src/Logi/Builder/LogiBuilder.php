<?php

namespace Pastmo\Logi\Builder;

class LogiBuilder {

    public $kodKategorii;
    public $tresc = array();
    public $ignorujZalogowanego = false;

    public static function getBuilder() {
	return new LogiBuilder();
    }

    public function setKodKategorii($kodKategorii) {
	$this->kodKategorii = $kodKategorii;
	return $this;
    }

    public function setIgnorujZalogowanego($ignorujZalogowanego) {
	$this->ignorujZalogowanego = $ignorujZalogowanego;
	return $this;
    }

    public function addTresc($klucz, $tresc) {
	$this->tresc[$klucz] = $tresc;
	return $this;
    }

    public function dodajTresc($tresc) {
	$this->tresc[] = $tresc;
	return $this;
    }

}
