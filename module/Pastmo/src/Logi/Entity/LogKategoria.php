<?php

namespace Pastmo\Logi\Entity;

class LogKategoria extends \Wspolne\Model\WspolneModel {

    public $kod;
    public $nazwa;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->kod = $this->pobierzLubNull($data, 'kod');
	$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
