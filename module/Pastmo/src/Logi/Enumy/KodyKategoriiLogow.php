<?php

namespace Pastmo\Logi\Enumy;

class KodyKategoriiLogow {

    const EDYCJA_STANOW_MAGAZYNOWYCH = "baza_produ";
    const EDYCJA_PARAMETROW_PRODUKCJI = "log_produk";
    const STATYSTYKI_STANOW_MAGAZYNOWYCH = "sta_mag";
    const SUMA_CEN_ZAKUPU_PRODUKTOW = "sta_cenz";

}
