<?php

namespace Pastmo\Logi\Menager;

class LogiKategorieMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    protected $wyszukiwanieZKontem = false;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Logi\KonfiguracjaModulu::LOGI_KATEGORIE_GATEWAY);
    }

}
