<?php

namespace Pastmo\Logi\Menager;

use \Pastmo\Logi\Enumy\KodyKategoriiLogow;
use Pastmo\Logi\Builder\LogiBuilder;
use Pastmo\Wspolne\Utils\EntityUtil;
use Pastmo\Wspolne\Utils\DateUtil;

class LogiMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public $czasPracy;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Logi\KonfiguracjaModulu::LOGI_GATEWAY);
    }

    public static function getBuilder() {
	return LogiBuilder::getBuilder();
    }

    public function dodajLog(LogiBuilder $builder) {
	$log = new \Pastmo\Logi\Entity\Log();

	if (!$builder->ignorujZalogowanego) {
	    $zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();
	    $log->uzytkownik = $zalogowany ? $zalogowany : null;
	}

	$log->kategoria = $builder->kodKategorii;
	$log->tresc = json_encode($builder->tresc);
	$this->zapisz($log);
    }

    public function getLogiDlaKategori($kodKategori, $order = self::DOMYSLNY_ORDER) {
	return $this->pobierzZWherem("kod_kategorii='$kodKategori'", $order);
    }

    public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1) {
	$this->listeners[] = $events->attach('zapisz', [$this, 'obsluzEventLogow']);
	$this->listeners[] = $events->attach('update', [$this, 'obsluzEventLogow']);
    }

    public function obsluzEventLogow(\Zend\EventManager\EventInterface $e) {
	$event = $e->getName();
	$params = $e->getParams();

	if ($params instanceof \BazaProduktow\Entity\ProduktMagazyn) {
	    if ($event === 'update') {
		$this->dodajLogEventuUpdate($params);
	    } elseif (!empty($params->produkt_wariacja->id) && !empty($params->magazyn->id)) {
		$this->dodajLogEventuUpdate($params);
	    } elseif (!empty($params->produkt_wariacja) && !empty($params->magazyn)) {
		$this->dodajLogEventuZapisz($params);
	    }
	}
    }

    private function dodajLogEventuUpdate($params) {
	if (empty($params->ilosc)) {
	    $params->ilosc = 0;
	}
	$builder = LogiBuilder::getBuilder()
		->setKodKategorii(KodyKategoriiLogow::EDYCJA_STANOW_MAGAZYNOWYCH)
		->addTresc('kod', $params->produkt_wariacja->produkt . '')
		->addTresc('wariacja', $params->produkt_wariacja . '')
		->addTresc('magazyn', $params->magazyn->nazwa)
		->addTresc('ilosc', $params->ilosc);
	$this->dodajLog($builder);
    }

    //TODO: Metodę trzeba przenieść do modułu z GGerp (żeby nie było w Pastmo)
    private function dodajLogEventuZapisz($params) {
	$produktyWariacjeMenager = $this->sm->get(\BazaProduktow\Menager\ProduktyWariacjeMenager::class);
	$magazynyMenager = $this->sm->get(\Magazyn\Menager\MagazynyMenager::class);

	if (!EntityUtil::czyEncjaZId($params->produkt_wariacja)) {
	    return;
	}
	$produktyWariacje = $produktyWariacjeMenager->pobierzZWherem("id = $params->produkt_wariacja->id");
	foreach ($produktyWariacje as $produktWariacja) {
	    if (empty($params->ilosc)) {
		$params->ilosc = 0;
	    }
	    $builder = LogiBuilder::getBuilder()
		    ->setKodKategorii(KodyKategoriiLogow::EDYCJA_STANOW_MAGAZYNOWYCH)
		    ->addTresc('kod', $produktWariacja->produkt . '')
		    ->addTresc('wariacja', $produktWariacja . '')
		    ->addTresc('ilosc', $params->ilosc);
	    $magazyny = $magazynyMenager->pobierzZWherem("id = $params->magazyn");
	    if (!empty($magazyny)) {
		$builder->addTresc('magazyn', $magazyny[0]->nazwa);
	    }
	    $this->dodajLog($builder);
	}
    }

    public function dodajLogEdycjaParametryProdukcji($dane) {
	$dodany = $this->przygotujDane($dane);
	$builder = LogiBuilder::getBuilder()
		->setKodKategorii(KodyKategoriiLogow::EDYCJA_PARAMETROW_PRODUKCJI)
		->addTresc('nazwa_projektu',
			EntityUtil::wydobadzPole(EntityUtil::wydobadzPole($dodany->projekt_produkt_wariacja, 'projekt'), 'nazwa') . '')
		->addTresc('czas_pracy', $dodany->czas_pracy . '')
		;
	$this->dodajLog($builder);
    }

    private function przygotujDane($dodany) {
	$projekt_produkt_wariacja = $dodany->projekt_produkt_wariacja;
	if (is_int($projekt_produkt_wariacja)) {
	    $projektyProduktWariacjaMenager = $this->sm->get(\Projekty\Menager\ProjektyProduktyWariacjeMenager::class);
	    $dodany->projekt_produkt_wariacja = $projektyProduktWariacjaMenager->getRekord($projekt_produkt_wariacja);
	}
	return $dodany;
    }

    private function czyUrzadzenie($wlasciciel) {
	if ($wlasciciel instanceof \Logowanie\Model\Uzytkownik) {
	    return false;
	}
	return true;
    }

    private function dodajProjektProduktWariacjaDoLoga($post, &$log) {
	$log->projekt_produkt_wariacja = $this->sm->get(\Projekty\Menager\ProjektyProduktyWariacjeMenager::class);
	$projektyMenager = $this->get(\Projekty\Menager\ProjektyMenager::class);
	$log->projekt_produkt_wariacja->projekt = $projektyMenager->getRekord($post['project_id']);
    }

}
