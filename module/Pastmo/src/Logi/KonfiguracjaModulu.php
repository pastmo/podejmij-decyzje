<?php

namespace Pastmo\Logi;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class KonfiguracjaModulu extends \Pastmo\KonfiguracjaModulu {

    const LOGI_GATEWAY = 'LOGI_GATEWAY';
    const LOGI_KATEGORIE_GATEWAY = 'LOGI_KATEGORIE_GATEWAY';

    public function getFactories() {
	return array(
		Menager\LogiMenager::class => function($sm) {
		    $table = $sm->get(\Wspolne\Menager\LogiMenager::class);
		    return $table;
		},
		Menager\LogiKategorieMenager::class => function($sm) {

		    $table = new Menager\LogiKategorieMenager($sm);
		    return $table;
		},
		self::LOGI_GATEWAY => function ($sm) {
		    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
		    $resultSetPrototype = new ResultSet();
		    $resultSetPrototype->setArrayObjectPrototype(new Entity\Log($sm));
		    return new TableGateway('logi', $dbAdapter, null, $resultSetPrototype);
		},
		self::LOGI_KATEGORIE_GATEWAY => function ($sm) {
		    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
		    $resultSetPrototype = new ResultSet();
		    $resultSetPrototype->setArrayObjectPrototype(new Entity\LogKategoria($sm));
		    return new TableGateway('logi_kategorie', $dbAdapter, null, $resultSetPrototype);
		},
	);
    }

}
