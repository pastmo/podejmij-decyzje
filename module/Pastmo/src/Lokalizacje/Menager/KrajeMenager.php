<?php

namespace Pastmo\Lokalizacje\Menager;

class KrajeMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Lokalizacje\KonfiguracjaModulu::KRAJE_GATEWAY);
    }

    public function zwrocTablicePanstw() {
	$kraje = $this->pobierzWszystkoArray("kod ASC");
	return $kraje;
    }

    public function pobierzKraj($kod) {
	if (!empty($kod)) {
	    return $this->getRekordKlucz('kod', $kod);
	} else {
	    new \Pastmo\Lokalizacje\Entity\Kraj();
	}
    }

}
