<?php

namespace Pastmo\Lokalizacje;

class KonfiguracjaModulu extends \Pastmo\KonfiguracjaModulu {

    const KRAJE_GATEWAY = "KrajeGateway";

    public function onBootstrap(\Zend\Mvc\MvcEvent $e) {
	parent::onBootstrap($e);

	$viewHelperMenager = $e->getApplication()->getServiceManager()->get('ViewHelperManager');
	$viewHelperMenager->setFactory('ustawLokalizacjeJs',
		function($sm) use ($e) {
	    $viewHelper = new Helper\UstawLokalizacjeJs($sm);
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('wyswietlSelectKrajow',
		function($sm) use ($e) {
	    $viewHelper = new Helper\WyswietlSelectKrajow($sm);
	    return $viewHelper;
	});
    }

    public function getFactories() {
	return [
		Menager\KrajeMenager::class => function($sm) {
		    return new Menager\KrajeMenager($sm);
		},
		self::KRAJE_GATEWAY => function ($sm) {
		    return $this->ustawGateway($sm, new Entity\Kraj($sm), 'kraje');
		},
	];
    }

}
