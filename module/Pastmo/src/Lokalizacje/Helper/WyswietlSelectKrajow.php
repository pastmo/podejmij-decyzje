<?php

namespace Pastmo\Lokalizacje\Helper;

class WyswietlSelectKrajow extends \Pastmo\Wspolne\Helper\TranslateHelper {

    private $krajeMenager;

    public function __construct($sm) {
	parent::__construct($sm);
	$this->krajeMenager = $this->sm->get(\Pastmo\Lokalizacje\Menager\KrajeMenager::class);
    }

    public function __invoke($wybranyKodKraju = "", $nazwaPola = "kraj_kod", $klasaSelecta = "") {

	$kraje = $this->krajeMenager->zwrocTablicePanstw();
	?>
	<select class="form-control select-kraju <?php echo $klasaSelecta; ?>" name="<?php echo $nazwaPola ?>">
	    <option value=""></option>
	    <?php
	    foreach ($kraje as $kraj):
		?>
	        <option value="<?php echo $kraj->kod; ?>"
		<?php if ($kraj->kod === $wybranyKodKraju) {
		    echo "selected";
		} ?>
			><?php echo $this->translateConst($kraj->nazwa); ?></option>
	<?php endforeach; ?>
	</select>
	<?php
    }

}
