<?php

namespace Pastmo\Lokalizacje\Helper;

use Pastmo\Wspolne\Utils\StrUtil;

class UstawLokalizacjeJs extends \Pastmo\Wspolne\Helper\TranslateHelper {

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	parent::__construct($sm);
    }

    public function __invoke() {
	$locale = $this->translator->getLocale();
	?>
	<script>var locale = '<?php echo \Pastmo\Wspolne\Utils\StrUtil::skrocStr($locale, 3); ?>';</script>
	<?php
    }

}
