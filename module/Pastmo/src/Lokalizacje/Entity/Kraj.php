<?php

namespace Pastmo\Lokalizacje\Entity;

class Kraj extends \Wspolne\Model\WspolneModel {

    public $kod;
    public $nazwa;
    public $nazwa_pelna;
    public $data_dodania;
    public $data_aktualizacji;
    public $etykiety;

    public function exchangeArray($data) {
	$this->kod = $this->pobierzLubNull($data, 'kod');
	$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
	$this->nazwa_pelna = $this->pobierzLubNull($data, 'nazwa_pelna');
	$this->data_dodania = $this->pobierzLubNull($data, 'data_dodania');
	$this->data_aktualizacji = $this->pobierzLubNull($data, 'data_aktualizacji');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	return $nazwaWKodzie;
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public function __toString() {
	return $this->nazwa;
    }

    public function pobierzEtykietyKraju() {
	$adresyEtykietyMenager = $this->sm->get(\Wysylka\Menager\AdresyEtykietMenager::class);
	$this->etykiety = $adresyEtykietyMenager->pobierzEtykietyKraju($this);
	return $this->etykiety;
    }

}
