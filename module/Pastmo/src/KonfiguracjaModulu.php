<?php

namespace Pastmo;

abstract class KonfiguracjaModulu {

    use FunkcjeKonfiguracyjne;

    const JEZYKI_GATEWAY = "JEZYKI_GATEWAY";

    public function onBootstrap(\Zend\Mvc\MvcEvent $e) {

    }

    abstract public function getFactories();

    public static function dodajKontrolery($array) {
	return $array;
    }

    public static function ustawLocale($sm){

	$translator = $sm->get('MvcTranslator');

	$logowanieFasada = $sm->get(\Logowanie\Menager\UzytkownicyMenager::class);
	$user = $logowanieFasada->getZalogowanegoUsera();

	if ($user && $user->slownik->jezyk_kod) {
	    $translator->setLocale($user->slownik->jezyk_kod);
	}
    }

}
