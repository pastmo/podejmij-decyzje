<?php
namespace Pastmo\Testy\Util;

class ObslugaTestowanejKlasy extends AbstractObsluga {

    public function ustawWynikGetRekord($obiektWynikowy, $gateway = null,
	    $exacly = 1) {

	$rowset = $this->getMockBuilder('WspolneTest\Mocki\RowSetMock')
		->disableOriginalConstructor()
		->getMock();
	$rowset->expects($this->exactly($exacly))
		->method('current')
		->will($this->returnValue(
				$obiektWynikowy
	));

	$gateway->expects($this->exactly($exacly))
		->method('select')
		->will($this->returnValue(
				$rowset
	));
    }

    public function ustawSelectWithCount($count, $exacly = 1) {
	$this->ustawCountRowSet($count, $exacly);
	$this->abstractMoctTest->gateway->expects($this->exactly($exacly))
		->method('selectWith')
		->will($this->returnValue(
				$this->abstractMoctTest->rowset
	));
    }

    public function ustawCountRowSet($count, $exacly = 1) {
	$this->ustawSqlMocka();

	$this->abstractMoctTest->rowset->expects($this->exactly($exacly))
		->method('count')
		->will($this->returnValue(
				$count
	));

	return $this->abstractMoctTest->rowset;
    }

    public function getPoprzednioDodany($wynik, $gateway, $ileRazy = 1) {
	$this->ustawWynikGetRekord($wynik, $gateway, $ileRazy);
	$this->ustawGetLastInsertValue(1, $ileRazy);
    }

    public function ustawGetLastInsertValue($wynik, $ileRazy = 1) {
	$this->ustawSqlMocka();
	$this->abstractMoctTest->gateway->expects($this->exactly($ileRazy))->method('getLastInsertValue')->will($this->returnValue($wynik));
    }

    public function ustawMockiDoPobierzZWherem($wynik, $ileRazy = 1) {
	$this->ustawSqlMocka();
	return $this->abstractMoctTest->gateway->expects($this->exactly($ileRazy))->method('selectWith')->will($this->returnValue($wynik));
    }

    public function ustawSqlMocka() {

	$this->abstractMoctTest->gateway->expects($this->any())
		->method('getSql')
		->will($this->returnValue(
				$this->abstractMoctTest->sqlMock
	));

	$this->abstractMoctTest->sqlMock->expects($this->any())
		->method('select')
		->will($this->returnValue(
				$this->abstractMoctTest->selectMock
	));
    }

    public function ustawSprawdzanieDelete($ileRazy) {
	$this->abstractMoctTest->gateway->expects($this->exactly($ileRazy))->method('delete');
    }

    public function ustawieniaPotrzebneDoUpdate($obiektWynikowy, $gateway) {
	$this->ustawWynikGetRekord($obiektWynikowy, $gateway);
    }

    public function ustawSprawdzanieUpdate($ileRazy) {
	$mockBuilder = $this->abstractMoctTest->gateway->expects($this->exactly($ileRazy))->method('update');
	return $mockBuilder;
    }

    public function ustawSprawdzanieInsert($ileRazy) {
	$mockBuilder = $this->abstractMoctTest->gateway->expects($this->exactly($ileRazy))->method('insert');
	return $mockBuilder;
    }

    public function mockujUnikalnaNazweProjektu() {
	$this->ustawSelectWithCount(0);
    }

}
