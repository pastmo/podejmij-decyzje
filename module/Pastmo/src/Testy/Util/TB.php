<?php

namespace Pastmo\Testy\Util;

class TB {

    const FABRYKA_REKORDOW = \FabrykaRekordow::class;
    const FABRYKA_MOCKOW = \FabrykaEncjiMockowych::class;

    public $pf;
    public $defaultPf;
    public $factoryClass;
    public $entityClass;
    public $parameters = array();
    public $resultField = 'lastEntity';
    public $oneToMany = array();
    public $runer;
    public $result;

    public static function create($class, $runner) {
	$builder = new TB();
	$builder->setDefaultPf($runner->defaultFactoryParameters);
	$builder->setEntityClass($class);
	$builder->runer = $runner;

	return $builder;
    }

    public function make() {

	$this->makeOneToManyEntities();
	$this->doMyEntity();
	$this->setResultToClientClass();

	return $this->result;
    }

    private function makeOneToManyEntities() {
	foreach ($this->oneToMany as $key => $builder) {
	    $this->parameters[$key] = $builder->make();
	}
    }

    private function doMyEntity() {
	$factory = $this->getFactory();
	$pf = $this->getPf();
	$this->result = $factory::makeEncje($this->entityClass, $pf->setparametry($this->parameters));
    }

    private function setResultToClientClass() {
	$field = $this->resultField;
	$this->runer->$field = $this->result;
    }

    private function getFactory() {
	if ($this->factoryClass) {
	    return $this->factoryClass;
	} else
	if (in_array(get_class($this->getPf()),
			array(\Pastmo\Testy\ParametryFabryki\PFMockowePowiazane::class, \Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::class))) {
	    return self::FABRYKA_MOCKOW;
	} else {
	    return self::FABRYKA_REKORDOW;
	}
    }

    public function getPf() {
	if ($this->pf) {
	    return $this->pf;
	} else {
	    return $this->defaultPf;
	}
    }

    public function setPF_MP($sm) {
	$this->setPf(\Pastmo\Testy\ParametryFabryki\PFMockowePowiazane::create($sm));
	return $this;
    }

    public function setPF_IBN($sm = null) {
	$this->setPf(\Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuNiepowiazane::create($sm));
	return $this;
    }

    public function setPF_IBP($sm) {
	$this->setPf(\Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuPowiazane::create($sm));
	return $this;
    }

    public function setPF_IZP($sm) {
	$this->setPf(\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($sm));
	return $this;
    }

    public function setPf($pf) {
	$this->pf = $pf;
	return $this;
    }

    public function addOneToMany($key, $oneToMany) {
	$this->oneToMany[$key] = $oneToMany;
	return $this;
    }

    public function setDefaultPf($defaultPf) {
	$this->defaultPf = $defaultPf;
	return $this;
    }

    public function setFactoryClass($factoryClass) {
	$this->factoryClass = $factoryClass;
	return $this;
    }

    public function setEntityClass($entityClass) {
	$this->entityClass = $entityClass;
	return $this;
    }

    public function setParameters(array $parameters) {
	$this->parameters = $parameters;
	return $this;
    }

    public function setResultField($resultField) {
	$this->resultField = $resultField;
	return $this;
    }

    public function setRuner($runer) {
	$this->runer = $runer;
	return $this;
    }

}
