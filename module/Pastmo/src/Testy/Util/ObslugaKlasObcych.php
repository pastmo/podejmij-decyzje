<?php

namespace Pastmo\Testy\Util;

class ObslugaKlasObcych extends AbstractObsluga {

    public $zalogowany;
    private $uprawnieniaUzytkownika = array();
    public $pierwszeSprawdzenieWyjatku;
    public $dzialajaceUprawnienia;

    public function ustawZalogowanegoUsera($user = null, $ileRazy = 1) {
	$this->zalogowany = $user;

	if (!$this->zalogowany) {
	    $this->zalogowany = \FabrykaEncjiMockowych::makeEncje(\Logowanie\Model\Uzytkownik::class,
			    \Pastmo\Testy\ParametryFabryki\PFMockowePowiazane::create($this->abstractMoctTest->sm));
	}

	if ($ileRazy = -1) {
	    $expects = $this->any();
	} else {
	    $expects = $this->exactly($ileRazy);
	}

	$this->abstractMoctTest->uzytkownikTable->expects($expects)
		->method('getZalogowanegoUsera')
		->will($this->returnValue($this->zalogowany));

	$this->abstractMoctTest->uzytkownikTable->expects($this->any())->method('getRekord')->willReturn($this->zalogowany);
    }

    public function ustawWynikSprawdzaniaUpawnienia($wynik) {
	$this->abstractMoctTest->uprawnieniaMenager
		->expects($this->any())
		->method('sprawdzUprawnienie')
		->willReturn($wynik);
    }

    public function ustawWynikSprawdzaniaUprawnienieUzytkonika($wynik) {
	$this->abstractMoctTest->uprawnieniaMenager
		->expects($this->any())
		->method('sprawdzUprawnienieUzytkownika')
		->willReturn($wynik);
    }

    public function ustawUprawnieniaUzytkownika(array $tablicaUprawnien = array(), $wyjatek = false) {
	$this->uprawnieniaUzytkownika = $tablicaUprawnien;

	$builder = $this->abstractMoctTest->uprawnieniaMenager
		->expects($this->any())
		->method('sprawdzUprawnienie');


	if ($wyjatek) {
	    $builder->will($this->returnCallback(array($this, 'wynikSprawdzaniaUprawnienieZWyatkiem')));
	} else {
	    $builder->will($this->returnCallback(array($this, 'wynikSprawdzaniaUprawnienia')));
	}
    }

    public function wynikSprawdzaniaUprawnienieZWyatkiem($kod) {
	if (!$this->pierwszeSprawdzenieWyjatku) {
	    return;
	}
	$this->pierwszeSprawdzenieWyjatku = false;

	$result = $this->wynikSprawdzaniaUprawnienia($kod);
	$this->dzialajaceUprawnienia = $result;

	throw new \Pastmo\Uzytkownicy\Exception\UprawnieniaException('Testy uprawnień kontrolera');
    }

    public function wynikSprawdzaniaUprawnienia($kod) {
	$result = in_array($kod, $this->uprawnieniaUzytkownika);
	return $result;
    }

    public function ustawGetRecord($menagerClass, $wynik) {
	$this->abstractMoctTest->sm
		->get($menagerClass)
		->expects($this->any())
		->method('getRekord')
		->will($this->returnValue($wynik));
    }

    public function ustawPobierzZWherem($menagerClass, $wynik) {
	$this->abstractMoctTest->sm
		->get($menagerClass)
		->expects($this->any())
		->method('pobierzZWherem')
		->will($this->returnValue($wynik));
    }

}
