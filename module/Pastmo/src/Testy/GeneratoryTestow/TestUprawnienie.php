<?php

namespace Pastmo\Testy\GeneratoryTestow;

class TestUprawnienie {

    const KOD_DOMYSLNY = 'KOD_DOMYSLNY';

    public $kodUprawnienia;
    public $czyAjax;
    public $akcja;

    public static function create($kodUprawnienia, $akcja, $czyAjax) {
	$tu = new TestUprawnienie();
	$tu->kodUprawnienia = $kodUprawnienia;
	$tu->czyAjax = $czyAjax;
	$tu->akcja = $akcja;
	return $tu;
    }

    public static function domyslneUprawnienie($akcja, $czyAjax) {
	$tu = new TestUprawnienie();
	$tu->kodUprawnienia = self::KOD_DOMYSLNY;
	$tu->czyAjax = $czyAjax;
	$tu->akcja = $akcja;
	return $tu;
    }

    public function getAkcje() {
	return $this->akcja;
    }

}
