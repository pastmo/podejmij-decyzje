<?php

namespace Pastmo\Testy\GeneratoryTestow;

use \Pastmo\Zasoby\PrzeszukiwaniePlikow\PrzeszukiwaczPlikow;

class GeneratorTestowUprawnien extends AbstrakcyjnyGeneratorTestow {

    private $przeszukiwacz;
    private $pobieraczAkcjiKontrolera;
    public $mapaUprawnien;
    private $ignorowane = false;

    public function __construct($sm) {
	$this->mapaUprawnien = new \Testy\GeneratoryTestow\MapaUprawnien();
	$this->pobieraczAkcjiKontrolera = new \Pastmo\Wspolne\Menager\PobieraczAkcjiZKlasyKontrolera($sm);
    }

    public function generujWlasciweMetody() {
	$this->ustawPrzeszukiwacz();

	$akcjePostAjax = $this->przeszukaj(PrzeszukiwaczPlikow::WYSLIJ_POST);
	$this->generujTestyUprawnieniaJson($akcjePostAjax);

	$akcjeGetAjax = $this->przeszukaj(PrzeszukiwaczPlikow::WYSLIJ_GET);
	$this->generujTestyUprawnieniaJson($akcjeGetAjax);

	$akcjeZKontrolerow = $this->pobierzAkcjeZKontrolerow();
	$this->generujTestyUprawnienia($akcjeZKontrolerow);
    }

    private function ustawPrzeszukiwacz() {
	$this->przeszukiwacz = PrzeszukiwaczPlikow::create()
		->setBasePath(dirname(__FILE__) . '/../../../../../public_html/js')
	;
    }

    private function przeszukaj($searched) {
	$akcjeAjax = $this->przeszukiwacz
		->setSearched($searched)
		->przeszukaj();
	$akcjeAjax = array_unique($akcjeAjax);
	return $akcjeAjax;
    }

    private function generujTestyUprawnienia($array) {
	foreach ($array as $akcja) {
	    $this->zrobTestyPozytywne($akcja);
//	    $this->zrobTestyNegatywne($akcja);
	}
    }

    private function generujTestyUprawnieniaJson($array) {
	foreach ($array as $akcja) {
	    $this->zrobTestyPozytywneJson($akcja);
//	    $this->zrobTestyNegatywneJson($akcja);
	}
    }

    private function zrobTestyPozytywneJson($akcja) {
	if ($this->mapaUprawnien->czyIgnorowanaAkcja($akcja)) {
	    return;
	}
	$testUprawnienie = $this->mapaUprawnien->pobierzUprawnienie($akcja, true);

	$this->zrobTestyPozytywneWspolne($akcja, 'pozytywneSprawdzanieUprawnienJson',
		"'{$testUprawnienie->kodUprawnienia}'", 'pozytywneJson');
    }

    private function zrobTestyPozytywne($akcja) {
	if ($this->mapaUprawnien->czyIgnorowanaAkcja($akcja)) {
	    return;
	}
	$testUprawnienie = $this->mapaUprawnien->pobierzUprawnienie($akcja, false);

	$this->zrobTestyPozytywneWspolne($akcja, 'pozytywneSprawdzanieUprawnien',
		"'{$testUprawnienie->kodUprawnienia}'", 'pozytywne');
    }

    private function zrobTestyPozytywneWspolne($akcja, $metoda, $uprawnieie, $typ) {

	$this->dodajMetode($this->zrobNazweMetodyTestowejZAkcji($akcja . "_$typ"
		), '$this->' . $metoda . '("' . $akcja . '",
		array(' . $uprawnieie . '));');
    }

    private function pobierzAkcjeZKontrolerow() {
	$result = array();
	foreach ($this->mapaUprawnien->klasyKontrolerow as $klasa) {
	    $aktualneAkcje = $this->pobieraczAkcjiKontrolera->pobierzZwyklaAkcjeZKontrolera($klasa);
	    $result = array_merge($result, $aktualneAkcje);
	}
	return $result;
    }

    public function getNamespace() {
	return 'WspolneTest\Controller';
    }

    public function getNazwaKlasy() {
	return 'UprawnieniaTest';
    }

    public function getPathPlikuWynikowego() {
	return dirname(__FILE__) . '/../../../../Wspolne/test/Controller/UprawnieniaTest.php';
    }

    public function wyswietlPodsumowanie() {
	if (count($this->mapaUprawnien->nieznalezioneUprawnienia) > 0) {

	    $this->mapaUprawnien->nieznalezioneUprawnienia = \Pastmo\Wspolne\Utils\ArrayUtil::sortujNapisy($this->mapaUprawnien->nieznalezioneUprawnienia,
			    'getAkcje');

	    echo PHP_EOL . PHP_EOL . "Akcje bez przypisanego kodu:" . PHP_EOL . PHP_EOL;

	    foreach ($this->mapaUprawnien->nieznalezioneUprawnienia as $testUprawnienie) {
		$ajax = $testUprawnienie->czyAjax ? 'true' : 'false';
		$domyslnyKod = TestUprawnienie::KOD_DOMYSLNY;
		echo "'{$testUprawnienie->akcja}'=> TestUprawnienie::create('$domyslnyKod','{$testUprawnienie->akcja}',{$ajax})," . PHP_EOL;
	    }
	}
    }

}
