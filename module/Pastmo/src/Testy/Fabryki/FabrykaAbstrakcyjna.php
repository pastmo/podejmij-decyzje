<?php

namespace Pastmo\Testy\Fabryki;

use \Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane;
use \Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane;

abstract class FabrykaAbstrakcyjna {

    protected $encja;
    protected $parametryFabryki;

    const DZIEN = 86400000;

    public function __construct(\Pastmo\Testy\ParametryFabryki\ParametryFabryki $parametryFabryki = null) {
	$this->parametryFabryki = $parametryFabryki;
    }

    public function makeEncje() {

	return $this->utworzEncje(null);
    }

    public function utworzEncje($sm, $zapiszDoBazy = true, $parametry = array()) {
	if (!$this->parametryFabryki) {
	    $this->parametryFabryki = new \Pastmo\Testy\ParametryFabryki\ParametryFabryki();
	    $this->parametryFabryki->setSm($sm)
		    ->setzapiszDoBazy($zapiszDoBazy)
		    ->setczyGenerowaniePowiazanychEncji(true)
		    ->setparametry($parametry)
		    ->setczyFabrykaMockow($sm === NULL);
	}

	$this->encja = $this->nowaEncja();

	if ($this->parametryFabryki->czyGenerowaniePowiazanychEncji) {
	    $this->dowiazInneTabele();
	}

	$this->dodajParamtery($this->encja, $this->parametryFabryki->parametry);

	if ($this->parametryFabryki->zapiszDoBazy) {
	    $table = $this->parametryFabryki->sm->get($this->pobierzNazweTableMenager());
	    $table->zapisz($this->encja);
	    $this->encja = $table->getPoprzednioDodany();
	}



	return $this->encja;
    }

    private function dodajParamtery(&$encja, $parametry) {
	while (list($key, $value) = each($parametry)) {
	    $encja->$key = $value;
	}
    }

    protected function getUnikalyNapis($przedrostek) {
	if ($this->parametryFabryki->sm && !$this->parametryFabryki->czyFabrykaMockow) {
	    $menager = $this->parametryFabryki->sm->get($this->pobierzNazweTableMenager());
	    $iloscZapisanych = $menager->pobierzZWheremCount('1');
	} else {
	    $iloscZapisanych = self::genrujId();
	}

	$nazwa = $przedrostek . $iloscZapisanych;
	return $nazwa;
    }

    public static function genrujId() {
	return rand(1, 1000);
    }

    public abstract function pobierzNazweTableMenager();

    public abstract function nowaEncja();

    public function dowiazInneTabele() {

    }

    protected function uzyjWlasciwejFabryki($klasa, $parametry = array()) {
	if ($this->parametryFabryki->czyFabrykaMockow) {
	    return \FabrykaEncjiMockowych::makeEncje($klasa,
			    PFMockoweNiepowiazane::create($this->parametryFabryki->sm)->setparametry($parametry));
	} else {
	    return \FabrykaRekordow::makeEncje($klasa,
			    PFIntegracyjneZapisNiepowiazane::create($this->parametryFabryki->sm)->setparametry($parametry));
	}
    }

    public function generujDate($przesuniecie = 0) {
	$int = mt_rand(1462055681 + $przesuniecie, 1472055681 + $przesuniecie);
	$string = date("Y-m-d H:i:s", $int);
	return $string;
    }

    protected function getSm() {
	return $this->parametryFabryki->sm;
    }

}
