<?php

namespace Pastmo\Testy\BazoweKlasyTestow;

use Zend\Stdlib\Parameters;
use Pastmo\Testy\Util\TB;

abstract class WspolnyControllerCommonTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    protected $traceError = true;
    protected $fabrykaControllerow;

    public function setUp() {

	parent::setUp();

	$_SERVER['HTTP_HOST'] = 'localhost';

	$this->traceError = true;
	$this->ustawMockiUstawienSystemu();

	$this->uprawnieniaMenager->expects($this->any())->method('pobierzUprawnieniaDlaRoli')->willReturn(array());
    }

    public function testIndex() {
	$this->dispatch('/' . $this->getSmallName());

	$this->sprawdzCzyStatus200();
	$this->sprawdzModulIKontroller();
	$this->sprawdzRouteName($this->getSmallName());
    }

    public function t_estPrzekierowaniePrzyNiezalogwanym() {//TODO: Wymyślić sposób na sprawdzenie tego
	try {
	    $this->throwExceptions(true);
	    $this->uprawnieniaMenager
		    ->method('sprawdzUprawnienie')
		    ->will(
			    $this->throwException(
				    new \Pastmo\Uzytkownicy\Exception\LogowanieException("Test niezalogowany")
			    )
	    );

	    $this->dispatch('/' . $this->getSmallName());
	    $this->fail('Brak wyjątku');
	} catch (\Pastmo\Uzytkownicy\Exception\LogowanieException $e) {

	} catch (\Exception $e) {
	    $this->fail('Nieprawidłowy wyjątek:' . $e->getMessage());
	}
    }

    protected function sprawdzModulIKontroller() {
	$this->assertModuleName($this->getBigName());
	$this->assertControllerName($this->getBigName() . '\Controller\\' . $this->getBigName());
	$this->assertControllerClass($this->getBigName() . 'Controller');
    }

    public function dodajDoPosta($key, $value) {
	$_POST[$key] = $value;
	$_REQUEST[$key] = $value;
    }

    public function getSmallName() {
	throw new \Exception("Należy przesłonić tę metodę");
    }

    public function getBigName() {
	throw new \Exception("Należy przesłonić tę metodę");
    }

    protected function ustawParametryGeta($parametry = array()) {
	$this->getRequest()
		->setMethod('GET')
		->setPost(new Parameters($parametry));
    }

    protected function ustawParametryPosta($parametry = array()) {
	$this->getRequest()
		->setMethod('POST')
		->setPost(new Parameters($parametry));
    }

    protected function ustawMockiUstawienSystemu() {
	$ustawienie = TB::create(\Pastmo\Wspolne\Entity\UstawienieSystemu::class, $this)->make();
	$this->ustawieniaSystemuMenager->expects($this->any())->method('pobierzUstawieniePoKodzie')->willReturn($ustawienie);
    }

    protected function pozytywneSprawdzanieUprawnienJson($url, array $uprawnienia) {
	$this->pozytywneSprawdzanieUprawnien($url, $uprawnienia);
	$this->sprawdzCzyFail(); //Wymuszamy wyjątek, więc musi być success: false
    }

    protected function pozytywneSprawdzanieUprawnien($url, array $uprawnienia) {
	$this->wspolneSprawdzanieUprawnien($url, $uprawnienia);
	$this->assertTrue($this->obslugaKlasObcych->dzialajaceUprawnienia,
		"Pozytywne sprawdzanie uprawnień: " . json_encode($uprawnienia));
    }

    protected function negatywneSprawdzanieUprawnienJson($url, array $uprawnienia) {
	$this->negatywneSprawdzanieUprawnien($url, $uprawnienia);
	$this->sprawdzCzyFail();
    }

    protected function negatywneSprawdzanieUprawnien($url, array $uprawnienia) {
	$this->wspolneSprawdzanieUprawnien($url, $uprawnienia);
	$this->assertFalse($this->obslugaKlasObcych->dzialajaceUprawnienia,
		"Negatywne sprawdzanie uprawnień: " . json_encode($uprawnienia));
    }

    private function wspolneSprawdzanieUprawnien($url, $uprawnienia) {
	$this->obslugaKlasObcych->pierwszeSprawdzenieWyjatku = true;
	$this->obslugaKlasObcych->ustawUprawnieniaUzytkownika($uprawnienia, true);
	$this->dispatch($url);
    }

}
