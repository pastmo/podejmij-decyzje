<?php

namespace Pastmo\Testy\BazoweKlasyTestow;

abstract class AbstractHelperTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    protected $helper;

    public function setUp() {
	parent::setUp();

	$this->utworzHelper();
	$viewRenerer = $this->sm->get('ViewPhpRenderer');
	$this->helper->setView($viewRenerer);
    }

    abstract function utworzHelper();
}
