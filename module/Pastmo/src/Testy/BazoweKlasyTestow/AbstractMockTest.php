<?php

namespace Pastmo\Testy\BazoweKlasyTestow;

class AbstractMockTest extends \Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase {

    use \Pastmo\Testy\TraityPomocnicze\SprawdzMetody,
	\Pastmo\Testy\Util\TestBuilderWrapper,
	\Pastmo\Testy\TraityPomocnicze\StaleWTestach,
	\Pastmo\Testy\TraityPomocnicze\UstawianieParametrowWTestach;

    public $kontaMailoweManager;
    public $pdfMenager;
    public $logiMenager;
    public $zasobyUploadMenager;
    public $zasobyDownloadMenager;
    public $uzytkownicyKontaMailoweMenager;
    public $ustawieniaSystemuMenager;
    public $czasMenager;
    public $sesjaMenager;
    public $tlumaczeniaMenager;
    public $platnosciMenager;
    //
    public $rowset;
    public $sm;
    public $sqlMock;
    //
    public $obslugaTestowanejKlasy;
    public $obslugaKlasObcych;
    public $obslugaEncji;
    public $view;

    public function setUp() {
	$this->ustawIgnorowanyTestZModuluPastmo();
	$this->setApplicationConfig(
		include \Application\Stale::configTestPath
	);
	parent::setUp();

	$this->sm = $this->getApplicationServiceLocator();
	$this->sm->setAllowOverride(true);

	$this->setDefaultFactoryParameters(\Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::create($this->sm));

	$this->obslugaKlasObcych = new \Pastmo\Testy\Util\ObslugaKlasObcych();
	$this->obslugaTestowanejKlasy = new \Pastmo\Testy\Util\ObslugaTestowanejKlasy();
	$this->obslugaEncji = new \Pastmo\Testy\Util\ObslugaEncji();

	$this->obslugaKlasObcych->init($this);
	$this->obslugaTestowanejKlasy->init($this);
	$this->obslugaEncji->init($this);

	$this->view = $this->getMockBuilder('Pastmo\Testy\Mocki\ViewMock')
		->disableOriginalConstructor()
		->getMock();

	$this->czasMenager = $this->getMockBuilder(\Pastmo\Wspolne\Menager\CzasMenager::class)
		->disableOriginalConstructor()
		->getMock();

	$this->kontaMailoweManager = $this->getMockBuilder(\Pastmo\Email\Menager\KontaMailoweMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->logiMenager = $this->getMockBuilder(\Pastmo\Logi\Menager\LogiMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->pdfMenager = $this->getMockBuilder(\Pastmo\Pdf\Menager\PdfMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->zasobyUploadMenager = $this->getMockBuilder(\Zasoby\Menager\ZasobyUploadMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->zasobyDownloadMenager = $this->getMockBuilder(\Zasoby\Menager\ZasobyDownloadMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->uzytkownicyKontaMailoweMenager = $this->getMockBuilder(\Logowanie\Menager\UzytkownicyKontaMailoweMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->ustawieniaSystemuMenager = $this->getMockBuilder(\Pastmo\Wspolne\Menager\UstawieniaSystemuMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->sesjaMenager = $this->getMockBuilder(\Wspolne\Menager\SesjaMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->tlumaczeniaMenager = $this->getMockBuilder(\Pastmo\Tlumaczenia\Menager\TlumaczeniaMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->platnosciMenager = $this->getMockBuilder(\Pastmo\Platnosci\Menager\PlatnosciMenager::class)
		->disableOriginalConstructor()
		->getMock();


	$this->sm->setService(\Pastmo\Wspolne\Menager\CzasMenager::class, $this->czasMenager);

	$this->sm->setService(\Pastmo\Email\Menager\KontaMailoweMenager::getClass(), $this->kontaMailoweManager);

	$this->sm->setService(\Pastmo\Pdf\Menager\PdfMenager::class, $this->pdfMenager);

	$this->sm->setService(\Pastmo\Logi\Menager\LogiMenager::class, $this->logiMenager);
	$this->sm->setService(\Wspolne\Menager\LogiMenager::class, $this->logiMenager);
	$this->sm->setService(\Zasoby\Menager\ZasobyUploadMenager::class, $this->zasobyUploadMenager);
	$this->sm->setService(\Zasoby\Menager\ZasobyDownloadMenager::class, $this->zasobyDownloadMenager);
	$this->sm->setService(\Logowanie\Menager\UzytkownicyKontaMailoweMenager::class,
		$this->uzytkownicyKontaMailoweMenager);
	$this->sm->setService(\Pastmo\Wspolne\Menager\UstawieniaSystemuMenager::class,
		$this->ustawieniaSystemuMenager);
	$this->sm->setService(\Wspolne\Menager\SesjaMenager::class, $this->sesjaMenager);
	$this->sm->setService(\Pastmo\Tlumaczenia\Menager\TlumaczeniaMenager::class, $this->tlumaczeniaMenager);
	$this->sm->setService(\Pastmo\Platnosci\Menager\PlatnosciMenager::class, $this->platnosciMenager);

	$translator = $this->sm->get('MvcTranslator');
	$translator->setLocale(\Pastmo\Uzytkownicy\Enumy\KodyJezykow::POLSKI);
	$this->traceError = true;
    }

    protected function tearDown() {
	$refl = new \ReflectionObject($this);
	foreach ($refl->getProperties() as $prop) {
	    if (!$prop->isStatic() && 0 !== strpos($prop->getDeclaringClass()->getName(), 'PHPUnit_')) {
		$prop->setAccessible(true);
		$prop->setValue($this, null);
	    }
	}
    }

    protected function ustawMockaGateway($name) {
	$this->gateway = $this->getMockBuilder('Zend\Db\TableGateway\TableGateway')
		->disableOriginalConstructor()
		->getMock();
	$this->sm->setService($name, $this->gateway);
    }

}
