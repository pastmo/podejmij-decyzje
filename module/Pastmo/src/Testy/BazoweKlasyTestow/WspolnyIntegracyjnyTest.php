<?php

namespace Pastmo\Testy\BazoweKlasyTestow;

use Zend\Stdlib\ArrayUtils;
use Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane;
use Pastmo\Testy\Util\TB;
use Logowanie\Enumy\KodyUprawnien;

abstract class WspolnyIntegracyjnyTest extends \Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase {

    use \Pastmo\Testy\Util\TestBuilderWrapper,
	\Pastmo\Testy\TraityPomocnicze\SprawdzMetody,
	\Pastmo\Testy\TraityPomocnicze\StaleWTestach,
	\Pastmo\Testy\TraityPomocnicze\UstawianieParametrowWTestach;

    protected $sm;
    protected $logowanieFasada;
    public $projekt;
    protected $ustawioneJednorazowo = false;
    protected $zalogowany;

    public function setUp() {
	$this->ustawIgnorowanyTestZModuluPastmo();
	$configOverrides = [];

	$this->setApplicationConfig(ArrayUtils::merge(
			include \Application\Stale::configTestPath, $configOverrides
	));

	parent::setUp();

	$this->sm = $this->getApplicationServiceLocator();

	$this->setDefaultFactoryParameters(PFIntegracyjneZapisNiepowiazane::create($this->sm));

//	\Zasoby\Menager\ZasobyUploadMenager::$uploadDir = "./public_html/test/";


	$this->logowanieFasada = $this->sm->get(\Logowanie\Fasada\LogowanieFasada::class);

	if (!$this->ustawioneJednorazowo) {
	    $this->ustawJednorazowo();
	    $this->ustawioneJednorazowo = true;
	}
    }

    public function startTransaction() {
	$projectyGateway = $this->sm->get(\Pastmo\Wspolne\KonfiguracjaModulu::USTAWIENIA_SYSTEMU_GATEWAY);
	$projectyGateway->adapter->getDriver()->getConnection()->beginTransaction();
    }

    public function rollbackTransaction() {
	$projectyGateway = $this->sm->get(\Pastmo\Wspolne\KonfiguracjaModulu::USTAWIENIA_SYSTEMU_GATEWAY);
	$projectyGateway->adapter->getDriver()->getConnection()->rollback();
    }

    //TODO: Przenieść do klasy bazowej z GGerp
    protected function getUnikalnaNazwaProjektu() {
	$projectMenager = $this->sm->get(\Projekty\Menager\ProjektyMenager::class);
	$iloscProjektow = $projectMenager->pobierzZWheremCount('1');
	$nazwa = 'nazwa' . $iloscProjektow;
	return $nazwa;
    }

    protected function utworzIZalogujUzytkownika() {
	$this->zalogowany = TB::create(\Logowanie\Model\Uzytkownik::class, $this)->setPF_IZP($this->sm)->make();
	$this->ustawZalogowanegoUsera($this->zalogowany->id);
    }

    protected function utworzIZalogujUzytkownikaZUprawnieniami(array $uprawnienia = [KodyUprawnien::DOMYSLNE_UPRAWNIENIE]) {
	$rola = TB::create(\Logowanie\Entity\Rola::class, $this)->setPF_IZP($this->sm)->make();

	foreach ($uprawnienia as $uprawnienie) {
	    TB::create(\Logowanie\Entity\UprawnienieRola::class, $this)
		    ->setParameters(['rola_id' => $rola->id, 'uprawnienie_kod' => $uprawnienie])
		    ->make();
	}

	$this->zalogowany = TB::create(\Logowanie\Model\Uzytkownik::class, $this)->setPF_IZP($this->sm)
		->setParameters(['rola' => $rola])
		->make();

	$this->ustawZalogowanegoUsera($this->zalogowany->id);
    }

    protected function ustawZalogowanegoUseraId1() {
	$this->ustawZalogowanegoUsera(1);
    }

    protected function ustawZalogowanegoUsera($id) {

	$this->sm->get(\Wspolne\Menager\SesjaMenager::class)->ustawIdUzytkownika($id);
    }

    //TODO: Przenieść do klasy bazowej z GGerp
    protected function dodajKrokMilowyDoProjektu($kod, $data = null) {
	\FabrykaRekordow::makeEncje(\Projekty\Entity\ProjektKrokMilowy::class,
		PFIntegracyjneZapisNiepowiazane::create($this->sm)
			->setparametry(array('projekt' => $this->projekt, 'krokMilowy' => $kod, 'data_zakonczenia' => $data)));
    }

    protected function zrobUzytkownika() {
	return \FabrykaRekordow::makeEncje(\Logowanie\Model\Uzytkownik::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));
    }

    public function ustawJednorazowo() {

    }

    protected function tearDown() {
	$this->czyscPamiecPodreczna();
    }

    private function czyscPamiecPodreczna() {
	$refl = new \ReflectionObject($this);
	foreach ($refl->getProperties() as $prop) {
	    if (!$prop->isStatic() && 0 !== strpos($prop->getDeclaringClass()->getName(), 'PHPUnit_')) {
		$prop->setAccessible(true);
		$prop->setValue($this, null);
	    }
	}
    }

}
