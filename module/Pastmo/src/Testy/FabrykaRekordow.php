<?php

namespace Pastmo\Testy;

class FabrykaRekordow {

    public static function makeEncje($klasa, $parametryFabryki) {
	switch ($klasa) {
	    case \Pastmo\Email\Entity\KontoMailowe::class:
		return (new FabrykiKonkretnychEncji\KontoMailoweFactory($parametryFabryki))->makeEncje();
	    case Pastmo\Email\Entity\RekordSzczegoluMaila::class:
		return (new FabrykiKonkretnychEncji\RekordSzczegoluMailaFactory($parametryFabryki))->makeEncje();
	    case Pastmo\Faktury\Entity\WystawcaFaktur::class:
		return (new WystawcaFakturFactory($parametryFabryki))->makeEncje();
	    case \Pastmo\Uzytkownicy\Entity\Uzytkownik::class:
		return (new FabrykiKonkretnychEncji\UzytkownikFactory($parametryFabryki))->makeEncje();
	    case \Pastmo\Uzytkownicy\Entity\Jezyk::class:
		return (new FabrykiKonkretnychEncji\JezykFactory($parametryFabryki))->makeEncje();
	    case \Pastmo\Logi\Entity\Log::class:
		return (new FabrykiKonkretnychEncji\LogFactory($parametryFabryki))->makeEncje();
	    case \Pastmo\Logi\Entity\LogKategoria::class:
		return (new FabrykiKonkretnychEncji\LogKategoriaFactory($parametryFabryki))->makeEncje();
	    case \Pastmo\Wspolne\Entity\UstawienieSystemu::class:
		return (new FabrykiKonkretnychEncji\UstawienieSystemuFactory($parametryFabryki))->makeEncje();
	    case \Zasoby\Entity\Zasob::class:
	    case \Pastmo\Zasoby\Entity\Zasob::class:
		return (new FabrykiKonkretnychEncji\ZasobFactory($parametryFabryki))->makeEncje();
	    case \Pastmo\Wspolne\Entity\Adres::class:
		return (new \Pastmo\Testy\FabrykiKonkretnychEncji\AdresFactory($parametryFabryki))->makeEncje();
	    case \Pastmo\Wiki\Entity\WikiArtykul::class:
		return (new FabrykiKonkretnychEncji\WikiArtykulFactory($parametryFabryki))->makeEncje();
	    case \Pastmo\Wiki\Entity\WikiArtykulHistoria::class:
		return (new FabrykiKonkretnychEncji\WikiArtykulHistoriaFactory($parametryFabryki))->makeEncje();
	    case \Logowanie\Entity\UprawnienieRola::class:
		return (new FabrykiKonkretnychEncji\UprawnienieRolaFactory($parametryFabryki))->makeEncje();
	    case \Pastmo\Platnosci\Entity\Cennik::class:
		return (new FabrykiKonkretnychEncji\CennikFactory($parametryFabryki))->makeEncje();
	    case \Pastmo\Platnosci\Entity\Platnosc::class:
		return (new FabrykiKonkretnychEncji\PlatnoscFactory($parametryFabryki))->makeEncje();
	    case \Pastmo\Tlumaczenia\Entity\Slownik::class:
		return (new FabrykiKonkretnychEncji\SlownikFactory($parametryFabryki))->makeEncje();
	    case \Pastmo\Tlumaczenia\Entity\Tlumaczenie::class:
		return (new FabrykiKonkretnychEncji\TlumaczenieFactory($parametryFabryki))->makeEncje();
	    case \Pastmo\Tlumaczenia\Entity\TlumaczenieSlownik::class:
		return (new FabrykiKonkretnychEncji\TlumaczenieSlownikFactory($parametryFabryki))->makeEncje();
	}
	return false;
    }

}
