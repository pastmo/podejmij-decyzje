<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pastmo\Testy\TraityPomocnicze;

trait StaleWTestach {

    static $DATA_7_9 = '2016-12-07 09:00:00';
    static $DATA_7_11 = '2016-12-07 11:00:00';
    static $DATA_7_13 = '2016-12-07 13:00:00';
    static $DATA_7_15 = '2016-12-07 15:00:00';
    static $DATA_7_17 = '2016-12-07 17:00:00';
    static $DATA_7_21 = '2016-12-07 21:00:00';
    static $DATA_8_7 = '2016-12-08 07:00:00';
    static $DATA_8_9 = '2016-12-08 09:00:00';
    static $DATA_8_11 = '2016-12-08 11:00:00';
    static $DATA_8_13 = '2016-12-08 13:00:00';
    static $DATA_8_15 = '2016-12-08 15:00:00';
    static $DATA_8_21 = '2016-12-08 21:00:00';
    static $DATA_12_11 = '2016-12-12 11:00:00';
    static $DATA_12_13 = '2016-12-12 13:00:00';
    static $DATA_12_15 = '2016-12-12 15:00:00';

}
