<?php

namespace Pastmo\Testy\TraityPomocnicze;

use Dashboard\Menager\PowiadomieniaMenager;

trait SprawdzMetody {

    protected function sprawdzBodyZawiera($szukanyTekst) {
	$body = $this->getResponse()->getBody();
	$this->assertContains($szukanyTekst, $body);
    }

    protected function sprawdzBodyNieZawiera($szukanyTekst) {
	$body = $this->getResponse()->getBody();
	$this->assertNotContains($szukanyTekst, $body);
    }

    protected function sprawdzStringZawiera($string, $szukanyTekst) {
	$this->assertContains($szukanyTekst, $string);
    }

    protected function sprawdzStringNieZawiera($string, $szukanyTekst) {
	$this->assertNotContains($szukanyTekst, $string);
    }

    protected function sprawdzOutputZawiera($string) {
	$this->expectOutputRegex("/($string)/s");
    }

    protected function sprawdzOutputNieZawiera($string) {
	$this->expectOutputRegex("/^((?!$string).)*$/s");
    }

    protected function sprawdzCzyStatus200() {
	$this->assertResponseStatusCode(200);
    }

    protected function sprawdzCzyPrzekierowanie() {
	$this->assertResponseStatusCode(302);
    }

    protected function sprawdzRouteName($expected) {
	$this->assertMatchedRouteName($expected);
    }

    protected function sprawdzPrzekierowanie($expected) {
	$this->assertRedirectTo($expected);
    }

    protected function sprawdzOdpowiedzZawiera($szukanyTekst) {
	$odpowiedz = $this->getResponse()->getBody();
	$this->sprawdzStringZawiera($odpowiedz, $szukanyTekst);
    }

    protected function sprawdzOdpowiedzJestRowna($szukanyTekst) {
	$odpowiedz = $this->getResponse()->getBody();
	$this->assertEquals($szukanyTekst, $odpowiedz);
    }

    protected function sprawdzOdpowiedzNieZawiera($szukanyTekst) {
	$odpowiedz = $this->getResponse()->getBody();
	$this->sprawdzStringNieZawiera($odpowiedz, $szukanyTekst);
    }

    protected function sprawdzCzyNieMaWyjatku() {
	$exception = $this->getApplication()->getMvcEvent()->getParam('exception');
	if ($exception) {
	    throw $exception;
	}
    }

    protected function sprawdzCzySukces() {
	$this->sprawdzOdpowiedzZawiera('"success":true');
    }

    protected function sprawdzCzyFail() {
	$this->sprawdzOdpowiedzZawiera('"success":false');
    }

    protected function sprwadzParametrTenSamDzien($data) {
	return $this->equalTo($data, 1);
    }

    protected function sprawdzPowiadomienieUzytkownika($uzytkownik, $kodKategorii, $fragmentTresci,
	    $projektId) {
	$powiadomienia = $this->sm->get(PowiadomieniaMenager::class)->pobierzPowiadomieniaNieprzeczytane($uzytkownik->id);
	$this->assertEquals(1, count($powiadomienia));

	$powiadomienie = $powiadomienia[0];
	$this->assertEquals($kodKategorii, $powiadomienie->kategoria->kod);
	$this->assertEquals($projektId, $powiadomienie->projekt->id);
	$this->sprawdzStringZawiera($powiadomienie->tresc, $fragmentTresci);
    }

}
