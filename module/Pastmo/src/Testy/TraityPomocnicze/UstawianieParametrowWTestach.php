<?php

namespace Pastmo\Testy\TraityPomocnicze;

trait UstawianieParametrowWTestach {

    public function arrayToParameters(array $post) {
	$parameters = new \Zend\Stdlib\Parameters($post);
	return $parameters;
    }

    protected function zrobMockObiektu($klasa, $metodyDoPrzesloniecia) {
	return $this->getMockBuilder($klasa)
			->setConstructorArgs([$this->sm])
			->setMethods($metodyDoPrzesloniecia)->getMock();
    }

    protected function nadpiszSerwis($klucz, $mock) {
	$this->sm->setService($klucz, $mock);
    }

    protected function ustawIgnorowanyTestZModuluPastmo() {
	$class = get_class($this);
	$ns = $this->wytnijDoUkosnika($this->wytnijDoUkosnika($class));

	if (in_array($ns, \Application\AktywneModulyZPastmo::IGNOROWANE_NAMESPACE_TESTOW)) {
	    $this->markTestSkipped($ns. " jest ignorowane w tym projekcie");
	}
    }

    private function wytnijDoUkosnika($nazwa) {
	return substr($nazwa, 0, strrpos($nazwa, '\\'));
    }

}
