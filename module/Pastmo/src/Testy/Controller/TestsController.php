<?php

namespace Pastmo\Testy\Controller;

use \Logowanie\Enumy\KodyUprawnien;
use Zend\View\Model\ViewModel;
use Pastmo\Wspolne\Utils\SearchPole;
use Pastmo\Wspolne\Adnotacje\DomyslnyJsonModel;

class TestsController extends \Wspolne\Controller\WspolneController {

    /**
     * @DomyslnyJsonModel
     */
    public function przedWszystkimi() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_ADMINISTRATORA, true);
	$layout = $this->layout();
	$layout->setTemplate('layout/bialy_layout');
    }

    public function indexAction() {
	$this->przedWszystkimi();
    }

    public function wyswietlChatAction() {
	$this->przedWszystkimi();
    }

    public function translateAction() {
	$this->przedWszystkimi();

	return new ViewModel();
    }

    public function nieznanyWyjatekAction() {
	$this->przedWszystkimi();
	throw new \Exception("Wyjątek systemowy");
    }

    public function uprawnieniaWyjatekAction() {
	$this->przedWszystkimi();
	throw new \Pastmo\Uzytkownicy\Exception\UprawnieniaException("Nie masz uprawnienia");
    }

    public function samLayoutAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_ADMINISTRATORA, true);
    }

    public function wysiwygAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_ADMINISTRATORA, true);

	$content = $this->params()->fromPost('content', '');
	$przekonwertowanyNapis = $this->zasobyFasada->konwertujZasobyZNapisu($content);

	return new ViewModel(['content' => $przekonwertowanyNapis]);
    }

    public function menuAction() {
	$this->przedWszystkimi();
    }

    public function submenuAction() {
	$this->przedWszystkimi();
    }

    public function rozwijalneMenuAction() {
	$this->przedWszystkimi();
    }

    public function wyszukiwanieGetAction() {
	$this->przedWszystkimi();

	$pola = array(
		SearchPole::create()->setDataType(SearchPole::INT)->setName('id'));

	$this->pobierzZGeta($this->tagiMenager, "dodatkowy where", $pola, "domyslne ASC");

	return $this->returnFail(array('msg' => 'akcja zrobiona tylko w celach testowych'));
    }

    public function wyszukiwanieStronicowaneAction() {
	$this->przedWszystkimi();

	$pola = array(
		SearchPole::create()->setDataType(SearchPole::INT)->setName('id'));

	$builder = \Pastmo\Wspolne\Builder\PobieranieStronicowaneBuilder::create()
		->setTable($this->tagiMenager)
		->setDodatkowyWhere("dodatkowy where")
		->setPola($pola)
		->setDomyslneSortowanie("domyslne ASC")
		->setGroupBy('kolumna group')
	;

	$this->pobierzZGetaStronicowanie($builder);

	return $this->returnFail(array('msg' => 'akcja zrobiona tylko w celach testowych'));
    }

    public function wyszukiwanieZDodatkowymOrderAction() {
	$this->przedWszystkimi();

	$pola = array(
		SearchPole::create()->setDataType(SearchPole::INT)->setName('id'));

	$builder = \Pastmo\Wspolne\Builder\PobieranieStronicowaneBuilder::create()
		->setTable($this->tagiMenager)
		->setDodatkowyWhere("dodatkowy where")
		->setPola($pola)
		->setDomyslneSortowanie("domyslne ASC")
		->setKolumnySortowania(['kolumna1' => 'ASC', 'kolumna2' => 'ASC'])
		->setZwrocPaginator(false)

	;

	$this->pobierzZGetaStronicowanie($builder);

	return $this->returnFail(array('msg' => 'akcja zrobiona tylko w celach testowych'));
    }

    public function wyszukiwanieZDodatkowymOrderBezSortowaniaAction() {
	$this->przedWszystkimi();

	$pola = array(
		SearchPole::create()->setDataType(SearchPole::INT)->setName('id'));

	$builder = \Pastmo\Wspolne\Builder\PobieranieStronicowaneBuilder::create()
		->setTable($this->tagiMenager)
		->setDodatkowyWhere("dodatkowy where")
		->setPola($pola)
		->setKolumnySortowania(['kolumna1' => 'ASC', 'kolumna2' => 'ASC'])
		->setZwrocPaginator(false)
	;

	$this->pobierzZGetaStronicowanie($builder);

	return $this->returnFail(array('msg' => 'akcja zrobiona tylko w celach testowych'));
    }

     public function wyswietlGadzetEmailAction() {
	$this->przedWszystkimi();

	return new ViewModel();
    }

    public function szablonAction(){
	$this->przedWszystkimi();
    }

    public function konfigPhpAction(){
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_ADMINISTRATORA, true);

	echo phpinfo();

	die;
    }

}
