<?php
namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class RekordSzczegoluMailaFactory  extends \FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new \Pastmo\Email\Entity\RekordSzczegoluMaila();
		$encja->tytul="Temat";

		return $encja;
	}

	public function pobierzNazweTableMenager() {
		throw new \Exception(__CLASS__." Nie zapisuje się do bazy danych");
	}

}
