<?php

namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class WikiArtykulFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Pastmo\Wiki\Entity\WikiArtykul();
	$encja->wartosc = __CLASS__;
	$encja->kod = __CLASS__;
	$encja->id_tematu = rand(1, 100);
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Pastmo\Wiki\Menager\WikiArtykulyMenager::class;
    }

}
