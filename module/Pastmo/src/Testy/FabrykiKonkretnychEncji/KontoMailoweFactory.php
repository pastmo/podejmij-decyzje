<?php

namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class KontoMailoweFactory extends \Pastmo\Testy\Fabryki\FabrykaAbstrakcyjna {

    const HASLO = 'Testowe1';

    public function nowaEncja() {
	$encja = new \Pastmo\Email\Entity\KontoMailowe($this->parametryFabryki->sm);

	$encja->host = 'serwer1578643.home.pl';
	$encja->user = 'testowe1@grupaglasso.pl';
	$encja->ustawHaslo(self::HASLO);
	$encja->port_imap = '143';


	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Pastmo\Email\Menager\KontaMailoweMenager::getClass();
    }

    public function dowiazInneTabele() {
	parent::dowiazInneTabele();

	$this->encja->uzytkownik = $this->uzyjWlasciwejFabryki(Logowanie\Model\Uzytkownik::class);
    }

}
