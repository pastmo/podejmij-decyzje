<?php

namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class CennikFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Pastmo\Platnosci\Entity\Cennik($this->getSm());
	$encja->kwota = 42.5;
	$encja->opis = __CLASS__;
	$encja->dni_przedluzenia = 30;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Pastmo\Platnosci\Menager\CennikMenager::class;
    }

}
