<?php
namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class AdresFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Pastmo\Wspolne\Entity\Adres();
	$encja->kod = "00-940";
	$encja->kraj = 'Polska';
	$encja->miasto = "Warszawa";
	$encja->ulica = "Aleje Jerozolimskie";

	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Pastmo\Wspolne\Menager\AdresyMenager::class;
    }

}
