<?php

namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class PlatnoscFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Pastmo\Platnosci\Entity\Platnosc($this->getSm());
	$encja->kwota = $this->genrujId();
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Pastmo\Platnosci\Menager\PlatnosciMenager::class;
    }

}
