<?php

namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class LogKategoriaFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Pastmo\Logi\Entity\LogKategoria();
	$encja->kod = $this->getUnikalyNapis('KLA');
	$encja->nazwa = __CLASS__;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Pastmo\Logi\Menager\LogiMenager::class;
    }

}
