<?php
namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class ZasobFactory  extends \Pastmo\Testy\Fabryki\FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new \Zasoby\Model\Zasob();
		$encja->url="1.jpg";
		$encja->nazwa="nazwa.jpg";
		$encja->typ_fizyczny='image/png';
		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return \Zasoby\Menager\ZasobyUploadMenager::class;
	}

}
