<?php

namespace Pastmo\Testy\FabrykiKonkretnychEncji;


class UzytkownikKontoMailoweFactory extends \FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new \Logowanie\Entity\UzytkownikKontoMailowe();
		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return \Logowanie\Menager\UzytkownicyKontaMailoweMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();

		$this->encja->uzytkownik = $this->uzyjWlasciwejFabryki(\Logowanie\Model\Uzytkownik::class);
		$this->encja->konto_mailowe = $this->uzyjWlasciwejFabryki(\Pastmo\Email\Entity\KontoMailowe::class);
	}

}
