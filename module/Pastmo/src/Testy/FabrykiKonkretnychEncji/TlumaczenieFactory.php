<?php

namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class TlumaczenieFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Pastmo\Tlumaczenia\Entity\Tlumaczenie($this->getSm());
	$encja->oryginalny_tekst = __CLASS__;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Pastmo\Tlumaczenia\Menager\TlumaczeniaMenager::class;
    }

}
