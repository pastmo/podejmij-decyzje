<?php

namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class TlumaczenieSlownikFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Pastmo\Tlumaczenia\Entity\TlumaczenieSlownik($this->getSm());
	$encja->tekst=__CLASS__;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Pastmo\Tlumaczenia\Menager\TlumaczeniaSlownikiMenager::class;
    }

}
