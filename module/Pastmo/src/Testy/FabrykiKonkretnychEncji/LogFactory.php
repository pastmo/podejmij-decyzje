<?php

namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class LogFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Pastmo\Logi\Entity\Log();
	$encja->tresc = __CLASS__;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Pastmo\Logi\Menager\LogiMenager::class;
    }

}
