<?php
namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class UprawnienieRolaFactory extends \Pastmo\Testy\Fabryki\FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Logowanie\Entity\UprawnienieRola($this->parametryFabryki->sm);
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Logowanie\Menager\UprawnieniaRoleMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->rola_id = $this->uzyjWlasciwejFabryki(\Logowanie\Entity\Rola::class);
	$this->encja->uprawnienie_kod = $this->uzyjWlasciwejFabryki(\Logowanie\Entity\Uprawnienie::class);
    }

}
