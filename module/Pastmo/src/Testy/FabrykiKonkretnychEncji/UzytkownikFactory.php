<?php

namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class UzytkownikFactory extends \Pastmo\Testy\Fabryki\FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Logowanie\Model\Uzytkownik($this->parametryFabryki->sm);
	$encja->login = "login";
	$encja->haslo = "haslo";
	$encja->imie = 'UzytkownikFactory';
	$encja->mail = $this->getUnikalyNapis("adres_email@asb.pl");
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Logowanie\Menager\UzytkownicyMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->avatar = $this->uzyjWlasciwejFabryki(\Zasoby\Model\Zasob::class);
	$this->encja->rola = $this->uzyjWlasciwejFabryki(\Logowanie\Entity\Rola::class);
    }

}
