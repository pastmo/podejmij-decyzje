<?php

namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class UstawienieSystemuFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Pastmo\Wspolne\Entity\UstawienieSystemu();
	$encja->wartosc = __CLASS__;
	$encja->kod = __CLASS__;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Pastmo\Wspolne\Menager\UstawieniaSystemuMenager::class;
    }

}
