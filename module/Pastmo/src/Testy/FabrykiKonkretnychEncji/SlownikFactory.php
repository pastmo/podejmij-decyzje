<?php

namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class SlownikFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Pastmo\Tlumaczenia\Entity\Slownik($this->getSm());
	$encja->nazwa=__CLASS__;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Pastmo\Tlumaczenia\Menager\SlownikiMenager::class;
    }

}
