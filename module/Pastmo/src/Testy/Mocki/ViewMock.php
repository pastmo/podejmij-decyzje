<?php

namespace Pastmo\Testy\Mocki;

class ViewMock implements \Zend\View\Renderer\RendererInterface {

    public function url() {

    }

    public function basePath($url = "") {
	return $url;
    }

    public function dodajWysiwyg() {

    }

    public function selected() {

    }

    public function wyswietlOknoParametrowWizualizacji() {

    }

    public function wyswietlOknoParametrowProdukcji() {

    }

    public function uprawnienia() {
	return true;
    }

    public function wyswietlPozycjeMenuRekorduEmail() {

    }

    public function wyswietlRekordEmail() {

    }

    public function wyswietlStatusyKrokowMiowych() {

    }

    public function partial($url, $parametry) {

    }

    public function getEngine() {

    }

    public function render($nameOrModel, $values = null) {

    }

    public function setResolver(\Zend\View\Resolver\ResolverInterface $resolver) {

    }

}
