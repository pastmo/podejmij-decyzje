<?php

namespace Pastmo\Testy\ZbiorczeFunkcje;

trait WspolneFunkcjeDlaTestowCyklicznychAktualizacji {

    public static $msgLogu;

    protected function ustawMockiCzasu($godzinaAktualizacji, $godzinaBiezaca) {
	$this->ustawieniaSystemuMenager->expects($this->once())->method('pobierzUstawieniePoKodzie')->willReturn($godzinaAktualizacji);
	$this->czasMenager->expects($this->once())->method('getAktualnyTimestamp')->willReturn("2017-10-11 $godzinaBiezaca");
    }

    protected function ustawSprawdzanieLogow($msgLogu) {
	WspolneFunkcjeDlaTestowCyklicznychAktualizacji::$msgLogu = $msgLogu;

	$this->logiMenager->expects($this->once())->method('dodajLog')->with(
		$this->callback(function(\Pastmo\Logi\Builder\LogiBuilder $builder) {
		    $this->sprawdzStringZawiera($builder->tresc['msg'],
			    WspolneFunkcjeDlaTestowCyklicznychAktualizacji::$msgLogu
		    );
		    return true;
		}));
    }

}
