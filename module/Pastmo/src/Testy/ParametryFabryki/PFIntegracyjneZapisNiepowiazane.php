<?php

namespace Pastmo\Testy\ParametryFabryki;

class PFIntegracyjneZapisNiepowiazane extends ParametryFabryki {

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	$this->sm = $sm;
	$this->czyFabrykaMockow = false;
	$this->zapiszDoBazy = true;
	$this->czyGenerowaniePowiazanychEncji = false;
	$this->parametry = array();
    }

    public static function create(\Zend\ServiceManager\ServiceManager $sm) {
	return new PFIntegracyjneZapisNiepowiazane($sm);
    }

}
