<?php

namespace Pastmo\Testy\ParametryFabryki;

class PFMockowePowiazane extends ParametryFabryki {

    public function __construct($sm = null) {
	$this->sm = $sm;
	$this->czyFabrykaMockow = true;
	$this->zapiszDoBazy = false;
	$this->czyGenerowaniePowiazanychEncji = true;
	$this->parametry = array();
    }

    public static function create($sm = null) {
	return new PFMockowePowiazane($sm);
    }

}
