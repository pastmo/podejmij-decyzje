<?php

namespace Pastmo\Testy\ParametryFabryki;

class ParametryFabryki {

    public $sm;
    public $zapiszDoBazy;
    public $parametry = array();
    public $czyGenerowaniePowiazanychEncji = true;
    public $czyFabrykaMockow = true;

    public function setSm($sm) {
	$this->sm = $sm;
	return $this;
    }

    public function setzapiszDoBazy($zapiszDoBazy) {
	$this->zapiszDoBazy = $zapiszDoBazy;
	return $this;
    }

    public function setparametry($parametry) {
	$this->parametry = $parametry;
	return $this;
    }

    public function setczyGenerowaniePowiazanychEncji($czyGenerowaniePowiazanychEncji) {
	$this->czyGenerowaniePowiazanychEncji = $czyGenerowaniePowiazanychEncji;
	return $this;
    }

    public function setczyFabrykaMockow($czyFabrykaMockow) {
	$this->czyFabrykaMockow = $czyFabrykaMockow;
	return $this;
    }

}
