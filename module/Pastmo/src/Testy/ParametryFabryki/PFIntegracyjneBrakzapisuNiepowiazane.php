<?php

namespace Pastmo\Testy\ParametryFabryki;

class PFIntegracyjneBrakzapisuNiepowiazane extends ParametryFabryki {

    public function __construct($sm = null) {
	$this->sm = $sm;
	$this->czyFabrykaMockow = false;
	$this->zapiszDoBazy = false;
	$this->czyGenerowaniePowiazanychEncji = false;
	$this->parametry = array();

    }

    public static function create($sm = null) {
	return new PFIntegracyjneBrakzapisuNiepowiazane($sm);
    }

}
