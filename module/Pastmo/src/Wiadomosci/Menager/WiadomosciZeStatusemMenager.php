<?php

namespace Pastmo\Wiadomosci\Menager;

class WiadomosciZeStatusemMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function zmienStatus($wiadomoscId, $nowyStatus) {
	$wiadomosc = $this->getRekord($wiadomoscId);
	$wiadomosc->status = $nowyStatus;
	$this->zapisz($wiadomosc);
    }

    public function zmienSkrzynke($wiadomoscId, $skrzynka) {
	$wiadomosc = $this->getRekord($wiadomoscId);
	$wiadomosc->skrzynka = $skrzynka;
	$this->zapisz($wiadomosc);
    }

}
