<?php

namespace Pastmo\Wiki\Menager;

use Zend\ServiceManager\ServiceManager;
use Pastmo\Wiki\KonfiguracjaModulu;
use Pastmo\Wiki\Menager\WikiArtykulyHistoriaMenager;
use Pastmo\Wspolne\Utils\ArrayUtil;
use \Logowanie\Menager\UprawnieniaMenager;

class WikiArtykulyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(ServiceManager $sm = null) {
	parent::__construct($sm, KonfiguracjaModulu::WIKI_GATEWAY);
    }

    public function pobierzArtykul($id_tematu, $jezyk_kod) {

	if ($id_tematu) {
	    $where = "jezyk_kod = '" . $jezyk_kod . "' AND id_tematu = $id_tematu";
	    $artykuly = $this->pobierzZWherem($where);
	    $wynik = ArrayUtil::pobierzPierwszyElement($artykuly);
	    return $wynik ? $wynik : $this->artykulNieZnaleziony($id_tematu, $jezyk_kod);
	}
	return new \Pastmo\Wiki\Entity\WikiArtykul;
    }

    public function pobierzListeArtykulowPowiazanych($artykul_szczegoly, $jezyk_kod) {

	if (!$artykul_szczegoly) {
	    return (new \Pastmo\Wiki\Entity\WikiArtykul());
	}
	if (isset($this->nieznaleziony)) {
	    $where = "id_tematu = '" . $artykul_szczegoly->id_tematu . "'";
	    $artykuly = $this->pobierzResultSetZWherem($where);
	    return $artykuly ? $artykuly : (new \Pastmo\Wiki\Entity\WikiArtykul());
	}
	$where = "jezyk_kod = '" . $jezyk_kod . "'";
	if (!$artykul_szczegoly->id_tematu) {
	    $where .= " AND nadrzedny_id_tematu IS NULL";
	} else {
	    $bierzaceNadrzedneIdTematu = $artykul_szczegoly->id_tematu;
	    $where .= " AND nadrzedny_id_tematu = '" . $bierzaceNadrzedneIdTematu . "'";
	}
	$artykuly = $this->pobierzResultSetZWherem($where);
	return $artykuly ? $artykuly : (new \Pastmo\Wiki\Entity\WikiArtykul());
    }

    public function getJezykiDisabled($artykul) {

	$wynik = null;
	if ($artykul && $artykul->id_tematu) {
	    $wynik = $this->getTabliceKodowJezykowDlaDanegoIdTematu($artykul->id_tematu);
	}
	return $wynik;
    }

    public function aktualizujLubUtworzArtykul($post) {

	$this->artykul = new \Pastmo\Wiki\Entity\WikiArtykul();
	$this->artykul->exchangeArray($post);
	$this->ustawZalogowanegoJakoAutora();
	if ($post && $post->id) {
	    $this->artykulDoArchiwum = $this->get(WikiArtykulyHistoriaMenager::class);
	    $this->parameters = $this->getRekord($post->id);
	    $this->artykulDoArchiwum->zapiszDoArchiwum($this->parameters);
	}
	$this->zapisz($this->artykul);
    }

    private function ustawZalogowanegoJakoAutora() {

	$zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();
	$this->artykul->autor = $zalogowany;
    }

    public function pobierzNowyParametrArtykulu($element) {

	$tablicaElementow = array_map(
		function ($artykul) use ($element) {
	    return $artykul->$element;
	}, ArrayUtil::konwertujNaArray(
			$this->pobierzResultSetZWherem('id>0'))
	);

	$tab = array_unique(array_filter($tablicaElementow));
	return (count($tab) > 0) ? max($tab) + 1 : 1;
    }

    public function getTabliceKodowJezykowDlaDanegoIdTematu($id_tematu) {

	$tablicaArtykuluowOZadanymIdTematu = ArrayUtil::konwertujNaArray(
			$this->pobierzResultSetZWherem('id_tematu = ' . $id_tematu));

	return array_map(
		function ($artykul) {
	    return $artykul->jezyk_kod;
	}, $tablicaArtykuluowOZadanymIdTematu
	);
    }

    public function pobierzKodyUprawnien() {
	$uprawnieniaMenager = $this->get(UprawnieniaMenager::class);
	return array(
	    $uprawnieniaMenager->pobierzUprawnieniePoKodzie('rol_def'),
	    $uprawnieniaMenager->pobierzUprawnieniePoKodzie('rol_kli'),
	    $uprawnieniaMenager->pobierzUprawnieniePoKodzie('rol_spr')
	);
    }

    private function artykulNieZnaleziony($id_tematu) {

	$artykul =  new \Pastmo\Wiki\Entity\WikiArtykul($id_tematu);

	$where = "id_tematu = $id_tematu";
	$artykuly = $this->pobierzZWherem($where);
	$wynik = ArrayUtil::pobierzPierwszyElement($artykuly);

	$artykul->nadrzedny_id_tematu = $wynik->nadrzedny_id_tematu;
	$artykul->tytul = $this->_translate("Artykuł nieznaleziony", false, 7);
	$artykul->id_tematu = $id_tematu;

	$this->nieznaleziony = true;
	return $artykul;
    }

}
