<?php

namespace Pastmo\Wiki\Menager;

use Pastmo\Wiki\Entity\WikiArtykulHistoria;
use Pastmo\Wiki\KonfiguracjaModulu;
use Zend\ServiceManager\ServiceManager;

class WikiArtykulyHistoriaMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(ServiceManager $sm = null) {
	parent::__construct($sm, KonfiguracjaModulu::WIKI_GATEWAY_HISTORIA);
    }

    public function zapiszDoArchiwum($artykul) {

	$artykulDoArchiwum = WikiArtykulHistoria::fromWikiArtykul($artykul);
	$this->zapisz($artykulDoArchiwum);
    }


}
