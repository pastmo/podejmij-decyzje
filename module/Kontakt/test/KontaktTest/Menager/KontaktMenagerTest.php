<?php

namespace KontaktTest\Menager;

use Pastmo\Testy\Util\TB;

class KontaktMenagerTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    public function setUp() {
	parent::setUp();
	$this->kontaktMenager = $this->zrobMockObiektu(\Kontakt\Menager\KontaktMenager::class, [
		'wyslijMaila'
	]);
    }

    public function test_zapiszKontakt_blad_recaptcha() {
	$post = $this->arrayToParameters(
		['g-recaptcha-response' => "błędny kod"]);

	$wynik = $this->kontaktMenager->zapiszKontakt($post);

	$this->assertFalse($wynik->success);
    }

}
