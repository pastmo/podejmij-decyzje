<?php

namespace KontaktTest\Controller;

use \Projekty\TworzenieProjektu\ProjektBuilder;
use \Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane;

class KontaktAjaxControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    protected $traceError = true;

    public function setUp() {
	parent::setUp();
    }

    public function testIndex() {
    }

    public function test_zapiszKontaktAction() {
	$this->kontaktMenager->expects($this->once())->method('zapiszKontakt');
	$this->dispatch('/kontakt_ajax/zapisz_kontakt');
    }

}
