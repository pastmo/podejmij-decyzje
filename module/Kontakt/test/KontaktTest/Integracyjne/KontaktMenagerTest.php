<?php

namespace KontaktTest\Integracyjne;

use Pastmo\Testy\Util\TB;

class KontaktMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $kontaktMenager;

    public function setUp() {
	parent::setUp();
	$this->kontaktMenager = $this->zrobMockObiektu(\Kontakt\Menager\KontaktMenager::class,
		[
		'wyslijMaila', 'sprawdzCaptcha'
	]);
    }

    public function test_dodajRekord() {
	$kontakt = TB::create(\Kontakt\Entity\Kontakt::class, $this)->make();

	$this->assertNotNull($kontakt->id);
    }

    public function test_zapiszKontakt() {
	$post = $this->arrayToParameters(
		['imie' => "Krzysztof Jarzyna", 'email' => 'abc@ss.pl',
			"tresc" => "Proszę o kontakt", 'zgoda_przetwarzanie' => '1',
			'zgoda_przetwarzanie_marketingowe' => '1']);

	$this->kontaktMenager->expects($this->once())->method('wyslijMaila');
	$wynik = $this->kontaktMenager->zapiszKontakt($post);

	$zapisany = $this->kontaktMenager->getPoprzednioDodany();
	$this->assertEquals("Krzysztof Jarzyna", $zapisany->imie);
	$this->assertEquals("abc@ss.pl", $zapisany->email);
	$this->assertEquals("Proszę o kontakt", $zapisany->tresc);
	$this->assertEquals(1, $zapisany->zgoda_przetwarzanie);
	$this->assertEquals(1, $zapisany->zgoda_przetwarzanie_marketingowe);

	$this->assertEquals(true, $wynik->success);
    }

    public function test_pobierzTrescMaila() {
	$kontakt = TB::create(\Kontakt\Entity\Kontakt::class, $this)->setParameters(['imie' => "Krzysztof Jarzyna", 'email' => 'abc@ss.pl',
			"tresc" => "Proszę o kontakt"])->make();
	$wynik = $this->kontaktMenager->pobierzTrescMaila($kontakt);

	$this->sprawdzStringZawiera($wynik, "Krzysztof Jarzyna");
    }

    public function t_est_zapiszKontakt_mail() {//Test wysyła maila do Pani Prezes- nie można tego nadużywać:)
	$this->kontaktMenager = $this->zrobMockObiektu(\Kontakt\Menager\KontaktMenager::class, [ 'sprawdzCaptcha']);

	$post = $this->arrayToParameters(
		['imie' => "Krzysztof Jarzyna", 'email' => 'abc@ss.pl', "tresc" => "Proszę o kontakt"]);

	$wynik = $this->kontaktMenager->zapiszKontakt($post);
	$this->assertTrue($wynik->success, $wynik . '');
    }

}
