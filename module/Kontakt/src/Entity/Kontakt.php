<?php

namespace Kontakt\Entity;

class Kontakt extends \Wspolne\Model\WspolneModel {

    public $id;
    public $email;
    public $imie;
    public $tresc;
    public $zgoda_przetwarzanie;
    public $zgoda_przetwarzanie_marketingowe;

    public function exchangeArray($data) {
	$this->data = $data;

	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->email = $this->pobierzLubNull($data, 'email');
	$this->imie = $this->pobierzLubNull($data, 'imie');
	$this->tresc = $this->pobierzLubNull($data, 'tresc');
	$this->zgoda_przetwarzanie = $this->pobierzLubNull($data, 'zgoda_przetwarzanie');
	$this->zgoda_przetwarzanie_marketingowe = $this->pobierzLubNull($data, 'zgoda_przetwarzanie_marketingowe');
    }

    public function dowiazListyTabelObcych() {

    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
