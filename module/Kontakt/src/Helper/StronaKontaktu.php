<?php
namespace Kontakt\Helper;

class StronaKontaktu extends \Pastmo\Wspolne\Helper\TranslateHelper {

    public function __construct($sm) {
	parent::__construct($sm);
    }

    public function __invoke() {
	$reCaptchaService = $this->sm->get('ReCaptchaService');

	return $this->view->partial('kontakt/helper/strona_kontaktu.phtml',
		['siteKey' => $reCaptchaService->getSiteKey()]);
    }

}
