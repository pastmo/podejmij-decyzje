<?php
namespace Kontakt\Testy;

class FabrykaRekordow {

    public static function makeEncje($klasa, $parametryFabryki) {
	switch ($klasa) {
	    case \Kontakt\Entity\Kontakt::class:
		return (new KontaktFactory($parametryFabryki))->makeEncje();
	}
	return false;
    }

}
