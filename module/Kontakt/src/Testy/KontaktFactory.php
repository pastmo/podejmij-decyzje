<?php

namespace Kontakt\Testy;

class KontaktFactory extends \Pastmo\Testy\Fabryki\FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Kontakt\Entity\Kontakt($this->parametryFabryki->sm);
	$encja->tresc = __CLASS__;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Kontakt\Menager\KontaktMenager::class;
    }

    public function dowiazInneTabele() {

    }

}
