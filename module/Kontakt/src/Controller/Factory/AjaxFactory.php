<?php

namespace Kontakt\Controller\Factory;

use Kontakt\Controller\KontaktAjaxController;

class AjaxFactory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new KontaktAjaxController();
    }

}
