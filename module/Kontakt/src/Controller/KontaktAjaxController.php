<?php

namespace Kontakt\Controller;

use Pastmo\Wspolne\Adnotacje\DomyslnyJsonModel;
use Zend\View\Model\JsonModel;

class KontaktAjaxController extends \Wspolne\Controller\WspolneController {

    public function zapiszKontaktAction() {
	$post = $this->getPost();
	$wynik = $this->kontaktFasada->zapiszKontakt($post);

	return $this->returnJsonModel($wynik->toArray());
    }

}
