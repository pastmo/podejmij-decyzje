<?php

namespace Kontakt;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module implements ConfigProviderInterface {

    use \Pastmo\FunkcjeKonfiguracyjne;

    const KONTAKT_GATEWAY = "KONTAKT_GATEWAY";

    public function getConfig() {
	return include __DIR__ . '/../config/module.config.php';
    }

    public function onBootstrap(\Zend\Mvc\MvcEvent $e) {
	$viewHelperMenager = $e->getApplication()->getServiceManager()->get('ViewHelperManager');

	$viewHelperMenager->setFactory('stronaKontaktu',
		function($sm) use ($e) {
	    $viewHelper = new \Kontakt\Helper\StronaKontaktu($sm);
	    return $viewHelper;
	});
    }

    public function getServiceConfig() {
	return array(
		'factories' => array(
			\Kontakt\Fasada\KontaktFasada::class => function($sm) {
			    $table = new \Kontakt\Fasada\KontaktFasada($sm);
			    return $table;
			},
			\Kontakt\Menager\KontaktMenager::class => function($sm) {
			    $table = new \Kontakt\Menager\KontaktMenager($sm);
			    return $table;
			},
			self::KONTAKT_GATEWAY => function($sm) {
			    return $this->ustawGateway($sm, Entity\Kontakt::class, 'kontakt');
			}
	));
    }

    public static function dowiazMenagery(\Wspolne\Controller\Factory\InitObiekt $initObiekt,
	    \Interop\Container\ContainerInterface $container) {

	$initObiekt->kontaktFasada = $container->get(\Kontakt\Fasada\KontaktFasada::class);
    }

}
