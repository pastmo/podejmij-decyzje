<?php

namespace Kontakt\Menager;

use Zend\ServiceManager\ServiceManager;
use Pastmo\Wspolne\Entity\DomyslnaOdpowiedzPrzetwarzania;

class KontaktMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(ServiceManager $sm) {
        $this->wyszukiwanieZKontem = false;
	parent::__construct($sm, \Kontakt\Module::KONTAKT_GATEWAY);
    }

    public function zapiszKontakt($post) {
	$odpowiedz = DomyslnaOdpowiedzPrzetwarzania::create();
	try {
	    $this->zapiszKontaktTry($post);
	} catch (\Exception $e) {
	    $this->logujWyjatek($e);
	    $odpowiedz->addMsg($e->getMessage());
	    $odpowiedz->setSuccess(false);
	}

	return $odpowiedz;
    }

    public function zapiszKontaktTry($post) {
	$this->sprawdzCaptcha($post);
	$this->zapiszRekordKontakt($post);
	$this->wyslijMailaSimple();
    }

    private function zapiszRekordKontakt($post) {
	$kontakt = new \Kontakt\Entity\Kontakt($this->sm);
	$kontakt->exchangeArray($post);
	$this->zapisz($kontakt);
    }

    public function wyslijMaila() {
	$dodany = $this->getPoprzednioDodany();

	$email = \Pastmo\Email\Entity\EmailDoWysylki::create();
	$email->trescHtml = $this->pobierzTrescMaila($dodany);

	$email->odbiorcaEmail = 'pr@pastmo.pl';
	$email->replyTo = $dodany->email;
	$email->temat = "Kontakt od:{$dodany->email}";

	$uzytkownikKonot = \Logowanie\Entity\UzytkownikKontoMailowe::zrobDomyslneKontoNadawcze($this->sm);
	$email->uzytkownikKonto = $uzytkownikKonot;

	$this->get(\Email\Fasada\EmailFasada::class)->wyslijEmail($email);
    }
    
    private function wyslijMailaSimple() {
        $dodany = $this->getPoprzednioDodany();
        
	$kontaktEmail = $this->get(\Logowanie\Model\KontaktEmail::class);
    $kontaktEmail->trescMaila= "Od: {$dodany->imie} Email: {$dodany->email} Tresc: {$dodany->tresc}";
	
	$kontaktEmail->ustawParametry('pr@pastmo.pl', '$this->uzytkownik->id',
        "email");
	$kontaktEmail->send();
    }

    public function pobierzTrescMaila($kontakt) {

	$tytul = "Witaj Monisiu!";
	$tresc = ["Od: {$kontakt->imie}", "Email: {$kontakt->email}", "Tresc: {$kontakt->tresc}"];

	$wynik = $this->get(\Email\Fasada\EmailFasada::class)->utworzTrescZSzablonem("email/layout/tresc_maila",
		['tytul' => $tytul, 'komunikaty' => $tresc]);

	return $wynik;
    }

    public function sprawdzCaptcha($post) {
	$reCaptchaService = $this->sm->get('ReCaptchaService');
	if (!$reCaptchaService->isValid($post['g-recaptcha-response'], $post)) {
	    throw new \Pastmo\Wspolne\Exception\PastmoException("Nie powiodła się walidacja ReCaptcha.");
	}
    }

}
