<?php

namespace Kontakt\Fasada;

class KontaktFasada extends \Wspolne\Fasada\WspolneFasada {

    public function __construct(\Zend\ServiceManager\ServiceManager $serviceMenager) {
	parent::__construct($serviceMenager);
    }

    public function zapiszKontakt($post) {
	return $this->get(\Kontakt\Menager\KontaktMenager::class)->zapiszKontakt($post);
    }

}
