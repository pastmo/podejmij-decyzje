<?php

namespace Kontakt;

return array(
	'controllers' => array(
		'factories' => array(
			'Kontakt\Controller\KontaktAjax' => Controller\Factory\AjaxFactory::class,
		),
	),
	'router' => array(
		'routes' => array(
			'kontakt_ajax' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/kontakt_ajax[/:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Kontakt\Controller\KontaktAjax',
						'action' => 'index',
					),
				),
			),
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Kontakt' => __DIR__ . '/../view',
		),
	),
);
