<?php

namespace DecyzjeTest\Controller;

use \Projekty\TworzenieProjektu\ProjektBuilder;
use \Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane;

class DecyzjeAjaxControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    protected $traceError = true;

    public function setUp() {
	parent::setUp();
    }

    public function testIndex() {

    }

    public function testzapisz_decyzje() {
	$this->decyzjeMenager->expects($this->once())->method('zapiszDecyzje')
		->willReturn(\Decyzje\Entity\OdpowiedzZapisuDecyzji::create());

	$this->dispatch('/decyzje_ajax/zapisz_decyzje');
	$this->sprawdzCzySukces();
    }

    public function testzapiszRoboczeDecyzje() {
	$this->decyzjeMenager->expects($this->once())->method('zapiszRoboczeDecyzje');

	$this->dispatch('/decyzje_ajax/zapisz_robocze_decyzje');
	$this->sprawdzCzySukces();
    }

}
