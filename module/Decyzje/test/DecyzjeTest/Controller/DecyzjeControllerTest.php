<?php

namespace DecyzjeTest\Controller;

use Pastmo\Testy\Util\TB;

class DecyzjeControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    protected $traceError = true;

    public function setUp() {
	parent::setUp();
    }

    public function testIndex() {
	$decyzja= TB::create(\Decyzje\Entity\Decyzja::class,$this)->make();
	$this->decyzjeMenager->expects($this->once())->method('pobierzRoboczeDecyzje')->willReturn($decyzja);
	
	$this->decyzjeSzczegolyMenager->expects($this->once())->method('pobierzDlaDecyzji')->willReturn([]);

	$this->dispatch('/decyzje');
	$this->sprawdzCzyStatus200();
    }

    public function test_decyzjeAction() {
	$this->decyzjeMenager->expects($this->once())->method('pobierzDecyzjeZalogowanego')->willReturn([]);
	$this->dispatch('/decyzje/decyzje');
    }

}
