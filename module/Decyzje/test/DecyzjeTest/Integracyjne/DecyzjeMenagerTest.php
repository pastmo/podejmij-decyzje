<?php

namespace DecyzjeTest\Integracyjne;

use Pastmo\Testy\Util\TB;
use Decyzje\Enumy\DecyzjaSzczegolyTyp;
use Decyzje\Enumy\DecyzjeWyniki;
use Decyzje\Menager\DecyzjeSzczegolyMenager;

class DecyzjeMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public $decyzjeMenager;

    public function setUp() {
	parent::setUp();
	$this->decyzjeMenager = $this->zrobMockObiektu(\Decyzje\Menager\DecyzjeMenager::class,
		['sprawdzWaznoscKonta']);
    }

    public function test_dodajRekord() {
	$this->utworzIZalogujUzytkownika();
	$decyzja = TB::create(\Decyzje\Entity\Decyzja::class, $this)->make();

	$this->assertNotNull($decyzja->id);
	$this->assertEquals($this->zalogowany->id, $decyzja->uzytkownik->id);
    }

    public function test_przeliczWynik() {
	$decyzja = TB::create(\Decyzje\Entity\Decyzja::class, $this)->make();

	$szczegolBuilder = TB::create(\Decyzje\Entity\DecyzjaSzczegoly::class, $this)
		->setParameters(['typ' => DecyzjaSzczegolyTyp::PLUS, 'wartosc' => 10, 'decyzja' => $decyzja]);
	$szczegolBuilder->make();
	$szczegolBuilder->parameters['typ'] = DecyzjaSzczegolyTyp::MINUS;
	$szczegolBuilder->parameters['wartosc'] = 2;
	$szczegolBuilder->make();

	$this->assertEquals(8, $decyzja->przeliczWynik());

	$szczegolBuilder->parameters['typ'] = DecyzjaSzczegolyTyp::MINUS;
	$szczegolBuilder->parameters['wartosc'] = 9;
	$szczegolBuilder->make();

	$this->assertEquals(-1, $decyzja->przeliczWynik());
    }

    public function test_pobierzSzczegol() {
	$decyzja = TB::create(\Decyzje\Entity\Decyzja::class, $this)->make();

	$szczegolBuilder = TB::create(\Decyzje\Entity\DecyzjaSzczegoly::class, $this)
		->setParameters(['selektor_html' => 'abc', 'wartosc' => 10, 'decyzja' => $decyzja]);
	$szczegol = $szczegolBuilder->make();

	$wynik = $decyzja->pobierzSzczegol('abc');
	$this->assertEquals($szczegol->id, $wynik->id);

	$wynik2 = $decyzja->pobierzSzczegol('nieistniejacy');
	$this->assertNull($wynik2->id);
    }

    public function test_aktualizujWynik() {
	$decyzja = TB::create(\Decyzje\Entity\Decyzja::class, $this)->make();
	$this->assertEquals(DecyzjeWyniki::NIEROZSTRZYGNIETY, $decyzja->wynik);

	$szczegolBuilder = TB::create(\Decyzje\Entity\DecyzjaSzczegoly::class, $this)
		->setParameters(['typ' => DecyzjaSzczegolyTyp::PLUS, 'wartosc' => 10, 'decyzja' => $decyzja]);
	$szczegolBuilder->make();
	$szczegolBuilder->parameters['typ'] = DecyzjaSzczegolyTyp::MINUS;
	$szczegolBuilder->parameters['wartosc'] = 2;
	$szczegolBuilder->make();

	$this->decyzjeMenager->aktualizujWynik($decyzja);
	$zmodyfikowany = $this->decyzjeMenager->getRekord($decyzja->id);
	$this->assertEquals(DecyzjeWyniki::TAK, $zmodyfikowany->wynik);

	$szczegolBuilder->parameters['typ'] = DecyzjaSzczegolyTyp::MINUS;
	$szczegolBuilder->parameters['wartosc'] = 9;
	$szczegolBuilder->make();

	$this->decyzjeMenager->aktualizujWynik($decyzja);
	$zmodyfikowany = $this->decyzjeMenager->getRekord($decyzja->id);
	$this->assertEquals(DecyzjeWyniki::NIE, $zmodyfikowany->wynik);
    }

    public function test_zapiszDecyzje() {
	$this->utworzIZalogujUzytkownika();

	$post = $this->arrayToParameters($this->zrobDomyslegoPosta());

	$wynikPrzetwarzania = $this->decyzjeMenager->zapiszDecyzje($post);

	$wynik = $this->decyzjeMenager->getPoprzednioDodany();

	$this->assertNotNull($wynik->id);
	$this->assertEquals('Nazwa decyzji', $wynik->nazwa);

	$szczegoly = $this->sm->get(DecyzjeSzczegolyMenager::class)->pobierzDlaDecyzji($wynik->id);

	$this->assertEquals(4, count($szczegoly));
	$this->assertEquals(DecyzjeWyniki::NIE, $wynik->wynik);
	$this->assertEquals(-2, $wynik->przeliczWynik());

	$this->sprawdzWynik($wynikPrzetwarzania);
    }

    public function test_zapiszDecyzje_aktualizacjaPosta() {
	$this->utworzIZalogujUzytkownika();

	$arr = $this->zrobDomyslegoPosta();
	$this->decyzjeMenager->zapiszDecyzjeTry($this->arrayToParameters($arr));
	$pierwszyZapis = $this->decyzjeMenager->getPoprzednioDodany();

	$arr['id'] = $pierwszyZapis->id;
	$arr['waga_p2'] = '7';
	$arr['opis_p2'] = 'nowy opis';
	$arr['opis_m0'] = '';
	$arr['waga_m0'] = '';
	$this->decyzjeMenager->zapiszDecyzjeTry($this->arrayToParameters($arr));

	$wynik = $this->decyzjeMenager->getRekord($pierwszyZapis->id);
	$szczegoly = $this->sm->get(DecyzjeSzczegolyMenager::class)->pobierzDlaDecyzji($pierwszyZapis->id);

	$this->assertEquals(4, count($szczegoly));
	$this->assertEquals(DecyzjeWyniki::TAK, $wynik->wynik);
	$this->assertEquals(7, $wynik->przeliczWynik());
    }

    public function test_nieaktywne_konto() {
	$this->utworzIZalogujUzytkownika();

	$this->decyzjeMenager = $this->zrobMockObiektu(\Decyzje\Menager\DecyzjeMenager::class, ['czyWazneKonto']);

	$this->decyzjeMenager->method('czyWazneKonto')->willReturn(false);

	$wynik = $this->decyzjeMenager->zapiszDecyzje($this->arrayToParameters([]));

	$this->assertEquals(false, $wynik->success);
	$this->assertEquals(1, count($wynik->getMsgs()));
    }

    public function test_zapiszRoboczeDecyzje() {
	$post = $this->arrayToParameters(['id' => "55", 'opis_p0' => "", "waga_p1" => "44"]);

	$this->decyzjeMenager->zapiszRoboczeDecyzje($post);
	$decyzja = $this->decyzjeMenager->pobierzRoboczeDecyzje();

	$this->assertEquals(55, $decyzja->id);
    }

    private function sprawdzWynik($odpowiedz) {
	$this->assertEquals(true, $odpowiedz->success);

	$array = $odpowiedz->toArray();

	$this->assertArrayHasKey('decyzja', $array);
	$this->assertArrayHasKey('szczegoly', $array);

	$json = json_encode($array);

	$this->sprawdzStringNieZawiera($json, 'nie+a');
    }

    private function zrobDomyslegoPosta() {
	return ['id' => "",
		'nazwa' => 'Nazwa decyzji',
		'opis_p0' => 'Opis p0',
		'waga_p0' => '2',
		'opis_p1' => "Opis p1",
		'waga_p1' => "3",
		'opis_p2' => "",
		'waga_p2' => "",
		'opis_m0' => 'Opis m 0',
		'waga_m0' => '2',
		'opis_m1' => "Opis p1",
		'waga_m1' => "5",
		'opis_m2' => "",
		'waga_m2' => ""];
    }

}
