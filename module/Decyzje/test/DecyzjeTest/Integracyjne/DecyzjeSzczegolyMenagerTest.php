<?php

namespace DecyzjeTest\Integracyjne;

use Pastmo\Testy\Util\TB;

class DecyzjeSzczegolyMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_dodajRekord() {
	$decyzjaSzczegol = TB::create(\Decyzje\Entity\DecyzjaSzczegoly::class, $this)->setPF_IZP($this->sm)->make();

	$decyzja = $this->sm->get(\Decyzje\Menager\DecyzjeMenager::class)->getPoprzednioDodany();
	$this->assertNotNull($decyzjaSzczegol->id);
	$this->assertEquals(\Decyzje\Enumy\DecyzjaSzczegolyTyp::PLUS, $decyzjaSzczegol->typ);
	$this->assertEquals($decyzja->id, $decyzjaSzczegol->decyzja->id);
    }

    public function test_pobierzDlaDecyzji() {
	$decyzjaSzczegol = TB::create(\Decyzje\Entity\DecyzjaSzczegoly::class, $this)->setPF_IZP($this->sm)->make();

	$szczegoly = $this->sm->get(\Decyzje\Menager\DecyzjeSzczegolyMenager::class)->pobierzDlaDecyzji($decyzjaSzczegol->decyzja->id);
	$this->assertEquals(1, count($szczegoly));
    }

}
