<?php

namespace DecyzjeTest\Entity;

use Decyzje\Entity\Decyzja;
use Pastmo\Testy\Util\TB;
use Decyzje\Enumy\DecyzjaSzczegolyTyp;

class DecyzjaTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    public function test_toJson() {
	$decyzja = new Decyzja();

	$decyzja->id = "id_decyzji";
	$decyzja->setDecyzjeSzczegoly(['nie_wyswietlany']);

	$wynik = json_encode($decyzja);

	$this->sprawdzStringZawiera($wynik, 'id_decyzji');
	$this->sprawdzStringNieZawiera($wynik, 'nie_wyswietlany');
    }

    public function test_getDecyzjePlusRozszerzone() {
	$decyzja = new Decyzja($this->sm);

	$builder = TB::create(\Decyzje\Entity\DecyzjaSzczegoly::class, $this)
		->setParameters(['selektor_html' => '_p4', 'typ' => DecyzjaSzczegolyTyp::PLUS]);
	$sz1 = $builder->make();
	$builder->parameters['selektor_html'] = '_p6';
	$sz2 = $builder->make();

	$this->decyzjeSzczegolyMenager->method('pobierzDlaDecyzji')->willReturn(['_p4' => $sz1, '_p6' => $sz2]);
	$szczegoly = $decyzja->getDecyzjePlusRozszerzone();

	$this->assertEquals(7, count($szczegoly));
	$this->assertEquals($sz1->id, $szczegoly['_p4']->id);
	$this->assertEquals($sz2->id, $szczegoly['_p6']->id);
    }

    public function test_fromArraySesji_getDecyzjePlus() {
	$array = ['id' => "",
		'nazwa' => 'Nazwa decyzji',
		'opis_p0' => 'Opis p0',
		'waga_p0' => '2',
		'opis_p1' => "Opis p1",
		'waga_p1' => "3",
		'opis_p2' => "",
		'waga_p2' => "",
		'opis_m0' => 'Opis m 0',
		'waga_m0' => '2',
		'opis_m1' => "Opis p1",
		'waga_m1' => "5",
		'opis_m2' => "",
		'waga_m2' => ""];

	$decyzja = Decyzja::fromArraySesji($array);

	$szczegoly = $decyzja->getDecyzjePlusRozszerzone();

	$this->assertEquals(6, count($szczegoly));
	$this->assertEquals("Opis p1", $szczegoly['_p1']->opis);
	$this->assertEquals(3, $szczegoly['_p1']->wartosc);
    }

}
