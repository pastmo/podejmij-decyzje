<?php

namespace Decyzje;

return array(
	'controllers' => array(
		'factories' => array(
			'Decyzje\Controller\Decyzje' => Controller\Factory\Factory::class,
			'Decyzje\Controller\DecyzjeAjax' => Controller\Factory\AjaxFactory::class,
		),
	),
	'router' => array(
		'routes' => array(
			'decyzje' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/decyzje[/:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Decyzje\Controller\Decyzje',
						'action' => 'index',
					),
				),
			),
			'decyzje_ajax' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/decyzje_ajax[/:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Decyzje\Controller\DecyzjeAjax',
						'action' => 'index',
					),
				),
			),
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Decyzje' => __DIR__ . '/../view',
		),
	),
);
