<?php

namespace Decyzje\Entity;

class OdpowiedzZapisuDecyzji extends \Pastmo\Wspolne\Entity\DomyslnaOdpowiedzPrzetwarzania {

    public $decyzja;
    public $szczegoly;

    public static function create() {
	return new self();
    }

    public function toArray() {
	$parent = parent::toArray();

	return array_merge($parent, ['decyzja' => $this->decyzja, 'szczegoly' => $this->szczegoly]);
    }

    function setDecyzja($decyzja) {
	$this->decyzja = $decyzja;
    }

    function setSzczegoly($szczegoly) {
	$this->szczegoly = $szczegoly;
    }

}
