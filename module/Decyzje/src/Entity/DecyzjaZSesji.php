<?php

namespace Decyzje\Entity;

use Decyzje\Enumy\DecyzjaSzczegolyTyp;
use Pastmo\Wspolne\Utils\ArrayUtil;

class DecyzjaZSesji extends Decyzja {

    private $arrayWejsciowa;

    public function __construct($arrayWejsciowa) {
	$this->arrayWejsciowa = $arrayWejsciowa;
	$this->exchangeArray($this->arrayWejsciowa);
    }

    public static function utworz($arrayWejsciowa) {
	$wynik = new DecyzjaZSesji($arrayWejsciowa);

	return $wynik;
    }

    public function getDecyzjeSzczegoly($czyPrzeladowywac = false) {
	if (!$this->decyzjeSzczegoly) {
	    $this->decyzjeSzczegoly = [];

	    foreach ($this->arrayWejsciowa as $klucz => $wartosc) {
		$pole = PolePrzetwarzania::create($klucz);

		if ($pole->czyPoleSzczegolow) {
		    ArrayUtil::ustawWartoscPodKluczemJesliNieIstnieje($this->decyzjeSzczegoly, $pole->selektor,
			    $pole->getPustaEncje()
		    );

		    $poleSzczegolow = $pole->poleSzczegolow;
		    $this->decyzjeSzczegoly[$pole->selektor]->$poleSzczegolow = $wartosc;
		}
	    }
	}

	return $this->decyzjeSzczegoly;
    }

}

class PolePrzetwarzania {

    public $czyPoleSzczegolow;
    public $selektor;
    public $poleSzczegolow;
    public $typ;

    public static function create($klucz) {
	$wynik = new PolePrzetwarzania();
	$wynik->ustawSelektor($klucz);
	$wynik->ustawTyp();
	$wynik->czyPoleSzczegolow = $wynik->selektor !== false;

	return $wynik;
    }

    public function getPustaEncje() {
	$szczegoly = new DecyzjaSzczegoly();
	$szczegoly->typ = $this->typ;
	$szczegoly->selektor_html = $this->selektor;
	return $szczegoly;
    }

    private function ustawTyp() {
	if ($this->selektor[1] == 'p') {
	    $this->typ = DecyzjaSzczegolyTyp::PLUS;
	} else if ($this->selektor[1] == 'm') {
	    $this->typ = DecyzjaSzczegolyTyp::MINUS;
	}
    }

    private function ustawSelektor($klucz) {
	$arr = explode("_", $klucz);

	if (count($arr) === 2) {
	    $this->selektor = "_" . $arr[1];
	    $this->poleSzczegolow = $arr[0] === "waga" ? "wartosc" : $arr[0];
	} else {
	    $this->selektor = false;
	}
    }

}
