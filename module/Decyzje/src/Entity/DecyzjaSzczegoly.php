<?php

namespace Decyzje\Entity;

class DecyzjaSzczegoly extends \Wspolne\Model\WspolneModel {

    public $id;
    public $decyzja_id;
    public $typ;
    public $opis;
    public $selektor_html;
    public $wartosc;

    public function exchangeArray($data) {
	$this->data = $data;

	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->decyzja_id = $this->pobierzLubNull($data, 'decyzja_id');
	$this->typ = $this->pobierzLubNull($data, 'typ');
	$this->opis = $this->pobierzLubNull($data, 'opis');
	$this->selektor_html = $this->pobierzLubNull($data, 'selektor_html');
	$this->wartosc = $this->pobierzLubNull($data, 'wartosc');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'decyzja':
		return 'decyzja_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function getEncjeTabeliObcej($name) {
	switch ($name) {
	    case 'decyzja':
		return $this->getEncjeZTabeliObcej('decyzja_id', \Decyzje\Menager\DecyzjeMenager::class, new Decyzja());
	}
    }

    public function getWartosc() {
	if ($this->typ === \Decyzje\Enumy\DecyzjaSzczegolyTyp::PLUS) {
	    return $this->wartosc;
	} else {
	    return -$this->wartosc;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
