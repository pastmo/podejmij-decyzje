<?php

namespace Decyzje\Entity;

use Decyzje\Enumy\DecyzjaSzczegolyTyp;

class Decyzja extends \Wspolne\Model\WspolneModel {

    public $id;
    public $uzytkownik_id;
    public $nazwa;
    public $data_dodania;
    public $wynik;
    protected $decyzjeSzczegoly;

    public function exchangeArray($data) {
	$this->data = $data;

	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->uzytkownik_id = $this->pobierzLubNull($data, 'uzytkownik_id');
	$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
	$this->data_dodania = $this->pobierzLubNull($data, 'data_dodania');
	$this->wynik = $this->pobierzLubNull($data, 'wynik');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'uzytkownik':
		return 'uzytkownik_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function getEncjeTabeliObcej($name) {
	switch ($name) {
	    case 'uzytkownik':
		return $this->getEncjeZTabeliObcej('uzytkownik_id', \Logowanie\Menager\UzytkownicyMenager::class,
				\Logowanie\Model\Uzytkownik::create());
	}
    }

    public function getDecyzjeSzczegoly($czyPrzeladowywac = false) {
	$this->pobierzListeZTabeliObcej(\Decyzje\Menager\DecyzjeSzczegolyMenager::class, 'pobierzDlaDecyzji',
		$this->id, null, 'decyzjeSzczegoly', $czyPrzeladowywac);
	return $this->decyzjeSzczegoly;
    }

    public function pobierzSzczegol($selektorHtml) {
	$szczegoly = $this->getDecyzjeSzczegoly();

	if (isset($szczegoly[$selektorHtml])) {
	    return $szczegoly[$selektorHtml];
	} else {
	    return new DecyzjaSzczegoly();
	}
    }

    public function przeliczWynik() {
	$szczegoly = $this->getDecyzjeSzczegoly(true);

	$wynik = 0;
	foreach ($szczegoly as $szczegol) {
	    $wynik+=$szczegol->getWartosc();
	}
	return $wynik;
    }

    public function getDecyzjePlus() {
	return $this->getSzczegolyTypu(DecyzjaSzczegolyTyp::PLUS);
    }

    public function getDecyzjeMinus() {
	return $this->getSzczegolyTypu(DecyzjaSzczegolyTyp::MINUS);
    }

    public function getDecyzjePlusRozszerzone() {
	return $this->getDecyzjeRozrzerzone(DecyzjaSzczegolyTyp::PLUS, "_p");
    }

    public function getDecyzjeMinusRozszerzone() {
	return $this->getDecyzjeRozrzerzone(DecyzjaSzczegolyTyp::MINUS, "_m");
    }

    private function getDecyzjeRozrzerzone($typ, $sel) {
	$noweDecyzje = $this->getSzczegolyTypu($typ);
	$puste = $this->utworzPusteDecyzje($sel);
	return array_merge($puste, $noweDecyzje);
    }

    private function getSzczegolyTypu($typ) {
	$szczegoly = $this->getDecyzjeSzczegoly();

	$wynik = [];

	foreach ($szczegoly as $szczegol) {
	    if ($szczegol->typ === $typ) {
		$wynik[$szczegol->selektor_html] = $szczegol;
	    }
	}

	return $wynik;
    }

    public function utworzPusteDecyzje($sel) {
	$wynik = [];
	for ($i = 0; $i < 6; $i++) {
	    $selektor = $sel . $i;
	    $wynik[$selektor] = new DecyzjaSzczegoly();
	}
	return $wynik;
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public function setDecyzjeSzczegoly($szczegoly) {
	$this->decyzjeSzczegoly = $szczegoly;
    }

    public static function fromArraySesji($post) {
	$wynik = DecyzjaZSesji::utworz($post);
	return $wynik;
    }

}
