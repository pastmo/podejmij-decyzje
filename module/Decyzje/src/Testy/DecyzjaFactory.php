<?php

namespace Decyzje\Testy;

class DecyzjaFactory extends \Pastmo\Testy\Fabryki\FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Decyzje\Entity\Decyzja($this->parametryFabryki->sm);
	$encja->nazwa = __CLASS__;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Decyzje\Menager\DecyzjeMenager::class;
    }

    public function dowiazInneTabele() {

    }

}
