<?php

namespace Decyzje\Testy;

class DecyzjaSzczegolyFactory extends \Pastmo\Testy\Fabryki\FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Decyzje\Entity\DecyzjaSzczegoly($this->parametryFabryki->sm);
	$encja->opis = __CLASS__;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Decyzje\Menager\DecyzjeSzczegolyMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->decyzja = $this->uzyjWlasciwejFabryki(\Decyzje\Entity\Decyzja::class);
    }

}
