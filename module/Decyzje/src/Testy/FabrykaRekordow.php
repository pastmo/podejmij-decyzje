<?php
namespace Decyzje\Testy;

class FabrykaRekordow {

    public static function makeEncje($klasa, $parametryFabryki) {
	switch ($klasa) {
	    case \Decyzje\Entity\Decyzja::class:
		return (new DecyzjaFactory($parametryFabryki))->makeEncje();
	    case \Decyzje\Entity\DecyzjaSzczegoly::class:
		return (new DecyzjaSzczegolyFactory($parametryFabryki))->makeEncje();
	}
	return false;
    }

}
