<?php

namespace Decyzje\Testy;

trait Mocki {

    public $decyzjeMenager;
    public $decyzjeSzczegolyMenager;

    public function mockujDecyzje() {

	$this->decyzjeMenager = $this->getMockBuilder(\Decyzje\Menager\DecyzjeMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->decyzjeSzczegolyMenager = $this->getMockBuilder(\Decyzje\Menager\DecyzjeSzczegolyMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->sm->setService(\Decyzje\Menager\DecyzjeSzczegolyMenager::class, $this->decyzjeSzczegolyMenager);
	$this->sm->setService(\Decyzje\Menager\DecyzjeMenager::class, $this->decyzjeMenager);
    }

}
