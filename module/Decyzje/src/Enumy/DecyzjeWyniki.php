<?php

namespace Decyzje\Enumy;

class DecyzjeWyniki {

    const TAK = 'tak';
    const NIE = 'nie';
    const NIEROZSTRZYGNIETY = 'nierozstrzygniety';

}
