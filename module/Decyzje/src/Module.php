<?php

namespace Decyzje;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module implements ConfigProviderInterface {

    use \Pastmo\FunkcjeKonfiguracyjne;

    const DECYZJE = "DECYZJE";
    const DECYZJE_SZCZEGOLY = "DECYZJE_SZCZEGOLY";

    public function getConfig() {
	return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig() {
	return array(
		'factories' => array(
			\Decyzje\Fasada\DecyzjeFasada::class => function($sm) {
			    $table = new \Decyzje\Fasada\DecyzjeFasada($sm);
			    return $table;
			},
			Menager\DecyzjeMenager::class => function($sm) {
			    $table = new Menager\DecyzjeMenager($sm);
			    return $table;
			},
			Menager\DecyzjeSzczegolyMenager::class => function($sm) {
			    $table = new Menager\DecyzjeSzczegolyMenager($sm);
			    return $table;
			},
			self::DECYZJE => function($sm) {
			    return $this->ustawGateway($sm, Entity\Decyzja::class, 'decyzje');
			},
			self::DECYZJE_SZCZEGOLY => function($sm) {
			    return $this->ustawGateway($sm, Entity\DecyzjaSzczegoly::class, 'decyzje_szczegoly');
			}
	));
    }

    public static function dowiazMenagery(\Wspolne\Controller\Factory\InitObiekt $initObiekt,
	    \Interop\Container\ContainerInterface $container) {

	$initObiekt->decyzjeFasada = $container->get(\Decyzje\Fasada\DecyzjeFasada::class);
    }

}
