<?php

namespace Decyzje\Menager;

use Zend\ServiceManager\ServiceManager;
use Decyzje\Enumy\DecyzjeWyniki;
use Decyzje\Enumy\DecyzjaSzczegolyTyp;
use Decyzje\Entity\OdpowiedzZapisuDecyzji;
use Pastmo\Wspolne\Utils\EntityUtil;
use Pastmo\Wspolne\Exception\PastmoException;

class DecyzjeMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    private $przetwarzanaDecyzja;
    private $wynik;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Decyzje\Module::DECYZJE);
    }

    public function zapisz(\Pastmo\Wspolne\Entity\WspolnyModel $model) {
	if (!$model->id) {
	    $model->ustawPole('uzytkownik', $this->get(\Wspolne\Menager\SesjaMenager::class)->pobierzIdUzytkownika());
	}
	parent::zapisz($model);
    }

    public function aktualizujWynik($decyzja) {
	$wartosc = $decyzja->przeliczWynik();

	if ($wartosc > 0) {
	    $wynik = DecyzjeWyniki::TAK;
	} else if ($wartosc < 0) {
	    $wynik = DecyzjeWyniki::NIE;
	} else {
	    $wynik = DecyzjeWyniki::NIEROZSTRZYGNIETY;
	}

	if ($decyzja->wynik !== $wynik) {
	    $decyzja->wynik = $wynik;
	    $this->zapisz($decyzja);
	}
    }

    public function pobierzDecyzjeZalogowanego() {
	$zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();
	return $this->pobierzZWherem("uzytkownik_id={$zalogowany->id}");
    }

    public function zapiszDecyzje($post) {
	try {

	    $this->beginTransaction();
	    $this->zapiszDecyzjeTry($post);
	    $this->commit();
	} catch (PastmoException $e) {
	    $this->rollback();
	    $this->wynik->setSuccess(false);
	    $this->wynik->addMsg($e->getMessage());
	    $this->logujWyjatek($e);
	} catch (\Exception $e) {
	    $this->rollback();
	    $this->wynik->setSuccess(false);
	    $this->wynik->addMsg("Wystąpił nieznany błąd");
	    $this->logujWyjatek($e);
	}

	return $this->wynik;
    }

    public function zapiszRoboczeDecyzje($post) {
	$this->sm->get(\Wspolne\Menager\SesjaMenager::class)->zapiszRoboczeDecyzje($post->toArray());
    }

    public function pobierzRoboczeDecyzje() {
	$post = $this->sm->get(\Wspolne\Menager\SesjaMenager::class)->pobierzRoboczeDecyzje();
	return \Decyzje\Entity\Decyzja::fromArraySesji($post);
    }

    public function zapiszDecyzjeTry($post) {
	$this->wynik = OdpowiedzZapisuDecyzji::create();

	$this->sprawdzWaznoscKonta();

	$this->post = $post;
	$nazwa = $this->post->nazwa;
	$id = $this->post->id;

	if ($id) {
	    $this->przetwarzanaDecyzja = $this->getRekord($id);
	} else {
	    $nowa = new \Decyzje\Entity\Decyzja();
	    $this->zapisz($nowa);
	    $this->przetwarzanaDecyzja = $this->getPoprzednioDodany();
	}

	$this->przetwarzanaDecyzja->nazwa = $nazwa;

	$this->zapisz($this->przetwarzanaDecyzja);

	$this->dodajSzczegoly(DecyzjaSzczegolyTyp::PLUS, '_p');
	$this->dodajSzczegoly(DecyzjaSzczegolyTyp::MINUS, '_m');

	$this->aktualizujWynik($this->przetwarzanaDecyzja);

	$this->wynik->setDecyzja($this->przetwarzanaDecyzja);
	$this->wynik->setSzczegoly($this->przetwarzanaDecyzja->getDecyzjeSzczegoly());
    }

    private function dodajSzczegoly($typ, $przedrostek) {
	for ($i = 0;; $i++) {
	    $selektorHtml = $przedrostek . $i;
	    $klucz = 'waga' . $selektorHtml;
	    if (isset($this->post->$klucz)) {
		$opisKlucz = 'opis' . $przedrostek . $i;

		$waga = $this->post->$klucz;
		$opis = $this->post->$opisKlucz;

		$szczegol = $this->get(DecyzjeSzczegolyMenager::class)->pobierzSzczegol($this->przetwarzanaDecyzja->id,
			$selektorHtml);

		if ($waga == "" && EntityUtil::czyEncjaZId($szczegol)) {

		    $this->get(DecyzjeSzczegolyMenager::class)->usunPoId($szczegol->id);
		} elseif ($waga != "") {

		    $szczegol->wartosc = $waga;
		    $szczegol->opis = $opis;
		    $szczegol->typ = $typ;

		    $this->get(DecyzjeSzczegolyMenager::class)->zapisz($szczegol);
		}
	    } else {
		break;
	    }
	}
    }

    public function sprawdzWaznoscKonta() {
        return;
	if (!$this->czyWazneKonto()) {
	    throw new PastmoException("Prosimy o opłacenie konta");
	}
    }

}
