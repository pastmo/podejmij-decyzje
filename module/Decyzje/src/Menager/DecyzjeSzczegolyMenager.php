<?php

namespace Decyzje\Menager;

use Zend\ServiceManager\ServiceManager;
use Pastmo\Wspolne\Utils\SearchPole;

class DecyzjeSzczegolyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    const ORDER_SELEKTORA = "selektor_html ASC";

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Decyzje\Module::DECYZJE_SZCZEGOLY);
    }

    public function pobierzDlaDecyzji($decyzjaId) {
	$resultSet = $this->pobierzResultSetZWherem($this->getWhereDecyzji($decyzjaId), self::ORDER_SELEKTORA);

	$wynik = [];
	foreach ($resultSet as $i => $rekord) {
	    $klucz = $rekord->selektor_html !== null ? $rekord->selektor_html : $i;
	    $wynik[$klucz] = $rekord;
	}

	return $wynik;
    }

    public function pobierzSzczegol($decyzjaId, $selektorHtml) {
	$where = $this->getWhereDecyzji($decyzjaId);
	$where.=" AND selektor_html='$selektorHtml'";

	$wynik = $this->pobierzZWherem($where);
	if (count($wynik) > 0) {
	    return $wynik[0];
	} else {
	    $szczegol = new \Decyzje\Entity\DecyzjaSzczegoly($this->sm);
	    $szczegol->ustawPole('decyzja', $decyzjaId);
	    $szczegol->selektor_html = $selektorHtml;
	    return $szczegol;
	}
    }

    private function getWhereDecyzji($decyzjaId) {
	return "decyzja_id=$decyzjaId";
    }

}
