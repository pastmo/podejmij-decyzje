<?php

namespace Decyzje\Controller;

use Zend\View\Model\ViewModel;
use \Logowanie\Enumy\KodyUprawnien;
use Decyzje\Entity\Decyzja;

class DecyzjeController extends \Wspolne\Controller\WspolneController {

    protected $zasobyTable;

    public function indexAction() {

	$id = $this->params()->fromRoute('id', false);
	$msg = $this->params()->fromPost('msg', false);

	if ($id) {
	    $this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	    $decyzja = $this->decyzjeFasada->getRekordDecyzji($id);
	} else {
	    $decyzja = $this->decyzjeFasada->pobierzRoboczeDecyzje();
	}

	return new ViewModel(array(
		'decyzja' => $decyzja,
		'msg' => $msg
	));
    }

    public function decyzjeAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$decyzje = $this->decyzjeFasada->pobierzDecyzjeZalogowanego();

	return new ViewModel(array(
		'decyzje' => $decyzje
	));
    }

    public function decyzjeSzczegolyAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$this->ustawBialyLayout();

	$id = $this->params()->fromRoute('id');
	$decyzja = $this->decyzjeFasada->getRekordDecyzji($id);

	return new ViewModel(array(
		'decyzja' => $decyzja
	));
    }

}
