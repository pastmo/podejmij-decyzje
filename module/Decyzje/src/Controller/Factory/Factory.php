<?php

namespace Decyzje\Controller\Factory;

use Decyzje\Controller\DecyzjeController;

class Factory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new DecyzjeController();
    }

}
