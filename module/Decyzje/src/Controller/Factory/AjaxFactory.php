<?php

namespace Decyzje\Controller\Factory;

class AjaxFactory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new \Decyzje\Controller\DecyzjeAjaxController();
    }

}
