<?php

namespace Decyzje\Controller;

use \Logowanie\Enumy\KodyUprawnien;

/**
 * @DomyslnyJsonModel
 */
class DecyzjeAjaxController extends \Wspolne\Controller\WspolneController {

    public function zapiszDecyzjeAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$post = $this->getPost();
	$wynik = $this->decyzjeFasada->zapiszDecyzje($post);

	return $this->returnJsonModel($wynik->toArray());
    }
    public function zapiszRoboczeDecyzjeAction() {

	$post = $this->getPost();
	$this->decyzjeFasada->zapiszRoboczeDecyzje($post);

	return $this->returnSuccess();
    }

}
