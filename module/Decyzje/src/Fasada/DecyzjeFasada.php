<?php

namespace Decyzje\Fasada;

class DecyzjeFasada extends \Wspolne\Fasada\WspolneFasada {

    public function __construct(\Zend\ServiceManager\ServiceManager $serviceMenager) {
	parent::__construct($serviceMenager);
    }

    public function pobierzDecyzjeZalogowanego() {
	return $this->get(\Decyzje\Menager\DecyzjeMenager::class)->pobierzDecyzjeZalogowanego();
    }

    public function getRekordDecyzji($id) {
	return $this->get(\Decyzje\Menager\DecyzjeMenager::class)->getRekord($id);
    }

    public function zapiszDecyzje($post) {
	return $this->get(\Decyzje\Menager\DecyzjeMenager::class)->zapiszDecyzje($post);
    }

    public function zapiszRoboczeDecyzje($post) {
	$this->get(\Decyzje\Menager\DecyzjeMenager::class)->zapiszRoboczeDecyzje($post);
    }

    public function pobierzRoboczeDecyzje() {
	return $this->get(\Decyzje\Menager\DecyzjeMenager::class)->pobierzRoboczeDecyzje();
    }

}
