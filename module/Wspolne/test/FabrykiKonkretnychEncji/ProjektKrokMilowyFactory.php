<?php

class ProjektKrokMilowyFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new \Projekty\Entity\ProjektKrokMilowy();
		$encja->krokMilowy="neg0";
		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return \Projekty\Menager\ProjektyKrokiMiloweMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();
	}

}
