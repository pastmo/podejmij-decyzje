<?php

class PudelkoFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$this->encja = new \Wysylka\Entity\Pudelko();
		$losowa_liczba = rand(1, 3);
		switch ($losowa_liczba):
			case 1:
				$this->encja->wielkosc = 'Małe';
				break;
			case 2:
				$this->encja->wielkosc = 'Średnie';
				break;
			case 3:
				$this->encja->wielkosc = 'Duże';
				break;
		endswitch;
		$this->encja->kod = "kod";
		return $this->encja;
	}

	public function pobierzNazweTableMenager() {
		return \Wysylka\Menager\PudelkaMenager::class;
	}

	public function dowiazInneTabele() {

	}

}
