<?php

class WiadomoscFactory extends FabrykaAbstrakcyjna {

    public $innyUser;

    public function nowaEncja() {
	$encja = new Konwersacje\Entity\Wiadomosc($this->parametryFabryki->sm);
	$encja->tresc = "tresc";
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return Konwersacje\Model\WiadomosciMenager::class;
    }

    public function dowiazInneTabele() {
	parent::dowiazInneTabele();
	$this->encja->watek = $this->uzyjWlasciwejFabryki(\Konwersacje\Entity\WatekWiadomosci::class);
	$this->encja->autor = $this->uzyjWlasciwejFabryki(\Logowanie\Model\Uzytkownik::class);
    }

    public static function create($parametryFabryki) {
	return new WiadomoscFactory($parametryFabryki);
    }

    public function utworzWiadomoscZNowymUzytkownikiem() {
	$this->innyUser = $this->uzyjWlasciwejFabryki(\Logowanie\Model\Uzytkownik::class);
	$this->uzyjWlasciwejFabryki(\Konwersacje\Entity\Wiadomosc::class,array('autor' => $this->innyUser));
    }


}
