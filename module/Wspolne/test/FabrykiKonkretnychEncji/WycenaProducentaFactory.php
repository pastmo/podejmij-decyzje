<?php

class WycenaProducentaFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new \Aukcje\Entity\WycenaProducenta();
		$encja->nazwa = "nazwa";
		$encja->komentarz = "komentarz";
		$encja->data_wyceny = date("Y-m-d");
		$encja->data_realizacji = date("Y-m-d");
		$encja->status = \Aukcje\Enumy\StatusWycenaProducenta::SKLADANIE_OFERT;

		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return \Aukcje\Menager\WycenyProducentowMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();
	}

}
