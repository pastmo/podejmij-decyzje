<?php

class ProduktZnizkaFactory  extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new \Produkty\Entity\ProduktZnizka();
		$encja->wartosc=rand(1,100);
		$encja->od_ilu_sztuk=rand(1,1000);
		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return Produkty\Menager\ProduktyZnizkiMenager::class;
	}

}
