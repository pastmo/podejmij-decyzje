<?php

class WystawcaFakturFactory  extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new Pastmo\Faktury\Entity\WystawcaFaktur();
		$encja->akceptacja=true;
		$encja->imie_nazwisko = "imie nazwisko";
		$encja->miejsce_wystawienia = "Wrocław";
		$encja->nazwa = "nazwa";
		$encja->nip = "nip";
		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return Pastmo\Faktury\Menager\WystawcyFakturMenager::class;
	}

}
