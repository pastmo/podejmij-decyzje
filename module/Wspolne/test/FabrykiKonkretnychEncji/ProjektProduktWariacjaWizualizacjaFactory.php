<?php

use \Projekty\Entity\ProjektProduktWariacjaWizualizacja;
use \Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane;
use \Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane;

class ProjektProduktWariacjaWizualizacjaFactory extends FabrykaAbstrakcyjna {

	private $sm;

	public function nowaEncja() {
		$encja = new \Projekty\Entity\ProjektProduktWariacjaWizualizacja();
		$encja->akceptacja = true;
		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return \Projekty\Menager\ProjektyProduktyWariacjeWizualizacjeMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();

		$this->encja->projekt_produkt_wariacja = $this->uzyjWlasciwejFabryki(Projekty\Entity\ProjektProduktWariacja::class);
		$this->encja->zasob = $this->uzyjWlasciwejFabryki(Zasoby\Model\Zasob::class);
	}

	public static function create(\Zend\ServiceManager\ServiceManager $sm) {
		$projektFactory = new ProjektProduktWariacjaWizualizacjaFactory();
		$projektFactory->sm = $sm;
		return $projektFactory;
	}

	public function zrobPowiazanaNiezaakceptowanaIndywidualizacje() {
		return \FabrykaRekordow::makeEncje(ProjektProduktWariacjaWizualizacja::class,
						PFIntegracyjneZapisPowiazane::create($this->sm)
								->setparametry(array('akceptacja' => false, 'typ' => \Projekty\Enumy\TypyWizualizacji::INDYWIDUALIZACJA)));
	}

	public function zrobNiepowiazanaNiezaakceptowanaWizualizacje($projektWariacjaId) {
		return \FabrykaRekordow::makeEncje(ProjektProduktWariacjaWizualizacja::class,
						PFIntegracyjneZapisNiepowiazane::create($this->sm)
								->setparametry(array(
									'akceptacja' => false,
									'typ' => \Projekty\Enumy\TypyWizualizacji::WIZUALIZACJA,
									'projekt_produkt_wariacja' => $projektWariacjaId
		)));
	}

}
