<?php

class ZamowienieWewnetrzneFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new Magazyn\Entity\ZamowienieWewnetrzne();
		$encja->status = 'zamowione';
		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return Magazyn\Menager\ZamowieniaWewnetrzneMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();

		$this->encja->uzytkownik_zamawiajacy = $this->uzyjWlasciwejFabryki(Logowanie\Model\Uzytkownik::class);
		$this->encja->uzytkownik_konczacy = $this->uzyjWlasciwejFabryki(Logowanie\Model\Uzytkownik::class);
		$this->encja->magazyn_zrodlowy = $this->uzyjWlasciwejFabryki(\Magazyn\Entity\Magazyn::class);
		$this->encja->magazyn_docelowy = $this->uzyjWlasciwejFabryki(\Magazyn\Entity\Magazyn::class,
				array('typ' => Magazyn\Enumy\MagazynyTypy::BIURO));
	}



}
