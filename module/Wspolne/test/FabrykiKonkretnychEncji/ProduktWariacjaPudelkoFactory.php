<?php

class ProduktWariacjaPudelkoFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new \BazaProduktow\Entity\ProduktWariacjaPudelko();

		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return \BazaProduktow\Menager\ProduktyWariacjePudelkaMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();

		if ($this->parametryFabryki->czyFabrykaMockow) {
			$this->encja->produkt_wariacja = \FabrykaEncjiMockowych::utworzEncje(BazaProduktow\Entity\ProduktWariacja::class);
		} else {
			$this->encja->produkt_wariacja = \FabrykaRekordow::makeEncje(\BazaProduktow\Entity\ProduktWariacja::class, $this->parametryFabryki);
		}

		if ($this->parametryFabryki->czyFabrykaMockow) {
			$this->encja->pudelko = \FabrykaEncjiMockowych::utworzEncje(Wysylka\Entity\Pudelko::class);
		} else {
			$this->encja->pudelko = \FabrykaRekordow::makeEncje(\Wysylka\Entity\Pudelko::class, $this->parametryFabryki);
		}
	}

}
