<?php

class ParametryWizualizacjiFactory extends FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Projekty\Entity\ParametryWizualizacji();
	$encja->termin_wizualizacji_grafik_od = $this->generujDate();
	$encja->termin_wizualizacji_grafik_do = $this->generujDate(5);
	$encja->czas_pracy = 1;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return Projekty\Menager\ParametryWizualizacjiMenager::class;
    }

    public function dowiazInneTabele() {
	parent::dowiazInneTabele();

	$this->encja->grafik = $this->uzyjWlasciwejFabryki(Logowanie\Model\Uzytkownik::class,
		array(
	    'imie' => "Leon"
	));
    }

}
