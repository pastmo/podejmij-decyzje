<?php

class ProduktKategoriaFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$this->encja = new \Produkty\Entity\ProduktKategoria();

		return $this->encja;
	}

	public function pobierzNazweTableMenager() {
		return \Produkty\Menager\ProduktyKategorieMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();

		if ($this->parametryFabryki->czyFabrykaMockow) {
			$this->encja->produkt = \FabrykaEncjiMockowych::utworzEncje(Produkty\Entity\Produkt::class);
		} else {
			$this->encja->produkt = \FabrykaRekordow::makeEncje(
					Produkty\Entity\Produkt::class, $this->parametryFabryki);
		}
	}

}
