<?php

class PrzypomnienieFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new \Dashboard\Entity\Przypomnienie();

		$encja->tresc = 'To standardowa treść przypomnienia.';
		$encja->data_przypomnienia = date("Y-m-d H:i:s");

		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return Dashboard\Menager\PrzeniesPrzypomnieniaMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();

		$this->encja->uzytkownik = $this->uzyjWlasciwejFabryki(Logowanie\Model\Uzytkownik::class);
	}

}
