<?php

class PowiadomienieKategoriaFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new \Dashboard\Entity\PowiadomienieKategoria();

		$encja->nazwa = 'To standardowa nazwa.';
		$encja->url_modul = "URL MODUL";
		$encja->url_akcja = "URL AKCJA";

		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return Dashboard\Menager\PowiadomieniaKategorieMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();
	}

}
