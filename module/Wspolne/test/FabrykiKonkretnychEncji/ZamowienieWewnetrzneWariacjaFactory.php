<?php


class ZamowienieWewnetrzneWariacjaFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new \Magazyn\Entity\ZamowienieWewnetrzneWariacja();
		$encja->ilosc=  $this->genrujId();
		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return Magazyn\Menager\ZamowieniaWewnetrzneWariacjeMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();

		$this->encja->uzytkownik = $this->uzyjWlasciwejFabryki(Logowanie\Model\Uzytkownik::class);

	}


}