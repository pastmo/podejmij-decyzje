<?php

class PowiadomienieFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new \Dashboard\Entity\Powiadomienie();

		$encja->tresc = 'To standardowa treść powiadomienia.';
		$encja->url_id = rand(1,100);
		$encja->data_przypomnienia = date("Y-m-d H:i:s");

		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return Dashboard\Menager\PowiadomieniaMenager::class;
	}

}
