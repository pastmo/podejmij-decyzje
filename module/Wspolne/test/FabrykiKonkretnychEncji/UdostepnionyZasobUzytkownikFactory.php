<?php

class UdostepnionyZasobUzytkownikFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new \Zasoby\Entity\UdostepnionyZasobUzytkownik();

		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return Zasoby\Menager\UdostepnioneZasobyUzytkownicyMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();

		$this->encja->uzytkownik = $this->uzyjWlasciwejFabryki(Logowanie\Model\Uzytkownik::class);
		$this->encja->udostepniony_zasob = $this->uzyjWlasciwejFabryki(\Zasoby\Entity\UdostepnionyZasob::class);
	}

	public function uzyjWlasciwejFabryki($klasa, $parametry = array()) {

		return \FabrykaRekordow::makeEncje($klasa,
				\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->parametryFabryki->sm)->setparametry($parametry));
	}

}
