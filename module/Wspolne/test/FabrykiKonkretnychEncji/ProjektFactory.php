<?php

use \Projekty\Entity\Projekt;
use \Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane;
use \Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuPowiazane;
use \Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane;
use Pastmo\Testy\ParametryFabryki\PFMockowePowiazane;
use Zend\ServiceManager\ServiceManager;

class ProjektFactory extends FabrykaAbstrakcyjna {

    public $wiadomosc;
    public $projekt;
    public $watekWiadomosci;
    private $sm;

    public function nowaEncja() {
	$encja = \Projekty\Entity\Projekt::create($this->parametryFabryki->sm);
	$encja->nazwa = $this->getUnikalnaNazwaProjektu();

	$encja->firma = new Firmy\Entity\Firma();
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return Projekty\Menager\ProjektyMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->parametry_wizualizacji = $this->uzyjWlasciwejFabryki(Projekty\Entity\ParametryWizualizacji::class);
	$this->encja->sprzedawca = $this->uzyjWlasciwejFabryki(\Logowanie\Model\Uzytkownik::class);
	$this->encja->avatar = $this->uzyjWlasciwejFabryki(Zasoby\Model\Zasob::class);
	$this->encja->parametry_produkcji = $this->uzyjWlasciwejFabryki(Produkcja\Entity\ParametryProdukcji::class);
	$this->encja->adres_wysylki = $this->uzyjWlasciwejFabryki(\Firmy\Entity\Adres::class);
	$this->encja->dane_do_faktury = $this->uzyjWlasciwejFabryki(Projekty\Entity\DaneDoFaktury::class);
	$this->encja->firma = $this->uzyjWlasciwejFabryki(Firmy\Entity\Firma::class);
	$this->encja->watek_wiadomosci = $this->uzyjWlasciwejFabryki(\Konwersacje\Entity\WatekWiadomosci::class,array('typ'=>  \Konwersacje\Enumy\WatkiWiadomosciTypy::PROJEKTOWY));
    }

    protected function getUnikalnaNazwaProjektu() {
	if ($this->parametryFabryki->sm && !$this->parametryFabryki->czyFabrykaMockow) {
	    $projectMenager = $this->parametryFabryki->sm->get(\Projekty\Menager\ProjektyMenager::class);
	    $iloscProjektow = $projectMenager->pobierzZWheremCount('1');
	} else {
	    $iloscProjektow = FabrykaEncjiMockowych::genrujId();
	}

	$nazwa = 'nazwa' . $iloscProjektow;
	return $nazwa;
    }

    public static function create(ServiceManager $sm, $czyMockowa = false) {
	$pf = \Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::create($sm);
	$pf->czyFabrykaMockow = $czyMockowa;

	$projektFactory = new ProjektFactory($pf);
	$projektFactory->sm = $sm;
	return $projektFactory;
    }

    public function utworzRozbudowanyProjekt($zapisywacWiadomosc = false) {
	$this->watekWiadomosci = $this->uzyjWlasciwejFabryki(\Konwersacje\Entity\WatekWiadomosci::class);

	$this->projekt = $this->uzyjWlasciwejFabryki(Projekt::class,
		array('watek_wiadomosci' => $this->watekWiadomosci));

	$this->uzyjWlasciwejFabryki(\Projekty\Entity\ProjektKrokMilowy::class,
		array('krokMilowy' => 'neg1', 'projekt' => $this->projekt));
	if ($this->parametryFabryki->czyFabrykaMockow) {
	    $uzytkownik = $this->uzyjWlasciwejFabryki(Logowanie\Model\Uzytkownik::class);

	    $this->wiadomosc = $this->uzyjWlasciwejFabryki(\Konwersacje\Entity\Wiadomosc::class,
		    array('watek' => $this->watekWiadomosci, 'autor' => $uzytkownik));
	} else {
	    $fabrykaWiadomosci = $zapisywacWiadomosc ?
		    PFIntegracyjneZapisPowiazane::create($this->sm) : PFIntegracyjneBrakzapisuPowiazane::create($this->sm);

	    $this->wiadomosc = \FabrykaRekordow::makeEncje(\Konwersacje\Entity\Wiadomosc::class,
			    $fabrykaWiadomosci->setparametry(array('watek' => $this->watekWiadomosci)));
	}
    }

    public function utworzMockowyProjektZDanymiDoFaktury() {
	$daneDoFaktury = \FabrykaEncjiMockowych::makeEncje(\Projekty\Entity\DaneDoFaktury::class,
			PFMockowePowiazane::create());
	$this->projekt = \FabrykaEncjiMockowych::makeEncje(Projekt::class,
			PFMockowePowiazane::create()
				->setparametry(array('dane_do_faktury' => $daneDoFaktury)));


	return $this->projekt;
    }

    public function utworzAktywnyProjekt() {
	$this->projekt = \FabrykaEncjiMockowych::makeEncje(Projekt::class,
			PFMockowePowiazane::create()
				->setparametry(array('status' => Projekty\Enumy\ProjektyStatusy::AKTYWNY)));


	return $this->projekt;
    }

    public function utworzZamknietyProjekt($powodZamkniecia) {
	$this->projekt = \FabrykaEncjiMockowych::makeEncje(Projekt::class,
			PFMockowePowiazane::create()
				->setparametry(array(
				    'status' => Projekty\Enumy\ProjektyStatusy::ZAMKNIETY,
				    'powod_zamkniecia' => $powodZamkniecia)));


	return $this->projekt;
    }

}
