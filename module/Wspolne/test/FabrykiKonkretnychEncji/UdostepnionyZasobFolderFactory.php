<?php

class UdostepnionyZasobFolderFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new \Zasoby\Entity\UdostepnionyZasobFolder();
		$encja->nazwa = "Domyślna nazwa.";

		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return Zasoby\Menager\UdostepnioneZasobyFolderyMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();
	}

}
