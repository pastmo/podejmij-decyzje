<?php

use Pastmo\Testy\ParametryFabryki\PFMockowePowiazane;
use BazaProduktow\Entity\ProduktWariacja;

class ProjektProduktWariacjaFactory extends FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Projekty\Entity\ProjektProduktWariacja($this->parametryFabryki->sm);
	$encja->cena = rand(1, 500);
	$encja->ilosc = rand(1, 500);

	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Projekty\Menager\ProjektyProduktyWariacjeMenager::class;
    }

    public function dowiazInneTabele() {
	parent::dowiazInneTabele();

	$this->encja->projekt = $this->uzyjWlasciwejFabryki(\Projekty\Entity\Projekt::class);
	$this->encja->produkt_wariacja = $this->uzyjWlasciwejFabryki(ProduktWariacja::class);
    }

}
