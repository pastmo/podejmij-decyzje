<?php

class TagFactory  extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new \Projekty\Entity\Tag();
		$encja->nazwa="nazwa";
		$encja->kolor="kolor";
		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return Projekty\Menager\TagiMenager::class;
	}

}
