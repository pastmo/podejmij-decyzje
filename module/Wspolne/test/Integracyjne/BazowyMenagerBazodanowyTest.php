<?php

namespace WspolneTest\Integracyjne;

use Pastmo\Testy\Util\TB;

class BazowyMenagerBazodanowyTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public function setUp() {
	parent::setUp();
	$this->utworzIZalogujUzytkownika();
    }

    public function test_fetchAll() {
	$zlecenie = TB::create(\Zlecenia\Entity\Zlecenie::class, $this)->make();

	$zleceniaMenager = $this->sm->get(\Zlecenia\Model\ZleceniaTable::class);
	$wynik = $zleceniaMenager->pobierzWszystkoArray();

	$this->assertEquals(1, count($wynik));
    }

    public function test_pobierzZWherem() {
	$zlecenie = TB::create(\Zlecenia\Entity\Zlecenie::class, $this)
			->setParameters(['nazwa' => "zlecenie"])->make();

	$zleceniaMenager = $this->sm->get(\Zlecenia\Model\ZleceniaTable::class);
	$wynik = $zleceniaMenager->pobierzZWherem("nazwa='zlecenie'");

	$this->assertEquals(1, count($wynik));
    }

    public function test_fetchAll_brak_kolumny_konto_id() {
	$rola = TB::create(\Logowanie\Entity\Rola::class, $this)->make();

	$roleMenager = $this->sm->get(\Logowanie\Menager\RoleMenager::class);
	$wynik = $roleMenager->pobierzWszystkoArray();

	$this->assertGreaterThan(1, count($wynik));
    }

    public function test_getRekord_proba_pobrania_rekordu_z_innego_konta() {
	$zlecenie = TB::create(\Zlecenia\Entity\Zlecenie::class, $this)->make();
	$this->utworzIZalogujUzytkownika();

	$this->expectException(\Exception::class);

	$this->sm->get(\Zlecenia\Model\ZleceniaTable::class)->getRekord($zlecenie->id);
    }

    public function test_getLogowanieFasada() {
	$wynik = $this->sm->get(\Zlecenia\Model\ZleceniaTable::class)->getLogowanieFasada();
	$this->assertEquals(\Logowanie\Fasada\LogowanieFasada::class, get_class($wynik));
    }

    public function test_getEmaileFasada() {
	$wynik = $this->sm->get(\Zlecenia\Model\ZleceniaTable::class)->getEmaileFasada();
	$this->assertEquals(\Email\Fasada\EmailFasada::class, get_class($wynik));
    }

    public function test_getKonwersacjeFasada() {
	$wynik = $this->sm->get(\Zlecenia\Model\ZleceniaTable::class)->getKonwersacjeFasada();
	$this->assertEquals(\Konwersacje\Fasada\KonwersacjeFasada::class, get_class($wynik));
    }

    public function test_getZasobyFasada() {
	$wynik = $this->sm->get(\Zlecenia\Model\ZleceniaTable::class)->getZasobyFasada();
	$this->assertEquals(\Zasoby\Fasada\ZasobyFasada::class, get_class($wynik));
    }

}
