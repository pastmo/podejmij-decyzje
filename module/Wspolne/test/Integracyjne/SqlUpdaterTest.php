<?php

namespace WspolneTest\Integracyjne;

class SqlUpdaterTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

	public function setUp() {
		parent::setUp();
	}

	public function t_estusunNieuzywaneKategorieProdukotw() {
		$sqlUpdate = new \Wspolne\Menager\SqlUpdater($this->sm);

		for ($i = 0; $i < 5; $i++) {
			$sqlUpdate->usunNieuzywaneKategorieProdukotw();
		}

		$wynik = $this->sm->get(\Produkty\Menager\KategorieProduktuMenager::class)->pobierzZWherem("nazwa='Szkło'");
		$this->assertEquals(0,count($wynik));

	}

}
