<?php

namespace WspolneTest\Integracyjne;

use Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest;
use Pastmo\Wspolne\Utils\ArrayUtil;
use Wspolne\Menager\SesjaMenager;

class SesjaMenagerTest extends WspolnyIntegracyjnyTest {

    private $sesjaMenager;

    public function setUp() {
	parent::setUp();
	$this->sesjaMenager = new SesjaMenager($this->sm);
    }

    public function test_zapiszRoboczeDecyzje() {
	$pusty = $this->sesjaMenager->pobierzRoboczeDecyzje();
	$this->assertTrue(is_array($pusty));

	$post = $this->arrayToParameters(['id' => "55", 'opis_p0' => "", "waga_p1" => "44"]);

	$this->sesjaMenager->zapiszRoboczeDecyzje($post->toArray());
	$wynik = $this->sesjaMenager->pobierzRoboczeDecyzje();
	$this->assertNotNull($wynik);

    }

}
