<?php

use Logowanie\Model\Uzytkownik;
use Konwersacje\Entity\WatekWiadomosci;
use Konwersacje\Entity\UzytkownikWatkekWiadomosci;
use Konwersacje\Entity\Wiadomosc;
use Logowanie\Entity\Uprawnienie;
use Logowanie\Entity\Rola;
use Logowanie\Entity\UprawnienieRola;
use Pastmo\Email\Entity\KontoMailowe;

class FabrykaRekordow extends Pastmo\Testy\FabrykaRekordow {

    static $MODULY_Z_FABRYKAMI = [
	    \Pastmo\Testy\FabrykaRekordow::class,
	    \Uzytkownicy\Testy\FabrykaRekordow::class,
	    \Kontakt\Testy\FabrykaRekordow::class,
	    \Decyzje\Testy\FabrykaRekordow::class,
	    ];

    public static function makeEncje($klasa, $parametryFabryki) {

	$odModulow = self::encjeZModulow($klasa, $parametryFabryki);
	if ($odModulow) {
	    return $odModulow;
	}

	switch ($klasa) {
	    case \Logowanie\Entity\UzytkownikKontoMailowe::class:
		return (new Pastmo\Testy\FabrykiKonkretnychEncji\UzytkownikKontoMailoweFactory($parametryFabryki))->makeEncje();

	    case Pastmo\Email\Entity\RekordSzczegoluMaila::class:
		return (new Pastmo\Testy\FabrykiKonkretnychEncji\RekordSzczegoluMailaFactory($parametryFabryki))->makeEncje();
	    case \Dashboard\Entity\Przypomnienie::class:
		return (new PrzypomnienieFactory($parametryFabryki))->makeEncje();
	    case \Dashboard\Entity\Powiadomienie::class:
		return (new PowiadomienieFactory($parametryFabryki))->makeEncje();
	    case \Dashboard\Entity\PowiadomienieKategoria::class:
		return (new PowiadomienieKategoriaFactory($parametryFabryki))->makeEncje();
	    case \Dashboard\Entity\Pomoc::class:
		return (new PomocFactory($parametryFabryki))->makeEncje();
	    case \Zasoby\Entity\UdostepnionyZasob::class:
		return (new Testy\FabrykiKonkretnychEncji\UdostepnionyZasobFactory($parametryFabryki))->makeEncje();
	    case \Zasoby\Entity\UdostepnionyZasobFolder::class:
		return (new UdostepnionyZasobFolderFactory($parametryFabryki))->makeEncje();
	    case \Zasoby\Entity\UdostepnionyZasobUzytkownik::class:
		return (new UdostepnionyZasobUzytkownikFactory($parametryFabryki))->makeEncje();
	    case Pastmo\Faktury\Entity\WystawcaFaktur::class:
		return (new WystawcaFakturFactory($parametryFabryki))->makeEncje();
		return (new WystawcaFakturFactory($parametryFabryki))->makeEncje();
	    case \Projekty\Entity\ProjektKrokMilowy::class:
		return (new ProjektKrokMilowyFactory($parametryFabryki))->makeEncje();
	    case WatekWiadomosci::class:
		return (new WatekWiadomosciFactory($parametryFabryki))->makeEncje();
	    case \Projekty\Entity\ProjektProduktWariacjaWizualizacja::class:
		return (new ProjektProduktWariacjaWizualizacjaFactory($parametryFabryki))->makeEncje();
	    case Projekty\Entity\Tag::class:
		return (new TagFactory($parametryFabryki))->makeEncje();
	    case \Konwersacje\Entity\WiadomoscTag::class:
		return (new WiadomoscTagFactory($parametryFabryki))->makeEncje();
	    case \Produkcja\Entity\Urzadzenie::class:
		return (new \Testy\FabrykiKonkretnychEncji\UrzadzenieFactory($parametryFabryki))->makeEncje();
	    case Produkcja\Entity\ParametryProdukcji::class:
		return (new Testy\FabrykiKonkretnychEncji\ParametryProdukcjiFactory($parametryFabryki))->makeEncje();
	    case Rola::class:
		return (new \Testy\FabrykiKonkretnychEncji\RolaFactory($parametryFabryki))->makeEncje();
	    case \Konwersacje\Entity\UzytkownikWiadomosc::class:
		return (new Testy\FabrykiKonkretnychEncji\UzytkownikWiadomoscFactory($parametryFabryki))->makeEncje();
	    case \Produkty\Entity\KategoriaProduktu::class:
		return (new \Testy\FabrykiKonkretnychEncji\KategoriaProduktuFactory($parametryFabryki))->makeEncje();
	    case Projekty\Entity\DaneDoFaktury::class:
		return (new \Testy\FabrykiKonkretnychEncji\DaneDoFakturyFactory($parametryFabryki))->makeEncje();
	    case \Produkty\Entity\KategoriaLojalnosciowa::class:
		return (new \Testy\FabrykiKonkretnychEncji\KategoriaLojalnosciowaFactory($parametryFabryki))->makeEncje();
	    case Firmy\Entity\UzytkownikFirma::class:
		return (new \Testy\FabrykiKonkretnychEncji\UzytkownikFirmaFactory($parametryFabryki))->makeEncje();
	    case \Wysylka\Entity\Przesylka::class:
		return (new \Testy\FabrykiKonkretnychEncji\PrzesylkaFactory($parametryFabryki))->makeEncje();
	    case \Wysylka\Entity\PrzesylkaPaczka::class:
		return (new \Testy\FabrykiKonkretnychEncji\PrzesylkaPaczkaFactory($parametryFabryki))->makeEncje();
	    case \Wysylka\Entity\PrzesylkaUslugaDodatkowa::class:
		return (new \Testy\FabrykiKonkretnychEncji\PrzesylkaUslugaDodatkowaFactory($parametryFabryki))->makeEncje();
	    case \Produkty\Entity\ProduktZasob::class:
		return (new \Testy\FabrykiKonkretnychEncji\ProduktZasobFactory($parametryFabryki))->makeEncje();
	    case UzytkownikWatkekWiadomosci::class:
		return (new \Testy\FabrykiKonkretnychEncji\UzytkownikWatkekWiadomosciFactory($parametryFabryki))->makeEncje();
	    case \Aukcje\Entity\WycenaProducenta::class:
		return (new WycenaProducentaFactory($parametryFabryki))->makeEncje();
	    case \Aukcje\Entity\WycenaProducentaFirma::class:
		return (new Testy\FabrykiKonkretnychEncji\WycenaProducentaFirmaFactory($parametryFabryki))->makeEncje();
	    case \Aukcje\Entity\WycenaProducentaWariacja::class:
		return (new Testy\FabrykiKonkretnychEncji\WycenaProducentaWariacjaFactory($parametryFabryki))->makeEncje();
	    case \Aukcje\Entity\WycenaProducentaUzytkownik::class:
		return (new Testy\FabrykiKonkretnychEncji\WycenaProducentaUzytkownikFactory($parametryFabryki))->makeEncje();
	    case \Konwersacje\Entity\ProjektowyWatekChatu::class:
		return (new \Testy\FabrykiKonkretnychEncji\ProjektowyWatekChatuFactory($parametryFabryki))->makeEncje();
	    case \Konwersacje\Entity\ProjektowyWatekChatuUzytkownik::class:
		return (new \Testy\FabrykiKonkretnychEncji\ProjektowyWatekChatuUzytkownikFactory($parametryFabryki))->makeEncje();
	    case \Dashboard\Entity\UzytkownikPowiadomienieKategoria::class:
		return (new \Testy\FabrykiKonkretnychEncji\UzytkownikPowiadomienieKategoriaFactory($parametryFabryki))->makeEncje();
	    case \Dashboard\Entity\UzytkownikPowiadomienie::class:
		return (new \Testy\FabrykiKonkretnychEncji\UzytkownikPowiadomienieFactory($parametryFabryki))->makeEncje();
	    case Wizualizacje\Entity\CzasPracy::class:
		return (new Testy\FabrykiKonkretnychEncji\CzasPracyFactory($parametryFabryki))->makeEncje();
	    case \Projekty\Entity\ProjektOznaczenie::class:
		return (new \Testy\FabrykiKonkretnychEncji\ProjektOznaczenieFactory($parametryFabryki))->makeEncje();
	    case \Projekty\Entity\ProjektOznaczenieKategoria::class:
		return (new \Testy\FabrykiKonkretnychEncji\ProjektOznaczenieKategoriaFactory($parametryFabryki))->makeEncje();
	    case \Projekty\Entity\ProjektTag::class:
		return (new \Testy\FabrykiKonkretnychEncji\ProjektTagFactory($parametryFabryki))->makeEncje();
	    case \Projekty\Entity\KrokMilowy::class:
		return (new \Testy\FabrykiKonkretnychEncji\KrokMilowyFactory($parametryFabryki))->makeEncje();
	    case \Produkcja\Entity\ParametryProdukcjiUrzadzenie::class:
		return (new \Testy\FabrykiKonkretnychEncji\ParametryProdukcjiUrzadzenieFactory($parametryFabryki))->makeEncje();
	    case \Produkty\Entity\TypProduktu::class:
		return (new \Testy\FabrykiKonkretnychEncji\TypProduktuFactory($parametryFabryki))->makeEncje();
	    case \BazaProduktow\Entity\ProduktWariacja::class:
		return (new \Testy\FabrykiKonkretnychEncji\ProduktWariacjaFactory($parametryFabryki))->makeEncje();
	    case \Produkty\Entity\TypProduktuUrzadzenie::class:
		return (new \Testy\FabrykiKonkretnychEncji\TypProduktuUrzadzenieFactory($parametryFabryki))->makeEncje();

	    default :
		return self::utworzEncje(null, $klasa, null, null, $parametryFabryki);
	}
    }

    /*
     * Metoda niezalecana, należy użyć makeEncje i również tam dodawać do switcha nowe fabryki
     */

    public static function utworzEncje($sm, $klasa, $parametry = array(), $zapis = true,
	    \Pastmo\Testy\ParametryFabryki\ParametryFabryki $parametryFabryki = null) {

	switch ($klasa) {
	    case \Logowanie\Entity\Uprawnienie::class:
		return self::utworzUprawnienie($sm, $parametry);
	    case \Logowanie\Entity\Rola::class:
		return self::utworzRole($sm, $parametry);
	    case \Logowanie\Entity\UprawnienieRola::class:
		return self::utworzUprawnienieRole($sm, $parametry);
	    case \Pastmo\Email\Entity\KontoMailowe::class:
		return self::utworzKontoMailowe($sm, $parametry);
	    case Projekty\Entity\Projekt::class:
		return (new ProjektFactory($parametryFabryki))->utworzEncje($sm, $zapis, $parametry);
	    case \Logowanie\Entity\UzytkownikProjekt::class:
		return (new UzytkownikProjektFactory($parametryFabryki))->utworzEncje($sm, $zapis, $parametry);
	    case \Projekty\Entity\ProjektProduktWariacjaWizualizacja::class:
		return (new ProjektProduktWariacjaWizualizacjaFactory())->utworzEncje($sm, $zapis, $parametry);
	    case Produkty\Entity\Produkt::class:
		return (new ProduktFactory($parametryFabryki))->utworzEncje($sm, $zapis, $parametry);
	    case \Projekty\Entity\Tag::class:
		return (new TagFactory())->utworzEncje($sm, $zapis, $parametry);
	    case \Zasoby\Model\Zasob::class:
		return (new Pastmo\Testy\FabrykiKonkretnychEncji\ZasobFactory($parametryFabryki))->utworzEncje($sm, $zapis,
				$parametry);
	    case Firmy\Entity\Firma::class:
		return (new FirmaFactory($parametryFabryki))->utworzEncje($sm, $zapis, $parametry);
	    case \Projekty\Entity\ProjektProduktWariacja::class:
		return (new ProjektProduktWariacjaFactory($parametryFabryki))->utworzEncje($sm, $zapis, $parametry);
	    case Firmy\Entity\Adres::class:
		return (new Testy\FabrykiKonkretnychEncji\AdresFactory($parametryFabryki))->utworzEncje($sm, $zapis,
				$parametry);
	    case \Produkty\Entity\ProduktZnizka::class:
		return (new ProduktZnizkaFactory($parametryFabryki))->utworzEncje($sm, $zapis, $parametry);
	    case \Wysylka\Entity\Pudelko::class:
		return (new PudelkoFactory($parametryFabryki))->utworzEncje($sm, $zapis, $parametry);
	    case \BazaProduktow\Entity\ProduktWariacjaPudelko::class:
		return (new ProduktWariacjaPudelkoFactory($parametryFabryki))->utworzEncje($sm, $zapis, $parametry);
	    case \Pastmo\Email\Entity\Email::class:
		return (new Pastmo\Testy\FabrykiKonkretnychEncji\EmailFactory($parametryFabryki))->makeEncje();
	    case Email\Entity\Email::class:
		return (new \Testy\FabrykiKonkretnychEncji\EmailFactory($parametryFabryki))->makeEncje();
	    case \Email\Entity\EmailFolder::class:
		return (new \Testy\FabrykiKonkretnychEncji\EmailFolderFactory($parametryFabryki))->makeEncje();
	    case Projekty\Entity\ParametryWizualizacji::class:
		return (new ParametryWizualizacjiFactory($parametryFabryki))->makeEncje();
	    case \BazaProduktow\Entity\ProduktWariacjaProduktWariacja::class:
		return (new ProduktWariacjaProduktWariacjaFactory($parametryFabryki))->makeEncje();
	    case \Produkty\Entity\ProduktKategoria::class:
		return (new ProduktKategoriaFactory($parametryFabryki))->makeEncje();
	    case Magazyn\Entity\ZamowienieWewnetrzne::class:
		return (new ZamowienieWewnetrzneFactory($parametryFabryki))->makeEncje();
	    case \Magazyn\Entity\ZamowienieWewnetrzneWariacja::class:
		return (new ZamowienieWewnetrzneWariacjaFactory($parametryFabryki))->makeEncje();
	    case \Magazyn\Entity\Magazyn::class:
		return (new MagazynFactory($parametryFabryki))->makeEncje();
	    case \Logowanie\Model\Uzytkownik::class:
		return (new Pastmo\Testy\FabrykiKonkretnychEncji\UzytkownikFactory($parametryFabryki))->makeEncje();
	    case \BazaProduktow\Entity\Konkurencja::class:
		return (new KonkurencjaFactory($parametryFabryki))->makeEncje();
	    case \BazaProduktow\Entity\ProduktMagazyn::class:
		return (new ProduktMagazynFactory($parametryFabryki))->makeEncje();
	    case \BazaProduktow\Entity\ProduktMagazynOdpad::class:
		return (new ProduktMagazynOdpadFactory($parametryFabryki))->makeEncje();
	    case Logowanie\Entity\UzytkownikKontoMailowe::class:
		return (new Pastmo\Testy\FabrykiKonkretnychEncji\UzytkownikKontoMailoweFactory($parametryFabryki))->makeEncje();

	    default :
 		throw new \Exception("Brak klasy:" . $klasa);
	}
    }

    private static function encjeZModulow($klasa, $parametryFabryki) {
	foreach (self::$MODULY_Z_FABRYKAMI as $fabrykaModulu) {
	    $encja = $fabrykaModulu::makeEncje($klasa, $parametryFabryki);
	    if ($encja) {
		return $encja;
	    }
	}

	return false;
    }

    public static function utworzUzytkownika($sm) {
	return (new Pastmo\Testy\FabrykiKonkretnychEncji\UzytkownikFactory())->utworzEncje($sm, true, array());
    }

    private static function utworzWatekWiadomosci($sm) {
	$table = $sm->get(\Konwersacje\Module::WATKI_WIADOMOSCI_TABLE);
	$encja = new WatekWiadomosci();
	$encja->temat = 'temat';
	$table->zapisz($encja);
	$dodanaEncja = $table->getPoprzednioDodany();
	return $dodanaEncja;
    }

    private static function utworzUzytkownikWatkekWiadomosci($sm, $parametry) {
	$table = $sm->get(\Konwersacje\Module::UZYTKOWNIK_WATKEK_WIADOMOSCI_TABLE);
	$encja = new UzytkownikWatkekWiadomosci();

	self::dodajParamtery($encja, $parametry);

	$table->zapisz($encja);
	$dodanaEncja = $table->getPoprzednioDodany();
	return $dodanaEncja;
    }

    private static function utworzUprawnienie($sm, $parametry) {
	$table = $sm->get(Logowanie\Menager\UprawnieniaMenager::class);
	$encja = new Uprawnienie();
	$encja->opis = "Opis";

	self::dodajParamtery($encja, $parametry);

	$table->zapisz($encja);
	$dodanaEncja = $table->getPoprzednioDodany();
	return $dodanaEncja;
    }

    private static function utworzRole($sm, $parametry) {
	$table = $sm->get(Logowanie\Menager\RoleMenager::class);
	$encja = new Rola();
	$encja->nazwa = "nazwa";

	self::dodajParamtery($encja, $parametry);

	$table->zapisz($encja);
	$dodanaEncja = $table->getPoprzednioDodany();
	return $dodanaEncja;
    }

    private static function utworzUprawnienieRole($sm, $parametry) {
	$table = $sm->get(\Logowanie\Menager\UprawnieniaRoleMenager::class);
	$encja = new UprawnienieRola();


	self::dodajParamtery($encja, $parametry);

	$table->zapisz($encja);
	$dodanaEncja = $table->getPoprzednioDodany();
	return $dodanaEncja;
    }

    private static function utworzKontoMailowe($sm, $parametry) {
	$table = $sm->get(Pastmo\Email\Menager\KontaMailoweMenager::getClass());
	$encja = new \Pastmo\Email\Entity\KontoMailowe();

	$encja->host = "host";
	self::dodajParamtery($encja, $parametry);

	$table->zapisz($encja);
	$dodanaEncja = $table->getPoprzednioDodany();
	return $dodanaEncja;
    }

    private static function dodajParamtery(&$encja, $parametry) {
	while (list($key, $value) = each($parametry)) {
	    $encja->$key = $value;
	}
    }

}
