<?php

use Logowanie\Model\Uzytkownik;
use Konwersacje\Entity\WatekWiadomosci;
use Konwersacje\Entity\UzytkownikWatkekWiadomosci;
use Konwersacje\Entity\Wiadomosc;
use Logowanie\Entity\Uprawnienie;
use Logowanie\Entity\Rola;
use Logowanie\Entity\UprawnienieRola;
use Pastmo\Email\Entity\KontoMailowe;

class FabrykaEncjiMockowych {

	public static function utworzEncje($klasa, $parametry = array(),
			$parametryFabryki = null) {

		if ($parametryFabryki === NULL) {
			$parametryFabryki = Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::create()->setparametry($parametry);
		}

		switch ($klasa) {

			default :
				$encja = \FabrykaRekordow::utworzEncje(null, $klasa, $parametry, false,
								$parametryFabryki);
				$encja->id = self::genrujId();
				return $encja;
		}
	}

	public static function makeEncje($klasa, $parametryFabryki) {
		switch ($klasa) {

			default :
				$encja = \FabrykaRekordow::makeEncje($klasa, $parametryFabryki);
				$encja->id = self::genrujId();
				return $encja;
		}

	}

	public static function utworzUzytkownika($parametry) {
		$encja = new Uzytkownik();
		$encja->login = 'login';
		$encja->id = self::genrujId();
		self::dodajParamtery($encja, $parametry);
		return $encja;
	}

	private static function dodajParamtery(&$encja, $parametry) {
		while (list($key, $value) = each($parametry)) {
			$encja->$key = $value;
		}
	}

	public static function genrujId() {
		return rand(1, 1000);
	}

}
