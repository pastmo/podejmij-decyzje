<?php

namespace WspolneTest;

use Zend\Loader\AutoloaderFactory;
use RuntimeException;

error_reporting(E_ALL | E_STRICT);

require_once dirname(__FILE__) . '\..\..\Pastmo\src\Testy\FabrykaRekordow.php';
require_once dirname(__FILE__) . '\Fabryki\FabrykaRekordow.php';
require_once dirname(__FILE__) . '\Fabryki\FabrykaEncjiMockowych.php';
require_once dirname(__FILE__) . '\..\..\Pastmo\src\Testy\Fabryki\FabrykaAbstrakcyjna.php';
require_once dirname(__FILE__) . '\Fabryki\FabrykaAbstrakcyjna.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\ProjektProduktWariacjaFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\ProjektProduktWariacjaWizualizacjaFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\ProduktFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\TagFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\ProjektFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\WiadomoscFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\FirmaFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\ProjektProduktWariacjaFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\UzytkownikProjektFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\ProduktZnizkaFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\ProduktWariacjaPudelkoFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\PudelkoFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\ParametryWizualizacjiFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\ProduktWariacjaProduktWariacjaFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\ProduktKategoriaFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\ZamowienieWewnetrzneWariacjaFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\ZamowienieWewnetrzneFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\MagazynFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\KonkurencjaFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\ProduktMagazynFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\ProduktMagazynOdpadFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\WycenaProducentaFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\PrzypomnienieFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\PowiadomienieFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\PowiadomienieKategoriaFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\PomocFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\UdostepnionyZasobFolderFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\UdostepnionyZasobUzytkownikFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\WystawcaFakturFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\ProjektKrokMilowyFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\WatekWiadomosciFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\TagFactory.php';
require_once dirname(__FILE__) . '\FabrykiKonkretnychEncji\WiadomoscTagFactory.php';
//TODO: UWAGA!!!
//Teraz już w ten sposób nie dodajemy fabryk- vide UrzadzenieFactory
//Wpisy z tego pliku powinny z czasem zniknąć. Po przeniesieniu plików proszę odpalić wszystkie testy.

abstract class WspolneBootstrap {

    protected static $sm;

    public static function init($modules = array()) {
        include __DIR__ . '/../../../vendor/autoload.php';
    }

    public static function chroot() {
        $rootPath = dirname(static::findParentPath('module'));
        chdir($rootPath);
    }

    public static function getServiceManager() {
        return static::$sm;
    }

    protected static function initAutoloader() {
        $vendorPath = static::findParentPath('vendor');

        if (file_exists($vendorPath . '/autoload.php')) {
            include $vendorPath . '/autoload.php';
        }

        if (!class_exists('Zend\Loader\AutoloaderFactory')) {
            throw new RuntimeException(
            'Unable to load ZF2. Run `php composer.phar install`'
            );
        }

        AutoloaderFactory::factory(array(
            'Zend\Loader\StandardAutoloader' => array(
                'autoregister_zf' => true,
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/' . __NAMESPACE__,
                ),
            ),
        ));
    }

    protected static function findParentPath($path) {
        $dir = __DIR__;
        $previousDir = '.';
        while (!is_dir($dir . '/' . $path)) {
            $dir = dirname($dir);
            if ($previousDir === $dir) {
                return false;
            }
            $previousDir = $dir;
        }
        return $dir . '/' . $path;
    }

    protected function getModules() {
        return $modules;
    }

}
