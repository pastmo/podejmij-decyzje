<?php

namespace WspolneTest;

class TestyAutoload {

	static public $classNames = array();

	public static function registerDirectory($dirName) {

		$di = new \DirectoryIterator($dirName);
		foreach ($di as $file) {

			if ($file->isDir() && !$file->isLink() && !$file->isDot()) {
				// recurse into directories other than a few special ones
				self::registerDirectory($file->getPathname());
			} elseif (substr($file->getFilename(), -4) === '.php') {
				// save the class name / path of a .php file found
				$className = substr($file->getFilename(), 0, -4);
				TestyAutoload::registerClass($className, $file->getPathname());
			}
		}
	}

	public static function registerClass($className, $fileName) {
		TestyAutoload::$classNames[$className] = $fileName;
	}

}
