<?php

namespace WspolneTest\Menager;

use Pastmo\Wspolne\Utils\SearchPole;

class BazowyMenagerBazodanowyTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    public function setUp() {
	parent::setUp();
	$this->produktyMenager = new \Zlecenia\Model\ZleceniaTable($this->sm);
    }

    public function testSearch_text() {
	$text = "ES0";
	$wynik = $this->produktyMenager->utworzWherePoWszystkichPolach($text);

	$expected = "kod  REGEXP '[\w]*([eę][sś]0)[\w]*'  OR opis  REGEXP '[\w]*([eę][sś]0)[\w]*' ";

	$this->assertEquals($expected, $wynik);
    }

    public function testSearch_int() {
	$text = "25";
	$wynik = $this->produktyMenager->utworzWherePoWszystkichPolach($text);

	$expected = "id= 25 OR kod  REGEXP '[\w]*(25)[\w]*'  OR opis  REGEXP '[\w]*(25)[\w]*' ";

	$this->assertEquals($expected, $wynik);
    }

    //Do tworzenia np: select * from firmy where adres_wysylki_id in(select id from adresy where ulica ='Brzozowa');
    public function testSearch_query() {
	$szukanyTekst = 'ES0';

	$searchPole = SearchPole::create()
		->setDataType(SearchPole::QUERY)
		->setQuery("in(select id from adresy where ulica %s)")
		->setName("adres_wysylki_id");
	$pola = array($searchPole);

	$search = new \Wspolne\Utils\Search('query');
	$wynik = $search->createWhere($pola, $szukanyTekst);

	$expected = "adres_wysylki_id in(select id from adresy where ulica   REGEXP '[\w]*([eę][sś]0)[\w]*' )";

	$this->assertEquals($expected, $wynik);
    }

    public function test_getLogowanieFasada() {
	$wynik = $this->sm->get(\Zlecenia\Model\ZleceniaTable::class)->getLogowanieFasada();
	$this->assertEquals(\Logowanie\Fasada\LogowanieFasada::class, get_class($wynik));
    }

    public function test_getEmaileFasada() {
	$wynik = $this->sm->get(\Zlecenia\Model\ZleceniaTable::class)->getEmaileFasada();
	$this->assertEquals(\Email\Fasada\EmailFasada::class, get_class($wynik));
    }

    public function test_getKonwersacjeFasada() {
	$wynik = $this->sm->get(\Zlecenia\Model\ZleceniaTable::class)->getKonwersacjeFasada();
	$this->assertEquals(\Konwersacje\Fasada\KonwersacjeFasada::class, get_class($wynik));
    }

    public function test_getZasobyFasada() {
	$wynik = $this->sm->get(\Zlecenia\Model\ZleceniaTable::class)->getZasobyFasada();
	$this->assertEquals(\Zasoby\Fasada\ZasobyFasada::class, get_class($wynik));
    }

}
