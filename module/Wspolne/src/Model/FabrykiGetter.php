<?php

namespace Wspolne\Model;

trait FabrykiGetter {

    public function getEmaileFasada() {
	return $this->fasadyContainer->getEmaileFasada();
    }

    public function getFirmyFasada() {
	return $this->fasadyContainer->getFirmyFasada();
    }

    public function getKonwersacjeFasada() {
	return $this->fasadyContainer->getKonwersacjeFasada();
    }

    public function getLogowanieFasada() {
	return $this->fasadyContainer->getLogowanieFasada();
    }

    public function getProduktyFasada() {
	return $this->fasadyContainer->getProduktyFasada();
    }

    public function getProjektyFasada() {
	return $this->fasadyContainer->getProjektyFasada();
    }

    public function getProdukcjaFasada() {
	return $this->fasadyContainer->getProdukcjaFasada();
    }

    public function getZasobyFasada() {
	return $this->fasadyContainer->getZasobyFasada();
    }

}
