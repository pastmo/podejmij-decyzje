<?php

namespace Wspolne\Model;

class FasadyContainter extends \Pastmo\Wspolne\Menager\WspolnyMenager {

    private $logowanieFasada;
    private $zarejestrowaneFasady = [
	    'logowanieFasada' => \Logowanie\Fasada\LogowanieFasada::class,
    ];

    public function getLogowanieFasada() {
	return $this->getFasada('logowanieFasada');
    }

    protected function getFasada($nazwa) {
	if (!$this->$nazwa) {
	    $this->$nazwa = $this->sm->get($this->zarejestrowaneFasady[$nazwa]);
	}
	return $this->$nazwa;
    }

}
