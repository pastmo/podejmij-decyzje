<?php

namespace Wspolne\Fasada;

use Zend\ServiceManager\ServiceManager;

class WspolneFasada {

    protected $serviceMenager;
    protected $sm;

    public function __construct(ServiceManager $serviceMenager) {
	$this->serviceMenager = $serviceMenager;
	$this->sm = $serviceMenager;
    }

    public function get($class) {
	return $this->sm->get($class);
    }

}
