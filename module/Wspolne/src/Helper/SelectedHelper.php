<?php

namespace Wspolne\Helper;

use Zend\View\Helper\AbstractHelper;

class SelectedHelper extends AbstractHelper {

    public function __construct() {

    }

    public function __invoke($expected, $value) {
	if ($expected === $value) {
	    echo 'selected="true"';
	}
    }

}
