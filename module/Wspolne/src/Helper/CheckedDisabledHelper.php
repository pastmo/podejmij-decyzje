<?php

namespace Wspolne\Helper;

use Zend\View\Helper\AbstractHelper;

class CheckedDisabledHelper extends AbstractHelper {

    public function __construct() {

    }

    public function __invoke($expected, $value) {
	if (in_array($value, $expected)) {
	    echo 'disabled checked';
	}
    }

}
