<?php

namespace Wspolne;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Artykuly\Model\Artykul;
use Artykuly\Model\ArtykulyTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Wspolne\Helper\SelectedHelper;
use Wspolne\Helper\CheckedDisabledHelper;
use Zend\Mvc\MvcEvent;
use Wspolne\Menager\PodpowiadanieMenager;

class Module implements ConfigProviderInterface {

    public function onBootstrap(MvcEvent $e) {
	$viewHelperMenager = $e->getApplication()->getServiceManager()->get('ViewHelperManager');

	$viewHelperMenager->setFactory('selected',
		function($sm) use ($e) {
	    $viewHelper = new SelectedHelper();
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('checkedDisabled',
		function($sm) use ($e) {
	    $viewHelper = new CheckedDisabledHelper();
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('dodajWysiwyg',
		function($sm) use ($e) {
	    $viewHelper = new Helper\BootstrapWysiwygHelper();
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('wyswietlSelectModlowWyszukiwania',
		function($sm) use ($e) {
	    $viewHelper = new Helper\WyszukiwaineHelper($sm);
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('wyswietlStrzalki',
		function($sm) use ($e) {
	    $viewHelper = new Helper\WyswietlStrzalki();
	    return $viewHelper;
	});
	;
    }

    public function getConfig() {
	return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig() {
	return array(
		'factories' => array(
			PodpowiadanieMenager::class => function($sm) {
			    $table = new PodpowiadanieMenager($sm);
			    return $table;
			},
			Menager\SqlUpdater::class => function($sm) {
			    $table = new Menager\SqlUpdater($sm);
			    return $table;
			},
			Menager\LogiMenager::class => function($sm) {

			    $table = new Menager\LogiMenager($sm);
			    return $table;
			},
		),
	);
    }

    public static function dowiazMenagery(\Wspolne\Controller\Factory\InitObiekt $initObiekt,
	    \Interop\Container\ContainerInterface $container) {

    }

}
