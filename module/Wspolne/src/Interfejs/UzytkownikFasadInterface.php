<?php

namespace Wspolne\Interfejs;

interface UzytkownikFasadInterface {

    public function getProduktyFasada();

    public function getProjektyFasada();

//    public function getFirmyFasada();

    public function getKonwersacjeFasada();

    public function getLogowanieFasada();

    public function getEmaileFasada();
}
