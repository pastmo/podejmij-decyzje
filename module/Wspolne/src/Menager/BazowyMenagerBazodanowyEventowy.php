<?php

namespace Wspolne\Menager;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Model\FabrykiGetter;

class BazowyMenagerBazodanowyEventowy extends \Pastmo\Wspolne\Menager\WspolnyMenagerBazodanowyEventowy implements \Wspolne\Interfejs\UzytkownikFasadInterface {

    use FabrykiGetter;

    protected $wyszukiwanieZKontem = true;

    public function __construct(ServiceManager $sm = null, $gatewayName = '') {
	parent::__construct($sm, $gatewayName);
	$this->fasadyContainer = new \Wspolne\Model\FasadyContainter($sm);
    }

    public function zapisz(\Pastmo\Wspolne\Entity\WspolnyModel $model) {

	$kontoId = $this->getLogowanieFasada()->getKontoIdZalogowanego();

	if ($this->wyszukiwanieZKontem && !EntityUtil::wydobadzId($model) && $kontoId) {

	    $model->konto = $kontoId;
	}

	parent::zapisz($model);
    }

    public function getRekord($id) {
	$id = (int) $id;

	$select = $this->getSelect();
	$select->where(array('id' => $id));
	$this->modyfikujSelecta($select);
	$rowset = $this->tableGateway->selectWith($select);

	$row = $rowset->current();
	if (!$row) {
	    throw new \Exception("Could not find row $id");
	}
	return $row;
    }

    public function modyfikujSelecta($select) {
	if (!$this->wyszukiwanieZKontem) {
	    return;
	}

	$kontoId = $this->getLogowanieFasada()->getKontoIdZalogowanego();

	if ($kontoId) {
	    $select->where("konto_id=$kontoId");
	}
    }

}
