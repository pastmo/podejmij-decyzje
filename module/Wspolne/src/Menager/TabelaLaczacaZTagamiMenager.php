<?php

namespace Wspolne\Menager;

abstract class TabelaLaczacaZTagamiMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    protected function pobierzPoIdEncji($encjaId) {
	$id=  \Pastmo\Wspolne\Utils\EntityUtil::wydobadzId($encjaId);

	$whereUsera=$this->getWhereZZalogowanegoUseraWTabeliLaczacejZTagiem();
	$tagi = $this->pobierzZWherem($this->kluczEncjiWTabeli() . "=$id AND $whereUsera");
	return $tagi;
    }

    public function aktualizujTagiDlaEncji($encjaId, $tagiArray) {
	$whereUsera=$this->getWhereZZalogowanegoUseraWTabeliLaczacejZTagiem();

	$this->usun($this->kluczEncjiWTabeli() . "=$encjaId AND $whereUsera");
	$this->dodajTagiDoEncji($encjaId, $tagiArray);
    }

    public function dodajTagiDoEncji($encjaId, $tagiArray) {
	$tagiMenager = $this->sm->get(\Projekty\Menager\TagiMenager::class);

	$zalogowany=$this->getLogowanieFasada()->getZalogowanegoUsera();

	foreach ($tagiArray as $tagi) {
	    $nazwa = $tagi['value'];

	    $encja = $this->getEncjaMenager()->getRekord($encjaId);

	    $tag = $tagiMenager->pobierzTagPoNazwie($nazwa);

	    if (!$tag) {
		$tag = new \Projekty\Entity\Tag();
		$tag->nazwa = $nazwa;
		$tag->uzytkownik = $zalogowany;
		$tagiMenager->zapisz($tag);
		$tag = $tagiMenager->getPoprzednioDodany();
	    } else {
		$encjoTagi = $this->pobierzEncjoTagi($encjaId, $tag->id);
		if (count($encjoTagi) > 0)
		    continue;
	    }

	    $this->zapiszEncjaTag($encja, $tag);
	}
    }

    protected function getEncjaMenager() {
	return $encjaMenager = $this->sm->get($this->getKlaseEncjiMenagera());
    }

    private function pobierzEncjoTagi($projektId, $tagId) {
	$tagi = $this->pobierzZWherem($this->kluczEncjiWTabeli() . "=$projektId AND tag_id=$tagId");
	return $tagi;
    }

    abstract protected function getKlaseEncjiMenagera();

    abstract protected function kluczEncjiWTabeli();

    abstract function zapiszEncjaTag($encja, $tag);
}
