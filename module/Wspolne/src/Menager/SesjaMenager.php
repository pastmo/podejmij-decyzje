<?php

namespace Wspolne\Menager;

class SesjaMenager extends \Pastmo\Sesja\Menager\SesjaMenager {

    protected $kontenery = [
	    'containerEkranu' => self::EKRAN_CONTAINER,
	    'containerUzytkownicy' => self::UZYTKOWNICY_CONTAINER,
	    'decyzjeContainer' => self::DECYZJE_CONTAINER,
    ];

    protected $decyzjeContainer;

    const DECYZJE_CONTAINER = 'DecyzjeContainer';
    const BAZA_PRODUKTOW_CONTAINER = 'BazaProduktowContainer';
    const KOSZYK_CONTAINER = 'KoszykContainer';
    const LICZBA_PRODUKTOW_KEY = 'liczbaproduktow';
    const CZY_WYSWIETLANIE_MINIATUR_KEY = 'czywyswietlanieminiatur';
    const KOSZYK_KEY = 'koszyk';


    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm);
    }

    public function zapiszRoboczeDecyzje($post = null) {
	$this->ustawKluczLubUsun($this->decyzjeContainer, 'robocze_decyzje', $post);
    }

    public function pobierzRoboczeDecyzje() {
	$decyzjaPost = $this->zwrocZawartoscKluczaLubFalsz($this->decyzjeContainer, 'robocze_decyzje');
	if ($decyzjaPost) {
	    return $decyzjaPost;
	}
	return [];
    }

}
