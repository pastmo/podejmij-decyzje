<?php

namespace Wspolne\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Pastmo\Wiadomosci\Enumy\SkrzynkiWiadomosci;
use Pastmo\Wiadomosci\Enumy\StatusyWiadomosci;
use Pastmo\Wspolne\Utils\SearchPole;
use Pastmo\Wspolne\Adnotacje\DomyslnyJsonModel;
use \Logowanie\Enumy\KodyUprawnien;

/**
 * @DomyslnyJsonModel
 */
class AjaxController extends \Wspolne\Controller\WspolneController {

    const KLUCZ_Z_PARAMETROW = "polecenie";

    public function indexAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_ADMINISTRATORA, true);
    }

    public function przypomnieniaAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$zalogowany = $this->uzytkownikTable->getZalogowanegoUsera();

	$wynik = array();

	$wynik['zlecenia'] = $this->zleceniaTable->pobierzZWherem('przeczytane=0 AND opiekun_id=' . $zalogowany->id);
	$wynik['zgloszenia'] = $this->zgloszeniaTable->pobierzZWherem('przeczytane=0 AND pracownik_id=' . $zalogowany->id);
	$wynik['konwersacje'] = $this->wiadomosciMenager->pobierzNieprzeczytaneDlaZalogowanego();


	switch ($this->params('format')) {
	    case 'json':

		return new JsonModel(array(
			'success' => true,
			'dane' => $wynik,
		));
	    default:
		return new ViewModel(array());
	}
    }

    public function updateDetailsAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_SPRZEDAWCY, true);

	$post = $this->getPost();

	if ($post) {
	    $projektId = $post->project_id;

	    $czyLaczenie = $post->attach_to_user;
	    $usuwaniePolaczenia = $post->detach_from_user;
	    $opisParametrowWizualizacji = $post->preview_desc;
	    $przypiszGrafika = $post->przypisz_grafika;
	    $nazwa = $post->name;
	    $idUrzadzenia = $post->device_id;

	    if ($czyLaczenie !== null) {
		$this->projektyMenager->polaczProjektZZalogowanymUzytkownikiem($projektId);
	    } else if ($usuwaniePolaczenia !== null) {
		$this->projektyMenager->usunSprzedawceZProjektu($projektId);
	    } else if ($opisParametrowWizualizacji !== null) {
		$this->projektyFasada->zapiszParametry($post);
	    } else if ($przypiszGrafika !== null) {
		$this->projektyFasada->polaczGrafikaZParametramiWizualizacji($post);
	    } else if ($nazwa !== null) {
		$this->projektyFasada->zmienNazweProjektu($projektId, $nazwa);
	    } else if ($idUrzadzenia !== null) {
		$this->produkcjaFasada->aktualizujParametryProdukcji($post);
	    } else {
		return $this->returnFail(array('msg' => $this->translate('Brak metody')));
	    }
	    return $this->returnSuccess();
	}
	return $this->returnFail(array('msg' => $this->translate('Brak posta')));
    }

    public function zapiszParametryProdukcjiAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_SPRZEDAWCY, true);
	$post = $this->getPost();
	if ($post) {
	    $this->produkcjaFasada->aktualizujParametryProdukcji($post);
	    return $this->returnSuccess();
	}
	return $this->returnFail(array('msg' => $this->translate('Brak posta')));
    }

    public function projectsOrderAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$post = $this->getPost();

	$this->projektyFasada->zmienKolejnoscProjektow($post);


	return $this->returnSuccess();
    }

    public function mailsAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_EMAIL, true);

	$zalogowany = $this->uzytkownikTable->getZalogowanegoUsera();
	if (!$zalogowany) {
	    return new JsonModel(array('success' => false, 'msg' => $this->translate("Niezalogowany użytkownik")));
	}

	$polecenie = $this->params()->fromRoute('polecenie', '');
	$errors = array();

	if ($polecenie === 'refresh_inbox') {
	    $this->kontaMailoweMenager->odswiezMaileZWszystkichSkrzynek();
	    $errors = $this->kontaMailoweMenager->emailScaner->errors;
	}

	if (count($errors) == 0) {
	    return $this->returnSuccess();
	} else {
	    return $this->returnFail(array('msgs' => $errors));
	}
    }

    public function mailAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_EMAIL, true);

	$polecenie = $this->params()->fromRoute('polecenie', '');
	$mailIds = $this->params()->fromPost('mail_ids', array());

	switch ($polecenie) {
	    case 'mark_as_spam':
		$nowa_skrzynka = SkrzynkiWiadomosci::SPAM;
		break;
	    case 'mark_as_not_spam':
		$nowa_skrzynka = SkrzynkiWiadomosci::ODBIORCZA;
		break;
	    case 'delete':
		$nowa_skrzynka = SkrzynkiWiadomosci::KOSZ;
		break;
	    case 'restore':
		$nowa_skrzynka = SkrzynkiWiadomosci::ODBIORCZA;
		break;
	    case 'mark_as_read':
		$nowy_status = StatusyWiadomosci::PRZECZYTANA;
		break;
	    case 'mark_as_not_read':
		$nowy_status = StatusyWiadomosci::NIEPRZECZYTANA;
		break;
	    case 'assign_project':
		$mail_id = $this->params()->fromPost('mail_id');
		$project_id = $this->params()->fromPost('project_id');
		break;
	    case 'change_folder':
		$nowa_skrzynka = $this->params()->fromPost('folder_id');
		break;
	    default:
		return $this->returnFail(array('msg' => 'Nieznane polecenie:' . $polecenie));
	}

	foreach ($mailIds as $mailId) {
	    if (isset($nowa_skrzynka)) {
		$this->emailFasada->zmienSkrzynke($mailId, $nowa_skrzynka);
	    }
	    if (isset($nowy_status)) {
		$this->emailFasada->zmienStatus($mailId, $nowy_status);
	    }
	}

	if (isset($project_id)) {
	    $this->projektyFasada->dodajMailaDoProjektu($mail_id, $project_id);
	    return $this->returnSuccess(
			    array('project_id' => $project_id)
	    );
	}


	return $this->returnSuccess();
    }

    public function createMailProjectAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_SPRZEDAWCY, true);

	try {
	    $post = $this->getPost();
	    if ($post) {

		$mail_id = $post->mail_id;
		$project_name = $post->project_name;

		$this->projektyFasada->utworzProjektZMaila($mail_id, $project_name);
		$dodany = $this->projektyMenager->getPoprzednioDodany();

		return $this->returnSuccess(
				array('project_id' => $dodany->id)
		);
	    }
	} catch (\Exception $e) {
	    return $this->returnFail(array('msg' => $e->getMessage()));
	}

	return $this->returnFail();
    }

    public function projectStepsAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::EDYCJA_STATUSOW, true);

	$post = $this->getPost();
	if ($post) {

	    $this->projektyKrokiMiloweMenager->aktualizujKrokiProjektu($post);

	    return $this->returnSuccess();
	}

	return $this->returnFail();
    }

    public function projectStepAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::EDYCJA_STATUSOW, true);

	$post = $this->getPost();
	if ($post) {
	    $this->projektyKrokiMiloweMenager->aktualizujPojedynczyKrokProjektu($post);
	    return $this->returnSuccess();
	}
    }

    public function projectStepsNotifAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$post = $this->getPost();

	if ($post) {

	    $uzytkownik = $this->uzytkownikTable->getZalogowanegoUsera();
	    $projektId = $post->project_id;
	    $tresc = $this->translate("Kontakt z klientem z projektu") . $projektId;
	    $data_przypomnienia = \Wspolne\Utils\StrUtil::polaczDateZCzasem($post->date, $post->time);

	    $this->dashboardFasada->dodajPrzypomnienie($uzytkownik->id, $tresc, $data_przypomnienia);

	    return $this->returnSuccess();
	}
    }

    public function projectSetTagsAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::TAGI_WIADOMOSCI, true);

	$request = $this->getRequest();
	if ($request->isPost()) {
	    $post = $request->getPost();

	    $project_id = $post->project_id;
	    $tags = $post->tags;

	    $this->projektyTagiMenager->aktualizujTagiWProjecie($project_id, $tags);

	    return $this->returnSuccess();
	}


	return $this->returnFail();
    }

    public function powiadomieniaProjektuAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_PROJEKTY, true);

	return $this->returnFail(array('msg' => 'Funkcjonalność nieobsługiwana'));
    }

    public function messageSetTagsAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::TAGI_WIADOMOSCI, true);

	$request = $this->getRequest();
	if ($request->isPost()) {
	    $post = $request->getPost();

	    $wiadomoscId = $post->message_id;
	    $tags = $post->tags;

	    $this->konwersacjeFasada->dodajTagiDoWiadomosci($wiadomoscId, $tags);

	    return $this->returnSuccess();
	}


	return $this->returnFail();
    }

    public function updateMtagAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::TAGI_WIADOMOSCI, true);

	return $this->updateTag();
    }

    public function updatePtagAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::TAGI_PROJEKTU, true);

	return $this->updateTag();
    }

    private function updateTag() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$post = $this->getPost();

	if ($post) {
	    $tagId = $post['tag_id'];

	    if (isset($post['color'])) {
		$kolor = $post['color'];

		$this->projektyFasada->zmienKolorTagu($tagId, $kolor);

		return $this->returnSuccess();
	    }
	    if (isset($post['delete'])) {

		$this->projektyFasada->usunTag($tagId);

		return $this->returnSuccess();
	    }
	}

	return $this->returnFail();
    }

    public function projectFindTagsAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::TAGI_PROJEKTU, true);

	$get = $this->getGet();

	if ($get) {

	    $tagi = $this->projektyFasada->wyszukajTagi($get['query']);
	    return $this->returnSuccess($tagi);
	}


	return new JsonModel(array(
		'success' => false
	));
    }

    public function messageFindTagsAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::TAGI_WIADOMOSCI, true);


	$get = $this->getGet();

	if ($get) {

	    $tagi = $this->konwersacjeFasada->wyszukajTagi($get['query']);
	    return $this->returnSuccess($tagi);
	}


	return new JsonModel(array(
		'success' => false
	));
    }

    public function projectsAction() {//TODO: Rozbić na mniejsze akcje lub otestować każdy przypadek
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_PROJEKTY, true);

	$polecenie = $this->params()->fromRoute('polecenie');

	$post = $this->getPost();

	switch ($polecenie) {
	    case 'transfer':
		return $this->przekazProjekt();
	    case 'order_info':
		$wynik = $this->projektyFasada->szczegolyZamowienia($this->getPost());
		return $this->returnSuccess($wynik);
	    case 'edit_product':
		if ($post) {
		    $this->projektyFasada->aktualizujWariacjeWProjekcie($post);
		    return $this->returnSuccess();
		}
	    case 'edit_address':
		if ($post) {
		    $this->projektyFasada->zapiszAdresWysylki($post);
		    return $this->returnSuccess();
		}
	    case 'invoice':
		if ($post) {
		    $this->projektyFasada->zapiszDaneDoFaktury($post);
		    return $this->returnSuccess();
		}
	    case 'set_invoice':
		if ($post) {
		    $id = $post['projekt_id'];
		    $this->projektyFasada->kopujDaneZFakturyDoFirmy($id);
		    return $this->returnSuccess();
		}
	    case 'set_address':
		if ($post) {
		    $id = $post['projekt_id'];
		    $this->projektyFasada->kopujAdresDostawyDoFirmy($id);
		    return $this->returnSuccess();
		}
	    case 'accept_address':
		if ($post) {
		    $id = $post['project_id'];
		    $akceptacja = $post['accepted'];
		    if ($akceptacja) {
			$this->projektyFasada->akceptujAdresWysylki($id);
		    } else {
			$this->projektyFasada->anulujAkceptacjeAdresuWysylki($id);
		    }
		    return $this->returnSuccess();
		}
	    case 'accept_invoice':
		if ($post) {
		    $id = $post['project_id'];
		    $akceptacja = $post['accepted'];
		    if ($akceptacja) {
			$this->projektyFasada->akceptujDaneDoFaktury($id);
		    } else {
			$this->projektyFasada->anulujAkceptacjeDanychDoFaktury($id);
		    }
		    return $this->returnSuccess();
		}

	    default :
		return new JsonModel(array(
			'success' => false,
			'msg' => $this->translate('Nieznane polecenie')
		));
	}

	return $this->returnFail();
    }

    private function przekazProjekt() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::PRZEKAZYWANIE_PROJEKTU_INNEMU_SPRZEDAWCY_ZA_ZGODA,
		true);

	$request = $this->getRequest();
	if ($request->isPost()) {
	    $post = $request->getPost();

	    $project_id = $post->project_id;
	    $userId = $post->attach_to_user;

	    $this->projektyMenager->zapiszPropozycjeZmianySprzedawcyProjektu($project_id, $userId);

	    return $this->returnSuccess();
	}


	return new JsonModel(array(
		'success' => false
	));
    }

    public function deleteProjectAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_SPRZEDAWCY, true);

	$request = $this->getRequest();
	if ($request->isPost()) {
	    $post = $request->getPost();

	    $this->projektyMenager->zamknijProjekt($post);

	    return $this->returnSuccess();
	}

	return $this->returnFail();
    }

///////////// helpers.js
    public function getVariationsAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::KARTA_PRODUKTU, true);

	$post = $this->getPost();
	if ($post) {

	    $produktId = $post->product_id;

	    if (!$post->check_visibility)
		$wariacje = $this->produktyWariacjeMenager->pobierzWariacjeDlaProduktu($produktId);
	    else
		$wariacje = $this->produktyWariacjeMenager->pobierzWidzialneWariacje($produktId);

	    $wynik = array();
	    foreach ($wariacje as $wariacja) {
		$wynik[] = array('id' => $wariacja->id, 'dimensions' => $wariacja->getWymiary());
	    }

	    return $this->returnSuccess(['variations' => $wynik]);
	}


	return $this->returnFail();
    }

    public function calculatePriceAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_SPRZEDAWCY, true);
	$uzytkownik = $this->uzytkownikTable->getZalogowanegoUsera();
	$post = $this->getPost();
	if ($post) {
	    $variationId = $post['variation_id'];
	    $ilosc = $post['quantity'];
	    if (!empty($variationId)) {
		$aktualnaCena = $this->produktyFasada->getAktualnaCene($variationId, $ilosc, $uzytkownik->id);
		return $this->returnSuccess(['base_price' => $aktualnaCena->cena_jednostkowa_przed,
				'discount_price' => $aktualnaCena->cena_jednostkowa_po]);
	    }
	}

	return $this->returnFail([
			'base_price' => 0,
			'discount_price' => 0]);
    }

    public function getProductDetailsAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::KARTA_PRODUKTU, true);
	$uzytkownik = $this->uzytkownikTable->getZalogowanegoUsera();
	$post = $this->getPost();

	if ($post) {
	    $wariacja = $this->produktyWariacjeMenager->getRekord($post->variation_id);
	    $obrazek = $wariacja->produkt->getGlownyObrazek();
	    $stan = $this->produktyFasada->zwrocStanMagazynuBezZamowionejIlosci($wariacja->id);
	    $aktualnaCena = $this->produktyFasada->getAktualnaCene($post->variation_id, 1, $uzytkownik->id);
	    $komponenty = $this->produktyFasada->pobierzKomponentyWariacji($wariacja->id);

	    return $this->returnSuccess([
			    'variation' => $wariacja . '',
			    'thumb' => "/" . $obrazek->wyswietl(),
			    'price' => $wariacja->cena,
			    'price_uv' => $wariacja->cena_uv,
			    'name' => $wariacja->produkt->kod,
			    'stan' => $stan,
			    'discount_price' => $aktualnaCena->cena_jednostkowa_po,
			    'komponenty' => $komponenty
	    ]);
	}


	return $this->returnFail();
    }

/////////// cart.js

    public function usunZKoszykaDostawAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_KOSZYK_DOSTAW, true);

	$post = $this->getPost();
	if ($post) {
	    foreach ($post->dostawyWariacje as $dostawaWariacja) {
		$this->magazynFasada->usunZamowienieWariacje($dostawaWariacja);
	    }
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function usunZKoszykaKlientaAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_KOSZYK_KLIENTA, true);

	$post = $this->getPost();
	if ($post) {
	    $this->koszykFasada->usunZKoszyka($post->index);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function edytujWariacjeWKoszykuAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_KOSZYK_KLIENTA, true);

	$post = $this->getPost();
	if ($post) {
	    $this->koszykFasada->edytujIloscWariacji($post->index, $post->ilosc);
	    return $this->returnSuccess();
	}
    }

    public function zapiszOdpadyAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_KOSZYK_KLIENTA, true);

	$post = $this->getPost();
	if ($post) {
	    $ex = $this->koszykFasada->zapiszWariacjeJakoOdpady($post['selected_items'], $post['magazine_id'],
		    $post['user_id'], $post['reason'], $post['quantity'], $post['comment']);
	    if (isset($ex))
		return $this->returnFail(['msg' => $ex]);
	    return $this->returnSuccess();
	}
    }

    public function newProjectAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_SPRZEDAWCY, true);

	$request = $this->getRequest();
	if ($request->isPost()) {
	    $post = $request->getPost();
	    $nazwaProjektu = $post['name'];

	    try {
		$this->projektyFasada->dodajNowyProjekt($nazwaProjektu);

		return $this->returnSuccess();
	    } catch (\Exception $e) {
		return $this->returnFail(array('msg' => $e->getMessage()));
	    }
	}
    }

    public function addToCartAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::EDYCJA_KOSZYKA_PRODUKTOW, true);
	$uzytkownik = $this->uzytkownikTable->getZalogowanegoUsera();
	$post = $this->getPost();
	if ($post) {
	    $this->magazynFasada->dodajDoKoszykaWariacje(null, $uzytkownik->id, $post->variation, null,
		    $post->quantity, null);
	    return $this->returnSuccess();
	}
    }

    public function dodajTabliceDoKoszykaDostawAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_KOSZYK_DOSTAW, true);
	$uzytkownik = $this->uzytkownikTable->getZalogowanegoUsera();
	$post = $this->getPost();
	if ($post) {

	    $projektId = $post->projekt_id;
	    $wariacje = $post->wariacje;
	    foreach ($wariacje as $wariacja) {
		$this->koszykFasada->dodajDoKoszykaZamowien(null, $uzytkownik->id, $wariacja['id'], $projektId,
			$wariacja['ilosc'], null);
	    }

	    return $this->returnSuccess();
	}
    }

    public function zamknijDostaweAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_KOSZYK_DOSTAW, true);
	$uzytkownik = $this->uzytkownikTable->getZalogowanegoUsera();
	$post = $this->getPost();
	if ($post) {
	    $error = $this->magazynFasada->zamowDostawe($post['dostawywariacje'], $uzytkownik->id);
	    if (isset($error))
		return $this->returnFail(['msg' => $error]);
	    return $this->returnSuccess();
	}
    }

    public function przeniesMiedzyKoszykiemKlientowASprzedawcowAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_KOSZYK_DOSTAW, true);
	$uzytkownik = $this->uzytkownikTable->getZalogowanegoUsera();
	$request = $this->getRequest();
	if ($request->isPost()) {
	    $post = $request->getPost();
	    $error = $this->koszykFasada->przeniesWariacjeDoKoszykaSprzedawcow($post->selected_items, $uzytkownik->id);
	    if (isset($error))
		return $this->returnFail(['msg' => $error]);
	    return $this->returnSuccess();
	}
    }

//////////////////
    public function projectMarksAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::TAGI_PROJEKTU, true);

	$post = $this->getPost();
	if ($post) {
	    $this->projektyFasada->oznaczProjekt($post);
	    return $this->returnSuccess();
	}
    }

    public function newMessageAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_WIADOMOSCI, true);

	$post = $this->getPost();
	if ($post) {

	    $this->konwersacjeFasada->dodajWiadomoscDoWlascicielaZPosta($post);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function newMessageDraftAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_WIADOMOSCI, true);

	$post = $this->getPost();
	if ($post) {
//			$idProjektu=$post['message_draft_id'];
//			$this->konwersacjeFasada->dodajWiadomoscDoProjektu();
	    return $this->returnSuccess();

	    /* [project_id] => (string) 3
	      [content] => (string) dfghj
	      [message_draft_id] => (string) */
	}
	return $this->returnFail();
    }

    public function productEdytujWariacjeAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::WARIACJE, true);
	$post = $this->getPost();
	if ($post) {
	    $this->produktyWariacjeMenager->zaktualizujWariacjeDlaAjax($post);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function productDodajNowaWariacjeAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::WARIACJE, true);
	$post = $this->getPost();
	if ($post) {
	    $ostatnio_dodany = [];
	    foreach ($post->variations as $wariacja) {
		$ostatnio_dodany[] = $this->produktyWariacjeMenager->dodajProduktWariacjeDlaAjax($wariacja);
	    }
	    return $this->returnSuccess([
			    'variations' => $ostatnio_dodany,
	    ]);
	}
	return $this->returnFail();
    }

    public function productUsunWariacjeAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::WARIACJE, true);
	$post = $this->getPost();
	if ($post) {
	    $this->produktyWariacjeMenager->usunProduktWariacjeDlaAjax($post);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function productKategorieAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::KATEGORIE, true);
	$post = $this->getPost();
	if ($post) {
	    $this->produktyKategorieMenager->zmienKategorieProduktu($post);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function productDodajZnizkeAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::ZNIZKI, true);
	$post = $this->getPost();
	if ($post) {
	    foreach ($post->discounts as $znizka) {
		$this->produktyZnizkiMenager->dodajProduktZnizke($znizka);
	    }
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function productZaktualizujZnizkeAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::ZNIZKI, true);
	$post = $this->getPost();
	if ($post) {
	    $this->produktyZnizkiMenager->zaktualizujProduktZnizke($post);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function productUsunZnizkeAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::ZNIZKI, true);
	$post = $this->getPost();
	if ($post) {
	    $this->produktyZnizkiMenager->usunProduktZnizke($post);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function productKomponentyAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::KOMPONENTY_WYMIENNE, true);
	$post = $this->getPost();
	if ($post) {
	    $this->produktyWariacjeProduktyWariacjeMenager->zmienProduktyWariacjeProduktyWariacje($post);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function productTagiAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::TAGI, true);
	$post = $this->getPost();
	if ($post) {
	    $this->produktyFasada->dodajLubUsunTag($post);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function productOdpadyAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::ODPADY, true);
	$post = $this->getPost();
	if ($post) {
	    $ostatnio_dodany = Array();
	    foreach ($post->odpady as $odpad) {
		$ostatnio_dodany[] = $this->produktyMagazynOdpadyMenager->dodajProduktMagazynOdpadDlaAjax($odpad);
	    }
	    return $this->returnSuccess(['odpady' => $ostatnio_dodany]);
	}
	return $this->returnFail();
    }

    public function productUsunOdpadAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::ODPADY, true);
	$post = $this->getPost();
	if ($post) {
	    $this->produktyMagazynOdpadyMenager->usunProduktMagazynOdpad($post->odpad_id);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function productDodajRezerwacjeAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::REZERWACJE, true);
	$post = $this->getPost();
	if ($post) {
	    $czy_sukces = array();
	    foreach ($post->rezerwacje as $rezerwacja) {
		$zwrot = $this->rezerwacjeMenager->dodajRezerwacjeDlaAjax($rezerwacja);
		$czy_sukces[] = $zwrot;
	    }
	    return new JsonModel(array(
		    'success' => $czy_sukces,
	    ));
	}
	return $this->returnFail();
    }

    public function productDodajKonkurencjeAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::KONKURENCJA, true);
	$post = $this->getPost();
	if ($post) {
	    foreach ($post->konkurencje as $konkurencja) {
		$this->konkurencjaMenager->dodajKonkurencje($konkurencja);
	    }
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function productEdytujKonkurencjeAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::KONKURENCJA, true);
	$post = $this->getPost();
	if ($post) {
	    $this->konkurencjaMenager->edytujKonkurencje($post);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function productWycenyAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::WYCENY_PRODUCENTOW, true);
	$uzytkownik = $this->uzytkownikTable->getZalogowanegoUsera();
	$post = $this->getPost();
	if ($post) {
	    $error = "";
	    foreach ($post['wyceny'] as $wycena) {
		$error .= $this->aukcjeFasada->dodajWyceneUzytkownikaIWariacji($wycena['produkt_wariacja_id'],
			$uzytkownik->id, $wycena['ilosc'], $wycena['producent'], $wycena['data_wyceny'], $wycena['cena_za_sztuke'],
			$wycena['komentarz'], $wycena['wycena']);
	    }
	    if (empty($error))
		return $this->returnSuccess();
	}
	return $this->returnFail(['msg' => $error]);
    }

    public function productUsunWyceneAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::WYCENY_PRODUCENTOW, true);
	$post = $this->getPost();
	if ($post) {
	    $this->aukcjeFasada->usunWyceneUzytkownikaIWariacje($post->wycena_id);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function productAction() { //akcja pozostawiona dla ewentualnych niezaktualizowanych skryptow
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);
	return $this->returnFail();
    }

    public function addFileForProductAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::ZDJECIA, true);

	$post = $this->getPost();
	$this->produktyFasada->zapiszZasobyDlaProduktu($post);
	return $this->returnSuccess();
    }

    public function removeFileAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::ZDJECIA, true);

	$post = $this->getPost();
	$this->produktyFasada->usunZasobyDlaProduktu($post);
	return $this->returnSuccess();
    }

    public function setAsMainAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::ZDJECIA, true);

	$post = $this->getPost();
	$this->produktyFasada->ustawJakoGlownyAjax($post);
	return $this->returnSuccess();
    }

    public function addCommentAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::KOMENTARZ_DO_WIZUALIZACJI, true);

	$post = $this->getPost();

	if ($post) {
	    $this->konwersacjeFasada->dodajKomentarzDoWizualizacji($post);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function addVisIndAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DODAWANIE_INDYWIZUALIZACJI, true);

	$post = $this->getPost();

	if ($post) {

	    $typ = $post->type;

	    if ($typ && $typ == "2") {
		$this->projektyFasada->dodajIndywidualizacjeDoWariacji($post);
	    } else {
		$this->projektyFasada->dodajWizualizacjeDoWariacji($post);
	    }
	    return $this->returnSuccess(array('files' => array(
				    array(
					    "name" => "-",
				    )
	    )));
	}
	return $this->returnFail();
    }

    public function attachCompanyAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::SZCZEGOLY_FIRMY_DODAWANIE_EDYCJA, true);

	$post = $this->getPost();

	if ($post) {
	    $this->projektyFasada->dodajLubUsunFirmeDoProjektuPost($post);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function addCompanyAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::SZCZEGOLY_FIRMY_DODAWANIE_EDYCJA, true);

	$post = $this->getPost();

	if ($post) {
	    $this->firmyFasada->dodajFirme($post);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function addMemberAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UZYTKOWNICY_W_PROJEKCIE_PRZYPISYWANIE, true);

	$post = $this->getPost();

	if ($post) {
	    $this->projektyFasada->dodajUzytkownikowDoProjektu($post);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function removeMemberAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UZYTKOWNICY_W_PROJEKCIE_PRZYPISYWANIE, true);

	$post = $this->getPost();

	if ($post) {
	    $uzytkownikId = $post['user_id'];
	    $projektId = $post['project_id'];
	    $this->logowanieFasada->usunPowiazanieUzytkownikaZProjektem($uzytkownikId, $projektId);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function userAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::EDYCJA_UZYTKOWNIKOW, true);

	$post = $this->getPost();
	$polecenie = $this->params()->fromRoute('polecenie', '');

	if ($post) {
	    switch ($polecenie) {
		case 'update':
		    $this->logowanieFasada->aktualizujUzytkownika($post);
		    return $this->returnSuccess();

		case 'company_assign':
		    $firmaId = $post['company_id'];
		    $userId = $post['user_id'];
		    $this->firmyFasada->polaczUzytkownikaZFirma($userId, $firmaId);
		    return $this->returnSuccess();

		default :
		    return $this->returnFail();
	    }
	}
    }

    public function updateStatueAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::AKTUALIZACJA_PRODUKTOW_W_PROJEKCIE, true);

	$post = $this->getPost();

	if ($post) {
	    $this->projektyFasada->aktualizujWariacjeWProjekcie($post);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function previewAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$key = $this->params()->fromRoute(self::KLUCZ_Z_PARAMETROW, "");

	if ($key === 'get_data') {
	    $data = $this->wizualizacjeFasada->daneDoWizualizacjiZCzasowPracy(true);
	    return $this->returnSuccess($data);
	}
    }

    public function productionAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_PRODUKCJA, true);

	$key = $this->params()->fromRoute(self::KLUCZ_Z_PARAMETROW, "");
	$post = $this->getPost();

	switch ($key) {
	    case 'preview_time':
		$this->projektyFasada->updateCzasParametrowWizualizacji($post);
		return $this->returnSuccess();
	    case 'production_time':
		$this->projektyFasada->updateCzasParametrowProdukcji($post);
		return $this->returnSuccess();
	    case 'get_data':
		$data = $this->wizualizacjeFasada->daneDoProdukcjiZCzasowPracy();
		return $this->returnSuccess($data);
	    default:
		return $this->returnFail(array('msg' => $this->translate('Nieznany klucz:') . $key));
	}
    }

    public function updateCzasyPracyAction() {
	$post = $this->getPost();

	if ($post) {
	    $this->wizualizacjeFasada->aktualizujCzasyPracyZPosta($post);
	    return $this->returnSuccess();
	}
	return $this->returnFail($this->translate("Brak przekazanych parametrów"));
    }

    public function removeDeliveryAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_MAGAZYN, true);

	$post = $this->getPost();

	if ($post) {
	    $this->magazynFasada->usunZamowienie($post['delivery_id']);
	    return $this->returnSuccess();
	}
    }

    public function deliveryProductAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_MAGAZYN_DOSTAWA, true);

	$post = $this->getPost();

	if ($post) {
	    switch ($post['akcja']) {
		case 'ilosc':
		    $this->magazynFasada->zmienIloscWariacji($post['zamowienie_id'], $post['ilosc']);
		    return $this->returnSuccess();
		case 'komentarz':
		    $this->magazynFasada->zmienKomentarzWariacji($post['zamowienie_id'], $post['komentarz']);
		    return $this->returnSuccess();
	    }
	}
    }

    public function zakonczDostaweAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_MAGAZYN_DOSTAWA, true);

	$post = $this->getPost();

	if ($post) {
	    $error = $this->magazynFasada->zakonczZamowienie($post['id'], $post['uzytkownik_id']);
	    if (empty($error))
		return $this->returnSuccess();
	    else
		return $this->returnFail(['msg' => $error]);
	}
    }

    public function wyszukiwanieAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$get = $this->getGet();
	$wynik = array();

	if ($get) {
	    $szukanyTekst = $get->query;
	    $wynik = $this->podpowiadanieMenager->pobierzPodpowiedzi($szukanyTekst);
	}

	return $this->returnSuccess($wynik);
    }

    public function wyszukiwanieUzytkownikowAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::PRZEGLADANIE_PROFILII_UZYTKOWNIKOW, true);

	$get = $this->getGet();

	if ($get) {
	    $szukanyTekst = $get->query;
	    $wynik = $this->uzytkownikTable->wyszukaj($szukanyTekst, 10);
	}
	return $this->returnSuccess($wynik);
    }

    public function dodajPrzypomnienieAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_PRACOWNIKA, true);

	$post = $this->getPost();

	if ($post) {
	    $error = $this->dashboardFasada->dodajPrzypomnienieZAjax($post);
	}
	if (empty($error)) {
	    return $this->returnSuccess();
	} else {
	    return $this->returnFail(['msg' => $error]);
	}
    }

    public function getERPdataAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);
	$post = $this->getPost();

	if ($post['uzytkownikId']) {
	    $przypomnienia = $this->dashboardFasada->pobierzPrzypomnieniaNieprzeczytane($post['uzytkownikId']);
	    $powiadomienia = $this->dashboardFasada->pobierzPowiadomieniaNieprzeczytane($post['uzytkownikId']);
	    return $this->returnSuccess([
			    'notifications' => $powiadomienia,
			    'reminders' => $przypomnienia
	    ]);
	}
    }

    public function getHelpTextAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$post = $this->getPost();

	if ($post) {
	    $tresc = $this->dashboardFasada->pobierzTrescPomocy($post['kod']);
	    return $this->returnSuccess(['tresc' => $tresc]);
	}
    }

    public function deleteMtagAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::TAGI_WIADOMOSCI, true);
	$post = $this->getPost();

	if ($post) {
	    $wiadomoscId = $post->wiadomosc_id;
	    $tagId = $post->tag_id;

	    $this->konwersacjeFasada->usunZWiadomosciITaga($wiadomoscId, $tagId);
	    return $this->returnSuccess();
	}

	return $this->returnFail();
    }

    public function removePreviewAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);
	$post = $this->getPost();

	if ($post) {
	    $wizualizacjaId = $post->preview_id;

	    $this->projektyFasada->usunWizualizacjePoId($wizualizacjaId);
	    return $this->returnSuccess();
	}

	return $this->returnFail();
    }

    public function msgVisibleAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_SPRZEDAWCY, true);
	$post = $this->getPost();

	if ($post) {
	    $this->konwersacjeFasada->aktualizujWidocznoscWiadomosci($post);
	    return $this->returnSuccess();
	}

	return $this->returnFail();
    }

    public function projectPreviewAcceptAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::AKCJAPTACJA_WIZUALIZACJI_I_INDYWIDUALIZACJI,
		true);
	$post = $this->getPost();


	if ($post) {
	    $wizualizacjaId = $post->preview_id;
	    $this->projektyFasada->akceptujWizualizacje($wizualizacjaId);
	    return $this->returnSuccess();
	}

	return $this->returnFail();
    }

    public function projectPreviewRemoveAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::AKCJAPTACJA_WIZUALIZACJI_I_INDYWIDUALIZACJI,
		true);
	$post = $this->getPost();

	if ($post) {
	    $wizualizacjaId = $post->preview_id;
	    $this->projektyFasada->usunAkceptacjeWizualizacji($wizualizacjaId);
	    return $this->returnSuccess();
	}

	return $this->returnFail();
    }

    public function updatePreviewAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DODAWANIE_WIZUALIZACJI, true);
	$post = $this->getPost();

	if ($post) {
	    $wizualizacjaId = $post->preview_id;
	    $this->projektyFasada->aktualizujWizualizacje($wizualizacjaId);
	    return $this->returnSuccess();
	}

	return $this->returnFail();
    }

    public function projectAllPreviewAcceptAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::AKCJAPTACJA_WIZUALIZACJI_I_INDYWIDUALIZACJI,
		true);
	$post = $this->getPost();

	if ($post) {
	    $projektWariacjaId = $post->projekt_wariacja_id;
	    $this->projektyFasada->aktualizujWszystkieIndywidualizacje($projektWariacjaId);
	    return $this->returnSuccess();
	}

	return $this->returnFail();
    }

    public function discountAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_ADMINISTRATORA, true);
	$polecenie = $this->params()->fromRoute('polecenie');
	$post = $this->getPost();
	if ($post) {
	    switch ($polecenie) {
		case 'new':
		    $returnArray = $this->produktyFasada->zapiszNoweKategorieLojalnoscioweAjax($post);
		    return $this->returnSuccess(['discounts' => $returnArray]);
		case 'change':
		    $this->produktyFasada->zmienNazweKategoriiLojalnosciowej($post['discount_id'], $post['name']);
		    return $this->returnSuccess();
		case 'delete':
		    $this->produktyFasada->usunKategorieLojalnosciowa($post['discount_id']);
		    return $this->returnSuccess();
		case 'default':
		    $this->produktyFasada->ustawKategorieDomyslna($post['discount_id']);
		    return $this->returnSuccess();
		default:
		    return $this->returnFail();
	    }
	}
    }

    public function sprawdzDostepnaIloscAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);
	$post = $this->getPost();
	if ($post) {
	    $stan = $this->produktyFasada->sprawdzStanMagazynuBezZamowionejIlosci($post['wariacja'], $post['ilosc'],
		    $post['magazyn']);
	    if ($stan)
		return $this->returnSuccess();
	}
	return $this->returnFail(['msg' => $this->translate("Nie jest dostępna określona ilość w magazynie.")]);
    }

    public function attachCategoryAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_SPRZEDAWCY, true);
	$post = $this->getPost();
	if ($post) {
	    $firmaId = $post['company_id'];
	    $kategoriaId = $post['discount_id'];
	    $this->firmyFasada->zmienKategorieLojalnosciowa($firmaId, $kategoriaId);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function attachSellerAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::SZCZEGOLY_FIRMY_DODAWANIE_EDYCJA, true);
	$post = $this->getPost();
	if ($post) {
	    $firmaId = $post['company_id'];
	    $sprzedawcaId = $post['seller_id'];
	    $this->firmyFasada->zmienDomyslnegoSprzedawca($firmaId, $sprzedawcaId);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function addCategoryAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_ADMINISTRATORA, true);

	$post = $this->getPost();
	if ($post) {
	    $kategoria = $this->produktyFasada->zrobNowaKategorie($post['name'], $post['parent_id'],
		    $post['typ_produktu_id']);
	    if (!empty($kategoria)) {
		return $this->returnSuccess();
	    }
	}
	return $this->returnFail();
    }

    public function updateCategoryAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_ADMINISTRATORA, true);

	$post = $this->getPost();
	if ($post) {
	    $this->produktyFasada->edytujNazweKategorii($post['name'], $post['category_id'], $post['typ_produktu_id']);
	    return $this->returnSuccess();
	}
	return $this->returnFail();
    }

    public function removeCategoryAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_ADMINISTRATORA, true);
	$error = 0;
	$post = $this->getPost();
	if ($post) {
	    $error = $this->produktyFasada->usunKategorie($post['category_id']);
	    if (empty($error))
		return $this->returnSuccess();
	}
	return $this->returnFail(['msg' => $error]);
    }

    public function ustawieniaKategorieAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_ADMINISTRATORA, true);
	$error = 0;
	$post = $this->getPost();
	if ($post) {
	    $error = $this->produktyFasada->zmienKategorieKomponentow($post['pudelka'], $post['podstawki'],
		    $post['komponenty_wymienne'], $post['komponenty_dodatkowe']);
	    if (empty($error))
		return $this->returnSuccess();
	}
	return $this->returnFail(['msg' => $error]);
    }

    public function messageCollapseAction() {
	$this->sprawdzUprawnienie(KodyUprawnien::WIADOMOSCI_W_PROJEKTACH);

	$rozwiniecie = $this->params()->fromPost('collapse');
	$ids = $this->params()->fromPost('id');

	$idUzytkownikWiadomosc = \Pastmo\Wspolne\Utils\ArrayUtil::zrobTablice($ids);

	$this->konwersacjeFasada->aktualizujRozwiniecieWiadomosci($idUzytkownikWiadomosc, $rozwiniecie);

	return $this->returnSuccess();
    }

    public function zakonczWizualizacjeAction() {
	$this->sprawdzUprawnienie(KodyUprawnien::UPRAWNIENIA_GRAFIKA);

	$id = $this->params()->fromPost('projekt_id', false);
	if ($id) {
	    $this->projektyKrokiMiloweMenager->zakonczWizualizacjeProjektu($id);
	    return $this->returnSuccess();
	}

	return $this->returnFail();
    }

    public function aktualizujAkceptacjeProduktuAction() {
	$this->sprawdzUprawnienie(KodyUprawnien::MENU_BAZA_PRODUKTOW_PRODUKTY);

	$post = $this->getPost();
	if ($post) {
	    $this->produktyMenager->zmienAkceptacje($post);
	    return $this->returnSuccess();
	}

	return $this->returnFail();
    }

    public function aktualizujWidzialnoscProduktuAction() {
	$this->sprawdzUprawnienie(KodyUprawnien::MENU_BAZA_PRODUKTOW_PRODUKTY);

	$post = $this->getPost();
	if ($post) {
	    $this->produktyMenager->zmienWidocznosc($post);
	    return $this->returnSuccess();
	}

	return $this->returnFail();
    }

    public function aktualizujWidzialnoscWariacjiAction() {
	$this->sprawdzUprawnienie(KodyUprawnien::MENU_BAZA_PRODUKTOW_PRODUKTY);

	$post = $this->getPost();
	if ($post) {
	    $this->produktyWariacjeMenager->zmienWidocznosc($post);
	    return $this->returnSuccess();
	}

	return $this->returnFail();
    }

    public function usunTypProduktuAction() {
	$this->sprawdzUprawnienie(KodyUprawnien::MENU_BAZA_PRODUKTOW);

	$post = $this->getPost();
	if ($post) {
	    $this->produktyFasada->usunTypProduktu($post->id);
	    return $this->returnSuccess();
	}

	return $this->returnFail();
    }

    public function zapiszKosztTransportuWProjekcieAction() {
	$this->sprawdzUprawnienie(KodyUprawnien::EDYCJA_TRANSPORTU_PRODUKTOW);

	$id = $this->params()->fromPost('id', false);
	$cena_transportu = $this->params()->fromPost('cena_transportu', false);

	if ($id) {
	    $this->projektyFasada->zapiszCeneTransportu($id, $cena_transportu);
	    return $this->returnSuccess();
	}

	return $this->returnFail();
    }

    public function akceptujCeneTransportuWProjekcieAction() {
	$this->sprawdzUprawnienie(KodyUprawnien::AKCEPTACJA_CEN_W_PROJEKCIE);

	$id = $this->params()->fromPost('id', false);
	$czy_akceptacja = $this->params()->fromPost('czy_akceptacja', false);

	if ($id) {
	    $this->projektyFasada->akceptujCeneTransportu($id, $czy_akceptacja);
	    return $this->returnSuccess();
	}

	return $this->returnFail();
    }

    public function usunTypProduktuUrzadzenieAction() {
	$this->sprawdzUprawnienie(KodyUprawnien::MENU_BAZA_PRODUKTOW);

	$post = $this->getPost();
	if ($post) {
	    $this->produktyFasada->usunTypProduktuUrzadzenia($post['id']);
	    return $this->returnSuccess();
	}

	return $this->returnFail();
    }

}
