<?php

namespace Wspolne\Controller;

use Zend\Session\Container;

class ElementyWidokuController {

    /**
     * @deprecated since version 2016-07-25
     * Metoda niezalecana, należy użyć BootstrapWysiwygHelper jako helper widoku.
     */
    public static function wyswietlWysiwyg() {
	return 'wspolne/wspolne/bootstrap_wysiwyg.phtml';
    }

    public static function wyswietlWiadomosc() {
	$wiadomosc = self::pobierzWiadomosc();
	if ($wiadomosc !== NULL) {
	    ?>
	    <div class="alert alert-danger" role="alert"><?php echo $wiadomosc; ?></div>
	    <?php
	}
    }

    public static function dodajWiadomosc($msg) {
	$container = new Container('namespace');
	$container->msg = $msg;
    }

    public static function pobierzWiadomosc() {
	$container = new Container('namespace');
	$msg = $container->msg;
	$container->msg = null;
	return $msg;
    }

}
