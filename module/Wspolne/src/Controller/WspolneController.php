<?php

namespace Wspolne\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class WspolneController extends \Pastmo\Controller\WspolnyController {

    //Fasady
    protected $firmyFasada;
    protected $konwersacjeFasada;
    protected $logowanieFasada;
    protected $zasobyFasada;
    protected $emailFasada;
    protected $kontaktFasada;
    //
    protected $cennikZlecenOplatyTable;
    protected $uzytkownikTable;
    protected $zleceniaTable;
    protected $zleceniaKategorieTable;
    protected $klienciTable;
    protected $marketingTable;
    protected $notatkiTable;
    protected $notatkiKlienciTable;
    protected $socialmediaTable;
    protected $adwordsTable;
    protected $zgloszeniaTable;
    protected $wiadomosciTable;
    protected $watkiWiadomosciTable;
    protected $portaleTable;
    protected $uzytkownikWatkekWiadomosciTable;
    protected $planowanieImprezTable;
    protected $roleMenager;
    protected $kadraTable;
    protected $zasobyTable;
    protected $domenyTable;
    protected $domenyPracownicyTable;
    protected $informacjeSeoTable;
    protected $serweryTable;
    protected $subdomenyTable;
    protected $uprawnieniaKategorieMenager;
    protected $uprawnieniaRoleMenager;
    protected $magazynTable;
    protected $namiotyTable;
    protected $fakturyTable;
    protected $przypominanieHaslaMenager;
    protected $uprawnieniaMenager;
    protected $kontaMailoweMenager;
    protected $maileMenager;
    protected $fakturyMenager;
    protected $wystawcyFakturyMenager;
    protected $logiMenager;
    protected $pdfMenager;

    public function init(Factory\InitObiekt $initObiekt) {
	parent::init($initObiekt);
	$this->logowanieFasada = $initObiekt->logowanieFasada;
	$this->kontaktFasada = $initObiekt->kontaktFasada;
	$this->decyzjeFasada = $initObiekt->decyzjeFasada;

	$this->uzytkownikTable = $initObiekt->uzytkownikTable;
	$this->uzytkownikWatkekWiadomosciTable = $initObiekt->uzytkownikWatkekWiadomosciTable;
	$this->roleMenager = $initObiekt->roleMenager;
	$this->uprawnieniaKategorieMenager = $initObiekt->uprawnieniaKategorieMenager;
	$this->uprawnieniaRoleMenager = $initObiekt->uprawnieniaRoleMenager;
	$this->przypominanieHaslaMenager = $initObiekt->przypominanieHaslaMenager;
	$this->uprawnieniaMenager = $initObiekt->uprawnieniaMenager;
	$this->logiMenager = $initObiekt->logiMenager;
    }

    public function getUzytkownicyMenager() {
	if ($this->uzytkownikTable) {
	    return $this->uzytkownikTable;
	}

	return $this->getFromServiceLocator(\Logowanie\Module::UZYTKOWNIK_TABLE);
    }

    protected function getZleceniaTable() {
	if ($this->zleceniaTable) {
	    return $this->zleceniaTable;
	}
	return $this->getFromServiceLocator(\Zlecenia\Module::ZLECENIA_TABLE);
    }

    protected function getZleceniaKategorieTable() {
	if ($this->zleceniaKategorieTable) {
	    return $this->zleceniaKategorieTable;
	}
	return $this->getFromServiceLocator(\Zlecenia\Module::KATEGORIE_TABLE);
    }

    protected function getKlienciTable() {
	if ($this->klienciTable) {
	    return $this->klienciTable;
	}
	return $this->getFromServiceLocator(\Klienci\Module::KLIENCI_TABLE);
    }

    protected function getPozycjonowanieTable() {
	if ($this->marketingTable) {
	    return $this->marketingTable;
	}
	return $this->getFromServiceLocator(\Marketing\Module::MARKETING_TABLE);
    }

    protected function getSocialmediaTable() {
	if ($this->socialmediaTable) {
	    return $this->socialmediaTable;
	}
	return $this->getFromServiceLocator(\Marketing\Module::SOCIALMEDIA_TABLE);
    }

    protected function getAdwordsTable() {
	if ($this->adwordsTable) {
	    return $this->adwordsTable;
	}
	return $this->getFromServiceLocator(\Marketing\Module::ADWORDS_TABLE);
    }

    public function getPortaleTable() {
	if ($this->portaleTable) {
	    return $this->portaleTable;
	}

	return $this->getFromServiceLocator(\Marketing\Module::PORTALE_TABLE);
    }

    protected function getPlanowanieImprezTable() {
	if ($this->planowanieImprezTable) {
	    return $this->planowanieImprezTable;
	}
	return $this->getFromServiceLocator(\PlanowanieImprez\Module::PLANOWANIEIMPREZ_TABLE);
    }

    protected function getKadraTable() {
	if ($this->kadraTable) {
	    return $this->kadraTable;
	}
	return $this->getFromServiceLocator(\Kadra\Module::KADRA_TABLE);
    }

    protected function getDomenyTable() {
	if ($this->domenyTable) {
	    return $this->domenyTable;
	}
	return $this->getFromServiceLocator(\Hosting\Module::DOMENY_TABLE);
    }

    protected function getSerweryTable() {
	if ($this->serweryTable) {
	    return $this->serweryTable;
	}
	return $this->getFromServiceLocator(\Hosting\Module::SERWERY_TABLE);
    }

    protected function getSubdomenyTable() {
	if ($this->subdomenyTable) {
	    return $this->subdomenyTable;
	}
	return $this->getFromServiceLocator(\Hosting\Module::SUBDOMENY_TABLE);
    }

    protected function getMagazynTable() {
	if ($this->magazynTable) {
	    return $this->magazynTable;
	}
	return $this->getFromServiceLocator(\Magazyn\Module::MAGAZYN_TABLE);
    }

    protected function getNamiotyTable() {
	if ($this->namiotyTable) {
	    return $this->namiotyTable;
	}
	return $this->getFromServiceLocator(\Magazyn\Module::NAMIOTY_TABLE);
    }

    protected function getFakturyTable() {
	if ($this->fakturyTable) {
	    return $this->fakturyTable;
	}
	return $this->getFromServiceLocator(\Ksiegowosc\Module::FAKTURY_TABLE);
    }

    private function getFromServiceLocator($name) {
	$sm = $this->getServiceLocator();
	$zleceniaTable = $sm->get($name);
	return $zleceniaTable;
    }

    protected function getPost() {
	$request = $this->getRequest();
	if ($request->isPost()) {
	    return $request->getPost();
	}
	return false;
    }

    protected function getGet() {
	$request = $this->getRequest();
	if ($request->isGet()) {
	    return $request->getQuery();
	}
	return false;
    }

    public function pobierzWartoscWhere() {
	$get = $this->getGet();
	$wyszukiwanie = isset($get['wyszukaj']) ? $get['wyszukaj'] : null;
	return $wyszukiwanie;
    }

    public function pobierzKluczOrder() {
	$get = $this->getGet();
	$wyszukiwanie = isset($get['sort_klucz']) ? $get['sort_klucz'] : null;
	return $wyszukiwanie;
    }

    public function pobierzTypOrder() {
	$get = $this->getGet();
	$wyszukiwanie = isset($get['sort_typ']) ? $get['sort_typ'] : null;
	return $wyszukiwanie;
    }

    public function ustawLayoutZBialymNaglowkiem() {
	$this->layout()->setTemplate('/layout/layout_z_bialym_naglowkiem');
    }

    protected function returnSuccess($param = array()) {
	return $this->returnFailSucces($param, true);
    }

    protected function returnFail($param = array()) {
	return $this->returnFailSucces($param, false);
    }

    private function returnFailSucces($param, $succes) {
	$successArray = array('success' => $succes);

	$result = array_merge($successArray, $param);
	return new JsonModel($result);
    }

    protected function dodajEdytujWidok($tytul, $id) {
	if ($id > 0) {
	    $zlecenie = $this->zleceniaTable->getRekord($id);
	} else {
	    $zlecenie = \Zlecenia\Entity\Zlecenie::create();
	}

	$typy = [];
	$klienci = $this->klienciTable->fetchAll('nazwa ASC');
	$modulPowrotu = $this->getBackModule('zlecenia');
	$sklepy = [];

	$wynik = new ViewModel(array(
		'zlecenie' => $zlecenie,
		'tytul' => $tytul,
		'klienci' => $klienci,
		'sklepy' => $sklepy,
		'back' => $modulPowrotu
	));
	$wynik->setTemplate('zlecenia/zlecenia/dodaj_edytuj');

	return $wynik;
    }

    protected function getBackModule($modulPowrotu = 'home') {
	$get = $this->getGet();
	if (isset($_GET['back'])) {
	    $modulPowrotu = $_GET['back'];
	}
	return $modulPowrotu;
    }

    protected function getBackAction($akcjaPowrotu = 'index') {
	$get = $this->getGet();
	if (isset($_GET['back_action'])) {
	    $akcjaPowrotu = $_GET['back_action'];
	}
	return $akcjaPowrotu;
    }

}
