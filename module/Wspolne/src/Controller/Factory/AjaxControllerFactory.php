<?php

namespace Wspolne\Controller\Factory;

use Wspolne\Controller\AjaxController;

class AjaxControllerFactory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new AjaxController();
    }

}
