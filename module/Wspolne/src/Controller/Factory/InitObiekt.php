<?php

namespace Wspolne\Controller\Factory;

class InitObiekt {

    use \Pastmo\Controller\PastmoInit;

    //Fasady
    public $firmyFasada;
    public $konwersacjeFasada;
    public $decyzjeFasada;
    public $logowanieFasada;
    public $zasobyFasada;
    public $emailFasada;
    public $kontaktFasada;
    public $cennikZlecenOplatyTable;
    public $uzytkownikTable;
    public $zleceniaTable;
    public $zleceniaKategorieTable;
    public $klienciTable;
    public $marketingTable;
    public $notatkiTable;
    public $notatkiKlienciTable;
    public $socialmediaTable;
    public $adwordsTable;
    public $zgloszeniaTable;
    public $watkiWiadomosciTable;
    public $wiadomosciTable;
    public $portaleTable;
    public $uzytkownikWatkekWiadomosciTable;
    public $planowanieImprezTable;
    public $kadraTable;
    public $roleMenager;
    public $zasobyTable;
    public $domenyTable;
    public $domenyPracownicyTable;
    public $informacjeSeoTable;
    public $serweryTable;
    public $subdomenyTable;
    public $uprawnieniaKategorieMenager;
    public $uprawnieniaRoleMenager;
    public $magazynTable;
    public $namiotyTable;
    public $fakturyMenager;
    public $przypominanieHaslaMenager;
    public $uprawnieniaMenager;
    public $kontaMailoweMenager;
    public $maileMenager;
    public $pdfMenager;
    public $wystawcyFakturyMenager;
    public $translator;
    public $constantTranslator;
    public $logiMenager;

}
