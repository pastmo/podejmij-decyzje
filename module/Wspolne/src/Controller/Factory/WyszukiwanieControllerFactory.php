<?php

namespace Wspolne\Controller\Factory;

use Wspolne\Controller\WyszukiwanieController;

class WyszukiwanieControllerFactory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new WyszukiwanieController();
    }

}
