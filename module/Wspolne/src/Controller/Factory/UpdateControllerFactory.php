<?php

namespace Wspolne\Controller\Factory;

class UpdateControllerFactory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new \Wspolne\Controller\UpdateController();
    }

}
