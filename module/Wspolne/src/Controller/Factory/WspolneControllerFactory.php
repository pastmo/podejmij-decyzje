<?php

namespace Wspolne\Controller\Factory;

use \Zend\ServiceManager\FactoryInterface;
use \Zend\ServiceManager\ServiceLocatorInterface;

abstract class WspolneControllerFactory extends \Pastmo\Controller\PastmoControllerFactory {

    public function createService(ServiceLocatorInterface $serviceLocator) {
	return $this($serviceLocator, '');
    }

      public function pobierzMinimalnaKonfiguracje() {
	return [
		\Logowanie\Module::class,
		\Pastmo\Module::class,
		\Wspolne\Module::class,
		\Kontakt\Module::class,
		\Decyzje\Module::class,
		]
	;
    }

    public function pobierzMenageryModulow() {
	return [
	];
    }

    abstract protected function newController();
}
