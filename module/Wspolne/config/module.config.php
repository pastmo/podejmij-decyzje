<?php

namespace Wspolne;

return array(
	'controllers' => array(
		'factories' => array(
			'Wspolne\Controller\Wspolne' => Controller\Factory\Factory::class,
			'Wspolne\Controller\Ajax' => Controller\Factory\AjaxControllerFactory::class,
			'Wspolne\Controller\Update' => Controller\Factory\UpdateControllerFactory::class,
			'Wspolne\Controller\Wyszukiwanie' => Controller\Factory\WyszukiwanieControllerFactory::class,
			'Wspolne\Controller\Tests' => Controller\Factory\TestsControllerFactory::class,
		),
	),
	'router' => array(
		'routes' => array(
			'wspolne' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/wspolne[/:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Wspolne\Controller\Wspolne',
						'action' => 'index',
					),
				),
			),
			'wyszukiwanie' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/wyszukiwanie[/:action]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Wspolne\Controller\Wyszukiwanie',
						'action' => 'index',
					),
				),
			),
			'ajax' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/ajax[/:action][/:polecenie][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'polecenie' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Wspolne\Controller\Ajax',
						'action' => 'index',
					),
				),
			),
			'update' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/update[/:action][/:polecenie][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'polecenie' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Wspolne\Controller\Update',
						'action' => 'index',
					),
				),
			),
			'tests' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/tests[/:action][/:polecenie][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'polecenie' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Wspolne\Controller\Tests',
						'action' => 'index',
					),
				),
			),
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'wspolne' => __DIR__ . '/../view',
		),
		'strategies' => array(
			'ViewJsonStrategy',
		),
	),
);
