<?php

namespace Lokalizacje;

namespace Lokalizacje;

class Module implements \Zend\ModuleManager\Feature\ConfigProviderInterface {

    public function getConfig() {
	return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig() {
	return array(
		'factories' => array(
			Menager\ConstantTranslatorMenager::class => function($sm) {
			    $menager = new Menager\ConstantTranslatorMenager($sm);
			    return $menager;
			}
		),
	);
    }

}
