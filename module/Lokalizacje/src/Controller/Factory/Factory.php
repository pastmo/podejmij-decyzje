<?php

namespace Lokalizacje\Controller\Factory;

use Lokalizacje\Controller\LokalizacjeController;

class Factory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new LokalizacjeController();
    }

}
