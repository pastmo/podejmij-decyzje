<?php

namespace Lokalizacje\Menager;

class ConstantTranslatorMenager extends \Pastmo\Wspolne\Menager\WspolnyMenager {

    public function translateConst($stala, $domkniecie = false) {
	switch ($stala) {
	    case "INBOX":
		return $this->translate('Odebrane');
	    case "Drafts":
		return $this->translate('Robocze');
	    case "Sent":
		return $this->translate('Wysłane');
	    case "Trash":
		return $this->translate('Kosz');
	    default:
		return $stala;
	};
    }

}
