<?php

namespace Lokalizacje;

return array(
	'controllers' => array(
		'factories' => array(
			'Lokalizacje\Controller\Lokalizacje' => Controller\Factory\Factory::class,
		),
	),
	'router' => array(
		'routes' => array(
			'lokalizacje' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/lokalizacje[/:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Lokalizacje\Controller\Lokalizacje',
						'action' => 'index',
					),
				),
			),
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Lokalizacje' => __DIR__ . '/../view',
		),
	),
);
