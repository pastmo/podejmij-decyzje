<?php

namespace Testy\FabrykiKonkretnychEncji;

class ParametryProdukcjiFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Produkcja\Entity\ParametryProdukcji;
	$encja->czas_pracy = 3600;
	$encja->deadline = date("Y-m-d H:i:s");
	$encja->urzadzenie = \Produkcja\Entity\Urzadzenie::create();
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Produkcja\Menager\ParametryProdukcjiMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->urzadzenie = $this->uzyjWlasciwejFabryki(\Produkcja\Entity\Urzadzenie::class,
		$parametry = array());
    }

}
