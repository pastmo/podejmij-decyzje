<?php

namespace Testy\FabrykiKonkretnychEncji;

class UzytkownikPowiadomienieFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Dashboard\Entity\UzytkownikPowiadomienie();
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Dashboard\Menager\UzytkownicyPowiadomieniaMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->uzytkownik = $this->uzyjWlasciwejFabryki(\Logowanie\Model\Uzytkownik::class);
	$this->encja->powiadomienie = $this->uzyjWlasciwejFabryki(\Dashboard\Entity\Powiadomienie::class);
    }

}
