<?php

namespace Testy\FabrykiKonkretnychEncji;

class ZlecenieFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Zlecenia\Entity\Zlecenie();
	$encja->opis = __CLASS__;
	$encja->firma = __CLASS__;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Zlecenia\Model\ZleceniaTable::class;
    }

}
