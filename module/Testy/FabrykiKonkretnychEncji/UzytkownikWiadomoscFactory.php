<?php

namespace Testy\FabrykiKonkretnychEncji;

class UzytkownikWiadomoscFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Konwersacje\Entity\UzytkownikWiadomosc;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Konwersacje\Menager\UzytkownicyWiadomosciMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->uzytkownik = $this->uzyjWlasciwejFabryki(\Logowanie\Model\Uzytkownik::class);
	$this->encja->wiadomosc = $this->uzyjWlasciwejFabryki(\Konwersacje\Entity\Wiadomosc::class);
    }

}
