<?php

namespace Testy\FabrykiKonkretnychEncji;

class WycenaProducentaWariacjaFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Aukcje\Entity\WycenaProducentaWariacja();
	$encja->ilosc = rand(1, 100);
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Aukcje\Menager\WycenyProducentowWariacjeMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->wycena_producentow = $this->uzyjWlasciwejFabryki(\Aukcje\Entity\WycenaProducenta::class);
	$this->encja->produkt_wariacja = $this->uzyjWlasciwejFabryki(\BazaProduktow\Entity\ProduktWariacja::class);
    }

}
