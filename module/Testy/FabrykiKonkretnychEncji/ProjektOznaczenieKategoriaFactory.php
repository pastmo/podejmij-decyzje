<?php

namespace Testy\FabrykiKonkretnychEncji;

class ProjektOznaczenieKategoriaFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Projekty\Entity\ProjektOznaczenieKategoria();
	$encja->nazwa = __CLASS__;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Projekty\Menager\ProjektyOznaczeniaKategorieMenager::class;
    }

    public function dowiazInneTabele() {
    }

}
