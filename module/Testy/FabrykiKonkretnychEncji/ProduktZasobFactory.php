<?php

namespace Testy\FabrykiKonkretnychEncji;

class ProduktZasobFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Produkty\Entity\ProduktZasob();
	$encja->typ = \Produkty\Enumy\TypProduktZasob::OBJ;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Produkty\Menager\ProduktyZasobyMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->produkt = $this->uzyjWlasciwejFabryki(\Produkty\Entity\Produkt::class);
	$this->encja->zasob = $this->uzyjWlasciwejFabryki(\Zasoby\Model\Zasob::class, ['nazwa' => 'obiekt.obj']);
	$this->encja->uzytkownik = $this->uzyjWlasciwejFabryki(\Logowanie\Model\Uzytkownik::class);
    }

}
