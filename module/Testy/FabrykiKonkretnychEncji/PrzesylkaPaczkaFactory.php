<?php

namespace Testy\FabrykiKonkretnychEncji;

class PrzesylkaPaczkaFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Wysylka\Entity\PrzesylkaPaczka();
	$encja->rodzaj = \Wysylka\Enumy\PrzesylkaPaczkaRodzaj::PACZKA;
	$encja->waga = 3.14;
	$encja->x = 1;
	$encja->y = 2;
	$encja->z = 3;
	$encja->ilosc = 10;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Wysylka\Menager\PrzesylkiPaczkiMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->przesylka = $this->uzyjWlasciwejFabryki(\Wysylka\Entity\Przesylka::class);
    }

}
