<?php

namespace Testy\FabrykiKonkretnychEncji;

class UzytkownikWatkekWiadomosciFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Konwersacje\Entity\UzytkownikWatkekWiadomosci;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Konwersacje\Model\UzytkownikWatkekWiadomosciTable::class;
    }

    public function dowiazInneTabele() {
	$this->encja->uzytkownik = $this->uzyjWlasciwejFabryki(\Logowanie\Model\Uzytkownik::class);
	$this->encja->wiadomosc = $this->uzyjWlasciwejFabryki(\Konwersacje\Entity\Wiadomosc::class);
    }

}
