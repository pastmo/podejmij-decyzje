<?php

namespace Testy\FabrykiKonkretnychEncji;

class ProjektowyWatekChatuUzytkownikFactory extends \FabrykaAbstrakcyjna {

    public $uzytkownik;

    public function nowaEncja() {
	$encja = new \Konwersacje\Entity\ProjektowyWatekChatuUzytkownik();
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Konwersacje\Menager\ProjektoweWatkiChatuUzytkownicyMenager::class;
    }

    public function dowiazInneTabele() {

	$watekWiadomosci = $this->uzyjWlasciwejFabryki(\Konwersacje\Entity\WatekWiadomosci::class);

	$this->encja->projektowy_watek_chatu = $this->uzyjWlasciwejFabryki(\Konwersacje\Entity\ProjektowyWatekChatu::class,
		array('watek_wiadomosci' => $watekWiadomosci));
	$this->encja->uzytkownik = $this->uzyjWlasciwejFabryki(\Logowanie\Model\Uzytkownik::class);
    }

    public static function create(\Pastmo\Testy\ParametryFabryki\ParametryFabryki $pf) {
	$factory = new ProjektowyWatekChatuUzytkownikFactory($pf);
	return $factory;
    }

}
