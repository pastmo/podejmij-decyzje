<?php

namespace Testy\FabrykiKonkretnychEncji;

class ProjektTagFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Projekty\Entity\ProjektTag();
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Projekty\Menager\ProjektyTagiMenager::class;
    }

}
