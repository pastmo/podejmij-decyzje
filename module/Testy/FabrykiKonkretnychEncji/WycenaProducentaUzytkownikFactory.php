<?php

namespace Testy\FabrykiKonkretnychEncji;

class WycenaProducentaUzytkownikFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Aukcje\Entity\WycenaProducentaUzytkownik();
	$encja->cena_za_sztuke = rand(1, 100);
	$encja->komentarz = "komentarz";
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Aukcje\Menager\WycenyProducentowUzytkownicyMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->wycena_producentow_wariacja = $this->uzyjWlasciwejFabryki(\Aukcje\Entity\WycenaProducentaWariacja::class);
	$this->encja->uzytkownik = $this->uzyjWlasciwejFabryki(\Logowanie\Model\Uzytkownik::class);
	$this->encja->firma = $this->uzyjWlasciwejFabryki(\Firmy\Entity\Firma::class);
    }

}
