<?php

namespace Testy\FabrykiKonkretnychEncji;

class ProjektOznaczenieFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Projekty\Entity\ProjektOznaczenie();
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Projekty\Menager\ProjektyOznaczeniaMenager::class;
    }

    public function dowiazInneTabele() {
    }

}
