<?php

namespace Testy\FabrykiKonkretnychEncji;

class UzytkownikFirmaFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Firmy\Entity\UzytkownikFirma();
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Firmy\Menager\UzytkownicyFirmyMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->uzytkownik = $this->uzyjWlasciwejFabryki(\Logowanie\Model\Uzytkownik::class);
	$this->encja->firma = $this->uzyjWlasciwejFabryki(\Firmy\Entity\Firma::class);
    }

}
