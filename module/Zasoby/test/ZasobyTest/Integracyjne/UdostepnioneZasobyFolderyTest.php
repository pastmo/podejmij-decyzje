<?php

namespace ZasobyTest\Integracyjne;

use Zasoby\Entity\UdostepnionyZasobFolder;
use Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane;
use \Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane;

require_once dirname(__FILE__) . '\..\..\..\..\Wspolne\test\Fabryki\FabrykaRekordow.php';

class UdostepnioneZasobyFolderyTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

	protected $traceError = true;
	protected $sm;
	protected $udostepnioneZasobyFolderyMenager;

	public function setUp() {
		$this->setApplicationConfig(
			include \Application\Stale::configTestPath
		);
		parent::setUp();

		$this->sm = $this->getApplicationServiceLocator();
		$this->udostepnioneZasobyFolderyMenager = $this->sm->get(\Zasoby\Menager\UdostepnioneZasobyFolderyMenager::class);
	}

	public function test_dodaj() {
		$udostepnionyFolder = \FabrykaRekordow::makeEncje(UdostepnionyZasobFolder::class,
				\Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuPowiazane::create($this->sm));
		$nazwa = $udostepnionyFolder->nazwa;
		$this->udostepnioneZasobyFolderyMenager->zapisz($udostepnionyFolder);
		$ostatniRekord = $this->udostepnioneZasobyFolderyMenager->getPoprzednioDodany();

		$this->assertEquals($ostatniRekord->nazwa, $nazwa);
	}

}
