<?php

namespace ZasobyTest\Integracyjne;

use Zasoby\Menager\ZasobyUploadMenager;

class ZasobyDownloadMenager_zipTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

	private $zasobyTable;

	public function setUp() {
		parent::setUp();
		$this->zasobyTable = new ZasobyUploadMenager($this->sm);
	}

	public function testTworzenieZipa() {
		$nazwa = 'testowy_zip';
		$pelnaNazwa = $this->zasobyTable->realPathTmp($nazwa . ".zip");

		$this->usunStarePliki($pelnaNazwa);

		$zasob = $this->zasobyTable->zapiszPlik('plik_testowy.txt',
				'Treść pliku testowego2');

		$odpowiedz = $this->zasobyTable->downloadZip(array($zasob->id), 'testowy_zip');

		$this->assertTrue(file_exists($pelnaNazwa));
		$this->assertNotNull($odpowiedz);
	}

	private function usunStarePliki($pelnaNazwa) {
		if (file_exists($pelnaNazwa)) {
			unlink($pelnaNazwa);
		}
		$this->assertFalse(file_exists($pelnaNazwa));
	}

}
