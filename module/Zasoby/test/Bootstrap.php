<?php
namespace ZasobyTest;

require_once dirname(__FILE__) . '/../../Wspolne/test/WspolneBootstrap.php';

class Bootstrap extends \WspolneTest\WspolneBootstrap {

}

Bootstrap::init(array(
	'Application','Logowanie','Zasoby','Wspolne','Email'
));
Bootstrap::chroot();
