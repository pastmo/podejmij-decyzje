<?php

namespace Zasoby\Helper;

use Zend\View\Helper\AbstractHelper;

class WyswietlDodawanieZasobow extends AbstractHelper {

    public function __construct() {

    }

    public function __invoke($inputDocelowy, $view, $selektorSubmita = ".start") {
	?>
	<div class="dropzone zasoby_upload"
	     data-desc="<?php echo $inputDocelowy; ?>"
	     data-submit="<?php echo $selektorSubmita; ?>"
	     data-url="<?php echo $view->url('zasoby', array('action' => "upload")); ?>">
	    <div class="dz-default dz-message"></div>
	</div>
	<?php
    }

}
