<?php

namespace Zasoby\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ZasobyController extends \Wspolne\Controller\WspolneController {

    public function indexAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);
	return new ViewModel(array(
		//'zasoby' => $this->getZasobyTable()->fetchAll(),
	));
    }

    public function dodajAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$post = $this->getPost();
	if ($post) {
	    $zasobyArray = $this->projektyMenager->pobierzIdZasobow($post, 'zasoby');
	}

	return new ViewModel(array(
	));
    }

    public function uploadAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$wynik = $this->zasobyFasada->zaladujPlik();
	if (count($wynik) > 0) {
	    return $this->returnSuccess(array('zasoby' => $wynik));
	} else {
	    return $this->returnFail();
	}
    }

    public function downloadAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$id = $this->params()->fromRoute('id', 0);
	$response = $this->zasobyFasada->downloadZasobResponse($id);
	return $response;
    }

    public function downloadZipAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$nazwa = $this->params()->fromQuery('nazwa', 'download');
	$ids = $this->params()->fromQuery('ids', array());

	$response = $this->zasobyFasada->downloadZip($ids, $nazwa);
	return $response;
    }

    ///TODO: Ta metoda na razie nie działa. Proszę jej nie używać
    public function wyswietlMiniaturkeAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);
	$id = $this->params()->fromRoute('id', 0);

	$response = $this->getResponse();

	$zasob = $this->zasobyFasada->getRekord($id);

	$fileName = \Zasoby\Menager\ZasobyUploadMenager::$uploadDir . $zasob->url;

	$imageContent = fopen($fileName, 'r');

	$response->setContent($imageContent);
	$response
		->getHeaders()
		->addHeaderLine('Content-Transfer-Encoding', 'binary')
		->addHeaderLine('Content-Type', 'image/png')
		->addHeaderLine('Content-Length', filesize($fileName));

	return $response;
//		$id = $this->params()->fromRoute('id', 0);
//		$response = $this->zasobyFasada->wyswietlMiniaturke($id);
//		return $response;
    }

}
