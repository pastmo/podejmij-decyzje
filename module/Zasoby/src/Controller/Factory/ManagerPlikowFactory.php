<?php

namespace Zasoby\Controller\Factory;

use Zasoby\Controller\ManagerPlikowController;

class ManagerPlikowFactory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new ManagerPlikowController();
    }

}
