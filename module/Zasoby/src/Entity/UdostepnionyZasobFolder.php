<?php

namespace Zasoby\Entity;

class UdostepnionyZasobFolder extends \Wspolne\Model\WspolneModel {

    public $id;
    public $uzytkownik;
    public $folder_nadrzedny;
    public $nazwa;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->uzytkownik = $this->pobierzLubNull($data, 'uzytkownik_id');
	$this->folder_nadrzedny = $this->pobierzLubNull($data, 'folder_nadrzedny_id');
	$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'uzytkownik':
		return 'uzytkownik_id';
	    case 'folder_nadrzedny':
		return 'folder_nadrzedny_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
