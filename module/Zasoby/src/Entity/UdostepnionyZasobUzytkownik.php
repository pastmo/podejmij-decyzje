<?php

namespace Zasoby\Entity;

class UdostepnionyZasobUzytkownik extends \Wspolne\Model\WspolneModel {

    public $id;
    public $uzytkownik;
    public $rola;
    public $udostepniony_zasob;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->uzytkownik = $this->pobierzLubNull($data, 'uzytkownik_id');
	$this->rola = $this->pobierzLubNull($data, 'rola_id');
	$this->udostepniony_zasob = $this->pobierzLubNull($data, 'udostepniony_zasob_id');
	if ($this->czyTabeleDostepne()) {
	    $this->data = $data;
	    $this->uzytkownik = $this->pobierzTabeleObca('uzytkownik_id', \Logowanie\Menager\UzytkownicyMenager::class,
		    new \Logowanie\Model\Uzytkownik());
	    $this->rola = $this->pobierzTabeleObca('rola_id', \Logowanie\Menager\RoleMenager::class,
		    new \Logowanie\Entity\Rola());
	    $this->udostepniony_zasob = $this->pobierzTabeleObca('udostepniony_zasob_id',
		    \Zasoby\Menager\UdostepnioneZasobyMenager::class, new \Zasoby\Entity\UdostepnionyZasob());
	}
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'uzytkownik':
		return 'uzytkownik_id';
	    case 'rola':
		return 'rola_id';
	    case 'udostepniony_zasob':
		return 'udostepniony_zasob_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
