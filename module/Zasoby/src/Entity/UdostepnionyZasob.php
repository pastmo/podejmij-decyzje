<?php

namespace Zasoby\Entity;

class UdostepnionyZasob extends \Wspolne\Model\WspolneModel {

    public $id;
    public $wlasciciel;
    public $udostepniony_zasob_folder;
    public $zasob;
    public $uzytkownicy;

    public function dowiazListyTabelObcych() {
	if ($this->czyTabeleDostepne()) {
	    $udostepnioneUzytkownicyMenager = $this->sm->get(\Zasoby\Menager\UdostepnioneZasobyUzytkownicyMenager::class);
	    $this->uzytkownicy = $udostepnioneUzytkownicyMenager->dowiazanieUzytkownikowDoPliku($this->id);
	}
    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->wlasciciel = $this->pobierzLubNull($data, 'wlasciciel_id');
	$this->udostepniony_zasob_folder = $this->pobierzLubNull($data, 'udostepniony_zasob_folder_id');
	$this->zasob = $this->pobierzLubNull($data, 'zasob_id');
	if ($this->czyTabeleDostepne()) {
	    $this->data = $data;
	    $this->wlasciciel = $this->pobierzTabeleObca('wlasciciel_id', \Logowanie\Menager\UzytkownicyMenager::class,
		    new \Logowanie\Model\Uzytkownik());
	    $this->udostepniony_zasob_folder = $this->pobierzTabeleObca('udostepniony_zasob_folder_id',
		    \Zasoby\Menager\UdostepnioneZasobyFolderyMenager::class, new \Zasoby\Entity\UdostepnionyZasobFolder());
	    $this->zasob = $this->pobierzTabeleObca('zasob_id', \Zasoby\Menager\ZasobyUploadMenager::class,
		    new \Zasoby\Model\Zasob());
	}
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'wlasciciel':
		return 'wlasciciel_id';
	    case 'udostepniony_zasob_folder':
		return 'udostepniony_zasob_folder_id';
	    case 'zasob':
		return 'zasob_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
