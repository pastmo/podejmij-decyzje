<?php

namespace Zasoby\Menager;

use Zend\ServiceManager\ServiceManager;

class UdostepnioneZasobyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    private $udostepnioneZasobyFolderyMenager;
    private $udostepnioneZasobyUzytkownicyMenager;
    private $zasobyFasada;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Zasoby\Module::UDOSTEPNIONE_ZASOBY_GATEWAY);

	$this->udostepnioneZasobyFolderyMenager = $this->sm->get(\Zasoby\Menager\UdostepnioneZasobyFolderyMenager::class);
	$this->udostepnioneZasobyUzytkownicyMenager = $this->sm->get(\Zasoby\Menager\UdostepnioneZasobyUzytkownicyMenager::class);
    }

    public function pobierzPlikiUzytkownika($uzytkownik_id, $folder_id) {

	if (!$folder_id) {
	    $query = "wlasciciel_id = $uzytkownik_id AND udostepniony_zasob_folder_id IS NULL";
	} else {
	    $query = "wlasciciel_id = $uzytkownik_id AND udostepniony_zasob_folder_id = $folder_id";
	}

	$pliki = $this->pobierzZWherem($query);

	usort($pliki, function($a, $b) {
	    return strcmp($a->zasob->nazwa, $b->zasob->nazwa);
	});

	return $pliki;
    }

    public function dodajZasobyUzytkownika($zasoby, $uzytkownik_id, $folder_id) {
	$zwrotnaTabela = [];
	foreach ($zasoby as $zasob) {
	    $udostepnionyZasob = new \Zasoby\Entity\UdostepnionyZasob();
	    $udostepnionyZasob->wlasciciel = $uzytkownik_id;
	    if ($folder_id)
		$udostepnionyZasob->udostepniony_zasob_folder = $folder_id;
	    $udostepnionyZasob->zasob = $zasob->id;

	    $this->zapisz($udostepnionyZasob);
	    $zwrotnaTabela[] = $this->getPoprzednioDodany();
	}

	return $zwrotnaTabela;
    }

    public function usunUdostepnionyPlik($udostepnionyZasobId) {
	$this->zasobyFasada = $this->sm->get(\Zasoby\Fasada\ZasobyFasada::class);
	if ($this->pobierzZWheremCount("id = $udostepnionyZasobId")) {
	    $udostepnionyZasob = $this->getRekord($udostepnionyZasobId);
	    $zwrotne = $this->usunPojedynczyZasob($udostepnionyZasobId, $udostepnionyZasob->zasob->id);
	}
	return $zwrotne;
    }

    public function pobierzUdostepnianeUzytkownikomZasoby($uzytkownikId) {
	$zasoby = $this->pobierzZWherem("wlasciciel_id = $uzytkownikId");
	$udostepnianeZasoby = [];
	foreach ($zasoby as $zasob) {
	    $zasob->dowiazListyTabelObcych();
	    if (!empty($zasob->uzytkownicy))
		$udostepnianeZasoby[] = $zasob;
	}

	return $udostepnianeZasoby;
    }

    public function usunUdostepnionyFolder($udostepnionyFolderId) {
	try {
	    $this->beginTransaction();
	    $this->zasobyFasada = $this->sm->get(\Zasoby\Fasada\ZasobyFasada::class);
	    $this->obsluzPodfoldery($udostepnionyFolderId);
	    $this->commit();
	} catch (Exception $ex) {
	    $this->rollback();
	    echo $ex->getMessage();
	}
    }

    private function obsluzPodfoldery($folderId) {
	$podfoldery = $this->udostepnioneZasobyFolderyMenager->sprawdzPodfoldery($folderId);
	if ($podfoldery) {
	    foreach ($podfoldery as $podfolder) {
		$this->obsluzPodfoldery($podfolder->id);
	    }
	} else {
	    $this->usunZawartoscFolderu($folderId);
	    $this->udostepnioneZasobyFolderyMenager->usunFolder($folderId);
	}
    }

    private function usunZawartoscFolderu($folderId) {
	$zawartosc = $this->pobierzZWherem("udostepniony_zasob_folder_id = $folderId");
	foreach ($zawartosc as $encja) {
	    $this->usunPojedynczyZasob($encja->id, $encja->zasob->id);
	}
    }

    private function usunPojedynczyZasob($id, $zasobId) {
	if (!$this->udostepnioneZasobyUzytkownicyMenager->sprawdzCzyUdostepniany($id)) {
	    $this->usunPoId($id);
	    $this->zasobyFasada->usunZasob($zasobId);
	    return true;
	} else
	    return false;
    }

}
