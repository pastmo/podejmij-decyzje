<?php

namespace Zasoby\Menager;

use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;
use Pastmo\Wspolne\Utils\EntityUtil;

/**
 * Do poprawnego działania konieczna jest aktywacja extension=php_fileinfo.dll, w php.ini
 */
class ZasobyUploadMenager extends \Pastmo\Zasoby\Menager\ZasobyUploadMenager {

     protected $wyszukiwanieZKontem = false;

    protected function kodujNazwe($nazwa, $rozszerzenie) {
	$zalogowany = $this->get(\Logowanie\Fasada\LogowanieFasada::class)->getZalogowanegoUsera();
	$idKonta = EntityUtil::wydobadzId(EntityUtil::wydobadzPole($zalogowany, 'konto'), 'id');

	$result = parent::kodujNazwe($nazwa, $rozszerzenie);

	if ($idKonta) {
	    $result = "$idKonta/$result";
	}

	return $result;

    }

    public static function getClass() {
	return __CLASS__;
    }

}
