<?php

namespace Zasoby;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zasoby\Model\Zasob;
use Zasoby\Menager\ZasobyUploadMenager;
use Zasoby\Menager\ZasobyDownloadMenager;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Mvc\MvcEvent;

class Module implements ConfigProviderInterface {

    const UDOSTEPNIONE_ZASOBY_GATEWAY = 'UdostepnioneZasobyGateway';
    const UDOSTEPNIONE_ZASOBY_FOLDERY_GATEWAY = 'UdostepnioneZasobyFolderyGateway';
    const UDOSTEPNIONE_ZASOBY_UZYTKOWNICY_GATEWAY = 'UdostepnioneZasobyUzytkownicyGateway';

    public function onBootstrap(MvcEvent $e) {
	$viewHelperMenager = $e->getApplication()->getServiceManager()->get('ViewHelperManager');

	$viewHelperMenager
		->setFactory('wyswietlDodawanieZasobow',
			function($sm) use ($e) {
		    $viewHelper = new Helper\WyswietlDodawanieZasobow();
		    return $viewHelper;
		})
	;
    }

    public function getConfig() {
	return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig() {
	return array(
		'factories' => array(
			Fasada\ZasobyFasada::class => function($sm) {
			    $table = new Fasada\ZasobyFasada($sm);
			    return $table;
			},
			Menager\UdostepnioneZasobyMenager::class => function($sm) {

			    $table = new Menager\UdostepnioneZasobyMenager($sm);
			    return $table;
			},
			Menager\UdostepnioneZasobyFolderyMenager::class => function($sm) {

			    $table = new Menager\UdostepnioneZasobyFolderyMenager($sm);
			    return $table;
			},
			Menager\UdostepnioneZasobyUzytkownicyMenager::class => function($sm) {

			    $table = new Menager\UdostepnioneZasobyUzytkownicyMenager($sm);
			    return $table;
			},
			self::UDOSTEPNIONE_ZASOBY_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Entity\UdostepnionyZasob($sm));
			    return new TableGateway('udostepnione_zasoby', $dbAdapter, null, $resultSetPrototype);
			},
			self::UDOSTEPNIONE_ZASOBY_FOLDERY_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Entity\UdostepnionyZasobFolder($sm));
			    return new TableGateway('udostepnione_zasoby_foldery', $dbAdapter, null, $resultSetPrototype);
			},
			self::UDOSTEPNIONE_ZASOBY_UZYTKOWNICY_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Entity\UdostepnionyZasobUzytkownik($sm));
			    return new TableGateway('udostepnione_zasoby_uzytkownicy', $dbAdapter, null, $resultSetPrototype);
			},
		),
	);
    }

    public static function dowiazMenagery(\Wspolne\Controller\Factory\InitObiekt $initObiekt,
	    \Interop\Container\ContainerInterface $container) {

	$initObiekt->zasobyFasada = $container->get(\Zasoby\Fasada\ZasobyFasada::class);
	$initObiekt->zasobyTable = $container->get(\Zasoby\Menager\ZasobyUploadMenager::class);
    }

}
