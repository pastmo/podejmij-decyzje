<?php

namespace Uzytkownicy\Entity;

class Konto extends \Wspolne\Model\WspolneModel {

    public $id;
    public $rejestrujacy_id;
    public $data_dodania;
    public $data_waznosci;

    public function exchangeArray($data) {
	$this->data = $data;

	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->data_dodania = $this->pobierzLubNull($data, 'data_dodania');
	$this->rejestrujacy_id = $this->pobierzLubNull($data, 'rejestrujacy_id');
	$this->data_waznosci = $this->pobierzLubNull($data, 'data_waznosci');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'rejestrujacy':
		return 'rejestrujacy_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
