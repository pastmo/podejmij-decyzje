<?php

namespace Uzytkownicy\Controller\Factory;

use Uzytkownicy\Controller\UzytkownicyController;

class UzytkownicyControllerFactory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

	protected function newController() {
		return new UzytkownicyController();
	}

}
