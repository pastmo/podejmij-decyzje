<?php

namespace Uzytkownicy\Menager;

use Pastmo\Platnosci\Entity\Platnosc;
use Pastmo\Wspolne\Utils\DateUtil;
use Pastmo\Wspolne\Utils\EntityUtil;

class KontaMenager extends \Pastmo\Wspolne\Menager\WspolnyMenagerBazodanowy {

    protected $wyszukiwanieZKontem = false;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Uzytkownicy\Module::KONTA_GATEWAY);
    }

    public function zapisz(\Pastmo\Wspolne\Entity\WspolnyModel $model) {
	parent::zapisz($model);

    }

    public function czyWazneKonto() {
	$zalogowany = $this->get(\Logowanie\Fasada\LogowanieFasada::class)->getZalogowanegoUsera();

	$teraz = $this->getCurrentTimestamp();

	$waznoscKonta = EntityUtil::wydobadzPole(EntityUtil::wydobadzPole($zalogowany, 'konto'), 'data_waznosci');

	if (!$waznoscKonta || DateUtil::data1WiekszOd2($teraz, $waznoscKonta)) {
	    return false;
	} else {
	    return true;
	}
    }

    public function przedluzKonto(\Pastmo\Platnosci\Entity\Platnosc $platnosc) {
	$cennik = $platnosc->cennik;
	$konto = $platnosc->konto;
	$teraz = $this->getCurrentTimestamp();

	$dataStart = DateUtil::data1WiekszOd2($teraz, $konto->data_waznosci) ? $teraz : $konto->data_waznosci;

	$dataDo = DateUtil::zwiekszDateODni($cennik->dni_przedluzenia, $dataStart);

	$konto->data_waznosci = $dataDo->format(DateUtil::FORMAT_TIMESTAMP);

	$this->zapisz($konto);
    }

}
