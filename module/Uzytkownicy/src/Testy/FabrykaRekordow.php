<?php

namespace Uzytkownicy\Testy;

class FabrykaRekordow {

    public static function makeEncje($klasa, $parametryFabryki) {
	switch ($klasa) {
	    case \Uzytkownicy\Entity\Konto::class:
		return (new FabrykiKonkretnychEncji\KontoFactory($parametryFabryki))->makeEncje();
	    case \Logowanie\Model\Uzytkownik::class:
		return (new FabrykiKonkretnychEncji\UzytkownikFactory($parametryFabryki))->makeEncje();
	}
	return false;
    }

}
