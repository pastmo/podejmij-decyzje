<?php

namespace Uzytkownicy\Testy\FabrykiKonkretnychEncji;

class UzytkownikFactory extends \Pastmo\Testy\FabrykiKonkretnychEncji\UzytkownikFactory {

    public function dowiazInneTabele() {
	$this->encja->konto = $this->uzyjWlasciwejFabryki(\Uzytkownicy\Entity\Konto::class);
	$this->encja->rola = $this->uzyjWlasciwejFabryki(\Logowanie\Entity\Rola::class);
    }

}
