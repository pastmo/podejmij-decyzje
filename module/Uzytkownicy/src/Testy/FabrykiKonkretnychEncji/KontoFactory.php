<?php

namespace Uzytkownicy\Testy\FabrykiKonkretnychEncji;

class KontoFactory extends \Pastmo\Testy\Fabryki\FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Uzytkownicy\Entity\Konto($this->parametryFabryki->sm);
	$encja->rejestrujacy_id = $this->uzyjWlasciwejFabryki(\Logowanie\Model\Uzytkownik::class)->id;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Uzytkownicy\Menager\KontaMenager::class;
    }

    public function dowiazInneTabele() {

    }

}
