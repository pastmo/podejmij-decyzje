<?php

namespace UzytkownicyTest\Integracyjne;

use Pastmo\Testy\Util\TB;
use Pastmo\Platnosci\Entity\Cennik;

class KontaMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $kontoMenager;

    public function setUp() {
	parent::setUp();
	$this->kontoMenager = $this->sm->get(\Uzytkownicy\Menager\KontaMenager::class);
    }

    public function test_dodajEncje() {
	$konto = $this->zrobKonto();

	$this->assertNotNull($konto->id);
	$this->assertNotNull($konto->data_waznosci);
	$this->sprawdzCzyUtworzonyFolderDlaKonta($konto->id);
    }

    public function test_czyWazneKonto() {
	$this->utworzIZalogujUzytkownika();
	$this->mockujCzasMenagera("2018-01-01 10:01");

	$wynik = $this->kontoMenager->czyWazneKonto();

	$this->assertTrue($wynik);
    }

    public function test_czyWazneKonto_niezalogowany() {
	$this->mockujCzasMenagera("2018-01-01 10:01");

	$wynik = $this->kontoMenager->czyWazneKonto();

	$this->assertFalse($wynik);
    }

    public function test_przedluzKonto() {
	$this->utworzIZalogujUzytkownika();
	$this->mockujCzasMenagera("2019-01-01 10:01:00");

	$platnosc = TB::create(\Pastmo\Platnosci\Entity\Platnosc::class, $this)
			->addOneToMany('cennik', TB::create(Cennik::class, $this)
				->setParameters(['dni_przedluzenia' => 365]))->make();

	$this->kontoMenager->przedluzKonto($platnosc);

	$zapisane = $this->kontoMenager->getRekord($platnosc->konto->id);
	$this->assertEquals("2020-01-01 10:01:00", $zapisane->data_waznosci);
    }

    private function sprawdzCzyUtworzonyFolderDlaKonta($id) {
	$menager = $this->sm->get(\Zasoby\Menager\ZasobyDownloadMenager::class);
	$fullPath = $menager->zasobRealPathZeStringa($id);
	$this->assertTrue(file_exists($fullPath));
    }

    private function zrobKonto() {
	return TB::create(\Uzytkownicy\Entity\Konto::class, $this)
			->setPF_IZP($this->sm)
			->make();
    }

    protected function mockujCzasMenagera($wartosc) {

	$this->czasMenager = $this->getMockBuilder(\Pastmo\Wspolne\Menager\CzasMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->sm->setService(\Pastmo\Wspolne\Menager\CzasMenager::class, $this->czasMenager);
	$this->czasMenager->method('getAktualnyTimestampBezSekund')->willReturn($wartosc);
	$this->czasMenager->method('getAktualnyTimestamp')->willReturn($wartosc);
    }

}
