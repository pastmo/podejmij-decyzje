<?php

namespace LogowanieTest;

require_once dirname(__FILE__) . '/../../Wspolne/test/WspolneBootstrap.php';

class Bootstrap extends \WspolneTest\WspolneBootstrap {

}

Bootstrap::init(array(
	'Wspolne', 'Logowanie','Application','Email','Firmy','Projekty','Zasoby','Pastmo'
));
Bootstrap::chroot();
