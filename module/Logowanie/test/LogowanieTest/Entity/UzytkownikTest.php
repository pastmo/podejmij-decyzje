<?php

namespace LogowanieTest\Entity;

class UzytkownikTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_json_encode() {
	$uzytkownik = \FabrykaEncjiMockowych::makeEncje(\Logowanie\Model\Uzytkownik::class,
			\Pastmo\Testy\ParametryFabryki\PFMockowePowiazane::create());

	$wynik = json_encode($uzytkownik);
	$this->sprawdzStringNieZawiera($wynik, '"haslo":"haslo"');
    }


}
