<?php

namespace LogowanieTest\Integracyjne;

use Logowanie\Model\Uzytkownik;
use Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane;
use Pastmo\Testy\Util\TB;

class UzytkownicyTableTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    protected $traceError = true;
    private $uzytkownikTable;

    public function setUp() {
	parent::setUp();

	$this->uzytkownikTable = $this->sm->get('Logowanie\Menager\UzytkownicyMenager');
    }

    public function test_dodajRekord() {
	$nazwa = 'test_dodajRekord';
	$uzytkownikTable = $this->sm->get('Logowanie\Menager\UzytkownicyMenager');

	$uzytkownik = new Uzytkownik();
	$uzytkownik->login = $nazwa;
	$uzytkownikTable->zapisz($uzytkownik);

	$ostatniRekord = $uzytkownikTable->getPoprzednioDodany();

	$konto = TB::create(\Uzytkownicy\Entity\Konto::class, $this)->setParameters(['rejestrujacy' => $ostatniRekord])->make();
	$ostatniRekord->konto = $konto;

	$uzytkownikTable->zapisz($ostatniRekord);

	$ostatniRekord = $uzytkownikTable->getRekord($ostatniRekord->id);

	$this->assertEquals($ostatniRekord->login, $nazwa);
	$this->assertEquals($ostatniRekord->jezyk_kod, \Pastmo\Uzytkownicy\Enumy\KodyJezykow::DOMYSLNY);
	$this->assertNotNull($ostatniRekord->konto->data_dodania);
    }

    public function test_zapisz_ustawianie_typu_z_roli() {
	$nazwa = 'test_dodajRekord';

	$rola = \FabrykaRekordow::makeEncje(\Logowanie\Entity\Rola::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm)
				->setparametry(array('typ' => \Logowanie\Model\TypyUzytkownikow::PRACOWNIK)));

	$uzytkownikTable = $this->sm->get('Logowanie\Menager\UzytkownicyMenager');

	$uzytkownik = new Uzytkownik();
	$uzytkownik->login = $nazwa;
	$uzytkownik->rola = $rola->id;
	$uzytkownikTable->zapisz($uzytkownik);

	$ostatniRekord = $uzytkownikTable->getPoprzednioDodany();

	$this->assertEquals($ostatniRekord->login, $nazwa);
	$this->assertEquals($ostatniRekord->typ, \Logowanie\Model\TypyUzytkownikow::PRACOWNIK);
    }

    public function test_pobierzUzytkownikowZUprawnieniamiBezZalogowanego() {
	$this->ustawZalogowanegoUseraId1();
	$uzytkownicy = $this->uzytkownikTable->pobierzUzytkownikowZUprawnieniamiBezZalogowanego(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE);

	$this->assertTrue(count($uzytkownicy) > 0);

	foreach ($uzytkownicy as $uzytkownik) {
	    $this->assertNotSame('1', $uzytkownik->id);
	}
    }

    public function test_zapiszZPosta_sprawdzanie_branu_napisywania_nieedytowanych_kolumn() {

	$uzytkownik = \FabrykaRekordow::makeEncje(Uzytkownik::class,
			PFIntegracyjneZapisPowiazane::create($this->sm)
				->setparametry(array('czy_aktywny' => true)));

	$post = array('id' => $uzytkownik->id, 'imie' => 'nowe imie');
	$uzytkownikTable = $this->sm->get('Logowanie\Menager\UzytkownicyMenager');

	$uzytkownikTable->zapiszZPosta($post);

	$zmodyfikowany = $uzytkownikTable->getRekord($uzytkownik->id);

	$this->assertEquals('nowe imie', $zmodyfikowany->imie);
	$this->assertEquals(true, $zmodyfikowany->czy_aktywny);
    }

}
