<?php

namespace LogowanieTest\Integracyjne;

use Logowanie\Model\Uzytkownik;
use Pastmo\Testy\Util\TB;
use Pastmo\Platnosci\Menager\PlatnosciMenager;

class RejestracjaMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    protected $traceError = true;
    private $rejestracjaMenager;
    private $aktywacjaTable;
    private $infoDoAdmina;
    private $uzytkownikTable;

    public function setUp() {
	parent::setUp();

	$this->sm->setAllowOverride(true);
	$this->aktywacjaTable = $this->getMockBuilder(\Logowanie\Model\AktywacjaMail::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->infoDoAdmina = $this->getMockBuilder(\Logowanie\Model\InfoDoAdminaMail::class
		)
		->disableOriginalConstructor()
		->getMock();

	$this->sm->setService(\Logowanie\Model\AktywacjaMail::class, $this->aktywacjaTable);
	$this->sm->setService(\Logowanie\Model\InfoDoAdminaMail::class, $this->infoDoAdmina);

	$this->rejestracjaMenager = $this->sm->get(\Logowanie\Menager\RejestracjaMenager::class);
	$this->uzytkownikTable = $this->sm->get(\Logowanie\Menager\UzytkownicyMenager::class);
    }

    public function test_zarejestrujUzytkownika_adres_platnosci() {
	$uzytkownik = TB::create(\Pastmo\Uzytkownicy\Entity\Uzytkownik::class, $this)
			->setPF_IBN()->make();

	$platnosciMenager = $this->zrobMockObiektu(PlatnosciMenager::class, ['utworzPlatnoscDlaUzytkownika']);
	$platnosciMenager->method('utworzPlatnoscDlaUzytkownika')->willReturn('url_wynikowy');
	$this->nadpiszSerwis(PlatnosciMenager::class, $platnosciMenager);

	$post = $this->arrayToParameters(['uzytkownik' => [
			'mail' => $uzytkownik->mail,
			'haslo' => 'abc',
			'haslo2' => 'abc'
	]]);
	$wynik = $this->rejestracjaMenager->zarejestruj($post);

	$this->assertTrue($wynik->isSuccess, $wynik->message);
	$this->assertNotNull($wynik->urlPrzekierowania);

	$dodany = $this->rejestracjaMenager->getPoprzednioDodany();

	$this->assertEquals(1, $dodany->czy_aktywny);
	$this->assertNotNull($dodany->rola->id);
    }

}
