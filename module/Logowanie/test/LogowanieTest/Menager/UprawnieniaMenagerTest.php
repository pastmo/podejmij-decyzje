<?php

namespace LogowanieTest\Menager;

use Logowanie\Menager\UprawnieniaMenager;
use Logowanie\Entity\Uprawnienie;
use Logowanie\Model\Uzytkownik;

class UprawnieniaMenagerTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $user;
    private $zaloguj = true;

    public function setUp() {
	parent::setUp();
	$this->ustawMockaGateway(\Logowanie\Module::UPRAWNIENIA_GATEWAY);
	$this->uprawnieniaMenager = new UprawnieniaMenager($this->sm);
    }

    public function testSprawdzUprawnieniaUzytkownikaTrue() {
	$this->zaloguj = false;
	$this->ustawParametry(1);
	$result = $this->wykonajSprawdzanieDlaUzytkownika();
	$this->assertTrue($result);
    }

    public function testSprawdzUprawnieniaTrue() {
	$result = $this->sprawdzUprawnienia(1, false);
	$this->assertTrue($result);
    }

    public function testSprawdzUprawnieniaFalse() {
	$this->ustwaZalogowanegoUsera();
	$this->ustawPobieranieUprawnienia();
	$result = $this->wykonajSprawdzanie(false);
	$this->assertFalse($result);
    }

    public function testSprawdzUprawnieniaTrueZeSpodziewanymWyjątekiem() {
	try {
	    $this->sprawdzUprawnienia(1, true);
	} catch (\Exception $e) {
	    $this->fail("Niespodziewany wyjątek");
	}
    }

    public function testSprawdzUprawnieniaTryWyjatek() {
	try {
	    $this->ustwaZalogowanegoUsera();
	    $this->ustawPobieranieUprawnienia();
	    $result = $this->wykonajSprawdzanie(true);
	    $this->assertFalse($result);
	    $this->fail("Brak wyjątku");
	} catch (\Pastmo\Uzytkownicy\Exception\UprawnieniaException $e) {
	    $this->assertEquals("Brak uprawnienia: 'Opis uprawnienia'", $e->getMessage());
	} catch (\Exception $e) {
	    $this->fail("Nieznany wyjątek:" . $e->getMessage());
	}
    }

    public function testSprawdzUprawnieniaNiezalogwanyUser() {
	try {
	    $uprawnieniaMenager = new UprawnieniaMenager($this->sm);
	    $uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE,
		    true);

	    $this->fail("Brak wyjątku");
	} catch (\Pastmo\Uzytkownicy\Exception\LogowanieException $e) {
	    $this->assertEquals('Nie jesteś zalogowany', $e->getMessage());
	} catch (\Exception $e) {
	    $this->fail("Nieznany wyjątek:" . $e->getMessage());
	}
    }

    public function testUzytkownikBezRoli() {
	try {
	    $this->ustawParametry(0, 0);
	    $this->user->rola->id = null;
	    $this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE,
		    true);

	    $this->fail("Brak wyjątku");
	} catch (\Pastmo\Uzytkownicy\Exception\UprawnieniaException $e) {
	    $this->assertEquals('Użytkownik nie ma przypisanej roli', $e->getMessage());
	} catch (\Exception $e) {
	    $this->fail("Nieznany wyjątek:" . $e->getMessage());
	}
    }

    public function testSprawdzUprawnienieGdyNieMaUprawnieniaNaBazie() {
	try {
	    $this->ustwaZalogowanegoUsera();

	    $countRowSet = $this->obslugaTestowanejKlasy->ustawCountRowSet(0);

	    $this->obslugaTestowanejKlasy->ustawMockiDoPobierzZWherem(null, 2)
		    ->will($this->onConsecutiveCalls($countRowSet, array()));

	    $result = $this->wykonajSprawdzanie(true);
	    $this->assertFalse($result);
	    $this->fail("Brak wyjątku");
	} catch (\Pastmo\Uzytkownicy\Exception\UprawnieniaException $e) {
	    $this->assertEquals("Brak uprawnienia: 'Opis uprawnienia'", $e->getMessage());
	} catch (\Pastmo\Wspolne\Exception\PastmoException $e) {
	    $this->assertEquals("Brak uprawnienia o kodzie rol_def w bazie danych", $e->getMessage());
	}catch (\Exception $e) {
	    $this->fail("Nieznany wyjątek:" . $e->getMessage());
	}
    }

    private function sprawdzUprawnienia($count, $czyWyjatek) {
	$this->ustawParametry($count);

	return $this->wykonajSprawdzanie($czyWyjatek);
    }

    private function wykonajSprawdzanie($czyWyjatek) {
	return $this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE,
			$czyWyjatek);
    }

    private function wykonajSprawdzanieDlaUzytkownika() {
	return $this->uprawnieniaMenager->sprawdzUprawnienieUzytkownika(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE,
			$this->user);
    }

    private function ustawParametry($count, $expected = 1) {

	$this->ustwaZalogowanegoUsera();
	$this->ustawSelectWithCount($count, $expected);
    }

    private function ustwaZalogowanegoUsera() {
	$this->user = new Uzytkownik();
	$this->user->id = 42;
	$this->user->rola = new \Logowanie\Entity\Rola();
	$this->user->rola->id = 23;

	if ($this->zaloguj) {
	    $this->obslugaKlasObcych->ustawZalogowanegoUsera($this->user);
	}
    }

    private function ustawSelectWithCount($count, $expected = 1) {
	$this->obslugaTestowanejKlasy->ustawSelectWithCount($count, $expected);
    }

    private function ustawPobieranieUprawnienia() {
	$uprawnienie = new Uprawnienie();
	$uprawnienie->opis = 'Opis uprawnienia';

	$countRowSet = $this->obslugaTestowanejKlasy->ustawCountRowSet(0);

	$this->obslugaTestowanejKlasy->ustawMockiDoPobierzZWherem(null, 2)
		->will($this->onConsecutiveCalls($countRowSet, array($uprawnienie)));
    }

    protected function nowyObiekt() {
	new Uprawnienie();
    }

}
