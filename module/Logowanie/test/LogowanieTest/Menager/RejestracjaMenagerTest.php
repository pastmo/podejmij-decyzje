<?php

namespace LogowanieTest\Menager;

use \ProjektyTest\Menager\ProjektyMenagerTest;
use \Logowanie\Model\Uzytkownik;
use \Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane;

class RejestracjaMenagerTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $post = array(
	    'uzytkownik' => [
		    'mail' => 'email@pastmo.pl',
		    'haslo' => 'abc',
		    'haslo2' => 'abc'
	    ]
    );
    public $projekt;
    public $uzytkownik;
    public $dodanyUzytkownik;

    public function setUp() {
	$this->markTestIncomplete("Testy do przemyślenia i sprawdzenia");
	parent::setUp();

	$this->rejestracjaMenager = new \Logowanie\Menager\RejestracjaMenager($this->sm);

	$this->projekt = \FabrykaEncjiMockowych::utworzEncje(\Projekty\Entity\Projekt::class);
	$this->uzytkownik = \FabrykaEncjiMockowych::makeEncje(\Logowanie\Model\Uzytkownik::class,
			PFMockoweNiepowiazane::create());
    }

    public function test_zarejestruj() {

	$this->ustawUzytkownikaPoMailu(new \Logowanie\Model\Uzytkownik());
	$this->ustawFabrykeUzytkownikow();
	$this->ustawPoprzednioDodany();
	$this->sprawdzCzyUzytkownikJestZapisywany();
	$this->sprawdzWysylanieMailaDoUzytkownika(1);
	$this->sprawdzWysylanieMailaDoAdmina(1);

	$post = $this->arrayToParameters($this->post);
	$wynik = $this->rejestracjaMenager->zarejestruj($post);
	$this->assertTrue($wynik->isSuccess, $wynik->message);
    }

    public function test_zarejestruj_wersja_skrocona() {

	$this->ustawUzytkownikaPoMailu(new \Logowanie\Model\Uzytkownik());
	$this->ustawFabrykeUzytkownikow();
	$this->ustawPoprzednioDodany();
	$this->sprawdzCzyUzytkownikJestZapisywany();

	$this->post = $this->utworzSkroconyPost();

	$wynik = $this->rejestracjaMenager->zarejestruj($this->post);
	$this->assertTrue($wynik->isSuccess, $wynik->message);
    }


    public function test_pobierzZarejestrujUzytkownikaPoMailu() {

	$this->ustawUzytkownikaPoMailu(new Uzytkownik());
	$this->ustawFabrykeUzytkownikow();
	$this->ustawPoprzednioDodany();
	$this->sprawdzCzyUzytkownikJestZapisywany();

	$uzytkownik = new Uzytkownik();
	$uzytkownik->mail = "email@email.pl";
	$wynik = $this->rejestracjaMenager->pobierzZarejestrujUzytkownikaPoMailu($uzytkownik);
	$this->assertEquals($this->dodanyUzytkownik->id, $wynik->id);
    }

    public function testkonwertujPost() {
	$this->post = $this->utworzSkroconyPost();

	$wynik = $this->rejestracjaMenager->konwertujPost($this->post);
	$uzytkownik = new \Logowanie\Model\Uzytkownik();
	$uzytkownik->exchangeArray($wynik);

	$this->assertNotNull($uzytkownik->mail);
    }


    public function test_zarejestruj_niezgodne_hasla() {
	$this->post['uzytkownik']['haslo2'] = "Zupełnie inne hasło";

	$wynik = $this->rejestracjaMenager->zarejestruj($this->post);
	$this->assertFalse($wynik->isSuccess, $wynik);
    }

    protected function ustawFabrykeUzytkownikow() {

	$this->uzytkownikTable->expects($this->any())->method('createUzytkownik')->willReturn(new \Logowanie\Model\Uzytkownik());
    }

    protected function ustawFirme() {

	$firma = \FabrykaEncjiMockowych::utworzEncje(\Firmy\Entity\Firma::class);
	$this->firmyMenager->expects($this->once())->method('pobierzLubUtworzFirme')->willReturn($firma);
    }

    protected function ustawPoprzednioDodany() {
	$this->dodanyUzytkownik = \FabrykaEncjiMockowych::utworzEncje(\Logowanie\Model\Uzytkownik::class);
	$this->uzytkownikTable->expects($this->any())->method('getPoprzednioDodany')->willReturn($this->dodanyUzytkownik);
    }

    protected function sprawdzCzyUzytkownikJestZapisywany() {
	$this->uzytkownikTable->expects($this->exactly(1))->method('zapisz');
    }

    protected function sprawdzLaczenieUzytkownikaZFirma() {
	$this->firmyMenager->expects($this->exactly(1))->method('polaczUzytkownikaZFirma');
    }

    private function utworzSkroconyPost() {
	return $this->arrayToParameters($this->post);
    }

    public function aktywujKonto($get) {

    }

    private function sprawdzWysylanieMailaDoUzytkownika($ileRazy) {
	$this->aktywacjaTable->expects($this->exactly($ileRazy))->method('send');
    }

    private function sprawdzWysylanieMailaDoAdmina($ileRazy) {
	$this->infoDoAdminaMail->expects($this->exactly($ileRazy))->method('send');
    }

}
