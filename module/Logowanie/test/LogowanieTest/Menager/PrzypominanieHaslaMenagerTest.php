<?php

namespace LogowanieTest\Integracyjne;



use \ProjektyTest\Menager\ProjektyMenagerTest;

class PrzypominanieHaslaMenagerTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

	private $post = array(
		"email" => 'email@email.pl',
	);
	private $get = array(
		'id' => '1',
		'kod' => 'c13fde8c21332db79e4f057dcd518e48'
	);
	public $projekt;
	public $uzytkownik;

	public function setUp() {
		parent::setUp();

		$this->przypominanieHaslaMenager = new \Logowanie\Menager\PrzypominanieHaslaMenager($this->sm);

		$this->projekt = \FabrykaEncjiMockowych::utworzEncje(\Projekty\Entity\Projekt::class);
		$this->uzytkownik =  \FabrykaEncjiMockowych::utworzEncje(\Logowanie\Model\Uzytkownik::class);
	}

	public function test_wyslijPotwierdzeniePrzypominaniaHasla() {

		$this->ustawUzytkownikaPoMailu(\FabrykaEncjiMockowych::utworzEncje(\Logowanie\Model\Uzytkownik::class));
		$this->sprawdzWyslanieMaila();

		$wynik = $this->przypominanieHaslaMenager->wyslijPotwierdzeniePrzypominaniaHasla($this->post);
		$this->assertTrue($wynik->isSuccess, $wynik->message);
	}

	public function test_wyslijPotwierdzeniePrzypominaniaHasla_brak_uzytkownika() {
		$wynik = $this->przypominanieHaslaMenager->wyslijPotwierdzeniePrzypominaniaHasla($this->post);
		$this->assertFalse($wynik->isSuccess, $wynik->message);
	}

	public function test_sprzawdzZadaniePrzypomnieniaHasla() {

		$this->obslugaKlasObcych->ustawGetRecord(\Logowanie\Menager\UzytkownicyMenager::class,
				$this->uzytkownik);
		$this->ustawPrawidlowyKod();

		$wynik = $this->przypominanieHaslaMenager->sprzawdzZadaniePrzypomnieniaHasla($this->get);
		$this->assertTrue($wynik->isSuccess, $wynik->message);
	}

	public function test_sprzawdzZadaniePrzypomnieniaHasla_blednyKod() {

		$this->obslugaKlasObcych->ustawGetRecord(\Logowanie\Menager\UzytkownicyMenager::class,
				$this->uzytkownik);

		$wynik = $this->przypominanieHaslaMenager->sprzawdzZadaniePrzypomnieniaHasla($this->get);
		$this->assertFalse($wynik->isSuccess, $wynik->message);
	}

	public function test_sprzawdzZadaniePrzypomnieniaHasla_brakUzytkownika() {

		$this->obslugaKlasObcych->ustawGetRecord(\Logowanie\Menager\UzytkownicyMenager::class,
				new \Logowanie\Model\Uzytkownik());

		$wynik = $this->przypominanieHaslaMenager->sprzawdzZadaniePrzypomnieniaHasla($this->get);
		$this->assertFalse($wynik->isSuccess, $wynik->message);
	}

	public function test_zmienHasloZPrzypomnienia() {
		$post = array('id' => 5, 'nowe_haslo' => "nowe_haslo", 'nowe_haslo2' => 'nowe_haslo');

		$this->obslugaKlasObcych->ustawGetRecord(\Logowanie\Menager\UzytkownicyMenager::class,
				$this->uzytkownik);


		$this->uzytkownikTable->expects($this->once())->method('zapisz');

		$wynik = $this->przypominanieHaslaMenager->zmienHasloZPrzypomnienia((object)$post);
		$this->assertTrue($wynik->isSuccess, $wynik->message);
	}

	private function sprawdzWyslanieMaila() {
		$this->resetHaslaZapytanieMail->expects($this->once())->method('send');
	}

	protected function ustawPrawidlowyKod() {
		$this->get['kod'] = \Logowanie\Menager\RejestracjaMenager::generujKod($this->uzytkownik);
	}

}
