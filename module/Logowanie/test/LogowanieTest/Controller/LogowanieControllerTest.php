<?php

namespace LogowanieTest\Controller;

use Pastmo\Testy\Util\TB;

class LogowanieControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    private $msg = "wiadomość komunikat";

    const sukces = 'sukces';
    const nie_sukces = 'nie_sukces';
    const post = 'post';
    const nie_post = 'nie_post';
    const menager = 'wywolywany menager';
    const nie_menager = 'nie_menager menager';
    const msg = 'zawiera_msg';
    const nie_msg = 'nie_msg';
    const zawiera_form = 'zawiera_form';
    const nie_form = 'nie_form';

    private $zalogowany;
    private $post;

    public function setUp() {
	parent::setUp();

	$this->post = ['uzytkownik' => ['login' => 'login', 'haslo' => 'abc']];
    }

    public function testIndex_przekierowanie_na_strone_glowna() {
	$this->obslugaKlasObcych->ustawZalogowanegoUsera();
	$this->dispatch('/' . $this->getSmallName());
	$this->assertRedirectTo('/decyzje');
    }

    public function testStronaGlowna() {
	$this->dispatch('/');
	$this->sprawdzCzyStatus200();
    }

    public function testlogowanieAction_przekierowanie_w_przypadku_braku_posta() {
	$this->dispatch('/logowanie/logowanie');
	$this->sprawdzPrzekierowanie('/');
    }

    public function test_wyswietlOknoLogowaniaAction() {

	$this->dispatch('/logowanie/wyswietl_okno_logowania');
	$this->sprawdzCzyStatus200();
    }

    public function test_wyswietlOknoRejestracjiAction() {

	$this->dispatch('/logowanie/wyswietl_okno_rejestracji');
	$this->sprawdzCzyStatus200();
    }

    private function ustawRedirect($redirect) {
	$this->post['redirect'] = $redirect;
    }

    private function mockujZaloguj() {
	$this->obslugaKlasObcych->ustawZalogowanegoUsera();
	$this->uzytkownikTable->method('zaloguj')->willReturn($this->obslugaKlasObcych->zalogowany);
    }

    public function getBigName() {
	return 'Logowanie';
    }

    public function getSmallName() {
	return 'logowanie';
    }

    protected function sprawdzWywolanieRejestracji() {
	$this->rejestracjaMenager->expects($this->exactly(1))->method('zarejestruj');
    }

    protected function sprawdzWywolanieAktywacji() {
	$this->aktywacjaMenager->expects($this->exactly(1))->method('aktywujKonto')
		->will($this->returnValue(new \Logowanie\Polecenie\WynikPrzetwarzaniaFormularza(true, "sukces")));
    }

    protected function ustawWynikPrzypominania($sukces, $komunikat, $ileRazy, $metoda) {
	$wynik = new \Logowanie\Polecenie\WynikPrzetwarzaniaFormularza($sukces, $komunikat);
	$this->przypominanieHaslaMenager
		->expects($this->exactly($ileRazy))
		->method($metoda)
		->willReturn($wynik);
    }

}
