<?php

namespace LogowanieTest\Controller;

use Pastmo\Testy\Util\TB;

class LogowanieAjaxControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    public function setUp() {
	parent::setUp();
    }

    public function testIndex() {

    }

    public function test_zalogujAction() {
	$this->ustawParametryPosta([]);

	$odpowiedzLogowania = \Pastmo\Uzytkownicy\Entity\OdpowiedzLogowania::create()->setMsg("Wiadomosc");

	$this->uzytkownikTable->expects($this->once())->method('zalogujUzytkownika')->willReturn($odpowiedzLogowania);

	$this->dispatch('/logowanie_ajax/zaloguj');

	$this->sprawdzBodyZawiera('Wiadomosc');
    }

    public function test_zarejestrujAction() {
	$this->ustawParametryPosta([]);

	$odpowiedzLogowania = \Pastmo\Uzytkownicy\Entity\OdpowiedzLogowania::create()->setMsg("Wiadomosc");

	$this->rejestracjaMenager->expects($this->once())->method('zarejestruj')->willReturn($odpowiedzLogowania);

	$this->dispatch('/logowanie_ajax/zarejestruj');

	$this->sprawdzBodyZawiera('Wiadomosc');
    }

}
