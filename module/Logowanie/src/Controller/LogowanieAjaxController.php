<?php

namespace Logowanie\Controller;

use Pastmo\Wspolne\Adnotacje\DomyslnyJsonModel;

/**
 * @DomyslnyJsonModel
 */
class LogowanieAjaxController extends \Wspolne\Controller\WspolneController {

    public function zalogujAction() {
	$this->post = $this->getPost();
	$wynik = $this->uzytkownikTable->zalogujUzytkownika($this->post);

	return $this->returnJsonModel($wynik->toArray());
    }

    public function zarejestrujAction() {
	$post = $this->getPost();

	$wynik = $this->logowanieFasada->zarejestruj($post);

	return $this->returnJsonModel($wynik->toArray());
    }

}
