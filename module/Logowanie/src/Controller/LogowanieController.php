<?php

namespace Logowanie\Controller;

use Zend\View\Model\ViewModel;
use \Logowanie\Model\Uzytkownik;
use \Logowanie\Form\LogowanieForm;
use Logowanie\Form\RejestracjaForm;
use \Logowanie\Polecenie\WynikPrzetwarzaniaFormularza;
use Wspolne\Utils\SearchPole;
use \Logowanie\Enumy\KodyUprawnien;

class LogowanieController extends \Pastmo\Uzytkownicy\Controller\LogowanieController {

    protected $uzytkownikTable;
    protected $wystawcyFaktur;

    public function listaAction() {
	$this->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$pola = array(
		SearchPole::create()->setDataType(SearchPole::STRING)->setName("imie"),
		SearchPole::create()->setDataType(SearchPole::STRING)->setName("nazwisko"),
		SearchPole::create()->setDataType(SearchPole::STRING)->setName("mail"),
		SearchPole::create()->setDataType(SearchPole::QUERY)->setName("rola_id")->setQuery("IN( select id from role where nazwa %s)"),
	);

	$uzytkownicy = $this->pobierzZGeta($this->uzytkownikTable, 1, $pola);
	return new ViewModel(array('uzytkownicy' => $uzytkownicy
	));
    }

    public function roleAction() {
	$zalogowany = $this->uzytkownikTable->getZalogowanegoUsera();
	if (!$zalogowany) {
	    return $this->redirect()->toRoute('logowanie', array('action' => 'index'));
	}
	$role = $this->roleMenager->pobierzZWherem("1", "nazwa ASC");
	return new ViewModel(array(
		'role' => $role
	));
    }

    public function roleUprawnieniaAction() {
	$zalogowany = $this->uzytkownikTable->getZalogowanegoUsera();
	if (!$zalogowany) {
	    return $this->redirect()->toRoute('logowanie', array('action' => 'index'));
	}

	$request = $this->getRequest();
	if ($request->isPost()) {
	    $post = $request->getPost();
	    $roleId = $post->get('role_id');
	    $uprawnienia = $post->get('uprawnienia');
	    $this->uprawnieniaRoleMenager->uaktualnijUprawnienia($roleId, $uprawnienia);
	}

	$rolaId = (int) $this->params()->fromRoute('id', 0);

	$rola = $this->roleMenager->getRekord($rolaId);
	$uprawnieniaKategorie = $this->uprawnieniaKategorieMenager->pobierzZWherem("1", "kod ASC");
	return new ViewModel(array(
		'uprawnieniaKategorie' => $uprawnieniaKategorie,
		'rola' => $rola
	));
    }

    public function roleUzytkownicyAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$rolaId = $this->params()->fromRoute('id', 0);
	$rola = $this->roleMenager->getRekord($rolaId);


	$uzytkownicy = $this->uzytkownikTable->pobierzUzytkownikowZRola($rolaId);

	return new ViewModel(array(
		'uzytkownicy' => $uzytkownicy,
		'rola' => $rola
	));
    }

    public function profilAction() {

	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$request = $this->getRequest();
	if ($request->isPost()) {
	    $post = $request->getPost();

	    $this->uzytkownikTable->zapiszZPosta($post);
	}

	$id = $this->params()->fromRoute('id', 0);

	if ($id > 0) {
	    $uzytkownik = $this->uzytkownikTable->getRekord($id);
	} else {
	    $uzytkownik = $this->uzytkownikTable->getZalogowanegoUsera();
	}

	$role = $this->roleMenager->fetchAll('nazwa ASC');
	$firmy = $this->firmyFasada->pobierzFirmy("1");

	return new ViewModel(array(
		'uzytkownik' => $uzytkownik,
		'role' => $role,
		'firmy' => $firmy,
		'id' => $id
	));
    }

    public function listaUzytkownikoAction() {
	$this->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);
	return new ViewModel(array(
		'uzytkownicy' => $this->getUzytkownicyMenager()->fetchAll(),
	));
    }

    public function indexAction() {
	if ($this->uzytkownikTable->getZalogowanegoUsera()) {
	    return $this->domyslnePrzekierowanieZalogowanego();
	}

	$layout = $this->layout();
	$layout->setTemplate('logowanie/layout/logowanie_layout');

	return $this->wyswietlFormularzLogowaniaRejstracji('logowanie');
    }

    public function rejestracjaAction() {
	$wynikPrzetwarzanie = null;
	$post = $this->getPost();

	if ($post) {

	    $wynikPrzetwarzanie = $this->logowanieFasada->zarejestruj($post);
	    if ($wynikPrzetwarzanie->isSuccess) {
		$this->redirect()->toUrl($wynikPrzetwarzanie->urlPrzekierowania);
	    }
	}

	return $this->wyswietlFormularzRejestracji($wynikPrzetwarzanie, $post);
    }

    protected function wyswietlFormularzRejestracji($wynikPrzetwarzanie = null, $post = array()) {
	$layout = $this->layout();
	$layout->setTemplate('logowanie/layout/logowanie_layout');


	$model = $this->wyswietlFormularzLogowaniaRejstracji('rejestracja', $wynikPrzetwarzanie, $post);
	$model->setTemplate('logowanie/logowanie/index');
	return $model;
    }

    private function wyswietlFormularzLogowaniaRejstracji($typ, $wynikPrzetwarzania = null, $post = array()) {
	$loginForm = $this->getLoginForm();
	$uzytkownik = new Uzytkownik();
	$loginForm->bind($uzytkownik);

	$rejestracjaForm = new RejestracjaForm();

	$msg = $this->params()->fromPost('msg', '');


	return new ViewModel([
		'login_form' => $loginForm,
		'rejestracja_form' => $rejestracjaForm,
		'typ' => $typ,
		'wynik_przetwarzania' => $wynikPrzetwarzania,
		'msg' => $msg,
		'siteKey' => $this->reCaptchaService->getSiteKey(),
		]
	);
    }

    public function poRejestracjiAction() {
	$layout = $this->layout();
	$layout->setTemplate('logowanie/layout/logowanie_layout');
    }

    public function resetHaslaAction() {
	$layout = $this->layout();
	$layout->setTemplate('logowanie/layout/logowanie_layout');


	$post = $this->getPost();
	if ($post) {
	    $wynik = $this->przypominanieHaslaMenager->wyslijPotwierdzeniePrzypominaniaHasla($post);

	    return new ViewModel(array('wynik' => $wynik));
	} else {
	    return new ViewModel(
		    array('wynik' => new WynikPrzetwarzaniaFormularza(false, false)));
	}
    }

    public function logoutAction() {
	$this->uzytkownikTable->wyloguj();
	return $this->redirect()->toRoute('home');
    }

    public function aktywujAction() {
	$layout = $this->layout();
	$layout->setTemplate('logowanie/layout/logowanie_layout');
	$get = $this->getGet();

	if ($get) {
	    $wynik = $this->logowanieFasada->aktywujKonto($get);
	    return new ViewModel(array('wynik' => $wynik));
	} else {
	    return new ViewModel(
		    array('wynik' => new WynikPrzetwarzaniaFormularza(false, "Brak kodu aktywacji")));
	}
    }

    public function resetujAction() {
	$layout = $this->layout();
	$layout->setTemplate('logowanie/layout/logowanie_layout');

	$post = $this->getPost();
	$get = $this->getGet();

	if ($post) {
	    $wynik = $this->przypominanieHaslaMenager->zmienHasloZPrzypomnienia($post);
	    return new ViewModel(array('wynik' => $wynik, 'is_post' => true));
	}

	if ($get && count($get) > 0) {
	    $wynik = $this->przypominanieHaslaMenager->sprzawdzZadaniePrzypomnieniaHasla($get);
	    $id = $get['id'];
	    return new ViewModel(array('wynik' => $wynik, 'is_post' => false, 'id' => $id));
	} else {
	    return new ViewModel(
		    array('wynik' => new WynikPrzetwarzaniaFormularza(false, false),
		    'is_post' => false));
	}
    }

    public function nowaRolaAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$nazwa = $this->params()->fromPost('nazwa');

	$this->roleMenager->dodajRole($nazwa);

	return $this->redirect()->toRoute('logowanie', array('action' => 'role'));
    }

    public function dodajFirmeUzytkownikaAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);
	$id = 0;
	$post = $this->getPost();

	if ($post) {
	    if (!empty($post->id)) {
		$uzytkownikId = $id = $post->id;
	    } else {
		$uzytkownikId = $this->uzytkownikTable->getZalogowanegoUsera()->id;
	    }
	    if (!empty($post->nazwa_firmy)) {
		$firma = $this->firmyFasada->pobierzLubUtworzFirme($post->nazwa_firmy);
		$this->firmyFasada->polaczUzytkownikaZFirma($uzytkownikId, $firma->id);
	    } elseif (!empty($post->firma_id)) {
		$this->firmyFasada->polaczUzytkownikaZFirma($uzytkownikId, $post->firma_id);
	    }
	}

	return $this->redirect()->toRoute('logowanie', array('action' => 'profil', 'id' => $id));
    }

    public static function getLoginForm() {
	$form = new LogowanieForm();
	return $form;
    }

    public static function getRejestracjaForm() {
	$form = new RejestracjaForm();
	return $form;
    }

    public function profilUstawieniaAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);
	$request = $this->getRequest();

	if ($request->isPost()) {
	    $post = $request->getPost();
	    $this->wystawcyFakturyMenager->zapiszWystawceFaktur($post);
	}

	$zalogowany = $this->uzytkownikTable->getZalogowanegoUsera();
	$wystawca = $this->wystawcyFakturyMenager->pobierzUtworzWystawce($zalogowany);

	return new ViewModel(array(
		'wystawca' => $wystawca
	));
    }

    public function regulaminAction() {
	return new ViewModel();
    }

    public function regulaminModalAction() {
	$this->ustawModalLayout();
	return new ViewModel();
    }

    public function politykaPrywatnosciAction() {
	return new ViewModel();
    }

    public function politykaPrywatnosciModalAction() {
	$this->ustawModalLayout();
	return new ViewModel();
    }

    public function wyswietlOknoLogowaniaAction() {
	$this->ustawModalLayout();

	$loginForm = $this->getLoginForm();
	$uzytkownik = new Uzytkownik();
	$loginForm->bind($uzytkownik);

	return new ViewModel([
		'login_form' => $loginForm,
		'siteKey' => $this->reCaptchaService->getSiteKey(),
		]
	);
    }

    public function wyswietlOknoRejestracjiAction() {
	$this->ustawModalLayout();

	$rejestracjaForm = new RejestracjaForm();

	return new ViewModel([
		'rejestracja_form' => $rejestracjaForm,
		'siteKey' => $this->reCaptchaService->getSiteKey(),
		]
	);
    }

    protected function domyslnePrzekierowanieZalogowanego() {
	return $this->redirect()->toRoute('decyzje');
    }

}
