<?php

namespace Logowanie\Controller\Factory;

use Logowanie\Controller\LogowanieController;

class AjaxFactory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new \Logowanie\Controller\LogowanieAjaxController();
    }

}
