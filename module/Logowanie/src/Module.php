<?php

namespace Logowanie;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Logowanie\Model\Uzytkownik;
use Logowanie\Entity\Uprawnienie;
use Logowanie\Entity\UprawnienieKategoria;
use Logowanie\Entity\Rola;
use Logowanie\Entity\UzytkownikProjekt;
use Logowanie\Menager\UzytkownicyMenager;
use Logowanie\Menager\UprawnieniaMenager;
use Logowanie\Menager\UprawnieniaKategorieMenager;
use Logowanie\Menager\UprawnieniaRoleMenager;
use Logowanie\Menager\RoleMenager;
use Logowanie\Menager\UzytkownicyProjektyMenager;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module implements ConfigProviderInterface {

    const UZYTKOWNIK_TABLE = 'Logowanie\Menager\UzytkownicyMenager';
    const UZYTKOWNIK_TABLE_GATEWAY = 'UzytkownicyMenagerGateway';
    const UPRAWNIENIA_GATEWAY = "UprawnieniaGateway";
    const UPRAWNIENIA_KATEGORIE_GATEWAY = "UprawnieniaKategorieGateway";
    const ROLE_GATEWAY = "RoleGateway";
    const UPRAWNIENIA_ROLE_GATEWAY = "UprawnieniaRoleGateway";
    const UZYTKOWNICY_PROJEKTY_GATEWAY = "UzytkownicyProjektyGateway";
    const UZYTKOWNICY_KONTA_MAILOWE_GATEWAY = "UzytkownicyKontaMailoweGateway";

    public function getConfig() {
	return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig() {
	return array(
		'factories' => array(
			Fasada\LogowanieFasada::class => function($sm) {
			    $table = new Fasada\LogowanieFasada($sm);
			    return $table;
			},
			UzytkownicyMenager::class => function($sm) {
			    $table = new UzytkownicyMenager($sm);
			    return $table;
			},
			Menager\UprawnieniaMenager::class => function($sm) {
			    $table = new UprawnieniaMenager($sm);
			    return $table;
			},
			Menager\UprawnieniaKategorieMenager::class => function($sm) {
			    $table = new UprawnieniaKategorieMenager($sm);
			    return $table;
			},
			Menager\RoleMenager::class => function($sm) {
			    $table = new RoleMenager($sm);
			    return $table;
			},
			Menager\UprawnieniaRoleMenager::class => function($sm) {
			    $table = new UprawnieniaRoleMenager($sm);
			    return $table;
			},
			Menager\UzytkownicyProjektyMenager::class => function($sm) {
			    $table = new UzytkownicyProjektyMenager($sm);
			    return $table;
			},
			Menager\RejestracjaMenager::class => function($sm) {
			    $table = new Menager\RejestracjaMenager($sm);
			    return $table;
			},
			Model\AktywacjaMail::class => function($sm) {
			    $table = new Model\AktywacjaMail($sm);
			    return $table;
			},
			Model\InfoDoAdminaMail::class => function($sm) {
			    $table = new Model\InfoDoAdminaMail($sm);
			    return $table;
			},
			Model\ResetHaslaZapytanieMail::class => function($sm) {
			    $table = new Model\ResetHaslaZapytanieMail($sm);
			    return $table;
			},
			Model\KontaktEmail::class => function($sm) {
			    $table = new Model\KontaktEmail($sm);
			    return $table;
			},
			Menager\AktywacjaMenager::class => function($sm) {
			    $table = new Menager\AktywacjaMenager($sm);
			    return $table;
			},
			Menager\PrzypominanieHaslaMenager::class => function($sm) {
			    $table = new Menager\PrzypominanieHaslaMenager($sm);
			    return $table;
			},
			Menager\UzytkownicyKontaMailoweMenager::class => function($sm) {
			    $table = new Menager\UzytkownicyKontaMailoweMenager($sm);
			    return $table;
			},
			self::UZYTKOWNIK_TABLE_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Uzytkownik($sm));
			    return new TableGateway('uzytkownicy', $dbAdapter, null, $resultSetPrototype);
			},
			self::UPRAWNIENIA_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Uprawnienie($sm));
			    return new TableGateway('uprawnienia', $dbAdapter, null, $resultSetPrototype);
			},
			self::UPRAWNIENIA_KATEGORIE_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new UprawnienieKategoria($sm));
			    return new TableGateway('uprawnienia_kategorie', $dbAdapter, null, $resultSetPrototype);
			},
			self::ROLE_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Rola($sm));
			    return new TableGateway('role', $dbAdapter, null, $resultSetPrototype);
			},
			self::UPRAWNIENIA_ROLE_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Rola($sm));
			    return new TableGateway('uprawnienia_role', $dbAdapter, null, $resultSetPrototype);
			},
			self::UZYTKOWNICY_PROJEKTY_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new UzytkownikProjekt($sm));
			    return new TableGateway('uzytkownicy_projekty', $dbAdapter, null, $resultSetPrototype);
			},
			self::UZYTKOWNICY_KONTA_MAILOWE_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Entity\UzytkownikKontoMailowe($sm));
			    return new TableGateway('uzytkownicy_konta_mailowe', $dbAdapter, null, $resultSetPrototype);
			},
		),
	);
    }

    public static function dowiazMenagery(\Wspolne\Controller\Factory\InitObiekt $initObiekt,
	    \Interop\Container\ContainerInterface $container) {

	$initObiekt->logowanieFasada = $container->get(\Logowanie\Fasada\LogowanieFasada::class);
	$initObiekt->uzytkownikTable = $container->get(\Logowanie\Module::UZYTKOWNIK_TABLE);
	$initObiekt->roleMenager = $container->get(\Logowanie\Menager\RoleMenager::class);
	$initObiekt->uprawnieniaKategorieMenager = $container->get(\Logowanie\Menager\UprawnieniaKategorieMenager::class);
	$initObiekt->uprawnieniaRoleMenager = $container->get(\Logowanie\Menager\UprawnieniaRoleMenager::class);
	$initObiekt->uprawnieniaMenager = $container->get(\Logowanie\Menager\UprawnieniaMenager::class);
	$initObiekt->przypominanieHaslaMenager = $container->get(\Logowanie\Menager\PrzypominanieHaslaMenager::class);


    }

}
