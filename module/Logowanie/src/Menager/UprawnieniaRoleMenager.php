<?php

namespace Logowanie\Menager;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;
use Logowanie\Entity\UprawnienieRola;

class UprawnieniaRoleMenager extends BazowyMenagerBazodanowy {

    protected $wyszukiwanieZKontem = false;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm,\Logowanie\Module::UPRAWNIENIA_ROLE_GATEWAY);
    }

    public function uaktualnijUprawnienia($roleId, $uprawnienia) {
	$this->usun("rola_id=$roleId");

	foreach ($uprawnienia as $uprawnienie) {
	    $uprawnienieRola = new \Logowanie\Entity\UprawnienieRola();
	    $uprawnienieRola->rola_id = $roleId;
	    $uprawnienieRola->uprawnienie_kod = $uprawnienie;
	    ;
	    $this->zapisz($uprawnienieRola);
	}
    }

}
