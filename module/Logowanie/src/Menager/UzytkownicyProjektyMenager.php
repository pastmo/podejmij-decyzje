<?php

namespace Logowanie\Menager;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;

class UzytkownicyProjektyMenager extends \Wspolne\Model\WspolneEventTable {

    public function __construct(ServiceManager $sm) {
	$this->sm = $sm;
	$this->tableGateway = $sm->get(\Logowanie\Module::UZYTKOWNICY_PROJEKTY_GATEWAY);
    }

    //TODO: Uwzględnić przypadek, że uzytkownicy_projekty nie jest uzupelnione, a pracownik jest ustawiony przez sprzedawca_id
    public function pobierzUzytkownicyProjektyWgStatusu($uzytkownikId, $status) {
	$wynik = $this->pobierzZWherem("uzytkownik_id=$uzytkownikId AND projekt_id IN(select id from  projekty where status='$status')");
	return $wynik;
    }

    public function pobierzUzytkownicyProjekty($uzytkownikId, $projektId) {
	$wynik = $this->pobierzZWherem("uzytkownik_id=$uzytkownikId AND projekt_id=$projektId");
	return $wynik;
    }

    public function pobierzUzytkownicyDlaProjektu($projektId) {
	$wynik = $this->pobierzZWherem("projekt_id=$projektId");
	return $wynik;
    }

    public function usunPowiazanieUzytkownikaZProjektem($uzytkownikId, $projektId) {
	$this->usun("uzytkownik_id=$uzytkownikId AND projekt_id=$projektId");
    }

    public function dodajDoUzytkownikProjekt($uzytkownik, $projekt) {
	$uzytkownikId = \Pastmo\Wspolne\Utils\EntityUtil::wydobadzId($uzytkownik);
	$projektId = \Pastmo\Wspolne\Utils\EntityUtil::wydobadzId($projekt);

	$poprzedniePolaczenia = $this->pobierzUzytkownicyProjekty($uzytkownikId, $projektId);

	if (count($poprzedniePolaczenia) === 0) {
	    $uzytkownikProjekt = new \Logowanie\Entity\UzytkownikProjekt();
	    $uzytkownikProjekt->uzytkownik = $uzytkownikId;
	    $uzytkownikProjekt->projekt = $projektId;

	    $this->zapisz($uzytkownikProjekt);
	}
    }

}
