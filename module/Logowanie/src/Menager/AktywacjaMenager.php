<?php

namespace Logowanie\Menager;

use Zend\ServiceManager\ServiceManager;
use Logowanie\Polecenie\WynikPrzetwarzaniaFormularza;

class AktywacjaMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    private $uzytkownicyTable;
    private $uzytkownik;
    private $kod;
    private $id;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Logowanie\Module::UZYTKOWNIK_TABLE);
	$this->uzytkownicyTable = $this->get(\Logowanie\Menager\UzytkownicyMenager::class);
    }

    public function aktywujKonto($get) {
	try {
	    return $this->aktywujKontoTry($get);
	} catch (\Exception $e) {
	    return new WynikPrzetwarzaniaFormularza(FALSE, $e->getMessage());
	}
    }

    private function aktywujKontoTry($get) {

	$this->ustawParametry($get);
	$this->sprawdzKod($this->uzytkownik, $this->kod);
	$this->aktywujUzytkownika();

	return new WynikPrzetwarzaniaFormularza(TRUE, $this->translate("Aktywowano pomyślnie"));
    }

    private function ustawParametry($get) {
	$this->id = $get['id'];
	$this->kod = $get['kod'];
	try {
	    $this->uzytkownik = $this->uzytkownicyTable->getRekord($this->id);
	} catch (\Exception $e) {
	    throw new \Wspolne\Exception\GgerpException($this->translate('Błędny kod aktywacji lub brak użytkownika'));
	}
    }

    public function sprawdzKod($uzytkownik, $kod) {
	$poprawnyKod = RejestracjaMenager::generujKod($uzytkownik);

	if ($kod !== $poprawnyKod) {
	    throw new \Wspolne\Exception\GgerpException($this->translate("Błędny kod aktywacji"));
	}
    }

    private function aktywujUzytkownika() {
	$this->uzytkownik->czy_aktywny = true;
	$this->uzytkownicyTable->zapisz($this->uzytkownik);
    }

}
