<?php

namespace Logowanie\Menager;

use Zend\ServiceManager\ServiceManager;
use Zend\Session\Container;
use Pastmo\Wspolne\Utils\SearchPole;
use Pastmo\Wspolne\Utils\EntityUtil;

class UzytkownicyMenager extends \Pastmo\Uzytkownicy\Menager\UzytkownicyMenager {

    protected $wyszukiwanieZKontem = false;

    public static function getClass() {
	return __CLASS__;
    }

    public function zapisz(\Pastmo\Wspolne\Entity\WspolnyModel $model) {

	if (!\Pastmo\Wspolne\Utils\EntityUtil::wydobadzId($model->rola)) {
	    $model->rola = $this->pobierzJednaRolePoKodzie(\Logowanie\Entity\KodyRol::Klient);
	}
	parent::zapisz($model);
    }

    public function getKontoIdZalogowanego() {
	$zalogowany = $this->getZalogowanegoUsera();
	$konto = \Pastmo\Wspolne\Utils\EntityUtil::wydobadzPole($zalogowany, 'konto');
	$kontoId = EntityUtil::wydobadzId($konto);

	return $kontoId;
    }

}
