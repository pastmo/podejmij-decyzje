<?php

namespace Logowanie\Menager;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;

class RoleMenager extends BazowyMenagerBazodanowy {

    protected $wyszukiwanieZKontem = false;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Logowanie\Module::ROLE_GATEWAY);
    }

    public function pobierzRolePoNazwie($nazwa) {
	$wynik = $this->pobierzZWherem("nazwa='$nazwa'");

	if (count($wynik) > 0) {
	    return $wynik[0];
	}
	return new \Logowanie\Entity\Rola();
    }

    public function pobierzRolePoKodzie($kod) {
	$wynik = $this->pobierzZWherem("kod='$kod'");

	return $wynik;
    }

    public function pobierzJednaRolePoKodzie($kod) {
	$wynik = $this->pobierzZWherem("kod='$kod'");

	if (count($wynik) > 0) {
	    return $wynik[0];
	}
	return new \Logowanie\Entity\Rola();
    }

    public function dodajRole($nazwa) {

	$rola = new \Logowanie\Entity\Rola();
	$rola->nazwa = $nazwa;
	$this->zapisz($rola);
    }

}
