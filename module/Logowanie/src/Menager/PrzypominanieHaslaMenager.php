<?php

namespace Logowanie\Menager;

use Zend\ServiceManager\ServiceManager;
use Logowanie\Polecenie\WynikPrzetwarzaniaFormularza;

class PrzypominanieHaslaMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    private $uzytkownicyTable;
    private $uzytkownik;
    private $kod;
    private $id;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Logowanie\Module::UZYTKOWNIK_TABLE);
	$this->uzytkownicyTable = $this->get(\Logowanie\Menager\UzytkownicyMenager::class);
    }

    public function wyslijPotwierdzeniePrzypominaniaHasla($get) {
	try {
	    return $this->wyslijPotwierdzeniePrzypominaniaHaslaTry($get);
	} catch (\Exception $e) {
	    return new WynikPrzetwarzaniaFormularza(false, $e->getMessage());
	}
    }

    public function sprzawdzZadaniePrzypomnieniaHasla($get) {

	try {
	    return $this->sprzawdzZadaniePrzypomnieniaHaslaTry($get);
	} catch (\Exception $e) {
	    return new WynikPrzetwarzaniaFormularza(FALSE, $e->getMessage());
	}
    }

    public function zmienHasloZPrzypomnienia($post) {
	try {
	    return $this->zmienHasloZPrzypomnieniaTry($post);
	} catch (\Exception $e) {
	    return new WynikPrzetwarzaniaFormularza(FALSE, $e->getMessage());
	}
    }

    private function wyslijPotwierdzeniePrzypominaniaHaslaTry($get) {
	$email = $get['email'];
	$this->pobierzUzytkownika($email);
	$this->sprawdzUzytkownika();
	$this->wyslijMaila();
	return new WynikPrzetwarzaniaFormularza(TRUE, $this->translate("Wysłano maila z potwierdzeniem zmiany hasła"));
    }

    private function sprzawdzZadaniePrzypomnieniaHaslaTry($get) {

	$this->ustawParametry($get);
	(new AktywacjaMenager($this->sm))->sprawdzKod($this->uzytkownik, $this->kod);
	return new WynikPrzetwarzaniaFormularza(true, $this->translate("Kod jest zgodny"));
    }

    public function zmienHasloZPrzypomnieniaTry($post) {
	$id = $post->id;
	$nowe_haslo = $post->nowe_haslo;
	$nowe_haslo2 = $post->nowe_haslo2;

	$uzytkownikTable = $this->get(\Logowanie\Menager\UzytkownicyMenager::class);
	$uzytkownik = $uzytkownikTable->getRekord($id);

	$uzytkownik->zmienHasloBezSprawdzaniaStarego($nowe_haslo, $nowe_haslo2);

	$uzytkownikTable->zapisz($uzytkownik);

	return new WynikPrzetwarzaniaFormularza(true, $this->translate("Hasło zostało zmienione"));
    }

    private function pobierzUzytkownika($email) {
	$this->uzytkownik = $this->get(\Logowanie\Menager\UzytkownicyMenager::class)->pobierzUzytkownikaPoMailu($email);
    }

    private function sprawdzUzytkownika() {
	if (!$this->uzytkownik->id) {
	    throw new \Wspolne\Exception\GgerpException($this->translate("Brak użytkownika o podanym adresie e-mail."));
	}
    }

    private function ustawParametry($get) {
	$this->id = $get['id'];
	$this->kod = $get['kod'];
	$this->uzytkownik = $this->uzytkownicyTable->getRekord($this->id);
    }

    private function wyslijMaila() {
	$resetMail = $this->get(\Logowanie\Model\ResetHaslaZapytanieMail::class);
	$resetMail->ustawParametry($this->uzytkownik->mail, $this->uzytkownik->id,
		RejestracjaMenager::generujKod($this->uzytkownik));
	$resetMail->send();
    }

}
