<?php

namespace Logowanie\Menager;

use Zend\ServiceManager\ServiceManager;
use Logowanie\Polecenie\WynikPrzetwarzaniaFormularza;
use \Logowanie\Model\Uzytkownik;

class RejestracjaMenager extends \Pastmo\Uzytkownicy\Menager\RejestracjaMenager {

    protected $wyszukiwanieZKontem = false;

    protected function akcjePoRejestracji() {
	parent::akcjePoRejestracji();

	$this->aktywujUzytkownika();
	$this->utworzKonto();
	$this->wyslijMailaSimple();
	$this->get(\Wspolne\Menager\SesjaMenager::class)->ustawIdUzytkownika($this->dodanyUzytkownik->id);
//	$cennik = $this->get(\Pastmo\Platnosci\Menager\CennikMenager::class)->pobierzDomyslnyCennik();
//	$url = $this->get(\Pastmo\Platnosci\Menager\PlatnosciMenager::class)->utworzPlatnoscDlaUzytkownika($this->dodanyUzytkownik,
//		$cennik);
	$this->wynik->setUrlPrzekierowania('/');
    }

    private function aktywujUzytkownika() {
	$this->dodanyUzytkownik->czy_aktywny = 1;
	$this->zapisz($this->dodanyUzytkownik);
    }

    private function wyslijMailaDoAdmina() {
	$wiadomoscHtml = $this->get(\Email\Fasada\EmailFasada::class)->utworzTrescZSzablonem("email/layout/tresc_maila",
		['tytul' => "Droga Pani Prezes!", 'komunikaty' => ["W sumie nie wiadomo co tu jeszcze pisać",
			"id użytownika: {$this->dodanyUzytkownik->id}", "jego mail: {$this->dodanyUzytkownik->mail}"]]);

	$email = \Pastmo\Email\Entity\EmailDoWysylki::create();
	$email->trescHtml = $wiadomoscHtml;

	$email->odbiorcaEmail = 'kontakt@podejmijdecyzje.pl';
	$email->temat = "Rejestracja-PODEJMIJ DECYZJĘ " . $this->dodanyUzytkownik->mail;

	$uzytkownikKonot = \Logowanie\Entity\UzytkownikKontoMailowe::zrobDomyslneKontoNadawcze($this->sm);
	$email->uzytkownikKonto = $uzytkownikKonot;

	$this->get(\Email\Fasada\EmailFasada::class)->wyslijEmail($email);
    }
    
    private function wyslijMailaSimple() {
        
	$kontaktEmail = $this->get(\Logowanie\Model\KontaktEmail::class);
    $kontaktEmail->trescMaila= "dodany użytkownik{$this->dodanyUzytkownik->id} jego mail: {$this->dodanyUzytkownik->mail}";
	$kontaktEmail->ustawParametry('pr@pastmo.pl', '$this->uzytkownik->id',
        'abc');
	$kontaktEmail->send();
    }

    private function utworzKonto() {

	$this->dodanyUzytkownik;
	$konto = new \Uzytkownicy\Entity\Konto();
	$konto->rejestrujacy_id = $this->dodanyUzytkownik->id;

	$kontaMenager = $this->get(\Uzytkownicy\Menager\KontaMenager::class);
	$kontaMenager->zapisz($konto);
	$zapisaneKonto = $kontaMenager->getPoprzednioDodany();

	$this->dodanyUzytkownik->konto = $zapisaneKonto;
	$this->zapisz($this->dodanyUzytkownik);
    }

    public function konwertujPost($post) {
	if (isset($post['uzytkownik'])) {
	    $post['mail'] = $post['uzytkownik']['mail'];
	    $post['haslo'] = $post['uzytkownik']['haslo'];
	    $post['haslo2'] = $post['uzytkownik']['haslo2'];
	}
	return $post;
    }

    protected function wyslijMaila() {

	$email = \Pastmo\Email\Entity\EmailDoWysylki::create();
	$email->trescHtml = $this->pobierzTrescMaila();

	$email->odbiorcaEmail = $this->dodanyUzytkownik->mail;
	$email->temat = "Rejestracja-PODEJMIJ DECYZJĘ";

	$uzytkownikKonot = \Logowanie\Entity\UzytkownikKontoMailowe::zrobDomyslneKontoNadawcze($this->sm);
	$email->uzytkownikKonto = $uzytkownikKonot;

	$this->get(\Email\Fasada\EmailFasada::class)->wyslijEmail($email);
    }

    protected function pobierzTrescMaila() {
	$wiadomoscHtml = $this->get(\Email\Fasada\EmailFasada::class)->utworzTrescZSzablonem("email/layout/tresc_maila",
		['tytul' => "Drogi użytkowniku!", 'komunikaty' => ["Dziękujemy za rejestrację na stronie podejmijdecyzje.pl",
			"Twoje konto zostanie aktywowane po zaksięgowaniu płatności"]]);
	return $wiadomoscHtml;
    }

}
