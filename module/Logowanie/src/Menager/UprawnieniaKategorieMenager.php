<?php

namespace Logowanie\Menager;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;

class UprawnieniaKategorieMenager extends BazowyMenagerBazodanowy {

    protected $wyszukiwanieZKontem = false;

    public function __construct(ServiceManager $sm) {
	$this->sm = $sm;
	$this->tableGateway = $sm->get(\Logowanie\Module::UPRAWNIENIA_KATEGORIE_GATEWAY);
    }

}
