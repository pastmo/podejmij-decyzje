<?php

namespace Logowanie\Fasada;

class LogowanieFasada extends \Wspolne\Fasada\WspolneFasada {

    private $uzytkownikTable;
    private $rejestracjaMenager;
    private $aktywacjaMenager;
    private $uzytkownicyProjektyMenager;
    private $uzytkownicyKontaMailoweMenager;
    private $jezykiMenager;

    public function __construct(\Zend\ServiceManager\ServiceManager $serviceMenager) {
	parent::__construct($serviceMenager);

	$this->uzytkownikTable = $this->serviceMenager->get(\Logowanie\Menager\UzytkownicyMenager::class);
	$this->rejestracjaMenager = $this->serviceMenager->get(\Logowanie\Menager\RejestracjaMenager::class);
	$this->aktywacjaMenager = $this->serviceMenager->get(\Logowanie\Menager\AktywacjaMenager::class);
	$this->uzytkownicyProjektyMenager = $this->serviceMenager->get(\Logowanie\Menager\UzytkownicyProjektyMenager::class);
	$this->uzytkownicyKontaMailoweMenager = $this->serviceMenager->get(\Logowanie\Menager\UzytkownicyKontaMailoweMenager::class);
	$this->jezykiMenager = $this->serviceMenager->get(\Pastmo\Uzytkownicy\Menager\JezykiMenager::class);
    }

    public function getZalogowanegoUsera() {
	return $this->uzytkownikTable->getZalogowanegoUsera();
    }

    /**
     *
     * @param array-
     * zobacz RejestracjaMenagerTest::post
     * lub
     * RejestracjaMenagerTest::utworzSkroconyPost()
     * @return WynikPrzetwarzaniaFormularza
     */
    public function zarejestruj($post) {
	return $this->rejestracjaMenager->zarejestruj($post);
    }

    public function zarejestrujUzytkownika(\Logowanie\Model\Uzytkownik $uzytkownik) {

	return $this->rejestracjaMenager->zarejestrujUzytkownika($uzytkownik);
    }

    public function pobierzZarejestrujUzytkownikaPoMailu(\Logowanie\Model\Uzytkownik $uzytkownik) {

	return $this->rejestracjaMenager->pobierzZarejestrujUzytkownikaPoMailu($uzytkownik);
    }

    public function zarejestrujLubUaktualnij(\Logowanie\Model\Uzytkownik $uzytkownik) {

	return $this->uzytkownikTable->zarejestrujLubUaktualnij($uzytkownik);
    }

    /**
     *
     * @param array $get
     * @return WynikPrzetwarzaniaFormularza
     */
    public function aktywujKonto($get) {
	return $this->aktywacjaMenager->aktywujKonto($get);
    }

    public function pobierzPracownikow() {
	return $this->uzytkownikTable->pobierzPracownikow();
    }

    public function usunPowiazanieUzytkownikaZProjektem($uzytkownikId, $projektId) {
	return $this->uzytkownicyProjektyMenager->usunPowiazanieUzytkownikaZProjektem($uzytkownikId, $projektId);
    }

    public function aktualizujUzytkownika($post) {
	$this->uzytkownikTable->zapiszZPosta($post);
    }

    public function zarejestrujLubUaktualnijZBuildera(\Wspolne\Interfejs\UzytkownikBuilderInterfejs $builder) {

	return $this->uzytkownikTable->zarejestrujLubUaktualnijZBuildera($builder);
    }

    public function pobierzUzytkownikaPoMailu($mail) {
	return $this->uzytkownikTable->pobierzUzytkownikaPoMailu($mail);
    }

    public function sprawdzPoprawnoscKontMailowych() {
	return $this->uzytkownicyKontaMailoweMenager->sprawdzPoprawnoscKontMailowych();
    }

    public function zapiszKontaMailowe($post) {
	$this->uzytkownicyKontaMailoweMenager->zapiszKontaMailowe($post);
    }

    public function pobierzKontaMailoweDlaZalogowanegoUzytkownika() {
	return $this->uzytkownicyKontaMailoweMenager->pobierzDlaZalogowanegoUzytkownika();
    }

    public function getPoprzednioDodanyUzytkownik() {
	return $this->uzytkownikTable->getPoprzednioDodany();
    }

    public function pobierzUzytkownikowZUprawnieniami($kodUprawnienia,
	    $order = \Pastmo\Wspolne\Menager\WspolnyMenagerBazodanowy::DOMYSLNY_ORDER, $limit = false) {
	return $this->uzytkownikTable->pobierzUzytkownikowZUprawnieniami($kodUprawnienia, $order, $limit);
    }

    public function pobierzDostepneJezyki() {
	return $this->jezykiMenager->pobierzDostepneJezyki();
    }

    public function getKontoIdZalogowanego() {
	return $this->uzytkownikTable->getKontoIdZalogowanego();
    }

}
