<?php

namespace Logowanie\Entity;

class UzytkownikProjekt extends \Wspolne\Model\WspolneModel {

    public $id;
    public $projekt;
    public $uzytkownik;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	if ($this->czyTabeleDostepne()) {
	    $this->data = $data;
	    $this->projekt = $this->pobierzTabeleObca('projekt_id', \Projekty\Menager\ProjektyMenager::class,
		    new \Projekty\Entity\Projekt());
	    $this->uzytkownik = $this->pobierzTabeleObca('uzytkownik_id', \Logowanie\Menager\UzytkownicyMenager::class,
		    new \Logowanie\Model\Uzytkownik());
	}
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {

	switch ($nazwaWKodzie) {
	    case 'projekt':
		return 'projekt_id';
	    case 'uzytkownik':
		return 'uzytkownik_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
