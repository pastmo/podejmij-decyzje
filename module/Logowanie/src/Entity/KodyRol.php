<?php

namespace Logowanie\Entity;

class KodyRol {

    const ADMIN = 'admin';
    const Klient = 'klient';
    const Grafik = 'grafik';
    const Grafik_głowny = 'grafik_gl';
    const Sprzedawca = 'sprzedawca';
    const Pusta = 'empty';
    const DOMYSLNIE = self::Klient;

}
