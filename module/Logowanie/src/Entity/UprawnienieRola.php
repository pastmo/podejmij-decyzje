<?php

namespace Logowanie\Entity;

use Zasoby\Model\Zasob;
use Zend\Exception;

class UprawnienieRola extends \Wspolne\Model\WspolneModel {

    public $id;
    public $uprawnienie_kod;
    public $rola_id;

    public function __construct($sm = null) {
	parent::__construct($sm);
    }

    public function getUprawnienia() {
	$this->dowiazListyTabelObcych();
	return $this->uprawnienia;
    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public function dowiazListyTabelObcych() {
	if ($this->czyTabeleDostepne()) {
	    $uprawnieniaMenager = $this->sm->get(\Logowanie\Menager\UprawnieniaMenager::class);

	    if (!$this->uprawnienia) {
		$this->uprawnienia = $uprawnieniaMenager->pobierzUprawnieniaDlaRoli($this->id);
	    }
	}
    }

    public function sprawdzUprawnienia($kod) {
	$uprawnienia = $this->getUprawnienia();
	if (array_key_exists($kod, $uprawnienia)) {
	    return true;
	}
	return false;
    }

}
