<?php

namespace Logowanie\Model;

class KontaktEmail extends \Pastmo\Email\Menager\BazowyWysylaczMaili {

    protected $title = "Kontakt";
    protected $body = "Kontakt z podejmijdecyzje: ";
    protected $copyIfNotWork = " (jeśli nie zadziała, to skopiuj  go do przeglądarki)";
    protected $akcja = 'resetuj';
    
    public $trescMaila='';
    
    public function generujTresc()
    {
        
        return $this->trescMaila;
    }

}
