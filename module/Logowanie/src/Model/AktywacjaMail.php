<?php

namespace Logowanie\Model;

class AktywacjaMail extends \Pastmo\Email\Menager\BazowyWysylaczMaili {

    protected $title = "Potwierdzenie rejestracji";
    protected $body = "Witaj, Twoje konto zostało założone. Możesz je aktywować klikając poniższy link: ";
    protected $copyIfNotWork = " (jeśli nie zadziała, to skopiuj  go do przeglądarki)";
    protected $akcja = 'aktywuj';

}
