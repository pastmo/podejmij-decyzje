<?php

namespace Logowanie\Model;

class ResetHaslaZapytanieMail extends \Pastmo\Email\Menager\BazowyWysylaczMaili {

    protected $title = "Przypomnienie hasła";
    protected $body = "Witaj, Dla Towjego konta zostało wygenerowane żądanie zmiany hasła. Aby potwierdzić kliknij poniższy link: ";
    protected $copyIfNotWork = " (jeśli nie zadziała, to skopiuj  go do przeglądarki)";
    protected $akcja = 'resetuj';

}
