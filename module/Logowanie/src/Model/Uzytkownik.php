<?php

namespace Logowanie\Model;

use Zasoby\Model\Zasob;
use Logowanie\Entity\Rola;

class Uzytkownik extends \Pastmo\Uzytkownicy\Entity\Uzytkownik {

    public $konto;

    public function exchangeArray($data) {
	parent::exchangeArray($data);

	$this->konto = $this->pobierzTabeleObca('konto_id', \Uzytkownicy\Menager\KontaMenager::class, new \Uzytkownicy\Entity\Konto());
    }

    public static function create($sm = null) {
	$encja = new Uzytkownik();
	$encja->rola = new Rola();

	return $encja;
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	$nazwaWKodzie=parent::konwertujNaKolumneDB($nazwaWKodzie);
	switch ($nazwaWKodzie) {
	    case 'konto':
		return 'konto_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
