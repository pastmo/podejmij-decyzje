<?php

namespace Logowanie\Form;

use Zend\Captcha;
use Zend\Form\Element;
use Zend\Form\Fieldset;
use Zend\Form\Form;
use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;
use Logowanie\Form\UzytkownikFileset;

class RejestracjaForm extends Form {

    protected $captcha;

    public function __construct() {
	parent::__construct();

	$this
		->setAttribute('method', 'post')
		->setAttribute('id','register-form')
		->setHydrator(new ClassMethodsHydrator(false))
		->setInputFilter(new InputFilter())
		->setAttribute('class','przelaczenie_logowanie')
	;
	$this->add(array(
		'type' => 'Logowanie\Form\UzytkownikFileset',
		'options' => array(
			'use_as_base_fieldset' => true,
		),
	));
	$this->add(array(
		'name' => 'submit',
		'attributes' => array(
			'type' => 'submit',
			'value' => 'Zarejestruj i zapłać',
			'class' => 'btn btn-outline btn-xl  decyzja btn-register'
		),
	));
    }

}
