<?php

namespace Logowanie\Polecenie;

class WynikPrzetwarzaniaFormularza extends \Pastmo\Wspolne\Entity\DomyslnaOdpowiedzPrzetwarzania{

    public $isSuccess;
    public $message;

    public function __construct($wynik, $message) {
	$this->setSuccess($wynik);
	$this->addMsg($message);
    }

    public function setSuccess($success) {
	parent::setSuccess($success);
	$this->isSuccess=$success;
    }

    public function addMsg($msg) {
	parent::addMsg($msg);
	$this->message = $msg;
    }

}
