<?php

namespace Application\Controller;

class LadowaczSkryptow {

    const WERSJA = Wersja::WERSJA;
    const URL = "url";
    const FUNKCJA_JS = "funkcja_js";

    static $skrypty = array();
    static $parametrySkryptow = array();
    static $parametryCss = array();
    static $cssy = array();

    public static function add($skrypt, $funcjaJs = false) {

	if (!in_array($skrypt, self::$skrypty)) {
	    self::$skrypty[] = $skrypt;
	    self::$parametrySkryptow[$skrypt] = [self::FUNKCJA_JS => $funcjaJs];
	}
    }

    public static function addCss($css, $parametry = "") {

	if (!in_array($css, self::$cssy)) {
	    self::$cssy[] = $css;
	    self::$parametryCss[$css] = $parametry;
	}
    }

    public static function wyswietl() {
	foreach (self::$skrypty as $skrypt) {
	    echo '<script src="' . self::wyswietlUrlZWersja($skrypt) . '"></script>' . PHP_EOL;
	}
    }

    public static function pobierzJakoTabliceJson() {
	$wynik = [];

	foreach (self::$skrypty as $skrypt) {
	    $parametry = self::$parametrySkryptow[$skrypt];
	    $wynik[] = array_merge([self::URL => self::wyswietlUrlZWersja($skrypt)], $parametry);
	}
	return json_encode($wynik);
    }

    public static function wyswietlCssy() {
	foreach (self::$cssy as $css) {
	    echo '<link href="' . $css . '?wersja=' . self::WERSJA . '" rel="stylesheet" '.self::$parametryCss[$css].'>' . PHP_EOL;
	}
    }

    public static function wyswietlUrlZWersja($url) {
	return $url . '?wersja=' . LadowaczSkryptow::WERSJA;
    }

}
