<?php

namespace Application\Controller;

use Application\Controller\IndexController;

class Factory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

	protected function newController() {
		return new IndexController();
	}

}
