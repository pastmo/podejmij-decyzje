<?php

namespace Application;

class AktywneModulyZPastmo {

    public static function pobierzAktywneModuly() {
	return array(
		new \Pastmo\Wspolne\KonfiguracjaModulu(),
		new \Pastmo\Sesja\KonfiguracjaModulu(),
		new \Pastmo\Wiadomosci\KonfiguracjaModulu(),
		new \Pastmo\Email\KonfiguracjaModulu(),
		new \Pastmo\Uzytkownicy\KonfiguracjaModulu(),
		new \Pastmo\Pdf\KonfiguracjaModulu(),
		new \Pastmo\Platnosci\KonfiguracjaModulu(),
		new \Pastmo\Faktury\KonfiguracjaModulu(),
		new \Pastmo\Zasoby\KonfiguracjaModulu(),
		new \Pastmo\Logi\KonfiguracjaModulu(),
		new \Pastmo\Lokalizacje\KonfiguracjaModulu(),
		new \Pastmo\Wiki\KonfiguracjaModulu(),
	);
    }

    public static function dowiazMenagery(\Wspolne\Controller\Factory\InitObiekt $initObiekt,
	    \Interop\Container\ContainerInterface $container) {
	\Pastmo\Platnosci\KonfiguracjaModulu::dowiazMenagery($initObiekt, $container);
    }

    public static function dodajKontrolery($array) {
	$array = \Pastmo\Platnosci\KonfiguracjaModulu::dodajKontrolery($array);
	return $array;
    }

    const IGNOROWANE_NAMESPACE_TESTOW = [];

}
