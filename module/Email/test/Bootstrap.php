<?php

namespace EmailTest;

require_once dirname(__FILE__) . '/../../Wspolne/test/WspolneBootstrap.php';

chdir(__DIR__);

class Bootstrap extends \WspolneTest\WspolneBootstrap {

}

Bootstrap::init(array(
	'Application','Pastmo','Wspolne','Logowanie','Email','Konwersacje','Zasoby'
));
Bootstrap::chroot();
