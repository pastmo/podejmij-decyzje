<?php

namespace Email;

return array(
	'controllers' => array(
		'factories' => array(
			'Email\Controller\Email' => Controller\Factory\Factory::class,
			'Email\Controller\EmailAjax' => Controller\Factory\EmailAjaxControllerFactory::class,
		),
	),
	'router' => array(
		'routes' => array(
			'email' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/email[/:action][/:id][/:skrzynka]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
						'skrzynka' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Email\Controller\Email',
						'action' => 'index',
					),
				),
			),
			'email_ajax' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/email_ajax[/:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Email\Controller\EmailAjax',
						'action' => 'dodaj',
					),
				),
			),
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Email' => __DIR__ . '/../view',
		),
	),
);
