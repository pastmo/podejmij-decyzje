<?php
namespace Email\Helper;

class WyswietlPozycjeMenuRekorduEmail extends \Pastmo\Wspolne\Helper\TranslateHelper {

    protected $view;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	parent::__construct($sm);
    }

    public function __invoke($klasa, $tekst, $skrzynki, $aktualnaSkrzynka,
	    $view, $data_id = null, $folder_id = null) {
	$this->view = $view;

	if (in_array($aktualnaSkrzynka, $skrzynki)):
	    ?>
	    <li><a href="javascript:void(0)" class="<?php echo $klasa; ?>"
		<?php if ($data_id): ?>
		       data-id="<?php echo $data_id; ?>"
		   <?php endif; ?>
		   <?php if ($folder_id): ?>
		       data-folder_id="<?php echo $folder_id; ?>"
		   <?php endif; ?>
	           >
		       <?php echo $tekst; ?>
	        </a></li>
	    <?php
	endif;
    }

}
