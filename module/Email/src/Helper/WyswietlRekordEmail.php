<?php

namespace Email\Helper;

use Pastmo\Wiadomosci\Enumy\StatusyWiadomosci;
use Pastmo\Wiadomosci\Enumy\SkrzynkiWiadomosci;
use Pastmo\Wspolne\Utils\EntityUtil;

class WyswietlRekordEmail extends \Pastmo\Wspolne\Helper\TranslateHelper {

    protected $view;
    protected $skrzynka;
    protected $wybraneKonto;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	parent::__construct($sm);
    }

    public function __invoke($view, $email = null, $wybraneKonto = null, $skrzynka = null) {

	$this->view = $view;
	$this->skrzynka = $skrzynka ? $skrzynka : SkrzynkiWiadomosci::ODBIORCZA;
	$this->wybraneKonto = $wybraneKonto ? $wybraneKonto : \Pastmo\Email\Entity\KontoMailowe::create();
	$email = EntityUtil::czyEncjaZId($email) ? $email : \Email\Entity\Email::create();

	$emailUrl = $this->view->url('email', array('action' => 'szczegoly', 'id' => $email->id));
	?>
	    <tr class="rekord_email <?php echo $email->status === StatusyWiadomosci::NIEPRZECZYTANA ? "danger messages-unread-message" : ""; ?> messages-message"
		href="<?php echo $emailUrl; ?>"
		    <?php if(!$email):?>
		style="display:none" 
		    <?php endif;?>
		    >
		<td><input type="checkbox" data-id="<?php echo $email->id; ?>" class="select-message zaznaczanie" value="<?php echo $email->id; ?>"/> </td>
		<td class="clickableColumn nadawca"><?php echo $email->nadawca; ?>  </td>
		<td class="clickableColumn tytul">
		    <a href="<?php echo $emailUrl; ?>">
			<?php echo $email->tytul; ?>                                                                         </a>
		</td>
		<td class="clickableColumn data_naglowka"><span class="nowrap"><?php echo $email->data_naglowka; ?>  </span> </td>
		<td>
		    <div class="btn-group">

			<button type="button" class="btn btn-xs btn-primary dropdown-toggle menu" data-toggle="dropdown">
			    <span class="caret"></span>
			    <span class="sr-only">Toggle Dropdown</span>
			</button>
			<ul class="dropdown-menu pull-right mail-options" role="menu">
			    <?php
			    $this->view->wyswietlPozycjeMenuRekorduEmail('mark-as-not-spam', $this->translate('Przywróć'),
				    array(SkrzynkiWiadomosci::SPAM), $this->skrzynka, $email->id);
			    ?>
			    <?php
			    $this->view->wyswietlPozycjeMenuRekorduEmail('restore', $this->translate('Przywróć'),
				    array(SkrzynkiWiadomosci::KOSZ), $this->skrzynka, $email->id);
			    ?>
			    <li>
				<a href="javascript:undefined" class="add-project" data-id="<?php echo $email->id; ?>"><?php echo $this->translate("Nowy projekt"); ?></a>
			    </li>
			    <li>
				<a href="javascript:undefined" class="assign-project" data-id="<?php echo $email->id; ?>"><?php echo $this->translate("Przypisz projek"); ?>t</a>
			    </li>
			    <li><a href="javascript:undefined" class="assign-closed-project" data-id="<?php echo $email->id; ?>"><?php echo $this->translate("Przypisz zamknięty projekt"); ?></a></li>
			    <li><a href="javascript:void(0)" class="" data-id="<?php echo $email->id; ?>"></a></li>
			    <?php
			    $this->view->wyswietlPozycjeMenuRekorduEmail('mark-as-spam', $this->translate('Oznacz jako spam'),
				    array(SkrzynkiWiadomosci::ODBIORCZA), $this->skrzynka, $email->id);
			    ?>
			    <?php
			    foreach ($this->wybraneKonto->getFoldery() as $folder):
				$this->view->wyswietlPozycjeMenuRekorduEmail('change_folder', $this->translate("Do") . "  $folder",
					array(SkrzynkiWiadomosci::ODBIORCZA, SkrzynkiWiadomosci::SPAM, SkrzynkiWiadomosci::KOSZ),
					$this->skrzynka, $email->id, $folder->id);
			    endforeach;
			    ?>

																							    <li><a href="javascript:void(0)" class="move-to-box" data-id="<?php echo $email->id; ?>">Przenieś do</a></li>
			    <li><a href="javascript:undefined" class="forward-to-seller" data-id="<?php echo $email->id; ?>"><?php echo $this->translate("Przekaż sprzedawcy"); ?></a></li>
			    <li class="divider"></li>
			    <li><a href="javascript:undefined" class="delete" data-id="<?php echo $email->id; ?>"><?php echo $this->translate("Usuń"); ?></a></li>
			</ul>
		    </div>

		</td>
	    </tr>
	<?php
    }

}
