<?php
namespace Email\Enumy;


class IkonyFolderow {

    public static function wyswietl() {
	$ikony = [];

	$ikony[]=Ikona::create('Odbiorcza')
		->setMozliweNazwyFizyczne(['INBOX'])
		->setIkonaHtml('fa-inbox');
	$ikony[]=Ikona::create('Wysłane')
		->setMozliweNazwyFizyczne(['INBOX.Sent','[Gmail]/Wys&AUI-ane'])
		->setIkonaHtml('fa-envelope-o');
	$ikony[]=Ikona::create('Robocze')
		->setMozliweNazwyFizyczne(['INBOX.Drafts','[Gmail]/Wersje robocze'])
		->setIkonaHtml('fa-file-text-o');
	$ikony[]=Ikona::create('Kosz')
		->setMozliweNazwyFizyczne(['INBOX.Trash','[Gmail]/Kosz'])
		->setIkonaHtml('fa-trash-o');
	$ikony[]=Ikona::create('Ważne')
		->setMozliweNazwyFizyczne(['[Gmail]/Wa&AXw-ne'])
		->setIkonaHtml('fa-certificate');
	$ikony[]=Ikona::create('Spam')
		->setMozliweNazwyFizyczne(['INBOX.Spam','[Gmail]/Spam'])
		->setIkonaHtml('fa-ban');

	$wynik=[];
	foreach($ikony as $ikona){
	    foreach($ikona->mozliweNazwyFizyczne as $skrzynka){
		$wynik[$skrzynka]=$ikona->ikonaHtml;
	    }
	}
	return json_encode($wynik);
    }

}

class Ikona {

    public $nazwa;
    public $mozliweNazwyFizyczne;
    public $ikonaHtml;

    public static function create($nazwa) {
	$ikona = new Ikona();
	$ikona->nazwa = $nazwa;
	return $ikona;
    }

    public function setMozliweNazwyFizyczne($mozliweNazwyFizyczne) {
	$this->mozliweNazwyFizyczne = $mozliweNazwyFizyczne;
	return $this;
    }

    public function setIkonaHtml($ikonaHtml) {
	$this->ikonaHtml = $ikonaHtml;
	return $this;
    }

}
