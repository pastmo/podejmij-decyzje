<?php

namespace Email;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use \Email\Entity\Email;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface {

    use \Pastmo\FunkcjeKonfiguracyjne;

    const MAILE_GATEWAY = "MAILE_GATEWAY";

    public function getAutoloaderConfig() {
	return array(
		'Zend\Loader\ClassMapAutoloader' => array(
			__DIR__ . '/../autoload_classmap.php',
		),
		'Zend\Loader\StandardAutoloader' => array(
			'namespaces' => array(
				__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
			),
		),
	);
    }

    public function getConfig() {
	return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig() {
	return array(
		'factories' => array(
			Fasada\EmailFasada::class => function($sm) {

			    $table = new Fasada\EmailFasada($sm);
			    return $table;
			},
			Menager\EmaileMenager::class => function($sm) {

			    $table = new \Email\Menager\EmaileMenager($sm);
			    return $table;
			},
			Menager\EmaileFolderyMenager::class => function($sm) {

			    $table = new Menager\EmaileFolderyMenager($sm);
			    return $table;
			},
			Menager\KontaMailoweMenager::class => function($sm) {

			    $table = new Menager\KontaMailoweMenager($sm);
			    return $table;
			},
			self::MAILE_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Email($sm));
			    return new TableGateway('emaile', $dbAdapter, null, $resultSetPrototype);
			},
			\Pastmo\Module::MAILE_FOLDERY_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Entity\EmailFolder($sm));
			    return new TableGateway('emaile_foldery', $dbAdapter, null, $resultSetPrototype);
			},
		),
	);
    }

    public static function dowiazMenagery(\Wspolne\Controller\Factory\InitObiekt $initObiekt,
	    \Interop\Container\ContainerInterface $container) {

	$initObiekt->emailFasada = $container->get(\Email\Fasada\EmailFasada::class);
	$initObiekt->maileMenager = $container->get(\Email\Menager\EmaileMenager::class);
	$initObiekt->emailScaner = $container->get(\Pastmo\Email\Menager\EmailScaner::class);
	$initObiekt->kontaMailoweMenager = $container->get(Menager\KontaMailoweMenager::class);

    }

}
