<?php

namespace Email\Menager;

use Pastmo\Wiadomosci\Enumy\StatusyWiadomosci;
use Pastmo\Wiadomosci\Enumy\SkrzynkiWiadomosci;

class EmaileMenager extends \Pastmo\Email\Menager\EmaileMenager {

    protected function getGatewayConst() {
	return \Email\Module::MAILE_GATEWAY;
    }

    protected function whereDlaSkrzynki($skrzynka) {
	if ($this->czyFolder($skrzynka)) {
	    $odbiorcza = $this->skrzynkaDlaFolderu();
	    return " AND skrzynka='$odbiorcza' AND email_folder_id='$skrzynka'";
	} else {
	    return " AND skrzynka='$skrzynka' AND email_folder_id IS NULL";
	}
    }

    public function zmienSkrzynke($wiadomoscId, $skrzynka) {
	if ($this->czyFolder($skrzynka)) {
	    $wiadomosc = $this->getRekord($wiadomoscId);
	    $wiadomosc->skrzynka = $this->skrzynkaDlaFolderu();
	    $wiadomosc->email_folder = $skrzynka;
	    $this->zapisz($wiadomosc);
	} else {
	    parent::zmienSkrzynke($wiadomoscId, $skrzynka);
	}
    }

    public function pobierzSkrzynke($skrzynka) {
	if ($this->czyFolder($skrzynka)) {
	    return $this->skrzynkaDlaFolderu();
	}
	return $skrzynka;
    }

    private function czyFolder($skrzynka) {
	return is_numeric($skrzynka);
    }

    private function skrzynkaDlaFolderu() {
	return SkrzynkiWiadomosci::ODBIORCZA;
    }

}
