<?php

namespace Email\Menager;

class KontaMailoweMenager extends \Pastmo\Email\Menager\KontaMailoweMenager {

    public function pobierzMaileDoWyswietlenia($kontoId, $folder, $strona, $rekordowNaStrone) {

	try {
	    $kontoMailowe = $this->getRekord($kontoId);
	    $this->emailScaner = $kontoMailowe->pobierzEmailScanera();
	    $odpowiedzOdswiezania = $this->emailScaner->pobierMaileDoWyswietlenia($folder, $strona, $rekordowNaStrone);
	    $odpowiedzOdswiezania->setIdKonta($kontoId);

	} catch (\Exception $e) {
	    $odpowiedzOdswiezania = \Pastmo\Email\Entity\OdpowiedzNaglowkowMaili::create($this->sm)->addErrors($e->getMessage(),
		    NULL);
	}


	return $odpowiedzOdswiezania;
    }
    public function pobierzSzczegolyMaila($kontoId, $messageId, $folder) {

	try {
	    $kontoMailowe = $this->getRekord($kontoId);
	    $this->emailScaner = $kontoMailowe->pobierzEmailScanera();
	    $odpowiedzOdswiezania = $this->emailScaner->pobierzSzczegolyMaila($messageId, $folder);

	} catch (\Exception $e) {
	    $odpowiedzOdswiezania = new \Pastmo\Email\Entity\OdpowiedzSzczegoluMaila();
	    $odpowiedzOdswiezania->addError($e->getMsg());
	}


	return $odpowiedzOdswiezania;
    }

}
