<?php

namespace Email\Menager;

class EmaileFolderyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Module::MAILE_FOLDERY_GATEWAY);
    }

    public function pobierzDlaKontaMailowego($kontoMailoweId) {
	return $this->pobierzZWherem("konto_mailowe_id=$kontoMailoweId");
    }

    public function dodajFolder($idFolderu, $nazwa) {
	$emailFolder = new \Email\Entity\EmailFolder();
	$emailFolder->konto_mailowe = $idFolderu;
	$emailFolder->nazwa = $nazwa;

	$this->zapisz($emailFolder);
    }

}
