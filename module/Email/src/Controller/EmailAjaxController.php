<?php

namespace Email\Controller;

use Zend\View\Model\JsonModel;
use Pastmo\Wspolne\Adnotacje\DomyslnyJsonModel;
use Logowanie\Enumy\KodyUprawnien;

/**
 * @DomyslnyJsonModel
 */
class EmailAjaxController extends \Pastmo\Email\Controller\EmailAjaxController {

    public function watkiAction() {
	try {
	    $zalogowany = $this->uzytkownikTable->getZalogowanegoUsera();
	    if (!$zalogowany) {
		return new JsonModel(array('success' => false, 'message' => "Niezalogowany użytkownik"));
	    }

	    $konta = $zalogowany->getKontaMailowe();

	    if (count($konta) > 0) {
		$aktywneKonto = $konta[0];

		$emails = $this->maileMenager->pobierzWolneMaileDlaKonta($aktywneKonto->id);


		$watkiDoWyslania = array();

		foreach ($emails as $email) {

		    $watkiDoWyslania[] = array(
			    'watek' => $email,
			    'dodano' => $email->data_naglowka);
		}

		return $this->returnSuccess(array('watki_wiadomosci' => $watkiDoWyslania));
	    }
	    return $this->returnSuccess(array('watki_wiadomosci' => []));
	} catch (\Pastmo\Wspolne\Exception\PastmoException $e) {
	    return $this->returnFail(['msg' => $e->getMessage()]);
	}
    }

    public function wczytajStroneAction() {
	$this->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$kontoId = $this->params()->fromRoute('id');
	$folder = $this->params()->fromQuery('folder');
	$strona = $this->params()->fromQuery('strona', 0);
	$rekordowNaStrone = $this->params()->fromQuery('rekordow_na_stronie', 10);

	$wynik = $this->kontaMailoweMenager->pobierzMaileDoWyswietlenia($kontoId, $folder, $strona, $rekordowNaStrone);

	return $this->returnSuccess(['odpowiedz' => $wynik]);
    }

    public function wczytajMailaAction() {
	$this->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$kontoId = $this->params()->fromRoute('id');
	$folder = $this->params()->fromQuery('folder');
	$messageId = $this->params()->fromQuery('message_id');

	$wynik = $this->kontaMailoweMenager->pobierzSzczegolyMaila($kontoId, $messageId, $folder);

	return $this->returnSuccess(['odpowiedz' => $wynik]);
    }

}
