<?php

namespace Email\Controller;

use Zend\View\Model\ViewModel;

class EmailController extends \Pastmo\Email\Controller\EmailController {

    public function indexAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);
	$zalogowany = $this->logowanieFasada->getZalogowanegoUsera();

	$watekWIadomosci = $this->getRequest()->getQuery('id');

	$uzytkownicy_konta = $this->logowanieFasada->pobierzKontaMailoweDlaZalogowanegoUzytkownika();

	$indexView = new ViewModel(array(
		'aktualnyWatek' => $watekWIadomosci ? $watekWIadomosci : 0,
		'uzytkownik' => $zalogowany,
		'uzytkownicy_konta' => $uzytkownicy_konta
	));
	$indexView->setTemplate('email/email/email_szablon');
//		$this->uzytkownikWatkekWiadomosciTable->ustawPrzeczytwane();
	$view = new ViewModel();
	$view->addChild($indexView, 'wiadomosci');

	return $view;
    }

    public function odswiezAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$zalogowany = $this->uzytkownikTable->getZalogowanegoUsera();
	$id = (int) $this->params()->fromRoute('id', 0);

	$konta = $zalogowany->getKontaMailowe();
	foreach ($konta as $konto) {
	    if ($konto->id !== 0) {
		@$this->kontaMailoweMenager->odsiwezMaile($konto->id);
	    }
	}
	return $this->redirect()->toRoute('email', array('id' => $id, 'skrzynka' => 'inbox'));
    }

    public function testSzablonuWiadomosciAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::UPRAWNIENIA_ADMINISTRATORA,
		true);

	$this->ustawLayoutSzablonuEmail();
    }

}
