<?php

namespace Email\Controller\Factory;

use Email\Controller\EmailController;

class Factory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

	protected function newController() {
		return new EmailController();
	}

}
