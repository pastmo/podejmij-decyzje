<?php

namespace Email\Entity;

class Email extends \Pastmo\Email\Entity\Email {

    public $email_folder;

    public function exchangeArray($data) {
	parent::exchangeArray($data);

	$this->email_folder = $this->pobierzTabeleObca('email_folder_id',
		\Email\Menager\EmaileFolderyMenager::class
		, new EmailFolder());
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	$nazwa = parent::konwertujNaKolumneDB($nazwaWKodzie);
	switch ($nazwa) {
	    case 'email_folder':
		return 'email_folder_id';
	    default:
		return $nazwa;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public static function konwertujZMailSkanera($mailSkanera, $kontoMailowe, $sm) {
	$mail = new Email();

	$mail->data_naglowka = $mailSkanera->data;
	$mail->tytul = $mailSkanera->tytul;
	$mail->konto_mailowe = $kontoMailowe;
	$mail->nadawca = $mailSkanera->nadawca;
	$mail->message_id = $mailSkanera->messageId;


//	$mail->emailZasoby = array();
//	foreach ($mailSkanera->zasoby as $zasob) {
//
//	    $mail->dodajPotencjalnyZasob($zasob);
//	}

	return $mail;
    }

}
