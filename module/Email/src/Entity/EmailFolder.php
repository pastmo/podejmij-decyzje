<?php

namespace Email\Entity;

class EmailFolder extends \Wspolne\Model\WspolneModel {

    public $id;
    public $nazwa;
    public $filtr;
    public $konto_mailowe;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
	$this->filtr = $this->pobierzLubNull($data, 'filtr');
	$this->konto_mailowe = $this->pobierzTabeleObca('konto_mailowe_id',
		\Pastmo\Email\Menager\KontaMailoweMenager::class
		, new \Pastmo\Email\Entity\KontoMailowe());
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'konto_mailowe':
		return 'konto_mailowe_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function __toString() {
	if ($this->nazwa) {
	    return $this->nazwa;
	}
	return '';
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
