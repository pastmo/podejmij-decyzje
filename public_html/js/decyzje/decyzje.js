var Decyzje = {
    init: function () {
	this.initWalidacjeFormularza();
	this.initPrzyciskDecyzji();
	this.initZapisywanieDecyzji();
	this.initDodawanieArgumentow();
	this.initWyswietlanieKomunikatowPoZaladowaniu();
    },
    initWalidacjeFormularza: function () {
	Decyzje.getFormularz().validate({
	    errorPlacement: function (error, element) {
		if (element.hasClass("opis_decyzji")) {
		    var tekst = error.html();
		    element.attr("placeholder", tekst);
		}
	    }
	});
	$('[name*="opis"]').each(function () {
	    $(this).rules('add', {
		required: function (element) {
		    return Decyzje.czyElementWymagany(element);
		}
	    });
	});
	$('[name*="waga"]').each(function () {
	    $(this).rules('add', {
		required: function (element) {
		    return Decyzje.czyElementWymagany(element);
		}
	    });
	});
    },
    initPrzyciskDecyzji: function () {
	$(document).on('click', '.przycisk_liczenia_decyzji', Decyzje.obliczDecyzjeDlaPoprawnegoFormularza);
    },
    initZapisywanieDecyzji: function () {
	$(document).on('click', '.zalogowany_uzytkownik .zapisz_decyzje a', function (e) {
	    Decyzje.zapiszDecyzjeDlaZalogowanego(e, this);
	});
	$(document).on('click', '.niezalogowany_uzytkownik .zapisz_decyzje a', function (e) {
	    Decyzje.zapiszDecyzjeDlaNiezalogowanego(e);
	});
    },
    zapiszDecyzjeDlaZalogowanego: function (e, self) {
	e.preventDefault();
	var przycisk = $(self);

	var wynikWalidacji = Decyzje.getFormularz().valid();
	if (wynikWalidacji) {

	    if (Ekran.czyMniejszyEkran()) {
		var tresc = $('#szablon_podawania_nazwy').children().clone();

		tresc.find('.nazwa').val($('#nazwa').val());

		tresc.find('.potwierdzenie_zapisu').click(function () {
		    Decyzje.zapiszDecyzje(tresc.find('.nazwa').val());
		});

		przycisk.parent().parent().append(tresc);


	    } else {
		bootbox.prompt({
		    title: "Podaj nazwę decyzji",
		    inputType: 'text',
		    value: $('#nazwa').val(),
		    callback: function (nazwa) {
			Decyzje.zapiszDecyzje(nazwa);
		    }})
	    }
	}
    },
    zapiszDecyzjeDlaNiezalogowanego: function (e) {
	e.preventDefault();

	var wynikWalidacji = Decyzje.getFormularz().valid();
	if (wynikWalidacji) {

	    AktywacjaModali.wyswietlOknoLogowania();

	}
    },
    initDodawanieArgumentow: function () {
	$(document).on('click', '.dodaj_argumenty', function (e) {
	    e.preventDefault();
	    var klon = $($('#template_wiersz').html());
	    var przycisk = $(this);
	    var typ = przycisk.data('typ');
	    var klasa_wagi = przycisk.data('klasa_wagi');
	    var i = $('#' + typ).children().length;

	    klon.find('.opis_decyzji').attr('name', 'opis' + typ + i);
	    klon.find('.waga_decyzji').attr('name', 'waga' + typ + i)
		    .addClass(klasa_wagi).addClass('bez_touchspin');

	    $('#' + typ).append(klon);
	    Decyzje.initPlusyIMinusy();
	});
    },
    initWyswietlanieKomunikatowPoZaladowaniu: function () {
	var msg = $('#decyzje_kontener').data('msg');

	if (msg != "") {
	    bootbox.alert(msg);
	}
    },
    initPlusyIMinusy: function () {
	var wagi = $(".waga_decyzji.bez_touchspin");
	wagi.TouchSpin({
	    min: 1,
	    max: 10});
	wagi.removeClass('bez_touchspin');

    },
    zapiszDecyzje: function (nazwa) {
	var parametry = Decyzje.pobierzParametryPosta(nazwa);
	Pastmo.wyslijPost('/decyzje_ajax/zapisz_decyzje', parametry, Decyzje.obsluzOdpowiedzZapisu);
    },
    zapiszRoboczeDecyzje: function () {
	var parametry = Decyzje.pobierzParametryPosta(false);
	Pastmo.wyslijPost('/decyzje_ajax/zapisz_robocze_decyzje', parametry, function () {});
    },
    pobierzParametryPosta: function (nazwa) {
	if (nazwa) {
	    $('#formularz_decyzji #nazwa').val(nazwa);
	}
	var form = Decyzje.getFormularz().serialize();

	return form;
    },
    obsluzOdpowiedzZapisu: function (msg) {
	window.location.href='/decyzje/decyzje';
    },
    obliczDecyzjeDlaPoprawnegoFormularza: function () {
	var wynikWalidacji = Decyzje.getFormularz().valid();
	if (wynikWalidacji) {
	    Decyzje.obliczDedycje();
	    Decyzje.zapiszRoboczeDecyzje();
	}
    },
    obliczDedycje: function () {

	var tak = Decyzje.wyliczPlus();
	var nie = Decyzje.wyliczMinus();
	AplikacjaAdapter.pokazElementColl($('.po_pierwszym_wywolaniu'));
	$('.przed_obliczeniem').hide();
	if (tak > nie) {
	    Decyzje.obsluzWynikTak();
	} else if (tak < nie) {
	    Decyzje.obsluzWynikNie();
	} else {
	    Decyzje.obsluzWynikNierozstrzygniety();
	}

    },
    wyliczPlus: function () {
	return this.wyliczWage("#argumenty_plus");
    },
    wyliczMinus: function () {
	return this.wyliczWage("#argumenty_minus");
    },
    obsluzWynikTak: function () {
	$('.werdykt').hide();
	AplikacjaAdapter.pokazElementColl($('#dostepna_decyzja'));
	$('.decyzja-tak').show();
    },
    obsluzWynikNie: function () {
	$('.werdykt').hide();
	AplikacjaAdapter.pokazElementColl($('#dostepna_decyzja'));
	$('.decyzja-nie').show();
    },
    obsluzWynikNierozstrzygniety: function () {
	$('.werdykt').hide();
	$('#rownowaga').show();
    },
    wyliczWage: function (kontener) {

	var elementy = $(kontener + " .waga_decyzji");
	var wynik = 0;
	elementy.each(function () {
	    wynik += Pastmo.toNumber($(this).val());
	});
	return wynik;
    },
    czyElementWymagany: function (element) {
	var name = $(element).attr("name");
	if (name === "opis_p0" || name === "waga_p0") {
	    return Decyzje.czySasiedniePoleJestWypelnione() ||
		    !Decyzje.czyConajmniejJednaWartosc('.waga_plus');
	} else if (name === "opis_m0" || name === "waga_m0") {
	    return Decyzje.czySasiedniePoleJestWypelnione() ||
		    !Decyzje.czyConajmniejJednaWartosc('.waga_minus');
	} else {
	    return Decyzje.czySasiedniePoleJestWypelnione(element);
	}
    },
    czyConajmniejJednaWartosc: function (selektorWagi) {
	var wynik = false;
	$(selektorWagi).each(function () {
	    var wartosc = $(this).val();
	    var czyWypelniony = !Pastmo.czyPuste(wartosc);
	    if (czyWypelniony) {
		wynik = true;
		return false
	    }
	});
	return wynik;
    },
    czySasiedniePoleJestWypelnione: function (element) {
	var wartosc = $(element).parent().siblings().find('input').val();
	return !Pastmo.czyPuste(wartosc);
    },
    getFormularz: function () {
	return $('#formularz_decyzji');
    }
}

if (typeof wTrakcieTestow === 'undefined') {
    Decyzje.init();
    Decyzje.initPlusyIMinusy();
}