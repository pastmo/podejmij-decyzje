DecyzjeArchiwum = {
    init: function () {
	this.initSzczegoly();
    },
    initSzczegoly: function () {
	$('.tytul_decyzji .szczegoly').click(function () {
	    var aktualny = $(this);
	    var kontener = aktualny.closest('.tytul_decyzji');
	    var id = kontener.data('id');
	    var nastepny = kontener.next();

	    var i = aktualny.find('i');
	    i.toggleClass('fa-caret-down');
	    i.toggleClass('fa-caret-up');

	    if (nastepny.hasClass('ukryty')) {
		Pastmo.wyslijGet('decyzje/decyzje_szczegoly/' + id, {}, function (msg) {
		    nastepny.empty();
		    nastepny.html(msg);
		}, true);
	    }

	    nastepny.toggleClass('ukryty');
	});
    },
}


if (typeof wTrakcieTestow === 'undefined') {
    DecyzjeArchiwum.init();
}