/*autor: http://bootsnipp.com/snippets/featured/login-and-register-tabbed-form*/

function zmianaWidokuLogowania(e, runner, formSelektor) {
    $(".przelaczenie_logowanie").fadeOut(100);
    $(formSelektor).delay(100).fadeIn(100);
    $('.przelaczenie_logowanie_link').removeClass('active');
    $(runner).addClass('active');
    e.preventDefault();
}


$(function () {

    $('#login-form-link').click(function (e) {
	zmianaWidokuLogowania(e, this, "#login-form");
    });

    $('#to_loggin_back').click(function (e) {
	$('#login-form-link').click();
    });

    $('#register-form-link').click(function (e) {
	zmianaWidokuLogowania(e, this, "#register-form");
    });

    $('#reset_hasla-link').click(function (e) {
	zmianaWidokuLogowania(e, this, "#reset_hasla");
    });

});
