var OknoRejestracji = {
    init: function () {
	OknoRejestracji.wylaczDomyslnyFormularz();
	if (typeof WyswietlanieModali.parametry['OknoRejestracji'] !== 'undefined') {
	OknoRejestracji.initCaptcha();
	}
    },
    wylaczDomyslnyFormularz: function () {
	var formularz = $('#register-form');
	formularz.submit(function (e) {
	    e.preventDefault();
	});

	formularz.find('.btn-register').click(function (e) {
	    var czyPoprawny = formularz.valid();
	    if (czyPoprawny) {
		var idCaptcha = $('#rejestracja_submit').data('id_captcha');
		grecaptcha.reset(idCaptcha);
		grecaptcha.execute(idCaptcha);
	    }
	});

    },
    initCaptcha: function () {
	PodpinanieCaptcha.podepnij('#rejestracja_submit');
    }

}

wyslijRejestracje = function () {
    var formularz = $('#register-form');
    var dane = formularz.serialize();
    Pastmo.wyslijPost('/logowanie_ajax/zarejestruj', dane, function (e) {
	bootbox.hideAll();
    })

}

if (typeof wTrakcieTestow === 'undefined') {
    OknoRejestracji.init();
}