var OknoLogowania = {
    init: function () {
	OknoLogowania.wylaczDomyslnyFormularz();
	if (typeof WyswietlanieModali.parametry['OknoLogowania'] !== 'undefined') {
	    OknoLogowania.initCaptcha();
	}
    },
    wylaczDomyslnyFormularz: function () {
	var formularz = $('#login-form');
	formularz.submit(function (e) {
	    e.preventDefault();
	});

	formularz.find('.btn-login').click(function (e) {
	    var formularz = $('#login-form');

	    var dane = formularz.serialize();
	    Pastmo.wyslijPost('/logowanie_ajax/zaloguj', dane, function (msg) {
		bootbox.hideAll();
		$('html').addClass('zalogowany_uzytkownik')
			.removeClass('niezalogowany_uzytkownik');

		if(msg.oplaconeKonto){
		    $('html').add('oplacone_konto');
		}
	    });
	});
    },
    initCaptcha: function () {
	PodpinanieCaptcha.podepnij('#logowanie_submit');
    }
}


wyslijLogowanie = function () {
}

if (typeof wTrakcieTestow === 'undefined') {
    OknoLogowania.init();
}