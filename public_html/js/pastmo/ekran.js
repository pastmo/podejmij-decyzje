var Ekran = {
    domyslnaWielkoscEkranu: 1500,
    BREAK_POINT_KOMPUTER: 1200,
    BREAK_POINT_TABLET: 768,
    BREAK_POINT_SMART: 452,
    trybEkranu: null,
    TRYB_DUZY_EKRAN: 'TRYB_DUZY_EKRAN',
    TRYB_KOMPUTER: 'TRYB_KOMPUTER',
    TRYB_TABLET: 'TRYB_TABLET',
    TRYB_SMART: 'TRYB_SMART',
    init: function () {
	this.sprawdzIZapiszNowaWielkoscEkranu();
	this.initSprawdzaniePoZmianieRozdzielczosci();
	this.ustawTrybEkranu();

    },
    initSprawdzaniePoZmianieRozdzielczosci: function () {
	var wyzwalacz = new Wyzwalacz(this.sprawdzIZapiszNowaWielkoscEkranu);
	$(window).resize(function () {
	    wyzwalacz.wywolaj();
	    Ekran.ustawTrybEkranu();
	});
    },
    sprawdzIZapiszNowaWielkoscEkranu: function () {
	var zapisanaSzerokosc = Ekran.getZapisanaSzerokosc();
	var biezacaSzczerokosc = Ekran.getBiezacaSzerokosc();

	if (biezacaSzczerokosc !== zapisanaSzerokosc) {
	    Ekran.zapiszNowaWielkoscEkranu(biezacaSzczerokosc);
	}
    },
    zapiszNowaWielkoscEkranu: function (nowaWielkoscEkranu) {
//	Pastmo.wyslijPost('pastmo_wspolne_ajax/zapisz_wielkosc_ekranu', {szerokosc: nowaWielkoscEkranu}, function () {})
    },
    getBiezacaSzerokosc: function () {
	return $(document).width();
    },
    getZapisanaSzerokosc: function () {
	if (typeof szerokoscEkranu !== 'undefined') {
	    return szerokoscEkranu;
	} else {
	    return this.domyslnaWielkoscEkranu;
	}
    },
    ustawTrybEkranu: function () {
	var wielkosc = Ekran.getBiezacaSzerokosc();

	if (wielkosc < this.BREAK_POINT_SMART) {
	    this.trybEkranu = this.TRYB_SMART;
	} else if (wielkosc < this.BREAK_POINT_TABLET) {
	    this.trybEkranu = this.TRYB_TABLET;
	} else if (wielkosc < this.BREAK_POINT_KOMPUTER) {
	    this.trybEkranu = this.TRYB_KOMPUTER;
	} else {
	    this.trybEkranu = this.TRYB_DUZY_EKRAN;
	}
    },
    getTrybEkranu: function () {
	return this.trybEkranu;
    },
    czyMniejszyEkran: function () {
	return this.czyEkranSmart() || this.trybEkranu === this.TRYB_TABLET;
    },
    czyEkranSmart: function () {
	return this.trybEkranu === this.TRYB_SMART;
    }
}

if (typeof wTrakcieTestow === 'undefined') {
    Ekran.init();
}