var Filtrowanie = function (parametry) {
    this.selektorPrzyciskow = "#przyciski .filtr";
    this.selektorAktywnychPrzyciskow = "#przyciski .filtr.active,#filtry_kontener_buttonow .filtry_buttony.active";
    this.selektorCleanPrzyciskow = "#przyciski .filtry_clean";
    this.selektorUstawianieDatyFiltraDodatkowego = "#filtr_daty_ustawianie";
    this.selektorUstawianieTekstowegoFiltraDodatkowego = "#filtr_pola_tekstowego_ustawianie";
    this.selektorUsuwaniaWszystkichFiltrowDodatkowych = ".filtry_dodatkowe_usun";
    this.kontenerFiltrowDodatkowych = '#filtry_dodatkowe';


    this.init = function (parametry) {
	this.aktywujPrzelaczeniePrzyciskow();
	this.aktywujPrzyciskDaty();
	this.aktywujPrzyciskTekstowy();
	this.aktywujSamodzielneButtony();
	this.aktywujUsuwanieWszystkichFitrlowDodatkowych();
	this.getSelektorElementow = parametry.getSelektorElementow;
	this.pobierzObiektDoUkryciaZElementu = parametry.pobierzObiektDoUkryciaZElementu;
	this.odswiezWidok();
    };



    this.aktywujPrzelaczeniePrzyciskow = function () {

	var self = this;

	this.aktywujPrzelaczenieZSelektora(this.selektorPrzyciskow);

	$(this.selektorCleanPrzyciskow).click(function () {
	    self.wylaczWszystkieFitry();
	    self.odswiezWidok();
	});

    };

    this.wylaczWszystkieFitry = function () {
	$(this.selektorPrzyciskow).removeClass('active');
    }

    this.aktywujPrzelaczenieZSelektora = function (selektor) {
	var self = this;

	$(selektor).click(function () {
	    $(this).toggleClass('active');
	    self.odswiezWidok();
	});
    }

    this.aktywujUsuwanieDodatkowychFiltrow = function (filtr) {

	var self = this;

	$(filtr).find('.filtr_dodatkowy_usuwanie').click(function () {
	    $(this).parent().remove();
	    self.odswiezWidok();
	});

    };

    this.aktywujPrzyciskDaty = function () {
	var self = this;
	$(this.selektorUstawianieDatyFiltraDodatkowego).on('change dp.hide', function () {
	    var value = $(this).val();
	    $(this).val('');
	    self.obslugPrzyciskFiltrowDodatkowych(value);
	});

    };
    this.aktywujPrzyciskTekstowy = function () {
	var self = this;

	$(this.selektorUstawianieTekstowegoFiltraDodatkowego).keyup(function (e) {
	    if (e.keyCode == 13) {
		var value = $(this).val();
		$(this).val('');
		self.obslugPrzyciskFiltrowDodatkowych(value);
	    }
	});


    };
    this.aktywujSamodzielneButtony = function () {
	var self = this;

	$('#filtry_kontener_buttonow').off('.filtry_buttony');
	$('#filtry_kontener_buttonow').on('click', '.filtry_buttony', function (e) {

	    var staraWartosc = $(this).hasClass('active');
	    $('#filtry_kontener_buttonow .filtry_buttony').removeClass('active');
	    if (staraWartosc === false) {
		$(this).addClass('active');
	    }
	    self.odswiezWidok();
	});


    };
    this.aktywujUsuwanieWszystkichFitrlowDodatkowych = function () {
	var self = this;

	$(this.selektorUsuwaniaWszystkichFiltrowDodatkowych).click(function (e) {
	    $(self.kontenerFiltrowDodatkowych).empty();
	    self.odswiezWidok();
	});


    };

    this.obslugPrzyciskFiltrowDodatkowych = function (value, self) {

	this.wylaczWszystkieFitry();

	var nowy = $('#fitry_dodatkowe_cover .filtr_dodatkowy').clone();

	nowy.data("filtr", value);
	nowy.find('.filtr_dodatkowy_tresc').append(value);
	nowy.addClass('active');
	this.aktywujUsuwanieDodatkowychFiltrow(nowy);
	this.aktywujPrzelaczenieZSelektora(nowy);
	$(this.kontenerFiltrowDodatkowych).append(nowy);

	this.odswiezWidok();
    }

    this.getAktywneFiltry = function () {

	var aktywne = $(this.selektorAktywnychPrzyciskow).map(function () {
	    return $(this).data("filtr");
	}).get();

	return aktywne;
    };

    this.odswiezWidok = function () {
	var aktywneFlagi = this.getAktywneFiltry();
	var self = this;

	var wszystkieElementy = $(this.getSelektorElementow());

	wszystkieElementy.each(function () {

	    var element = $(this);
	    var flagi_elementu = element.data('flagi-filtrowania');
	    var czyWidoczny = self.sprawdzCzyElementSpelniaWszystkieFlagi(aktywneFlagi, flagi_elementu);

	    self.zmienWidocznoscElementu(element, czyWidoczny);

	});
    };

    this.sprawdzCzyElementSpelniaWszystkieFlagi = function (aktywneFlagi, flagi_elementu) {

	for (i = 0; i < aktywneFlagi.length; i++) {
	    var flaga = aktywneFlagi[i];
	    if (!Pastmo.czyTablicaZawieraFagment(flagi_elementu, flaga)) {
		return false;
	    }
	}
	return true;
    }

    this.zmienWidocznoscElementu = function (element, czyWidoczny) {
	if (czyWidoczny) {
	    this.pobierzObiektDoUkryciaZElementu(element).show();
	} else {
	    this.pobierzObiektDoUkryciaZElementu(element).hide();
	}
    }

    this.init(parametry);
};
