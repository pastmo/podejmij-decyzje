var BazowyOdswiezaczEmail = {
    timer: null,
    selektorGlowny: "#gadzet_email ",
    selektorIkonki: "#gadzet_email i",
    selektorIlosciMaili: "#gadzet_email .item-quantity",
    selektorZmianyOdswiezaniaMaili: "#zmiana_odswiezania_email select",
    selektorTytulowMaili:"#mail-list",
    klasaAktywna: "pobieranie_maili",
    init: function () {
	this.initTimer();
	this.initZmianeCzasuODswiezania();

    },
    initTimer: function () {
	OdswiezaczEmail.timer = new Timer(OdswiezaczEmail.wyslijZapytanieOdswiezaniaMaili);
	var interwal = OdswiezaczEmail.pobierzCzestotliwoscOdswiezania();
	OdswiezaczEmail.timer.interval = interwal;
	OdswiezaczEmail.timer.uruchomCykliczneOdswiezanie();
    },
    initZmianeCzasuODswiezania: function () {
	$(this.selektorZmianyOdswiezaniaMaili).change(function () {
	    var value = $(this).val();
	    Pastmo.wyslijPost('email_ajax/zmien_czestotliwosc_odswiezania', {wartosc: value}, function () {});
	    OdswiezaczEmail.timer.zmienInterwal(value);

	});
    },
    wyslijZapytanieOdswiezaniaMaili: function () {
	$(OdswiezaczEmail.selektorIkonki).addClass(OdswiezaczEmail.klasaAktywna);
	Pastmo.wyslijGet('email_ajax/odswiez_maile_ze_wszystkich_skrzynek', [], OdswiezaczEmail.obsluzNoweMaile);
    },
    obsluzNoweMaile: function (msg) {
	$(OdswiezaczEmail.selektorIkonki).removeClass(OdswiezaczEmail.klasaAktywna);
	OdswiezaczEmail.obsluzPowiadomienieWMenu(msg);
	OdswiezaczEmail.wyswietlNoweMaile(msg);
	OdswiezaczEmail.wykonajAkcjePoOdswiezeniu();
    },
    obsluzPowiadomienieWMenu: function (msg) {
	var ilePrzetworzonychMaili = this.getIleMailiWOdpowiedzi(msg);

	var wczesniejszaWartosc = parseInt($(this.selektorIlosciMaili).html());
	var iloscPobranychMaili = wczesniejszaWartosc + ilePrzetworzonychMaili;

	if (iloscPobranychMaili === 0) {
	    $(this.selektorIlosciMaili).hide();
	} else {
	    $(this.selektorIlosciMaili).show();
	}

	$(this.selektorIlosciMaili).html(iloscPobranychMaili);
    },
    wyswietlNoweMaile: function (msg) {
	var self = this;

	if ($(OdswiezaczEmail.getSelektorSzablonu()).length === 0) {
	    return;
	}

	msg.odpowiedz.forEach(function (konto) {
	    konto.przetworzoneMaile.forEach(function (element) {
		var szablon = $(self.getSelektorSzablonu()).clone();
		self.wypelnijSzablon(element, szablon);
		$(self.selektorTytulowMaili).prepend(szablon);
	    })
	});
    },
    getIleMailiWOdpowiedzi: function (msg) {
	var odpowiedz = msg.odpowiedz;
	var wynik = 0;

	odpowiedz.forEach(function (konto) {
	    wynik += konto.ilePobranychMaili;
	});

	return wynik;
    },
    pobierzCzestotliwoscOdswiezania: function () {
	return czestotliwoscOdswiezaniaMaili;
    },
    wypelnijSzablon: function (element, szablon) {},
    getSelektorSzablonu: function () {},
    wykonajAkcjePoOdswiezeniu: function () {},
};

PagesCommon.ustawDziedziczenie(BazowyOdswiezaczEmail);

