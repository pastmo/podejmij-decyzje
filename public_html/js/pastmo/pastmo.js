function dodajLoader(selector) {
$(selector).after('<div class="pastmo_loader loader"></div>');
}

function usunLoader() {
$('.pastmo_loader').remove();
}

function wyswietlBlad(msg) {
bootbox.alert(msg);
}

function wyswietlBledy(msgs) {
komunikat = "";
	for (i = 0; i < msgs.length; i++) {
komunikat += msgs[i] + "\n";
	}

wyswietlBlad(komunikat);
}

function wyswietlAktywnePozyjeMenuProduktow() {
$(".ukryte_kategorie").has(".active").removeClass('ukryte_kategorie');
	$(".active").siblings().removeClass('ukryte_kategorie');
	$(".active .ukryte_kategorie").removeClass('ukryte_kategorie');
	$(".active").removeClass('ukryte_kategorie');
}

var Pastmo = {
initZasobyInput: function (inputElementSelector) {
$(inputElementSelector).val("[]");
	},
	dodajIdZasobuDoInputa: function (idZasobu, inputElementSelector) {
	var inputVal = $(inputElementSelector).val();
		var inputArray = JSON.parse(inputVal);
		inputArray.push(idZasobu);
		$(inputElementSelector).val(JSON.stringify(inputArray));
	},
	sprawdzIloscNaMagazynie: function (ilosc, wariacja, magazyn) {
	var post = {
	ilosc: ilosc,
		magazyn: magazyn,
		wariacja: wariacja
	};
		$.post(url('/ajax/sprawdz_dostepna_ilosc'), post, function (data) {
		if (data.success !== true) {
		wyswietlBlad("Nie jest dostępna określona ilość w magazynie.");
		}
		});
	},
	potwierdzenie: function (funkcjaNaTak) {
	bootbox.confirm("Jesteś pewien?", function (result) {
	if (result) {
	funkcjaNaTak();
	}
	});
	},
	wyswietlKomunikat: function (tresc) {
	bootbox.alert(tresc);
	},
	czyPuste: function (string) {
	return string === undefined || string === null || string === "";
	},
	toString: function (input, defaultValue) {
	if (!Pastmo.czyPuste(input)) {
	return input;
	}

	if (defaultValue === undefined) {
	defaultValue = '';
	}
	return defaultValue;
	},
	toNumber: function (input) {
	if (isNaN(input)) {
	return 0;
	}
	var result = Number(input);
		return result;
	},
	wyswietlCene: function (input) {
	return parseFloat(Math.round(input * 100) / 100).toFixed(2);
	},
	valueUndefined: function (input, defaultValue) {
	if (Pastmo.czyDefined(input)) {
	return input;
	}
	return defaultValue;
	},
	czyDefined: function (value) {
	return value !== undefined;
	},
	pobierzFuncjeZWindow:function(klasa, funkcja, domyslnie){

	var domyslnaFunkcja = Pastmo.valueUndefined(domyslnie, function(){});
		var klasa = Pastmo.valueUndefined(window[klasa], {});
		return Pastmo.valueUndefined(klasa[funkcja], domyslnaFunkcja);
	},
	wyslijGet: function (adres, parametry, funkcjaOdbioru, ignorujZwykleBledy = false) {
	Pastmo.wyslijWspolne(adres, parametry, funkcjaOdbioru, "GET", false, ignorujZwykleBledy);
	},
	wyslijGetPelnyAdres: function (adres, parametry, funkcjaOdbioru, ignorujZwykleBledy = false) {
	Pastmo.wyslijWspolne(adres, parametry, funkcjaOdbioru, "GET", true, ignorujZwykleBledy);
	},
	wyslijPost: function (adres, parametry, funkcjaOdbioru, ignorujZwykleBledy = false) {
	Pastmo.wyslijWspolne(adres, parametry, funkcjaOdbioru, "POST", false, ignorujZwykleBledy);
	},
	wyslijPostPelnyAdres: function (adres, parametry, funkcjaOdbioru, ignorujZwykleBledy = false) {
	Pastmo.wyslijWspolne(adres, parametry, funkcjaOdbioru, "POST", true, ignorujZwykleBledy);
	},
	wyslijWspolne: function (adres, parametry, funkcjaOdbioru, metoda, pelnyAdres, ignorujZwykleBledy = false) {
	if (!pelnyAdres) {
	adres = url(adres);
	}
	var promise = $.ajax({
	url: adres,
		type: metoda,
		data: parametry,
	});
		promise.fail(function (jqXHR, textStatus) {
		Pastmo.obsluzError(jqXHR.responseText);
		});
		promise.done(function (msg) {
		if (ignorujZwykleBledy || Pastmo.sprawdzCzySuccess(msg)) {
		if (funkcjaOdbioru) {
		funkcjaOdbioru(msg);
		}
		} else {
		Pastmo.obsluzError(msg);
		}
		});
		return promise;
	},
	anulujRequest:function(request){
	if (request && typeof request.statusText === "undefined") {
	request.responseText = "anulowano"
		request.abort();
	}
	},
	czyRequestPozwalaNaWyslanieNastepnego:function(request){
	return !request || typeof request.statusText !== "undefined";
	},
	sprawdzCzySuccess: function (msg) {
	var set = typeof msg === 'object' && 'success' in msg && msg['success'];
		return set;
	},
	obsluzError: function (msg) {
	var wiadomosc = "Nieznany błąd";
		if (typeof msg === 'string') {
	if (msg === "anulowano"){
	return;
	}
	wiadomosc = this.wydobadzWiadomoscZWyjatku(msg);
	} else if ('msg' in msg) {
	wiadomosc = msg['msg'];
	} else if ('content' in msg) {
	try {
	var xdebug_message = msg['content']['exception']['xdebug_message'];
		wiadomosc = this.wydobadzWiadomoscZWyjatku(xdebug_message);
	} catch (e) {
	}
	}
	Pastmo.wyswietlKomunikat(wiadomosc);
	},
	obsluzErrorFileupload: function (e, data) {
	var response = data.response();
		if (response.jqXHR !== undefined){
	var text = response.jqXHR.responseText;
		Pastmo.obsluzError(text);
	}
	},
	wydobadzWiadomoscZWyjatku: function (xdebug_message) {
	try {
	var jsonowyElement = JSON.parse(xdebug_message);
		if (typeof jsonowyElement.msg !== undefined){
	return jsonowyElement.msg;
	}
	} catch (e) {
	}

	var start = xdebug_message.indexOf('Brak uprawnienia');
		if (start !== - 1) {
	var stop = xdebug_message.indexOf(' in', start);
		var dlugosc = stop - start;
		var wiadomosc = xdebug_message.substr(start, dlugosc);
	} else {
	var wiadomosc = xdebug_message;
	}

	return wiadomosc;
	},
	getMsg: function (key) {
	var result = Msg[key];
		return Pastmo.toString(result, 'not found');
	},
	sprawdzUprawnienia: function (szukaneUprawnienie) {
	var result = uprawnienia.indexOf(szukaneUprawnienie);
		return result !== - 1;
	},
	strContains: function (array, element) {
	var strContainer = '' + array;
		var strElement = '' + element;
		return Pastmo.inArray(strContainer.toUpperCase(), strElement.toUpperCase());
	},
	inArray: function (array, element) {
	return array.indexOf(element) !== - 1;
	},
	czyTablicaZawieraFagment: function (array, fragment) {
	for (i = 0; i < array.length; i++) {
	var element = array[i];
		if (Pastmo.strContains(element, fragment)) {
	return true;
	}
	}
	return false;
	},
	getMaxFileSizeFromPhpIniInit: function () {
	maxFileSizeFromPhpIni = 20 * 1024 * 1024;
//        Pastmo.maxFileSizeFromPhpIni();
		return maxFileSizeFromPhpIni;
	},
	maxFileSizeFromPhpIniInit: function () {
	Pastmo.wyslijGet('/zasoby/pobierz_maksymalny_rozmiar_zalacznika', {}, Pastmo.pobierzDane);
	},
	pobierzDane: function (dane) {
//        console.log(dane.rozmiar);
	},
	konwertujObiektNaTablice: function (obiekt) {
	var tablica = [];
		for (var key in obiekt) {
	if (obiekt[key] !== null) {
	tablica.push(obiekt[key]);
	}
	}
	return tablica;
	},
	konwertujObiektNaTabliceAsocjacyjna: function (obiekt) {
	var tablica = [];
		for (var key in obiekt) {
	tablica[key] = obiekt[key];
	}
	return tablica;
	},
	oznaczPoleJakoZapisane:function(self){
	$(self).addClass('zapisane');
		setTimeout(function () {
		$(self).removeClass('zapisane');
		}, 2000);
	}
};
	jQuery(document).ready(function ($) {
$('.toggle_button').click(function (e) {
$('.toggle').toggle();
});
	wyswietlAktywnePozyjeMenuProduktow();
	if (typeof (locale) !== 'undefined') {
moment.locale(locale);
}
});
//http://jquery-howto.blogspot.com/2009/09/get-url-parameters-values-with-jquery.html
	$.extend({
	getQueryParameters: function () {
	var vars = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		for (var i = 0; i < hashes.length; i++)
	{
	hash = hashes[i].split('=');
		vars.push(hash[0]);
		vars[hash[0]] = hash[1];
	}
	return vars;
	},
		getQuerySingleParam: function (name) {
		return $.getUrlVars()[name];
		}
	});
