ObslugaInputow.podepnijSelektorDaty = function (element) {
    var jezyk = Ustawienia.getLocale();

    element.datetimepicker({
	locale: jezyk,
	minDate: new Date,
	format: "YYYY-MM-DD",
//	debug: true,
    });
};
ObslugaInputow.podepnijSelektorDataCzasu = function (element, parametry) {

    var standardoweParametry = {
	locale: Ustawienia.getLocale(),
	minDate: new Date,
	format: "YYYY-MM-DD HH:mm",
//	debug: true,
    };

    var przekazaneParametry = Pastmo.valueUndefined(parametry, {});

    var par = Object.assign(standardoweParametry, przekazaneParametry);

    element.datetimepicker(par);
};

ObslugaInputow.godzinyRobocze = {disabledHours: [0, 1, 2, 3, 4, 5, 6, 7, 8, 18, 19, 20, 21, 22, 23, 24]};

ObslugaInputow.klasaObsluzonegoElementu = "";

if (typeof wTrakcieTestow === 'undefined') {
    jQuery(document).ready(function ($) {
	ObslugaInputow.podepnijDateTimepickerPodWszystkieTimeInputy();
	ObslugaInputow.podepnijDatepickerPodWszystkieDateInputy();
    });
}