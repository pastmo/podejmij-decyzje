Dymki = function (selektor, funkcjaNaShow, funkcjaModyfikujacaParametry) {
    var self = this;
    this.selektor = selektor;
    this.selektorFlagiAtywnosci = "aktywne_pobieranie";
    this.selektorKonteneraTresci = '.tmp_wynikow';
    this.funkcjaNaShow = Pastmo.valueUndefined(funkcjaNaShow, function () {});
    this.funkcjaModyfikujacaParametry = Pastmo.valueUndefined(funkcjaModyfikujacaParametry, function (param) {});

    var self = this;

    $(selektor).hover(function () {

	var e = $(this);
	if (e.data(self.selektorFlagiAtywnosci)) {
	    return;
	}
	e.data(self.selektorFlagiAtywnosci, true);
	self.wyswietlTooltip(e);

    },
	    function () {
		var e = $(this);
		e.data(self.selektorFlagiAtywnosci, false);
		e.siblings('.popover').remove();
	    });
    this.wyswietlTooltip = function (e) {
	var kontenerTresci = e.siblings(self.selektorKonteneraTresci).children();
	var indeks = $(self.selektor).index(e);
	var selektorAktywnego = self.selektor + ":eq(" + indeks + ")";
	$(selektorAktywnego);

	if (kontenerTresci.length > 0) {
	    if (e.data(self.selektorFlagiAtywnosci)) {
		var domyslneParametry = {
		    html: true,
		    placement: 'auto',
		    container: selektorAktywnego,
		    trigger: 'hover',
		    content: function () {
			var tresc = kontenerTresci.html();
			return tresc;
		    }
		};

		self.funkcjaModyfikujacaParametry(domyslneParametry, e, indeks);

		e.popover(domyslneParametry)
			.on('shown.bs.popover', function (e) {
			    self.funkcjaNaShow(this);
			})
			.popover('show')
			;
	    }
	} else {
	    $.get(e.data('url'), function (d) {
		e.siblings(self.selektorKonteneraTresci).append(d);
		self.wyswietlTooltip(e);
	    });
	}
    }

}
