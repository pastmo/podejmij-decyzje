var AktywacjaPrzyciskow = {
    aktywujPrzyciskiZPotwierdzeniemAjax: function () {
	$(document).on('click', '.przycisk_z_potwierdzeniem_ajax', AktywacjaPrzyciskow.przyciskZPotwierdzeniemAjax);
    },
    aktywujPrzyciskiZPotwierdzeniem: function () {
	$(document).on('click', '.przycisk_z_potwierdzeniem', function (e) {
	    var href = $(this).data("url");
	    AktywacjaPrzyciskow.przyciskZPotwierdzeniem(e, href);
	});
    },
    przyciskZPotwierdzeniemAjax: function (e) {
	e.preventDefault();
	var kontener = new WysylaczPrzyciskuAjax($(this));

	bootbox.confirm(kontener.komunikat, function (result) {
	    if (result) {
		kontener.wyslij();
	    }
	});
    },
    przyciskZPotwierdzeniem: function (e, href) {
	e.preventDefault();
	Pastmo.potwierdzenie(function () {
	    location.href = href;
	});
    },
}

var WysylaczPrzyciskuAjax = function (element) {

    var url = element.data('url');
    var parametryPost = Pastmo.valueUndefined(element.data('parametry_post'), {});
    var klasa = element.data('klasa_odbioru');
    var funkcja = element.data('funkcja_odbioru');
    this.komunikat = element.data('komunikat_potwierdzenia');
    var funkcjaOdbioru = Pastmo.pobierzFuncjeZWindow(klasa, funkcja);

    this.wyslij = function () {
	Pastmo.wyslijPost(url, parametryPost, funkcjaOdbioru);
    }
}

if (typeof wTrakcieTestow === 'undefined') {
    AktywacjaPrzyciskow.aktywujPrzyciskiZPotwierdzeniemAjax();
    AktywacjaPrzyciskow.aktywujPrzyciskiZPotwierdzeniem();
}