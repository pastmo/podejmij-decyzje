var Ustawienia = {
    getCzasOdswiezania: function () {
	return odswiezanieJs;
    },
    initJezyki: function () {
	var jezyk = this.getLocale();

	bootbox.setDefaults({
	    locale: jezyk
	});

	moment.locale(jezyk);
    },
    getLocale: function () {
	return Pastmo.valueUndefined(locale, 'pl');
    }

}

jQuery(document).ready(function ($) {
    Ustawienia.initJezyki();
});