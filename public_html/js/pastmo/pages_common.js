PagesCommon = {
    me: null,
    reload: function () {
	location.reload();
    },
    ustawDziedziczenie: function (object) {
	var newProto = Object.create(this);
	object.__proto__ = newProto;
	newProto.me = object;
    }
}