jQuery(document).ready(function ($) {

    $('.przycisk_zaawansowane').click(
	    function () {

		bootbox.dialog({
		    title: "Wyszukiwanie zaawansowane",
		    message: '<div id="wyszukiwanie">  ' +
			    $('#wybor_modulow_wraper').html() +
			    ' </div>',
		    buttons: {
			success: {
			    label: "Save",
			    className: "btn-success",
			    callback: function () {

				$('#wybor_modulow_wraper .wyszukiwanie_input')
					.attr("checked", function (arr) {
					    var checked = $('#wyszukiwanie .wyszukiwanie_input')[arr].checked;
					    return checked;
					});
			    }
			}
		    }
		}
		);

		return false;
	    });
});
