var sortowanie;
var strzalka_gora = "aktywna_gora";
var strzalka_dol = "aktywny_dol";
var brak_wyboru = "brak_wyboru";



function Sortowanie(klucz, typ) {

    this.zmienSortowanie = function (klucz) {
	if (this.klucz !== klucz) {
	    this.klucz = klucz;
	    this.typ = this.stany[0];
	} else {
	    var index = this.stany.indexOf(this.typ);
	    index++;
	    this.typ = this.stany[index % this.stany.length];
	}

	this.sortuj(this.klucz, this.typ);

    };


    this.sortuj = function (klucz, typ) {
	$('#sort_key').val(klucz);
	$('#sort_typ').val(typ);

	$('.wyszukiwarka_form').submit();
    }

    this.uaktualijKlasy = function () {
	$('.strzalki').removeClass(strzalka_gora);
	$('.strzalki').removeClass(strzalka_dol);
	$('.strzalki').each(function (index, element, array) {
	    if ($(element).data('klucz') === sortowanie.klucz && sortowanie.typ !== "") {
		if (sortowanie.typ === "DESC") {
		    $(element).addClass(strzalka_gora);
		} else if (sortowanie.typ === "ASC") {
		   $(element).addClass(strzalka_dol);
		} else {
		   $(element).addClass(brak_wyboru);
		}
	    } else {
		$(element).addClass(brak_wyboru);
	    }
	});
    }


    this.stany = ['ASC', 'DESC', ''];
    this.ASC=this.stany[0];
    this.DESC=this.stany[1];

    this.klucz = klucz;
    this.typ = typ;



}

function aktywujSortowanie() {
    var aktualnyKlucz = $('#sort_key').val();
    var aktualnyTyp = $('#sort_typ').val();

    sortowanie = new Sortowanie(aktualnyKlucz, aktualnyTyp);
    sortowanie.uaktualijKlasy();

    $('.strzalki .gora').click(function () {
	var klucz = $(this).parent().data('klucz');
	sortowanie.sortuj(klucz,sortowanie.ASC);
    });

    $('.strzalki .dol').click(function () {
	var klucz = $(this).parent().data('klucz');
	sortowanie.sortuj(klucz,sortowanie.DESC);
    });

    return;
}


jQuery(document).ready(function ($) {
    aktywujSortowanie();
});