var Podpowiadanie = {
    getObiektBloodhound: function (url) {
	var podpowiadanie = new Bloodhound({
	    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
	    queryTokenizer: Bloodhound.tokenizers.whitespace,
	    remote: {
		url: url + "?query=%QUERY",
		wildcard: '%QUERY'
	    }
	});
	return podpowiadanie;
    },
}

jQuery(document).ready(function ($) {

    var url = $('#typeahead-search').data('url');

    var podpowiadanie=Podpowiadanie.getObiektBloodhound(url);


    var wyszukiwarka = $('#typeahead-search').typeahead(null, {
	name: 'wyszykiwanie',
	display: 'value',
	source: podpowiadanie,
	templates: {
	    empty: [
		'Empty result'

	    ].join('\n'),
	    suggestion: Handlebars.compile(
		    '<div class="header-search-element">' +
		    '<p>{{ value }}</p>' +
		    '<p class="header-search-type">{{ type }}</p>' +
		    '</div>'
		    )
	}
    });

    wyszukiwarka.on('typeahead:selected', function (evt, data) {
	window.location.href = data.url;
    });

    $('#typeahead-przypomnienia').typeahead(null, {
	name: 'wyszykiwanie1',
	display: 'value',
	source: podpowiadanie,
	templates: {
	    empty: [
		'Empty result'
	    ].join('\n'),
	    suggestion: Handlebars.compile(
		    '<div class="header-search-element">' +
		    '<p>{{ value }}</p>' +
		    '<p class="header-search-type">{{ type }}</p>' +
		    '</div>'
		    )
	}
    });

    $("#typeahead-przypomnienia").on("typeahead:selected typeahead:autocompleted", function (e, datum) {
	$('#typeahead-przypomnienia-menager').val(datum.menager);
	$('#typeahead-przypomnienia-id').val(datum.id);
    });

});
