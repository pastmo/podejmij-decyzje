var onloadCallback = function () {

    $(".g-recaptcha").each(function () {
	PodpinanieCaptcha.podepnij(this);
    });

};

var PodpinanieCaptcha = {
    podepnij: function (self) {
	var object = $(self);
	var funkcjaOdbioru = object.data('funkcja_obslugi');

	var id = grecaptcha.render(object.attr("id"), {
	    'sitekey': object.data('sitekey'),
	    "callback": function (token) {
		window[funkcjaOdbioru](token);
	    }
	});

	object.attr('data-id_captcha', id);
    }
};