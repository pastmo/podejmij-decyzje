var InfoCiasteczka = {
    init: function () {
	this.wyswietlInformacje();
	this.aktywujPrzycisk();
    },
    wyswietlInformacje: function () {
	var czyZgoda = Cookies.get('zgoda_ciastka');

	if (typeof czyZgoda === 'undefined') {
	    $('#ciacho_komunikat').removeClass('ukryte');
	}
    },
    aktywujPrzycisk: function () {

	$('#zamykanie_ciastek').click(function () {
	    $('#ciacho_komunikat').addClass('ukryte');
	    Cookies.set('zgoda_ciastka', true);
	});

    }
};

if (typeof wTrakcieTestow === 'undefined') {
    InfoCiasteczka.init();
}