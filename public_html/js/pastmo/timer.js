var aktywneOdswiezania = [];

function Timer(funkcjaNaZdarzenie, divider = 1) {

    this.interval = Ustawienia.getCzasOdswiezania() * divider;
    this.funkcjaNaZdarzenie = funkcjaNaZdarzenie;
    this.obiektInterwalu = null;

    this.uruchomCykliczneOdswiezanie = function () {
	if (!Pastmo.inArray(aktywneOdswiezania, this.funkcjaNaZdarzenie)) {
	    aktywneOdswiezania.push(this.funkcjaNaZdarzenie);
	    this.setInterval();
	}
    }
    ;

    this.setInterval = function () {
	var int = this.interval;
	this.obiektInterwalu = setInterval(this.funkcjaNaZdarzenie, int);
    }

    this.zmienInterwal = function (nowy) {
	this.zatrzymajCykliczneOdswiezanie();
	this.interval = nowy;
	this.uruchomCykliczneOdswiezanie();
    };

    this.zatrzymajCykliczneOdswiezanie = function () {
	var index = aktywneOdswiezania.indexOf(this.funkcjaNaZdarzenie);
	if (index > -1) {
	    aktywneOdswiezania.splice(index, 1);

	    clearInterval(this.obiektInterwalu);
	}
    } ;

      this.reset = function () {
	this.zatrzymajCykliczneOdswiezanie();
	this.uruchomCykliczneOdswiezanie();
    };
}