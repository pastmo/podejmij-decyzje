var aktywneAkcje = {};

function Wyzwalacz(akcja) {

    this.funkcja = akcja;
    this.aktualnyParametr;
    this.opoznienie = 400;

    this.wywolaj = function (parametr) {
	self = this;
	this.czyscStareWywolania();
	this.ustawParametr(parametr);

	aktywneAkcje[this.funkcja] = setTimeout(function () {
	    self._wywolaj();
	},self.opoznienie);
    };
    this.zatrzymaj = function () {
	this.czyscStareWywolania();
    };

    this.ustawParametr = function (parametr) {
	this.aktualnyParametr = parametr;
    };

    this.czyscStareWywolania = function () {
	var timeout = aktywneAkcje[this.funkcja];

	if (timeout !== undefined) {
	    clearTimeout(timeout);
	}
    };

    this._wywolaj = function () {
	this.funkcja(this.aktualnyParametr);
    };

}