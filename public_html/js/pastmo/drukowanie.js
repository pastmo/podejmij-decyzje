Drukowanie = {
    printContent: function (el) {
	var restorepage = $('body').html();
	var printcontent = $('#' + el).clone();
	$('body').empty().html(printcontent);
	window.print();
	$('body').html(restorepage);
    },
    dodajDoWydruku: function (element, kolumny_do_wydruku) {
	var jElement = $(element);

	var wlasciweKolumny = jElement.parent().siblings();

	var tekst = "";

	$.each(kolumny_do_wydruku, function (index, value) {
	    var nazwa = $($('th .nazwa')[value]).html();
	    tekst += Drukowanie.opakuj(nazwa + " : " + $(wlasciweKolumny[value]).html());
	});


	tekst += "<hr>";

	$('#drukDiv').append(tekst);
    },
    opakuj: function (tekst) {
	return "<p><pre>" + Drukowanie.usunDivy(tekst) + "</pre></p>";
    },
    usunDivy: function (tekst) {
	tekst = Drukowanie.replaceAll(tekst, '</div><div>', ", ");
	tekst = Drukowanie.replaceAll(tekst, '<div>', "");
	tekst = Drukowanie.replaceAll(tekst, '</div>', "");
	return tekst;
    },
//http://stackoverflow.com/questions/1144783/replacing-all-occurrences-of-a-string-in-javascript
    replaceAll: function (str, find, replace) {
	return str.replace(new RegExp(Drukowanie.escapeRegExp(find), 'g'), replace);
    },
    escapeRegExp: function (str) {
	return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
    }
}