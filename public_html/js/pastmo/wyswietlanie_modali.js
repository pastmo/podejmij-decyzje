var WyswietlanieModali = {
    parametry: {},
    wyswietlModala: function (url, parametryGeta, parametryDoInita, parametryBootboxa) {
	var kontener = new KontenerOdbioruModala(parametryDoInita, parametryBootboxa);
	Pastmo.wyslijGet(url, parametryGeta, kontener.odbior, true);
    },
    odbiorModala: function (dane, kontener) {
	var dodawaneOkienko = $("<div>" + dane + "</div>");
	var skrypty = dodawaneOkienko.find('#dolaczone_skrypty').data('skrypty');
	var tytul = dodawaneOkienko.find("#parametry_modala").data('title');
	var parametry = kontener.pobierzParametry({message: dane, title: tytul});

	bootbox.dialog(parametry);

//	WyswietlanieModali.aktywujDraggable();

//	ObslugaInputow.podepnijDatepickerPodWszystkieDateInputy();
	$.each(skrypty, kontener.dolaczLubAktywujSkrypt);
    },
    dolaczLubAktywujSkrypt: function (index, element, kontener) {
	var url = element.url;
	var nazwaFunkcji = element.funkcja_js;

	WyswietlanieModali.parametry[nazwaFunkcji] = kontener.parametryInita;

	if (typeof window[nazwaFunkcji] === 'undefined') {
	    var jsElm = document.createElement("script");
	    jsElm.type = "application/javascript";
	    jsElm.src = url;
	    document.body.appendChild(jsElm);
	} else {
	    window[nazwaFunkcji].init();
	}

    },
    dodajParametry: function (klucz, parametry) {
	this.parametry[klucz] = parametry;
    },
    getParametry: function (klucz) {
	var wynik = this.parametry[klucz];
	return Pastmo.valueUndefined(wynik, {});
    },
    aktywujDraggable: function () {
	$(".bootbox .modal-dialog").draggable({
	    handle: ".modal-header"
	});
    }
}

var KontenerOdbioruModala = function (parametryDoInita, parametryBootboxa) {
    var self = this;
    this.parametryInita = parametryDoInita;
    this.parametryBootboxa = Pastmo.valueUndefined(parametryBootboxa);

    this.odbior = function (dane) {
	WyswietlanieModali.odbiorModala(dane, self);
    }
    this.dolaczLubAktywujSkrypt = function (index, element) {
	WyswietlanieModali.dolaczLubAktywujSkrypt(index, element, self);
    }
    this.pobierzParametry = function (parametryModala) {
	var standardoweParametry = {
	    size: "large",
	    buttons: {
		close: {
		    label: "Zamknij",
		    className: "btn btn-primary"
		},
		success: {
		    label: "Zapisz",
		    className: "btn btn-primary zapis_modala",
		}
	    }
	};
	var wynik = Object.assign(standardoweParametry, this.parametryBootboxa, parametryModala);

	return wynik;
    }


}
