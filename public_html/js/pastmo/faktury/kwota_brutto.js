function obliczBrutto() {
    var netto = Number($('#netto').val());
    var vat = Number($('#vat').val());

    var brutto = netto + (netto * vat / 100);
    brutto=Math.round(brutto * 100) / 100;

    if (!isNaN(brutto)) {
	$('#brutto').html(brutto);
    }
}


jQuery(document).ready(function ($) {
    $('#netto').change(function () {
	obliczBrutto();
    });
    $('#vat').change(function () {
	obliczBrutto();
    });
    $('#netto').keyup(function () {
	obliczBrutto();
    });
    $('#vat').keyup(function () {
	obliczBrutto();
    });
});