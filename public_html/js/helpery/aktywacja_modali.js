var AktywacjaModali = {
    init: function () {
	this.initOknoLogowania();
	this.initOknoRejestracji();
	this.initOknoRegulaminu();
	this.initOknoPolitykiPrywatnosci();
    },
    initOknoLogowania: function () {
	$(document).off('.wyswietlanie_okna_logowania');
	$(document).on('click', '.wyswietlanie_okna_logowania', function (e) {
	    e.preventDefault();
	    AktywacjaModali.wykonajWyswietlaniaOknaLogowania();
	});
    },
    wyswietlOknoLogowania: function () {
	AktywacjaModali.wykonajWyswietlaniaOknaLogowania();
    },
    wykonajWyswietlaniaOknaLogowania: function () {
	if (Ekran.czyMniejszyEkran()) {
	    window.location.href='/logowanie';
	} else {
	    var parametry = PrzyciskiModali.bezPrzyciskow;
	    parametry.animate = false;
	    parametry.size = "medium"
	    WyswietlanieModali.wyswietlModala('/logowanie/wyswietl_okno_logowania', {}, {}, parametry);
	}
    },
    initOknoRejestracji: function () {
	$(document).off('.wyswietlanie_okna_rejestracji');
	$(document).on('click', '.wyswietlanie_okna_rejestracji', function (e) {
	    e.preventDefault();

	    var parametry = PrzyciskiModali.bezPrzyciskow;
	    parametry.animate = false;
	    WyswietlanieModali.wyswietlModala('/logowanie/wyswietl_okno_rejestracji', {}, {}, parametry);

	});
    },
    initOknoRegulaminu: function () {
	$(document).off('.regulamin_modal');
	$(document).on('click', '.regulamin_modal', function (e) {
	    e.preventDefault();

	    var parametry = PrzyciskiModali.bezPrzyciskow;
	    parametry.animate = false;
	    WyswietlanieModali.wyswietlModala('/logowanie/regulamin_modal', {}, {}, parametry);

	});
    },
    initOknoPolitykiPrywatnosci: function () {
	$(document).off('.polityka_prywatnosci_modal');
	$(document).on('click', '.polityka_prywatnosci_modal', function (e) {
	    e.preventDefault();

	    var parametry = PrzyciskiModali.bezPrzyciskow;
	    parametry.animate = false;
	    WyswietlanieModali.wyswietlModala('/logowanie/polityka_prywatnosci_modal', {}, {}, parametry);

	});
    },
};

var PrzyciskiModali = {
    bezPrzyciskow: {
	buttons: {
	}
    },
    przyciskZamykania: {
	buttons: {
	    close: {
		label: "Zamknij",
		className: "btn btn-default zamykanie_modala"
	    }
	}
    },
    zamykanieZOstrzezeniem: function (komunikat) {
	return{
	    buttons: {
		close: {
		    label: komunikat,
		    className: "btn btn-danger"
		}
	    }
	}
    },
    takNie: {
	buttons: {
	    close: {
		label: "Nie",
		className: "btn btn-primary zamykanie_modala"
	    },
	    success: {
		label: "Tak",
		className: "btn btn-primary zapis_modala",
	    }
	}
    },
    zapiszWyjdz: {
	buttons: {
	    close: {
		label: "Wyjdź",
		className: "btn zamykanie_modala"
	    },
	    success: {
		label: "Zapisz",
		className: "btn btn-primary zapis_modala",
	    }
	}
    },
};
if (typeof wTrakcieTestow === 'undefined') {
    AktywacjaModali.init();

}
