"use strict"

function url(url) {
    var newUrl = url.replace(/-/g, "_");
    var result = home_url + removeFirstSlash(newUrl);
    return result;
}

function removeFirstSlash(path)
{
    if (path.substring(0, 1) === '/') {
	path = path.substring(1);
    }
    return path;
}

var PodejmijDecyzje = {
    init: function () {
	
    },
  
    pokazFlex: function (obiekt) {
	obiekt.css('display', 'flex');
    }
};

if (typeof wTrakcieTestow === 'undefined') {
    PodejmijDecyzje.init();
}