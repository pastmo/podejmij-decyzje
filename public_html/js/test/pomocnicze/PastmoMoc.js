var wTrakcieTestow = true;
var locale = 'pl';
var odswiezanieJs = 4200;
var uprawnienia = ["men_auk", "men_awy", "men_aza", "men_azk", "men_bka", "men_bpd", "men_bpr", "men_bus", "men_bzm", "men_bzn", "men_das", "men_edi", "men_ema", "men_fir", "men_kal", "men_kli", "men_kos", "men_ksp", "men_mag", "men_mdo", "men_mpl", "men_pdy", "men_prd", "men_pro", "men_rol", "men_skl", "men_sog", "men_spr", "men_sta", "men_swm", "men_urz", "men_uzp", "men_uzy", "men_wia", "men_wiz", "men_wka", "men_wmp", "men_wnp", "men_wpn", "men_wpr", "men_wys", "pro_ace", "pro_ain", "pro_app", "pro_awi", "pro_etp", "pro_eup", "pro_ppr", "pro_wce", "rol_adm", "rol_def", "rol_eup", "rol_ggr", "rol_gra", "rol_kli", "rol_pra", "rol_spr", "rol_tup", "skl_psl", "sta_psg", "sta_psp", "sta_pss", "ust_pus", "uzy_euz", "uzy_ppu", "uzy_tuz", "zam_pwy"];
var bootbox = {alert: function () {}, confirm: function () {}, prompt: function () {}, dialog: function () {}, setDefaults: function () {}};
var RefreshPlayer = function () {};
var LoadPlayer = function () {};

function url(url) {
    return url;
}

function getCurrentUserId() {
    return 42;
}

var PastmoMoc = {
    mockujAjaxa: function (odpowiedz) {
	spyOn($, 'ajax').and.callFake(function (req) {
	    var d = $.Deferred();
	    d.resolve(odpowiedz);
	    return d.promise();
	})
    },
    mockujVal: function (v1, v2, v3, v4, v5, v6, v7) {
	spyOn($.fn, 'val').and.returnValues(v1, v2, v3, v4, v5, v6, v7);
    },
    mockujData: function (wynik) {
	spyOn($.fn, 'data').and.callFake(function (req) {
	    return wynik;
	});
    },
    mockujAttr: function (wynik) {
	spyOn($.fn, 'attr').and.callFake(function (req) {
	    return wynik;
	});
    },
    mockujClosest: function (wynik) {
	spyOn($.fn, 'closest').and.callFake(function (req) {
	    return wynik;
	});
    },
    mockujAppend: function () {
	spyOn($.fn, "append");
    },
    mockujParent: function (wynik) {
	spyOn($.fn, 'parent').and.callFake(function (req) {
	    return wynik;
	});
    },
    mockujUprawnienia: function (wynik) {
	spyOn(Pastmo, 'sprawdzUprawnienia').and.callFake(function () {
	    return wynik;
	});
    },
    utworzEventMock: function (wynik) {
	spyOn($.fn, 'val').and.callFake(function (req) {
	    return wynik;
	});
    },
    utworzMockNaKlasie: function (klasa, metody) {
	var mock = jasmine.createSpyObj(klasa, metody);
	return mock;
    },
    utworzMocka: function (nazwaMetody) {
	var mock = jasmine.createSpy(nazwaMetody);
	return mock;
    },
    mockujIndex: function (v1, v2, v3, v4, v5, v6, v7) {
	spyOn($.fn, 'index').and.returnValues(v1, v2, v3, v4, v5, v6, v7);
    },
    mockujSize: function (v1, v2, v3, v4, v5, v6, v7) {
	spyOn($.fn, 'size').and.returnValues(v1, v2, v3, v4, v5, v6, v7);
    },
    mockujTimeline: function () {
	spyOn(vis, 'Timeline').and.callFake(function () {
	    return{'on': function () {}}
	});
    },
    mockujFunkcjeObiektu: function (obiekt, funkcja, v1, v2, v3, v4, v5, v6, v7) {
	spyOn(obiekt, funkcja).and.returnValues(v1, v2, v3, v4, v5, v6, v7);
    },
    foo: function () {},
    successOdp: {success: true},
    pojedynczyElement: function (elementy, przedrostek, czyWidoczny) {
	elementy.forEach(function (item, index) {
	    var wynik = $(przedrostek + item).is(":visible");
	    expect(wynik).toBe(czyWidoczny);
	});
    },
    wywolajEventKlawisza: function (selektor, eventCode, keyCode) {

	var e = $.Event(eventCode, {keyCode: keyCode});
	$(selektor).trigger(e);
    },
    sprawdzWywolanieAjaxaPost: function (url, dane) {
	this.sprawdzWywolanieAjaxa(url, 'POST', dane);
    },
    sprawdzWywolanieAjaxaGet: function (url, dane) {
	this.sprawdzWywolanieAjaxa(url, 'GET', dane);
    },
    sprawdzWywolanieAjaxa: function (url, metoda, dane) {
	expect($.ajax).toHaveBeenCalledWith({url: url, type: metoda, data: dane});

    },
}
