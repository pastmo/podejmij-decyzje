var projektyOdpowiedziAjax = {
    odpowiedzNaUpdateKrokuMilowego: function () {
	return {"success":true,"wynik":{"krokiMilowe":[{"krok_id":1,"kolejnosc":"3","kod":"neg3","data":"2017-08-15","kolor":"#3FCF36"},{"krok_id":2,"kolejnosc":"2","kod":"gra2","data":null,"kolor":"#36A9E1"},{"krok_id":3,"kolejnosc":"2","kod":"pro2","data":"2017-08-16","kolor":"#36A9E1"}],"msgs":[],"udanaZmianaKroku":true,"wymaganyFormularz":false}};
    },
    odpowiedzNaUpdateKrokuMilowego_formularz_parametrow_produkcji: function () {
	return {"success":true,"wynik":{"krokiMilowe":[{"krok_id":1,"kolejnosc":"3","kod":"neg3","data":null,"kolor":"#3FCF36"}],"msgs":["Brak parametr\u00f3w produkcji"],"udanaZmianaKroku":false,"wymaganyFormularz":"parametry_produkcji"}};
    },
    odpowiedzNaUpdateKrokuMilowego_formularz_daty_wysylki: function () {
	return {"success":true,"wynik":{"krokiMilowe":[{"krok_id":2,"kolejnosc":"3","kod":"gra3","data":null,"kolor":"#3FCF36"},{"krok_id":1,"kolejnosc":"3","kod":"neg3","data":null,"kolor":"#3FCF36"},{"krok_id":3,"kolejnosc":"2","kod":"pro2","data":null,"kolor":"#36A9E1"}],"msgs":["Nieustawiona data wysy\u0142ki"],"udanaZmianaKroku":false,"wymaganyFormularz":"data_wysylki"}};
    },
    odpowiedzNaPobieranieProjektow:function(){
	return $("#test_odpowiedz_ajax").html();
    },
}

