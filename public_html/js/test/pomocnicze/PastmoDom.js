var PastmoDom = {
    element: null,
    e: null,
    keyDown: function (selektor, kodPrzycisku) {
	PastmoDom.trigerKeyEventDiv(selektor, kodPrzycisku, "keydown");
    },
    keyUp: function (selektor, kodPrzycisku) {
	PastmoDom.trigerKeyEventDiv(selektor, kodPrzycisku, "keyup");
    },
    drop: function (selektor) {
	this.pobierzElement(selektor);
	this.zrobDropEvent();
	this.ustawWartoscWHtmlu();
	this.wywolajZdarzenie();
    },
    trigerKeyEventDiv: function (selektor, kodPrzycisku, zdarzenie) {
	this.pobierzElement(selektor);
	this.zrobKeyEvent(kodPrzycisku, zdarzenie);
	this.ustawWartoscWHtmlu();
	this.wywolajZdarzenie();

    },
    pobierzElement: function (selektor) {
	this.element = $(selektor);
	this.element.focus();
    },
    zrobKeyEvent: function (kodPrzycisku, zdarzenie) {
	this.e = jQuery.Event(zdarzenie);
	this.e.which = kodPrzycisku;
    },
    zrobDropEvent: function () {

	var base64 = "iVBORw0KGgoAAAANSUhEUgAAAAoAAAAJCAIAAACExCpEAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAwSURBVChTY3gro4IHES39/xMWNhG6kfWhIagEUAWmIqAIQgiiAg3hNBaC8ErLqAAAHHRzxxyBezYAAAAASUVORK5CYII=";

	var binary = fixBinary(atob(base64));
	file = new Blob([binary], {type: 'image/png'});

	this.e = jQuery.Event('drop');
	this.e.originalEvent = {dataTransfer: {files: [file]}};
//	this.e.which = kodPrzycisku;
    },
    ustawWartoscWHtmlu: function () {
	this.element.html(String.fromCharCode(this.e.which));
    },
    wywolajZdarzenie: function () {
	this.element.trigger(this.e);
    },
};

function fixBinary(bin) {
    var length = bin.length;
    var buf = new ArrayBuffer(length);
    var arr = new Uint8Array(buf);
    for (var i = 0; i < length; i++) {
	arr[i] = bin.charCodeAt(i);
    }
    return buf;
}