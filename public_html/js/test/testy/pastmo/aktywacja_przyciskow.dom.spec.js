var TestySkryptuPastmo = {
    foo: function () {}
};

describe('pastmo aktywacja_przyciskow.dom.spec', function () {

    beforeEach(function () {
	jasmine.getFixtures().fixturesPath = 'base/js/test/testy/pastmo';
	loadFixtures('aktywacja_przyciskow.dom.spec.html');

	AktywacjaPrzyciskow.aktywujPrzyciskiZPotwierdzeniemAjax();
    });

    it('aktywujAjaxowyPrzyciskiZPotwierdzeniem', function () {
	spyOn(bootbox, 'confirm');

	$('#test_przycisku_ajax').click();

	expect(bootbox.confirm).toHaveBeenCalledWith('Czy jesteś pewien, że chcesz wykonać tę akcję?', jasmine.any(Function));
    });
    it('aktywujAjaxowyPrzyciskiZPotwierdzeniem', function () {
	spyOn(Pastmo, 'wyslijPost');

	var kontener = new WysylaczPrzyciskuAjax($('#test_przycisku_ajax'));
	kontener.wyslij();

	expect(Pastmo.wyslijPost).toHaveBeenCalledWith('url_akcji_ajax', {dodatkowy_parametr: 1}, jasmine.any(Function));
    });
    it('aktywujAjaxowyPrzyciskiZPotwierdzeniem minimalne ustawienia', function () {
	spyOn(Pastmo, 'wyslijPost');

	var kontener = new WysylaczPrzyciskuAjax($('#test_przycisku_ajax_bez_parametow_posta'));
	kontener.wyslij();

	expect(Pastmo.wyslijPost).toHaveBeenCalledWith('url_akcji_ajax', jasmine.any(Object), jasmine.any(Function));
    });

    it('aktywujAjaxowyPrzyciskiZPotwierdzeniem- wywolywanie akcji po wyslaniu posta', function () {

	spyOn(TestySkryptuPastmo, 'foo');
	PastmoMoc.mockujAjaxa({success: true});

	var kontener = new WysylaczPrzyciskuAjax($('#test_przycisku_ajax'));
	kontener.wyslij();

	expect(TestySkryptuPastmo.foo).toHaveBeenCalled();
    });
});
