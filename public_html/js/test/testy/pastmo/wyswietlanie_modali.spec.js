describe('Testy wyswietlanie_modali>>', function () {

    it('wyswietl modal i sprawdz dolaczone skrypty', function () {
	spyOn(bootbox, 'dialog');
	spyOn(WyswietlanieModali, 'dolaczLubAktywujSkrypt');
	PastmoMoc.mockujAjaxa('<div>odpowiedz</div><div id="dolaczone_skrypty" data-skrypty=\'[{"url":"skrypt.js","funkcja_js":"Funkcja"}]\'');
	WyswietlanieModali.wyswietlModala('url', [], []);

	expect(bootbox.dialog).toHaveBeenCalled();
	expect(WyswietlanieModali.dolaczLubAktywujSkrypt).toHaveBeenCalledWith(0, {url: 'skrypt.js', funkcja_js: 'Funkcja'}, jasmine.any(KontenerOdbioruModala));

    });

    it('wyswietl modal i sprawdz tytul modala', function () {
	spyOn(bootbox, 'dialog');
	spyOn(WyswietlanieModali, 'dolaczLubAktywujSkrypt');
	PastmoMoc.mockujAjaxa('<div id="parametry_modala" data-title="Tytul modala">odpowiedz</div><div id="dolaczone_skrypty" data-skrypty=\'[]\'');
	WyswietlanieModali.wyswietlModala('url', [], []);

	expect(bootbox.dialog).toHaveBeenCalled();
	var result = bootbox.dialog.calls.mostRecent().args[0].title;
	expect(result).toBe('Tytul modala');

    });

    it('dodatkowe parametry do bootboxa', function () {
	spyOn(bootbox, 'dialog');
	spyOn(WyswietlanieModali, 'dolaczLubAktywujSkrypt');
	PastmoMoc.mockujAjaxa('<div>odpowiedz</div><div id="dolaczone_skrypty" data-skrypty=\'[{"url":"skrypt.js","funkcja_js":"Funkcja"}]\'');
	WyswietlanieModali.wyswietlModala('url', [], [],{'inny_parametr_bootboxa':'inny parametr bootboxa'});

	var result = bootbox.dialog.calls.mostRecent().args[0].inny_parametr_bootboxa;
	expect(result).toBe('inny parametr bootboxa');

    });

    it('wyswietl modal i dodanie parametru do inita', function () {
	spyOn(bootbox, 'dialog');
	spyOn(ParametryWizualizacji, 'init');
	PastmoMoc.mockujAjaxa('<div id="parametry_modala" data-title="Tytul modala">odpowiedz</div><div id="dolaczone_skrypty" data-skrypty=\'[{"url":"skrypt.js","funkcja_js":"ParametryWizualizacji"}]\'');
	WyswietlanieModali.wyswietlModala('url', [], 'parametry do inita');

	var zapisaneParametry=WyswietlanieModali.getParametry('ParametryWizualizacji');

	expect(ParametryWizualizacji.init).toHaveBeenCalled();
	expect(zapisaneParametry).toBe('parametry do inita');

    });
    it('pobierz parametry', function () {
	var foo = {funkcja: function () {}};
	WyswietlanieModali.dodajParametry('klucz', {nazwaFunkcji: foo});
	var wynik = WyswietlanieModali.getParametry('klucz');

	expect(wynik.nazwaFunkcji).toBe(foo);

    });

    it('pobierz parametry undefined', function () {
	var wynik = WyswietlanieModali.getParametry('naznalezionyKlucz');

	expect(wynik).toEqual(jasmine.any(Object));

    });


});
