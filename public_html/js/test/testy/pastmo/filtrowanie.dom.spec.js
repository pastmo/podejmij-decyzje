describe('filtrowanie.dom.spec.js ', function () {
    var filtrowanie;


    beforeEach(function () {
	jasmine.getFixtures().fixturesPath = 'base/js/test/testy/pastmo';
	loadFixtures('filtrowanie.dom.spec.html');
	filtrowanie = new Filtrowanie(
		{
		    getSelektorElementow: function () {
			return "#kafelki .element";
		    },
		    pobierzObiektDoUkryciaZElementu: function (element) {
			return element;
		    }
		});
    });

    it('test pobieranie aktywnych filtrow', function () {
	var wynik = filtrowanie.getAktywneFiltry();

	expect(wynik).toEqual(["Filtr 2"]);
	sprawdzWidocznoscElementow([2], [1, 3, 4]);

    });
    it('test zmiana aktywności filtrów filtrów', function () {

	$('#f1').click();

	var wynik = filtrowanie.getAktywneFiltry();

	expect(wynik).toEqual(["Filtr 1", "Filtr 2"]);

    });
    it('test zmiana przycisk clean', function () {

	czyscWszystkieFiltry();

	var wynik = filtrowanie.getAktywneFiltry();

	expect(wynik).toEqual([]);
	sprawdzWidocznoscElementow([1, 2, 3, 4], []);

    });
    it('test odswiezanie widoku', function () {

	filtrowanie.odswiezWidok();

	sprawdzWidocznoscElementow([2], [1, 3, 4]);

    });
    it('test widocznosc elementow po kliknieciu filtra', function () {
	czyscWszystkieFiltry();
	$('#f1').click();

	sprawdzWidocznoscElementow([1], [2, 3, 4, 5]);

    });
    it('test filtrowania po dodatkowym filtrze', function () {
	czyscWszystkieFiltry();
	filtrowanie.obslugPrzyciskFiltrowDodatkowych('Filtr dodatkowy');
	sprawdzTrescIFiltr();


    });

    it('test filtrowania po dodatkowym filtrze', function () {
	czyscWszystkieFiltry();
	filtrowanie.obslugPrzyciskFiltrowDodatkowych('Filtr dodatkowy');
	sprawdzTrescIFiltr();


    });

    it('test filtrowania po dodatkowym filtrze- niepelny napis', function () {
	czyscWszystkieFiltry();
	filtrowanie.obslugPrzyciskFiltrowDodatkowych('dodat');
	sprawdzWidocznoscElementow([5], [1, 2, 3, 4]);


    });

    it('test filtrowania po dodatkowym filtrze- ignorowanie wielkosci liter', function () {
	czyscWszystkieFiltry();
	filtrowanie.obslugPrzyciskFiltrowDodatkowych('filtr dodatkowy');
	sprawdzWidocznoscElementow([5], [1, 2, 3, 4]);


    });

    it('test filtrowania po dacie', function () {
	czyscWszystkieFiltry();
	$('#filtr_daty_ustawianie').val('Filtr dodatkowy');
	$('#filtr_daty_ustawianie').change();
	sprawdzTrescIFiltr();


    });

    it('test filtrowania po dodatkowym filtrze', function () {
	czyscWszystkieFiltry();
	$('#filtr_pola_tekstowego_ustawianie').val('Filtr dodatkowy');

	PastmoMoc.wywolajEventKlawisza('#filtr_pola_tekstowego_ustawianie', "keyup", 13);

	$('').keyup({keyCode: 13});
	sprawdzTrescIFiltr();


    });

    it('test filtrowania po dodatkowym filtrze- usuwanie wcześniejszych filtrow', function () {
	$('#filtr_pola_tekstowego_ustawianie').val('Filtr dodatkowy');

	PastmoMoc.wywolajEventKlawisza('#filtr_pola_tekstowego_ustawianie', "keyup", 13);

	$('').keyup({keyCode: 13});
	sprawdzTrescIFiltr();


    });
    function sprawdzTrescIFiltr() {
	var wynikTresc = $('#filtry_dodatkowe .filtr_dodatkowy .filtr_dodatkowy_tresc').html();
	var wynikFiltr = $('#filtry_dodatkowe .filtr_dodatkowy').data('filtr');

	expect(wynikTresc).toBe('Filtr dodatkowy');
	expect(wynikFiltr).toBe('Filtr dodatkowy');

	sprawdzWidocznoscElementow([5], [1, 2, 3, 4]);
    }

    it('test usuwanie filtra dodatkowego po kliknieciu na niego', function () {
	czyscWszystkieFiltry();
	filtrowanie.obslugPrzyciskFiltrowDodatkowych('Filtr dodatkowy');

	$('#filtry_dodatkowe .filtr_dodatkowy .filtr_dodatkowy_usuwanie').click();

	var wynik = $('#filtry_dodatkowe .filtr_dodatkowy');

	expect(wynik.length).toBe(0);

	sprawdzWidocznoscElementow([1, 2, 3, 4, 5], []);

    });

    it('test usuwanie wszystkich filtrow dodatkowych', function () {
	filtrowanie.obslugPrzyciskFiltrowDodatkowych('Filtr dodatkowy');
	filtrowanie.obslugPrzyciskFiltrowDodatkowych('Filtr dodatkowy2');

	$('#fdel').click();

	var wynik = $('#filtry_dodatkowe').children();

	expect(wynik.length).toBe(0);
	sprawdzWidocznoscElementow([1, 2, 3, 4, 5], []);

    });

    it('test przelaczenie filtra dodatkowego po', function () {
	czyscWszystkieFiltry();
	filtrowanie.obslugPrzyciskFiltrowDodatkowych('Filtr dodatkowy');

	$('#filtry_dodatkowe .filtr_dodatkowy').click();

	sprawdzWidocznoscElementow([1, 2, 3, 4, 5], []);

    });

    it('test filtrowania kilku filtrow', function () {
	czyscWszystkieFiltry();
	$('#f3').click();
	$('#f4').click();
	sprawdzWidocznoscElementow([4], [1, 2, 3, 5]);


    });
    it('aktywujSamodzielneButtony- przelaczanie klasy active', function () {
	czyscWszystkieFiltry();

	$('#filtry_kontener_buttonow .pro1').click();
	expect($('#filtry_kontener_buttonow .pro1').hasClass('active')).toBe(true);

	$('#filtry_kontener_buttonow .pro1').click();
	expect($('#filtry_kontener_buttonow .pro1').hasClass('active')).toBe(false);
    });

    it('aktywujSamodzielneButtony- przelaczanie klasy active', function () {
	czyscWszystkieFiltry();

	$('#filtry_kontener_buttonow .pro1').click();
	expect($('#filtry_kontener_buttonow .pro1').hasClass('active')).toBe(true);

	sprawdzWidocznoscElementow([1], [2, 3, 4, 5]);
    });

    function czyscWszystkieFiltry() {
	$('#f0').click();
    }

    function sprawdzWidocznoscElementow(elementyDostepne, elementyNiedostepne) {
	PastmoMoc.pojedynczyElement(elementyDostepne, '#el', true);
	PastmoMoc.pojedynczyElement(elementyNiedostepne, '#el', false);

    }
    ;


});
