function emptyFunction() {}

describe('Timer', function () {

    beforeEach(function () {
	aktywneOdswiezania = [];
    })

    it('sprawdzanie czy jest uruchomiona tylko jedna instancja odswiezania jednoczesnie', function () {
	var timer = new Timer(emptyFunction);
	spyOn(timer, 'setInterval');

	timer.uruchomCykliczneOdswiezanie();
	timer.uruchomCykliczneOdswiezanie();

	expect(timer.setInterval.calls.count()).toBe(1);
    });

    it('sprawdzanie czy jest usuwana funkcja podczas zatrzymania odswiezania', function () {
	var timer = new Timer(emptyFunction);
	spyOn(timer, 'setInterval');

	timer.uruchomCykliczneOdswiezanie();
	timer.zatrzymajCykliczneOdswiezanie();

	expect(aktywneOdswiezania.length).toBe(0);
    });
})