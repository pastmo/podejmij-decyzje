var Msg = {};

describe('pastmo.spec.js>>', function () {

    it('toString', function () {
	var wynik = Pastmo.toString("abc");
	expect(wynik).toBe("abc");
    });

    it('toString undefined', function () {
	var wynik = Pastmo.toString();
	expect(wynik).toBe("");
    });

    it('toString null', function () {
	var wynik = Pastmo.toString(null, 'domyslnie');
	expect(wynik).toBe("domyslnie");
    });

    it('get msg', function () {

	Msg.test = 'testowa wiadomość';

	var result = Pastmo.getMsg('test');

	expect(result).toBe('testowa wiadomość');
    });

    it('get msg', function () {
	var result = Pastmo.getMsg('not_found');

	expect(result).toBe('not found');
    });
    it('wyslij get', function () {
	spyOn(PastmoMoc, 'foo');
	PastmoMoc.mockujAjaxa(pastmoOdpowiedziAjax.statusSuccess);
	Pastmo.wyslijGet('adres', {}, PastmoMoc.foo);

	expect(PastmoMoc.foo).toHaveBeenCalled();
    });

    it('wyslij post', function () {
	spyOn(PastmoMoc, 'foo');
	PastmoMoc.mockujAjaxa(pastmoOdpowiedziAjax.statusSuccess);
	Pastmo.wyslijGet('adres', {});

	expect(PastmoMoc.foo).not.toHaveBeenCalled();
    });

    it('wyslij zwykły wyjątek', function () {
	spyOn(PastmoMoc, 'foo');
	spyOn(Pastmo, 'obsluzError');
	PastmoMoc.mockujAjaxa(pastmoOdpowiedziAjax.zwyklyBlad);
	Pastmo.wyslijGet('adres', {}, PastmoMoc.foo);

	expect(PastmoMoc.foo).not.toHaveBeenCalled();
	expect(Pastmo.obsluzError).toHaveBeenCalled();
    });
    it('wyslij ignorowanie logicznego bledu', function () {
	spyOn(PastmoMoc, 'foo');
	spyOn(Pastmo, 'obsluzError');
	PastmoMoc.mockujAjaxa(pastmoOdpowiedziAjax.zwyklyBlad);
	Pastmo.wyslijGet('adres', {}, PastmoMoc.foo, true);

	expect(PastmoMoc.foo).toHaveBeenCalled();
	expect(Pastmo.obsluzError).not.toHaveBeenCalled();
    });

    it('sprawdzCzySuccess true', function () {
	var wynik = Pastmo.sprawdzCzySuccess(pastmoOdpowiedziAjax.statusSuccess);

	expect(wynik).toBe(true);
    });

    it('sprawdzCzySuccess false', function () {
	var wynik = Pastmo.sprawdzCzySuccess(pastmoOdpowiedziAjax.zwyklyBlad);

	expect(wynik).toBe(false);
    });
    it('sprawdzCzySuccess false tekstowy', function () {
	var wynik = Pastmo.sprawdzCzySuccess(pastmoOdpowiedziAjax.bladUprawnienTekstowy);

	expect(wynik).toBe(false);
    });

    it('obsluzError zwykly blad', function () {
	spyOn(Pastmo, 'wyswietlKomunikat');

	Pastmo.obsluzError(pastmoOdpowiedziAjax.zwyklyBlad);

	expect(Pastmo.wyswietlKomunikat).toHaveBeenCalledWith('Brak posta');
    });

    it('obsluzError string', function () {
	spyOn(Pastmo, 'wyswietlKomunikat');

	Pastmo.obsluzError("Napis string");

	expect(Pastmo.wyswietlKomunikat).toHaveBeenCalledWith("Napis string");
    });

    it('obsluzError blad uprawnien', function () {
	spyOn(Pastmo, 'wyswietlKomunikat');

	Pastmo.obsluzError(pastmoOdpowiedziAjax.bladUprawnien);

	expect(Pastmo.wyswietlKomunikat).toHaveBeenCalledWith("Brak uprawnienia: 'Uprawnienia sprzedawcy'");
    });

    it('obsluzError blad uprawnien tekstowy- z funkcji error', function () {
	spyOn(Pastmo, 'wyswietlKomunikat');

	Pastmo.obsluzError(pastmoOdpowiedziAjax.bladUprawnienTekstowy);

	expect(Pastmo.wyswietlKomunikat).toHaveBeenCalledWith("Brak uprawnienia: 'Szczegoly firmy dodawanie edycja'");
    });

    it('obsluzError error json ale przeslany jako tekst', function () {
	spyOn(Pastmo, 'wyswietlKomunikat');

	Pastmo.obsluzError('{"success":false,"msg":"Brak uprawnienia: \u0027Uprawnienia grafika\u0027"}');

	expect(Pastmo.wyswietlKomunikat).toHaveBeenCalledWith("Brak uprawnienia: 'Uprawnienia grafika'");
    });

    it('Sprawdzanie uprawnien true', function () {
	var wynik = Pastmo.sprawdzUprawnienia(KodyUprawnien.DOMYSLNE_UPRAWNIENIE);

	expect(wynik).toBe(true);
    });
    it('Sprawdzanie uprawnien false', function () {
	var wynik = Pastmo.sprawdzUprawnienia('nieistniejace');

	expect(wynik).toBe(false);
    });
    it('Sprawdzanie uprawnien mock', function () {
	PastmoMoc.mockujUprawnienia("wynik mockowany");

	var wynik = Pastmo.sprawdzUprawnienia(KodyUprawnien.DODAWANIE_SZCZEGOLOW_ZAMOWIENIA);

	expect(wynik).toBe("wynik mockowany");
    });
    it('Sprawdzanie to number- liczba', function () {
	var wynik = Pastmo.toNumber(5);

	expect(wynik).toBe(5);
    });
    it('Sprawdzanie to number- nieliczba', function () {
	var wynik = Pastmo.toNumber('napis');

	expect(wynik).toBe(0);
    });
    it('wyswietlCene', function () {
	var wynik = Pastmo.wyswietlCene('5');

	expect(wynik).toBe('5.00');
    });
    it('strContains', function () {
	var wynik = Pastmo.strContains(2458, '4');

	expect(wynik).toBe(true);
    });
    it('strContains ignore case', function () {
	var wynik = Pastmo.strContains('napis', 'AP');

	expect(wynik).toBe(true);
    });
    it('testy parsowania stringa- tablica', function () {
	var str = '["Filtr 1"]';
	var wynik = JSON.parse(str);

	expect(wynik).toEqual(['Filtr 1']);
    });
    it('testy parsowania stringa- obiekt', function () {

	var str = '{"abc":"Filtr 1"}';

	var wynik = JSON.parse(str);

	expect(wynik).toEqual({abc: 'Filtr 1'});
    });
    it('czyTablicaZawieraFagment', function () {
	var arr = ['filtr jeden', 'filtr 2'];
	var str = 'jed';
	var wynik = Pastmo.czyTablicaZawieraFagment(arr, str);

	expect(wynik).toEqual(true);
    });
    it('pobierz funcje z window', function () {
	var klasa = {funkcja: function () {}};
	window['klasa'] = klasa;
	var wynik = Pastmo.pobierzFuncjeZWindow('klasa', 'funkcja');

	expect(wynik).toBe(klasa.funkcja);
    });
    it('pobierz funcje z window- nieistniejaca funckja', function () {
	var wynik = Pastmo.pobierzFuncjeZWindow('NieistniejacaKlasa', 'NieistniejacaFunkcja');

	expect(wynik).toEqual(jasmine.any(Function));
    });

});