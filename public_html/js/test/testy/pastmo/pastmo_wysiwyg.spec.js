describe('pastmo_wysiwyg ', function () {
    beforeEach(function(){
	jasmine.getFixtures().fixturesPath = 'base/js/test/testy/pastmo';
	loadFixtures('pastmo_wysiwyg.spec.html');
    });

    it('Sprawdzanie zmiany w htmlu', function () {

	$('#editor').wysiwyg();

	PastmoDom.keyDown('#editor', 77);

	expect($('#editor')).toContainText(String.fromCharCode(77));

    });

    it('przesloniecie readFileIntoDataUrl', function () {

	$('#editor').wysiwyg();

	PastmoDom.drop('#editor');

	//TODO: Wymyślić jak zrobić sprawdzanie, skoro ładowanie pliku kończy się po zakończeniu testu

    });
});