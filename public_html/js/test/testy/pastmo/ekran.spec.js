describe("ekran.spec.js", function () {


    beforeEach(function () {

    });

    it('getBiezacaSzerokosc', function () {
	var biezacaWielkosc = Ekran.getBiezacaSzerokosc();

	expect(biezacaWielkosc > 100).toBe(true);
    });

    it('getZapisanaSzerokosc- domyslna wielkosc jesli niezapisane', function () {
	var zapisanaWielkosc = Ekran.getZapisanaSzerokosc();

	expect(zapisanaWielkosc).toBe(Ekran.domyslnaWielkoscEkranu);
    });

    it('init- wysylanie zmiany wielkosci ekranu', function () {
	spyOn(Ekran, 'getBiezacaSzerokosc').and.returnValue(300);
	spyOn(Ekran, 'getZapisanaSzerokosc').and.returnValue(900);
	spyOn(Pastmo, 'wyslijPost');

	Ekran.init();

	expect(Pastmo.wyslijPost).toHaveBeenCalled();
	expect(Ekran.trybEkranu).toBe(Ekran.TRYB_SMART);
    });

    it('ustawTrybEkranu- duze ekrany', function () {
	spyOn(Ekran, 'getBiezacaSzerokosc').and.returnValue(900);

	Ekran.ustawTrybEkranu();

	expect(Ekran.trybEkranu).toBe(Ekran.TRYB_KOMPUTER);
    });
    it('ustawTrybEkranu- duze ekrany', function () {
	spyOn(Ekran, 'getBiezacaSzerokosc').and.returnValue(1300);

	Ekran.ustawTrybEkranu();

	expect(Ekran.trybEkranu).toBe(Ekran.TRYB_DUZY_EKRAN);
    });

    it('ustawTrybEkranu- komputer', function () {
	spyOn(Ekran, 'getBiezacaSzerokosc').and.returnValue(900);

	Ekran.ustawTrybEkranu();

	expect(Ekran.trybEkranu).toBe(Ekran.TRYB_KOMPUTER);
    });

    it('ustawTrybEkranu- tablety', function () {
	spyOn(Ekran, 'getBiezacaSzerokosc').and.returnValue(600);

	Ekran.ustawTrybEkranu();

	expect(Ekran.trybEkranu).toBe(Ekran.TRYB_TABLET);
    });
    it('ustawTrybEkranu- smart', function () {
	spyOn(Ekran, 'getBiezacaSzerokosc').and.returnValue(300);

	Ekran.ustawTrybEkranu();

	expect(Ekran.trybEkranu).toBe(Ekran.TRYB_SMART);
    });

});