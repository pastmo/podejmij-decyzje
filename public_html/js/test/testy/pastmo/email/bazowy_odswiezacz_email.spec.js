describe('Odswiezacz Email ', function () {

    it('pobieranie maili', function () {
	spyOn(OdswiezaczEmail, 'obsluzNoweMaile');
	PastmoMoc.mockujAjaxa(emailOdpowiedziAjax.odswiez_maile_ze_wszystkich_skrzynek);

	OdswiezaczEmail.wyslijZapytanieOdswiezaniaMaili();

	expect(OdswiezaczEmail.obsluzNoweMaile).toHaveBeenCalled();
    });

    it('ilosc aktywnych maili', function () {
	var wynik = OdswiezaczEmail.getIleMailiWOdpowiedzi(emailOdpowiedziAjax.odswiez_maile_ze_wszystkich_skrzynek);
	expect(wynik).toBe(16);
    });

//TODO_TEST: Zrobić, żeby test coś sprawdzał
    it('sprawdzanie bledu przy updatowaniu listy maili', function () {
	OdswiezaczEmail.wyswietlNoweMaile(emailOdpowiedziAjax.odswiez_maile_ze_wszystkich_skrzynek_jeden_nowy_mail);
    });
    it('pobierz czestotliwosc odswiezania', function () {
	czestotliwoscOdswiezaniaMaili = 35000;

	var wynik = OdswiezaczEmail.pobierzCzestotliwoscOdswiezania();
	expect(wynik).toBe(35000);
    });

});