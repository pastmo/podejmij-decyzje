var ProjektyTabeleAjax = {
tabelaProduktow: function () {
return `<table class="table statue-list">
    <thead>
	<tr>
	    <th>Nazwa:</th>
	    <th>Cena (zł)</th>
	    <th>Ilość (szt.)</th>
	    <th>Wartość (zł)</th>
	    <th></th>
	    <th>Akcja</th>
	</tr>
    </thead>
    <tbody>

	    	<tr data-id="24" data-name="RES002">
    	    <td>
    		<input value="1" type="checkbox" style="height:12px" class="product-checkbox"
    		       data-ilosc="12">
    		<a class="product-name" href="/edycja_produktu/1">RES002</a><br>
    		<span>0x0x0</span>
    	    </td>
    	    <td><span class="nowrap">10.00</span></td>
    	    <td>12</td>
    	    <td>
		        		<span class="nowrap">120    		    <input type="hidden" class="skladowa_ceny" value="120">
    		</span>
    	    </td>
    	    <td>
		        	    </td>
    	    <td>
    		<div class="btn-group">
    		    <button type="button" class="btn btn-xs btn-default btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			    Akcja&nbsp;<span class="caret"></span>
    		    </button>
    		    <ul class="dropdown-menu pull-right" role="menu">
			    				<li><button type="button" class="btn btn-white btn-block accept">Akceptacja</button></li>
				<li><button type="button" data-price="10.00" data-quantity="12"
					    class="btn btn-white btn-block edit-product">Edytuj</button></li>
				<li><button type="button" class="btn btn-white btn-block delete-product">Usuń</button></li>
			        		    </ul>
    		</div>
    	    </td>
    	</tr>
	    	<tr data-id="20" data-name="RES003">
    	    <td>
    		<input value="4" type="checkbox" style="height:12px" class="product-checkbox"
    		       data-ilosc="1123">
    		<a class="product-name" href="/edycja_produktu/2">RES003</a><br>
    		<span>9x9x9</span>
    	    </td>
    	    <td><span class="nowrap">15.00</span></td>
    	    <td>1123</td>
    	    <td>
		        		<span class="nowrap">16845    		    <input type="hidden" class="skladowa_ceny" value="16845">
    		</span>
    	    </td>
    	    <td>
		        	    </td>
    	    <td>
    		<div class="btn-group">
    		    <button type="button" class="btn btn-xs btn-default btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			    Akcja&nbsp;<span class="caret"></span>
    		    </button>
    		    <ul class="dropdown-menu pull-right" role="menu">
			    				<li><button type="button" class="btn btn-white btn-block accept">Akceptacja</button></li>
				<li><button type="button" data-price="15.00" data-quantity="1123"
					    class="btn btn-white btn-block edit-product">Edytuj</button></li>
				<li><button type="button" class="btn btn-white btn-block delete-product">Usuń</button></li>
			        		    </ul>
    		</div>
    	    </td>
    	</tr>


	<tr class="transport">
	    <td>
		Transport                    </td>
	    <td colspan="2">
		<input class="transport form-control skladowa_ceny" type="text" value="100.00"
		       disabled>
	    </td>
	    <td>zł</td>
	    <td>
		    		<button class="btn btn-xs btn-success" >
    		    <i class="glyphicon glyphicon-ok" title="Zaakceptowany"></i>
    		</button>
			    </td>
	    <td>

    		<button class="cancel-transport-accept btn btn-xs btn-success">Cofnij</button>

			    </td>
	</tr>

	<tr class="sum">
	    <td>
		Suma	    </td>
	    <td class="suma">
		17 065,00	    </td>
	    <td>

	    </td>
	    <td>

	    </td>
	    <td>

	    </td>
	</tr>
    </tbody>
</table>`;
	},
	wizualizacjeProjektu: function () {
	return `<form id='fileupload13' action='' method='POST' enctype='multipart/form-data'>
        <span class='btn btn-xs btn-success fileinput-button' style='display: none'>
    	<input type='file' name='files[]' multiple=''>
        </span>

        <div data-id='13' style='margin-bottom:10px;' class='col-sm-4 product-vis-282 '>
    	<a href='#' class='visual-notif hide'><i class='fa fa-eye-slash'></i></a>
    	<a href='#' class='visual-notif '><i class='fa white fa-eye'></i></a>


    	<a href='/upload/P3odZSr9Fd4OYvnwOt7GJEAoXaFZRR2CLcMYZKybpeiuDCWT.png' data-toggle='lightbox' class='img-thumbnail'>
    	    <div class='img-thumbnail vis-thumbnail' style='border:none' alt=''></div>
    	</a>
    	<a href='/projekty/wizualizacje/13' data-placement='left' data-toggle='popover' data-trigger='hover'
    	   data-html='true' data-content='x: 22, y: 22, z: 234' class='name'>
	       RES003    	</a>
    	<a id='product-vis-13' href='' class='hide' style='position: relative; z-index: 1;'></a>

    	<div id='html5_1ahn4g6h512m1kacsee1grg1ui63_container' class='moxie-shim moxie-shim-html5' style='position: absolute; top: 0px; left: 0px; width: 0px; height: 0px; overflow: hidden; z-index: 0;'>
    	    <input id='html5_1ahn4g6h512m1kacsee1grg1ui63' type='file' style='font-size: 999px; opacity: 0; position: absolute; top: 0px; left: 0px; width: 100%; height: 100%;' multiple='' accept=''></div></div>
    </form>
    <form id='fileupload6' action='' method='POST' enctype='multipart/form-data'>
        <span class='btn btn-xs btn-success fileinput-button' style='display: none'>
    	<input type='file' name='files[]' multiple=''>
        </span>

        <div data-id='6' style='margin-bottom:10px;' class='col-sm-4 product-vis-282 '>
    	<a href='#' class='visual-notif '><i class='fa fa-eye-slash'></i></a>
    	<a href='#' class='visual-notif hide'><i class='fa white fa-eye'></i></a>


    	<a href='/upload/P3odZSr9Fd4OYvnwOt7GJEAoXaFZRR2CLcMYZKybpeiuDCWT.png' data-toggle='lightbox' class='img-thumbnail'>
    	    <div class='img-thumbnail vis-thumbnail' style='border:none' alt=''></div>
    	</a>
    	<a href='/projekty/wizualizacje/6' data-placement='left' data-toggle='popover' data-trigger='hover'
    	   data-html='true' data-content='x: 442, y: 443, z: 443' class='name'>
	       RES003    	</a>
    	<a id='product-vis-6' href='' class='hide' style='position: relative; z-index: 1;'></a>

    	<div id='html5_1ahn4g6h512m1kacsee1grg1ui63_container' class='moxie-shim moxie-shim-html5' style='position: absolute; top: 0px; left: 0px; width: 0px; height: 0px; overflow: hidden; z-index: 0;'>
    	    <input id='html5_1ahn4g6h512m1kacsee1grg1ui63' type='file' style='font-size: 999px; opacity: 0; position: absolute; top: 0px; left: 0px; width: 100%; height: 100%;' multiple='' accept=''></div></div>
    </form>`;
	}
};