describe('ustawienia.spec.js', function () {

    it('pobieranie ustawien', function () {
	var wynik = Ustawienia.getCzasOdswiezania();

	expect(wynik).toBe(odswiezanieJs);
    });

    it('pobieranie jezyka', function () {
	var wynik = Ustawienia.getLocale();

	expect(wynik).toBe('pl');
    });

    it('pobieranie jezyka', function () {
	locale='inny';
	var wynik = Ustawienia.getLocale();

	expect(wynik).toBe('inny');
    });
})