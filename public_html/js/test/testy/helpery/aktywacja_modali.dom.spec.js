describe("aktywacja_modali =>", function () {
    var foo = function () {}
    beforeEach(function () {
	jasmine.getFixtures().fixturesPath = 'base/js/test/testy/helpery';
	loadFixtures('aktywacja_modali.dom.spec.html');
	spyOn(WyswietlanieModali, 'wyswietlModala');
	AktywacjaModali.init();
    });
    it('initOknoLogowania"', function () {

	$('.wyswietlanie_okna_logowania').click();

	expect(WyswietlanieModali.wyswietlModala).toHaveBeenCalledWith('/logowanie/wyswietl_okno_logowania', {}, jasmine.any(Object), jasmine.any(Object));
	;
    });
    it('initOknoLogowania"', function () {

	$('.wyswietlanie_okna_rejestracji').click();

	expect(WyswietlanieModali.wyswietlModala).toHaveBeenCalledWith('/logowanie/wyswietl_okno_rejestracji', {}, jasmine.any(Object), jasmine.any(Object));
	;
    });
});
