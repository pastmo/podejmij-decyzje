describe('decyzje.dom.spec.js', function () {

    beforeEach(function () {
	Decyzje.init();
	spyOn(Decyzje, 'obsluzWynikTak');
	spyOn(Decyzje, 'obsluzWynikNie');
	spyOn(Decyzje, 'obsluzWynikNierozstrzygniety');
    });

    it('obliczDedycje- tak', function () {
	spyOn(Decyzje, 'wyliczPlus').and.returnValue(5);
	spyOn(Decyzje, 'wyliczMinus').and.returnValue(2);

	Decyzje.obliczDedycje();

	expect(Decyzje.obsluzWynikTak).toHaveBeenCalled();

    });

    it('obliczDedycje- nie', function () {
	spyOn(Decyzje, 'wyliczPlus').and.returnValue(5);
	spyOn(Decyzje, 'wyliczMinus').and.returnValue(8);

	Decyzje.obliczDedycje();

	expect(Decyzje.obsluzWynikNie).toHaveBeenCalled();

    });

    it('obliczDedycje- nierozstrzygniete', function () {
	spyOn(Decyzje, 'wyliczPlus').and.returnValue(5);
	spyOn(Decyzje, 'wyliczMinus').and.returnValue(5);

	Decyzje.obliczDedycje();

	expect(Decyzje.obsluzWynikNierozstrzygniety).toHaveBeenCalled();

    });

});