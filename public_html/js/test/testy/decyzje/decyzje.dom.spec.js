describe('decyzje.dom.spec.js', function () {

    beforeEach(function () {
	jasmine.getFixtures().fixturesPath = 'base/js/test/testy/decyzje';
	loadFixtures('decyzje.dom.spec.html');

	Decyzje.init();
    });

    it('wyliczPlus', function () {
	$("#waga_plus1").val(4);
	$("#waga_plus2").val(6);

	var wynik = Decyzje.wyliczPlus();

	expect(wynik).toBe(10);

    });
    it('wyliczMinus', function () {
	$("#waga_minus1").val(4);
	$("#waga_minus3").val(6);

	var wynik = Decyzje.wyliczMinus();

	expect(wynik).toBe(10);

    });
    it('walidacja formularza- pusty formularz', function () {
	var wynik = $('#formularz_decyzji').valid();

	expect(wynik).toBe(false);

    });
    it('walidacja formularza- caly formularz wypelniony', function () {
	$('.waga_decyzji').val(5);
	$('.opis_decyzji').val("opis");

	var wynik = $('#formularz_decyzji').valid();

	expect(wynik).toBe(true);

    });
    it('walidacja formularza- wymagany opis jesli jest wpisana wartosc', function () {
	$("#waga_minus1").val(4);
	var wynik = $('#formularz_decyzji').valid();

	expect(wynik).toBe(false);

    });
    it('czySasiedniePoleJestWypelnione true- sprawdzanie dla opisu', function () {
	$("#waga_minus1").val(4);
	var wynik = Decyzje.czySasiedniePoleJestWypelnione("#opis_minus1");

	expect(wynik).toBe(true);

    });
    it('czySasiedniePoleJestWypelnione true- sprawdzanie dla wagi', function () {
	$("#opis_minus1").val("jakis opis");
	var wynik = Decyzje.czySasiedniePoleJestWypelnione("#waga_minus1");

	expect(wynik).toBe(true);

    });
    it('czySasiedniePoleJestWypelnione', function () {
	var wynik = Decyzje.czySasiedniePoleJestWypelnione("#opis_minus1");

	expect(wynik).toBe(false);

    });
    it('czySasiedniePoleJestWypelnione', function () {
	var wynik = Decyzje.czySasiedniePoleJestWypelnione("#waga_minus1");

	expect(wynik).toBe(false);

    });
    it('czyConajmniejJednaWartosc false', function () {
	var wynik = Decyzje.czyConajmniejJednaWartosc('.waga_plus');

	expect(wynik).toBe(false);

    });

    it('czyConajmniejJednaWartosc false- przeciwny selektor', function () {
	$("#waga_plus1").val(4);
	var wynik = Decyzje.czyConajmniejJednaWartosc('.waga_minus');

	expect(wynik).toBe(false);

    });

    it('czyConajmniejJednaWartosc true', function () {
	$("#waga_plus1").val(4);
	var wynik = Decyzje.czyConajmniejJednaWartosc('.waga_plus');

	expect(wynik).toBe(true);

    });

    it('pobierzParametryPosta', function () {
	spyOn(Pastmo, 'wyslijPost');

	$("#waga_minus1").val(5);
	$("#waga_plus1").val(4);
	var wynik = Decyzje.pobierzParametryPosta("nazwa_uzytkownika");

	expect(wynik).toBe('id=&nazwa=nazwa_uzytkownika&szczegoly_id_p0=&opis_p0=&waga_p0=4&szczegoly_id_p1=&opis_p1=&waga_p1=&szczegoly_id_p2=&opis_p2=&waga_p2=&szczegoly_id_m1=&opis_m0=&waga_m0=5&szczegoly_id_m2=&opis_m1=&waga_m1=&szczegoly_id_m3=&opis_m2=&waga_m2s=');

    });
    it('obsluzOdpowiedzZapisu', function () {

	$("#opis_plus2").val('Jakiś tam opis');

	Decyzje.obsluzOdpowiedzZapisu({"success": true, "msg": "", "decyzja": {"id": "42", "uzytkownik_id": "2", "nazwa": "Nazwa decyzji 1", "data_dodania": "2018-02-15 17:10:25", "wynik": "tak", "konto": null, "wyswietlanie_js": "", "updateSet": null, "translator": {}, "constantTranslator": null}, "szczegoly": [{"id": "21", "decyzja_id": "9", "typ": "plus", "opis": "fghj", "wartosc": "2", "konto": null, "wyswietlanie_js": "", "updateSet": null, "translator": {}, "constantTranslator": null}, {"id": "22", "decyzja_id": "9", "typ": "plus", "opis": "iiiiiiiii", "wartosc": "3", "konto": null, "wyswietlanie_js": "", "updateSet": null, "translator": {}, "constantTranslator": null}, {"id": "23", "decyzja_id": "9", "typ": "minus", "opis": "nbvc", "wartosc": "1", "konto": null, "wyswietlanie_js": "", "updateSet": null, "translator": {}, "constantTranslator": null}, {"id": "24", "decyzja_id": "9", "typ": "minus", "opis": "nbgvc", "wartosc": "3", "konto": null, "wyswietlanie_js": "", "updateSet": null, "translator": {}, "constantTranslator": null}]});
	expect($("#id").val()).toBe('42');
	expect($("#nazwa").val()).toBe('Nazwa decyzji 1');

    });

});
