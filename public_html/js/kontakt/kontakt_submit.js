var KontaktSubmit = {
    init: function () {
	$(document).on('click', '#wysylanie_kontaktu', function (e) {
	    e.stopPropagation();
	    var przycisk = $(this);
	    var idCaptcha = przycisk.data('id_captcha');

	    var formularz = $('#kontakt-form');

	    var isValid = formularz.valid();

	    if (isValid) {
		var formularz = $('#kontakt-form');
		var form = formularz.serialize();
		var url = formularz.data('url');

		var loader = $('#miejsce_na_loader').addClass('loader');

		Pastmo.wyslijPost(url, form, function () {
		    loader.removeClass('loader');
		    formularz.find('input[type!="checkbox"]').val("");
		    formularz.find('textarea').val("");
		    formularz.find('input:checkbox').prop('checked', false);
		    grecaptcha.reset();

		    $('#msg').empty().show().html('Twoja wiadomość została wysłana').delay(3000).fadeOut(300);
		});

	    }
	});

	$('#kontakt-form').submit(function (e) {
	    e.preventDefault();
	})
    }};

function kontaktSubmit(token) {


}

$(document).ready(function () {
    KontaktSubmit.init();
});
